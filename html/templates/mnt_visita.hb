<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Visita mantenimiento</h5>
  </div>
  <div class="col-md-7">
    
  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.mantenimiento}}}
</fieldset>

 <fieldset class="siom-fieldset">
  <legend>Mantenimiento</legend>
  {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Visita</legend>
  	{{{loadTemplate "form_formulario" data}}}
</fieldset>
{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js"  type="text/javascript" charset="utf-8"></script>
<script src="js/mnt_visita.js" type="text/javascript" charset="utf-8"></script>
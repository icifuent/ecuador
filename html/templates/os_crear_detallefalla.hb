<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Crear orden de servicio/Reporte falla</h5>
    </div>
</div>
<div>
    <fieldset class="siom-fieldset">
        <legend>Reporte Falla</legend>
        {{#data.cabFalla}}
        <div class="row">
            <label id="usua_id" class="col-sm-2 siom-value" style="display:none;">{{usua_id}}</label>
        </div>
        <div class="row">
            <label class="col-sm-1 siom-label">USUARIO</label>
            <label id="usua_nombre" class="col-sm-2 siom-value">{{usua_nombre}}</label>
            <label class="col-sm-1 siom-label">FECHA DETECCION DE LA FALLA</label>
            <label id="defa_fecha_creacion" class="col-sm-2 siom-value">{{defa_fecha_creacion}}</label>
            <label class="col-sm-1 siom-label">LATITUD</label>
            <label id="defa_latitud" class="col-sm-2 siom-value">{{defa_latitud}}</label>
            <label class="col-sm-1 siom-label">LONGITUD</label>
            <label id="defa_longitud" class="col-sm-1 siom-value">{{defa_longitud}}</label>
        </div>
        {{else}}
        <p class="siom-no-data">Sin listado de Emplazamiento</p>
        {{/data.cabFalla}}
    </fieldset>

    {{#data.emplazamiento}}
    <input autocomplete="off" type="hidden" id="empl-prueba" value="{{empl_id}}"> {{/data.emplazamiento}}
    <fieldset class="siom-fieldset">
        <legend>Emplazamiento</legend>
        <div>
            {{#data.emplazamiento}}
            <table width="100%">
                <tr>
                    <td width="40%" colspan="2">
                        <div class="siom-label">NOMBRE</div>
                        <div class="siom-value">{{empl_nombre}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">NEMONICO</div>
                        <div class="siom-value">{{empl_nemonico}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">CLASIFICACIÓN</div>
                        <div class="siom-value">{{clas_nombre}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">TECNOLOGIA</div>
                        <div class="siom-value">{{tecn_nombre}}</div>
                    </td>
                </tr>
                <tr>
                    <td width="40%" colspan="2">
                        <div class="siom-label">DIRECCIÓN</div>
                        <div class="siom-value">{{empl_direccion}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">COMUNA</div>
                        <div class="siom-value">{{comu_nombre}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">REGIÓN</div>
                        <div class="siom-value">{{regi_nombre}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">ZONA</div>
                        <div class="siom-value">{{zona_nombre}}</div>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <div class="siom-label">MACROSITIO</div>
                        <div class="siom-value"></div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">SUBTEL</div>
                        <div class="siom-value"></div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">ESPACIO</div>
                        <div class="siom-value">{{empl_espacio}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">TIPO ACCESO</div>
                        <div class="siom-value">{{empl_tipo_acceso}}</div>
                    </td>
                    <td width="20%">
                        <div class="siom-label">ACCESO</div>
                        <div class="siom-value"></div>
                    </td>
                </tr>
                <tr>
                    <td width="60%" colspan="3" valign="top">
                        <div class="siom-label">OBSERVACIÓN</div>
                        <div class="siom-value">{{empl_observacion}}</div>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <div class="siom-label">RIESGO VANDALICO</div>
                        <div class="siom-value"></div>
                    </td>
                </tr>
            </table>
            {{/data.emplazamiento}}
        </div>
    </fieldset>
    <fieldset class="siom-fieldset" style="margin-top:10px;">
        <legend>Orden de servicio</legend>
        <form class="form-horizontal siom-form-tiny" role="form" action="#/os/crear/defa" method="POST" id="form_crear">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="fecha_solicitud" class="col-sm-4 control-label">Fecha programada de atención</label>
                        <input autocomplete="off" type="hidden" name="orse_fecha_solicitud" value="">
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="col-md-4">
                                    <input autocomplete="off" id="fecha_solicitud" class="form-control" size="14" type="text" data-date-autoclose="true" name="orse_fecha_solicitud_fecha"
                                        placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />
                                </div>
                                <div class="col-md-4">
                                    <div class="input-group bootstrap-timepicker">
                                        <input autocomplete="off" type="text" class="form-control timepicker" aria-describedby="addon" data-field="time" name="orse_fecha_solicitud_hora"
                                            data-modal-backdrop="true" placeholder="hh:mm" value="{{now '%H:%M'}}">
                                        <span class="input-group-addon" id="addon">
                                            <i class="glyphicon glyphicon-time"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-4 text-left">
                                    <div class="checkbox">
                                        <label>
                                            <input autocomplete="off" type="checkbox" value="" name="orse_fecha_solicitud_ahora" id="orse_fecha_solicitud_ahora">Ahora</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tipo" class="col-sm-4 control-label">Tipo</label>
                        <div class="col-sm-8">
                            <select id="tipo" class="selectpicker" data-width="100%" name="orse_tipo" title="Seleccione tipo" data-size="6" data-container="body">
                                {{#data.tipos}}
                                <option value="{{.}}">{{.}}</option>
                                {{/data.tipos}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="empresa" class="col-sm-4 control-label">Empresa</label>
                        <div class="col-sm-8">
                            <select id="empresa" class="selectpicker" data-width="100%" name="empr_id" title="Seleccione empresa" data-container="body">
                                {{#data.empresa}}
                                <option value="{{empr_id}}" data-coem_tipo="{{coem_tipo}}">{{empr_nombre}}</option>
                                {{/data.empresa}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="especialidad" class="col-sm-4 control-label">Especialidad</label>
                        <div class="col-sm-8">
                            <select id="especialidad" class="selectpicker" data-width="100%" name="espe_id" data-value="{{data.especialidad.espe_id}}" title="Seleccione especialidad" data-container="body">
                                {{#data.especialidades}}
                                <option value="{{espe_id}}">{{espe_nombre}}</option>
                                {{/data.especialidades}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="subespecialidad" class="col-sm-4 control-label">Alarma</label>
                        <div class="col-sm-8">
                            <select id="subespecialidad" class="selectpicker" data-width="100%" name="sube_id" data-hide-disabled="true" title="Seleccione alarma"
                                data-container="body" data-value="{{data.subespecialidad.sube_id}}">
                                {{#data.subespecialidades}}
                                <option value="{{sube_id}}" data-espe-id="{{espe_id}}">{{sube_nombre}}</option>
                                {{/data.subespecialidades}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="indisponibilidad" class="col-sm-4 control-label">Indisponibilidad</label>
                        <div class="col-sm-8">
                            <select id="indisponibilidad" class="selectpicker" data-width="100%" name="orse_indisponibilidad" title="seleccione indisponibilidad"
                                data-container="body">
                                <option value="SI">SI</option>
                                <option value="NO">NO</option>
                                <option value="PARCIAL">PARCIAL</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
                        <div class="col-sm-8">
                            {{#data.cabFalla}}
                            <textarea class="form-control" name="orse_descripcion" id="orse_descripcion" rows="5" placeholder="Descripcion">{{defa_descripcion}}</textarea>
                            <div id="orse_descripcion_palabras" class="siom-info text-right text-danger">
                                <label id="contPalabras"></label>
                                <label>palabras.</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tag" class="col-sm-4 control-label">Cód. Denuncia Falla asociada</label>
                        <div class="col-sm-8">
                            <input autocomplete="off" type="text" class="form-control" name="orse_defaid" placeholder="Cód. Denuncia Falla asociada" value="{{defa_id}}"
                                readonly/>
                        </div>
                        {{/data.cabFalla}}
                    </div>
                    <div class="form-group">
                        <label for="tag" class="col-sm-4 control-label">Cód. Incidencia</label>
                        <div class="col-sm-8">
                            <input autocomplete="off" type="text" class="form-control" name="orse_tag" placeholder="Cód. Incidencia" />
                            <div id="orse_tag_mensaje" class="siom-info text-right text-danger"> Si no tiene ticket REMEDY, poner "SIN TICKET"</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="formulario" class="col-sm-4 control-label">Formulario</label>
                        <div class="col-sm-8">
                            <select id="formulario" class="selectpicker" data-width="100%" name="form_id" data-valores="{{JSON data.def_formulario}}"
                                title="Seleccione formulario" multiple data-container="body" data-hide-disabled="true">
                                {{#data.formularios}}
                                <option data-osdf="{{form_id}}" value="{{form_id}}">{{form_nombre}}</option>
                                {{/data.formularios}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="orden_servicio" class="col-sm-4 control-label">Causa Afectación</label>
                        <div class="col-sm-8">
                            <select id="causa_afectacion" class="selectpicker" data-width="100%" name="caaf_id" data-valores="{{JSON data.def_formulario}}"
                                title="Seleccione causa" data-container="body" data-hide-disabled="true">
                                {{#data.causa_afectacion}}
                                <option data-osdf="{{caaf_id}}" value="{{caaf_id}}">{{caaf_nombre}}</option>
                                {{/data.causa_afectacion}}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <label class="control-label">Documentación</label>
                        <br>
                        <span class="btn btn-default btn-sm btn-file">Agregar archivo(s)
                            <input autocomplete="off" type="file" name="archivos[]" id="archivo-0" data-id="0">
                        </span>
                        <small class="text-muted"> Máximo
                            <b>{{window.config.max_file_upload}} 20.0 MB</b> por archivo</small>
                        <ul id="listado-archivos" class="list-group siom-file-list col-md-10"></ul>
                    </div>
                    <div class="row">
                        <table class="table table-responsive">
                            {{#each data.eviFalla}}
                            <tr class="row">
                                <td id="{{repo_id}}" class="col-md-3 text-center">
                                    <a href="{{repo_ruta}}" target="blank">
                                        <img class="img-thumbnail" style="width:50%; height:30%;" src="{{repo_ruta}}" />
                                    </a>
                                    <br>
                                    <a class="btn btn-danger" onclick="ocultarEvidencia('{{repo_id}}')">Quitar</a>
                                </td>
                            </tr>
                            {{/each}}
                            <tr class="row">
                                {{#each data.eviVideo}}
                                <td id="{{repo_id}}" class="col-md-6 text-center">
                                    <video style="width: 60%;" controls>
                                        <source src="{{repo_ruta}}" type="video/mp4">
                                        <embed src="{{repo_ruta}}" type="application/x-shockwave-flash" width="980" height="570" allowscriptaccess="always" allowfullscreen="true"
                                            autoplay="false"></embed>
                                    </video>
                                    <br>
                                    <a class="btn btn-danger" onclick="ocultarEvidencia('{{repo_id}}')">Quitar</a>
                                </td>
                                {{/each}} {{#each data.eviAudio}}
                                <td id="{{repo_id}}" class="col-md-6 text-center">
                                    <audio src="{{repo_ruta}}" preload="none" controls></audio>
                                    <br>
                                    <a class="btn btn-danger" onclick="ocultarEvidencia('{{repo_id}}')">Quitar</a>
                                </td>
                                {{/each}}
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row text-center">
                <div class="col-md-12">
                    {{#data.emplazamiento}}
                    <input autocomplete="off" type="hidden" name="empl_id" id="empl_id" value="{{empl_id}}"> {{/data.emplazamiento}}
                    <a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-primary" data-loading-text="Creando..." autocomplete="off" id="submit_crear">Crear O.S.</button>
                </div>
            </div>
        </form>
    </fieldset>
</div>

<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/os_crear_detallefalla.js" type="text/javascript" charset="utf-8"></script>
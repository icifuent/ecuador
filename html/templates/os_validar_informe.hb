<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Validación de informe</h5>
 </div>
 <div class="col-md-7 text-right">
 	{{#if status}}
 	<button type="button" class="btn btn-primary btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{data.informe.info_id}}">Descargar PDF</button>
   	{{/if}}
 </div>
</div>
{{#if status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;" id="siom-validar-informe">
    <legend>Informe</legend>
    {{#if data.informe}}
    	<div class="row">
		  <label class="col-sm-2 siom-label">GENERADO POR</label>
		  <label class="col-sm-4 siom-value">{{data.informe.usua_creador}}</label>
		  <label class="col-sm-2 siom-label">FECHA</label>
		  <label class="col-sm-4 siom-value">{{data.informe.info_fecha_creacion}}</label>
		</div>

		<hr>
		
		{{{loadTemplate "form_informe" data}}}

		<hr>

		<div class="row">
		 <div class="col-md-12 text-right">
		 	{{#if status}}
		 	<button type="button" class="btn btn-primary btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{data.informe.info_id}}">Descargar PDF</button>
		   	{{/if}}
		 </div>
		</div>

		<hr>

    	<form class="form form-horizontal siom-form-tiny" role="form" action="#/os/informe/{{data.os.orse_id}}/validar/{{data.informe.info_id}}" method="POST" id="siom-validar-informe-form">
    		<div class="form-group">
				<label for="info_estado" class="col-sm-4 control-label">Validación</label>
			   	<div class="col-sm-4">
			   		<select class="selectpicker" name="info_estado"  title="seleccione validación"  data-container="body">
                     	<option value=""></option>
			    		<option value="PREAPROBADO">PRE-APROBADO</option>
			    		<option value="RECHAZADO">RECHAZADO</option>
			    	</select>
		        </div>
			</div>
    		<div class="form-group">
				<label for="info_observacion" class="col-sm-4 control-label">Observación</label>
		   		<div class="col-sm-4">
		    		<textarea class="form-control" name="info_observacion" rows="4"></textarea>
		   		</div>
			</div>
			<div class="row text-center">
		     	<div class="col-md-12">
		     		<a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
		      		<button type="submit" class="btn btn-primary" data-loading-text="Validando..." autocomplete="off">Validar</button>
		     	</div>
		    </div>
		</form>

    {{else}}
    	<div class="alert alert-warning" role="alert">
	  	<strong>Aviso</strong> ruta indicada no tiene un informe definido.
		</div>
    {{/if}}
</fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}
    
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/os_validar_informe.js" type="text/javascript" charset="utf-8"></script>


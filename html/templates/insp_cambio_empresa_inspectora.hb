<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Cambio de empresa inspectora</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>

{{#if data.status}}
{{{loadTemplate "template_emplazamiento" data.emplazamiento}}}

{{{loadTemplate "template_inspeccion" data.inspeccion}}}

<fieldset class="siom-fieldset" style="margin-top:20px;">
    <legend>Cambio de empresa inspectora</legend>

    <form class="form form-horizontal siom-form-tiny" role="form" action="#/insp/cambio_empresa_inspectora/{{data.inspeccion.insp_id}}" method="POST" id="siom-form-cambio-empresa">

      <div class="row form-group">
        <div class="col-sm-2 col-md-offset-1">
          <label class="control-label">EMPRESA ACTUAL</label>
        </div>
        <div class="col-sm-4">
          <label class="control-label">{{data.inspeccion.empr_nombre}}</label>
        </div>
      </div>

   		<div class="row form-group">
		    <div class="col-sm-2 col-md-offset-1">
		    	<label class="control-label">EMPRESA NUEVA</label>
		   	</div>

		    <div class="col-sm-4">
          <input type="hidden" name="old_empr_id" value="{{data.inspeccion.empr_id}}"/>
		    	<select id="empr_id" class="selectpicker" data-width="100%" title='Seleccione una empresa' name="empr_id">
		    		<option value=""></option>
		    	{{#data.empresas}}
		     		<option value="{{empr_id}}">{{empr_nombre}}</option>
		    	{{/data.empresas}}
		    	</select>
		   	</div>
	  	</div>

	  
  		<hr>
        <div class="row text-center siom-form-actions">
          <div class="col-md-12">
            <a href="#/insp/bandeja" class="btn btn-default">Cancelar</a>
           
            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..." autocomplete="off" id="submit_cambiar">Guardar</button>
          </div>
        </div>
  	</form>
 </fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/insp_cambio_empresa_inspectora.js" type="text/javascript" charset="utf-8"></script>

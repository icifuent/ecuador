<table width="100%">
  <tr>
      <td width="20%">
      	<div class="siom-label">Nº MNT</div>
 		<div class="siom-value">{{empty mant_id}}</div>
      </td>
      <td width="20%">
      	<div class="siom-label">PERIODO</div>
 		     <div class="siom-value">{{empty peri_nombre}}</div>
      </td>
    <td width="20%">
      	<div class="siom-label">ESPECIALIDAD</div>
 		    <div class="siom-value">{{empty espe_nombre}}</div>
    </td>
      <td width="20%">
      	<div class="siom-label">ALARMA</div>
 		<div class="siom-value">{{empty sube_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">DESCRIPCIÓN</div>
    <div class="siom-value">{{empty mant_descripcion}}</div>
      </td>
  </tr>
  <tr>
      <td width="20%">
      	<div class="siom-label">EMPRESA</div>
 		<div class="siom-value">{{empty empr_nombre}}</div>
      </td>
      <td width="20%">
      	<div class="siom-label">ESTADO</div>
 		<div><div class="siom-value col-xs-4" style="padding-right:0px;padding-left:0px;">{{empty mant_estado}}</div> {{#if mant_observacion}} : {{mant_observacion}}{{/if}}</div>
      </td>
      <td width="20%">
      	<div class="siom-label">FECHA INICIO PERIODO</div>
 		<div class="siom-value">{{empty mape_fecha_inicio}}</div>
      </td>
      <td width="20%">
      	<div class="siom-label">FECHA PROGRAMADA</div>
 		<div class="siom-value">{{empty mant_fecha_programada}}</div>
      </td>
      <td width="20%">
      	<div class="siom-label">FECHA CIERRE PERIODO</div>
 		<div class="siom-value">{{empty mape_fecha_cierre}}</div>
      </td>
  </tr>

  {{#if archivos}}
  <tr>
      <td width="100%" colspan="5" align="left">
        <div class="siom-label">DOCUMENTACIÓN</div>
        <div class="siom-docu-link">
           {{#each archivos}}
            <a href="{{repo_ruta}}" target="blank">{{repo_nombre}}</a><small> | {{repo_descripcion}}</small><br>
           {{/each}}
        </div>
      </td>
  </tr>
  {{/if}}

</table>

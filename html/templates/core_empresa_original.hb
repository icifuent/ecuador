<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Empresas</h5>
  </div>
  <div class="col-md-7 text-right">
    <a class="btn btn-primary btn-sm" href="#/core/empresa">
      <span class="glyphicon glyphicon-plus" aria-hidden="true" id="AgregarBton"></span> Agregar
   </a>
  </div>
</div>

{{#if data.status}}
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/core/empresa/filtro" method="POST" id="siom-form-empresa">
            <div class="form-group">
              <label for="id" class="col-sm-5 control-label">Rut</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="empr_rut" value="{{data.empr_rut}}">
              </div>
            </div>
            <hr>

            <div class="form-group">
              <label for="nemonico" class="col-sm-5 control-label">Nombre</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" name="empr_nombre" value="{{data.empr_nombre}}">
              </div>
            </div>
            <hr>

             <div class="form-group">
              <label for="periodos" class="col-sm-5 control-label">Estado</label>
              <div class="col-sm-7">
                <select id="estado" class="selectpicker" data-width="100%"   name="empr_estado">
                 <option value="">Todos</option>
                    <option value="ACTIVO">ACTIVO</option>
                    <option value="NOACTIVO">NOACTIVO</option>
                </select>
              </div>
            </div>
            <hr>

            <div class="form-group">
              <label for="region" class="col-sm-5 control-label">Comuna</label>
              <div class="col-sm-7">
                <select id="regiones"  class="selectpicker" data-width="100%" title='Todas' name="comu_id">
                  <option value="">Todas</option>
                  {{#data.comunas}}
                    <option value="{{comu_id}}">{{comu_nombre}}</option>
                  {{/data.comunas}}
                </select>
              </div>
            </div>


        </form>
      </div>
    </div>
  </div>
  <div class="col-md-9" id="core-empresa-lista">
  </div>
</div>






{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}




<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/core_empresa.js" type="text/javascript" charset="utf-8"></script>

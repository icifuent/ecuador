<link rel="stylesheet" type="text/css" href="css/dashboardmnt.css" media="screen" />

<div class="row">
	<div class="col-md-12 text-right">
        <div class="siom-form-tiny">
            Alcance: <b>{{zona_alcance}}</b>
        </div>
    </div>
</div>
{{#if status}}
<div class="row">
  <div class="col-md-3">
        
        <tr>
        </tr>
    <div class="siom-chart-title">
      <table>
      <tr></tr>
      <tr>
      ORDEN DE SERVICIO POR ESPECIALIDAD
      </tr></table>
    </div>
    <div class="siom-chart" id="siom-chart-os-espe" data-data="{{JSON os_espe}}">Generando gráfico...</div>
  </div>
  <div class="col-md-4">
    <div class="siom-chart-title">AVANCE ORDENES DE SERVICIO</div>
    <div class="siom-chart" id="siom-chart-os-zona" data-data="{{JSON os_zona}}">Generando gráfico...</div>
  </div>
  <div class="col-md-4">
    <div class="siom-chart-title">GASTOS ORDEN DE SERVICIO</div>
    <div class="siom-chart" id="siom-chart-os-gastos" data-data="{{JSON os_gasto}}">Generando gráfico...</div>
  </div>
</div>



<hr>

<div class="siom-form-tiny text-right">
  Zona 
  <select id="os_zona" class="selectpicker" data-container="body" data-espe="{{JSON os_espe_zona}}" data-gestor="{{JSON os_gestor_zona}}" data-gasto="{{JSON os_gasto_zona}}">
    {{#os_zonas}}
    <option value="{{.}}">{{.}}</option>
    {{/os_zonas}}
  </select>
</div> 

<div class="row" style="margin-top:10px;">
  <div class="col-md-3">
    <div class="siom-chart-title">ORDEN DE SERVICIO POR ESPECIALIDAD<br><small id="siom-chart-os-espe-zona-nombre"></small></div>
    <div class="siom-chart" id="siom-chart-os-espe-zona">Generando gráfico...</div>
  </div>
  <div class="col-md-4">
    <div class="siom-chart-title">AVANCE ORDENES DE SERVICIO POR GESTOR<br><small id="siom-chart-os-gestor-zona-nombre"></small></div>
    <div class="siom-chart" id="siom-chart-os-gestor-zona">Generando gráfico...</div>
  </div>
  <div class="col-md-4">
    <div class="siom-chart-title">GASTOS ORDEN DE SERVICIO POR GESTOR<br><small id="siom-chart-os-gastos-zona-nombre"></small></div>
    <div class="siom-chart" id="siom-chart-os-gastos-zona">Generando gráfico...</div>
  </div>
</div>

{{else}}
  <div class="alert alert-danger" role="alert">
    {{error}}
    {{#if debug}}<br><small>{{debug}}</small>{{/if}}
  </div>
{{/if}}

<script src="js/dashboard_os_graficos.js" type="text/javascript" charset="utf-8"></script>
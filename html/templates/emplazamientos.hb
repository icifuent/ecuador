<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Emplazamientos</h5>
  </div>
  <div class="col-md-7 text-right">
    <div class="btn-group btn-group-sm" data-toggle="buttons">
      <label class="btn btn-primary active" data-tipo="mapa">
        <input autocomplete="off" type="radio" name="options"  checked> Mapa
      </label>
      <label class="btn btn-primary" data-tipo="lista">
        <input autocomplete="off" type="radio" name="options" id="option2"> Lista
      </label>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/emplazamientos/filtro" method="POST" id="siom-form-emplazamientos-filtro">
          <input  type="hidden" name="tipo" value="mapa">

          <div class="form-group">
            <div class="col-sm-5">
              <button type="button" class="btn btn-primary btn-default" id="LimpiarEmplazamientos">
                <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
            </div>
            <div class="col-sm-7">
              <button type="button" class="btn btn-primary btn-default" id="BuscarEmplazamientos">
                <span class="glyphicon" aria-hidden="true"></span> Buscar
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="nombre" class="col-sm-5 control-label">Nombre</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="nombre" name="empl_nombre">
            </div>
          </div>

          <div class="form-group">
            <label for="nemonico" class="col-sm-5 control-label">Nemónico</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="nemonico" name="empl_nemonico">
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Visita en proceso</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" title='Todas' name="empl_visitas">
                <option value=""></option>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Indisponibilidad</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" title='Todas' name="orse_indisponibilidad">
                <option value=""></option>
                <option value="SI">SI</option>
                <option value="NO">NO</option>
                <option value="PARCIAL">PARCIAL</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Macrositio</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" title='Todas' name="empl_macrositio">
                <option value=""></option>
                <option value="1">SI</option>
                <option value="0">NO</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Subtel</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" title='Todas' name="empl_subtel">
                <option value=""></option>
                <option value="1">SI</option>
                <option value="0">NO</option>
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Tecnología</label>
            <div class="col-sm-7">
              <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas' multiple name="tecn_id">
                {{#data.tecnologias}}
                <option value="{{tecn_id}}">{{tecn_nombre}}</option>
                {{/data.tecnologias}}
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="direccion" class="col-sm-5 control-label">Dirección</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" name="empl_direccion" id="empl_direccion">
            </div>
          </div>

          <div class="form-group">
            <label for="zona" class="col-sm-5 control-label">Zona</label>
            <div class="col-sm-7">
              <select name="zona_id" class="selectpicker" data-width="100%" multiple title='Todas'>
                {{#data.zonas}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.zonas}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="region" class="col-sm-5 control-label">Región</label>
            <div class="col-sm-7">
              <select name="regi_id" class="selectpicker" data-width="100%" multiple title='Todas'>
                {{#data.regiones}}
                <option value="{{regi_id}}">{{regi_nombre}}</option>
                {{/data.regiones}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="cluster" class="col-sm-5 control-label">Cluster</label>
            <div class="col-sm-7">
              <select name="clus_id" class="selectpicker" data-width="100%" multiple title='Todos'>
                {{#data.clusters}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.clusters}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="categorias" class="col-sm-5 control-label">Categoría</label>
            <div class="col-sm-7">
              <select name="cate_id" class="selectpicker" data-width="100%" multiple title='Todos'>
                {{#data.categorias}}
                <option value="{{cate_id}}">{{cate_nombre}}</option>
                {{/data.categorias}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="duto_torre" class="col-sm-5 control-label">Dueños Torres</label>
            <div class="col-sm-7">
              <select name="duto_id" class="selectpicker" data-width="100%" multiple title='Todas'>
                <option value="">Todos</option>
                {{#data.duenos_torres}}
                <option value="{{duto_id}}">{{duto_nombre}}</option>
                {{/data.duenos_torres}}
              </select>
            </div>
          </div>

          <!--
          <div class="text-center">
            <button type="submit" class="btn btn-primary" data-loading-text="Filtrando..." >Filtrar</button>
          </div>
          -->
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-9" id="emplazamientos-estados">
  </div>
</div>

<script src="js/emplazamientos.js" type="text/javascript" charset="utf-8"></script>
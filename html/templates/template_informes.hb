<fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Informes</legend>
      {{#each this}}
        <div class="row">
          <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
          <label class="col-sm-2 siom-value">{{info_fecha_creacion}}</label>
          <label class="col-sm-2 siom-label">CREADO POR</label>
          <label class="col-sm-3 siom-value">{{usua_creador}}</label>
          <label class="col-sm-1 siom-label">ESTADO</label>
          <label class="col-sm-1 siom-value">{{info_estado}}</label>
          <label class="col-sm-1 siom-link">
          <a href="#/insp/informe/{{info_id_relacionado}}/ver/{{info_id}}"> VER </a>
                    
          </label>
        </div>

        {{#isnt info_estado 'SINVALIDAR'}}
        <div class="row">
          <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
          <label class="col-sm-2 siom-value">{{info_fecha_validacion}}</label>
          <label class="col-sm-2 siom-label">VALIDADO POR</label>
          <label class="col-sm-2 siom-value">{{usua_validador}}</label>
        </div>
        {{/isnt}}

        {{#gt ../data.informes.length 1}}
        <hr>
        {{/gt}}
      {{else}}
      <p class="siom-no-data">Sin informes</p>
      {{/each}}
    </fieldset>

    <script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>

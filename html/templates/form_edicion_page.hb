<div class="form-edicion-container form-edicion-container-page" id="page_{{fogr_id}}"  data-type="page" data-properties="{{JSON properties}}" data-options="{{JSON options}}">
  <div class="row header">
    <div class="col-md-4 title">{{#if fogr_nombre}} {{fogr_nombre}} {{else}} <span class="text-danger">sin nombre</span> {{/if}}</div>
    <div class="col-md-8 menu">
      <ul class="nav nav-pills pull-right">
        <li class="dropdown">
          <a id="{{fogr_id}}" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            Agregar control
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" aria-labelledby="page_{{fogr_id}}">
            {{#each controls}}
            <li><a class="btn btn-xs form-edicion-add-control" role="button" data-id="page_{{../fogr_id}}" data-type="{{type}}">{{label}}</a></li>
            {{/each}}
          </ul>
        </li>

        <li role="presentation"><a class="btn btn-xs form-edicion-remove" role="button" data-id="page_{{fogr_id}}">Eliminar</a></li>

        <li role="presentation"><a class="btn btn-xs form-edicion-properties" role="button" data-id="page_{{fogr_id}}">Propiedades</a></li>
      </ul>
    </div>
  </div>
  

</div>
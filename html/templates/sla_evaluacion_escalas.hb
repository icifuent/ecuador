
<div class="row siom-section-header">
	<div class="col-md-5"><h5>Ponderación de Notas</h5></div>
</div>

<div class="col-md-10">
	<table id="tabla_ponderacion" data-datos="{{JSON pesos_notas}}" class="table table-bordered" style="font-size: 90%;margin-left: 15px;" >
		<thead>
			<tr>
				<th>ITEM</th>
				<th>SUB ITEM</th>
				<th>Peso específico en ITEM</th>                
				<th>Peso específico FINAL</th>                
			</tr>               
		</thead>
		<tbody id="tabla_ponderacion_body"></tbody>
	</table>
</div>

<div class="row siom-section-header">
	<div class="col-md-5"><h5>Rangos de Notas</h5></div>
	<table id="tabla_rangos" data-datos="{{JSON escala_notas}}" class="table table-bordered" style="font-size:80%;margin-left: 15px;" >
	</table>	
</div>

<div class="row siom-section-header">
	<div class="col-md-5"><h5>Rangos de Calificaciones</h5></div>
</div>

<div class="col-md-5">
	<table id="tabla_calificaciones" data-datos="{{JSON calificaciones}}" class="table table-bordered" style="font-size: 90%;text-align: center;">
		<thead>
			<tr>
				<th>NOTA</th>
				<th>CALIFICACION</th>
			</tr>
		</thead>
		<tbody id="tabla_calificaciones_body"></tbody>
	</table>
</div>

<script type="text/javascript" chartset="utf-8" src="js/sla_evaluacion_escalas.js"></script>   

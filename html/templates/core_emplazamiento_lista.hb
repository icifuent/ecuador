<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
  	{{#data}}
  	<div class="os-crear" id="{{empl_id}}">
	  <div class="estado text-center">
	  	<span class="glyphicon glyphicon-stop status status-{{estadoColorStatus empl_estado}}" aria-hidden="true"></span>
	 </div>

	 <div class="acciones text-right">
         <button type="button" class="btn btn-default btn-xs editarEmpl"  data-id="{{empl_id}}" data-empr_id="{{empr_id}}" data-data="{{JSON .}}" id="BotonEmplazamientoEditar">
         <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
       </button>

       {{#is empl_estado 'ACTIVO'}}
       <button type="button" id="BotonEmplazamientoCambiarEstado" class="btn btn-default btn-xs"  data-id="{{empl_id}}" data-nombre="{{empl_nombre}}" data-estado="{{empl_estado}}" >
         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
       </button>
       {{else}}
       <button type="button" id="BotonEmplazamientoCambiarEstado" class="btn btn-default btn-xs"  data-id="{{empl_id}}" data-nombre="{{empl_nombre}}" data-estado="{{empl_estado}}" >
         <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
       </button>
       {{/is}}
	  </div>

	  <div class="info text-left">
	  	<h4>{{empl_nombre}} <small><small>{{empl_nemonico}}</small></small></h4>
	  	<div class="info_adicional">Nombre: <strong>{{empl_nombre}}</strong>, Contratos: <strong>{{cont_nombre}}</strong></div>
	  </div>

	  <hr>
	</div>

	{{/data}}
  </div>

   <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	<div class="pagination-info">
	    	<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
	    </div>
	  </div>
	  <div class="col-md-8 text-right">
	    <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>


</div>

<script src="js/core_emplazamiento_lista.js" type="text/javascript" charset="utf-8"></script>

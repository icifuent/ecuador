<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Ingreso de informe vía Web</h5>
  </div>
  <div class="col-md-7">
    
  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Visita</legend>
  	{{{loadTemplate "form_formulario" data}}}
</fieldset>
{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js"  type="text/javascript" charset="utf-8"></script>
<script src="js/os_visita.js" type="text/javascript" charset="utf-8"></script>
<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Crear MPS</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>


<div id="wizard" data-initialize="wizard" class="wizard complete">
  <ul class="steps">
    <li data-step="1" class="active">
      <span class="badge">1</span>Seleccionar emplazamiento
      <span class="chevron"></span>
    </li>
    <li data-step="2">
      <span class="badge">2</span>Información de MPS
      <span class="chevron"></span>
    </li>
  </ul>
  <div class="actions">
  </div>
  <div class="step-content">
    <div class="step-pane sample-pane active" data-step="1">
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <form class="form-horizontal siom-form-tiny" role="form" action="#/mnt/crear/filtro" method="POST" id="siom-form-mnt-crear-filtro">

                <div class="form-group">
                  <div class="col-sm-5">
                    <button type="button" class="btn btn-primary btn-default" id="LimpiarMntCrear">
                      <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
                  </div>
                  <div class="col-sm-7">
                    <button type="button" class="btn btn-primary btn-default" id="BuscarMntCrear">
                      <span class="glyphicon" aria-hidden="true"></span> Buscar
                  </div>
                </div>

                <hr>

                <div class="form-group">
                  <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                  <div class="col-sm-7">
                    <input autocomplete="off" type="text" class="form-control" id="nombre" name="empl_nombre">
                  </div>
                </div>

                <div class="form-group">
                  <label for="nemonico" class="col-sm-5 control-label">Nemónico</label>
                  <div class="col-sm-7">
                    <input autocomplete="off" type="text" class="form-control" id="nemonico" name="empl_nemonico">
                  </div>
                </div>

                <hr>

                <div class="form-group">
                  <label for="tecnoclogia" class="col-sm-5 control-label">Tecnología</label>
                  <div class="col-sm-7">
                    <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas' multiple name="tecn_id">
                      {{#data.tecnologias}}
                      <option value="{{tecn_id}}">{{tecn_nombre}}</option>
                      {{/data.tecnologias}}
                    </select>
                  </div>
                </div>

                <hr>

                <div class="form-group">
                  <label for="direccion" class="col-sm-5 control-label">Dirección</label>
                  <div class="col-sm-7">
                    <input autocomplete="off" type="text" class="form-control" id="empl_direccion" name="empl_direccion">
                  </div>
                </div>

                <div class="form-group">
                  <label for="zona" class="col-sm-5 control-label">Zona</label>
                  <div class="col-sm-7">
                    <select name="zona_id" class="selectpicker" data-width="100%" title='Todas'>
                      <option value="">Todas</option>
                      {{#data.zonas}}
                      <option value="{{zona_id}}">{{zona_nombre}}</option>
                      {{/data.zonas}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="region" class="col-sm-5 control-label">Región</label>
                  <div class="col-sm-7">
                    <select name="regi_id" class="selectpicker" data-width="100%" title='Todas' data-container="body">
                      <option value="">Todas</option>
                      {{#data.regiones}}
                      <option value="{{regi_id}}">{{regi_nombre}}</option>
                      {{/data.regiones}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="cluster" class="col-sm-5 control-label">Cluster</label>
                  <div class="col-sm-7">
                    <select name="clus_id" class="selectpicker" data-width="100%" title='Todos'>
                      <option value="">Todos</option>
                      {{#data.clusters}}
                      <option value="{{zona_id}}">{{zona_nombre}}</option>
                      {{/data.clusters}}
                    </select>
                  </div>
                </div>
                <!--
                <div class="text-center">
                  <button type="submit" class="btn btn-primary" data-loading-text="Filtrando..." >Filtrar</button>
                </div>
                -->
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-9" id="mnt-crear-lista">
        </div>

      </div>
    </div>
    <div class="step-pane sample-pane" data-step="2">
      <fieldset class="siom-fieldset" id="siom-emplazamiento-info">
        <legend>Emplazamiento</legend>
        <div id="siom-empl-info"></div>
      </fieldset>

      <fieldset class="siom-fieldset" style="margin-top:10px;">
        <legend>Mantenimiento preventivo a solicitud</legend>
        <form class="form-horizontal siom-form-tiny" role="form" action="#/mnt/crear" method="POST" id="form_crear">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="fecha_solicitud" class="col-sm-4 control-label">Fecha programada</label>
                <input  type="hidden" name="mant_fecha_programada" value="">
                <div class="col-sm-8">
                  <div class="row">
                    <div class="col-md-4">
                      <input autocomplete="off" id="fecha_solicitud" class="form-control" size="14" type="text" data-date-autoclose="true" name="mant_fecha_programada_fecha"
                        placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />

                    </div>
                    <div class="col-md-4">
                      <div class="input-group bootstrap-timepicker">
                        <input autocomplete="off" type="text" class="form-control timepicker" aria-describedby="addon" data-field="time" id="mant_fecha_solicitud_hora"
                          name="mant_fecha_solicitud_hora" data-modal-backdrop="true" placeholder="hh:mm" value="{{now '%H:%M'}}">
                        <span class="input-group-addon" id="addon">
                          <i class="glyphicon glyphicon-time"></i>
                        </span>
                      </div>
                    </div>
                    <div class="col-md-4 text-left">
                      <div class="checkbox">
                        <label>
                          <input  type="checkbox" value="" name="mant_fecha_solicitud_ahora" id="mant_fecha_solicitud_ahora">Ahora</label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="empresa" class="col-sm-4 control-label">Empresa</label>
                <div class="col-sm-8">
                  <select id="empresa" class="selectpicker" data-width="100%" name="empr_id" title="Seleccione empresa" data-container="body">
                    {{#data.empresa}}
                    <option value="{{empr_id}}">{{empr_nombre}}</option>
                    {{/data.empresa}}
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="especialidad" class="col-sm-4 control-label">Especialidad</label>
                <div class="col-sm-8">
                  <select id="especialidad" class="selectpicker" data-width="100%" name="espe_id" title="Seleccione especialidad" data-container="body">
                    {{#data.especialidad}}
                    <option value="{{espe_id}}">{{espe_nombre}}</option>
                    {{/data.especialidad}}
                  </select>
                </div>
              </div>


              <div class="form-group">
                <label for="formulario" class="col-sm-4 control-label">Formulario</label>
                <div class="col-sm-8">
                  <select id="formulario" class="selectpicker" data-width="100%" name="form_id" title="Seleccione formulario" multiple data-container="body"
                    data-hide-disabled="true">
                    {{#data.formulario}}
                    <option value="{{form_id}}" data-espe-id="{{espe_id}}" data-refe-seleccion="{{refe_preseleccion}}">{{form_nombre}}</option>
                    {{/data.formulario}}
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
                <div class="col-sm-8">
                  <textarea class="form-control" name="mant_descripcion" rows="5"></textarea>
                  <div id="mant_descripcion_palabras" class="siom-info text-right text-danger">
                    <span>0</span> palabras de {{data.max_words}}</div>
                </div>
              </div>

            </div>
            <div class="col-md-6">
              <label class="control-label">Documentación</label>
              <br>
              <span class="btn btn-default btn-sm btn-file">
                Agregar archivo(s)
                <input autocomplete="off" type="file" name="archivos[]" id="archivo-0" data-id="0">
              </span>
              <ul id="listado-archivos" class="list-group siom-file-list">

              </ul>
            </div>
          </div>

          <hr>
          <div class="row text-center">
            <div class="col-md-12">
              <input  type="hidden" name="empl_id" id="empl_id">
              <a href="#/mnt/bandeja" class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="submit_crear">Crear MPS</button>
            </div>
          </div>
        </form>
      </fieldset>

    </div>
  </div>
</div>


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mnt_crear.js" type="text/javascript" charset="utf-8"></script>
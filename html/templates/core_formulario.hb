{{#if data.status}}
<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Formularios</h5>
    </div>
    <div class="col-md-7">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a id="BotonFormAgregar" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Agregar formulario
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="BotonFormAgregar">
                    {{#each contratos}}
                    <li>
                        <a class="btn btn-xs add-form" role="button" data-cont-id="{{cont_id}}">{{cont_nombre}}</a>
                    </li>
                    {{/each}}
                </ul>
            </li>
        </ul>
        {{!--
        <a class="btn btn-primary btn-sm" id="BotonFormAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar
        </a>
        --}}
    </div>
</div>

<div id="Wizardformulario" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" id="Primero" class="active">
            <span class="badge">1</span>Formularios
            <span class="chevron"></span>
        </li>
        <li data-step="2" id="Segundo">
            <span class="badge">2</span>Información
            <span class="chevron"></span>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/formulario/filtro" method="POST" id="FormFormularioLista">
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-7">
                                        <input autocomplete="off" type="text" class="form-control" name="form_nombre" value="{{data.form_nombre}}">
                                    </div>
                                </div>
                                <!-- div class="form-group">
                                    <label class="col-sm-5 control-label">Contrato</label>
                                    <div class="col-sm-7">
                                        <select class="selectpicker" data-width="100%" name="cont_id">
											<option value="">Todos</option>
                                            {{#contratos}}
                                            <option value="{{cont_id}}">{{cont_nombre}}</option>
                                            {{/contratos}}
                                        </select>
                                    </div>
                                </div -->

                                <div class="form-group">
                                    <label for="periodos" class="col-sm-5 control-label">Estado</label>
                                    <div class="col-sm-7">
                                        <select id="estado" class="selectpicker" data-width="100%" name="form_estado">
                                            <option value="">Todos</option>
                                            <option value="ACTIVO">ACTIVO</option>
                                            <option value="NOACTIVO">NOACTIVO</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-formulario-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">
            <div id="core-formulario-form">Cargando...</div>
        </div>
    </div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small>
    {{/if}}
</div>
{{/if}}
<style>
    .elementos.over {
        border: 2px dashed #000;
    }
</style>

<script src="js/bootstrap-wizard.js?v={{version}}" type="text/javascript" charset="utf-8"></script>
<script src="js/core_formulario.js?v={{version}}" type="text/javascript" charset="utf-8"></script>
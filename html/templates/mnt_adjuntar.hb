<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Adjuntar archivo(s) a mantención</h5>
  </div>
  <div class="col-md-7">
    
  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Mantenimiento</legend>
  {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
</fieldset>


<form class="form-horizontal siom-form-tiny" role="form" action="#/mnt/adjuntar/{{data.mantenimiento.mant_id}}" method="POST" id="form_adjuntar">

  <fieldset class="siom-fieldset">
      <legend>Adjuntar archivo(s)</legend>
      
      <span class="btn btn-default btn-sm btn-file">
      Agregar archivo(s) 
          <input type="file" name="archivos[]" id="archivo-0" data-id="0">
      </span>
      <small class="text-muted"> Máximo <b>{{data.max_file_upload}}</b> por archivo</small>
      
      <ul id="listado-archivos" class="list-group siom-file-list">
        
      </ul>
  </fieldset>

  <hr>
  <div class="row text-center">
    <div class="col-md-12">
      <a href="#/mnt/bandeja" class="btn btn-default">Cancelar</a>
      <button type="submit" class="btn btn-primary" >Guardar</button>
    </div>
  </div>

</form>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/mnt_adjuntar.js" type="text/javascript" charset="utf-8"></script>

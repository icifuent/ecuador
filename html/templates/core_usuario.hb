<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Editar Usuarios</h5>
    </div>
    {{#data}}
    <div class="col-md-7 text-right" id="AgregarUsuario">
        <a class="btn btn-primary btn-sm" href="#/core/usuario" id="BotonUsuarioAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-data="{{JSON .}}"></span> Agregar
        </a>
    </div>
    {{/data}}
</div>

{{#if data.status}}
<div id="WizardUsuario" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" class="active" id="Primero"><span class="badge">1</span>Listado de usuarios<span class="chevron"></span></li>
        <li data-step="2" id="Segundo"><span class="badge">2</span>Información<span class="chevron"></span></li>
        <li data-step="3" id="Tercero"><span class="badge">3</span>Contratos y Zonas<span class="chevron"></span></li>
        <li data-step="4" id="Cuarto" ><span class="badge">4</span>Notificaciones<span class="chevron"></span></li>
        <li data-step="5" id="Quinto" ><span class="badge">5</span>IMEI<span class="chevron"></span></li>
		<li data-step="6" id="Sexto"  ><span class="badge">6</span>Cursos<span class="chevron"></span></li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/usuario/filtro" method="POST" id="FormUsuarioLista">
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-7">
                                        <input autocomplete="off" type="text" class="form-control" name="usua_nombre" value="{{data.usua_nombre}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="periodos" class="col-sm-5 control-label">Estado</label>
                                    <div class="col-sm-7">
                                        <select id="estado" class="selectpicker" data-width="100%" name="usua_estado">
                                            <option value="">Todos</option>
                                            {{#data.estados}}
												<option value="{{.}}">{{.}}</option>
                                            {{/data.estados}}
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-usuario-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">
            <div class="panel panel-default">
                <div class="col-md-12 text-right">
                    <div class="acc-wizard-step">
                        <button class="btn btn-primary" type="submit" id="BotonUsuarioContratoZonas" style="margin-top:10px">Siguiente Paso</button>
                    </div>
                </div>
                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/usuario/" method="POST" id="FormUsuarioEditar">
                    <input autocomplete="off" type="hidden" id="usua_id" name="usua_id">
                    <input autocomplete="off" type="hidden" id="usua_creador" name="usua_creador">

                    <div class="col-md-8">
                        <fieldset class="siom-fieldset" id="siom-usuario-info">
                            <legend>Usuario</legend>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Empresa</label>
                                            <div class="col-sm-8">

                                                <div id="editar" style="display: none;">
                                                    <input autocomplete="off" type="text" id="empr_nombre" name="empr_nombre" class="form-control" placeholder="Empresa" value="" disabled>
                                                </div>

                                                <div id="agregar">
												    <select id="empr_id" name="empr_id" class="selectpicker" data-width="100%" title='Seleccione Empresa' data-container="body">
    												{{#data.empresas}}
    													<option name="empr_id" value="{{empr_id}}">{{empr_nombre}}</option>
    												{{/data.empresas}}
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="usua_nombre" class="col-sm-3 control-label">Nombre</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_nombre" name="usua_nombre" class="form-control" placeholder="Nombre" value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="usua_alias" class="col-sm-3 control-label">Alias</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_alias" name="usua_alias" class="form-control" placeholder="Alias" value="">
                                            </div>
                                        </div>
                                    </td>

									<td colspan="2">
                                        <div class="form-group">
                                            <label for="usua_rut" class="col-sm-3 control-label">RUT</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_rut" name="usua_rut" class="form-control" placeholder="RUT" value="">
                                            </div>
                                        </div>
                                    </td>

                                </tr>

                                <tr>

									<td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_alias" class="col-sm-3 control-label">Login</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_login" name="usua_login" class="form-control" id="usua_login" placeholder="Login" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_alias" class="col-sm-3 control-label">Password</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="password" id="usua_password" name="usua_password" class="form-control" id="usua_password" placeholder="Password" value="">
                                            </div>
                                        </div>
                                    </td>

                                </tr>


								<tr>

									<td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_giro" class="col-sm-3 control-label">Cargo</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_cargo" name="usua_cargo" class="form-control" placeholder="Cargo" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_direccion" class="col-sm-3 control-label">Correo Electrónico</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_correo_electronico" name="usua_correo_electronico" class="form-control" placeholder="Correo Electrónico" value="">
                                            </div>
                                        </div>
                                    </td>


                                </tr>



                                <tr>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_direccion" class="col-sm-3 control-label">Teléfono Fijo</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_telefono_fijo" name="usua_telefono_fijo" class="form-control" placeholder="Teléfono Fijo" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_direccion" class="col-sm-3 control-label">Teléfono Móvil</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_telefono_movil" name="usua_telefono_movil" class="form-control" placeholder="Teléfono Móvil" value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>

									<td colspan="2">
                                        <div class="form-group">
                                            <label for="empr_direccion" class="col-sm-3 control-label">Dirección</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="usua_direccion" name="usua_direccion" class="form-control" placeholder="Dirección" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="usua_estado" class="col-sm-3 control-label">Estado</label>
                                            <div class="col-sm-8">
                                                <select id="usua_estado" name="usua_estado" class="selectpicker" data-width="100%" data-container="body">
                                                    {{#data.estados}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.estados}}
                                                </select>

                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                    </div>

                    <div class="col-md-4">
                        <fieldset class="siom-fieldset">
                            <legend>Perfiles</legend>
                            <div class="form-group">
                                <div id="dual-list-box-employees">
                                    <label for="empr_direccion" class="col-sm-2 control-label">Perfil</label>
                                    <div class="col-sm-5">
                                        <select id="UsuarioPerfil" name="perf_id[]" class="selectpicker" data-width="370px" title='Sin Perfil' multiple="multiple"  data-container="body">
										{{#data.perfiles}}
                                            <option name="perf_id[]" value="{{perf_id}}">{{perf_nombre}}</option>
                                        {{/data.perfiles}}
										</select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <!-- Selfie Tecnico BWBB INICIO-->
                    <div id="div_selfie_tecnico" class="col-md-4" style="display: none;">
                        <fieldset class="siom-fieldset">
                            <legend>Foto </legend>
                            <div class="form-group">
                                <div id="dual-list-box-employees">
                                    <label for="empr_direccion" class="col-sm-2 control-label">Foto T&eacutecnico</label>
                                    <div class="col-sm-5">
                                        <select id="usua_selfie_tecnico" name="usua_selfie_tecnico" class="selectpicker" data-width="370px" title="Seleccione opci&oacuten" data-container="body">
                                            <option name="op_1" value="NO">NO</option>
                                            <option name="op_2" value="SI">SI</option>                                            
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <!-- Selfie Tecnico BWBB FIN-->
                    
                    <div class="col-md-4" id="UsuarioTipoAcceso">
                        <fieldset class="siom-fieldset">
                            <legend>Tipo Acceso</legend>
                            <div class="form-group">
                                <div id="dual-list-box-employees">
                                    <label class="col-sm-2 control-label">API</label>
                                    <div class="col-sm-5">
                                        <input autocomplete="off" type="checkbox" id="usua_acceso_api" name="usua_acceso_api" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div id="dual-list-box-employees">
                                    <label class="col-sm-2 control-label">Movil</label>
                                    <div class="col-sm-5">
                                        <input autocomplete="off" type="checkbox" id="usua_acceso_movil" name="usua_acceso_movil" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div id="dual-list-box-employees">
                                    <label class="col-sm-2 control-label">Web</label>
                                    <div class="col-sm-5">
                                        <input autocomplete="off" type="checkbox" id="usua_acceso_web" name="usua_acceso_web" value="">
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </div>

                    <div class="row text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="BotonUsuarioGuardar"
                                style="margin:10px">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

		<div class="step-pane sample-pane" data-step="3">

			<div class="col-md-12 text-right">
				<div class="acc-wizard-step"><button class="btn btn-primary" type="submit" id="BotonUsuarioNotificaciones" style="margin-top:10px">Siguiente Paso</button></div>
			</div>

            <div class="panel panel-default">
                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/usuario/edit/contrato_zonas" method="POST" id="FormUsuarioContratosZonas">
                    <fieldset class="siom-fieldset" id="FieldsetUsuarioContratos">
                        <legend>Contratos</legend>
                        <div class="row">
                            <div id="dual-list-box-employees" class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="control-label">Lista de Contratos</label>
                                        <ul class="list-group siom-asignacion-list" id="ListaContratos"></ul>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Contratos del Usuario</label>
                                        <ul class="list-group siom-asignacion-list" id="ListaContratosUsuario"></ul>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="control-label">Zonas de pertenencia Contrato: <i id="NombreContratoZonas"> </i></label>
                                            <ul class="list-group siom-asignacion-list" id="ListaContratoUsuarioZonas" style="overflow-x: hidden;"></ul>
                                        </div>
                                    </div>
                                </div>

								<!--
                                <div class="row text-center">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary" data-loading-text="Creando..." autocomplete="off" id="BotonGuardarContratosYZonas" style="margin-bottom:10px">Guardar Contratos y Zonas</button>
                                    </div>
                                </div>-->

                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

		<div class="step-pane sample-pane" data-step="4">
			<div class="col-md-12 text-right">
				<div class="acc-wizard-step">
					<button class="btn btn-primary" type="submit" id="BotonUsuarioIMEI" style="margin-top:10px">Siguiente Paso</button>
				</div>
			</div>
			<form class="form-horizontal siom-form-tiny" role="form" action="#/core/usuario/" id="FormUsuarioNotificaciones" method="POST">
				<input  type="hidden" id="cont_id" name="cont_id" value="">
				<input  type="hidden" id="usua_id" name="usua_id" value="">

				<fieldset class="siom-fieldset" id="siom-usuario-Tipo-Notificaciones">
					<legend>Notificaciones</legend>
					<div class="row">
						<div class="col-md-3">
							<label class="control-label">Contratos en los que participa Usuario</label>
							<ul class="list-group siom-asignacion-list" id="ListaContratosNotificaciones"></ul>
						</div>


						<div class="col-md-4">
							<label class="control-label">Notificaciones OS Contrato:<i class="NombreContratoNotificaciones"></i></label>
							<div class="panel panel-default" id="NotificacionesOS">
								<div class="panel-body">
									<div class="row">
										<label class="col-md-6 control-label">Evento</label>
										<label class="col-md-3 control-label">Enviar Email</label>
										<label class="col-md-3 control-label">Notificación web</label>
									</div>

									<div class="row">
										<label class="col-md-6 control-label">ASIGNADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="ASIGNADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="ASIGNADA" data-modu_alias="OS" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">CREADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="CREADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="CREADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">INFORME_AGREGADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="INFORME_AGREGADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="INFORME_AGREGADO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">INFORME_VALIDADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="INFORME_VALIDADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="INFORME_VALIDADO"  data-rcun_evento="ASIGNADA" >
									</div>

									<div class="row">
										<label class="col-md-6 control-label">OS_VALIDADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="OS_VALIDADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="OS_VALIDADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">PRESUPUESTO_AGREGADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="PRESUPUESTO_AGREGADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="PRESUPUESTO_AGREGADO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">PRESUPUESTO_VALIDADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="PRESUPUESTO_VALIDADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="PRESUPUESTO_VALIDADO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_CAMBIO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_CAMBIO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_CAMBIO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_CAMBIO_VALIDADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_CAMBIO_VALIDADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_CAMBIO_VALIDADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_INFORME</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_INFORME" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_INFORME" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_INFORME_VALIDADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_INFORME_VALIDADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_INFORME_VALIDADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">VISITA_EJECUTANDO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="VISITA_EJECUTANDO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="VISITA_EJECUTANDO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>
                                    <div class="row">
										<label class="col-md-6 control-label">SOLICITUD_ESTADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_PARADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="OS" data-rcun_evento="SOLICITUD_PARADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-4">
							<label class="control-label">Notificaciones Mantenimientos Contrato:<i class="NombreContratoNotificaciones"></i></label>
							<div class="panel panel-default" id="NotificacionesMNT">
								<div class="panel-body">
									<div class="row">
										<label class="col-md-6 control-label">Evento</label>
										<label class="col-md-3 control-label">Enviar Email</label>
										<label class="col-md-3 control-label">Notificación web</label>
									</div>

									<div class="row">
										<label class="col-md-6 control-label">ASIGNADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="ASIGNADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="ASIGNADA" data-modu_alias="MNT" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">CREADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="CREADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="CREADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">INFORME_AGREGADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="INFORME_AGREGADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="INFORME_AGREGADO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">INFORME_VALIDADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="INFORME_VALIDADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="INFORME_VALIDADO"  data-rcun_evento="ASIGNADA" >
									</div>

									<div class="row">
										<label class="col-md-6 control-label">PRESUPUESTO_AGREGADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="PRESUPUESTO_AGREGADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="PRESUPUESTO_AGREGADO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">PRESUPUESTO_VALIDADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="PRESUPUESTO_VALIDADO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="PRESUPUESTO_VALIDADO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_CAMBIO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_CAMBIO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_CAMBIO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_CAMBIO_VALIDADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_CAMBIO_VALIDADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_CAMBIO_VALIDADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_INFORME</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_INFORME" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_INFORME" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">SOLICITUD_INFORME_VALIDADA</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_INFORME_VALIDADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_INFORME_VALIDADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

									<div class="row">
										<label class="col-md-6 control-label">VISITA_EJECUTANDO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="VISITA_EJECUTANDO" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="VISITA_EJECUTANDO" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>

                                    <div class="row">
										<label class="col-md-6 control-label">SOLICITUD_ESTADO</label>
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_PARADA" data-rcun_accion="ENVIAR_EMAIL">
										<input autocomplete="off" class="col-md-3" type="checkbox" data-modu_alias="MNT" data-rcun_evento="SOLICITUD_PARADA" data-rcun_accion="ENVIAR_NOTIF_WEB">
									</div>
								</div>
							</div>
						</div>

					</div>
				</fieldset>
			</form>
		</div>

		<div class="step-pane sample-pane" data-step="5">
			<div class="col-md-12 text-right">
				<div class="acc-wizard-step">
					<button class="btn btn-primary" type="submit" id="BotonUsuarioCursos" style="margin-top:10px">Siguiente Paso</button>
				</div>
			</div>
			<form class="form-horizontal siom-form-tiny" role="form" action="#/core/usuario/" method="POST" id="FormUsuarioIMEI">
				<input autocomplete="off" type="hidden" id="usua_id" name="usua_id">
				<input autocomplete="off" type="hidden" id="reum_estado" value="ACTIVO">
				<fieldset class="siom-fieldset" id="siom-usuario-Imei">
					<legend>IMEI</legend>
					<div id="dual-list-box-employees" class="form-group">
						<div class="col-md-3">
							<div class="siom-os-lista siom-os-presupuesto-lista-items" id="ListaIMEI"></div>
						</div>
						<div class="col-md-4">
							<div class="row">
								<legend>Agregar IMEI</legend>
								<div class="form-group" style="margin-top:10px">
									<label class="col-md-2 control-label">IMEI</label>
									<div class="col-md-6">
										<input autocomplete="off" type="text" class="form-control" id="reum_imei" name="reum_imei" placeholder="IMEI" value="">
									</div>
								</div>
							</div>
							<div class="row text-center">
								<div class="col-md-8">
									<button type="submit" class="btn btn-primary" data-loading-text="Creando..." autocomplete="off" id="submit_Guardar_Imei">Guardar IMEI</button>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</form>
		</div>


		<div class="step-pane sample-pane" data-step="6">
			<form class="form-horizontal siom-form-tiny" role="form" action="#/core/usuario/" method="POST" id="FormUsuarioCursos">
				<input autocomplete="off" type="hidden" id="usua_id" name="usua_id">
				<fieldset class="siom-fieldset" id="siom-usuario-Cursos">
					<legend>Cursos</legend>

					<div class="col-md-11" id="TableUsuarioCursos">
					</div>

				</fieldset>
			</form>
		</div>
	</div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/core_usuario.js" type="text/javascript" charset="utf-8"></script>

<div class="row siom-section-header">
    <fieldset class="siom-fieldset" style="margin-left:20px; margin-right:20px;">
        <div class="row col-md-12">
            {{{loadTemplate "template_emplazamiento" data.emplazamiento}}}
        </div>

        <div class="col-md-12" style="padding-left: 0px;padding-right: 28px;">
            <fieldset class="siom-fieldset">
                <form class="form-horizontal siom-form-tiny" role="form" action="#/inve/emplazamiento/item/guardar" method="POST" id="siom-form-add-item">


                    <div class="form-group">
                        <label class="col-sm-4 control-label">Codigo</label>

                        <div class="col-sm-5">
                            <input autocomplete="off" type="text" class="form-control" id="inel_codigo" name="inel_codigo">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Nombre
                        </label>

                        <div class="col-sm-5">
                            <input  type="hidden" value="{{#data.emplazamiento}}{{empl_id}}{{/data.emplazamiento}}" name="empl_id"></input>
                            <input  type="hidden" name="inel_id" value="{{inel_id}}"></input>

                            <input autocomplete="off" type="text" class="form-control" id="inel_nombre" name="inel_nombre">
                        </div>
                    </div>




                    <div class="form-group">
                        <label class="col-sm-4 control-label">Descripción</label>

                        <div class="col-sm-5">
                            <textarea type="text" class="form-control" id="inel_descripcion" name="inel_descripcion"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-4 control-label">Ubicación
                        </label>

                        <div class="col-sm-5">
                            <input autocomplete="off" type="text" class="form-control" id="inel_ubicacion" name="inel_ubicacion">
                        </div>
                    </div>



                    <div class="form-group">

                        <label for="fecha_solicitud" class="col-sm-4 control-label">Estado
                        </label>

                        <div class="col-sm-5">
                            <select id="tecnologia" class="selectpicker" data-width="100%" title='ACTIVO' name="inel_estado">

                                <option value="ACTIVO">ACTIVO</option>
                                <option value="NOACTIVO">NO ACTIVO</option>

                            </select>
                        </div>
                    </div>
                    <fieldset class="siom-fieldset col-sm-6 col-md-offset-3">
                        <legend>Detalle</legend>

                        <div id="item" style="padding-left: 0px;"></div>

                        <a class="btn btn-info btn-xs pull-right" id="addItem">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar detalle
                        </a>

                    </fieldset>

                    <div class="row text-center">
                        <div class="col-md-12" style="padding-top: 10px;">
                            {{#data.emplazamiento}}
                            <a href="#/inve/emplazamiento/{{empl_id}}/items" class="btn btn-default">Cancelar</a>
                            {{/data.emplazamiento}}
                            <button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="submit_guardar_item">Guardar</button>
                        </div>
                    </div>
                </form>
            </fieldset>
        </div>

    </fieldset>

</div>




<script src="js/inve_crear.js" type="text/javascript" charset="utf-8"></script>
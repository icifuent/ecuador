<table width="100%">
	<tr>
		<td width="20%">
			<div class="siom-label">Nº OS</div>
			<div class="siom-value">{{empty orse_id}}</div>
		</td>
		<td width="20%">
			<div class="siom-label">TIPO</div>
			<div class="siom-value">{{empty orse_tipo}}</div>
		</td>
		<td width="20%">
			<div class="siom-label">ESPECIALIDAD</div>
			<div class="siom-value">{{empty espe_nombre}}</div>
		</td>
		<td width="20%">
			<div class="siom-label">ALARMA</div>
			<div class="siom-value">{{empty sube_nombre}}</div>
		</td>
		<td width="20%">
			<div class="siom-label">EMPRESA</div>
			<div class="siom-value">{{empty empr_nombre}}</div>
		</td>
	</tr>
	<tr>
		<td width="40%" colspan="2">
			<div class="siom-label">DESCRIPCIÓN</div>
			<div class="siom-value">{{empty orse_descripcion}}</div>
		</td>
		{{#if defa_id}}
		<td width="20%">
			<div class="siom-label">REPORTE FALLA </div>
			<div class="siom-value">{{empty defa_id}}
				<a class="siom-link" href="#/defaMovil/detalle/{{defa_id}}"> VER</a>
			</div>
		</td>
		<td width="20%">
			<div class="siom-label">CÓD. INCIDENCIA</div>
			<div class="siom-value">SIN TICKET</div>
		</td>
		{{else}}
		<td width="20%">
			<div class="siom-label">CÓD. INCIDENCIA</div>
			<div class="siom-value">{{empty orse_tag}}</div>
		</td>
		{{/if}}
		<td width="20%">
			<div class="siom-label">FECHA PROGRAMADA</div>
			<div class="siom-value">{{empty orse_fecha_solicitud}}</div>
		</td>
	</tr>
	<tr>
		<td width="20%">
			<div class="siom-label">USUARIO CREADOR</div>
			<div class="siom-value">{{empty usua_creador_nombre}}</div>
		</td>

		<td width="20%">
			<div class="siom-label">FECHA CREACIÓN</div>
			<div class="siom-value">{{empty orse_fecha_creacion}}</div>
		</td>

		<td width="20%">
			<div class="siom-label">USUARIO VALIDADOR</div>
			<div class="siom-value">{{empty usua_validador_nombre}}</div>
		</td>

		<td width="20%">
			<div class="siom-label">FECHA VALIDACION</div>
			<div class="siom-value">{{empty orse_fecha_validacion}}</div>
		</td>

		<td width="20%">
			<div class="siom-label">ESTADO</div>
			<div class="siom-value">{{empty orse_estado}}</div>
		</td>
	</tr>
	{{#if archivos}}
	<tr>
		<td width="100%" colspan="5" align="left" valign="top">
			<div class="siom-label">DOCUMENTACIÓN</div>
			<div class="siom-docu-link">
				{{#each archivos}}
				<a href="{{repo_ruta}}" target="blank">{{repo_nombre}}</a>
				<small> | {{repo_descripcion}}</small>
				<br> {{/each}}
			</div>
		</td>
	</tr>
	{{/if}}

</table>
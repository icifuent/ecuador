{{#if data.status}}
<div class="row">
      <div class="col-sm-9">

      {{{loadTemplate "template_emplazamiento" data.emplazamiento}}}
          
      {{{loadTemplate "template_inspeccion" data.inspeccion}}}

      <fieldset class="siom-fieldset"  style="margin-top:20px;">
        <legend>Asignaciones</legend>
        {{#each data.asignacion}}
        <table width="100%">
          <tr>
              <td width="20%">
                <div class="siom-label">FECHA ASIGNACIÓN</div>
                <div class="siom-value">{{inas_fecha_asignacion}}</div>
              </td>
              <td width="30%">
                <div class="siom-label">ASIGNADA POR</div>
                <div class="siom-value">{{usua_creador}}</div>
              </td>
              <td width="30%">
                <div class="siom-label">ASIGNADA A</div>
                <div class="siom-value">{{usua_asignado}}</div>
              </td>
              <td width="20%">
                <div class="siom-label">ESTADO</div>
                <div class="siom-value">{{inas_estado}}</div>
              </td>
          </tr>
        </table>
        {{else}}
        <p class="siom-no-data">Sin asignaciones</p>
        {{/each}}
      </fieldset>

      {{{loadTemplate "template_visitas" data.visitas}}}

      {{{loadTemplate "template_informes" data.informes}}}

      </div>
      <div class="col-sm-3">
          {{{loadTemplate "template_mapa" data.emplazamiento }}}
     
          {{{loadTemplate "template_eventos" data.eventos }}}
      </div>
</div>

{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
  </div>
{{/if}}



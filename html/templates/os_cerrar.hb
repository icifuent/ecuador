<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Cerrar orden de servicio</h5>
  </div>
  <div class="col-md-7">
    
  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Presupuesto</legend>
  {{#each data.presupuesto}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
    <label class="col-sm-2 siom-value">{{pres_fecha_creacion}}</label>
    <label class="col-sm-2 siom-label">CREADO POR</label>
    <label class="col-sm-3 siom-value">{{usua_creador}}</label>
    <label class="col-sm-1 siom-label">ESTADO</label>
    <label class="col-sm-1 siom-value">{{pres_estado}}</label>
    <label class="col-sm-1 siom-link">
      <a href="#/os/presupuesto/{{../data.os.orse_id}}/validar/{{pres_id}}" target="blank"> REVISAR </a>
    </label>
  </div>

  {{#isnt pres_estado 'SINVALIDAR'}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
    <label class="col-sm-2 siom-value">{{pres_fecha_validacion}}</label>
    <label class="col-sm-2 siom-label">VALIDADO POR</label>
    <label class="col-sm-2 siom-value">{{usua_validador}}</label>
  </div>
  {{/isnt}}
  
  {{#gt ../data.presupuesto.length 1}}
  <hr>
  {{/gt}}
  {{/each}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Informe</legend>
  {{#each data.informe}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
    <label class="col-sm-2 siom-value">{{info_fecha_creacion}}</label>
    <label class="col-sm-2 siom-label">CREADO POR</label>
    <label class="col-sm-3 siom-value">{{usua_creador}}</label>
    <label class="col-sm-1 siom-label">ESTADO</label>
    <label class="col-sm-1 siom-value">{{info_estado}}</label>
    <label class="col-sm-1 siom-link">
      <a href="#/os/informe/{{../data.os.orse_id}}/validar/{{info_id}}" target="blank"> REVISAR </a>
    </label>
  </div>

  {{#isnt info_estado 'SINVALIDAR'}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
    <label class="col-sm-2 siom-value">{{info_fecha_validacion}}</label>
    <label class="col-sm-2 siom-label">VALIDADO POR</label>
    <label class="col-sm-2 siom-value">{{usua_validador}}</label>
  </div>
  {{/isnt}}
  
  {{#gt ../data.informe.length 1}}
  <hr>
  {{/gt}}
  {{/each}}
  
</fieldset>

<hr>

<form class="form form-horizontal" role="form" action="#/os/cerrar/{{data.os.orse_id}}" method="POST" id="siom-cerrar-form">
  <div class="row text-center">
    <div class="col-md-10 col-md-offset-1">
        Usted podrá rechazar Informe o Presupuesto pre aprobados, de forma independiente.<br>
        El rechazo de algunos de ellos mantendrá la Orden de Servicio abierta hasta la aprobación o rechazo definitivos.<br><br>
    </div>
  </div>

  <div class="row text-center">
    <div class="col-md-6 col-md-offset-3">
        <textarea name="orse_comentario" class="form-control" rows="3" placeholder="Comentario"></textarea>
        <br>
    </div>
  </div>

  <div class="row text-center">
    <div class="col-md-12">
      <input type="hidden" name="orse_estado" value=""/>
      {{!--
      <button class="btn btn-default" data-orse-estado="CANCELADA">Cancelar</button>
      --}}
      <a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
      <button class="btn btn-danger" data-orse-estado="RECHAZADA">Rechazar</button>
      <button class="btn btn-success" data-orse-estado="APROBADA">Aprobar</button>
    </div>
  </div>

</form>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/os_cerrar.js" type="text/javascript" charset="utf-8"></script>

<div class="row">
	<div class="col-sm-12">
		<form class="form-horizontal" role="form" id="EditModalForm">
			<input  type="hidden" id="{{empl_id}}">
			<input  type="hidden" id="{{usua_creador}}">
			<div class="form-group">
				<label for="empl_fecha_creacion" class="col-sm-2 siom-label">Fecha creación</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_fecha_creacion" name="empl_fecha_creacion" value="{{empl_fecha_creacion}}"
				 readonly>
			</div>

			<div class="form-group">
				<label for="empl_nemonico" class="col-sm-2 siom-label">Nemonico</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_nemonico" placeholder="Nemonico" value="{{empl_nemonico}}">
			</div>

			<div class="form-group">
				<label for="empl_nombre" class="col-sm-2 siom-label">Nombre</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_nombre" placeholder="Nombre" value="{{empl_nombre}}">
			</div>

			<div class="form-group">
				<label for="empl_direccion" class="col-sm-2 siom-label">Dirección</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_direccion" placeholder="Dirección" value="{{empl_direccion}}">
			</div>

			<div class="form-group">
				<label for="comu_id" class="col-sm-2 siom-label">Comuna</label>
				<select id="comu_id" class="selectpicker" title='Seleccione una comuna...'>
					{{#data.comunas}}
					<option value="{{comu_id}}">{{comu_nombre}}</option>
					{{/data.comunas}}
				</select>
			</div>

			<div class="form-group">
				<label for="empl_referencia" class="col-sm-2 siom-label">Referencia</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_referencia" placeholder="Referencia" value="{{empl_referencia}}">
			</div>

			<div class="form-group">
				<label for="empl_tipo" class="col-sm-2 siom-label">Tipo</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_tipo" placeholder="Tipo" value="{{empl_tipo}}">
			</div>

			<div class="form-group">
				<label for="empl_tipo_acceso" class="col-sm-2 siom-label">Tipo Acceso</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_tipo_acceso" placeholder="Tipo Acceso" value="{{empl_tipo_acceso}}">
			</div>

			<div class="form-group">
				<label for="empl_nivel_criticidad" class="col-sm-2 siom-label">Nivel Criticidad</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_nivel_criticidad" placeholder="Nivel Criticidad"
				 value="{{empl_nivel_criticidad}}">
			</div>

			<div class="form-group">
				<label for="empl_espacio" class="col-sm-2 siom-label">Espacio</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_espacio" placeholder="Espacio" value="{{empl_espacio}}">
			</div>

			<div class="form-group">
				<label for="empl_macrositio" class="col-sm-2 siom-label">Macrositio</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_macrositio" placeholder="Macrositio" value="{{empl_macrositio}}">
			</div>

			<div class="form-group">
				<label for="empl_subtel" class="col-sm-2 siom-label">Subtel</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_subtel" placeholder="Subtel" value="{{empl_subtel}}">
			</div>

			<div class="form-group">
				<label for="empl_distancia" class="col-sm-2 siom-label">Distancia</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_distancia" placeholder="Distancia" value="{{empl_distancia}}">
			</div>

			<div class="form-group">
				<label for="empl_latitud" class="col-sm-2 siom-label">Latitud</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_latitud" placeholder="Latitud" value="{{empl_latitud}}">
			</div>

			<div class="form-group">
				<label for="empl_longitud" class="col-sm-2 siom-label">Longitud</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_longitud" placeholder="Longitud" value="{{empl_longitud}}">
			</div>

			<div class="form-group">
				<label for="empl_observacion" class="col-sm-2 siom-label">Observación</label>
				<textarea class="col-sm-2 siom-value" id="empl_observacion" placeholder="Observación"></textarea>
			</div>

			<div class="form-group">
				<label for="empl_id_emplazamiento_atix" class="col-sm-2 siom-label">Emplazamiento Atix</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_id_emplazamiento_atix" placeholder="Emplazamiento Atix"
				 value="{{empl_id_emplazamiento_atix}}">
			</div>

			<div class="form-group">
				<label for="empl_observacion_ingreso" class="col-sm-2 siom-label">Observación Ingreso</label>
				<input autocomplete="off" type="text" class="col-sm-2 siom-value" id="empl_observacion_ingreso" placeholder="Observacion Ingreso"
				 value="{{empl_observacion_ingreso}}">
			</div>

			<div class="form-group">
				<label for="empl_estado" class="col-sm-2 siom-label">Estado</label>
				<select id="empl_estado" class="selectpicker" title='Seleccione un estado...'>
					<option value="ACTIVO">ACTIVO</option>
					<option value="NOACTIVO">NO ACTIVO</option>
				</select>
			</div>

			<hr>
			<div class="row text-center">
				<div class="col-md-12">
					<a href="#/core/emplazamiento" class="btn btn-default" id="submit_Cancelar">Cancelar</a>
					<button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="submit_Crear">Crear Emplazamiento</button>
				</div>
			</div>


		</form>
	</div>
</div>
<!--    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="SaveBtn" data-loading-text="Guardando..." >Guardar</button>
      </div> -->
<script src="js/core_emplazamiento_crear.js" type="text/javascript" charset="utf-8"></script>
{{#if forms}}
<div id="wizard" class="wizard complete" data-initialize="wizard" data-restrict="previous" data-step="2">

	<ul class="steps">
		{{#each forms}}
		<li data-step="{{add @index 1}}" {{#if @first}}class="active" {{/if}}>
			<span class="badge">{{add @index 1}}</span>{{form_name}}
			<span class="chevron"></span>
		</li>
		{{/each}}
	</ul>
	<!--
		<div class="actions">
		    <button type="button" class="btn btn-default btn-prev"><span class="glyphicon glyphicon-arrow-left"></span>Previo</button>
		    <button type="button" class="btn btn-default btn-next" data-last="Complete">Siguiente<span class="glyphicon glyphicon-arrow-right"></span></button>
		</div>
		-->
	<div class="step-content">
		{{#each forms}}
		<div class="step-pane sample-pane siom-visita-pane {{#if @first}}active{{/if}}" data-step="{{add @index 1}}">

			<form class="form-horizontal siom-form-tiny siom-form" role="form" method="POST" data-form-id="{{form_id}}" data-form-name="{{form_name}}"
			 data-form="{{JSON .}}" data-last="{{@last}}" data-route="{{../route}}">
				<input  type="hidden" name="form_id" value="{{form_id}}" />
				<table width="700px" align="center">
					<tr>
						<td width="30%" align="left" valign="top" class="siom-form-label">
							FECHA VISITA
						</td>
						<td width="70%" align="left" valign="top" class="siom-form-control">

							<div class="row">
								<div class="col-md-6">

									<input autocomplete="off" id="fecha_solicitud" class="form-control" size="14" type="text" data-date-autoclose="true" name="fecha_visita"
									 placeholder="dd/mm/yyyy" value="{{now '%d/%m/%Y'}}" />

								</div>
								<div class="col-md-6">

									<div class="input-group bootstrap-timepicker">
										<input autocomplete="off" type="text" class="form-control timepicker" aria-describedby="addon" data-field="time" name="hora_visita"
										 data-modal-backdrop="true" placeholder="hh:mm" value="{{now '%H:%M'}}">
										<span class="input-group-addon" id="addon">
											<i class="glyphicon glyphicon-time"></i>
										</span>
									</div>
								</div>
							</div>

						</td>
					</tr>
					<tr>
						<td width="100%" height="20px" colspan="2">
						</td>
					</tr>

					{{#each pages}} {{#gt ../pages.length 1}}
					<tr>
						<td colspan="2">
							<div class="siom-form-subtitle">{{{fopa_name}}}</div>
						</td>
					</tr>
					{{/gt}} {{#each controls}} {{#is foco_type "SAVE"}}
					<tr>
						<td width="100%" colspan="2" align="center">
							<hr>
							<input  type="hidden" name="{{foco_id}}" value="" /> {{#if foco_options.cancel_label}}
							<button type="submit" class="btn btn-default btn-xs" data-foco-id="{{foco_id}}" value="{{ifnull foco_options.cancel_action 'cancel'}}">{{foco_options.cancel_label}}</button>
							{{/if}} {{#if foco_options.save_label}}
							<button type="submit" class="btn btn-primary btn-xs save" id="save" data-validating-text="Validando..." data-loading-text="Guardando..."
							  data-foco-id="{{foco_id}}" value="{{ifnull foco_options.save_action 'save'}}">{{foco_options.save_label}}</button>
							{{/if}}
						</td>
					</tr>

					{{else}} {{#is foco_type "AGGREGATOR"}}
					<tr class="siom-form-aggregated">
						<td width="100%" colspan="2" align="right" class="siom-form-aggregator">
							<button type="button" class="btn btn-info btn-xs aggregator" name="{{foco_id}}" data-foco-id="{{foco_id}}" data-options="{{JSON foco_options}}">{{foco_label}}</button>
						</td>
					</tr>
					<tr class="siom-form-aggregated">
						<td width="100%" colspan="2">
							<ul id="listado-aggregator-{{foco_id}}" class="list-group siom-aggregator-list ctrl-aggregator">
							</ul>
						</td>
					</tr>
					{{else}}
					<tr {{#if foco_options.aggregated}} class="siom-form-aggregated" {{/if}}>

						<td width="40%" align="left" valign="top" class="siom-form-label">
							{{foco_label}} {{#isnt foco_required '0'}} (*){{/isnt}}
						</td>

						<td width="60%" align="left" valign="top" class="siom-form-control">
							{{#is foco_type "CAMERA"}}
							<span class="btn btn-default btn-sm btn-file">
								Agregar archivo(s)
								<input autocomplete="off" type="file" accept="image/*" name="{{foco_id}}[]" id="archivo-{{foco_id}}-0" data-id="0" data-foco-id="{{foco_id}}"
								 data-label="{{foco_label}}" data-type="{{foco_type}}" data-required="{{foco_required}}" data-list="listado-archivos-{{foco_id}}"
								 data-preview="true">
							</span>
							<ul id="listado-archivos-{{foco_id}}" class="list-group siom-file-list ctrl-camera"></ul>
							{{/is}} {{#is foco_type "SELECT"}}
							<select class="selectpicker" name="{{foco_id}}" data-label="{{foco_label}}" data-type="{{foco_type}}" data-required="{{foco_required}}"
							 title="Sin selección" data-width="100%">
								{{#each foco_options.data}}
								<option value="{{.}}">{{.}}</option>
								{{/each}}
							</select>
							{{/is}} {{#is foco_type "TEXT"}}
							<input autocomplete="off" type="text" class="form-control" name="{{foco_id}}" data-label="{{foco_label}}" data-type="{{foco_type}}"
							 rows="1" data-required="{{foco_required}}"> {{/is}}
						</td>
					</tr>
					{{/is}} {{/is}} {{/each}} {{/each}} {{!--
					<tr>
						<td width="30%" align="left" valign="top" class="siom-form-label">
							INFORMACION ADICIONAL
						</td>
						<td width="70%" align="left" valign="top" class="siom-form-control">
							<span class="btn btn-default btn-sm btn-file">
								Agregar archivo(s)
								<input autocomplete="off" type="file" name="info_adicional[]" id="info-{{form_id}}-0" data-id="0" data-list="listado-info_adicional-{{form_id}}"
								 data-preview="false">
							</span>
							<ul id="listado-info_adicional-{{form_id}}" class="list-group siom-file-list ctrl-camera" data-required="{{foco_required}}">
							</ul>
					</tr>
					--}}
				</table>
			</form>
		</div>
		{{/each}}
	</div>
</div>
{{else}}
<div class="alert alert-warning" role="alert">
	<strong>Aviso</strong> no hay datos para generar formulario
</div>
{{/if}}

<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/form_formulario.js" type="text/javascript" charset="utf-8"></script>
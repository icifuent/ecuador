<table width="100%">
  <tr>
      
      <td width="20%">
      	 <div class="siom-label">NOMBRE</div>
 		     <div class="siom-value">{{empty inec_campo}}</div>
      </td>
      <td width="40%" colspan="2">
      	 <div class="siom-label">DESCRIPCIÓN</div>
 		     <div class="siom-value">{{empty inec_valor}}</div>
      </td>
      
  </tr>


  {{#if archivos}}
  <tr>
      <td width="100%" colspan="5" align="left">
        <div class="siom-label">DOCUMENTACIÓN</div>
        <div class="siom-docu-link">
           {{#each archivos}}
            <a href="{{repo_ruta}}" target="blank">{{repo_nombre}}</a><small> | {{repo_descripcion}}</small><br>
           {{/each}}
        </div>
      </td>
  </tr>
  {{/if}}

</table>

<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Validar solicitud Informe via Web</h5>
 </div>
 <div class="col-md-7">
 </div>
</div>
{{#if status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<form class="form form-horizontal siom-form-tiny" role="form" action="#/os/solicitud/informe/{{data.os.orse_id}}/validar/{{data.tarea.tare_id}}" method="POST" id="form_solicitud_informe">

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Solicitud</legend>
  <table width="700px" align="center">
    <tr>
      <td width="30%" align="left" valign="top" class="siom-form-label">Solicitud</td>
      <td width="70%" align="left" valign="top" class="siom-form-control">
        <textarea class="form-control" rows="3" disabled>{{data.tarea.tare_data.razon}}</textarea>
      </td>
    </tr>

    <tr>
      <td width="30%" align="left" valign="top" class="siom-form-label">Resolución</td>
      <td width="70%" align="left" valign="top" class="siom-form-control">
        <select class="selectpicker" name="orse_solicitud_informe_web" title="Seleccione resolución"  data-container="body">
          <option value=""></option>
          <option value="APROBADA">APROBADA</option>
          <option value="RECHAZADA">RECHAZADA</option>
        </select>
      </td>
    </tr>
    
  </table>  

  <hr>
  
  <div class="row text-center">
    <div class="col-md-12">
      <a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
      <button type="submit" class="btn btn-primary" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
    </div>
  </div>
  </fieldset>
</form>

{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
      <br><small>{{data.debug}}</small>
     {{/if}}
  </div>
{{/if}}


<script src="js/os_solicitud_validar_informe.js" type="text/javascript" charset="utf-8"></script> 


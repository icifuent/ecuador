<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>periodicidads</h5>
    </div>
    {{#data}}
    <div class="col-md-7 text-right" id="Agregarperiodicidad">
        <a class="btn btn-primary btn-sm" id="BotonPeriodicidadAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-data="{{JSON .}}"></span> Agregar
        </a>
    </div>
    {{/data}}
</div>

{{#if data.status}}
<div id="Wizardperiodicidad" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" id="Primero" class="active">
            <span class="badge">1</span>Periodicidad
            <span class="chevron"></span>
        </li>
        <li data-step="2" id="Segundo">
            <span class="badge">2</span>Editar
            <span class="chevron"></span>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/periodicidad/filtro" method="POST" id="FormPeriodicidadLista">
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-7">
                                        <input autocomplete="off" type="text" class="form-control" name="peri_nombre" value="{{data.peri_nombre}}">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-periodicidad-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">
            <div class="panel panel-default">

                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/periodicidad/editar" method="POST" id="FormperiodicidadEditar">
                    <input  type="hidden" id="peri_id" name="peri_id">


                    <div class="col-md-8">
                        <fieldset class="siom-fieldset" id="siom-periodicidad-info">
                            <legend>periodicidad</legend>
                            <table width="100%">
                                <tr>
                                    <!--     <td colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Empresa</label>
                                            <div class="col-sm-8">
												<select id="UsuarioEmpresa" name="empr_id" class="selectpicker" data-width="100%" title='Seleccione Empresa' data-container="body">
												{{#data.empresas}}
													<option name="empr_id" value="{{empr_id}}">{{empr_nombre}}</option>
												{{/data.empresas}}
												</select>
                                            </div>
                                        </div>
                                    </td>
                                -->
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="peri_nombre" class="col-sm-3 control-label">Nombre</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="peri_nombre" name="peri_nombre" class="form-control" placeholder="Nombre" value="">
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="peri_meses" class="col-sm-3 control-label">Meses</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="peri_meses" name="peri_meses" class="form-control" placeholder="Meses" value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="peri_orden" class="col-sm-3 control-label">Orden</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="peri_orden" name="peri_orden" class="form-control" placeholder="Orden" value="">
                                            </div>
                                        </div>
                                    </td>

                                </tr>


                            </table>
                        </fieldset>
                    </div>




                    <div class="row text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="BotonperiodicidadGuardar"
                                style="margin:10px">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small>
    {{/if}}
</div>
{{/if}}


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/core_periodicidad.js" type="text/javascript" charset="utf-8"></script>
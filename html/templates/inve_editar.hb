{{{loadTemplate "template_emplazamiento" data.emplazamiento}}}

<fieldset class="siom-fieldset">
    <legend>Item</legend>
    <form class="form-horizontal siom-form-tiny" role="form" action="#/inve/emplazamiento/item/update" method="POST" id="siom-form-add-item">
        {{#data.item}}
        <input  type="hidden" name="empl_id" value="{{empl_id}}"></input>
        <input  type="hidden" name="inel_id" value="{{inel_id}}"></input>
        <div class="form-group">
            <label class="col-sm-4 control-label">Codigo</label>

            <div class="col-sm-5">
                <input autocomplete="off" type="text" class="form-control" id="inel_codigo" value="{{inel_codigo}}" name="inel_codigo" readonly="readonly">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Nombre</label>

            <div class="col-sm-5">
                <input autocomplete="off" type="text" class="form-control" id="inel_nombre" value="{{inel_nombre}}" name="inel_nombre">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Descripción</label>

            <div class="col-sm-5">
                <textarea type="text" class="form-control" id="inel_descripcion" name="inel_descripcion">{{inel_descripcion}}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label">Ubicación</label>
            <div class="col-sm-5">
                <input autocomplete="off" type="text" class="form-control" id="inel_ubicacion" value="{{inel_ubicacion}}" name="inel_ubicacion">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-4 control-label">Estado</label>
            <div class="col-sm-5">
                <select class="selectpicker" data-width="100%" title='ACTIVO' name="inel_estado">
                    <option value="ACTIVO">ACTIVO</option>
                    <option value="NOACTIVO">NO ACTIVO</option>
                </select>
            </div>
        </div>
        {{/data.item}}


        <fieldset class="siom-fieldset col-sm-6 col-md-offset-3">
            <legend>Campos adicionales</legend>
            {{#data.caracteristicas}}
            <input  type="hidden" name="inec_id[]" value="{{inec_id}}"></input>

            <div class="form-group">
                <label class="col-sm-2 control-label">Genero</label>
                <div class="col-sm-10">
                    <input autocomplete="off" type="text" class="form-control" id="{{inec_id}}" value="{{inec_genero}}" name="inec_genero[]">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Especie</label>
                <div class="col-sm-10">
                    <input autocomplete="off" type="text" class="form-control" id="{{inec_id}}" name="inec_especie[]" value="{{inec_especie}}"></input>
                </div>

            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Campo</label>
                <div class="col-sm-10">
                    <input autocomplete="off" type="text" class="form-control" id="{{inec_id}}" name="inec_campo[]" value="{{inec_campo}}"></input>
                </div>

            </div>

            <div class="form-group">
                <label for="fecha_solicitud" class="col-sm-2 control-label">Valor</label>
                <div class="col-sm-10">
                    <input autocomplete="off" type="text" class="form-control" id="{{inec_id}}" value="{{inec_valor}}" name="inec_valor[]">
                </div>
            </div>
            <hr> {{/data.caracteristicas}}
            <div id="item" style="padding-left: 0px;"></div>
            <a class="btn btn-info btn-xs pull-right" id="addItem">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar detalle
            </a>
        </fieldset>

        {{#data.item}}
        <div class="row text-center">
            <div class="col-md-12" style="padding-top: 10px;">
                <a href="#/inve/emplazamiento/{{empl_id}}/items/{{inel_id}}" class="btn btn-default">Cancelar</a>
                <button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="submit_editar_item">Guardar</button>
            </div>
        </div>
        {{/data.item}}
    </form>
</fieldset>


<script src="js/inve_editar.js" type="text/javascript" charset="utf-8"></script>
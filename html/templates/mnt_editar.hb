<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Editar mantenimiento</h5>
  </div>

</div>

<div class="step-pane sample-pane" data-step="2">
  {{{loadTemplate "template_emplazamiento" mantenimiento}}} {{#mantenimiento}}
  <fieldset class="siom-fieldset" style="margin-top:10px;">
    <legend>Mantenimiento</legend>
    <form class="form-horizontal siom-form-tiny" role="form" action="#/mnt/editar/{{mant_id}}" method="POST" id="form_editar_mnt">

      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="id" class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8">
              <input autocomplete="off" type="text" class="form-control" disabled="disabled" value="{{mant_id}}" />
            </div>
          </div>

          <div class="form-group">
            {{#ocultarElementoPorPerfil "MNT_EDITAR_GESTOR"}}
            <label for="fecha_solicitud" class="col-sm-4 control-label">Fecha programada</label>
            <div class="col-sm-8">
              <div class="row">
                <div class="col-md-4">
                  <input autocomplete="off" id="mant_fecha_programada_fecha" class="form-control" size="14" type="text" data-date-autoclose="true"
                    name="mant_fecha_programada_fecha" placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />

                </div>
                <div class="col-md-4">
                  <div class="input-group bootstrap-timepicker">
                    <input autocomplete="off" type="text" class="form-control timepicker" aria-describedby="addon" data-field="time" id="mant_fecha_programada_hora"
                      name="mant_fecha_programada_hora" data-modal-backdrop="true" placeholder="hh:mm" value="{{now '%H:%M'}} ">
                    <span class="input-group-addon" id="addon">
                      <i class="glyphicon glyphicon-time"></i>
                    </span>
                  </div>
                </div>
                <!--div class="col-md-4 text-left">
                      <div class="checkbox">
                        <label><input   type="checkbox" value="" name="mant_fecha_solicitud_ahora" id="mant_fecha_solicitud_ahora">Ahora</label>
                      </div>
                    </div-->
              </div>
            </div>
            {{/ocultarElementoPorPerfil}}
          </div>

          <!-- Hasta aqui -->

          <div class="form-group">
            {{#ocultarElementoPorPerfil "MNT_EDITAR_GESTOR"}}
            <label for="estado" class="col-sm-4 control-label">Estado</label>
            <div class="col-sm-8">
              <select {{estado_disable}} id="mant_estado" class="selectpicker estado" value="{{mant_estado}}" data-width="100%" name="mant_estado"
                title="Estado" data-container="body">
                <option value="0">Seleccionar Estado</option>
                <option value="CREADA">CREADA</option>
                <option value="VALIDANDO">VALIDANDO</option>
                <option value="APROBADA">APROBADA</option>
                <option value="RECHAZADA">RECHAZADA</option>
                <option value="FINALIZADA">FINALIZADA</option>
                <option value="ANULADA">ANULADA</option>
                <option value="NO REALIZADO">NO REALIZADO</option>

              </select>
            </div>
            {{/ocultarElementoPorPerfil}}
          </div>

          <!--  Habilitar solo para administrador -->

          <div class="form-group">
            <label for="empresa" class="col-sm-4 control-label">Empresa</label>
            <div class="col-sm-8">
              <select id="empr_id" class="selectpicker empresa" value="{{empr_id}}" data-value-select="{{empr_id}}" data-width="100%" name="empr_id"
                title="Seleccione empresa" data-container="body">
                {{#empresas}}
                <option name="empr_id" value="{{empr_id}}">{{empr_nombre}}</option>
                {{/empresas}}
              </select>
            </div>
          </div>

          <!--    <div class="form-group">
              <label for="especialidad" class="col-sm-4 control-label">Especialidad</label>
              <div class="col-sm-8">
                <select id="especialidad" class="selectpicker" data-width="100%" name="espe_id" title="Seleccione especialidad"  data-container="body" disabled="disabled">
                  <option value="{{espe_id}}">{{espe_nombre}}</option>
                 
                </select>
              </div>
            </div> -->

          <div class="form-group">
            <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
            <div class="col-sm-8">
              <textarea class="form-control" name="mant_descripcion" rows="5" value="{{mant_descripcion}}">{{mant_descripcion}}</textarea>
              <div id="mant_descripcion_palabras" class="siom-info text-right text-danger">
                <span>0</span> palabras de {{../max_words}}</div>
            </div>
          </div>

        </div>
      </div>

      <hr>
      <div class="row text-center">
        <div class="col-md-12">
          <a href="#/mnt/bandeja" class="btn btn-default">Cancelar</a>
          <button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="submit_editar_mnt">Guardar</button>
        </div>
      </div>
    </form>
  </fieldset>
  {{/mantenimiento}}
</div>


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mnt_editar.js" type="text/javascript" charset="utf-8"></script>
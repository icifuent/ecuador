    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Asignaciones</legend>
      {{#each this}}
        <div class="row">
          <label class="col-sm-2 siom-label">FECHA ASIGNACIÓN</label>
          <label class="col-sm-2 siom-value">{{maas_fecha_asignacion}}</label>
          <label class="col-sm-2 siom-label">ASIGNADA POR</label>
          <label class="col-sm-3 siom-value">{{usua_nombre}}</label>
          <label class="col-sm-1 siom-label">ESTADO</label>
          <label class="col-sm-2 siom-value">{{maas_estado}}</label>
        </div>
        <div class="row">
          <label class="col-sm-2 siom-label">JEFE CUADRILLA</label>
          <label class="col-sm-2 siom-value">{{jefe}}</label>
          <label class="col-sm-2 siom-label">ACOMPAÑANTE(S)</label>
          <label class="col-sm-4 siom-value">
          {{#each this}}
            {{acompanantes}}<br>
          {{/each}}
          </label>
        </div>
        {{#gt ../data.asignacion.length 1}}
        <hr>
        {{/gt}}

      {{else}}
      <p class="siom-no-data">Sin asignaciones</p>
      {{/each}}
    </fieldset>
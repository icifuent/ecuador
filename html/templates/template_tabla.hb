{{#if datos}}
<table data-toggle="table"  class="table table-condensed table-tiny" data-sort-order="desc">
    <thead>
        <tr>
          {{#campos}}
          <th data-align="left" data-sortable="true">{{.}}</th>
          {{/campos}}
        </tr>
    </thead>
    <tbody>
        {{#datos}}
        <tr>
          {{#this}}
            <td>{{.}}</td>
          {{/this}}
        </tr> 
        {{/datos}}
    </tbody>       
</table>
<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script> 
{{else}}
  <p class="siom-no-data">Sin datos</p>
{{/if}}






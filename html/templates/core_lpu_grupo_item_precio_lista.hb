
<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" >
  	{{#data}}
  	<div class="os" id="{{lpip_id}}">

	   <div class="acciones text-right">
        <button type="button" class="btn btn-default btn-xs editar" data-id="{{lpip_id}}" data-data="{{JSON.}}" >
          <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
        </button>


	  </div>


	  <div class="info text-left">
	  	<h4>{{lpip_id}} <small>{{lpip_sap_capex}}</small></h4>
	  </div>

	  <hr>

	</div>

	{{/data}}
  </div>

   <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	<div class="pagination-info">
	    	<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
	    </div>
	  </div>
	  <div class="col-md-8 text-right">
	    <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>


</div>


<script src="js/core_lpu_grupo_lista.js" type="text/javascript" charset="utf-8"></script>

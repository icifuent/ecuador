<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Bandeja de indisponibilidad</h5>
  </div>
  <div class="col-md-7 text-right">
  
    {{#siomChequearPerfil "#/indisponbilidad/todos"}}
      <a class="btn btn-primary btn-sm" href="#/indisponibilidad/todos">
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Generar OS
      </a>
    {{/siomChequearPerfil}}
  </div>
</div>

{{#if data.status}}
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/os/indisponiblidad/filtro" method="POST" id="siom-form-indisponibilidad-bandeja">

            <div class="form-group">
              <div class="col-sm-5">
                <button type="button" class="btn btn-primary btn-default" id="LimpiarIndisponibilidadBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
              </div>
              <div class="col-sm-7">
                <button type="button" class="btn btn-primary btn-default" id="BuscarIndisponibilidadBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Buscar
              </div>
            </div>

            <hr>

            <div class="form-group">
              <label for="id" class="col-sm-5 control-label">Cluster</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="indi_cluster" name="indi_cluster" value="{{data.indi_cluster}}">
              </div>
            </div>
            <div class="form-group">
              <label for="tipo" class="col-sm-5 control-label">Prioridad</label>
              <div class="col-sm-7">
                <select class="selectpicker" data-width="100%" name="indi_prioridad" title="Todos" data-value="{{data.indi_prioridad}}">
                  <option value="">TODOS</option>
				          {{#data.tipos}}
                    <option value="{{.}}">{{.}}</option>
				          {{/data.tipos}}
                </select>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-9" id="indi-lista">
  </div>
</div>

<div class="modal fade" id="ModalSolicitud">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Solicitud</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" rows="3" placeholder="Ingrese razón de solicitud"></textarea>
        <small>Se le enviará una notificación al encargado respectivo para autorizar solicitud.</small>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-loading-text="Solicitando..." data-complete-text="Solicitado!">Confirmar</button>
      </div>
    </div>
  </div>
</div>


{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/indisponibilidad_bandeja.js" type="text/javascript" charset="utf-8"></script>

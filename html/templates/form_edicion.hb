<div class="row">
    <div class="col-md-6 col-md-offset-3">
    	<div class="form-edicion-container" id="form" data-data="{{JSON .}}" data-type="form" data-cont-id="{{form.cont_id}}">

    		<div class="row header">
    			<div class="col-md-4 title">{{form.form_nombre}}</div>

		        <div class="col-md-8 menu">
			      	<ul class="nav nav-pills pull-right">
			        	<li role="presentation">
			        		<a class="btn btn-xs form-edicion-add-page" role="button" data-id="form">Agregar página</a>
			        	</li>
			        	<li role="presentation">
			        		<a class="btn btn-xs form-edicion-sort-pages" role="button" data-id="form">Ordenar páginas</a>
			        	</li>
			        	<li role="presentation">
			        		<a class="btn btn-xs form-edicion-properties" role="button" data-id="form">Propiedades</a>
			        	</li>
			      	</ul>
			    </div>
			</div>    

	    </div>
       
    </div>
</div>


<div class="row text-center form-edicion-actions">
    <div class="col-md-12">
    	<button type="button" class="btn btn-default" id="BotonFormCancelar">Cancelar</button>
        <button type="submit" class="btn btn-primary" data-loading-text="Guardando..." autocomplete="off" id="BotonFormGuardar">Guardar</button>
    </div>
</div>

<script src="js/dragula.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/form_edicion.js" type="text/javascript" charset="utf-8"></script>
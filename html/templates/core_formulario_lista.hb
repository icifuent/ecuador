
<div class="panel panel-default">
     <div class="panel-body siom-mnt-lista siom-lista-full-height" style="overflow:auto !important;">

        <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
            {{#data}}
            <div class="os-crear" id="{{form_id}}">
                <div class="estado text-center">
                    <span class="glyphicon glyphicon-stop status status-{{estadoColorStatus form_estado}}" aria-hidden="true"></span>
                </div>

                <div class="acciones text-right">
                    <button type="button" class="btn btn-default btn-xs" data-form-id="{{form_id}}" data-data="{{JSON .}}" id="BotonFormEditar">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </button>

                    {{#is form_estado 'ACTIVO'}}
                    <button type="button" id="BotonFormCambiarEstado" class="btn btn-default btn-xs"  data-id="{{form_id}}" data-nombre="{{form_nombre}}" data-estado="{{form_estado}}" >
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                    {{else}}
                    <button type="button" id="BotonFormCambiarEstado" class="btn btn-default btn-xs"  data-id="{{form_id}}" data-nombre="{{form_nombre}}" data-estado="{{form_estado}}" >
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                    {{/is}}
                </div>

                <div class="info text-left">
                    <h4>{{form_nombre}} <small>{{form_alias}}</small></h4>
                    <div class="info_adicional">Descripcion: <strong>{{form_descripcion}}</strong>
                    Especialidad: <strong>{{espe_nombre}}</strong>

                    </div>
                </div>

                <hr>
            </div>
            {{/data}}
        </div>


        <hr>

    </div>

    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                <div class="pagination-info">
                    <small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
                </div>
            </div>
            <div class="col-md-8 text-right">
                <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
            </div>
        </div>
    </div>

</div>

<script src="js/core_formulario_lista.js?v={{version}}" type="text/javascript" charset="utf-8"></script>
<form class="form-horizontal form-edicion-container-properties" role="form">


  <script>

  </script> {{#each properties}}
  <div class="form-group">
    <label for="nombre" class="col-sm-4 control-label">{{label}}</label>
    <div class="col-sm-8">
      {{#is type "TEXT"}}
      <input autocomplete="off" type="text" class="form-control" name="{{@key}}" value="{{value}}"> {{/is}} {{#is type "NUMERIC"}}
      <input autocomplete="off" type="text" onkeypress="return isNumberKey(event)" mintlength="1" maxlength="3" class="form-control"
        name="{{@key}}" value="{{value}}"> {{/is}} {{#is type "SELECT"}}
      <select class="form-control selectpicker" name="{{@key}}" data-style="btn-default btn-xs" value="{{value}}">
        {{#each options}}
        <option value="{{.}}" {{#is . ../value}} selected{{/is}}>{{@key}} {{value}}</option>
        {{/each}}
      </select>
      {{/is}} {{#is type "STATIC"}}
      <input autocomplete="off" type="text" class="form-control" name="{{@key}}" value="{{value}}" readonly="true"> {{/is}}

    </div>
  </div>
  {{/each}} {{#ifNotEmptyObject options}}
  <hr> {{#each options}}
  <div class="form-group">
    <label for="nombre" class="col-sm-4 control-label">{{label}}</label>
    <div class="col-sm-8">
      {{#is type "TEXT"}}
      <input autocomplete="off" type="text" class="form-control" name="{{@key}}" value="{{value}}"> {{/is}} {{#is type "NUMERIC"}}
      <input autocomplete="off" type="text" onkeypress="return isNumberKey(event)" mintlength="1" maxlength="3" class="form-control"
        name="{{@key}}" value="{{value}}"> {{/is}} {{#is type "SELECT"}}
      <select class="form-control selectpicker" name="{{@key}}" data-style="btn-default btn-xs" value="{{value}}">
        {{#each options}}
        <option value="{{.}}" {{#is . ../value}} selected{{/is}}>{{@key}}</option>
        {{/each}}
      </select>
      {{/is}} {{#is type "ARRAY"}}
      <input autocomplete="off" type="text" class="form-control" name="{{@key}}" value="{{value}}" data-type="array">
      <span class="help-block">Ingrese items separados por comas</span>
      {{/is}}
    </div>
  </div>
  {{/each}} {{/ifNotEmptyObject}} {{#in type '["select","text"]' }}
  <hr>

  <div class="form-group">
    <label class="col-sm-4 control-label">Inventariable</label>
    <div class="col-sm-8">
      <input type="checkbox" name="indf-inventariable" {{#is indf_inventariable "1"}}checked{{/is}}>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Genero</label>
    <div class="col-sm-8">
      <input autocomplete="off" type="text" class="form-control" name="indf-genero" value="{{indf_genero}}">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Especie</label>
    <div class="col-sm-8">
      <input autocomplete="off" type="text" class="form-control" name="indf-especie" value="{{indf_especie}}">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Campo</label>
    <div class="col-sm-8">
      <input autocomplete="off" type="text" class="form-control" name="indf-campo" value="{{indf_campo}}">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Código</label>
    <div class="col-sm-8">
      <input  type="checkbox" name="indf-codigo" {{#is indf_codigo "1"}}checked{{/is}}>
    </div>
  </div>


  {{/in}}

</form>
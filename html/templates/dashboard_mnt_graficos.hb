
<div class="row">
	<div class="col-md-12 text-right">
        <div class="siom-form-tiny">
            Alcance: <b>{{zona_alcance}}</b>
        </div>
    </div>
</div>	
<br/>

{{#if status}}

  {{#each mnt_zona}}
  <div class="col-md-4">
    <div class="siom-chart-title " >MPP {{@key}}</div>
    <div class="siom-chart siom-chart-columns-zonas" data-data="{{JSON .}}">Generando gráfico...</div>
  </div>
  {{/each}}

  {{#each mnt_espe_peri}}
  <div class="col-md-4">
    <div class="siom-chart-title">MPP ZONA {{@key}}</div>
    <div class="siom-chart siom-chart-columns-zonas" data-data="{{JSON .}}">Generando gráfico...</div>
  </div>
  {{/each}}
  
{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}<br><small>{{data.debug}}</small>{{/if}}
  </div>
{{/if}}

<script src="js/dashboard_mnt_graficos.js" type="text/javascript" charset="utf-8"></script>
<div class="row siom-section-header">
    <div class="col-md-9">
        <h5>Inventario</h5>
    </div><div class="col-md-3" style="text-align: right;padding-right: 0px;">
        <a class="btn btn-primary btn-sm" href="#/inve/emplazamiento/{{empl_id}}/item/agregar" style="margin-right: 20px;">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar Item
        </a>
    </div>

</div>
{{#if data.status}}
</div>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                    {{{loadTemplate "inve_emplazamiento_items_filtro" data}}}
            </div>
        </div>
    </div>
    
    <div class="col-md-9" id="inve-item-lista" style="margin-right: 0px;padding-right: 35px;padding-top: 5px;"></div>

</div>


{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br><small>{{data.debug}}</small> {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/inve_emplazamiento_item_filtro.js" type="text/javascript" charset="utf-8"></script>
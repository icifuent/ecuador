<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Asignación Inspeccion</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.emplazamiento}}}
</fieldset>



  {{{loadTemplate "template_inspeccion" data.inspeccion}}}



<fieldset class="siom-fieldset" style="margin-top:20px;">
    <legend>Asignación</legend>

    <form class="form form-horizontal siom-form-tiny" role="form" action="#/insp/asignacion/{{data.inspeccion.insp_id}}" method="POST" id="siom-form-asignacion">
   		<div class="row form-group">
		    <div class="col-sm-2 col-md-offset-1">
		    	<label class="control-label">USUARIO</label>
		   	</div>
		    <div class="col-sm-4">
		    	<select id="jefecuadrilla" class="selectpicker" data-width="100%"  title='Seleccione un Usuario' name="jefecuadrilla" data-live-search="true">
		    		<option value=""></option>
		    	{{#data.jefes}}
		     		<option value="{{usua_id}}">{{usua_nombre}}</option>
		    	{{/data.jefes}}
		    	</select>
		   	</div>
	  	</div>

	  
  		<hr>
        <div class="row text-center siom-form-actions">
          <div class="col-md-12">
            <a href="#/insp/bandeja" class="btn btn-default">Cancelar</a>
           
            <button type="submit" class="btn btn-primary" data-loading-text="Asignando..." autocomplete="off" id="submit_asignar">Guardar</button>
          </div>
        </div>
  	</form>
 </fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/insp_asignacion.js" type="text/javascript" charset="utf-8"></script>

<div class="panel panel-default">
    <div class="panel-body siom-mnt-lista siom-lista-full-height" style="overflow:hidden;">


        <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow-x:hidden;">
            {{#data}}
            <div class="mnt col-md-12">

                <div class="estado text-center">
                    <span class="id" style="word-wrap: break-word;">Codigo {{inel_codigo}}</span>
                    <span class="label label-{{#is inel_estado 'ACTIVO'}}success{{else}}danger{{/is}} status">{{inel_estado}}</span>
                </div>

                <div class="acciones text-right">
                    <div class="link"><a class="btn btn-primary btn-xs" href="#/inve/emplazamiento/{{empl_id}}/items/{{inel_id}}" role="button">Ver Detalle</a>
                    </div>
                </div>

                <div class="info text-left col-md-offset-3">

                    <h4>
                      {{#is insp_responsable 'MOVISTAR'}}
                      <i class="glyphicon glyphicon-home"></i>&nbsp; 
                      {{else}}
                      {{/is}}
                      {{inel_nombre}} <small>{{inve_marca}}</small>
                </h4>


                    <div class="info_adicional">
                        Ubicacion: <strong>{{inel_ubicacion}}</strong> Descripcion: <strong>{{inel_descripcion}}</strong>, {{#if mant_fecha_ejecuccion}}
                        <br> Fecha ejecuccion MNT: <strong>{{mant_fecha_ejecuccion}}</strong> {{/if}}
                    </div>
                </div>
            </div>
            <hr class="col-md-12"> 
                    <input type="hidden" value="{{empl_id}}" name="empl_id" id="empl_id">

            {{/data}}
        </div>


        <hr>

    </div>

    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                {{#if status}}
                <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..." data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
                {{/if}}
            </div>
            <div class="col-md-8 text-right">
                <div class="pagination-info">
                    Página {{pagina}}/{{paginas}} ({{total}} registros)
                </div>
                <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
            </div>
        </div>
    </div>

</div>


<script src="js/inve_items_lista.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>

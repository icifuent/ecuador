<div class="panel panel-default" id="mapa" style="{{#is options.tipo 'mapa'}} display:block; {{else}} display:none; {{/is}}">
  <div class="panel-body siom-mapa-full-height" id="emplazamientos_mapa" data-emplazamientos="{{JSON data}}" >
  	Cargando mapa...
  </div>
  <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-6 text-left">
	  	{{#siomChequearPerfil "#/emplazamientos/descargar"}}
		 	<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
		 {{/siomChequearPerfil}}
	  </div>
	  <div class="col-md-6 text-right">
	  	<small>{{total}} emplazamientos / 
	  	{{#gt con_visita 0}}
	  		{{con_visita}} visita(s)
	  	{{else}}
	  		Sin visitas en proceso
	  	{{/gt}}
	  	</small>
	  </div>
	</div>
  </div>
</div>

<div class="panel panel-default" id="lista" style="{{#is options.tipo 'lista'}} display:block; {{else}} display:none; {{/is}}">
  <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
  	{{#data}}
  	<div class="os-crear">
  	  <div class="estado text-center">
	  	<span class="glyphicon glyphicon-stop status status-{{emplColorStatus orse_indisponibilidad}}" aria-hidden="true"></span>
	  </div>

	  <div class="acciones col-md-4 text-right">
	  	<div class="link"><a href="#/emplazamientos/detalle/{{empl_id}}">Ver detalle</a></div>
	  </div>
	  <div class="info text-left">
	  	<h4>{{empl_nombre}} <small>{{empl_nemonico}}</small></h4>
	  	<div class="info_adicional">Clasificación: <strong>{{clas_nombre}}</strong>, Dirección: <strong>{{empl_direccion}}</strong>, Responsable: <strong>{{empl_responsable}}</strong></div>
	  </div>
	  
	</div>
	<hr>
	{{/data}}
  </div>
  <div class="panel-footer siom-paginacion">
  	<div class="row">
	  		<div class="col-md-6 text-left">
  				{{#siomChequearPerfil "#/emplazamientos/descargar"}}
		 		<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
	  			{{/siomChequearPerfil}}
	  		</div>
	  <div class="col-md-6 text-right">
	  	<small>{{total}} emplazamientos</small>
	  </div>
	</div>
  </div>
</div>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/markerclusterer.js" type="text/javascript" charset="utf-8"></script>
<script src="js/emplazamientos_estados.js" type="text/javascript" charset="utf-8"></script>
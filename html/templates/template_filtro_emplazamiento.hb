<div id="siom-form-emplazamientos-filtro">
  <div class="form-group">

    <div class="form-group">
      <div class="col-sm-5">
        <button type="button" class="btn btn-primary btn-default" id="LimpiarInspCrear">
          <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
      </div>
      <div class="col-sm-7">
        <button type="button" class="btn btn-primary btn-default" id="BuscarInspCrear">
          <span class="glyphicon" aria-hidden="true"></span> Buscar
      </div>
    </div>

    <hr>

    <label for="nombre" class="col-sm-5 control-label">Nombre</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="nombre" name="empl_nombre">
    </div>
  </div>

  <div class="form-group">
    <label for="nemonico" class="col-sm-5 control-label">Nemónico</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="nemonico" name="empl_nemonico">
    </div>
  </div>

  <hr>

  <div class="form-group">
    <label for="tecnoclogia" class="col-sm-5 control-label">Tecnología</label>
    <div class="col-sm-7">
      <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas' multiple name="tecn_id">
        {{#data.tecnologias}}
        <option value="{{tecn_id}}">{{tecn_nombre}}</option>
        {{/data.tecnologias}}
      </select>
    </div>
  </div>

  <hr>

  <div class="form-group">
    <label for="direccion" class="col-sm-5 control-label">Dirección</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="empl_direccion" name="empl_direccion">
    </div>
  </div>

  <div class="form-group">
    <label for="zona" class="col-sm-5 control-label">Zona</label>
    <div class="col-sm-7">
      <select name="zona_id" class="selectpicker" data-width="100%" title='Todas'>
        <option value="">Todas</option>
        {{#data.zonas}}
        <option value="{{zona_id}}">{{zona_nombre}}</option>
        {{/data.zonas}}
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="region" class="col-sm-5 control-label">Región</label>
    <div class="col-sm-7">
      <select name="regi_id" class="selectpicker" data-width="100%" title='Todas' data-container="body">
        <option value="">Todas</option>
        {{#data.regiones}}
        <option value="{{regi_id}}">{{regi_nombre}}</option>
        {{/data.regiones}}
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="cluster" class="col-sm-5 control-label">Cluster</label>
    <div class="col-sm-7">
      <select name="clus_id" class="selectpicker" data-width="100%" title='Todos'>
        <option value="">Todos</option>
        {{#data.clusters}}
        <option value="{{zona_id}}">{{zona_nombre}}</option>
        {{/data.clusters}}
      </select>
    </div>
  </div>
  <!--
                <div class="text-center">
                  <button type="submit" class="btn btn-primary" data-loading-text="Filtrando..." >Filtrar</button>
                </div>
                -->
</div>
<script src="js/template_filtro_emplazamiento.js" type="text/javascript" charset="utf-8"></script>
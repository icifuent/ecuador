{{#if archivos}}
  {{#each archivos}}
  <div class="siom-subtitle">{{@key}}</div>
  <ul class="list-group siom-file-list">
    {{#each .}}
    <li class="list-group-item siom-docu-file">
      <div class="siom-docu-nombre"><a href="{{repo_ruta}}" target="blank">{{repo_nombre}}</a></div>
      <div class="siom-docu-descripcion">{{repo_descripcion}}</div>
      <div class="siom-docu-info">{{usua_creador}} | {{repo_fecha_creacion}}</div>


      <div class="siom-docu-acciones pull-right">
          {{#if ../../canEdit}}
          <button type="button" class="btn btn-default btn-xs" data-repo_id="{{repo_id}}" data-data="{{JSON .}}" data-action="edit">
              <span class="glyphicon glyphicon-pencil" aria-hidden="true"> Editar</span>
          </button>
          {{/if}}

          {{#if ../../canDelete}}
          <button type="button" class="btn btn-default btn-xs"  data-repo_id="{{repo_id}}" data-repo_nombre="{{repo_nombre}}"  data-action="delete">
              <span class="glyphicon glyphicon-trash" aria-hidden="true"> Eliminar</span>
          </button>
          {{/if}}
      </div>

    </li>
    {{/each}}
  </ul>
  {{/each}}

{{else}}
  <div class="alert alert-notice" role="alert">
    No se han encontrado archivos relacionados a los mantenimientos.
  </div>
{{/if}}
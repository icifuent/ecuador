<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Tracking</h5>
  </div>
  <div class="col-md-7 text-right"></div>
</div>

{{#if data.status}}
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/tracking/filtro" method="POST" id="siom-form-tracking-filtro">

          <div class="form-group">
            <div class="col-sm-5">
              <button type="button" class="btn btn-primary btn-default" id="LimpiarTracking">
                <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
            </div>
            <div class="col-sm-7">
              <button type="button" class="btn btn-primary btn-default" id="BuscarTracking">
                <span class="glyphicon" aria-hidden="true"></span> Buscar
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="fecha_inicio" class="col-sm-5 control-label">Fecha inicio</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_inicio" class="form-control" size="14" type="text" data-date-autoclose="true" name="ustr_fecha_inicio"
                value="{{#if data.ustr_fecha_inicio}}{{data.ustr_fecha_inicio}}{{/if}}" />
            </div>
          </div>
          <div class="form-group">
            <label for="fecha_fin" class="col-sm-5 control-label">Fecha fin</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_fin" class="form-control" size="14" type="text" data-date-autoclose="true" name="ustr_fecha_fin"
                value="{{#if data.ustr_fecha_fin}}{{data.ustr_fecha_fin}}{{/if}}" />
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Usuario</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" name="ustr_usuario" id="ustr_usuario" data-container="body" data-live-search="true">
                <!--option value="%">Seleccionar usuario</option-->
                <option value="0">Seleccionar usuario</option>
                {{#data.usuarios}}
                <option value="{{usua_id}}">{{usua_nombre}}</option>
                {{/data.usuarios}}
              </select>
            </div>
          </div>

          <!--
          <div class="form-group">
            <label class="col-sm-5 control-label">Mostrar OS</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" name="mostrar_os">
                <option value="0">NO</option>
                <option value="1">SI</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-5 control-label">Mostrar MNT</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" name="mostrar_mnt">
                <option value="0">NO</option>
                <option value="1">SI</option>
              </select>
            </div>
          </div>
		  -->

        </form>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div id="tracking">
      <div id="tracking-map"></div>
      <div id="tracking-legend"></div>
    </div>
  </div>
</div>
<script src="js/tracking.js" type="text/javascript" charset="utf-8"></script> {{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}} {{#if data.debug}}
  <br>
  <small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}
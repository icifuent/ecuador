<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Solicitud validar cambio fecha programada</h5>
 </div>
 <div class="col-md-7">
 </div>
</div>
{{#if status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Mantenimiento</legend>
  {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
</fieldset>

<form class="form form-horizontal siom-form-tiny" role="form" action="#/mnt/solicitud/cambio_fecha_programada/{{data.mantenimiento.mant_id}}/validar/{{data.tarea.tare_id}}" method="POST" id="form_solicitud_cambio_fecha">

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Solicitud</legend>
  <table width="700px" align="center">
    <tr>
      <td width="30%" align="left" valign="top" class="siom-form-label">Nueva fecha programada</td>
      <td width="70%" align="left" valign="top" class="siom-form-control">
        <label class="form-control">{{data.tarea.tare_data.nueva_fecha_programada}}</label>
      </td>
    </tr>

    <tr>
      <td width="30%" align="left" valign="top" class="siom-form-label">Solicitud</td>
      <td width="70%" align="left" valign="top" class="siom-form-control">
        <textarea class="form-control" rows="3" disabled>{{data.tarea.tare_data.razon}}</textarea>
      </td>
    </tr>

    <tr>
      <td width="30%" align="left" valign="top" class="siom-form-label">Resolución</td>
      <td width="70%" align="left" valign="top" class="siom-form-control">
        <input type="hidden" name="mant_fecha_programada" value="{{data.tarea.tare_data.nueva_fecha_programada}}"/>
        <input type="hidden" name="maso_id" value="{{data.tarea.tare_data.maso_id}}"/>
        <select class="selectpicker" name="mant_solicitud_cambio_fecha_programada" title="Seleccione resolución">
          <option value=""></option>
          <option value="APROBADA">APROBADA</option>
          <option value="RECHAZADA">RECHAZADA</option>
        </select>
      </td>
    </tr>
    
  </table>  

  <hr>
  
  <div class="row text-center">
    <div class="col-md-12">
      <a href="#/mnt/bandeja" class="btn btn-default">Cancelar</a>
      <button type="submit" class="btn btn-primary" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
    </div>
  </div>
  </fieldset>
</form>

{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
      <br><small>{{data.debug}}</small>
     {{/if}}
  </div>
{{/if}}


<script src="js/mnt_solicitud_validar_cambio_fecha_programada.js" type="text/javascript" charset="utf-8"></script> 


<fieldset class="siom-fieldset">
            <legend>Inventario</legend>

<table width="100%">
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">ID</div>
        <div class="siom-value">{{empty inve_id}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">Marca</div>
        <div class="siom-value">{{empty inve_marca}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">Modelo</div>
        <div class="siom-value">{{empty inve_modelo}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">Serie</div>
        <div class="siom-value">{{empty inve_serie}}</div>
      </td>
  </tr>
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">Estado</div>
        <div class="siom-value">{{empty inve_estado}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">Descripcion</div>
        <div class="siom-value">{{empty inve_descripcion}}</div>
      </td>
      
  </tr>

  
</table>
 </fieldset>
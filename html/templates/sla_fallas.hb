<div class="row siom-section-header">
	<div class="col-md-4">
		<h5>SLA: Tasa de Fallas</h5>
	</div>
</div>

<ul class="nav nav-tabs">
	<li class="active">
		<a class="BotonSLAFallasMenu" data-tipo_datos="grafico_zonas">Grafico Zonas</a>
	</li>
	<li>
		<a class="BotonSLAFallasMenu" data-tipo_datos="detalle">Detalle</a>
	</li>
	<!--li><a class="BotonSLAFallasMenu" data-tipo_datos="grafico_especialidades">Grafico Especialidades</a></li-->
	{{#siomChequearPerfil "#/sla/*/validacion"}}
	<li>
		<a class="BotonSLAFallasMenu" data-tipo_datos="validacion">Validacion</a>
	</li>
	{{/siomChequearPerfil}}
</ul>

<div class="row" style="margin-left: 5px; margin-top:20px;">

	<form class="form-horizontal siom-form-tiny" role="form" action="#/sla/fallas/filtro" method="POST" id="siom-form-sla-fallas">

		<div class="col-md-3">
			<fieldset class="siom-fieldset" style="padding-bottom:20px; margin-top:0px;">
				<legend>Filtros</legend>
				<div class="form-group" id="FiltroEspecialidades" style="display:block">
					<label for="id" class="col-sm-5 control-label">Especialidades</label>
					<div class="col-sm-7">
						<select class="selectpicker" data-width="100%" multiple name="espe_id" title="TODAS" data-container="body">
							{{#especialidades}}
							<option value="{{espe_id}}">{{espe_nombre}}</option>
							{{/especialidades}}
						</select>
					</div>
				</div>
				<div class="form-group" id="FiltroEspecialidad" style="display:none">
					<label for="id" class="col-sm-5 control-label">Especialidad</label>
					<div class="col-sm-7">
						<select class="selectpicker" data-width="100%" name="espe_idu" title="TODAS" data-container="body">
							{{#especialidades}}
							<option value="{{espe_id}}">{{espe_nombre}}</option>
							{{/especialidades}}
						</select>
					</div>
				</div>

				<div class="form-group" id="FiltroZona" style="display:none">
					<label for="id" class="col-sm-5 control-label">Zona</label>
					<div class="col-sm-7">
						<select class="selectpicker" data-width="100%" name="zona_id" title="TODAS" data-container="body">
							<optgroup label="MOVISTAR">
								{{#zonas_movistar}}
								<option value="{{zona_id}},zona">{{zona_nombre}}</option>
								{{/zonas_movistar}}
							</optgroup>
							<optgroup label="CONTRATO">
								{{#zonas_contrato}}
								<option value="{{zona_id}},zona">{{zona_nombre}}</option>
								{{/zonas_contrato}}
							</optgroup>
							<optgroup label="REGIONES">
								{{#regiones}}
								<option value="{{regi_id}},region">{{regi_nombre}}</option>
								{{/regiones}}
							</optgroup>
						</select>
					</div>
				</div>

				<div class="form-group" id="FiltroZonaTipo" style="display:block">
					<label for="id" class="col-sm-5 control-label">Tipo de Zona</label>
					<div class="col-sm-7">
						<select class="selectpicker" data-width="100%" name="zona_tipo">
							{{#zonas_tipos}}
							<option value="{{.}}">{{.}}</option>
							{{/zonas_tipos}}
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="id" class="col-sm-5 control-label">Inicio Periodo</label>
					<div class="col-sm-7">
						<input autocomplete="off" id="orse_fecha_validacion_inicio" class="form-control" size="14" type="text" name="orse_fecha_validacion_inicio"
						 value="{{#if data.orse_fecha_validacion_inicio}} {{formatDate data.orse_fecha_validacion_inicio '%d-%m-%Y'}} {{/if}}"
						/>
					</div>
				</div>
				<div class="form-group">
					<label for="id" class="col-sm-5 control-label">Fin Periodo</label>
					<div class="col-sm-7">
						<input autocomplete="off" id="orse_fecha_validacion_termino" class="form-control" size="14" type="text" name="orse_fecha_validacion_termino"
						 value="{{#if data.orse_fecha_validacion_termino}} {{formatDate data.orse_fecha_validacion_termino '%d-%m-%Y'}} {{/if}}"
						/>
					</div>
				</div>
				<div class="form-group" id="os_id" style="display:none">
					<label for="id" class="col-sm-5 control-label">Nº OS</label>
					<div class="col-sm-7">
						<input autocomplete="off" type="text" class="form-control" name="orse_id" value="{{data.orse_id}}">
					</div>
				</div>

				<input  type="hidden" id="tipo_datos" name="tipo_datos" value="grafico_zonas" />
				<input  type="hidden" id="pagina" name="pagina" value="1" />
			</fieldset>
		</div>

		{{#if data.status}}
		<div class="row" style="margin-left: 15px;">
			<div class="col-md-9" id="sla-fallas-datos"></div>
		</div>

		{{else}}
		<div class="alert alert-danger" role="alert">
			{{data.error}} {{#if data.debug}}
			<br>
			<small>{{data.debug}}</small> {{/if}}
		</div>
		{{/if}}

	</form>
</div>


<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/sla_fallas.js" type="text/javascript" charset="utf-8"></script>
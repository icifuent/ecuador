<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Inventario</h5>
  </div>

</div>
{{#if data.status}}
</div>

<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">

      
              <form class="form-horizontal siom-form-tiny" role="form" action="#/inve/emplazamiento/filtro" method="POST" id="siom-form-emplazamientos-filtro">


               {{{loadTemplate "inve_emplazamiento_filtro" data}}}
             </form>
    


      </div>
    </div>
  </div>
  <div class="col-md-9" id="inve-emplazamiento-lista"></div>
  
</div>


{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/inve_emplazamiento.js" type="text/javascript" charset="utf-8"></script>

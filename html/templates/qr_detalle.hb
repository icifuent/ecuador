<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Detalle Registro de Código QR</h5>
    </div>
    <div class="col-md-7">
    </div>
</div>
{{#if data.status}}
<div class="row">
    <div class="col-sm-9">
      				  
        <!--/fieldset-->
        <!--fieldset class="siom-fieldset">
            <legend>Mantenimiento</legend>
            {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
        </fieldset-->
        <!--div class="siom-section-subtitle">Usuario</div-->
        <fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Resumen</legend>
            {{#each data.cabQr}}
            <div class="row">
                <label class="col-sm-1 siom-label">USUARIO</label>
                <label class="col-sm-2 siom-value">{{usua_nombre}}</label>
				<label class="col-sm-1 siom-label">FECHA DE CAPTURA QR</label>
                <label class="col-sm-1 siom-value">{{leco_fecha}}</label>
                <label class="col-sm-1 siom-label">LATITUD</label>
                <label class="col-sm-1 siom-value">{{leco_latitud}}</label>
				<label class="col-sm-1 siom-label">LONGITUD</label>
                <label class="col-sm-1 siom-value">{{leco_longitud}}</label><p align="left">
				<!--label class="col-sm-1 siom-label">EMPLAZAMIENTO</label>
                <label class="col-sm-1 siom-value">{{empl_nombre}}</label--></p>
                <label class="col-sm-1 siom-link">
                </label>
            </div>
            <!--div class="row siom-subtitle">
                <div class="col-sm-2">DETALLE</div>
            </div-->
            {{#each detalle}}
            <!--div class="row">
                <label class="col-sm-3 col-md-offset-1 siom-value">{{rtfr_accion}}</label>
                <label class="col-sm-2 siom-label">FECHA</label>
                <label class="col-sm-2 siom-value">{{rtfr_fecha}}</label>
                <label class="col-sm-2 siom-label">ESTADO</label>
                <label class="col-sm-2 siom-value">{{#osEstadoDetalleVisita rtfr_estado}}{{/osEstadoDetalleVisita}}</label>
            </div-->
            {{else}}
            <!--p class="siom-no-data">Sin ingreso a sitio</p-->
            {{/each}}
            {{#gt ../data.visitas.length 1}}
            <hr>
            {{/gt}}
            {{else}}
            <p class="siom-no-data">Sin Resumen</p>
            {{/each}}
        </fieldset>
		 <fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Emplazamiento</legend>
            {{#each data.detEmp}}
            <div class="row">
				<label class="col-sm-1 siom-label">NOMBRE EMPLAZAMIENTO</label>
                <label class="col-sm-2 siom-value">{{empl_nombre}}</label>
				<label class="col-sm-1 siom-label">DIRECCIÓN</label>
                <label class="col-sm-2 siom-value">{{empl_direccion}}</label>
				<label class="col-sm-1 siom-label">CONTRATO</label>
                <label class="col-sm-2 siom-value">{{cont_nombre}}</label>
				<label class="col-sm-1 siom-label">OBSERVACION</label>
                <label class="col-sm-2 siom-value">{{empl_observacion}}</label>
                <!--label class="col-sm-1 siom-link"-->
                </label>
            </div>
            <!--div class="row siom-subtitle">
                <div class="col-sm-2">DETALLE</div>
            </div-->
            {{#each detalle}}
            <!--div class="row">
                <label class="col-sm-3 col-md-offset-1 siom-value">{{rtfr_accion}}</label>
                <label class="col-sm-2 siom-label">FECHA</label>
                <label class="col-sm-2 siom-value">{{rtfr_fecha}}</label>
                <label class="col-sm-2 siom-label">ESTADO</label>
                <label class="col-sm-2 siom-value">{{#osEstadoDetalleVisita rtfr_estado}}{{/osEstadoDetalleVisita}}</label>
            </div-->
            {{else}}
            <!--p class="siom-no-data">Sin ingreso a sitio</p-->
            {{/each}}
            {{#gt ../data.visitas.length 1}}
            <hr>
            {{/gt}}
            {{else}}
            <p class="siom-no-data">Sin Emplazamiento</p>
            {{/each}}
        </fieldset>
		 <!--div class="siom-section-subtitle">Fecha</div>
        <fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Fecha de envío</legend>
            {{#each data.detQr}}
            <div class="row">
                <label class="col-sm-2 siom-label">FECHA DE CAPTURA QR</label>
                <label class="col-sm-2 siom-value">{{leco_fecha}}</label>
                <!--label class="col-sm-2 siom-label">ASIGNADA POR</label>
                <label class="col-sm-3 siom-value">{{usua_nombre}}</label>
                <label class="col-sm-1 siom-label">ESTADO</label>
                <label class="col-sm-2 siom-value">{{maas_estado}}</label-->
            <!--/div>
            <div class="row">
                <!--label class="col-sm-2 siom-label">JEFE CUADRILLA</label>
                <label class="col-sm-2 siom-value">{{detalle.jefe}}</label>
                <label class="col-sm-2 siom-label">ACOMPAÑANTE(S)</label>
                <label class="col-sm-4 siom-value"-->
             <!--   {{#each detalle.acompanantes}}
                {{.}}<br>
                {{/each}}
                </label>
            </div>
            {{#gt ../data.leco_fecha.length 1}}
            <hr>
            {{/gt}}
            {{else}}
            <p class="siom-no-data">Sin fecha</p>
            {{/each}}
        </fieldset-->
		  <!--div class="siom-section-subtitle">Resumen</div>
        <!--fieldset class="siom-fieldset">
                  <legend>Qr Registrados por el usuario</legend>
				          {{#data.detQr}}
                  <label class="col-sm-3 siom-value" value="{{rlco_codigo_descifrado}}">{{rlco_codigo_descifrado}} </label>
				  <!--label class="col-sm-2 siom-value" value="{{rlc.leco_id}}">{{rlc.leco_id}}</label-->
				         <!-- {{/data.detQr}}-->
			<!--fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Detalle</legend>
            {{#each data.detQr}}
			 <div>
			 
			 <table class="table table-striped table-hover table-condensed"   data-sort-order="desc">
                                <thead>
                                    <tr>
									<th  class="col-sm-1 siom-label" >N° QR</th>
							
                                    <th class="col-sm-1 siom-label" >DETALLE DE REGISTRO</th> 
								
                                    </tr>
                                </thead>
                                <tbody id="peri_lista"></tbody>
                            </table>
                            <table class="table table-striped table-hover table-condensed"   data-sort-order="desc">
                                <thead>
                                    <tr>
						
									<label class="col-sm-3 siom-value">{{rlco_orden}}</label>
                                 
									<label  class="col-sm-3 siom-value">{{rlco_codigo_descifrado}}</label>	
                                    </tr>
                                </thead>
                                <tbody id="peri_lista"></tbody>
                            </table>
              </div>
			
            <!--div class="row">
                <!--label class="col-sm-3 siom-label">DETALLE DEL REGISTRO DEL CODIGO</label-->
				<!--label class="col-sm-3 siom-value">{{rlco_orden}}</label>
                <label class="col-sm-3 siom-value">{{rlco_codigo_descifrado}}</label>
                <!--label class="col-sm-2 siom-label">ASIGNADA POR</label>
                <label class="col-sm-3 siom-value">{{usua_nombre}}</label>
                <label class="col-sm-1 siom-label">ESTADO</label>
                <label class="col-sm-2 siom-value">{{maas_estado}}</label-->
            <!--/div>
            <div class="row">
                <!--label class="col-sm-2 siom-label">JEFE CUADRILLA</label>
                <label class="col-sm-2 siom-value">{{detalle.jefe}}</label>
                <label class="col-sm-2 siom-label">ACOMPAÑANTE(S)</label>
                <label class="col-sm-4 siom-value"-->
             <!--   {{#each detalle.acompanantes}}
                {{.}}<br>
                {{/each}}
                </label>
            </div>
            {{#gt ../data.leco_fecha.length 1}}
            <hr>
            {{/gt}}
            {{else}}
            <p class="siom-no-data">Sin fecha</p>
            {{/each}}
        </fieldset-->
		<div class="col-md-16">
                    <fieldset class="siom-fieldset">
                        <legend>Detalle</legend>
						<label class="col-sm-1 siom-label">N° QR</label>
						<label class="col-sm-6 siom-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DETALLE DEL REGISTRO</label>
						 {{#each data.detQr}}
                        <div>                                
                            <table class="table table-striped table-hover table-condensed"   data-sort-order="desc">
                               
                                    
                                <!--tbody id="peri_lista"></tbody-->
                            </table>
							<table class="table table-striped table-hover table-condensed"   data-sort-order="desc">
                               
									  <label class="col-sm-3 siom-value">{{rlco_orden}}</label>
                                    
									  <label text-align="left" class="col-sm-3 siom-value">&nbsp;{{rlco_codigo_descifrado}}</label>	
									   <i class="glyphicon glyphicon-qrcode"></i>	
                                    </tr>
                                <!--tbody id="peri_lista"></tbody-->
                            </table>
                        </div>
						  {{/each}}
                     </fieldset>
        </div>
		
		
</div>
	 <div class="col-sm-3">
		

<!--script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu-916DdpKAjTmJNIgngS6HL_kDIKU0aU&callback=myMap"></script-->
	<!--fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend></legend>
      <div id="siom-qr-detalle-mapa" class="siom-detalle-mapa" data-latitud="{{data.lec.leco_latitud}}" data-longitud="{{data.lec.leco_longitud}}" data-distancia="{{data.lec.leco_distancia}}"></div>
    </fieldset-->
    </div>
<!--/div-->
</div>
{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
    <br><small>{{data.debug}}</small>
    {{/if}}
</div>
{{/if}}
<script src="js/qr_detalle.js" type="text/javascript" charset="utf-8"></script>
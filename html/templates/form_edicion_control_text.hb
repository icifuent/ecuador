<div class="row form-edicion-container-control" id="control_{{foit_id}}" data-type="text" data-properties="{{JSON properties}}"
  data-options="{{JSON options}}" data-indf-inventariable="{{indf_inventariable}}" data-indf-genero="{{indf_genero}}" data-indf-especie="{{indf_especie}}"
  data-indf-campo="{{indf_campo}}" data-indf-codigo="{{indf_codigo}}" data-aggregated="{{aggregated}}">
  <div class="col-md-8 form-group">
    <label class="control-label">{{foit_nombre}}</label>
    <input autocomplete="off" type="text" class="form-control input-sm" name="{{field}}">
  </div>
  <div class="col-md-4 menu">
    <ul class="nav nav-pills pull-right">
      <li role="presentation">
        <a class="btn btn-xs form-edicion-remove" role="button" data-id="control_{{foit_id}}">Eliminar</a>
      </li>
      <li role="presentation">
        <a class="btn btn-xs form-edicion-properties" role="button" data-id="control_{{foit_id}}">Propiedades</a>
      </li>
    </ul>
  </div>
</div>
<div class="panel panel-default">
    <div class="panel-body siom-defaMovil-lista" style="overflow:auto !important; height: 500px;">
        {{#defaM}}
        <div class="row">
            <div class="col-md-2 text-center">
                <div class="row">
                    <span class="id">Falla Nº : {{defa_id}}</span>
                </div>
                <div class="row">
                    <span class="label label-{{defaColorStatus defa_estado}} status">{{defa_estado}}</span>
                </div>
            </div>
            <div class="col-md-7">
                <i class="glyphicon glyphicon-user"></i>&nbsp;Usuario:
                <strong>{{usua_nombre}}</strong>
                <br>
                <!--div class="info_qr">{{qr_prioridad}}</div-->
                Fecha de reporte de falla:&nbsp;
                <strong>{{defa_fecha_creacion}}</strong>
                <br>
                <!--label class="col-sm-2 siom-value"></label-->
                Empresa:&nbsp;
                <strong>{{empr_nombre}}</strong>
                {{#if orse_id}}
                <br>
                Orden de servicio asignada:&nbsp;
                <strong>{{orse_id}}</strong>&nbsp;
                <a href="#/os/detalle/{{orse_id}}">VER</a>
                {{else}}
                <br>
                Orden de servicio sin generar
                {{/if}}
            </div>
            <div class="col-md-3 text-right">
                <div class="dropdown">
                    <a href="#/defaMovil/detalle/{{defa_id}}">Ver detalle</a>
                </div>
            </div>
        </div>
        <hr> {{/defaM}}
    </div>
    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                {{#if status}}
                <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..." data-processing-text="Generando..."
                    data-filters="{{JSON filtros}}"></button>
                {{/if}}
            </div>
            <div class="col-md-8 text-right">
                <div class="pagination-info">
                    Página {{pagina}}/{{paginas}} ({{total}} registros)
                </div>
                <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
                <!--<button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-download-alt"></i></button>-->
            </div>
        </div>
    </div>
</div>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/defaMovil_bandeja_lista.js" type="text/javascript" charset="utf-8"></script>
<div class="panel panel-default">
    <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
        {{#data}}
        <div class="os-crear" id="{{peri_id}}">
            <div class="estado text-center">
                <span class="glyphicon glyphicon-stop status status-success" aria-hidden="true"></span>
            </div>

            <div class="acciones text-right">
                <button type="button" class="btn btn-default btn-xs" data-peri_id="{{peri_id}}" data-data="{{JSON .}}" id="BotonPeriodicidadEditar">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </button>

            
            </div>

            <div class="info text-left">
                <h4>{{peri_nombre}} </h4>
                <div class="info_adicional">Orden: <strong>{{peri_orden}}</strong><small> Meses : {{peri_meses}}</small></div>
            </div>

            <hr>
        </div>

        {{/data}}
    </div>

    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                <div class="pagination-info">
                    <small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
                </div>
            </div>
            <div class="col-md-8 text-right">
                <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
            </div>
        </div>
    </div>


</div>

<script src="js/core_periodicidad_lista.js" type="text/javascript" charset="utf-8"></script>

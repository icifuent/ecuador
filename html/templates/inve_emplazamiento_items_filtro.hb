<form class="form-horizontal siom-form-tiny" role="form" action="#/inve/emplazamiento/{{empl_id}}/items/filtro" method="POST"
  id="siom-form-items-filtro">


  <div class="form-group">
    <label for="nombre" class="col-sm-5 control-label">Codigo</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="nombre" name="inel_codigo">
    </div>
  </div>

  <div class="form-group">
    <label for="nemonico" class="col-sm-5 control-label">Nombre</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="nemonico" name="inel_nombre">
    </div>
  </div>

  <div class="form-group">
    <label for="direccion" class="col-sm-5 control-label">Ubicacion</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" name="inel_ubicacion">
    </div>
  </div>

  <div class="form-group">
    <label for="direccion" class="col-sm-5 control-label">Descripcion</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" name="inel_descripcion">
    </div>
  </div>

  <div class="form-group">
    <label for="zona" class="col-sm-5 control-label">Estado</label>
    <div class="col-sm-7">
      <select name="inel_estado" class="selectpicker" data-width="100%">
        <option value="">TODOS</option>
        {{#estados}}
        <option value="{{.}}">{{.}}</option>
        {{/estados}}
      </select>
    </div>
  </div>
</form>
<!--
          <div class="text-center">
            <button type="submit" class="btn btn-primary" data-loading-text="Filtrando..." >Filtrar</button>
          </div>
          -->
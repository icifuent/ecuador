<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Mantención</h5>
  </div>
  <div class="col-md-7 text-right">
    {{#siomChequearPerfil "#/mnt/crear"}}
    <a class="btn btn-primary btn-sm" href="#/mnt/crear">
      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear MPS
    </a>
    {{/siomChequearPerfil}} {{#siomChequearPerfil "#/mnt/reportes_mnt"}}
    <a class="btn btn-primary btn-sm" href="#/mnt/reportes_mnt">
      <span class="glyphicon glyphicon" aria-hidden="true"></span> Reportes MNT
    </a>
    {{/siomChequearPerfil}}

  </div>
</div>

{{#if data.status}}
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/mnt/bandeja/filtro" method="POST" id="siom-form-mnt-bandeja">

          <div class="form-group">
            <div class="col-sm-5">
              <button type="button" class="btn btn-primary btn-default" id="LimpiarMntBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
            </div>
            <div class="col-sm-7">
              <button type="button" class="btn btn-primary btn-default" id="BuscarMntBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Buscar
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="id" class="col-sm-5 control-label">Nº MNT</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="mant_id" name="mant_id" value="{{data.mant_id}}">
            </div>
          </div>

          <div class="form-group">
            <label for="periodos" class="col-sm-5 control-label">Periodicidad</label>
            <div class="col-sm-7">
              <select id="periodos" class="selectpicker" data-width="100%" title='Todas' name="peri_id" data-value="{{data.peri_id}}">
                <option value="">Todos</option>
                {{#data.periodos}}
                <option value="{{peri_id}}">{{peri_nombre}}</option>
                {{/data.periodos}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="fecha_inicio" class="col-sm-5 control-label">Fecha Inicio Programada</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_inicio" class="form-control" size="14" type="text" data-date-autoclose="true" name="mant_fecha_programada_inicio"
                value="{{#if data.mant_fecha_programada_inicio}} {{formatDate data.mant_fecha_programada_inicio '%d-%m-%Y'}} {{/if}}"
              />
            </div>
          </div>
          <div class="form-group">
            <label for="fecha_fin" class="col-sm-5 control-label">Fecha Fin Programada</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_fin" class="form-control" size="14" type="text" data-date-autoclose="true" name="mant_fecha_programada_termino"
                value="{{#if data.mant_fecha_programada_termino}}{{formatDate data.mant_fecha_programada_termino '%d-%m-%Y'}}{{/if}}"
              />
            </div>
          </div>
          <div class="form-group">
            <label for="estado" class="col-sm-5 control-label">Estado</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" multiple title='Todos' name="mant_estado" data-value="{{JSON data.mant_estado}}">
                <option value="CREADA">CREADA</option>
                <option value="ASIGNANDO">ASIGNANDO</option>
                <option value="ASIGNADA">ASIGNADA</option>
                <option value="EJECUTANDO">EJECUTANDO</option>
                <option value="VALIDANDO">VALIDANDO</option>
                <option value="APROBADA">APROBADA</option>
                <option value="RECHAZADA">RECHAZADA</option>
                <option value="FINALIZADA">FINALIZADA</option>
                <option value="ANULADA">ANULADA</option>
                <option value="NO REALIZADO">NO REALIZADO</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="estado" class="col-sm-5 control-label">Empresa Responsable</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" title='Todas' name="mant_responsable" data-value="{{data.mant_responsable}}">
                <option value="">Todas</option>
                <option value="MOVISTAR">MOVISTAR</option>
                <option value="CONTRATISTA">CONTRATISTA</option>
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label class="col-sm-5 control-label">Emplazamiento</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="empl_nombre" name="empl_nombre" value="{{data.empl_nombre}}">
            </div>
          </div>
          <div class="form-group">
            <label for="clasificaciones" class="col-sm-5 control-label">Clasificación</label>
            <div class="col-sm-7">
              <select id="clasificaciones" class="selectpicker" data-width="100%" title='Todas' name="clas_id" data-value="{{data.clas_id}}">
                <option value="">Todas</option>
                {{#data.clasificaciones}}
                <option value="{{clas_id}}">{{clas_nombre}}</option>
                {{/data.clasificaciones}}
              </select>
            </div>
          </div>


          <div class="form-group">
            <label for="macrositio" class="col-sm-5 control-label">Macrositio</label>
            <div class="col-sm-7">
              <select id="macrositio" class="selectpicker" data-width="100%" title='Todas' name="empl_macrositio" data-value="{{data.empl_macrositio}}">
                <option value="">Todos</option>
                <option value="0">NO</option>
                <option value="1">SI</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="subtel" class="col-sm-5 control-label">Subtel</label>
            <div class="col-sm-7">
              <select id="subtel" class="selectpicker" data-width="100%" title='Todas' name="empl_subtel" data-value="{{data.empl_subtel}}">
                <option value="">Todos</option>
                <option value="0">NO</option>
                <option value="1">SI</option>
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="especialidad" class="col-sm-5 control-label">Especialidad</label>
            <div class="col-sm-7">
              <select id="especialidad" class="selectpicker" data-width="100%" title='Todas' name="espe_id" data-value="{{data.espe_id}}">
                <option value="">Todas</option>
                {{#data.especialidades}}
                <option value="{{espe_id}}">{{espe_nombre}}</option>
                {{/data.especialidades}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="tecnicos" class="col-sm-5 control-label">Tecnico Asignado</label>
            <div class="col-sm-7">
              <select id="tecnicos" class="selectpicker" data-width="100%" title='Todas' name="usua_id" data-value="{{data.usua_id}}">
                <option value="">Todos</option>
                {{#data.tecnicos}}
                <option value="{{usua_id}}">{{usua_nombre}}</option>
                {{/data.tecnicos}}
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="subgerencia" class="col-sm-5 control-label">Sub Gerencia</label>
            <div class="col-sm-7">
              <select id="subgerencia" class="selectpicker" data-width="100%" title='Todas' name="subg_id" data-value="{{data.subg_id}}">
                <option value="">Todas</option>
                {{#data.sub_gerencias}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.sub_gerencias}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="zona" class="col-sm-5 control-label">Zona</label>
            <div class="col-sm-7">
              <select id="zona" class="selectpicker" data-width="100%" title='Todas' name="zona_id" data-value="{{data.zona_id}}">
                <option value="">Todas</option>
                {{#data.zonas}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.zonas}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="region" class="col-sm-5 control-label">Región</label>
            <div class="col-sm-7">
              <select id="region" class="selectpicker" data-width="100%" title='Todas' name="regi_id" data-value="{{data.regi_id}}">
                <option value="">Todas</option>
                {{#data.regiones}}
                <option value="{{regi_id}}">{{regi_nombre}}</option>
                {{/data.regiones}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="cluster" class="col-sm-5 control-label">Cluster</label>
            <div class="col-sm-7">
              <select id="cluster" class="selectpicker" data-width="100%" title='Todas' name="clus_id" data-value="{{data.clus_id}}">
                <option value="">Todos</option>
                {{#data.clusters}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.clusters}}
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="cluster" class="col-sm-5 control-label">Usuario Creador</label>
            <div class="col-sm-7">
              <select name="usua_creador" class="selectpicker" data-width="100%" title='Todos' data-value="{{data.usua_creador}}" data-container="body">
                <option value="">Todos</option>
                {{#data.usuarios_creador}}
                <option value="{{usua_id}}">{{usua_nombre}}</option>
                {{/data.usuarios_creador}}
              </select>
            </div>
          </div>


          <div class="form-group">
            <label for="zona_movistar" class="col-sm-5 control-label">Zona Movistar</label>
            <div class="col-sm-7">
              <select id="zona" class="selectpicker" data-width="100%" title='Todas' name="zona_movistar_id" data-value="{{data.zona_movistar_id}}">
                <option value="">Todas</option>
                {{#data.zona_movistar}}
                <option value="{{zona_movistar_id}}">{{zona_movistar_nombre}}</option>
                {{/data.zona_movistar}}
              </select>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
  <div class="col-md-9" id="mnt-bandeja-lista">
  </div>
</div>

<div class="modal fade" id="ModalSolicitud">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Solicitud</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" rows="3" placeholder="Ingrese razón de solicitud"></textarea>
        <small>Se le enviará una notificación al encargado respectivo para autorizar solicitud.</small>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-loading-text="Solicitando..." data-complete-text="Solicitado!">Confirmar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="ModalSolicitudCambioFecha">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Solicitud de cambio de fecha programada</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal siom-form-tiny" role="form">
          <div class="form-group">
            <label for="fecha_programada" class="col-sm-5 control-label">Fecha inicio</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_programada" class="form-control" size="14" type="text" data-date-autoclose="true" name="mant_fecha_programada"
                value="" />
            </div>
          </div>

          <div class="form-group">
            <label for="fecha_inicio" class="col-sm-5 control-label">Razón de solicitud</label>
            <div class="col-sm-7">
              <textarea class="form-control" rows="3" placeholder="Ingrese razón de solicitud"></textarea>
              <small class="text-right">Se le enviará una notificación al encargado respectivo para autorizar solicitud.</small>
            </div>
          </div>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-loading-text="Solicitando..." data-complete-text="Solicitado!">Confirmar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}} {{#if data.debug}}
  <br>
  <small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mnt_bandeja.js" type="text/javascript" charset="utf-8"></script>
<div class="panel panel-default">
    <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
        {{#data}}
        <div class="os-crear" id="{{cont_id}}">
            <div class="estado text-center">
                <span class="glyphicon glyphicon-stop status status-{{estadoColorStatus cont_estado}}" aria-hidden="true"></span>
            </div>

            <div class="acciones text-right">
                <button type="button" class="btn btn-default btn-xs" data-cont_id_form="{{cont_id}}" data-data="{{JSON .}}" id="BotonContratoEditar">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                </button>

                {{#is cont_estado 'ACTIVO'}}
                <button type="button" id="BotonContratoCambiarEstado" class="btn btn-default btn-xs"  data-id="{{cont_id}}" data-nombre="{{cont_nombre}}" data-estado="{{cont_estado}}" >
                    <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                </button>
                {{else}}
                <button type="button" id="BotonContratoCambiarEstado" class="btn btn-default btn-xs"  data-id="{{cont_id}}" data-nombre="{{cont_nombre}}" data-estado="{{cont_estado}}" >
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </button>
                {{/is}}
            </div>

            <div class="info text-left">
                <h4>{{cont_nombre}} <small>{{cont_alias}}</small></h4>
                <div class="info_adicional">Descripcion: <strong>{{cont_descripcion}}</strong></div>
            </div>

            <hr>
        </div>

        {{/data}}
    </div>

    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                <div class="pagination-info">
                    <small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
                </div>
            </div>
            <div class="col-md-8 text-right">
                <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
            </div>
        </div>
    </div>


</div>

<script src="js/core_contrato_lista.js" type="text/javascript" charset="utf-8"></script>

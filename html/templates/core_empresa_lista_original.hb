
<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" >
  	{{#data}}
  	<div class="os" id="{{empr_id}}">
	  <div class="estado text-center">
	  	<span class="id">{{empr_rut}}</span>
	 </div>

	  <div class="acciones text-right">
        <button type="button" class="btn btn-default btn-xs editar"  data-id="{{empr_id}}">
          <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
        </button>

        <button type="button" class="btn btn-default btn-xs eliminar"  data-id="{{empr_id}}">
          <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
        </button>
	  </div>

	  <div class="info text-left">
	  	<h4>{{empr_alias}} <small>{{empr_nombre}}</small></h4>
	  	<div class="info_adicional">Dirección: <strong>{{empr_direccion}}</strong>, Giro: <strong>{{empr_giro}}</strong>, Estado: <strong>{{empr_estado}}</strong></div>
	  </div>

		<hr>

	</div>

	{{/data}}
  </div>

   <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	<div class="pagination-info">
	    	<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
	    </div>
	  </div>
	  <div class="col-md-8 text-right">
	    <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>


</div>


<script src="js/core_empresa_lista.js" type="text/javascript" charset="utf-8"></script>


<div class="modal fade" id="EditModal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-sm-12">
		      	<form class="form-horizontal" role="form" id="EditModalForm">
		      		<input type="hidden" id="empr_id">
      				<input type="hidden" id="usua_creador">
		      		 <fieldset class="siom-fieldset"  style="margin-top:20px;">
    					<legend>Empresa</legend>

						<div class="form-group">
							<label for="empr_fecha_creacion" class="col-sm-2 siom-label">Fecha creación</label>
							<input type="text" class="col-sm-2 siom-value" id="empr_fecha_creacion" disabled="true" readonly>
						</div>

						<div class="form-group">
							<label for="usua_creador_nombre" class="col-sm-2 siom-label">Usuario creador</label>
							<input type="text" class="col-sm-2 siom-value" id="usua_creador_nombre" disabled="true" readonly>
						</div>

				      	<div class="form-group">
							<label for="empr_rut" class="col-sm-2 siom-label">Rut</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_rut" placeholder="Rut" value="">
						</div>

						<div class="form-group">
							<label for="empr_nombre" class="col-sm-2 siom-label">Nombre</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_nombre" placeholder="Nombre" value="">
						</div>

						<div class="form-group">
							<label for="empr_alias" class="col-sm-2 siom-label">Alias</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_alias" placeholder="Alias" value="">
						</div>

						<div class="form-group">
							<label for="empr_giro" class="col-sm-2 siom-label">Giro</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_giro" placeholder="Giro" value="">
						</div>

				      	<div class="form-group">
							<label for="empr_direccion" class="col-sm-2 siom-label">Dirección</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_direccion" placeholder="Dirección" value="">
						</div>

						<div class="form-group">
							<label for="comu_id" class="col-sm-2 siom-label">Comuna</label>
								<select id="comu_id" class="selectpicker"  title='Seleccione una comuna...'>
								{{#data.comunas}}
						            <option value="{{comu_id}}">{{comu_nombre}}</option>
						         {{/data.comunas}}
						         </select>
						</div>

						<div class="form-group">
							<label for="empr_contacto_nombre" class="col-sm-2 siom-label">Nombre contacto</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_contacto_nombre" placeholder="Nombre contacto" value="">
						</div>

						<div class="form-group">
							<label for="empr_contacto_direccion" class="col-sm-2 siom-label">Dirección</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_contacto_direccion" placeholder="Dirección" value="">
						</div>

						<div class="form-group">
							<label for="empr_contacto_email" class="col-sm-2 siom-label">Email</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_contacto_email" placeholder="Email" value="">
						</div>

						<div class="form-group">
							<label for="empr_contacto_telefono_fijo" class="col-sm-2 siom-label">Teléfono fijo</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_contacto_telefono_fijo" placeholder="Teléfono fijo" value="">
						</div>

						<div class="form-group">
							<label for="empr_contacto_telefono_movil" class="col-sm-2 siom-label">Teléfono móvil</label>
								<input type="text" class="col-sm-2 siom-value" id="empr_contacto_telefono_movil" placeholder="Teléfono  móvil">
						</div>

						<div class="form-group">
							<label for="empr_estado" class="col-sm-2 siom-label">Estado</label>
								<select id="empr_estado" class="selectpicker" title='Seleccione un estado...'>
									<option value="ACTIVO">ACTIVO</option>
									<option value="NOACTIVO">NO ACTIVO</option>
						         </select>
						</div>
						<div class="form-group">
							<label for="empr_observacion" class="col-sm-2 siom-label">Observación</label>
								<textarea class="col-sm-2 siom-value" id="empr_observacion" placeholder="Observación"></textarea>
						</div>


					</fieldset>

				</form>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="SaveBtn" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
      </div>
   </div>
  </div>
</div>
</div>

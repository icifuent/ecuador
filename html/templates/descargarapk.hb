<div class="row">
    <div class="col-sm-4 col-md-offset-4" id="login">
        <div class="account-wall">
            <img src="img/logo-navbar.png" id="logo-siom">

            <form>
                <a class="btn btn btn-success btn-block" href="/apk/SIOM_productionEcuador-1.5.4.apk">
                    <span class="glyphicon glyphicon" aria-hidden="true"></span> Descargar app Producción Ecuador - 1.5.4
                </a>
            </form>
            <p>La versión 1.5.4, trae como mejora, el poder cerrar los formularios en la primera página, esto con movito de ahorro de tiempo para técnico cuando el formulario no aplique el llenado de data.</p>
        </div> 
        <div class="row" id="login-footer">
                <div class="col-md-3">
                    <img src="img/logo-telefonica.png" id="logo-telefonica">
                </div>
                <div class="col-md-9">
                    <p>D. Red/ G. Gestion de Redes y Servicios/ SG. Operación de Red</p>
                </div>
            </div>       
    </div>
</div>


<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Crear inspección</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>


<div id="wizard" data-initialize="wizard" class="wizard complete">
  <ul class="steps">
    <li data-step="1" class="active">
      <span class="badge">1</span>Seleccionar emplazamiento
      <span class="chevron"></span>
    </li>
    <li data-step="2">
      <span class="badge">2</span>Información de Inspección
      <span class="chevron"></span>
    </li>
  </ul>
  <div class="actions">
  </div>
  <div class="step-content">
    <div class="step-pane sample-pane active" data-step="1">
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <form class="form-horizontal siom-form-tiny" role="form" action="#/insp/crear/filtro" method="POST">
                {{{loadTemplate "template_filtro_emplazamiento" data}}}
              </form>

            </div>
          </div>
        </div>
        <div class="col-md-9" id="insp-crear-lista">
        </div>

      </div>
    </div>
    <div class="step-pane sample-pane" data-step="2">
      <fieldset class="siom-fieldset" id="siom-emplazamiento-info">
        <legend>Emplazamiento</legend>
        <div id="siom-empl-info"></div>
      </fieldset>

      <fieldset class="siom-fieldset" style="margin-top:10px;">
        <legend>Inspección</legend>
        <form class="form-horizontal siom-form-tiny" role="form" action="#/insp/crear" method="POST" id="form_crear">

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="fecha_solicitud" class="col-sm-4 control-label">Fecha inspección</label>
                <div class="col-sm-8">
                  <input  type="hidden" name="insp_fecha_solicitud" value="">
                  <input autocomplete="off" id="fecha_solicitud" class="form-control" size="14" type="text" data-date-autoclose="true" name="insp_fecha_solicitud_fecha"
                    placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />
                </div>
              </div>

              <div class="form-group">
                <label for="tipo_inspecion" class="col-sm-4 control-label">Tipo inspección</label>
                <div class="col-sm-8">
                  <select id="tipo_inspecion" class="selectpicker" data-width="100%" name="insp_tipo" title="Seleccione Inspeción" data-container="body">
                    <option value=""></option>
                    {{#data.tipos}}
                    <option value="{{.}}">{{.}}</option>
                    {{/data.tipos}}
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="mant_id" class="col-sm-4 control-label">ID Mantenimiento</label>
                <div class="col-sm-8">
                  <input autocomplete="off" type="text" class="form-control" id="mant_id" name="mant_id" disabled="true" />
                </div>
              </div>

              <div class="form-group">
                <label for="especialidad" class="col-sm-4 control-label">Especialidad</label>
                <div class="col-sm-8">
                  <select id="especialidad" class="selectpicker" data-width="100%" name="espe_id" title="Seleccione especialidad" data-container="body">
                    <option value=""></option>
                    {{#data.especialidad}}
                    <option value="{{espe_id}}">{{espe_nombre}}</option>
                    {{/data.especialidad}}
                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="empresa" class="col-sm-4 control-label">Empresa</label>
                <div class="col-sm-8">
                  <select id="empresa" class="selectpicker" data-width="100%" name="empr_id" title="Seleccione empresa" data-container="body">
                    <option value=""></option>
                    {{#data.empresa}}
                    <option value="{{empr_id}}">{{empr_nombre}}</option>
                    {{/data.empresa}}
                  </select>
                </div>
              </div>



              <div class="form-group">
                <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
                <div class="col-sm-8">
                  <textarea class="form-control" name="insp_descripcion" rows="5"></textarea>
                </div>
              </div>

            </div>
            <div class="col-md-6">
              <label class="control-label">Documentación</label>
              <br>
              <span class="btn btn-default btn-sm btn-file">
                Agregar archivo(s)
                <input autocomplete="off" type="file" name="archivos[]" id="archivo-0" data-id="0">
              </span>
              <ul id="listado-archivos" class="list-group siom-file-list">

              </ul>
            </div>
          </div>

          <hr>
          <div class="row text-center">
            <div class="col-md-12">
              <input  type="hidden" name="empl_id" id="empl_id">
              <a href="#/insp/bandeja" class="btn btn-default">Cancelar</a>
              <button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="submit_crear">Guardar</button>
            </div>
          </div>
        </form>
      </fieldset>

    </div>
  </div>
</div>


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/insp_crear.js" type="text/javascript" charset="utf-8"></script>
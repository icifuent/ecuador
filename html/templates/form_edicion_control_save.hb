<div class="row form-edicion-container-control" id="control_{{foit_id}}" data-type="save" data-properties="{{JSON properties}}" data-options="{{JSON options}}">
  <div class="col-md-8 form-group text-center">
      <button type="button" class="btn btn-danger btn-control">Cancelar</button>
      <button type="button" class="btn btn-success btn-control">Guardar</button>
  </div>
  <div class="col-md-4 menu menu-btn-control">
      <ul class="nav nav-pills pull-right">
        <li role="presentation"><a class="btn btn-xs form-edicion-remove" role="button" data-id="control_{{foit_id}}">Eliminar</a></li>
        <li role="presentation"><a class="btn btn-xs form-edicion-properties" role="button" data-id="control_{{foit_id}}">Propiedades</a></li>
      </ul>
  </div>
</div>
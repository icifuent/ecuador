<div class="panel panel-default">
  <div class="panel-body siom-qr-lista siom-lista-full-height" >
  	{{#qrs}}
  	<div class="qr">
	  <div class="estado text-center">
	  	<span class="id"> QR Nº  : {{leco_id}}</span>
        <i class="glyphicon glyphicon-qrcode"></i>		
	 </div>
	 
	  <div class="acciones text-right">
	  	<div class="dropdown">
	  			<a href="#/qr/detalle/{{leco_id}}" class="detalle">Ver detalle</a>
	  	  	
		</div>
		
	  </div>

	   <div class="info_adicional"><i class="glyphicon glyphicon-user"></i>&nbsp;Usuario: <strong>{{usua_nombre}}</strong><br>
	  			<!--div class="info_qr">{{qr_prioridad}}</div-->
				FECHA DE CAPTURA QR:&nbsp;<strong>{{leco_fecha}}</strong>
                <!--label class="col-sm-2 siom-value"></label-->
        		
	   </div>
	</div> 

	<hr>
	{{/qrs}}
  </div>
  
    <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	{{#if status}}
		 	<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}"></button>
		{{/if}}

	  </div>
	  <div class="col-md-8 text-right">
	  	<div class="pagination-info">
	    	Página {{pagina}}/{{paginas}} ({{total}} registros)
	    </div>
	    <div id="pagination" name="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>
</div>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/qr_bandeja_lista.js" type="text/javascript" charset="utf-8"></script>


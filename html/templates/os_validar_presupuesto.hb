<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Validación de presupuesto</h5>
 </div>
 <div class="col-md-7">

 </div>
</div>

{{#if status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Informes</legend>
  {{#each data.informes}}
    <div class="row">
      <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
      <label class="col-sm-2 siom-value">{{info_fecha_creacion}}</label>
      <label class="col-sm-2 siom-label">CREADO POR</label>
      <label class="col-sm-3 siom-value">{{usua_creador}}</label>
      <label class="col-sm-1 siom-label">ESTADO</label>
      <label class="col-sm-1 siom-value">{{info_estado}}</label>
      <label class="col-sm-1 siom-link">
        <a href="#/os/informe/{{../data.os.orse_id}}/ver/{{info_id}}" target="blank"> VER </a>
      </label>
    </div>

    {{#isnt info_estado 'SINVALIDAR'}}
    <div class="row">
      <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
      <label class="col-sm-2 siom-value">{{info_fecha_validacion}}</label>
      <label class="col-sm-2 siom-label">VALIDADO POR</label>
      <label class="col-sm-2 siom-value">{{usua_validador}}</label>
    </div>
    {{/isnt}}

    {{#gt ../data.informes.length 1}}
    <hr>
    {{/gt}}
  {{else}}
  <p class="siom-no-data">Sin informes</p>
  {{/each}}
</fieldset>

<fieldset class="siom-fieldset">
   <legend>Validación de presupuesto</legend>
   {{#if data.presupuesto}}
	<form class="form form-horizontal" role="form" action="#/os/presupuesto/{{data.orse_id}}/validar/{{data.pres_id}}" method="POST" id="siom-form-validar-presupuesto">
		<div class="row siom-os-presupuesto-header">
	        <div class="col-sm-4">Actividad</div>
	        <div class="col-sm-2 text-center">Categoria</div>
	        <div class="col-sm-1 text-center">Valor unitario</div>
	        <div class="col-sm-1 text-center">Cantidad</div>
	        <div class="col-sm-1 text-center">Subtotal</div>
	        <div class="col-sm-3 text-center"></div>
	      </div>

		<div class="siom-os-presupuesto-lista">	
		{{#each data.presupuesto}}
			<div class="siom-os-presupuesto-lista-validar {{#is prit_estado 'VALIDADO'}}bg-success{{else}}bg-danger{{/is}}" id="{{prit_id}}">
    		   	<div class="row">
    		      <div class="col-sm-4">
                     <label class="nombre">{{lpit_nombre}}</label><br>
	  		         <label class="comentario">{{lpgr_nombre}}</label>
			      </div>
			      <div class="col-sm-2 text-center categoria">
			          {{lpgc_nombre}}
			      </div>
                  <div class="col-sm-1 text-center precio">
                      {{lpip_precio}} 
                  </div>
			      <div class="col-sm-1 text-center cantidad">
			          {{prit_cantidad}}
			      </div>
			      <div class="col-sm-1 text-center subtotal">
			          {{presCalcSubtotal lpip_precio prit_cantidad}} 
			      </div>
			      <div class="col-sm-3 text-center validacion">
			      	<div class="check">
			      		<input type="hidden" name="prit_id" value="{{prit_id}}">
			      		<input type="hidden" name="prit_estado" value="{{prit_estado}}">
			      		<input type="hidden" name="prit_comentario" value="{{prit_comentario}}">
			      		<input type="hidden" name="subtotal" value="{{presCalcSubtotal lpip_precio prit_cantidad}} ">

			      		<input type="checkbox" value="VALIDADO" data-subtotal="{{presCalcSubtotal lpip_precio prit_cantidad}}" data-prit-id="{{prit_id}}" id="estado_{{prit_id}}" {{#is prit_estado 'VALIDADO'}}checked{{/is}}>
			      	</div>
			      	<div class="observacion"><textarea class="form-control" rows="1" id="observacion_{{prit_id}}">{{prit_comentario}}</textarea></div>						  	
			   	  </div>
		   		</div>
		   	</div>
		{{/each}}
		</div>
		<div class="row">
			<div class="col-sm-7 text-left">
				<div class="col-sm-7 text-left">
				<!--<button type="button" class="btn btn-default btn-xs siom-presupuesto-exportar" id="download" data-loading-text="Descargando..."data-processing-text="Procesando..." data-pres-id="{{data.pres_id}}">
	              Descargar en Excel
	            </button> -->
			</div>
			</div>
			<div class="col-sm-1 text-right siom-os-presupuesto-total-validacion">
	        	TOTAL
	        </div>
	        <div class="col-sm-1 text-center siom-os-presupuesto-total-validacion" id="siom-os-presupuesto-total-validacion-valor">
	        	0
	        </div>
	        <div class="col-sm-3 text-right siom-os-presupuesto-estado-validacion" id="siom-os-presupuesto-estado-validacion" data-items="{{data.presupuesto.length}}">
				0 items aprobados de un total de {{data.presupuesto.length}}
			</div>
		</div>
		<hr>
		<div class="row text-center">
			<div class="col-md-12">
				<button type="button" id="submit_presupuesto" class="btn btn-primary" data-loading-text="Validando presupuesto..." autocomplete="off">Validar presupuesto</button>
			</div>
		</div>
	</form>
	{{else}}
		<div class="alert alert-warning" role="alert">
	  	<strong>Aviso</strong> ruta indicada no tiene un presupuesto definido.
		</div>
	{{/if}}
</fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}



<script src="js/os_validar_presupuesto.js" type="text/javascript" charset="utf-8"></script>


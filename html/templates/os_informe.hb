<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Informe orden de servicio</h5>
  </div>
  <div class="col-md-7">
  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Informe</legend>
  <form role="form" action="#/os/informe/{{data.orse_id}}" method="POST" id="siom-form-informe" class="form form-horizontal siom-form-tiny">
    <div class="row siom-informe-form">
      <div class="col-md-6 col-md-offset-2">
        <div class="form-group">
          <label class="col-sm-4 control-label">Fecha visita</label>
          <div class="col-sm-8">
            <div class="row">
              <div class="col-md-6">
                <input autocomplete="off" class="form-control datepicker" size="14" type="text" data-date-autoclose="true" name="info_fecha_visita"
                  placeholder="dd-mm-yyyy" />
              </div>
              <div class="col-md-6">
                <div class="input-group bootstrap-timepicker">
                  <input autocomplete="off" type="text" class="form-control timepicker" aria-describedby="addon" data-field="time" name="info_hora_visita"
                    data-modal-backdrop="true" placeholder="hh:mm">
                  <span class="input-group-addon" id="addon">
                    <i class="glyphicon glyphicon-time"></i>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
          <div class="col-sm-8">
            <textarea class="form-control" name="info_descripcion" rows="5"></textarea>
          </div>
        </div>

        <div class="form-group">
          <label for="archivo" class="col-sm-4 control-label">Archivo</label>
          <div class="col-sm-8">
            <input  id="files" type="file" class="form-control" multiple="true" name="archivo">
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="row text-center">
      <div class="col-md-12">
        <button type="submit" class="btn btn-primary" data-loading-text="Agregando..."  id="submit_informe">Agregar informe</button>
      </div>
    </div>
  </form>
</fieldset>
{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}} {{#if data.debug}}
  <br>
  <small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/os_informe.js" type="text/javascript" charset="utf-8"></script>
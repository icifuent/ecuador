<fieldset class="siom-fieldset">
<legend>Emplazamiento</legend>

<table width="100%">
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">NOMBRE</div>
        <div class="siom-value">{{empty empl_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">NEMONICO</div>
        <div class="siom-value">{{empty empl_nemonico}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">CLASIFICACIÓN</div>
        <div class="siom-value">{{empty clas_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">TECNOLOGIA</div>
        <div class="siom-value">{{empty tecn_nombre}}</div>
      </td>
  </tr>
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">DIRECCIÓN</div>
        <div class="siom-value">{{empty empl_direccion}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">COMUNA</div>
        <div class="siom-value">{{empty comu_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">REGIÓN</div>
        <div class="siom-value">{{empty regi_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ZONA</div>
        <div class="siom-value">{{empty zona_nombre}}</div>
      </td>
  </tr>
  <tr>
      <td width="20%">
        <div class="siom-label">MACROSITIO</div>
        <div class="siom-value">{{boolean empl_macrositio}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">SUBTEL</div>
        <div class="siom-value">{{boolean empl_subtel}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ESPACIO</div>
        <div class="siom-value">{{empty empl_espacio}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">TIPO ACCESO</div>
        <div class="siom-value">{{empty empl_tipo_acceso}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ACCESO</div>
        <div class="siom-value">{{empty empl_observacion_ingreso}}</div>
      </td>
      
  </tr>
  <tr>
      <td width="60%" colspan="3" valign="top">
        <div class="siom-label">OBSERVACIÓN</div>
        <div class="siom-value">{{empty empl_observacion}}</div>
      </td>
      <td width="20%" valign="top">
        {{#if gestor_responsable}}
        <div class="siom-label">GESTOR RESPONSABLE</div>
        <div class="siom-value">
          {{#each gestor_responsable}}
            {{.}}<br>
          {{/each}}
        </div>
        {{/if}}
      </td>
      <td width="20%" valign="top">
       {{#if despachador_responsable}} 
        <div class="siom-label">DESPACHADOR RESPONSABLE</div>
        <div class="siom-value">
          {{#each despachador_responsable}} 
            {{.}}<br>
          {{/each}}
        </div>
        {{/if}}
      </td>
  </tr>
</table>
 </fieldset>
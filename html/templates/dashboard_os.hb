<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Dashboard Orden de servicio</h5>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/dashboard/os" method="POST" id="siom-form-dashboard-os">

            <fieldset class="siom-fieldset" style="padding-bottom:20px; margin-top:0px;">
                <legend>Filtros</legend>

                <div class="col-md-3" id="FiltroEspecialidades">
                    <div>
                        <label>Tipo de Filtro</label>
                    </div>
                    <select class="selectpicker" data-width="100%" id="tipo_filtro" name="tipo_filtro" title="Tipo de Filtro" data-container="body">
                        <option value="mes">Mes</option>
                        <option value="fecha">Fecha</option>
                    </select>
                </div>

                <div class="col-md-3 filtro_fecha" style="display:none">
                    <div>
                        <label>Fecha de inicio</label>
                    </div>
                    <input autocomplete="off" id="orse_fecha_inicio" class="form-control" size="14" type="text" name="orse_fecha_inicio" value="{{#if data.orse_fecha_inicio}} {{formatDate data.orse_fecha_inicio '%d-%m-%Y'}} {{/if}}"
                    />
                </div>

                <div class="col-md-3 filtro_fecha" style="display:none">
                    <div>
                        <label>Fecha de término</label>
                    </div>
                    <input autocomplete="off" id="orse_fecha_termino" class="form-control" size="14" type="text" name="orse_fecha_termino" value="{{#if data.orse_fecha_termino}} {{formatDate data.orse_fecha_termino '%d-%m-%Y'}} {{/if}}"
                    />
                </div>

                <div class="col-md-6 filtro_mes" style="display:block">
                    <div>
                        <label>Mes</label>
                    </div>
                    <input autocomplete="off" id="mes" class="form-control" type="text" data-date-autoclose="true" value="" />
                    <input autocomplete="off" id="orse_fecha_mes" name="orse_fecha_mes" type="hidden" value="{{month}}" />
                    <input autocomplete="off" id="orse_fecha_anio" name="orse_fecha_anio" type="hidden" value="{{year}}" />
                    <input  type="hidden" id="cont_id" name="cont_id" value="1">
                </div>

                <div class="col-md-2" id="FiltroEspecialidades">
                    <div>
                        <label></label>
                    </div>
                    <input  id="BotonFiltrar" type="button" class="btn btn-primary" value="Filtrar" />
                </div>


            </fieldset>
        </form>
    </div>
    <div class="col-md-6">
        <div class="siom-chart-title">
            <table width="70%">
                <tbody>
                    <tr>
                        <td>
                            <div class="input-color" align="left">
                                <p>APROBADAS</p>
                                <div class="color-box" style="background-color: #22B24C;"></div>
                            </div>
                        </td>
                        <td>

                            <div class="input-color" align="left">
                                <p>PROGRAMADA {{#is data.cont_id '1'}}EZENTIS{{/is}} {{#is data.cont_id '17'}}EZENTIS{{/is}}
                                    {{#is data.cont_id '10'}}EZENTIS{{/is}} {{#is data.cont_id '18'}}ISS{{/is}}</p>
                                <div class="color-box" style="background-color: #EF5BE8;"></div>
                            </div>
                        </td>

                        <td>
                            <div class="input-color" align="left">
                                <p>GASTOS</p>
                                <div class="color-box" style="background-color: #FF0000;"></div>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="input-color" align="left">
                                <p>PROGRAMADA MOVISTAR</p>
                                <div class="color-box" style="background-color: #2A54E3;"></div>
                            </div>
                        </td>
                        <td>
                            <div class="input-color" align="left">
                                <p>RECHAZADAS</p>
                                <div class="color-box" style="background-color: #6c6b73;"></div>
                            </div>
                        </td>

                        <td>

                        </td>
                        <td></td>


                    </tr>
                    <tr>

                        <td></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>
        </div>
        <table class="dashboard-table">
        </table>
    </div>

</div>


<br/>

<div id="dashboard_os_graficos"></div>

<script src="js/dashboard_os.js" type="text/javascript" charset="utf-8"></script>
<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Ver de informe</h5>
 </div>
 <div class="col-md-7 text-right">
 	{{#if status}}
 	<button type="button" class="btn btn-primary btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{data.informe.info_id}}">Descargar PDF</button>
   	{{/if}}
 </div>
</div>
{{#if status}}

  {{{loadTemplate "template_emplazamiento" data.emplazamiento}}}


 
  {{{loadTemplate "template_inspeccion" data.inspeccion}}}


<fieldset class="siom-fieldset"  style="margin-top:20px;" id="siom-ver-informe">
    <legend>Informe</legend>
    {{#if data.informe}}
    	<div class="row">
		  <label class="col-sm-2 siom-label">GENERADO POR</label>
		  <label class="col-sm-4 siom-value">{{data.informe.usua_nombre}}</label>
		  <label class="col-sm-2 siom-label">FECHA</label>
		  <label class="col-sm-4 siom-value">{{data.informe.info_fecha_creacion}}</label>
		</div>

		<hr>
		
		{{{loadTemplate "form_informe" data}}}

		<hr>

		<div class="row">
		 <div class="col-md-12 text-right">
		 	{{#if status}}
		 	<button type="button" class="btn btn-primary btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{data.informe.info_id}}">Descargar PDF</button>
		   	{{/if}}
		 </div>
		</div>

		<hr>

		<div class="row text-center">
	     	<div class="col-md-12">
	     		<a href="#/insp/detalle/{{insp_id}}" class="btn btn-default">Volver</a>
	     	</div>
	    </div>
    {{else}}
    	<div class="alert alert-warning" role="alert">
	  	<strong>Aviso</strong> ruta indicada no tiene un informe definido.
		</div>
    {{/if}}
</fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>    
<script src="js/template_ver_informe.js" type="text/javascript" charset="utf-8"></script>


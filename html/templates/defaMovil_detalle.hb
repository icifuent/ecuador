<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Detalle Reporte de Falla</h5>
    </div>
</div>
<!--inicio status-->
{{#if data.status}}
<div class="row" style="padding-bottom: 20px;">
    <form name="frmDefaMovil" method="POST" id="frmDefaMovil" action="#/os/crear/detallefalla">
        <div class="col-md-9">
            <fieldset class="siom-fieldset" style="margin-top:20px;">
                <legend>Resumen</legend>
                <!--inicio cabFalla-->
                <label id="usua_id" class="col-sm-2 siom-value" style="display:none;">{{data.cabFalla.usua_id}}</label>
                <div class="row">
                    <label class="col-md-3 siom-label">USUARIO</label>
                    <label id="usua_nombre" class="col-md-3 siom-value">{{data.cabFalla.usua_nombre}}</label>
                    <label class="col-md-3 siom-label">FECHA DETECCION DE LA FALLA</label>
                    <label id="defa_fecha_creacion" class="col-md-3 siom-value">{{data.cabFalla.defa_fecha_creacion}}</label>
                </div><br>
                <div class="row">
                    <label class="col-md-2 siom-label">LATITUD</label>
                    <label id="defa_latitud" class="col-md-2 siom-value">{{data.cabFalla.defa_latitud}}</label>
                    <label class="col-md-2 siom-label">LONGITUD</label>
                    <label id="defa_longitud" class="col-md-2 siom-value">{{data.cabFalla.defa_longitud}}</label>
                {{#each data.detFalla}}    
                    <label class="col-md-2 siom-label">N° DE FALLA</label>
                    <label class="col-md-2 siom-value">{{defa_id}}</label>
                    <input autocomplete="off" type="hidden" id="defa_id" name="defa_id" value="{{defa_id}}" />
                </div>
                <div class="row">
                    <label class="col-md-2 siom-label">DESCRIPCIÓN DE LA FALLA</label>
                    <label id="defa_descripcion" class="col-md-10 siom-value">{{defa_descripcion}}</label> 
                </div><br>
                <div class="row">
                    <label class="col-md-2 siom-label">ESPECIALIDAD</label>
                    <label id="defa_especialidad" class="col-md-2 siom-value">{{defa_especialidad}}</label>
                    <label class="col-md-2 siom-label">ALARMA</label>
                    <label id="defa_subespecialidad" class="col-md-2 siom-value">{{defa_subespecialidad}}</label>
                    <label class="col-md-2 siom-label">ESTADO</label>
                    <label id="defa_estado" class="col-md-2 siom-value">{{defa_estado}}</label>
                </div>
                {{/each}}
            </fieldset>
            <fieldset class="siom-fieldset" style="margin-top:20px;">
                <legend>Evidencias - Imágenes</legend>
                <div>
                    <table class="table table-responsive">
                        <tr class="row">
                            {{#each data.eviFalla}}
                            <td class="col-md-3">
                                <a href="{{repo_ruta}}" target="blank">
                                    <img class="img-responsive img-thumbnail" style="size: auto;" src="{{repo_ruta}}" />
                                </a>
                            </td>
                            {{else}}
                            <p class="col-md-3 siom-value">No existen imágenes como evidencia.</p>
                            {{/each}}
                        </tr>
                    </table>
                </div>
                <br>
            </fieldset>
            <div class="row">
                <div class="col-md-5">
                    <fieldset class="siom-fieldset" style="margin-top:20px;">
                        <legend>Evidencias - Videos</legend>
                        {{#each data.eviVideo}}
                        <video  style="width: 60%;" controls>
                            <source src="{{repo_ruta}}" type="video/mp4">
                            <embed src="{{repo_ruta}}" type="application/x-shockwave-flash" width="980" height="570" allowscriptaccess="always" allowfullscreen="true" autoplay="false"></embed>
                        </video>
                        {{else}}
                        <p class="col-md-3 siom-value">No existen videos como evidencia.</p>
                        {{/each}}
                    </fieldset>
                </div>
                <div class="col-md-7">
                    <fieldset class="siom-fieldset" style="margin-top:20px;">
                        <legend>Evidencias - Audio</legend>
                        {{#each data.eviAudio}}
                        <audio src="{{repo_ruta}}" preload="none" controls></audio>
                        {{else}}
                        <p class="col-md-3 siom-value">No existen audios como evidencia.</p>
                        {{/each}}
                    </fieldset>
                    <fieldset class="siom-fieldset" style="margin-top:20px;">
                        <div class="row">
                            {{#each data.detFalla}}
                            <input id="defa_estado" value="{{defa_estado}}" hidden/>
                            {{/each}}
                            <div id="divCancelarOS" class="col-md-4 text-center">
                                <a class="btn btn-danger" role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#ModalSolicitudCancelar"
                                    data-defa-id="{{defa_id}}" data-tipo="cancelar" data-title="Solicitud de cancelacion">Cancelar Reporte</a>
                            </div>
                            <div id="divDerivarOS" class="col-md-4 text-center">
                                <a class="btn btn-warning" role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#ModalSolicitudDerivar"
                                    data-defa-id="{{defa_id}}" data-tipo="derivar" data-title="Solicitud de derivacion">Derivar</a>
                            </div>
                            <div id="divCrearOs" class="col-md-4 text-center">
                                <a class="btn btn-primary" id="btnCrearOS" name="btnCrearOS">Crear OS</a>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <fieldset class="siom-fieldset" style="margin-top:20px;">
                <legend>Ubicación</legend>
                <div id="siom-os-detalle-mapa" class="siom-detalle-mapa" data-latitud="{{data.cabFalla.defa_latitud}}" data-longitud="{{data.cabFalla.defa_longitud
          }}" data-distancia="200" data-emplazamientos="{{JSON data.listEmp}}"></div>
            </fieldset>
            <fieldset class="siom-fieldset" style="margin-top:20px;">
                <legend>Seleccione Emplazamiento</legend>
                <div>
                    <table class="table table-hover">
                        <thead class="row">
                            <tr>
                                <th></th>
                                <th>Emplazamiento</th>
                                <th>Distancia a Reporte de Falla</th>
                            </tr>
                        </thead>
                        <tbody>
                            {{#each data.listEmp}}
                            <tr>
                                <td>
                                    <input autocomplete="off" class="siom-label" type="radio" id="empl_id" name="empl_id" value="{{empl_id}}">
                                </td>
                                <td>
                                    <label class="siom-value">{{empl_nombre}}</label>
                                </td>
                                <td>{{#calcularDistancia defa_latitud defa_longitud empl_latitud empl_longitud}}{{/calcularDistancia}}
                                    mts.
                                </td>
                            </tr>
                            {{else}}
                            <p class="siom-no-data">Sin listado de Emplazamiento</p>
                            {{/each}}
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
    </form>
</div>

<div class="modal fade" id="ModalSolicitudDerivar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Solicitud derivar</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <label>Seleccione contrato a derivar: </label>
                    </div>
                    <div class="col-md-7">
                        <select class="form-control" autocomplete="off">
                            <option>Seleccione contrato</option>
                            {{#each data.contratos}}
                            <option value="{{cont_nombre}}">{{cont_nombre}}</option>
                            {{/each}}
                        </select>
                    </div>
                </div><br>
                <div class="row text-center">
                    <div class="col-md-12">
                        <textarea class="form-control" rows="2" placeholder="Ingrese motivo de la derivacion" autocomplete="off"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnDerivarCancelar" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btnDerivar" class="btn btn-primary" data-defa-id="{{defa_id}}" data-loading-text="Derivando..."
                    data-complete-text="Derivar!">Derivar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalSolicitudCancelar">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Solicitud cancelar</h4>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col-md-12">
                        <textarea id="comentCancelar" class="form-control" rows="2" placeholder="Ingrese motivo de la cancelacion" autocomplete="off"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnCancelarCan" class="btn btn-default" data-dismiss="modal">Salir</button>
                <button type="button" id="btnCancelar" class="btn btn-primary" data-defa-id="{{defa_id}}" data-loading-text="Derivando..."
                    data-complete-text="Derivar!">Cancelar</button>
            </div>
        </div>
    </div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small>
    {{/if}}
</div>
<!--fin status-->
{{/if}}

<script src="js/defaMovil_detalle.js" type="text/javascript" charset="utf-8">
    /* $('#ubicacion').ap */
    $('#pagination').bootpag().on("page", function (event, num) {
        page = num;
        data = $.extend($('#pagination').data("filters"), { page: page });
        window.app.runRoute('post', '#/os/crear/filtro/' + page, data);
    });
    var empl_id = $('#empl_id').value();
    if (empl_id == null) {
        alert("Emplazamiento esta vacio");
    }
</script>
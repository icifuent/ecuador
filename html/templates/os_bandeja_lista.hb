<div class="panel panel-default" >
	<div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
		{{#ordenes}}
			<div class="os">
				<div class="estado text-center">
					<span class="id">Nº {{orse_id}}</span>
					<span class="label label-{{osColorStatus orse_fecha_solicitud orse_estado orse_tipo}} status">{{orse_estado}}</span>
				</div>
				<div class="acciones text-right" >
					<div class="dropdown">
						{{#siomChequearPerfil "#/os/detalle/*"}}
							<a href="#/os/detalle/{{orse_id}}" class="detalle">Ver detalle</a>
						{{/siomChequearPerfil}}
					
						{{#siomChequearPerfil "#/os/mas_opciones"}}
							<a id="more_options_{{orse_id}}" href="#" class="dropdown-toggle mas_opciones" data-toggle="dropdown"> Más opciones</a>
							<ul class="dropdown-menu" role="menu" aria-labelledby="more_options_{{orse_id}}">

							{{#siomChequearPerfil "#/os/editar/*"}}
								<li role="presentation"><a role="menuitem" tabindex="-1" href="#/os/editar/{{orse_id}}">Editar</a></li>
							{{/siomChequearPerfil}}

							{{#osChequearActiva orse_estado}}
								
								{{#osChequearActivaResponsable orse_responsable}}
									{{#siomChequearPerfil "#/os/asignacion/*"}}
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#/os/asignacion/{{orse_id}}">Asignar</a></li>
									{{/siomChequearPerfil}}
								{{/osChequearActivaResponsable}}

								{{#siomChequearPerfil "#/os/presupuesto/*"}}
									{{#osConPresupuesto orse_tipo}}	
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#/os/presupuesto/{{orse_id}}">Ingresar presupuesto</a></li>
										{{#osChequearActivaResponsable orse_responsable}}
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#/os/presupuesto/{{orse_id}}/modificar">Modificar presupuesto</a></li>
										{{/osChequearActivaResponsable}}
									{{/osConPresupuesto}}
								{{/siomChequearPerfil}}

								{{#is orse_solicitud_informe_web 'APROBADA'}}
									{{#siomChequearPerfil "#/os/visita/*"}}
										<li role="presentation"><a role="menuitem" tabindex="-1" href="#/os/visita/{{orse_id}}">Subir informe visita via Web</a></li>
									{{/siomChequearPerfil}}
								{{else}}
									{{#siomChequearPerfil "#/os/solicitud/informe/*"}}
										{{#is orse_solicitud_informe_web 'NOSOLICITADA'}}
											<li role="presentation">
												<a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#ModalSolicitud" data-orse-id="{{orse_id}}" data-tipo="informe"  data-title="Solicitud de subida de informe via Web">Solicitar subida de informe via Web</a>
											</li>
										{{/is}}
										{{#is orse_solicitud_informe_web 'SOLICITANDO'}}
											<li role="presentation" class="disabled">
												<a role="menuitem" tabindex="-1" href="#" class="solicitando" disabled="true">Solicitando subida de informe via Web</a>
											</li>
										{{/is}}
									{{/siomChequearPerfil}}	
								{{/is}}

								{{#siomChequearPerfil "#/os/solicitud/cambio/*"}}			
									{{#osConSolicitudCambio orse_tipo}}	
										{{#is orse_solicitud_cambio 'NOSOLICITADA'}}
											<li role="presentation">
												<a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#ModalSolicitud" data-orse-id="{{orse_id}}" data-tipo="cambio" data-title="Solicitud de cambio ">Solicitar cambio</a>
											</li>
										{{/is}}
										{{#is orse_solicitud_cambio 'SOLICITANDO'}}
											<li role="presentation" class="disabled">
												<a role="menuitem" tabindex="-1" href="#" class="solicitando" disabled="true">Solicitando cambio</a>
											</li>
										{{/is}}		
									{{/osConSolicitudCambio}}
								{{/siomChequearPerfil}}

								{{#siomChequearPerfil "#/os/anular/*"}}
									<li role="presentation">
										<a class="siom-anular-os" role="menuitem" tabindex="-1" data-orse-id="{{orse_id}}" >Anular OS</a>
									</li>
								{{/siomChequearPerfil}}


								{{#if pres_id}}
									{{#osConPresupuesto orse_tipo}}	
										{{#siomChequearPerfil "#/os/presupuesto/*/validar/*"}}
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#/os/presupuesto/{{orse_id}}/validar/{{pres_id}}">Validar presupuesto</a></li>
										{{/siomChequearPerfil}}
									{{/osConPresupuesto}}
								{{/if}}

								{{#siomChequearPerfil "#/os/adjuntar/*"}}
									<li role="presentation" class="divider"></li>

									<li role="presentation">
										<a role="menuitem" tabindex="-1" href="#/os/adjuntar/{{orse_id}}">Agregar archivo(s)</a>
									</li>
								{{/siomChequearPerfil}}
								</ul>
							{{/osChequearActiva}}
					{{/siomChequearPerfil}}
					</div>
			
				{{#osChequearActiva orse_estado}}					
					{{#if tarea}}
						{{#each tarea}}
							{{#siomChequearTarea this}}
								<a class="btn btn-primary btn-xs" href="{{@link}}" role="button">{{capitalizeFirst @texto}}</a>
							{{else}} 
								<div class="esperando-tarea">{{uppercase @texto_alt}}</div>
							{{/siomChequearTarea}}
						{{/each}}
					{{else}}
					    <div class="sin-tarea">EN PROCESO</div>
					{{/if}}
			 	 		<div class="timeago fecha" title="{{orse_fecha_solicitud}}">{{timeAgo orse_fecha_solicitud}}</div>
			 	 	{{else}}
			 	 		<div class="sin-tarea">FINALIZADA</div>
			 	 		<div class="timeago fecha" title="{{orse_fecha_solicitud}}">{{timeAgo orse_fecha_solicitud}}</div>
				{{/osChequearActiva}}

				</div>

				<div class="info text-left">
					<h4>
						{{#is orse_responsable 'MOVISTAR'}}
							<i class="glyphicon glyphicon-home"></i>&nbsp; 
						{{else}}
							<i class="glyphicon glyphicon-user"></i>&nbsp;
						{{/is}}
						{{empl_nombre}} <small>{{empl_nemonico}}</small>
					</h4>
					<div class="info_os">{{orse_tipo}} - {{sube_nombre}}</div>
					<div class="info_adicional">Dirección: <strong>{{empl_direccion}}</strong></div>
				</div>
			</div>
			<hr>
		{{/ordenes}}
	</div>
	<div class="panel-footer siom-paginacion">
		<div class="row">
			<div class="col-md-4 text-left">
				{{#siomChequearPerfil "#/os/descargar"}}
				{{#if status}}
				<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
				{{/if}}
				{{/siomChequearPerfil}}

				{{#siomChequearPerfil "#/os/descargar"}}
				{{#siomChequearPerfil "#/os/reporte/sap"}}
				<button type="button" class="btn btn-default btn-xs" id="reporteSAP" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}"><span class="glyphicon glyphicon-download" aria-hidden="true"></span> Descargar reporte SAP</button>
				{{/siomChequearPerfil}}
				{{/siomChequearPerfil}}
			</div>
			<div class="col-md-8 text-right">
				<div class="pagination-info">
					Página {{pagina}}/{{paginas}} ({{total}} registros)
				</div>
				<div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
				<!--<button class="btn btn-default btn-xs"><i class="glyphicon glyphicon-download-alt"></i></button>-->
			</div>
		</div>
	</div>
</div>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/os_bandeja_lista.js" type="text/javascript" charset="utf-8"></script>

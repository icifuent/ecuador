<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Contrato</h5>
    </div>
    {{#data}}
    <div class="col-md-7 text-right" id="AgregarContrato">
        <a class="btn btn-primary btn-sm" href="#/core/Contrato" id="BotonContratoAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-data="{{JSON .}}"></span> Agregar
        </a>
    </div>
    {{/data}}
</div>

{{#if data.status}}
<div id="WizardContrato" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" id="Primero" class="active">
            <span class="badge">1</span>Contratos
            <span class="chevron"></span>
        </li>
        <li data-step="2" id="Segundo">
            <span class="badge">2</span>Informacion
            <span class="chevron"></span>
        </li>
        <li data-step="3" id="Tercero">
            <span class="badge">3</span>Zonas
            <span class="chevron"></span>
        </li>
        <li data-step="4" id="Cuarto">
            <span class="badge">4</span>LPU
            <span class="chevron"></span>
        </li>
        <li data-step="5" id="Quinto">
            <span class="badge">5</span>Documentos
            <span class="chevron"></span>
        </li>
        <li data-step="6" id="sexto">
            <span class="badge">6</span>Especialidad
            <span class="chevron"></span>
        </li>
        <li data-step="7" id="septimo">
            <span class="badge">7</span>Periodicidad
            <span class="chevron"></span>
        </li>
        <li data-step="8" id="octavo">
            <span class="badge">8</span>Mantenimiento
            <span class="chevron"></span>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/contrato/filtro" method="POST" id="FormContratoLista">
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-7">
                                        <input autocomplete="off" type="text" class="form-control" name="cont_nombre" value="{{data.cont_nombre}}"></input>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="periodos" class="col-sm-5 control-label">Estado</label>
                                    <div class="col-sm-7">
                                        <select id="estado" class="selectpicker" data-width="100%" name="cont_estado">
                                            <option value="">Todos</option>
                                            {{#data.estados}}
                                            <option value="{{.}}">{{.}}</option>
                                            {{/data.estados}}
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-contrato-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">

            <div class="col-md-12 text-right">
                <div class="acc-wizard-step">
                    <button class="btn btn-primary btn-sm" type="submit" id="BotonContratoZonas" style="margin-top:10px">Siguiente Paso</button>
                </div>
            </div>

            <div class="panel panel-default">

                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/contrato/editar" method="POST" id="FormContratoEditar">
                    <input autocomplete="off" id="cont_id" name="cont_id" style="display:none" />
                    <input autocomplete="off" id="usua_creador" name="usua_creador" style="display:none" />

                    <div class="col-md-8">
                        <fieldset class="siom-fieldset" id="siom-Contrato-info">
                            <legend>Contrato</legend>
                            <table width="100%">
                                <tr>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="cont_nombre" class="col-sm-3 control-label">Nombre</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="cont_nombre" name="cont_nombre" class="form-control" placeholder="Nombre" value=""
                                                />
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="cont_alias" class="col-sm-3 control-label">Alias</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="cont_alias" name="cont_alias" class="form-control" placeholder="Alias" value=""
                                                />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="cont_observacion" class="col-sm-3 control-label">Observacion</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="cont_observacion" name="cont_observacion" class="form-control" placeholder="observacion"
                                                    value="" />
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="cont_descripcion" class="col-sm-3 control-label">Descripcion</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="cont_descripcion" name="cont_descripcion" class="form-control" placeholder="Descripcion"
                                                    value="" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <tr>

                                        <td colspan="2">
                                            <div class="form-group">
                                                <label for="cont_fecha_inicio" class="col-sm-3 control-label">Fecha inicio</label>
                                                <div class="col-sm-8">
                                                    <input autocomplete="off" id="cont_fecha_inicio" class="form-control" size="14" type="text" data-date-autoclose="true" name="cont_fecha_inicio"
                                                        placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />
                                                </div>
                                            </div>
                                        </td>
                                        <td colspan="2">
                                            <div class="form-group">
                                                <label for="cont_fecha_termino" class="col-sm-3 control-label">Fecha termino</label>
                                                <div class="col-sm-8">
                                                    <input autocomplete="off" id="cont_fecha_termino" class="form-control" size="14" type="text" data-date-autoclose="true" name="cont_fecha_termino"
                                                        placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>



                                        <td colspan="2">
                                            <div class="form-group">
                                                <label for="cont_estado" class="col-sm-3 control-label">Estado</label>
                                                <div class="col-sm-8">
                                                    <select id="cont_estado" name="cont_estado" class="selectpicker" data-width="100%" data-container="body">
                                                        {{#data.estados}}
                                                        <option value="{{.}}">{{.}}</option>
                                                        {{/data.estados}}
                                                    </select>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>


                            </table>
                        </fieldset>
                    </div>

                    <div class="row text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="BotonContratoGuardar"
                                style="margin:10px">Guardar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="3">

            <div class="col-md-12 text-right">
                <div class="acc-wizard-step">
                    <button class="btn btn-primary btn-sm" type="submit" id="BotonContratoLPU" style="margin-top:10px">Siguiente Paso</button>
                </div>
            </div>

            <div class="panel panel-default">

                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/contrato" method="POST" id="FormContratoZonas">
                    <input autocomplete="off" id="cont_id_form" name="cont_id_form" style="display:none" />

                    <div class="col-md-12">
                        <fieldset class="siom-fieldset" id="siom-Contrato-info">
                            <legend>Zonas</legend>

                            <div class="col-md-9" id="TableContratoZonasLista">
                            </div>

                            <div class="col-md-11" id="TableContratoZonasCrear">
                            </div>

                        </fieldset>
                        <br></br>
                    </div>

                    <div class="row"></div>
                </form>
            </div>
        </div>


        <div class="step-pane sample-pane" data-step="4">

            <div class="col-md-12 text-right">
                <div class="acc-wizard-step">
                    <button class="btn btn-primary btn-sm" type="submit" id="BotonContratoDocumentos" style="margin-top:10px">Siguiente Paso</button>
                </div>
            </div>

            <div class="panel panel-default">


                <div class="col-md-12">
                    <fieldset class="siom-fieldset" id="siom-Contrato-info">
                        <legend>LPU</legend>

                        <div class="input-group">
                            <select id="ContratoLPU" class="selectpicker" data-width="100%" name="lpu_id" title="Seleccione LPU">
                                <option value=""></option>
                            </select>
                            <span class="input-group-btn">
                                <button id="ContratoLPUEditar" type="button" class="btn btn-default">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </button>
                                <button id="ContratoLPUAgregar" type="button" class="btn btn-default">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>

                        <div id="ContratoLPUGuardarForm" class="input-group" style="display: none;">
                            <input  type="hidden" value="" />
                            <input autocomplete="off" type="text" class="form-control" placeholder="Nombre LPU" value="" />
                            <select class="selectpicker" title="Seleccione estado">
                                <option value="ACTIVO">ACTIVO</span>
                                    </button>
                                </option>
                                <option value="NOACTIVO">NOACTIVO</span>
                                    </button>
                                </option>
                            </select>
                            <span class="input-group-btn">
                                <button id="ContratoLPUGuardar" type="button" class="btn btn-default">
                                    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>

                    </fieldset>
                </div>

                <div class="col-md-12">
                    <fieldset class="siom-fieldset" id="siom-Contrato-info">
                        <legend>Detalle</legend>

                        <form role="form" action="#/core/contrato/lpu" method="POST" id="FormContratoLPUDetalle" class="form siom-form-tiny">
                            <input autocomplete="off" id="cont_id_form" name="cont_id_form" style="display:none" />
                            <div class="col-md-5">

                                <label class="control-label">GRUPOS</label>
                                <div class="input-group">
                                    <select id="ContratoLPUGrupo" class="selectpicker" data-width="100%" name="grup_id" title="Seleccione grupo">
                                        <option value=""></option>
                                    </select>
                                    <span class="input-group-btn">
                                        <button id="ContratoLPUGrupoEditar" type="button" class="btn btn-default btn-xs lpu-editar">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                        <button id="ContratoLPUGrupoAgregar" type="button" class="btn btn-default btn-xs lpu-agregar">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="ContratoLPUGrupoGuardarForm" class="input-group" style="display: none;">
                                    <input  type="hidden" value="" />
                                    <input autocomplete="off" type="text" class="form-control" id="grup_nombre" placeholder="Nombre grupo" value="" />
                                    <span class="input-group-btn">
                                        <button id="ContratoLPUGrupoGuardar" type="button" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>

                                <div style="height:10px"></div>

                                <label class="control-label">SUBGRUPOS</label>
                                <div class="input-group">
                                    <select id="ContratoLPUSubgrupo" class="selectpicker" data-width="100%" name="grup_id" title="Seleccione subgrupo">
                                        <option value=""></option>
                                    </select>
                                    <span class="input-group-btn">
                                        <button id="ContratoLPUSubgrupoEditar" type="button" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                        <button id="ContratoLPUSubgrupoAgregar" type="button" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>
                                <div id="ContratoLPUSubgrupoGuardarForm" class="input-group" style="display: none;">
                                    <input  type="hidden" value="" />
                                    <input autocomplete="off" type="text" class="form-control" id="grup_nombre" placeholder="Nombre subgrupo" value="" />
                                    <span class="input-group-btn">
                                        <button id="ContratoLPUSubgrupoGuardar" type="button" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>

                                <div style="height:10px"></div>

                                <label class="control-label">CLASES</label>
                                <ul id="ContratoLPUSubgrupoClase" class="list-unstyled">
                                </ul>
                                <div class="input-group">
                                    <input autocomplete="off" id="ContratoLPUSubgrupoClaseGuardarForm" type="text" class="form-control" id="grup_nombre" placeholder="Nueva clase"
                                        value="" />
                                    <span class="input-group-btn">
                                        <button id="ContratoLPUSubgrupoClaseGuardar" type="button" class="btn btn-default btn-xs">
                                            <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                                        </button>
                                    </span>
                                </div>


                            </div>

                            <div class="col-md-7">

                                <div class="form-group">

                                    <div class="row">
                                        <div class="col-md-1">
                                            <label class="control-label">ITEMES</label>
                                        </div>
                                        <button type="button" id="ContratoLPUSubgrupoItemAgregar" class="btn btn-primary pull-right">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                    <div class="row">
                                        <div class="siom-os-lista siom-os-presupuesto-lista" id="ContratoLPUSubgrupoItems" style="height: 380px;overflow:auto !important;">
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div id="ContratoLPUSubgrupoItemEditarModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog modal-lg">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Editar Item</h4>
                                            </div>

                                            <div class="modal-body">
                                                <table class="table-condensed" id="ContratoLPUSubgrupoItemEditarInfo">
                                                    <input  type="hidden" id="lpit_id" class="item-data" value="">
                                                    <tr>
                                                        <td class="col-md-1">
                                                            <label class="control-label">NOMBRE</label>
                                                        </td>
                                                        <td class="col-md-11">
                                                            <input autocomplete="off" type="text" class="form-control item-data"
                                                                disabled id="lpit_nombre" placeholder="Nombre item" value="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label">COMENTARIO</label>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" class="form-control item-data"
                                                                disabled id="lpit_comentario" placeholder="Comentario item" value="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label class="control-label">UNIDAD</label>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" class="form-control item-data"
                                                                disabled id="lpit_unidad" placeholder="Unidad item" value="">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <button type="button" id="ContratoLPUSubgrupoItemPrecioInfoEditar"
                                                                class="btn btn-default pull-right item-editar">
                                                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <hr>
                                                <label class="control-label">PRECIOS</label>
                                                <table class="table table-striped table-hover table-condensed" id="ContratoLPUSubgrupoItemEditarInfoPreciosEditar" style="display: none">
                                                    <tr>
                                                        <th>Clase</th>
                                                        <th>Zona</th>
                                                        <th>Precio</th>
                                                        <th>CAPEX</th>
                                                        <th>OPEX</th>
                                                        <th>Code1</th>
                                                        <th>Code2</th>
                                                        <th>Code3</th>
                                                        <th>Code4</th>
                                                        <th>Code5</th>
                                                        <th>Estado</th>
                                                        <th></th>
                                                    </tr>
                                                    <tr>
                                                        <input  type="hidden" id="lpip_id" class="precio-data" value="">
                                                        <input  type="hidden" id="lpit_id" class="precio-data" value="">
                                                        <input  type="hidden" id="zona_id" class="precio-data" value="">
                                                        <input  type="hidden" id="lpgc_id" class="precio-data" value="">
                                                        <td nowrap id="lpgc_nombre"></td>
                                                        <td nowrap id="zona_nombre"></td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_precio" class="form-control precio-data"
                                                                placeholder="Precio" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_sap_capex" class="form-control precio-data"
                                                                placeholder="CAPEX" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_sap_opex" class="form-control precio-data"
                                                                placeholder="OPEX" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_code1" class="form-control precio-data"
                                                                placeholder="Code1" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_code2" class="form-control precio-data"
                                                                placeholder="Code2" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_code3" class="form-control precio-data"
                                                                placeholder="Code3" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_code4" class="form-control precio-data"
                                                                placeholder="Code4" value=""></input>
                                                        </td>
                                                        <td>
                                                            <input autocomplete="off" type="text" id="lpip_code5" class="form-control precio-data"
                                                                placeholder="Code5" value=""></input>
                                                        </td>
                                                        <td>
                                                            <select id="lpip_estado" class="precio-data" title="Seleccione estado">
                                                                <option value="ACTIVO">ACTIVO</option>
                                                                <option value="NOACTIVO">NOACTIVO</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <button type="button" id="ContratoLPUSubgrupoItemEditarInfoPreciosGuardar"
                                                                class="btn btn-default btn-xs">
                                                                <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <div id="ContratoLPUSubgrupoItemEditarInfoPrecios">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </form>

                    </fieldset>
                </div>


            </div>
        </div>

        <div class="step-pane sample-pane" data-step="5">



            <div class="col-md-12 text-right">
                <div class="acc-wizard-step" style="margin-top:10px">
                    <button class="btn btn-default btn-sm" id="BotonContratoDocumentosAgregar">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar documento
                    </button>


                    <button class="btn btn-primary btn-sm" type="submit" id="BotonContratoEspecialidad">Siguiente Paso</button>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="col-md-12">
                    <fieldset class="siom-fieldset">
                        <legend>Documentos en el contrato</legend>
                        <div id="ContratoDocumentosLista"></div>
                    </fieldset>
                </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="6">
            <div class="col-md-12 text-right">
                <div class="acc-wizard-step" style="margin-top:10px">
                    <button class="btn btn-primary btn-sm" type="submit" id="BotonContratoPeriodicidad">Siguiente Paso</button>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="col-md-12">
                    <fieldset class="siom-fieldset">
                        <legend>Especialidad en el contrato</legend>
                        <div id="ContratoEspecialidadLista">
                            <div class="col-md-11" style="padding-left: 0px;padding-right: 44px;">
                                <select id="Contratoespe" class="selectpicker" data-width="100%" name="espe_id" title="Seleccione Especialidad">
                                    <option value=""></option>
                                </select>
                            </div>

                            <div class="col-md-1" style="padding-left: 0px;padding-right: 0px;">
                                <span class="input-group-btn">
                                    <button id="ContratoESPEditar" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                    </button>
                                    <button id="ContratoESPAgregar" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                    </button>
                                </span>
                            </div>
                            <div id="ContratoESPuardarForm" class="input-group" style="display: none;">
                                <input  type="hidden" class="form-control" id="hidden_esp_guardar" value="" />
                                <input autocomplete="off" type="text" class="form-control" placeholder="Nombre Especialidad" value="" id="textHidden" />
                                <select class="selectpicker" title="Seleccione estado">
                                    <option value="ACTIVO">ACTIVO</span>
                                        </button>
                                    </option>
                                    <option value="NOACTIVO">NOACTIVO</span>
                                        </button>
                                    </option>
                                </select>
                                <span class="input-group-btn">
                                    <button id="ContratoESPGuardar" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true">Guardar</span>
                                    </button>
                                </span>
                                <span class="input-group-btn">
                                    <button id="CancelarEspecialidad" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true">Cancelar</span>
                                    </button>
                                </span>
                            </div>
                            <div class="col-md-12">
                                <fieldset class="siom-fieldset">
                                    <legend>Formularios</legend>


                                    <form id="form_checkbox">

                                        <div class="col-md-12">
                                            <div class="info text-left col-md-8">

                                            </div>
                                            <div class="col-md-2"></div>
                                            <div class="col-md-2">Preseleccion</div>
                                        </div>

                                        <div id="form_id"></div>
                                    </form>


                                </fieldset>
                                <div class="col-md-12 text-right">
                                    <div class="acc-wizard-step" style="margin-top:10px">

                                        <button class="btn btn-primary btn-sm" type="submit" id="BotonGuardarEspecialidadFormulario">Guardar</button>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </fieldset>
                </div>
            </div>

        </div>

        <div class="step-pane sample-pane" data-step="7">
            <input autocomplete="off" id="cont_id_form" name="cont_id_form" style="display:none" />
            <div class="col-md-12 text-right">
                <div class="acc-wizard-step" style="margin-top:10px">
                    <button class="btn btn-primary btn-sm" type="submit" id="BotonContratoMantenimiento">Siguiente Paso</button>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="col-md-12" style="min-height: 300px;">
                    <fieldset class="siom-fieldset">
                        <legend>Periodicidad en el contrato </legend>
                        <div id="ContratoPeriodicidadLista" class="col-md-10">
                            <select id="peri_contrato" class="selectpicker" data-width="100%" name="peri_id" title="Seleccione periodicidad">
                                <option value=""></option>
                            </select>

                        </div>
                        <div class="col-md-1" style="padding-left: 0px;padding-right: 0px;">
                            <button id="ContratoPeriAgregar" type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </div>

                        <div class="col-md-12" id="formularioPeri" style="display:none;">
                            <fieldset class="siom-fieldset">
                                <div class="col-md-2"> Dias Apertura : </div>
                                <div class="col-md-4">
                                    <input autocomplete="off" type="text" class="form-control rcpe" id="rcpe_dias_apertura"></input>
                                </div>
                                <div class="col-md-2"> Dias Post cierre : </div>
                                <div class="col-md-4">
                                    <input autocomplete="off" type="text" class="form-control rcpe" id="rcpe_dias_post_cierre"></input>
                                </div>
                                <div class="col-md-2"> Participa en MPP: </div>
                                <div class="col-md-4" style="padding-top: 5px;">
                                    <select id="rcpe_participa_mpp" class="selectpicker" title="Seleccione estado">
                                        <option value="1">SI</span>
                                        </option>
                                        <option value="0">NO</span>
                                        </option>
                                    </select>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-4" style="padding-top: 5px;">
                                    <button id="GuardarContratoPeri" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"> Guardar</span>
                                    </button>
                                    <button id="CancelarContratoPeri" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-trash" aria-hidden="true"> Cancelar</span>
                                    </button>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <fieldset class="siom-fieldset">
                                <legend>Periodicidad</legend>
                                <div>
                                    <table class="table table-striped table-hover table-condensed" data-sort-order="desc">
                                        <thead>
                                            <tr>
                                                <th data-align="center">Nombre</th>
                                                <th data-align="center">Meses Periodicidad</th>
                                                <th data-align="center">Orden Periodicidad</th>
                                                <th data-align="center">Dias apertura</th>
                                                <th data-align="center">Dias post cierre</th>
                                                <th data-align="center">participa MPP</th>
                                            </tr>
                                        </thead>
                                        <tbody id="peri_lista"></tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>


        <div class="step-pane sample-pane" data-step="8">
            <input autocomplete="off" id="cont_id_form" name="cont_id_form" style="display:none" />

            <div class="col-md-12">
                <fieldset class="siom-fieldset" style="overflow: auto;max-height: 500px;">
                    <legend>Mantenimiento</legend>



                    <table class="table-striped table-hover table-condensed col-md-12" id="tabla">
                        <thead>
                            <tr>
                                <th style="text-align: center;">
                                    <h5>Periodicidad</h5>
                                </th>
                                <th style="text-align: center;">
                                    <h5>Clasificacion programación</h5>
                                </th>
                                <th style="text-align: center;">
                                    <h5>Especialidad</h5>
                                </th>
                                <th style="text-align: center;">
                                    <h5>Formulario</h5>
                                </th>
                                <th style="text-align: center;">
                                    <h5>Meses</h5>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="mant_form_list" style="text-align: center;">
                            <tr>
                                <td>
                                    <select id="mant_peri" class="selectpicker col-md-12" title=" Periodicidad"></select>

                                </td>
                                <td>
                                    <select id="mant_clasi" class="selectpicker col-md-12" title=" Clasificacion">

                                    </select>
                                </td>
                                <td>
                                    <select id="mant_espe" class="selectpicker col-md-12" title=" Especialidad">

                                    </select>
                                </td>
                                <td>
                                    <select id="mant_form" class="selectpicker col-md-12" title=" Formulario">

                                    </select>
                                </td>
                                <td>
                                    <select id="mant_mes" class="selectpicker col-md-12" title=" Mes">

                                    </select>
                                </td>
                                <td>
                                    <button id="AgregarMantDefForm" type="button" class="btn btn-default">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"> Agregar</span>
                                    </button>
                                </td>
                            </tr>

                        </tbody>
                    </table>
            </div>
            </fieldset>


        </div>


        <div class="modal fade" id="ModalDocumentosEditar">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Editar documento</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal siom-form-tiny" role="form" action="#/core/contrato/editar" method="POST" id="FormContratoEditar">
                            <input  type="hidden" name="repo_id" value="" />

                            <div class="form-group">
                                <label for="repo_tipo_doc" class="col-sm-3 control-label">Tipo documento</label>
                                <div class="col-sm-8">
                                    <input autocomplete="off" type="text" name="repo_tipo_doc" class="form-control" placeholder="Tipo documento" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="repo_nombre" class="col-sm-3 control-label">Nombre</label>
                                <div class="col-sm-8">
                                    <input autocomplete="off" type="text" name="repo_nombre" class="form-control" placeholder="Nombre" value="" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="repo_descripcion" class="col-sm-3 control-label">Descripción</label>
                                <div class="col-sm-8">
                                    <input autocomplete="off" type="text" name="repo_descripcion" class="form-control" placeholder="Descripción" value="" />
                                </div>
                            </div>

                            <div class="form-group archivo">
                                <label for="repo_descripcion" class="col-sm-3 control-label">Archivo</label>
                                <div class="col-sm-8">
                                    <input autocomplete="off" type="file" name="archivo">
                                </div>
                            </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary" data-loading-text="Guardando..." data-complete-text="Guardado!">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

        {{else}}
        <div class="alert alert-danger" role="alert">
            {{data.error}} {{#if data.debug}}
            <br>
            <small>{{data.debug}}</small>
            {{/if}}
        </div>
        {{/if}}

        <script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" type="text/css" href="css/bootstrap-table.css" media="screen" />

        <script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/core_contrato.js" type="text/javascript" charset="utf-8"></script>
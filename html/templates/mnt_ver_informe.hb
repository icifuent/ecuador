<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Validación de informe</h5>
 </div>
 <div class="col-md-7 text-right">
   	{{#if status}}
 	<button type="button" class="btn btn-primary btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{data.informe.info_id}}">Descargar PDF</button>
   	{{/if}}
 </div>
</div>
{{#if status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset">
  <legend>Mantenimiento</legend>
  {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;" id="siom-ver-informe">
    <legend>Informe</legend>
    {{#if data.informe}}
    	<table width="100%">
		  <tr>
		      <td width="20%">
		        <div class="siom-label">GENERADO POR</div>
		        <div class="siom-value">{{data.informe.usua_nombre}}</div>
		      </td>
		      <td width="20%">
		        <div class="siom-label">FECHA</div>
		    	<div class="siom-value">{{data.informe.info_fecha_creacion}}</div>
		      </td>
		   </tr>
		</table>
		<hr>
		
		{{{loadTemplate "form_informe" data}}}

		<hr>
		<div class="row">
		 <div class="col-md-12 text-right">
		 	{{#if status}}
		 	<button type="button" class="btn btn-primary btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{data.informe.info_id}}">Descargar PDF</button>
		   	{{/if}}
		 </div>
		</div>

		<hr>
		
    	<div class="row text-center">
	     	<div class="col-md-12">
	     		<a href="#/mnt/detalle/{{mantenimiento.mant_id}}" class="btn btn-default">Volver</a>
	     	</div>
	    </div>

    {{else}}
    	<div class="alert alert-warning" role="alert">
	  	<strong>Aviso</strong> ruta indicada no tiene un informe definido.
		</div>
    {{/if}}
</fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}
    
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mnt_ver_informe.js" type="text/javascript" charset="utf-8"></script>


<link rel="stylesheet" type="text/css" href="css/dashboardmnt.css" media="screen" />

<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Dashboard Mantenimiento</h5>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/dashboard/mnt" method="POST" id="siom-form-dashboard-mnt">

            <fieldset class="siom-fieldset" style="padding-bottom:20px; margin-top:0px;">
                <!--legend>Filtros</legend-->

                <div class="col-md-4" id="FiltroEspecialidades" style="display:block">
                    <div>
                        <label>Especialidades</label>
                    </div>
                    <select class="selectpicker" data-width="100%" name="espe_id" multiple title="TODAS" data-container="body">
                        {{#especialidades_mnt}}
                        <option value="{{espe_id}}">{{espe_nombre}}</option>
                        {{/especialidades_mnt}}
                    </select>
                </div>


                <div class="col-md-4" id="filtroPeriodo" style="display:block">
                    <div>
                        <label>Periodos</label>
                    </div>
                    <select class="selectpicker" data-width="100%" name="peri_id" multiple title="TODOS" data-container="body">
                        {{#periodos}}
                        <option value="{{peri_id}}">{{peri_nombre}}</option>
                        {{/periodos}}

                    </select>
                </div>

                <div class="col-md-4">
                    <div>
                        <label>Fecha de referencia</label>
                    </div>
                    <input autocomplete="off" id="mant_fecha_referencia" class="form-control" size="14" type="text" name="mant_fecha_referencia"
                        value="{{#if data.mant_fecha_referencia}} {{formatDate data.mant_fecha_referencia '%d-%m-%Y'}} {{/if}}"
                    />
                </div>
            </fieldset>
            <!--            <center><div>
                    <img src="img/dashboardMNT.png" id="leyendaMNT" align="middle">
            </div></center> -->
        </form>
    </div>
    <div class="col-md-6">
        <table class="dashboard-table">
            <tr>
                <td>
                    <div class="input-color">
                        <p>APROBADAS</p>
                        <div class="color-box" style="background-color: #22B24C;"></div>
                    </div>
                </td>
                <td>
                    <p>&nbsp</p>
                </td>
                <td>
                    <div class="input-color">
                        <p> {{#is data.cont_id '1'}}EZENTIS{{/is}} {{#is data.cont_id '17'}}EZENTIS{{/is}} {{#is data.cont_id
                            '10'}}EZENTIS{{/is}} {{#is data.cont_id '18'}}ISS{{/is}}</p>
                        <div class="color-box" style="background-color: #EF5BE8;"></div>
                    </div>
                </td>
                <td>
                    <p>&nbsp</p>
                </td>
                <td>
                    <div class="input-color">
                        <p>MOVISTAR</p>
                        <div class="color-box" style="background-color: #2A54E3;"></div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="input-color">
                        <p>PROGRAMADAS</p>
                        <div class="color-box" style="background-color: #000000;"></div>
                    </div>
                </td>
                <td>
                    <p>&nbsp</p>
                </td>
                <td>
                    <div class="input-color">
                        <p>RECHAZADAS</p>
                        <div class="color-box" style="background-color: #6c6b73;"></div>
                    </div>
                </td>
                <td>
                    <p>&nbsp</p>
                </td>
                <td>
                    <div class="input-color">
                        <p>NO REALIZADA</p>
                        <div class="color-box" style="background-color: #643100;"></div>
                    </div>
                </td>
            </tr>
        </table>
    </div>

</div>
<br/>

<div id="dashboard_mnt_graficos"></div>

<script src="js/dashboard_mnt.js" type="text/javascript" charset="utf-8"></script>
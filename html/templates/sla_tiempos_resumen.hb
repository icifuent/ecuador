<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script> 
<link rel="stylesheet" type="text/css" href="css/bootstrap-table.css" media="screen" />

<div class="row siom-section-header">
    <div class="col-md-4">
        <h5>Resumen</h5>
    </div>
	
	<div class="col-md-8">
        <h5>Cumplimiento</h5>
    </div>
</div>

<div class="row">
	<div class="col-md-4" style="float: left; padding-top: 5px;  padding-bottom: 5px;">
		<table  class="table table-bordered"  >
			<thead>
				<tr>
				  <th data-align="right" >Datos</th>
				  <th data-align="right" >Valor</th>
				</tr>
			</thead>
			<tbody>
			   {{#resumen}}
				<tr><td>Total OSGN</td> <td>{{total_osgn}}</td></tr>
				<tr><td>Total OSGU</td><td>{{total_osgu}}</td></tr>
				<tr><td>Total OSEN</td><td>{{total_osen}}</td></tr>
				<tr><td>Total OSEU</td><td>{{total_oseu}}</td></tr>
				<tr><td>Promedio Tiempo OSGN</td><td>{{prom_osgn_tiempo}}</td></tr>
				<tr><td>Promedio Tiempo OSGU</td><td>{{prom_osgu_tiempo}}</td></tr>
				<tr><td>Promedio Tiempo OSEN</td><td>{{prom_osen_tiempo}}</td></tr>
				<tr><td>Promedio Tiempo OSEU</td><td>{{prom_oseu_tiempo}}</td></tr>
				{{/resumen}}
			</tbody>
		</table>
	</div>

	<div class="col-md-8" style="float: left; padding-top: 5px;  padding-bottom: 5px;padding-right: 0px;">
		<table  class="table table-bordered"  >            
			<tbody>
			   {{#resumen}}
				<tr>
					<td style="width:33%">SLA Tiempo Respuesta 
						<div class="panel panel-default text-center"><label style="font-size: 50px;">{{sla_tiempo_respuesta}}%</label></div> 
					</td>
					<td style="width:33%">SLA Entrega Presupuesto
						<div class="panel panel-default text-center"><label style="font-size: 50px;">{{sla_entrega_presupuesto}}%</label></div>
					</td>
					<td style="width:34%">SLA Entrega Informe
						<div class="panel panel-default text-center"><label style="font-size: 50px;">{{sla_entrega_informe}}%</label></div>
					</td>
				</tr>

				<tr>
					<td>SLA Solucion final 
						<div class="panel panel-default text-center"><label style="font-size: 50px;">{{sla_solucion_final}}%</label></div>
					</td>
					<td>SLA Validacion Presupuesto
						<div class="panel panel-default text-center"><label style="font-size: 50px;">{{sla_validacion_presupuesto}}%</label></div>
					</td>
					<td>SLA Validacion Informe
						<div class="panel panel-default text-center"><label style="font-size: 50px;">{{sla_validacion_informe}}%</label></div>
					</td>
				</tr>                      
				{{/resumen}}
			</tbody>
		</table>
	</div>
</div>

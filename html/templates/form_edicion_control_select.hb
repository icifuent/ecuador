<div class="row form-edicion-container-control" id="control_{{foit_id}}"  data-type="select" data-properties="{{JSON properties}}" data-options="{{JSON options}}" data-indf-inventariable="{{indf_inventariable}}" data-indf-genero="{{indf_genero}}" data-indf-especie="{{indf_especie}}" data-indf-campo="{{indf_campo}}" data-indf-codigo="{{indf_codigo}}" data-aggregated="{{aggregated}}">
  <div class="col-md-8 form-group">
      <label class="control-label">{{foit_nombre}}</label>

      <select class="form-control input-sm selectpicker" name="{{field}}" title="">
         {{#if options.data}} 
           {{#each options.data.value}}
           <option>{{.}}</option>
           {{/each}}
         {{/if}}
      </select>
  </div>
  <div class="col-md-4 menu">
      <ul class="nav nav-pills pull-right">
        <li role="presentation"><a class="btn btn-xs form-edicion-remove" role="button" data-id="control_{{foit_id}}">Eliminar</a></li>
        <li role="presentation"><a class="btn btn-xs form-edicion-properties" role="button" data-id="control_{{foit_id}}">Propiedades</a></li>
      </ul>
  </div>
</div>
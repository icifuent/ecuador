<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" >
  	{{#data}}
  	<div class="os-crear">
  	  <div class="estado text-center">
	  	<span class="glyphicon glyphicon-stop status" aria-hidden="true"></span>
	  </div>
	  <div class="acciones col-md-4 text-right">
	  	<button class="btn btn-primary btn-xs os-crear-selector" data-empl-id="{{empl_id}}" data-data="{{JSON .}}">Seleccionar</button>
	  </div>
	  <div class="info text-left">
	  	<h4>{{empl_nombre}} <small>{{empl_nemonico}}</small></h4>
	  	<div class="info_adicional">Clasificación: <strong>{{clas_nombre}}</strong>, Dirección: <strong>{{empl_direccion}}</strong></div>
	  </div>
	  
	</div>
	<hr>
	{{/data}}
  </div>
  <div class="panel-footer siom-paginacion">
      <div class="row">
      <div class="col-md-4 text-left">
        {{#if status}}
        <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
      {{/if}}
      </div>
      <div class="col-md-8 text-right">
        <div class="pagination-info">
          Página {{pagina}}/{{paginas}} ({{total}} registros)
        </div>
        <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
      </div>
    </div>
    </div>
  

     
</div>
<script>
$('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/insp/crear/filtro/'+page,data);
});
</script>
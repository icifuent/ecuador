<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important" >
  	{{#data}}
  	<div class="os-crear">
  	  <div class="estado text-center">
	  	<span class="glyphicon glyphicon-stop status" aria-hidden="true"></span>
	  </div>
	  <div class="acciones col-md-4 text-right">
	  	<button class="btn btn-primary btn-xs os-crear-selector" data-empl-id="{{empl_id}}" data-data="{{JSON .}}">Seleccionar</button>
	  </div>
	  <div class="info text-left">
	  	<h4>{{empl_nombre}} <small>{{empl_nemonico}}</small></h4>
	  	<div class="info_adicional">Clasificación: <strong>{{clas_nombre}}</strong>, Dirección: <strong>{{empl_direccion}}</strong>, Responsable: <strong>{{empl_responsable}}</strong></div>
	  </div>
	  
	</div>
	<hr>
	{{/data}}
  </div>
  <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	<div class="pagination-info">
	    	<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
	    </div>
	  </div>
	  <div class="col-md-8 text-right">
	    <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>
</div>
<script>
$('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/os/crear/filtro/'+page,data);
});
</script>
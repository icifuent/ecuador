<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script> 
<link rel="stylesheet" type="text/css" href="css/bootstrap-table.css" media="screen" />

<div class="row siom-section-header">
    <div class="col-md-4">
        <h5>Detalle</h5>
    </div>
	<div class="col-md-1">
        <button type="button" class="btn btn-success pull-right" id="BotonSLAExportar" data-tipo="INSP">Exportar</button>
    </div>
</div>

<div class="col-md-15">
    <table data-toggle="table" data-sort-order="desc">
        <thead>
            <tr>    
              <th data-align="right" data-sortable="true">Zona</th>
              <th data-align="right" data-sortable="true">Especialidad</th>
              <th data-align="right" data-sortable="true">Total<br/>Mantenimientos</th>
              <th data-align="right" data-sortable="true">Total<br/>Mantenimientos<br/>Aprobados</th>
              <th data-align="right" data-sortable="true">Total<br/>Mantenimientos<br/>Rechazados</th>
              <th data-align="right" data-sortable="true">Total<br/>Mantenimientos<br/>No realizados</th>
              <th data-align="right" data-sortable="true">Total<br/>Inspecciones<br/>Rechazadas</th>
              <th data-align="right" data-sortable="true">Calidad [%]</th>              
            </tr>
        </thead>
        <tbody>
            {{#detalle}}
            <tr>
                <td>{{zona_nombre}}</td>
                <td>{{espe_nombre}}</td>
                <td>{{total_mant}}</td>
                <td>{{total_mant_aprobados}}</td>
                <td>{{total_mant_rechazados}}</td>
                <td>{{total_mant_no_realizados}}</td>
                <td>{{total_insp_rechazadas}}</td>
                <td>{{tasa_calidad}} %</td>
            </tr> 
            {{/detalle}}
        </tbody>
    </table>
</div>

<style>
.fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
    line-height: 15px;
    text-align: center;
}
</style>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>    
<script src="js/sla_exportar.js" type="text/javascript" charset="utf-8"></script>    

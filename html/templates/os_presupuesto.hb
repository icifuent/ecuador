<div class="row siom-section-header">
	<div class="col-md-5">
		<h5>Presupuesto orden de servicio</h5>
	</div>
	<div class="col-md-7">

	</div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
	<legend>Emplazamiento</legend>
	{{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Orden de servicio</legend>
	{{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Presupuesto</legend>
	<form role="form" action="#/os/presupuesto/{{data.orse_id}}" method="POST" id="form_presupuesto" class="form siom-form-tiny">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<select id="grupo-lpu" class="selectpicker" data-width="100%" name="grup_id" title="Seleccione grupo">
						<option value=""></option>
						{{#data.lpu_grupo}}
						<option value="{{lpgr_id}}">{{lpgr_nombre}}</option>
						{{/data.lpu_grupo}}
					</select>
				</div>

				<div class="form-group">
					<select id="subgrupo-lpu" class="selectpicker" data-width="100%" name="subg_id" data-hide-disabled="true" title="Seleccione subgrupo">
						<option value="" data-default="true"></option>
						{{#data.lpu_subgrupo}}
						<option value="{{lpgr_id}}" data-grupo="{{lpgr_id_padre}}">{{lpgr_nombre}}</option>
						{{/data.lpu_subgrupo}}
					</select>
				</div>

				<div class="form-group">
					<input autocomplete="off" type="text" class="form-control input-sm" id="filtro-lpu">
				</div>

				<div class="form-group">
					<div class="siom-os-lista siom-os-presupuesto-lista siom-lista-full-height-header" id="lista-lpu" style="overflow:auto !important">
						{{#each data.lpu}}
						<div class="siom-os-presupuesto" data-grupo="{{lpgr_id_padre}}" data-subgrupo="{{lpgr_id}}" id="item_{{lpit_id}}" data-id="{{lpit_id}}"
						 data-nombre="{{lpit_nombre}}">
							<div class="grupo">{{lpgr_nombre_padre}}</div>
							<div class="nombre">{{lpit_nombre}}</div>
							<div class="subgrupo">{{lpgr_nombre}}</div>
							<div class="unidad">Unidad:
								<strong>{{lpit_unidad}}</strong>
							</div>
							<div class="precios">
								{{#each precios}}
								<button type="button" class="btn btn-primary btn-xs precio" data-lpit-id="{{../lpit_id}}" data-lpit-nombre="{{../lpit_nombre}}"
								 data-lpit-grupo="{{../lpgr_nombre_padre}}" data-lpit-subgrupo="{{../lpgr_nombre}}" data-lpip-id="{{lpip_id}}" data-lpip-precio="{{lpip_precio}}"
								 data-lpgc-nombre="{{lpgc_nombre}}">
									<small>{{lpgc_nombre}} |
										<strong>{{lpip_precio}} </strong>
									</small>
								</button>
								{{/each}}
							</div>
						</div>
						{{/each}}
					</div>
				</div>

			</div>


			<div class="col-md-8">

				<!--div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpgcNombre" placeholder="lpgcNombre" value="PUNTO BAREMO" style="display:none" />
			  </div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpipId" placeholder="lpipId" value="1" style="display:none" />
			  </div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpitSubgrupo" placeholder="lpitSubgrupo"  style="display:none" value="Automaticamente" />
			  </div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpitGrupo" placeholder="lpitGrupo"  style="display:none" value="Generado" />
			  </div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpitNombre" placeholder="Actividad" />
			  </div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpipPrecio" placeholder="Valor Unitario" />
			  </div>
			  <div class="col-sm-7">
				<input   type="text" class="form-control" name="lpitId" placeholder="lpitId" value="1" style="display:none" />
			  </div>

				<button id="buttonAgregarItem" type="button" class="btn btn-primary btn-xs precio">
				  Agregar Item
				</button>
			</div-->
				<div class="siom-os-presupuesto-header">
					<table width="100%">
						<tr>
							<th class="item">Nº</th>
							<th class="actividad">Actividad</th>
							<th class="categoria">Categoria</th>
							<th class="precio">Valor unitario</th>
							<th class="cantidad">Cantidad</th>
							<th class="subtotal">Subtotal</th>
							<th class="eliminar"></th>
						</tr>
					</table>
				</div>
				<div class="siom-os-lista siom-os-presupuesto-lista-items siom-lista-full-height-header" id="lista_presupuesto" style="overflow:auto !important">
				</div>

				<div class="siom-os-presupuesto-footer">
					<table width="100%">
						<tr>
							<td align="left" class="items" id="total_items">0 items ingresados</td>
							<td align="right" class="total" id="total_presupuesto" data-total="0">TOTAL 0 UF</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<hr>
		<div class="row text-center siom-form-actions">
			<div class="col-md-12">
				<a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
				<button type="submit" id="submit_presupuesto" class="btn btn-primary" data-loading-text="Agregando presupuesto..." >Agregar presupuesto</button>
			</div>
		</div>
	</form>
</fieldset>
{{else}}
<div class="alert alert-danger" role="alert">
	{{data.error}} {{#if data.debug}}
	<br>
	<small>{{data.debug}}</small>
	{{/if}}
</div>
{{/if}}

<script src="js/os_presupuesto.js" type="text/javascript" charset="utf-8"></script>
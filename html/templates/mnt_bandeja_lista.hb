<div class="panel panel-default" >
    <div class="panel-body siom-mnt-lista siom-lista-full-height" style="overflow:auto !important">
	    {{#mantenimientos}}
            <div class="mnt">
                <div class="estado text-center">
                    <span class="id">Nº {{mant_id}}</span>
  		  <span class="label label-{{mntColorStatus mant_fecha_programada mant_estado}} status">{{mant_estado}}</span>
      </div>
      <div class="acciones text-right">
	  	  <div class="dropdown">
          {{#siomChequearPerfil "#/mnt/detalle/*"}}
	  	      <a href="#/mnt/detalle/{{mant_id}}" class="detalle">Ver detalle</a>
          {{/siomChequearPerfil}}

          {{#siomChequearPerfil "#/mnt/mas_opciones"}}
            
		        <a id="more_options_{{mant_id}}" href="#" class="dropdown-toggle mas_opciones" data-toggle="dropdown"> Más opciones</a>
		        <ul class="dropdown-menu" role="menu" aria-labelledby="more_options_{{mant_id}}">
            
            {{#siomChequearPerfil "#/mnt/editar/*"}}
              <li role="presentation"><a role="menuitem" tabindex="-1" href="#/mnt/editar/{{mant_id}}">Editar</a></li>
            {{/siomChequearPerfil}}
  		  	
            
            {{#mntChequearActivo mant_estado}}

              {{#siomChequearPerfil "#/mnt/asignacion/*"}}
                  {{#mntChequearActivaResponsable mant_responsable}}
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#/mnt/asignacion/{{mant_id}}">Asignar</a></li>
                  {{/mntChequearActivaResponsable}}
              {{/siomChequearPerfil}}

              {{#is mant_solicitud_informe_web 'APROBADA'}}
                {{#siomChequearPerfil "#/mnt/visita/*"}}
    		  	       <li role="presentation"><a role="menuitem" tabindex="-1" href="#/mnt/visita/{{mant_id}}">Ingresar visita</a></li>
                {{/siomChequearPerfil}}
              {{else}}
                {{#siomChequearPerfil "#/mnt/solicitud/informe/*"}}
                  {{#is mant_solicitud_informe_web 'NOSOLICITADA'}}
                    <li role="presentation">
                      <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#ModalSolicitud" data-mant-id="{{mant_id}}" data-tipo="informe"  data-title="Solicitud de subida de informe via Web">Solicitar subida de informe via Web</a>
                    </li>
                  {{/is}}
                  {{#is mant_solicitud_informe_web 'SOLICITANDO'}}
                    <li role="presentation" class="disabled">
                      <a role="menuitem" tabindex="-1" href="#" class="solicitando" disabled="true">Solicitando subida de informe via Web</a>
                    </li>
                  {{/is}}
                {{/siomChequearPerfil}}
              {{/is}}
            
              {{#siomChequearPerfil "#/mnt/solicitud/cambio_fecha_programada/*"}}
                {{#is mant_solicitud_cambio_fecha 'SOLICITANDO'}}
                  <li role="presentation" class="disabled">
                    <a role="menuitem" tabindex="-1" href="#" class="solicitando" disabled="true">Solicitando cambio de fecha</a>
                  </li>
                {{else}}
                  <li role="presentation">
                    <a role="menuitem" tabindex="-1" href="#" data-toggle="modal" data-target="#ModalSolicitudCambioFecha" data-mant-id="{{mant_id}}">Solicitar cambio de fecha</a>
                  </li>
                {{/is}}
              {{/siomChequearPerfil}}

              {{#siomChequearPerfil "#/mnt/adjuntar/*"}}
                <li role="presentation">
                  <a role="menuitem" tabindex="-1" href="#/mnt/adjuntar/{{mant_id}}">Agregar archivo(s)</a>
                </li>
              {{/siomChequearPerfil}} 
                  
              {{#siomChequearPerfil "#/mnt/anular/*"}}
                <!--
                <li role="presentation" class="divider"></li>
                -->
                <li role="presentation">
                    <a class="siom-anular-mnt" role="menuitem" tabindex="-1" data-mant-id="{{mant_id}}">Anular</a>
                </li>
              {{/siomChequearPerfil}}                      
  		        </ul>
          {{/mntChequearActivo}}
            {{/siomChequearPerfil}}
      </div>

		  <div class="modal fade" id="confirmModalAnular" tabindex="-1" role="dialog">
			  <div class="modal-dialog">
				  <div class="modal-content">
					  <div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal">
							  <span aria-hidden="true">&times;</span>
							  <span class="sr-only">Close</span>
						  </button>
						  <h4 class="modal-title" style="text-align:left;" id="confirmModalAnularTitle">
							  Ingrese comentario para la anulacion
						  </h4>
					  </div>
					  <div class="modal-body">
						  <textarea id="coment_anulacion" class="form-control" rows="6" ></textarea>
					  </div>
					  <div class="modal-footer">
  						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  						<button type="button" class="btn btn-primary" id="confirm_anular">aceptar</button>
					  </div>
				  </div>
			  </div>
		  </div>
		  
      {{#mntChequearActivo mant_estado}}
        {{#if tarea}}
					{{#each tarea}} 
						{{#siomChequearTarea this}}
					    	<a class="btn btn-primary btn-xs" href="{{@link}}" role="button">{{capitalizeFirst @texto}}</a>
          {{/siomChequearTarea}}  
					{{/each}}
					{{#siomChequearTareaAlt tarea}}
						<div class="esperando-tarea">{{uppercase @texto_alt}}</div>
					{{/siomChequearTareaAlt}}

        {{else}}
          <div class="sin-tarea">SIN TAREA PENDIENTE</div>
        {{/if}}
          <div class="timeago fecha" title="{{mant_fecha_programada}}">{{timeAgo mant_fecha_programada}}</div>
        {{else}}
          <div class="sin-tarea">FINALIZADA</div>
          <div class="timeago fecha" title="{{mant_fecha_programada}}">{{timeAgo mant_fecha_programada}}</div>
      {{/mntChequearActivo}}
     
      
	  </div>
      <div class="info text-left">
  	  	<h4>

  	  	{{#is mant_responsable 'MOVISTAR'}}
          <i class="glyphicon glyphicon-home"></i>&nbsp; 
        {{else}}
          <i class="glyphicon glyphicon-user"></i>&nbsp;
        {{/is}}

  	  	{{empl_nombre}} <small>{{empl_nemonico}}</small></h4>
  	  	<div class="info_os"><b>{{clas_nombre}}</b> | {{espe_nombre}} - {{peri_nombre}}</div>
        <div class="info_adicional">Dirección: <strong>{{empl_direccion}}</strong><br>
  	                                Fecha inicio: <strong>{{formatDate mape_fecha_inicio "%d-%m-%Y"}}</strong>,  
                                    Fecha programada: <strong>{{formatDate mant_fecha_programada "%d-%m-%Y"}}</strong>, 
                                    Fecha cierre: <strong>{{formatDate mape_fecha_cierre "%d-%m-%Y"}}</strong>    
  	    </div>
  	  </div>
    </div>
	 <hr>
  {{/mantenimientos}}
  </div>
  <div class="panel-footer siom-paginacion">
  	<div class="row">
  	  <div class="col-md-4 text-left">
        {{#if status}}
        <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
      {{/if}}
      </div>
      <div class="col-md-8 text-right">
        <div class="pagination-info">
          Página {{pagina}}/{{paginas}} ({{total}} registros)
        </div>
  	    <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
  	  </div>
    </div>
  </div>
</div>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/mnt_bandeja_lista.js" type="text/javascript" charset="utf-8"></script>
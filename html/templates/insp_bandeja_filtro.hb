<form class="form-horizontal siom-form-tiny" role="form" action="#/insp/bandeja/filtro" method="POST" id="siom-form-insp-bandeja">

  <div class="form-group">
    <div class="col-sm-5">
      <button type="button" class="btn btn-primary btn-default" id="LimpiarInspBandeja">
        <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
    </div>
    <div class="col-sm-7">
      <button type="button" class="btn btn-primary btn-default" id="BuscarInspBandeja">
        <span class="glyphicon" aria-hidden="true"></span> Buscar
    </div>
  </div>

  <hr>

  <div class="form-group">
    <label for="id" class="col-sm-5 control-label">Nº inspeccion</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="insp_id" name="insp_id" value="{{insp_id}}">
    </div>
  </div>

  <div class="form-group">
    <label for="estado" class="col-sm-5 control-label">Estado</label>
    <div class="col-sm-7">
      <select class="selectpicker" data-width="100%" title='TODOS' name="insp_estado" data-value="{{data.estados}}">{{#estados}}
        <option value="{{.}}">{{.}}</option>
        {{/estados}}
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="fecha_inicio" class="col-sm-5 control-label">Fecha inicio</label>
    <div class="col-sm-7">
      <input autocomplete="off" id="fecha_inicio" class="form-control" size="14" type="text" data-date-autoclose="true" name="insp_fecha_solicitud_inicio"
      />
    </div>
  </div>

  <div class="form-group">
    <label for="fecha_fin" class="col-sm-5 control-label">Fecha fin</label>
    <div class="col-sm-7">
      <input autocomplete="off" id="fecha_fin" class="form-control" size="14" type="text" data-date-autoclose="true" name="insp_fecha_solicitud_termino"
      />
    </div>
  </div>



  <div class="form-group">
    <label class="col-sm-5 control-label">Usuario Inspector</label>
    <div class="col-sm-7">
      <select class="selectpicker" data-width="100%" data-live-search="true" name="usuario_inspeccion" title="TODOS">
        <option value="">TODOS</option>
        {{#usuarios}}
        <option value="{{usua_id}}">{{usua_nombre}}</option>
        {{/usuarios}}
      </select>
    </div>
  </div>

  <hr>

  <div class="form-group">
    <label class="col-sm-5 control-label">Nº Mantenimiento</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="mant_id" name="mant_id">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-5 control-label">Tecnico Mantenimiento</label>
    <div class="col-sm-7">
      <select class="selectpicker" data-width="100%" data-live-search="true" name="tecnico_mantenimiento" title="TODOS">
        <option value="">TODOS</option>
        {{#tecnicos}}
        <option value="{{usua_id}}">{{usua_nombre}}</option>
        {{/tecnicos}}
      </select>
    </div>
  </div>

  <hr>

  <div class="form-group">
    <label class="col-sm-5 control-label">Nº OS</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="orse_id" name="orse_id">
    </div>
  </div>

  <div class="form-group">
    <label for="tipo" class="col-sm-5 control-label">Tipo OS</label>
    <div class="col-sm-7">
      <select class="selectpicker" data-width="100%" name="orse_tipo" title="Todos">
        <option value="">TODOS</option>
        {{#tipos}}

        <option value="{{.}}">{{.}}</option>

        {{/tipos}}
      </select>
    </div>
  </div>
  <hr>
  <div class="form-group">
    <label class="col-sm-5 control-label">Nombre Emplazamiento</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="empl_nombre" name="empl_nombre" value="{{data.empl_nombre}}">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-5 control-label">Nemonico</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="empl_nemonico" name="empl_nemonico" value="{{data.empl_nemonico}}">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-5 control-label">Direccion</label>
    <div class="col-sm-7">
      <input autocomplete="off" type="text" class="form-control" id="empl_direccion" name="empl_direccion" value="{{data.empl_direccion}}">
    </div>
  </div>

  <div class="form-group">
    <label class="col-sm-5 control-label">Comuna</label>
    <div class="col-sm-7">
      <select class="selectpicker" data-width="100%" data-live-search="true" name="comu_id" title="TODAS">
        <option value="">TODAS</option>
        {{#comunas}}
        <option value="{{comu_id}}">{{comu_nombre}}</option>
        {{/comunas}}
      </select>
    </div>
  </div>

</form>
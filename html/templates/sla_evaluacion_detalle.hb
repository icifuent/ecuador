<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Detalle</h5>
    </div>
</div>
	
<div class="col-md-11" id="escala" style="float: left; padding-top: 5px;  padding-bottom: 5px;padding-right: 0px;padding-left: 0px;">
    <table class="table table-bordered" id="tabla_escala"  data-datos="{{JSON escala_nota}}" style="font-size:75%;">
        <thead id="head_escala">
            <tr>					
				<th rowspan="2">Zona</th>
				<th rowspan="2">Especialidad</th>
                <th colspan="3">Ejecución MPP</th>
                <th colspan="2">SLA</th>
                <th colspan="2">CALIDAD</th>
                <th colspan="3">PPRR e IMAGEN</th>
				<th rowspan="2">NOTA Final</th>	
				<th rowspan="2">Calificacion</th>					
            </tr>
            <tr>   
				<th>AA,AB,AC,AD</th>
				<th>BA,BB,BC,BD</th>
				<th>CRONOGRAMA MPP</th>
				<th>OSU</th>
				<th>OSN</th>
				<th>AUDITORIA MPP</th>
				<th>AUDITORIA OS</th>
				<th>AUDITORIA PPRR</th>
				<th>PERSONAL ACTUALIZADO</th>
				<th>PRESENTACION PERSONAL</th>
            </tr>
        </thead>
        <tbody style="text-align:center;" id="tbody_escala">
		{{#notas}}
			<tr>
				<!--
				<td>{{zona_nombre}}</td>
				<td>{{espe_nombre}}</td>					
				<td>{{nota_ejecucion_aa_ab_ac_ad}}</td>
				<td>{{nota_ejecucion_ba_bb_bc_bd}}</td>
				<td>{{nota_cronograma}}</td>
				<td>{{nota_tiempos_osu}}</td>
				<td>{{nota_tiempos_osn}}</td>
				<td>{{nota_calidad_mpp}}</td>
				<td>{{nota_calidad_os}}</td>
				<td>{{nota_calidad_pprr}}</td>
				<td>{{nota_personal}}</td>
				<td>{{nota_presentacion}}</td>
				<td>{{nota_final}}</td>-->
				
				<td>{{zona_nombre}}</td>
				<td>{{espe_nombre}}</td>					
				<td>{{nota_ejecucion_aa_ab_ac_ad}}{{#ifequal nota_ejecucion_aa_ab_ac_ad "(10)"}} {{else}} <br/>({{tasa_ejecucion_aa_ab_ac_ad}})% {{/ifequal}}</td>
				<td>{{nota_ejecucion_ba_bb_bc_bd}}{{#ifequal nota_ejecucion_ba_bb_bc_bd "(10)"}} {{else}} <br/>({{tasa_ejecucion_ba_bb_bc_bd}})% {{/ifequal}}</td>
				<td>{{nota_cronograma}}{{#ifequal nota_cronograma "(10)"}} {{else}} <br/>({{tasa_cronograma}})% {{/ifequal}}</td>
				<td>{{nota_tiempos_osu}}{{#ifequal nota_tiempos_osu "(10)"}} {{else}} <br/>({{tasa_tiempos_osu}})% {{/ifequal}}</td>
				<td>{{nota_tiempos_osn}}{{#ifequal nota_tiempos_osn "(10)"}} {{else}} <br/>({{tasa_tiempos_osn}})% {{/ifequal}}</td>
				<td>{{nota_calidad_mpp}}{{#ifequal nota_calidad_mpp "(10)"}} {{else}} <br/>({{tasa_calidad_mpp}})% {{/ifequal}}</td>
				<td>{{nota_calidad_os}}{{#ifequal nota_calidad_os "(10)"}} {{else}} <br/>({{tasa_calidad_os}})% {{/ifequal}}</td>
				<td>{{nota_calidad_pprr}}{{#ifequal nota_calidad_pprr "(10)"}} {{else}} <br/>({{tasa_calidad_pprr}})% {{/ifequal}}</td>
				<td>{{nota_personal}}{{#ifequal nota_personal "(10)"}} {{else}} <br/>({{tasa_personal}})% {{/ifequal}}</td>
				<td>{{nota_presentacion}}{{#ifequal nota_presentacion "(10)"}} {{else}} <br/>({{tasa_presentacion}})% {{/ifequal}}</td>
				<td>{{nota_final}}</td>
				<td>{{calificacion}}</td>
				
				</tr>
		{{/notas}}
        </tbody>
    </table>
	
</div>
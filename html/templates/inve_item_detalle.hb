

<table width="100%">
  <tr>
      <td width="20%">
        <div class="siom-label">CODIGO</div>
        <div class="siom-value">{{empty inel_codigo}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">NOMBRE</div>
        <div class="siom-value">{{empty inel_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">UBICACIÓN</div>
        <div class="siom-value">{{empty inel_ubicacion}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">FECHA</div>
        <div class="siom-value">{{empty inel_fecha}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ESTADO</div>
        <div class="siom-value">{{empty inel_estado}}</div>
      </td>
  </tr>
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">EMPLAZAMIENTO</div>
        <div class="siom-value">{{empty empl_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">DESCRIPCIÓN</div>
        <div class="siom-value">{{empty inel_descripcion}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">MODULO</div>
        <div class="siom-value">{{empty inel_fuente_modulo}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ID</div>
        <div class="siom-value">{{empty inel_fuente_id_relacionado}}</div>
      </td>
      
  </tr>


</table>

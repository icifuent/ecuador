<table width="100%" class="siom-os-presupuesto-item" id="{{lpipId}}" data-lpip-id="{{lpipId}}" style="overflow:auto !important">
  <tr>
    <td class="actividad">
      <label class="nombre">{{lpitNombre}}</label>
      <br>
      <input  type="hidden" name="lpitNombre" value="{{lpitNombre}}" />
      <label class="comentario">{{lpitGrupo}} / {{lpitSubgrupo}}</label>
    </td>
    <td class="categoria">{{lpgcNombre}}</td>
    <td class="precio" id="precio-{{lpipId}}" data-precio="{{lpipPrecio}}">{{lpipPrecio}}</td>
    <input  type="hidden" name="lpipPrecio" value="{{lpipPrecio}}" />
    <td class="cantidad">
      <input  type="hidden" name="lpip_id" value="{{lpipId}}" />
      <input autocomplete="off" type="text" class="form-control input-xs vcenter text-center" name="prit_cantidad" id="cantidad-{{lpipId}}"
        value="1" data-id="{{lpipId}}" />
    </td>
    <td class="subtotal" id="subtotal-{{lpipId}}">0</td>
    <td class="eliminar">
      <i class="glyphicon glyphicon-trash eliminar" data-id="{{lpipId}}"></i>
    </td>
</table>
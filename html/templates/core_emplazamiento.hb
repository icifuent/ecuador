<script src="<?php echo auto_version('js/bootstrap-wizard.js');?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo auto_version('js/core_emplazamiento.js');?>" type="text/javascript" charset="utf-8"></script>

<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Editar Emplazamientos</h5>
    </div>
    {{#data}}
    <div class="col-md-7 text-right" id="AgregarEmplazamiento">
        <a class="btn btn-primary btn-sm" href="#/core/emplazamiento" id="BotonEmplazamientoAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-data="{{JSON .}}"></span> Agregar
        </a>
    </div>
    {{/data}}

</div>

{{#if data.status}}
<div id="WizardEmplazamiento" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" id="Primero" class="active">
            <span class="badge">1</span>Busqueda Emplazamientos
            <span class="chevron"></span>
        </li>
        <li data-step="2" id="Segundo">
            <span class="badge">2</span>Editar
            <span class="chevron"></span>
        </li>
        <li data-step="3" id="Tercero">
            <span class="badge">3</span>Contratos y Zonas de Pertenecia
            <span class="chevron"></span>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/emplazamiento/filtro" method="POST" id="FormEmplazamientoLista">

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-6">
                                        <input autocomplete="off" type="text" class="form-control" id="nombre" name="empl_nombre">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Nemónico</label>
                                    <div class="col-sm-6">
                                        <input autocomplete="off" type="text" class="form-control" id="nemonico" name="empl_nemonico">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Estado</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todos' name="empl_estado">
                                            <option value="">Todos</option>
                                            {{#data.estados}}
                                            <option value="{{.}}">{{.}}</option>
                                            {{/data.estados}}
                                        </select>
                                    </div>
                                </div>


                                <hr>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Macrositio</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todas' name="empl_macrositio">
                                            <option value="">Todos</option>
                                            <option value="1">SI</option>
                                            <option value="0">NO</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Subtel</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todas' name="empl_subtel">
                                            <option value="">Todos</option>
                                            <option value="1">SI</option>
                                            <option value="0">NO</option>
                                        </select>
                                    </div>
                                </div>


                                <hr>


                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Tecnología</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todas' multiple name="tecn_id">
                                            {{#data.tecnologias}}
                                            <option value="{{tecn_id}}">{{tecn_nombre}}</option>
                                            {{/data.tecnologias}}
                                        </select>
                                    </div>
                                </div>


                                <hr>


                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Dirección</label>
                                    <div class="col-sm-6">
                                        <input autocomplete="off" type="text" class="form-control" name="empl_direccion">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Región</label>
                                    <div class="col-sm-6">
                                        <select name="regi_id" class="selectpicker" data-width="100%" title='Todas'>
                                            <option value="">Todas</option>
                                            {{#data.regiones}}
                                            <option value="{{regi_id}}">{{regi_nombre}}</option>
                                            {{/data.regiones}}
                                        </select>
                                    </div>
                                </div>

                                <!--
								  <div class="form-group">
									<label class="col-sm-5 control-label">Zona</label>
									<div class="col-sm-6">
									  <select name="zona_id" class="selectpicker" data-width="100%" multiple title='Todas'>
										{{#data.zonas}}
										  <option value="{{zona_id}}">{{zona_nombre}}</option>
										{{/data.zonas}}
									  </select>
									</div>
								  </div>

								  <div class="form-group">
									<label class="col-sm-5 control-label">Cluster</label>
									<div class="col-sm-6">
									  <select name="clus_id" class="selectpicker" data-width="100%" multiple title='Todos'>
										{{#data.clusters}}
										  <option value="{{zona_id}}">{{zona_nombre}}</option>
										{{/data.clusters}}
									  </select>
									</div>
								  </div>
								  -->
                                <hr>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Categorías</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todas' name="cate_id">
                                            <option value="">Todas</option>
                                            {{#data.categorias}}
                                            <option value="{{cate_id}}">{{cate_nombre}}</option>
                                            {{/data.categorias}}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Clasificaciones</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todas' name="clas_id">
                                            <option value="">Todas</option>
                                            {{#data.clasificaciones}}
                                            <option value="{{clas_id}}">{{clas_nombre}}</option>
                                            {{/data.clasificaciones}}
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Dueños Torres</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todas' name="duto_id">
                                            <option value="">Todos</option>
                                            {{#data.duenos_torres}}
                                            <option value="{{duto_id}}">{{duto_nombre}}</option>
                                            {{/data.duenos_torres}}
                                        </select>
                                    </div>
                                </div>

                                <hr>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-emplazamiento-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">

            <script>
                function BotonEmplazamientoAction() {
                    CargaDatosContratosZonasEmplazamiento();
                    $('#WizardEmplazamiento').wizard('selectedItem', { step: 3 });
                    return true;
                }
            </script>
            <div class="col-md-12 text-right">
                <div class="acc-wizard-step">
                    <button class="btn btn-primary btn-md" type="submit" id="BotonEmplazamientoContrato" onclick="BotonEmplazamientoAction" style="margin-top:10px">Contratos y Zonas</button>
                </div>
            </div>


            <div class="panel panel-default">

                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/emplazamiento/edit" method="POST" id="FormEmplazamientoEditar">
                    <input  type="hidden" id="empl_id" name="empl_id" />
                    <input  type="hidden" id="usua_creador" name="usua_creador" />

                    <div class="col-md-9">
                        <fieldset class="siom-fieldset" id="siom-emplazamiento-info">
                            <legend>Emplazamiento</legend>
                            <table width="100%">

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Categoría</label>
                                            <div class="col-sm-6">
                                                <select id="cate_id" name="cate_id" class="selectpicker spEmplEditar" title='Seleccione Categorías...'>
                                                    {{#data.categorias}}
                                                    <option value="{{cate_id}}">{{cate_nombre}}</option>
                                                    {{/data.categorias}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Clasificacion</label>
                                            <div class="col-sm-6">
                                                <select id="clas_id" name="clas_id" class="selectpicker spEmplEditar" title='Seleccione clasificaciones...'>
                                                    {{#data.clasificaciones}}
                                                    <option value="{{clas_id}}">{{clas_nombre}}</option>
                                                    {{/data.clasificaciones}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Dueño Torre</label>
                                            <div class="col-sm-6">
                                                <select id="duto_id" name="duto_id" class="selectpicker spEmplEditar" title='Seleccione Dueño Torre...'>
                                                    {{#data.duenos_torres}}
                                                    <option value="{{duto_id}}">{{duto_nombre}}</option>
                                                    {{/data.duenos_torres}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tecnologías</label>
                                            <div class="col-sm-6">
                                                <select id="tecn_id" name="tecn_id" class="selectpicker spEmplEditar" title='Sin Tecnologias' multiple>
                                                    {{#data.tecnologias}}
                                                    <option value="{{tecn_id}}">{{tecn_nombre}}</option>
                                                    {{/data.tecnologias}}
                                                </select>
                                            </div>
                                        </div>

                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nemonico</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_nemonico" name="empl_nemonico" type="text" class="form-control" placeholder="Nemonico"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nombre</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_nombre" name="empl_nombre" type="text" class="form-control" placeholder="Nombre" value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Dirección</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_direccion" name="empl_direccion" type="text" class="form-control" placeholder="Dirección"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Comuna</label>
                                            <div class="col-sm-6">
                                                <select id="comu_id" name="comu_id" class="selectpicker spEmplEditar" title='Seleccione una comuna...'>
                                                    {{#data.comunas}}
                                                    <option value="{{comu_id}}">{{comu_nombre}}</option>
                                                    {{/data.comunas}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Referencia</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_referencia" name="empl_referencia" type="text" class="form-control" placeholder="Referencia"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tipo</label>
                                            <div class="col-sm-6">
                                                <select id="empl_tipo" name="empl_tipo" class="selectpicker spEmplEditar" title='Seleccione tipo...'>
                                                    {{#data.tipos}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.tipos}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Tipo Acceso</label>
                                            <div class="col-sm-6">
                                                <select id="empl_tipo_acceso" name="empl_tipo_acceso" class="selectpicker spEmplEditar" title='Seleccione acceso...'>
                                                    {{#data.tipos_acceso}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.tipos_acceso}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nivel Criticidad</label>
                                            <div class="col-sm-6">
                                                <select id="empl_nivel_criticidad" name="empl_nivel_criticidad" class="selectpicker spEmplEditar" title='Seleccione nivel...'>
                                                    {{#data.criticidades}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.criticidades}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Espacio</label>
                                            <div class="col-sm-6">
                                                <select id="empl_espacio" name="empl_espacio" class="selectpicker spEmplEditar" title='Seleccione espacio...'>
                                                    {{#data.espacios}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.espacios}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Macrositio</label>
                                            <div class="col-sm-6">
                                                <select id="empl_macrositio" name="empl_macrositio" class="selectpicker spEmplEditar" title='Seleccione macrositio...'>
                                                    <option value="1">Si</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Subtel</label>
                                            <div class="col-sm-6">
                                                <select id="empl_subtel" name="empl_subtel" class="selectpicker spEmplEditar" title='Seleccione Subtel...'>
                                                    <option value="1">Si</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Distancia</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_distancia" name="empl_distancia" type="text" class="form-control" placeholder="Distancia"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Latitud</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_latitud" name="empl_latitud" type="text" class="form-control" placeholder="Latitud" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Longitud</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_longitud" name="empl_longitud" type="text" class="form-control" placeholder="Longitud"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Emplazamiento Atix</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_id_emplazamiento_atix" name="empl_id_emplazamiento_atix" type="text" class="form-control"
                                                    placeholder="Emplazamiento Atix" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Observación Ingreso</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empl_observacion_ingreso" name="empl_observacion_ingreso" type="text" class="form-control"
                                                    placeholder="Observacion Ingreso" value="">
                                            </div>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Estado</label>
                                            <div class="col-sm-6">
                                                <select id="empl_estado" name="empl_estado" class="selectpicker spEmplEditar" title='Seleccione estado...'>
                                                    {{#data.estados}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.estados}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Observación</label>
                                            <div class="col-sm-6">
                                                <textarea id="empl_observacion" name="empl_observacion" class="form-control" placeholder="Observación"></textarea>
                                            </div>
                                        </div>
                                    </td>

                                </tr>

                            </table>
                        </fieldset>
                    </div>

                    <div class="col-md-3">
                        <fieldset class="siom-fieldset" style="margin-top:20px;">
                            <legend>Ubicación</legend>
                            <i id="ubicacion"></i>
                        </fieldset>
                    </div>

                    <div class="row text-center">
                        <div class="col-md-12" style="margin-top:10px; margin-bottom:10px">
                            <button type="submit" class="btn btn-danger" id="BotonEmplazamientoCancelar">Cancelar</button>
                            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="BotonEmplazamientoGuardar">Guardar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <div class="step-pane sample-pane" data-step="3">
            <div class="panel panel-default">

                <div class="row">
                    <div class="col-md-12 text-right">
                        <select id="ListaContratos" class="selectpicker" data-container="body">
                        </select>

                        <button class="btn btn-primary" id="BotonAgregarEmplazamientoContrato">Agregar a contrato</button>
                    </div>
                </div>


                <ul class="nav nav-tabs" id="TabsContratosEmplazamiento">
                </ul>
                <div id="TabsContentContratosEmplazamiento" class="tab-content">
                </div>
            </div>
        </div>

    </div>
</div>
{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small> {{/if}}
</div>
{{/if}}
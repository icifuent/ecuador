<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" style="overflow:auto !important">
  	{{#data}}
  	<div class="os-crear" id="{{empr_id}}">
	  <div class="estado text-center">
	  	<span class="glyphicon glyphicon-stop status status-{{estadoColorStatus usua_estado}}" aria-hidden="true"></span>
	 </div>

	 <div class="acciones text-right">
       <button type="button" class="btn btn-default btn-xs"  data-id="{{usua_id}}" data-empr_id="{{empr_id}}" data-data="{{JSON .}}" id="BotonUsuarioEditar">
         <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
       </button>

       {{#is usua_estado 'ACTIVO'}}
       <button type="button" class="btn btn-default btn-xs"  data-id="{{usua_id}}" data-nombre="{{usua_nombre}}" data-estado="{{usua_estado}}" id="BotonUsuarioCambiarEstado">
         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
       </button>
       {{else}}
       <button type="button" class="btn btn-default btn-xs"  data-id="{{usua_id}}" data-nombre="{{usua_nombre}}" data-estado="{{usua_estado}}" id="BotonUsuarioCambiarEstado">
         <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
       </button>
       {{/is}}
	  </div>

	  <div class="info text-left">
	  	<h4>{{usua_nombre}} <small>{{usua_login}}</small></h4>
	  	<div class="info_adicional">Empresa: <strong>{{empr_nombre}}</strong>, Email: <strong>{{usua_correo_electronico}}</strong></div>
	  </div>

	  <hr>
	</div>

	{{/data}}
  </div>

   <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	<div class="pagination-info">
	    	<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
	    </div>
	  </div>
	  <div class="col-md-8 text-right">
	    <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>


</div>

<script src="js/core_usuario_lista.js" type="text/javascript" charset="utf-8"></script>

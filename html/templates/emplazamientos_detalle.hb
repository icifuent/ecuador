<div class="row siom-section-header">
    <div class="col-md-5">
      <h5>Detalle emplazamiento</h5>
    </div>
    <div class="col-md-7"></div>
</div>

{{#if data.status}}
<div class="row">
    <div class="col-sm-9">
      <div class="siom-section-subtitle">Resumen</div>
      <fieldset class="siom-fieldset">
          <legend>Información</legend>
          {{{loadTemplate "empl_descripcion" data.emplazamiento}}}
      </fieldset>
    </div>

  <div class="col-sm-3">
    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Ubicación</legend>
      <div id="siom-empl-detalle-mapa" class="siom-detalle-mapa" data-latitud="{{data.emplazamiento.empl_latitud}}" data-longitud="{{data.emplazamiento.empl_longitud}}" data-distancia="{{data.emplazamiento.empl_distancia}}"></div>
    </fieldset>
  </div>

    {{#siomChequearPerfil "#/emplazamientos/detalle/historico"}}
    <div class="col-sm-9">
        <fieldset class="siom-fieldset" id="bla" data-datos="{{JSON emplazamiento_historico}}">
            <legend>Historico</legend>
          
            {{#emplazamiento_historico}}
            <hr>
            <table width="100%">
				<tr> 
					<td width="40%">{{emhi_fecha}}</td>
					<td width="40%">{{usua_historico}}</td>
					<td><button class="Btn-detalle" id="{{emhi_id}}"><span id="span_{{emhi_id}}">ver</span></button></td>
				</tr>
            
            </table>
            <div style="display: none;" id="emhi_{{emhi_id}}" data-open="false">
				{{{loadTemplate "empl_historico" .}}}
            </div>
         
			{{/emplazamiento_historico}}
		  
      </fieldset>
    </div>
	{{/siomChequearPerfil}}	

</div>


{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
  </div>
{{/if}}

<script src="js/emplazamientos_detalle.js" type="text/javascript" charset="utf-8"></script>

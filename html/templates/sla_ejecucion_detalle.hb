<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script> 
<link rel="stylesheet" type="text/css" href="css/bootstrap-table.css" media="screen" />

<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Detalle</h5>
    </div>
	<div class="col-md-5">
        <button type="button" class="btn btn-success pull-right" id="BotonSLAExportar" data-tipo="MNT">Exportar</button>
    </div>
</div>

<div class="col-md-10" style="float: left; padding-top: 5px;  padding-bottom: 5px;padding-right: 0px;padding-left: 0px;">
	<table data-toggle="table" data-sort-order="desc">
        <thead>
            <tr>
              <th data-align="right" data-sortable="true">Zona</th>
              <th data-align="right" data-sortable="true">Especialidad</th>
              <th data-align="right" data-sortable="true">Total<br/>Mantenimientos</th>
              <th data-align="right" data-sortable="true">Total<br/>Mantenimientos<br/>Aprobados</th>
              <th data-align="right" data-sortable="true">Cumplimiento [%]</th>              
            </tr>
        </thead>
        <tbody>
            {{#detalle}}
            <tr>
                <td>{{zona_nombre}}</td>
                <td>{{espe_nombre}}</td>
                <td>{{total_mant}}</td>
                <td>{{total_mant_aprobados}}</td>
                <td>{{tasa_ejecucion}} %</td>
            </tr> 
			{{/detalle}}
        </tbody>
    </table>
</div>

<style>
.fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
    line-height: 15px;
    text-align: center;
}
</style>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>    
<script src="js/sla_exportar.js" type="text/javascript" charset="utf-8"></script>    

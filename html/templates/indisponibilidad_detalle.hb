<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Detalle Indisponibilidad</h5>
    </div>
    <div class="col-md-7">
    </div>
</div>
{{#if data.status}}
<div class="row">
    <div class="col-sm-9">
        <div class="siom-section-subtitle">Resumen</div>
        <fieldset class="siom-fieldset">
            <legend>Emplazamiento</legend>
            {{{loadTemplate "empl_descripcion" data.mantenimiento}}}
        </fieldset>
        <fieldset class="siom-fieldset">
            <legend>Mantenimiento</legend>
            {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
        </fieldset>
        <div class="siom-section-subtitle">Asignación</div>
        <fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Asignaciones</legend>
            {{#each data.asignacion}}
            <div class="row">
                <label class="col-sm-2 siom-label">FECHA ASIGNACIÓN</label>
                <label class="col-sm-2 siom-value">{{maas_fecha_asignacion}}</label>
                <label class="col-sm-2 siom-label">ASIGNADA POR</label>
                <label class="col-sm-3 siom-value">{{usua_nombre}}</label>
                <label class="col-sm-1 siom-label">ESTADO</label>
                <label class="col-sm-2 siom-value">{{maas_estado}}</label>
            </div>
            <div class="row">
                <label class="col-sm-2 siom-label">JEFE CUADRILLA</label>
                <label class="col-sm-2 siom-value">{{detalle.jefe}}</label>
                <label class="col-sm-2 siom-label">ACOMPAÑANTE(S)</label>
                <label class="col-sm-4 siom-value">
                {{#each detalle.acompanantes}}
                {{.}}<br>
                {{/each}}
                </label>
            </div>
            {{#gt ../data.asignacion.length 1}}
            <hr>
            {{/gt}}
            {{else}}
            <p class="siom-no-data">Sin asignaciones</p>
            {{/each}}
        </fieldset>
        <div class="siom-section-subtitle">Visita</div>
        <fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Visitas</legend>
            {{#each data.visitas}}
            <div class="row">
                <label class="col-sm-2 siom-label">FECHA DESPACHO</label>
                <label class="col-sm-2 siom-value">{{tare_fecha_despacho}}</label>
                <label class="col-sm-2 siom-label">JEFE DE CUADRILLA</label>
                <label class="col-sm-2 siom-value">{{usua_nombre}}</label>
                <label class="col-sm-2 siom-label">ESTADO</label>
                <label class="col-sm-1 siom-value">{{tare_estado}}</label>
                <label class="col-sm-1 siom-link">
                </label>
            </div>
            <div class="row siom-subtitle">
                <div class="col-sm-2">DETALLE</div>
            </div>
            {{#each detalle}}
            <div class="row">
                <label class="col-sm-3 col-md-offset-1 siom-value">{{rtfr_accion}}</label>
                <label class="col-sm-2 siom-label">FECHA</label>
                <label class="col-sm-2 siom-value">{{rtfr_fecha}}</label>
                <label class="col-sm-2 siom-label">ESTADO</label>
                <label class="col-sm-2 siom-value">{{#osEstadoDetalleVisita rtfr_estado}}{{/osEstadoDetalleVisita}}</label>
            </div>
            {{else}}
            <p class="siom-no-data">Sin ingreso a sitio</p>
            {{/each}}
            {{#gt ../data.visitas.length 1}}
            <hr>
            {{/gt}}
            {{else}}
            <p class="siom-no-data">Sin visitas</p>
            {{/each}}
        </fieldset>
        <div class="siom-section-subtitle">Informe</div>
        <fieldset class="siom-fieldset"  style="margin-top:20px;">
            <legend>Informes</legend>
            {{#each data.informes}}
            <table width="100%">
                <tr>
                    <td width="30%">
                        <div class="siom-label">FECHA CREACIÓN</div>
                        <div class="siom-value">{{info_fecha_creacion}}</div>
                    </td>
                    <td width="30%">
                        <div class="siom-label">CREADO POR</div>
                        <div class="siom-value">{{usua_creador}}</div>
                    </td>
                    <td width="35%">
                        <div class="siom-label">ESTADO</div>
                        <div class="siom-value">{{info_estado}}</div>
                    </td>
                    <td width="5%">
                        <a class="siom-link" href="#/mnt/informe/{{../data.mantenimiento.mant_id}}/ver/{{info_id}}"> VER </a>
                    </td>
                </tr>
                {{#isnt info_estado 'SINVALIDAR'}}
                <tr>
                    <td width="30%">
                        <div class="siom-label">FECHA VALIDACIÓN</div>
                        <div class="siom-value">{{info_fecha_validacion}}</div>
                    </td>
                    <td width="30%">
                        <div class="siom-label">VALIDADO POR</div>
                        <div class="siom-value">{{usua_validador}}</div>
                    </td>
                    <td width="35%">
                        <div class="siom-label">OBSERVACIÓN</div>
                        <div class="siom-value">{{info_observacion}}</div>
                    </td>
                    <td width="5%">
                    </td>
                </tr>
                {{/isnt}}
            </table>
            {{#gt ../data.informes.length 1}}
            <hr>
            {{/gt}}      
            {{else}}
            <p class="siom-no-data">Sin informes</p>
            {{/each}}
        </fieldset>
    </div>
</div>
</div>
</div>
{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
    <br><small>{{data.debug}}</small>
    {{/if}}
</div>
{{/if}}
<script src="js/mnt_detalle.js" type="text/javascript" charset="utf-8"></script>


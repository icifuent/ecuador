

{{#if data.status}}
  <div class="row">
      <div class="col-sm-9">
          {{{loadTemplate "template_emplazamiento" data.emplazamiento}}}

          <fieldset class="siom-fieldset">
              <legend>Item inventario</legend>

              {{#data.inventario}}
              
              {{{loadTemplate "inve_item_detalle" this}}}
              
               <div style="text-align:end">
                 {{#if @first}}
                 <a class="btn btn-primary btn-xs" href="#/inve/emplazamiento/{{empl_id}}/item/{{inel_id}}/editar">
                    Editar
                </a>
                {{else}}
                 <a disabled="true" class="btn btn-primary btn-xs" href="#/inve/emplazamiento/{{empl_id}}/item/{{inel_id}}/editar">
                    Editar
                 </a>
                {{/if}}
                
                {{#if caracteristicas}}
                   <button class="btn btn-primary btn-xs btn-mostrar-campos-adic" data-inel-id="{{inel_id}}">Ver campos adicionales</button>
                {{else}}
                   <button disabled="true" class="btn btn-primary btn-xs">Sin campos adicionales</button>
                {{/if}}
              </div>

              <fieldset class="siom-fieldset" id='caracteristicas_{{inel_id}}' style="display:none;">
                <legend>Campos adicionales</legend>
                <table width="100%">
                    <tr>
                      <th>GENERO</th>
                      <th>ESPECIE</th>
                      <th>CAMPO</th>
                      <th>VALOR</th>
                    </tr>
                    {{#caracteristicas}}
                    <tr>
                        <td width="25%">{{inec_genero}}</td>
                        <td width="25%">{{inec_especie}}</td>
                        <td width="25%">{{inec_campo}}</td>
                        <td width="25%">{{parseInventarioElementoCaracteristica inec_campo inec_valor}}</td>
                    </tr>
                    {{/caracteristicas}}
                </table>
              </fieldset>

              <hr>
              {{/data.inventario}}        
        </fieldset>
      </div>

      <div class="col-sm-3">
         {{{loadTemplate "template_mapa" data.emplazamiento }}}
      </div>
  </div>

{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
  </div>
{{/if}}

<script src="js/inve_detalle.js" type="text/javascript" charset="utf-8"></script>


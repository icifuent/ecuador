    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Visitas</legend>
      <table width="100%">
      {{#each this}}
        <tr>
            <td width="40%">
              <div class="siom-label">FECHA DESPACHO</div>
              <div class="siom-value">{{tare_fecha_despacho}}</div>
            </td>
            <td width="40%">
              <div class="siom-label">ASIGNADA A</div>
              <div class="siom-value">{{usua_nombre}}</div>
            </td>
            <td width="20%">
              <div class="siom-label">ESTADO</div>
              <div class="siom-value">{{tare_estado}}</div>
            </td>
        </tr>
        {{#if detalle}}
           </td>
                <td width="5%">
                <a class="siom-link" href="#/os/informe/verpreliminar/{{tare_id}}"> VER 2</a>
              </td>
        <tr>
          <td colspan="3">
            <div class="siom-subtitle-details">DETALLE VISITA</div>
            <table align="center" width="100%">
              <tr>
                <td width="40%">
                  <div class="siom-label">FORMULARIO</div>
                </td>
                <td width="40%">
                  <div class="siom-label">FECHA</div>
                </td>
                <td width="20%">
                  <div class="siom-label">ESTADO</div>
                </td>
            </tr>
              
        {{#each detalle}}
            <tr>
              <td width="40%">
                <div class="siom-value">{{rtfr_accion}}</div>
              </td>
              <td width="40%">
                <div class="siom-value">{{rfrr_fecha}}</div>
              </td>
              <td width="20%">
                <div class="siom-value">{{#osEstadoDetalleVisita rfrr_estado}}{{/osEstadoDetalleVisita}}</div>
              </td>
            </tr>
        {{/each}}
          </table>
        </tr>
        {{#gt this.length 1}}
        <hr>
        {{/gt}}
        {{/if}}

      {{else}}
      <p class="siom-no-data">Sin visitas</p>
      {{/each}}
      </table>
    </fieldset>

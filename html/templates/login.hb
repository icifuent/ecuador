﻿<div class="row">
    <div class="col-sm-4 col-md-offset-4" id="login">
        <div class="account-wall">
            <img src="img/logo-navbar.png" id="logo-siom">

            <form class="form-signin" action="#/login/post" method="POST" id="formLogin">
                {{#if error}}
                <div class="error text-danger">{{error}}</div>
                <br> {{/if}}

                <!-- <?php if(isset($_GET['CaptchaFail']) ?>

                <div>captcha Failed, please try again.</div>

                <?php } ?>      -->
                <label id="lblRecaptchaError"></label>

                <input autocomplete="off" type="text" class="form-control input-sm" maxlenght="20" id="inputUsername" name="user" placeholder="Usuario"
                    value="{{user}}" autofocus>

                <input autocomplete="off" type="password" class="form-control input-sm" placeholder="Password" name="pass" id="inputPassword"
                    value="{{pass}}">

                <div align="center" class="g-recaptcha" id="g-recaptcha" onload="javascript:Recaptcha.reload()" data-sitekey="6LfjymsUAAAAABkChP5UtQRaW2h0vt6p73eaHnZq"></div>
                <br/>

                <button class="btn btn btn-primary btn-block" type="submit" id="btnSubmitLogin" name="btnSubmitLogin">Ingresar</button>

                {{#if validating}}
                <div class="validating">
                    <img src="img/loading_small.gif" />
                </div>
                {{/if}}
            </form>


        </div>
        <div class="row" id="login-footer">
            <div class="col-md-3">
                <img src="img/logo-telefonica.png" id="logo-telefonica">
            </div>
            <div class="col-md-9">
                <p>D. Red/ G. Gestion de Redes y Servicios/ SG. Operación de Red</p>
            </div>
        </div>
    </div>

</div>


<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="js/login.js"></script>

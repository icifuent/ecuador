<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Cerrar inspección</h5>
  </div>
  <div class="col-md-7">
    
  </div>
</div>

{{#if data.status}}

{{{loadTemplate "template_emplazamiento" data.emplazamiento}}}

{{{loadTemplate "template_inspeccion" data.inspeccion}}}

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Informe</legend>
  {{#informe}}
  <div class="row">

    <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
    <label class="col-sm-2 siom-value">{{info_fecha_creacion}}</label>
    <label class="col-sm-2 siom-label">CREADO POR</label>
    <label class="col-sm-3 siom-value">{{usua_creador}}</label>
    <label class="col-sm-1 siom-label">ESTADO</label>
    <label class="col-sm-1 siom-value">{{info_estado}}</label>
    <label class="col-sm-1 siom-link">
      <a href="#/insp/informe/{{../data.inspeccion.insp_id}}/validar/{{info_id}}" > REVISAR </a>
    </label>
  </div>

  {{#isnt info_estado 'SINVALIDAR'}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
    <label class="col-sm-2 siom-value">{{info_fecha_validacion}}</label>
    <label class="col-sm-2 siom-label">VALIDADO POR</label>
    <label class="col-sm-2 siom-value">{{usua_validador}}</label>
  </div>
  {{/isnt}}
  
  {{#gt ../data.informe.length 1}}
  <hr>
  {{/gt}}
  {{/informe}}
  
</fieldset>

<hr>

<form class="form form-horizontal" role="form" action="#/insp/cerrar/{{data.inspeccion.insp_id}}" method="POST" id="siom-cerrar-form">
  <!--
  <div class="row text-center">
    <div class="col-md-10 col-md-offset-1">
        [DEFINIR TEXTO MNT]<br><br><br>
    </div>
  </div>
  --> 

  <div class="row text-center">
    <div class="col-md-6 col-md-offset-3">
        <textarea name="insp_observacion" class="form-control" rows="2" placeholder="Observación"></textarea>
        <br>
    </div>
  </div>

  <div class="row text-center">
    <div class="col-md-12">
      <input type="hidden" name="insp_estado" value=""/>
      {{!--
      <button class="btn btn-default" data-insp-estado="CANCELADA">Cancelar</button>
      --}}
      <a href="#/insp/bandeja" class="btn btn-default">Cancelar</a>
      <button class="btn btn-danger" data-insp-estado="RECHAZADA">Rechazar</button>
      <button class="btn btn-warning" data-insp-estado="ACEPTADA_REPAROS">Aprobar con reparos</button>
      <button class="btn btn-success" data-insp-estado="ACEPTADA">Aprobar</button>
    </div>
  </div>

</form>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/insp_cerrar.js" type="text/javascript" charset="utf-8"></script>

<fieldset class="siom-fieldset" style="margin-top:20px;">
    <legend>Detalle</legend>
    <form role="form" action="#/core/contrato/lpu" method="POST" id="FormContratoLPUDetalle" class="form siom-form-tiny">
        <div class="row">
            <div class="col-md-4">

                <select id="grupo-lpu" class="selectpicker" data-width="100%" name="grup_id" title="Seleccione grupo">
                    <option value=""></option>
                    {{#lpu_grupo}}
                    <option value="{{lpgr_id}}">{{lpgr_nombre}}</option>
                    {{/lpu_grupo}}
                </select>



                <select id="subgrupo-lpu" class="selectpicker" data-width="100%" name="subg_id" data-hide-disabled="true" title="Seleccione subgrupo">
                    <option value="" data-default="true"></option>
                    {{#lpu_subgrupo}}
                    <option value="{{lpgr_id}}" data-grupo="{{lpgr_id_padre}}">{{lpgr_nombre}}</option>
                    {{/lpu_subgrupo}}
                </select>

                <input autocomplete="off" type="text" class="form-control input-sm" id="filtro-lpu">

                <div class="siom-os-lista siom-os-presupuesto-lista siom-lista-full-height-header" id="lista-lpu">
                    {{#each lpu}}
                    <div class="siom-os-presupuesto" data-grupo="{{lpgr_id_padre}}" data-subgrupo="{{lpgr_id}}" id="item_{{lpit_id}}" data-id="{{lpit_id}}"
                        data-nombre="{{lpit_nombre}}">
                        <div class="grupo">{{lpgr_nombre_padre}}</div>
                        <div class="nombre">{{lpit_nombre}}</div>
                        <div class="subgrupo">{{lpgr_nombre}}</div>
                        <div class="unidad">Unidad:
                            <strong>{{lpit_unidad}}</strong>
                        </div>
                        <div class="precios">
                            {{#each precios}}
                            <button type="button" class="btn btn-primary btn-xs precio" data-lpit-id="{{../lpit_id}}" data-lpit-nombre="{{../lpit_nombre}}"
                                data-lpit-grupo="{{../lpgr_nombre_padre}}" data-lpit-subgrupo="{{../lpgr_nombre}}" data-lpip-id="{{lpip_id}}"
                                data-lpip-precio="{{lpip_precio}}" data-lpgc-nombre="{{lpgc_nombre}}">
                                <small>{{lpgc_nombre}} |
                                    <strong>{{lpip_precio}} UF</strong>
                                </small>
                            </button>
                            {{/each}}
                        </div>
                    </div>
                    {{/each}}
                </div>


            </div>
            <div class="col-md-8">

            </div>
        </div>

    </form>
</fieldset>
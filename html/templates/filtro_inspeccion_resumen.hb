<div id="siom-form-emplazamientos-filtro">



                <div class="form-group">
                  <label for="tecnoclogia" class="col-sm-5 control-label">Zona contrato</label>
                  <div class="col-sm-7">
                    <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas'  name="cont_id">
                      {{#contratos}}
                        <option value="{{cont_id}}">{{tecn_nombre}}</option>
                      {{/contratos}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="tecnoclogia" class="col-sm-5 control-label">Zona movistar</label>
                  <div class="col-sm-7">
                    <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas'  name="zona_id">
                      <option value="">Todas</option>                      
                      {{#data.tecnologias}}
                        <option value="{{tecn_id}}">{{tecn_nombre}}</option>
                      {{/data.tecnologias}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="tecnoclogia" class="col-sm-5 control-label">Region</label>
                  <div class="col-sm-7">
                    <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas'  name="regi_id">
                      <option value="">Todas</option>
                      {{#regiones}}
                        <option value="{{regi_id}}">{{regi_nombre}}</option>
                      {{/regiones}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="zona" class="col-sm-5 control-label">Cluster</label>
                  <div class="col-sm-7">
                    <select name="zona_id" class="selectpicker" data-width="100%" title='Todas'>
                      <option value="">Todas</option>
                      {{#cluster}}
                        <option value="{{clus_id}}">{{clus_nombre}}</option>
                      {{/cluster}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="region" class="col-sm-5 control-label">Comuna</label>
                  <div class="col-sm-7">
                    <select name="comu_id" class="selectpicker" data-width="100%" title='Todas' data-container="body">
                      <option value="">Todas</option>
                      {{#comunas}}
                        <option value="{{comu_id}}">{{comu_nombre}}</option>
                      {{/comunas}}
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="cluster" class="col-sm-5 control-label">Usuario</label>
                  <div class="col-sm-7">
                    <select name="usua_id" class="selectpicker" data-width="100%" title='Todos'>
                      <option value="">Todos</option>
                      {{#usuario}}
                        <option value="{{usua_id}}">{{usua_nombre}}</option>
                      {{/usuario}}
                    </select>
                  </div>
                </div>
                <!--
                <div class="text-center">
                  <button type="submit" class="btn btn-primary" data-loading-text="Filtrando..." autocomplete="off">Filtrar</button>
                </div>
                -->
</div>
        <script src="js/filtro_inspeccion_resumen.js" type="text/javascript" charset="utf-8"></script>


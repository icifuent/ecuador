{{#if mantenimiento}}
  <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Mantenimiento</legend>
      {{{loadTemplate "mnt_descripcion" mantenimiento}}}
  </fieldset>
{{/if}}
<fieldset class="siom-fieldset"  style="margin-top:20px;">
    <legend>Inspección</legend>
    <table width="100%">
      <tr>
         <td width="20%">
            <div class="siom-label">Nº INSPECCIÓN</div>
            <div class="siom-value">{{insp_id}}</div>
          </td>
          <td width="20%">
            <div class="siom-label">TIPO</div>
            <div class="siom-value">{{insp_tipo}}</div>
          </td>
          <td width="20%">
            <div class="siom-label">EMPRESA</div>
            <div class="siom-value">{{empr_nombre}}</div>
          </td>
          <td width="20%">
            <div class="siom-label">ESPECIALIDAD</div>
            <div class="siom-value">{{espe_nombre}}</div>
          </td>
          <td width="20%" colspan="2">
            <div class="siom-label">FECHA SOLICITUD</div>
            <div class="siom-value">{{insp_fecha_solicitud}}</div>
          </td>
      </tr>
      <tr>
          <td width="20%">
            <div class="siom-label">USUARIO CREADOR</div>
            <div class="siom-value">{{empty usua_creador_nombre}}</div>
          </td>

          <td width="20%">
            <div class="siom-label">FECHA CREACIÓN</div>
            <div class="siom-value">{{empty insp_fecha_creacion}}</div>
          </td>

          <td width="20%">
            <div class="siom-label">USUARIO VALIDADOR</div>
            <div class="siom-value">{{empty usua_validador_nombre}}</div>
          </td>

          <td width="20%">
            <div class="siom-label">FECHA VALIDACION</div>
            <div class="siom-value">{{empty insp_fecha_validacion}}</div>
          </td>

          <td width="20%">
            <div class="siom-label">ESTADO</div>
            <div class="siom-value">{{empty insp_estado}}</div>
          </td>
      </tr>
      <tr>
          <td width="20%" colspan="1">
            <div class="siom-label">CONFORMIDAD</div>
            <div class="siom-value">{{#if insp_conformidad}} {{insp_conformidad}} % {{else}}-{{/if}}</div>
          </td>
          <td width="40%" colspan="3">
            <div class="siom-label">DESCRIPCIÓN</div>
            <div class="siom-value">{{insp_descripcion}}</div>
          </td>
          <td width="40%" colspan="2">
            <div class="siom-label">OBSERVACIÓN</div>
            <div class="siom-value">{{insp_observacion}}</div>
          </td>
      </tr>
      {{#if archivos}}
      <tr>
          <td width="100%" colspan="5" align="left" valign="top">
            <div class="siom-label">DOCUMENTACIÓN</div>
            <div class="siom-docu-link">
               {{#each archivos}}
                <a href="{{repo_ruta}}" target="blank">{{repo_nombre}}</a><small> | {{repo_descripcion}}</small><br>
               {{/each}}
            </div>
          </td>
      </tr>
      {{/if}}
    </table>              
</fieldset>
{{#if os}}
    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Orden de servicio</legend>
      {{{loadTemplate "os_descripcion" os}}}
    </fieldset>
{{/if}}

    
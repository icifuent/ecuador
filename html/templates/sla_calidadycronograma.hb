<div class="row siom-section-header">
	<div class="col-md-4">
		<h5>SLA: Calidad Y Cronograma</h5>
	</div>
</div>

<ul class="nav nav-tabs">
	<li class="active">
		<a class="BotonSLACalidadMenu" data-tipo_datos="grafico_zonas">Grafico</a>
	</li>
	<li>
		<a class="BotonSLACalidadMenu" data-tipo_datos="detalle">Detalle</a>
	</li>
	{{#siomChequearPerfil "#/sla/*/calidadycronograma"}}
	<li>
		<a class="BotonSLACalidadMenu" data-tipo_datos="validacion">Validacion</a>
	</li>
	{{/siomChequearPerfil}}
</ul>

<div class="row" style="margin-left: 5px; margin-top:20px;">
	<form class="form-horizontal siom-form-tiny" role="form" action="#/sla/calidadycronograma/filtro" method="POST" id="siom-form-sla-calidad-cronograma">
		<div class="col-md-14">
			<div class="col-md-3">
				<fieldset class="siom-fieldset" style="padding-bottom:20px; margin-top:0px;">
					<legend>Filtros</legend>
					<div class="form-group" id="FiltroEspecialidades" style="display:block">
						<label for="id" class="col-sm-5 control-label">Especialidades</label>
						<div class="col-sm-7">
							<select class="selectpicker" data-width="100%" multiple name="espe_id" title="TODAS" data-container="body">
								{{#especialidades_mnt}}
								<option value="{{espe_id}}">{{espe_nombre}}</option>
								{{/especialidades_mnt}}
							</select>
						</div>
					</div>
					<!--div class="form-group" id="FiltroEspecialidad" style="display:none">
							<div class="col-md-3" id="FiltroEspecialidad" style="display:none">
								<label for="id" class="col-sm-5 control-label">Especialidad</label>
								<div class="col-sm-7">
									<select class="selectpicker" data-width="100%" name="espe_idu" title="TODAS" data-container="body">
										{{#especialidades_mnt}}
										<option value="{{espe_id}}">{{espe_nombre}}</option>
										{{/especialidades_mnt}}
									</select>
								</div>
							</div>
						</div-->

					<!--div class="form-group" id="FiltroZona" style="display:none">
							<label for="id" class="col-sm-5 control-label">Zona</label>
							<div class="col-sm-7">
								<select class="selectpicker" data-width="100%" name="zona_id" title="TODAS" data-container="body">
									<optgroup label="MOVISTAR">
										{{#zonas_movistar}}
										<option value="{{zona_id}},zona">{{zona_nombre}}</option>
										{{/zonas_movistar}}
									</optgroup>
									<optgroup label="CONTRATO">
										{{#zonas_contrato}}
										<option value="{{zona_id}},zona">{{zona_nombre}}</option>
										{{/zonas_contrato}}
									</optgroup>
									<optgroup label="REGIONES">
										{{#regiones}}
										<option value="{{regi_id}},region">{{regi_nombre}}</option>
										{{/regiones}}
									</optgroup>
								</select>
							</div>
						</div-->

					<div class="form-group" id="FiltroZonaTipo" style="display:block">
						<!--div class="col-md-3" id="FiltroZonaTipo" style="display:block">
								<div>
									Tipo de zona
								</div-->
						<label for="id" class="col-sm-5 control-label">Tipo de Zona</label>
						<div class="col-sm-7">
							<select class="selectpicker" data-width="100%" name="zona_tipo">
								{{#zonas_tipos}}
								<option value="{{.}}">{{.}}</option>
								{{/zonas_tipos}}
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="id" class="col-sm-5 control-label">Inicio Periodo</label>
						<div class="col-sm-7">
							<input autocomplete="off" id="mant_fecha_validacion_inicio" class="form-control" size="14" type="text" name="mant_fecha_validacion_inicio"
							 value="{{#if data.mant_fecha_validacion_inicio}} {{formatDate data.mant_fecha_validacion_inicio '%d-%m-%Y'}} {{/if}}"
							/>
						</div>
					</div>
					<div class="form-group">
						<label for="id" class="col-sm-5 control-label">Fin Periodo</label>
						<div class="col-sm-7">
							<input autocomplete="off" id="mant_fecha_validacion_termino" class="form-control" size="14" type="text" name="mant_fecha_validacion_termino"
							 value="{{#if data.mant_fecha_validacion_termino}} {{formatDate data.mant_fecha_validacion_termino '%d-%m-%Y'}} {{/if}}"
							/>
						</div>
					</div>
					<div class="form-group" id="insp_id" style="display:none">
						<label for="id" class="col-sm-5 control-label">Nº Inspección</label>
						<div class="col-sm-7">
							<input autocomplete="off" type="text" class="form-control" name="insp_id" value="{{data.insp_id}}">
						</div>
					</div>
					<input type="hidden" id="tipo_datos" name="tipo_datos" value="grafico_zonas" />
					<!--input type="hidden" id="pagina" name="pagina" value="1"/-->
				</fieldset>
			</div>

			{{#if data.status}}
			<div class="col-md-9" id="sla-calidadycronograma-datos"></div>

			{{else}}
			<div class="alert alert-danger" role="alert">
				{{data.error}} {{#if data.debug}}
				<br>
				<small>{{data.debug}}</small> {{/if}}
			</div>
			{{/if}}

	</form>
	</div>
</div>

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/sla_calidadycronograma.js" type="text/javascript" charset="utf-8"></script>
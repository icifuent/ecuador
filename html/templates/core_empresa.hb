<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Editar Empresa</h5>
    </div>
    {{#data}}
    <div class="col-md-7 text-right" id="AgregarEmpresa">
        <a class="btn btn-primary btn-sm" href="#/core/empresa" id="BotonEmpresaAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-data="{{JSON .}}"></span> Agregar
        </a>
    </div>
    {{/data}}

</div>

{{#if data.status}}
<div id="WizardEmpresa" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" id="Primero" class="active">
            <span class="badge">1</span>Busqueda Empresa
            <span class="chevron"></span>
        </li>
        <li data-step="2" id="Segundo">
            <span class="badge">2</span>Editar
            <span class="chevron"></span>
        </li>
        <li data-step="3" id="Tercero">
            <span class="badge">3</span>Contratos y Zonas de Pertenecia
            <span class="chevron"></span>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/empresa/filtro" method="POST" id="FormEmpresaLista">

                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-6">
                                        <input autocomplete="off" type="text" class="form-control" id="nombre" name="empr_nombre">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Estado</label>
                                    <div class="col-sm-6">
                                        <select class="selectpicker" data-width="100%" title='Todos' name="empr_estado">
                                            <option value="">Todos</option>
                                            {{#data.estados}}
                                            <option value="{{.}}">{{.}}</option>
                                            {{/data.estados}}
                                        </select>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-empresa-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">
            <div class="panel panel-default">
                <div class="col-md-12 text-right">
                    <div class="acc-wizard-step">
                        <button class="btn btn-primary" type="submit" id="BotonEmpresaContrato" style="margin-top:10px">Contratos y zonas
                        </button>
                    </div>
                </div>
                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/empresa/edit" method="POST" id="FormEmpresaEditar">
                    <input  type="hidden" id="empr_id" name="empr_id" />
                    <input  type="hidden" id="usua_creador" name="usua_creador" />

                    <div class="col-md-9">
                        <fieldset class="siom-fieldset" id="siom-emplazamiento-info">
                            <legend>Empresa</legend>
                            <table width="100%">
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nombre</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_nombre" name="empr_nombre" type="text" class="siom-value" placeholder="Nombre" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Alias</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_alias" name="empr_alias" type="text" class="siom-value" placeholder="Alias" value="">
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Dirección</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_direccion" name="empr_direccion" type="text" class="siom-value" placeholder="Dirección"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Comuna</label>
                                            <div class="col-sm-6">
                                                <select id="comu_id" name="comu_id" class="selectpicker spEmprEditar" title='Seleccione una comuna...'>
                                                    {{#data.comunas}}
                                                    <option value="{{comu_id}}">{{comu_nombre}}</option>
                                                    {{/data.comunas}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Rut Empresa</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_rut" name="empr_rut" type="text" class="siom-value" placeholder="Rut" value="">
                                            </div>
                                        </div>
                                    </td>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Giro</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_giro" name="empr_giro" type="text" class="siom-value" placeholder="Giro" value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Observación</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_observacion" name="empr_observacion" type="text" class="siom-value" placeholder="Observacion"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contacto nombre</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_contacto_nombre" name="empr_contacto_nombre" type="text" class="siom-value" placeholder="Nombre"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contacto telefono fijo</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_contacto_telefono_fijo" name="empr_contacto_telefono_fijo" type="text" class="siom-value"
                                                    placeholder="Telefono fijo" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contacto telefono movil</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_contacto_telefono_movil" name="empr_contacto_telefono_movil" type="text" class="siom-value"
                                                    placeholder="Telefono movil" value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contacto direccion</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_contacto_direccion" name="empr_contacto_direccion" type="text" class="siom-value" placeholder="direccion"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contacto Email</label>
                                            <div class="col-sm-6">
                                                <input autocomplete="off" id="empr_contacto_email" name="empr_contacto_email" type="text" class="siom-value" placeholder="Email"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%" colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Estado</label>
                                            <div class="col-sm-6">
                                                <select id="empr_estado" name="empr_estado" class="selectpicker spEmprEditar" title='Seleccione estado...'>
                                                    {{#data.estados}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.estados}}
                                                </select>



                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                            </table>
                        </fieldset>
                    </div>


                    <div class="row text-center">
                        <div class="col-md-12" style="margin-top:10px; margin-bottom:10px">
                            <button type="submit" class="btn btn-primary" data-loading-text="Creando..."  id="BotonEmpresaGuardar">Guardar Empresa</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <div class="step-pane sample-pane" data-step="3">
            <div class="panel panel-default">
                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/emplazamiento/edit/contrato_zonas" method="POST"
                    id="FormEmplazamientoContratosZonas">
                    <fieldset class="siom-fieldset" id="FieldsetEmplazamientoContratos">
                        <legend>Contratos</legend>
                        <div class="row">
                            <div id="dual-list-box-employees" class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <label class="control-label">Lista de Contratos</label>
                                        <ul class="list-group siom-asignacion-list" id="ListaContratos"></ul>
                                    </div>

                                    <div class="col-md-5">
                                        <label class="control-label">Contratos de la Empresa</label>
                                        <ul class="list-group siom-asignacion-list" id="ListaContratosEmpresa"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>

    </div>
</div>
{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small> {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/core_empresa.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
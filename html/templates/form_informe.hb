{{#each formularios}}
<div class="siom-form">
	<table width="800px" align="center">
		<tr>
			<td class="siom-form-name" colspan="2">{{uppercase rtfr_accion}}</td>
		</tr>
		<tr>
			<td width="70%" valign="top">
				{{#each data}}
					<table width="100%">
					<tr>
						<td class="siom-group" colspan="2">{{uppercase @key}}</td>
					</tr>
					
					{{#each .}}
						{{#formNotAggregated fova_valor foit_opciones}}

							{{#is foit_tipo "CAMERA"}}	
								<tr>
								  	<td class="siom-label" colspan="2">{{uppercase foit_nombre}}</td>
								</tr>
							  	<tr>
							  		<td class="siom-image" colspan="2">
							  			<ul class="list-group siom-file-list ctrl-camera">
							  			{{#each fova_valor}}
							  			<li class="list-group-item">
							  			<a href="uploads/{{image}}" target="blank"><img src="uploads/{{image}}"/></a>
							  			<label>{{emptyForm comment}}</label>
							  			</li>
							  			{{/each}}
							  			</ul>
							  		</td>
							  	</tr>
							  	
							{{else}}
								{{#is foit_tipo "AGGREGATOR"}}	
								<tr>
								  	<td class="siom-label" colspan="2">{{uppercase foit_nombre}}</td>
								</tr>
								<tr>
							  		<td class="siom-aggregator" colspan="2">
							  		{{#each fova_valor}}
							  			<table width="100%">
							  			{{#each .}}
							  			{{#formAggregatorIsImage .}}
								  			<tr>
											  <td class="siom-label" colspan="2">{{formAggregatorLabel ../../foit_opciones @index}}</td>
											</tr>

										  	<tr>
										  		<td class="siom-image" colspan="2">
										  			<ul class="list-group siom-file-list ctrl-camera">
										  			{{#each .}}
										  			<li class="list-group-item">
										  			<a href="uploads/{{image}}" target="blank"><img src="uploads/{{image}}"/></a>
										  			<label>{{emptyForm comment}}</label>
										  			</li>
										  			{{/each}}
										  			</ul>
										  		</td>
										  	</tr>

							  			{{else}}
								  			<tr>
											  <td class="siom-label">{{formAggregatorLabel ../../foit_opciones @index}}</td>
											  <td class="siom-value">{{emptyForm .}}</td>
											</tr>
										{{/formAggregatorIsImage}}

							  			{{/each}}
							  			</table>
							  		{{/each}}
							  		</td>
							  	</tr>
								{{else}}
									<tr>
									  <td class="siom-label">{{uppercase foit_nombre}}</td>
									  <td class="siom-value">{{emptyForm fova_valor}}</td>
									</tr>
								{{/is}}
							{{/is}}

						{{/formNotAggregated}}
					{{/each}}
					
					</table>
				{{/each}}
			</td>
			<td width="30%" valign="top">
				<table width="100%">
					<tr>
						<td width="30%" class="siom-label">FECHA</td>
						<td class="siom-value">{{rfrr_fecha}}</td>
					</tr>
					<tr>
						{{#is fore_ubicacion 'web'}}
						<td class="siom-label">UBICACIÓN</td>
						<td class="siom-value">WEB</td>
						{{else}}
						<td class="siom-label" colspan="2">UBICACIÓN</td>
						{{/is}}
					</tr>
					{{#if fore_ubicacion.lat}}
					<tr>
						<td class="siom-label" colspan="2">	
							<div class="siom-informe-mapa col-sm-10 col-md-offset-1" data-latitud="{{fore_ubicacion.lat}}" data-longitud="{{fore_ubicacion.lng}}">
					      		Sin ubicación
					      	</div>
						</td>
					</tr>
					{{/if}}
				</table>
			</td>
		</tr>
	</table>
</div>
{{else}}
	<div class="alert alert-warning" role="alert">
  	<strong>Aviso</strong> no hay datos asociados a este informe
	</div>
{{/each}}
<script src="js/form_informe.js" type="text/javascript" charset="utf-8"></script>

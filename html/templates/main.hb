<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="row">
            <div class="col-md-6 text-left">
                <img class="navbar-logo" src="img/logo-navbar.png">
                <div class="hidden-xs navbar-logo-sub"> Sistema Integrador de Operación y Mantenimiento </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 text-right">
                <span class="siom-user">{{user}}</span>
                <ul class="nav navbar-nav navbar-right navbar-icons">
                    <li>
                        <a href="#/dashboard"><i class="fa fa-home"></i></a>
                    </li>
                    <li>
                        <a href="#/usuario"><i class="fa fa-user"></i></a>
                    </li>
                    <li>
                        <a href="#"> <i class="fa fa-question-circle"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="label label-{{#gt tasks.length 0}}danger{{else}}success{{/gt}} siom-tareas-cantidad" id="siom-numero-tareas">{{tasks.length}}</span><span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu siom-tareas-lista" role="menu" id="siom-lista-tareas">
                            {{#ListarTareas tasks}}
                            <li>
                                <div class="row">
                                    <div class="col-xs-8 text-left">
                                        <div class="tarea"><a href="{{@link}}">{{@texto}}</a></div>
                                        <div class="emplazamiento">{{empl_nombre}}</div>
                                        <div class="direccion">{{empl_direccion}}</div>
                                    </div>
                                    <div class="col-xs-4 text-right">
                                        <div class="id_relacionado"><b>{{tare_modulo}}</b> Nº {{tare_id_relacionado}}</div>
                                        <div class="fecha">{{timeAgo tare_fecha_despacho}}</div>
                                    </div>
                                   </div>
                            </li>
                            {{/ListarTareas}}
                        </ul>

                    </li>
                    <li>
                        <a href="#/logout"> <i class="fa fa-power-off"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="row content-menu">
        <div class="col-md-3 text-left">
            <form role="form" class="form form-inline form-contrato" action="#/contrato/cambiar" method="post">
                <div class="form-group">
                    <label>contrato</label>
                    <select class="form-control input-sm" name="cont_id" id="cont_id">
                        {{#each contracts}}
                        <option value="{{cont_id}}" {{#is cont_id ../contract}}selected{{/is}}>{{cont_nombre}}</option>
                        {{/each}}
                    </select>
                </div>
                <input type="submit" class="btn btn-default btn-xs" value="Cambiar" />
            </form>
        </div>
        <div class="col-md-9">
            <ul class="nav navbar-nav navbar-right menu">
                {{#each sections}}
                    {{#if sections}}
                        {{#siomChequearPerfil link}}
                        <li class="dropdown">
                            <a href="{{link}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{section}} <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">

                            {{#each sections}}
                                {{#if this.separator}}
                                <li class="divider"></li>
                                {{else}}
                                    {{#siomChequearPerfil link}}
                                    {{#if default}}
                                    <li class="active"><a href="{{this.link}}">{{this.section}} <span class="sr-only">(current)</span></a></li>
                                    {{else}}
                                        <li><a href="{{this.link}}">{{this.section}}</a></li>
                                    {{/if}}
                                    {{/siomChequearPerfil}}
                                {{/if}}
                            {{/each}}
                            </ul>
                        </li>
                        {{/siomChequearPerfil}}
                    {{else}}
                        {{#if separator}}
                        <li class="divider"></li>
                        {{else}}
                            {{#siomChequearPerfil link}}

                            {{#if default}}
                                <li class="active"><a href="{{link}}">{{section}} <span class="sr-only">(current)</span></a></li>
                            {{else}}
                                <li><a href="{{link}}">{{section}}</a></li>
                            {{/if}}

                            {{/siomChequearPerfil}}
                        {{/if}}
                    {{/if}}
                {{/each}}
            </ul>
        </div>
    </div>
</header>
<body>
    <div class="main-container">
        <div class="main-content" style="overflow:auto;">
            <div class="row" >
                <div class="col-md-12">
                {{#if partials}}
                    {{>section_template}}
                {{else}}
                    <div class="main-loading"><img src="img/loading.gif"/><label>Cargando</label></div>
                {{/if}}
                </div>
            </div>
        </div>
    </div>

    <footer id="footer" style="overflow:hidden;position:absolute;">
        <div class="col-md-10">
            <img src="img/logo-telefonica.png" class="footer-logo">
            <div  class="footer-logo-sub">D. Red/ G. Gestion de Redes y Servicios/ SG. Operación de Red</div>
        </div>
        <div class="col-md-2 text-right footer-version">
            versión CLOUD {{version}}
        </div>
    </footer>

    <div id="alertModal" class="modal fade siom-modal-alert">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">SIOM</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm"  data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>

    <div id="confirmModal" class="modal fade siom-modal-confirm">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">SIOM</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm"  data-status="false">Cancelar</button>
            <button type="button" class="btn btn-primary btn-sm"  data-status="true">Aceptar</button>
          </div>
        </div>
      </div>
    </div>

    <div id="successModal" class="modal fade siom-modal-success">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">SIOM</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm"  data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>

    <div id="errorModal" class="modal fade siom-modal-error">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">SIOM</h4>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm"  data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
</body>
<script src="js/main.js" type="text/javascript" charset="utf-8"></script>

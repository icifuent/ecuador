<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap-table.css" media="screen" />

<div class="row siom-section-header">
	<div class="col-md-5">
		<h5>Validacion</h5>
	</div>
</div>

<form class="form-horizontal siom-form-tiny" role="form" method="POST">

	<div class="panel panel-default" style="overflow: hidden;">
		<div class="panel-body siom-os-lista">
			<table class="table table-striped table-hover table-condensed" data-toggle="table" data-sort-order="desc" style="font-size: 0.7em; height:100%;">
				<thead>
					<tr style="line-height: 10px;min-height: 10px;height: 10px;">
						<th data-align="right" data-sortable="true">Nº Mant</th>
						<th data-align="right" data-sortable="true">Periodo</th>
						<th data-align="right" data-sortable="true">Especialidad</th>
						<th data-align="right" data-sortable="true">Clase</th>
						<th data-align="right" data-sortable="true">Estado</th>
						<th data-align="right" data-sortable="true">Emplazamiento</th>
						<th data-align="right" data-sortable="false">Descripcion</th>
						<!--th data-align="right" data-sortable="false">Informe</th-->
						<th data-align="right" data-sortable="true">Hora
							<br/>validacion</th>
						<th data-align="right" data-sortable="true">Exclusion</th>
						<th data-align="right" data-sortable="true">Observacion</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{{#validacion}}
					<tr>
						<td>{{mant_id}}</td>
						<td>{{peri_nombre}}</td>
						<td>{{espe_nombre}}</td>
						<td>{{clas_nombre}}</td>
						<td>{{mant_estado}}</td>
						<td>{{empl_nombre}}</td>
						<td>{{mant_descripcion}}</td>
						<!--td><button type="button" class="btn btn-xs download" data-loading-text="Descargando..."data-processing-text="Descargando..." data-info-id="{{info_id}}" style="font-size: 0.7em; padding:2px">Descargar</button></td-->
						<td>{{mant_fecha_validacion}}</td>
						<td>
							<input  class="clase-data" type="checkbox" name="sla_ejecucion_exclusion" disabled {{#ifNoZero sla_ejecucion_exclusion}}
							 checked {{/ifNoZero}}></input>
						</td>
						<td>
							<input autocomplete="off" class="clase-data" type="text" name="sla_ejecucion_observacion" disabled value="{{sla_ejecucion_observacion}}"></input>
						</td>
						<td>
							<button type="button" class="btn btn-default btn-xs BotonSLAEjecucionValidacionEditar clase-editar" data-mant_id="{{mant_id}}"
							 style="padding:2px;height:20px">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</button>

							<button type="button" class="btn btn-xs download" data-info-id="{{info_id}}" style="padding:2px;height:20px">
								<span class="glyphicon glyphicon-download" aria-hidden="true"></span>
							</button>
						</td>

					</tr>
					{{/validacion}}
				</tbody>

			</table>
		</div>

		<div class="panel-footer siom-paginacion">
			<div class="row">
				<div class="col-md-4 text-left">
					<div class="pagination-info">
						<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
					</div>
				</div>
				<div class="col-md-8 text-right">
					<div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
				</div>
			</div>
		</div>
	</div>

</form>

<style>
	.fixed-table-container thead th .th-inner,
	.fixed-table-container tbody td .th-inner {
		line-height: 15px;
		text-align: center;
	}
</style>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/sla_ejecucion_validacion.js" type="text/javascript" charset="utf-8"></script>
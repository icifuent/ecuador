
<div class="row siom-section-header">
</div>
<div class="row">

    <div class="col-md-7">
        <div class="siom-chart" id="siom-grafico-bar" data-data="{{JSON grafico_zonas}}">Generando gráfico...</div>
    </div>
</div>
<br />
<br />
<br />
<table>
    <tr>
        <td>    
            <div class="input-color">
                <div class="color-box" style="background-color: #00b050;"></div>
                <p>Total OS Cumple</p>
            </div>
        </td>
        <td>
            <div class="input-color">
                <div class="color-box" style="background-color: #c00000;"></div>
                <p>Total OS No Cumple</p>
            </div>
        </td>
        <td>
            <div class="input-color">
                <div class="color-box" style="background-color: #00b0f0;"></div>
                <p>Sin OS</p>
            </div>
        </td>
        <td>
            <div class="input-color">
                <div class="color-box" style="background-color: #000000;"></div>
                <p>Tolerancia</p>
            </div>
        </td>
    </tr>
</table>
<script src="js/template_grafico_bar.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="css/sla.css" media="screen" />
<div class="row siom-section-header">
	<div class="col-md-5">
		<h5>Configuración de usuario</h5>
	</div>
	<div class="col-md-7">
	</div>
</div>

<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Cambio de contraseña</legend>
	<form class="form form-horizontal siom-form-tiny" role="form" action="#/usuario" method="POST" id="form-usuario-contrasena">
		<div class="form-group">
			<label for="info_estado" class="col-sm-4 control-label">Contraseña actual</label>
			<div class="col-sm-4">
				<input autocomplete="off" type="password" class="form-control" name="usua_password" placeholder="Contraseña actual" />
			</div>
		</div>
		<div class="form-group">
			<label for="info_estado" class="col-sm-4 control-label">Contraseña nueva</label>
			<div class="col-sm-4">
				<input autocomplete="off" type="password" class="form-control" name="usua_password_nuevo" placeholder="Contraseña nueva"
				/>
			</div>
		</div>
		<div class="form-group">
			<label for="info_estado" class="col-sm-4 control-label">Repita contraseña nueva</label>
			<div class="col-sm-4">
				<input autocomplete="off" type="password" class="form-control" name="usua_password_nuevo2" placeholder="Contraseña nueva"
				/>
			</div>
		</div>

		<hr>

		<div class="row text-center siom-form-actions">
			<div class="col-md-12">
				<input  type="hidden" name="usua_id" value="{{usua_id}}">
				<a href="#" class="btn btn-default" onclick="window.history.back();return false;">Cancelar</a>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</form>
</fieldset>

<script src="js/usuario.js" type="text/javascript" charset="utf-8"></script>
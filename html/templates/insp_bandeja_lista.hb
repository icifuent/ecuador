<div class="panel panel-default">
    <div class="panel-body siom-mnt-lista siom-lista-full-height" style="overflow:auto !important">
        {{#inspecciones}}
        <div class="mnt">
            <div class="estado text-center">
                <span class="id">Nº {{insp_id}}</span>
                <span class="label label-{{inspColorStatus insp_fecha_programada insp_estado}} status">{{inspFormatEstado insp_estado}}</span>
            </div>

            <div class="acciones text-right">
                <div class="dropdown">
                    {{#siomChequearPerfil "#/insp/detalle/*"}}
                    <a href="#/insp/detalle/{{insp_id}}" class="detalle">Ver detalle</a> 
                    {{/siomChequearPerfil}} 

                    {{#inspChequearActivo insp_estado}} 
                    {{#siomChequearPerfil "#/insp/mas_opciones"}}
                    <a id="more_options_{{insp_id}}" href="#" class="dropdown-toggle mas_opciones" data-toggle="dropdown"> Más opciones</a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="more_options_{{insp_id}}">

                        {{#siomChequearPerfil "#/insp/asignacion/*"}}
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#/insp/asignacion/{{insp_id}}">Asignar</a></li>
                        {{/siomChequearPerfil}} 
                        
                        {{#siomChequearPerfil "#/insp/cambio_empresa_inspectora/*"}} 
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#/insp/cambio_empresa_inspectora/{{insp_id}}">Cambio empresa inspectora</a></li>
                        {{/siomChequearPerfil}} 

                        {{#siomChequearPerfil "#/insp/adjuntar/*"}}
                        <li role="presentation">
                            <a role="menuitem" tabindex="-1" href="#/insp/adjuntar/{{insp_id}}">Agregar archivo(s)</a>
                        </li>
                        {{/siomChequearPerfil}}
                        
                        
                        {{#siomChequearPerfil "#/insp/anular/*"}}
                        <li role="presentation">
                            <a class="siom-anular-insp" role="menuitem" tabindex="-1" data-insp-id="{{insp_id}}">Anular</a>
                        </li>
                      {{/siomChequearPerfil}}   

                    </ul>
                    {{/siomChequearPerfil}} 
                    {{/inspChequearActivo}}
                </div>

            {{#if tarea}}
                {{#siomChequearTarea tarea}}
                    <a class="btn btn-primary btn-xs" href="{{@link}}" role="button">{{capitalizeFirst @texto}}</a>
                {{else}}
                    <div class="esperando-tarea">{{uppercase @texto_alt}}</div>
                {{/siomChequearTarea}}
            {{else}}
                <div class="sin-tarea">SIN TAREA PENDIENTE</div>
            {{/if}}
                <div class="timeago fecha" title="{{insp_fecha_solicitud}}">{{timeAgo insp_fecha_solicitud}}</div>
            </div>
            <div class="info text-left">
                <h4>
                  {{#is insp_responsable 'MOVISTAR'}}
                  <i class="glyphicon glyphicon-home"></i>&nbsp; 
                  {{else}}
                  <i class="glyphicon glyphicon-user"></i>&nbsp;
                  {{/is}}

                  {{empl_nombre}} <small>{{empl_nemonico}}</small>
                </h4>
                <div class="info_mnt">
                    <b>{{insp_tipo}} 
                    {{#is insp_tipo "MPP"}} Nº: {{mant_id}} {{/is}}
                    - {{empr_nombre}}
                    </b>
                </div>
                <div class="info_adicional">
                    Conformidad: <strong>{{#if insp_conformidad}} {{insp_conformidad}} % {{else}}-{{/if}}</strong>,
                    OS Nº: <strong>{{empty orse_id}}</strong>
                    {{#if mant_fecha_ejecucion}}
                    ,Fecha ejecuccion MNT: <strong>{{mant_fecha_ejecucion}}</strong>
                    {{/if}}
                </div>
            </div>
        </div>
        <hr> 

        {{/inspecciones}}
    </div>
    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                {{#if status}}
                <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..." data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
                {{/if}}
            </div>
            <div class="col-md-8 text-right">
                <div class="pagination-info">
                    Página {{pagina}}/{{paginas}} ({{total}} registros)
                </div>
                <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
            </div>
        </div>
    </div>
</div>


<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/insp_bandeja_lista.js" type="text/javascript" charset="utf-8"></script>
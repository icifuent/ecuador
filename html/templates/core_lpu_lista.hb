
<div class="panel panel-default">
  <div class="panel-body siom-os-lista siom-lista-full-height" >
  	{{#data}}
  	<div class="os" id="{{lpu_id}}">
	  <div class="estado text-center">
	  	<span class="glyphicon glyphicon-stop status status-{{estadoColorStatus lpu_estado}}" aria-hidden="true"></span>
	   </div>
	   <div class="acciones text-right">
        <button type="button" class="btn btn-default btn-xs editar" data-id="{{lpu_id}}" data-data="{{JSON.}}" >
          <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>
        </button>



       {{#is lpu_estado 'ACTIVO'}}
       <button type="button" class="btn btn-default btn-xs  eliminarLPU" data-id="{{lpu_id}}" data-nombre="{{lpu_nombre}}" data-estado="{{lpu_estado}}" data-contrato="{{cont_id}}" >
         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
       </button>
       {{else}}
       <button type="button" class="btn btn-default btn-xs  eliminarLPU" data-id="{{lpu_id}}" data-nombre="{{lpu_nombre}}" data-estado="{{lpu_estado}}" data-contrato="{{cont_id}}" >
         <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
       </button>
       {{/is}}

	  </div>


	  <div class="info text-left">
	  	<h4>{{lpu_id}} <small>{{lpu_nombre}}</small></h4>
	  </div>

	  <hr>

	</div>

	{{/data}}
  </div>

   <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	<div class="pagination-info">
	    	<small>Página {{pagina}}/{{paginas}} ({{total}} registros)</small>
	    </div>
	  </div>
	  <div class="col-md-8 text-right">
	    <div id="pagination" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>


</div>


<script src="js/core_lpu_lista.js" type="text/javascript" charset="utf-8"></script>

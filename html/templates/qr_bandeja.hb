<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Bandeja Código QR</h5>
  </div>
</div>fff

{{#if data.status}}
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/qr/bandeja/filtro" method="POST" id="siom-form-qr-filtro" name="siom-form-qr-filtro" >

            <div class="form-group">
              <div class="col-sm-5">
                <button  class="btn btn-primary btn-default" type="reset" value="Reset" >
                <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
              </div>
              <div class="col-sm-7">
                <button type="button" class="btn btn-primary btn-default" id="BuscarQrBandeja" name="BuscarQrBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Buscar
              </div>
            </div>

            <hr>

            <div class="form-group">
              <label for="id" class="col-sm-5 control-label">N° QR</label>
              <div class="col-sm-7">
                <input type="text" class="form-control" id="leco_id" name="leco_id" value="{{data.leco_id}}">
              </div>
            </div>
            <div class="form-group">
              <label for="tecnologia" class="col-sm-5 control-label">Usuario</label>
              <div class="col-sm-7">
                <select class="selectpicker" data-width="100%" name="usua_leco" id="usua_leco" data-container="body" data-live-search="true">
                  <option value="0">Seleccionar Usuario</option>
				          {{#data.usua_leco}}
                  <option value="{{usua_id}}">{{usua_nombre}}</option>
				          {{/data.usua_leco}}
                </select>
              </div>
            </div>
			 <!--div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Usuario</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" name="usua_leco" id="usua_leco" data-container="body" data-live-search="true">
                <option value="0">Seleccionar usuario</option>
                {{#data.usua_leco}}
	            	<option value="{{usua_id}}">{{usua_nombre}}</option>
	            {{/data.usua_leco}}
              </select>
            </div>
          </div-->
			
			
			 <!--div class="form-group">
              <label for="fecha_inicio" class="col-sm-5 control-label">Fecha inicio</label>
              <div class="col-sm-7">
                <input type="date" id="leco_fecha" class="form-control" size="14" type="text" data-date-autoclose="true" name="leco_fecha" value="{{#if data.leco_fecha}}{{data.leco_fecha}}{{/if}}"/>
              </div>
            </div>
            <div class="form-group">
              <label  for="fecha_fin" class="col-sm-5 control-label">Fecha fin</label>
              <div class="col-sm-7">
                <input type="date" id="leco_fecha" class="form-control" size="14" type="text" data-date-autoclose="true" name="leco_fecha" data-value="{{data.leco_fecha}}"/>
              </div>
            </div>
            <!--/div-->
        </form>
      </div>
	  </div>
    </div>
  <div class="col-md-9" id="qr-bandeja-lista">
  </div>
</div>

<div class="modal fade" id="ModalSolicitud">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Solicitud</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" rows="3" placeholder="Ingrese razón de solicitud"></textarea>
        <small>Se le enviará una notificación al encargado respectivo para autorizar solicitud.</small>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-loading-text="Solicitando..." data-complete-text="Solicitado!">Confirmar</button>
      </div>
    </div>
  </div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/qr_bandeja.js" type="text/javascript" charset="utf-8"></script>
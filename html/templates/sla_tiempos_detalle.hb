<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap-table.css" media="screen" />

<div class="row siom-section-header">
    <div class="col-md-6">
        <h5>Detalle</h5>
    </div>
	<div class="col-md-6">
        <button type="button" class="btn btn-success pull-right" id="BotonSLAExportar">Exportar</button>
    </div>
</div>

<form class="form-horizontal siom-form-tiny" role="form" method="POST" id="siom-form-sla-bandeja">
    <div class="col-md-11" style="float: left; padding-top: 5px;  padding-bottom: 5px;padding-right: 0px;padding-left: 0px;">
	
        <table data-toggle="table" data-sort-order="desc">
            <thead>
                <tr>
                    <th data-align="right" data-sortable="true" rowspan='2'>Zona</th>
                    <th colspan='3' style="text-align: center;">OSGN + OSEN</th>
                    <th colspan='3' style="text-align: center;">OSGU + OSEU</th>
                </tr>
                <tr>
                    <th data-align="right" data-sortable="true">Total OS</th>
                    <th data-align="right" data-sortable="true">OS < 24 h</th>
                    <th data-align="right" data-sortable="true">Cumplimiento [%]</th>
                    <th data-align="right" data-sortable="true">Total OS</th>
                    <th data-align="right" data-sortable="true">OS <  4 h</th>
					<th data-align="right" data-sortable="true">Cumplimiento [%]</th>
                </tr>

            </thead>
            <tbody>
                {{#detalle}}
                <tr>
                    <td>{{zona_nombre}}</td>
                    <td>{{total_osgn_osen}}</td>
                    <td>{{total_osgn_osen_sla}}</td>
                    <td>{{porcentaje_osgn_osen_sla}} %</td>
                    <td>{{total_osgu_oseu}}</td>
                    <td>{{total_osgu_oseu_sla}}</td>
                    <td>{{porcentaje_osgu_oseu_sla}} %</td>
                </tr>
                {{/detalle}}


            </tbody>
        </table>
	</div>
</form>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>    
<script src="js/sla_exportar.js" type="text/javascript" charset="utf-8"></script>    
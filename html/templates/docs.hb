<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Documentación</h5>
  </div>
  <div class="col-md-7">
  </div>
</div>

{{#if data.status}}

<fieldset class="siom-fieldset">
  <legend>Contrato</legend>
  {{{loadTemplate "cont_descripcion" data.contrato}}}
</fieldset>

<fieldset class="siom-fieldset">
  <legend>Archivos</legend>
  {{{loadTemplate "template_documentos_lista" data}}}
</fieldset>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}
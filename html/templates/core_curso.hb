<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Cursos</h5>
    </div>
    {{#data}}
    <div class="col-md-7 text-right" id="AgregarCurso">
        <a class="btn btn-primary btn-sm" href="#/core/curso" id="BotonCursoAgregar">
            <span class="glyphicon glyphicon-plus" aria-hidden="true" data-data="{{JSON .}}"></span> Agregar
        </a>
    </div>
    {{/data}}
</div>

{{#if data.status}}
<div id="WizardCurso" data-initialize="wizard" class="wizard complete">
    <ul class="steps">
        <li data-step="1" id="Primero" class="active">
            <span class="badge">1</span>Cursos
            <span class="chevron"></span>
        </li>
        <li data-step="2" id="Segundo">
            <span class="badge">2</span>Información
            <span class="chevron"></span>
        </li>
    </ul>

    <div class="step-content">
        <div class="step-pane sample-pane active" data-step="1">
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="form-horizontal siom-form-tiny" role="form" action="#/core/curso/filtro" method="POST" id="FormCursoLista">
                                <div class="form-group">
                                    <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                                    <div class="col-sm-7">
                                        <input autocomplete="off" type="text" class="form-control" name="curs_nombre" value="{{data.curs_nombre}}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="periodos" class="col-sm-5 control-label">Estado</label>
                                    <div class="col-sm-7">
                                        <select id="estado" class="selectpicker" data-width="100%" name="curs_estado">
                                            <option value="">Todos</option>
                                            {{#data.estados}}
                                            <option value="{{.}}">{{.}}</option>
                                            {{/data.estados}}
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-9" id="core-curso-lista"> </div>
            </div>
        </div>

        <div class="step-pane sample-pane" data-step="2">
            <div class="panel panel-default">

                <form class="form-horizontal siom-form-tiny" role="form" action="#/core/curso/editar" method="POST" id="FormCursoEditar">
                    <input  type="hidden" id="curs_id" name="curs_id">
                    <input  type="hidden" id="usua_creador" name="usua_creador">

                    <div class="col-md-8">
                        <fieldset class="siom-fieldset" id="siom-curso-info">
                            <legend>Curso</legend>
                            <table width="100%">
                                <tr>
                                    <!--     <td colspan="2">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Empresa</label>
                                            <div class="col-sm-8">
												<select id="UsuarioEmpresa" name="empr_id" class="selectpicker" data-width="100%" title='Seleccione Empresa' data-container="body">
												{{#data.empresas}}
													<option name="empr_id" value="{{empr_id}}">{{empr_nombre}}</option>
												{{/data.empresas}}
												</select>
                                            </div>
                                        </div>
                                    </td>
                                -->
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="curs_nombre" class="col-sm-3 control-label">Nombre</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="curs_nombre" name="curs_nombre" class="form-control" placeholder="Nombre" value="">
                                            </div>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="curs_descripcion" class="col-sm-3 control-label">Descripcion</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="curs_descripcion" name="curs_descripcion" class="form-control" placeholder="Descripcion"
                                                    value="">
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <tr>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="curs_alias" class="col-sm-3 control-label">Alias</label>
                                            <div class="col-sm-8">
                                                <input autocomplete="off" type="text" id="curs_alias" name="curs_alias" class="form-control" placeholder="Alias" value="">
                                            </div>
                                        </div>
                                    </td>

                                    <td colspan="2">
                                        <div class="form-group">
                                            <label for="curs_estado" class="col-sm-3 control-label">Estado</label>
                                            <div class="col-sm-8">
                                                <select id="curs_estado" name="curs_estado" class="selectpicker" data-width="100%" data-container="body">
                                                    {{#data.estados}}
                                                    <option value="{{.}}">{{.}}</option>
                                                    {{/data.estados}}
                                                </select>
                                            </div>
                                        </div>
                                    </td>

                                </tr>


                            </table>
                        </fieldset>
                    </div>




                    <div class="row text-center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="BotonCursoGuardar"
                                style="margin:10px">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small>
    {{/if}}
</div>
{{/if}}


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/core_curso.js" type="text/javascript" charset="utf-8"></script>
<table width="100%">
  <tr>
      <td width="10%">
      	<div class="siom-label">Nº CONT</div>
 		<div class="siom-value">{{empty cont_id}}</div>
      </td>
      <td width="40%">
      	<div class="siom-label">NOMBRE</div>
 		<div class="siom-value">{{empty cont_nombre}}</div>
      </td>
      <td width="25%">
      	<div class="siom-label">FECHA INICIO</div>
 		<div class="siom-value">{{empty cont_fecha_inicio}}</div>
      </td>
      <td width="25%">
      	<div class="siom-label">FECHA TERMINO</div>
    <div class="siom-value">{{empty cont_fecha_termino}}</div>
      </td>
  </tr>
  <tr>
      <td width="50%" colspan="2">
      	 <div class="siom-label">DESCRIPCIÓN</div>
 		     <div class="siom-value">{{empty cont_descripcion}}</div>
      </td>
      <td width="50%" colspan="2">
      	 <div class="siom-label">OBSERVACIÓN</div>
 		     <div class="siom-value">{{empty cont_observacion}}</div>
      </td>
  </tr>

</table>
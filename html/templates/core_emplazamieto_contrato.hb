<form class="form-horizontal siom-form-tiny emplazamiento_contrato_zonas" role="form" id="FormEmplazamientoContratosZonas_{{cont_id}}" method="POST">
	<div class="row">
		<div class="col-md-6">
			<fieldset class="siom-fieldset">
				<legend>Mantenimiento</legend>

				<div class="form-group">
					<label class="col-sm-3 control-label">Participa en MPP</label>
					<div class="col-sm-8">
						<input type="checkbox" name="rece_mpp" value="1" {{#is mantenimiento.contrato_emplazamiento.rece_mpp "1"}}checked{{/is}}>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Clasificación para MPP</label>
					<div class="col-sm-8">
						<select class="selectpicker" title='' name="clpr_id">
							<option value=""></option>
							{{#each mantenimiento.clasificacion_programacion}}
							<option value="{{clpr_id}}" {{#is ../mantenimiento.contrato_emplazamiento.clpr_id clpr_id}} selected {{/is}}>{{clpr_nombre}}</option>
							{{/each}}
						</select>
					</div>
				</div>
				
			</fieldset>
		</div>

		<div class="col-md-6">
			<fieldset class="siom-fieldset zonas">
				<legend>Zonas</legend>

				{{#each zonas}}
				<div class="form-group">
					<label class="col-sm-3 control-label">{{@key}}</label>
					<div class="col-sm-8">
						<select class="selectpicker" title='' multiple name="zonas" data-tipo="{{@key}}">
							{{#each this}}
							<option value="{{id}}" {{#if selected}} selected {{/if}}>{{name}}</option>
							{{/each}}
						</select>
					</div>
				</div>
				
				{{/each}}
			</fieldset>
		</div>
	</div>
	
	<div class="row text-center">
		<div class="col-md-12" style="margin-top:10px; margin-bottom:10px">
			<button type="submit" class="btn btn-primary btn-sm" data-cont-id="{{cont_id}}" data-loading-text="Guardando..." autocomplete="off">Guardar</button>
		</div>
	</div>
	<input type="hidden" name="empl_id" value="{{empl_id}}"/>
	<input type="hidden" name="cont_id" value="{{cont_id}}"/>
</form>
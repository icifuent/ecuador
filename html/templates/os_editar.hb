<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Editar orden de servicio</h5>
  </div>

</div>

<div class="step-pane sample-pane" data-step="2">
  <fieldset class="siom-fieldset">
    <legend>Emplazamiento</legend>

    {{{loadTemplate "empl_descripcion" emplazamiento}}} {{#os}}
    </table>
  </fieldset>
  <fieldset class="siom-fieldset" style="margin-top:10px;">
    <legend>Orden de servicio</legend>
    <form class="form-horizontal siom-form-tiny" role="form" action="#/os/editar/{{orse_id}}" method="POST" id="form_editar_os">

      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="form-group">
            <label for="id" class="col-sm-4 control-label">ID</label>
            <div class="col-sm-8">
              <input  type="text" class="form-control" disabled="disabled" value="{{orse_id}}" />
            </div>
          </div>

          <div class="form-group" {{estado_disable}}>
            {{#ocultarElementoPorPerfil "OS_EDITAR_GESTOR"}}
            <label for="fecha_solicitud" class="col-sm-4 control-label">Fecha programada de atención</label>
            <div class="col-sm-8">
              <div class="row">
                <div class="col-md-4">
                  <input autocomplete="off" id="orse_fecha_programada_fecha" class="form-control" size="14" type="text" data-date-autoclose="true"
                    name="orse_fecha_programada_fecha" placeholder="dd-mm-yyyy" value="{{now '%d-%m-%Y'}}" />

                </div>
                <div class="col-md-4">

                  <div class="input-group bootstrap-timepicker">
                    <input autocomplete="off" type="text" class="form-control timepicker" aria-describedby="addon" data-field="time" id="orse_fecha_programada_hora"
                      name="orse_fecha_programada_hora" data-modal-backdrop="true" placeholder="hh:mm" value="{{now '%H:%M'}} ">
                    <span class="input-group-addon" id="addon">
                      <i class="glyphicon glyphicon-time"></i>
                    </span>
                  </div>

                </div>
                <!--div class="col-md-4 text-left">
                    <div class="checkbox">
                        <label><input   type="checkbox" value="" name="orse_fecha_solicitud_ahora" id="orse_fecha_solicitud_ahora">Ahora</label>
                    </div>
                  </div-->
              </div>
            </div>
            {{/ocultarElementoPorPerfil}}
          </div>


          <div class="form-group">
            <label for="tipo" class="col-sm-4 control-label">Tipo</label>
            <div class="col-sm-8">
              <select id="tipo" class="selectpicker orse_tipo" data-width="100%" name="orse_tipo" data-valor="{{orse_tipo}}" data-size="6"
                data-container="body" disabled="disabled">
                <option value="{{orse_tipo}}">{{orse_tipo}}</option>
              </select>
            </div>
          </div>

          <!--div class="form-group">
              <label for="empresa" class="col-sm-4 control-label">Empresa</label>
              <div class="col-sm-8">
                <select id="empresa" class="selectpicker empresa" data-width="100%" name="empr_id" data-valor="{{empr_id}}" data-container="body">

                  <option value="{{empr_id}}" data-coem-tipo="{{coem_tipo}}">{{empr_nombre}}</option>

                  {{#empresa}}

                  <option value="{{empr_id}}" id="{{empr_id}}" data-coem-tipo="{{coem_tipo}}">{{empr_nombre}}</option>
                  {{/empresa}}
                </select>
              </div>
            </div-->

          <div class="form-group">
            <label for="empresa" class="col-sm-4 control-label">Empresa</label>
            <div class="col-sm-8">
              <select id="empr_id" class="selectpicker empresa" value="{{empr_id}}" data-width="100%" name="empr_id" data-value-select="{{empr_id}}"
                title="Seleccione empresa" data-container="body">
                {{#empresa}}
                <option name="empr_id" value="{{empr_id}}">{{empr_nombre}}</option>
                {{/empresa}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="especialidad" class="col-sm-4 control-label">Especialidad</label>
            <div class="col-sm-8">
              <select id="espe_id" class="selectpicker especialidad" data-width="100%" name="espe_id" data-hide-disabled="true" data-value-select="{{espe_id}}"
                title="Seleccione especialidad" data-container="body">
                <!--option name="espe_id" value="{{espe_id}}">{{espe_nombre}}</option-->
                {{#especialidad}}
                <option name="espe_id" value="{{espe_id}}">{{espe_nombre}}</option>
                {{/especialidad}}
              </select>
            </div>
          </div>


          <div class="form-group">
            <label for="subespecialidad" class="col-sm-4 control-label">Alarma</label>
            <div class="col-sm-8">
              <select id="subespecialidad" class="selectpicker subespecialidad" data-value-select="{{sube_id}}" data-width="100%" name="sube_id"
                value="{{orse_sube_id}}" data-hide-disabled="true" data-container="body" title="Seleccione Alarma">
                {{#subespecialidad}}
                <option data-espe-id="{{espe_id}}" id="{{sube_id}}" value="{{sube_id}}">{{sube_nombre}}</option>
                {{/subespecialidad}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="indisponibilidad" class="col-sm-4 control-label">Indisponibilidad</label>
            <div class="col-sm-8">
              <select id="indisponibilidad" class="selectpicker indisponibilidad" data-valor="{{orse_indisponibilidad}}" data-width="100%"
                name="orse_indisponibilidad" title="seleccione indisponibilidad" disabled="disabled" data-container="body">
                <option value="{{orse_indisponibilidad}}">{{orse_indisponibilidad}}</option>

              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="descripcion" class="col-sm-4 control-label">Descripción</label>
            <div class="col-sm-8">
              <textarea class="form-control" name="orse_descripcion" rows="5" disabled="disabled" placeholder="Descripcion">{{orse_descripcion}}</textarea>
              <div id="orse_descripcion_palabras" class="siom-info text-right text-danger">
                <span>0</span> palabras de {{../max_words}}</div>
            </div>

          </div>

          <div class="form-group">
            <label for="tag" class="col-sm-4 control-label">Cód. Incidencia</label>
            <div class="col-sm-8">
              <input  type="text" class="form-control" disabled="disabled" name="orse_tag" placeholder="Cód. Incidencia (opcional)"
                value="{{orse_tag}}" />
            </div>
          </div>

          <div class="form-group">
            <label for="tag" class="col-sm-4 control-label">Delta tiempo</label>
            <div class="col-sm-8">
              <input autocomplete="off" type="text" class="form-control" name="orse_delta_tiempo" placeholder="delta tiempo" value="{{orse_delta_tiempo}}"
              />
            </div>
          </div>

          <div class="form-group">
            <label for="tag" class="col-sm-4 control-label">Formulario</label>
            <div class="col-sm-8">
              <input  type="text" class="form-control" disabled="disabled" name="form_nombre" id="form_nombre" data-valorform="{{JSON def_formulario}}"
              />
            </div>
          </div>

          <!--De aqui-->
          <div class="form-group">
            {{#ocultarElementoPorPerfil "OS_EDITAR_GESTOR"}}
            <label for="estado" class="col-sm-4 control-label">Estado</label>
            <div class="col-sm-8">

              <select {{estado_disable}} id="orse_estado" class="selectpicker estado" data-valor="{{JSON data.orse_estado}}" data-width="100%"
                name="orse_estado" title="Estado" data-container="body">

                <option value="0">Seleccionar Estado</option>
                <option value="CREADA">CREADA</option>
                <option value="VALIDANDO">VALIDANDO</option>
                <option value="APROBADA">APROBADA</option>
                <option value="RECHAZADA">RECHAZADA</option>
                <!-- <option value="FINALIZADA">FINALIZADA</option>-->
                <option value="ANULADA">ANULADA</option>
                <option value="NOACTIVO">NO REALIZADO</option>

              </select>
            </div>
            {{/ocultarElementoPorPerfil}}
          </div>
          <!--Hasta aqui-->


        </div>
      </div>
      {{/os}}
      <hr>
      <div class="row text-center">
        <div class="col-md-12">
          <a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
          <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="submit_editar_os">Guardar O.S.</button>
        </div>
      </div>
    </form>
  </fieldset>
</div>


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/os_editar.js" type="text/javascript" charset="utf-8"></script>
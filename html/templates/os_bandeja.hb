<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Bandeja de ordenes de servicio</h5>
  </div>
  <div class="col-md-7 text-right">

    {{#siomChequearPerfil "#/os/crear"}}
    <a class="btn btn-primary btn-sm" href="#/os/crear">
      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Crear O.S.
    </a>
    {{/siomChequearPerfil}} {{#siomChequearPerfil "#/os/reportes_os"}}
    <a class="btn btn-primary btn-sm" href="#/os/reportes_os">
      <span class="glyphicon glyphicon" aria-hidden="true"></span> Reportes OS
    </a>

    {{/siomChequearPerfil}}
  </div>
</div>

{{#if data.status}}
<div class="row">
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-body">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/os/bandeja/filtro" method="POST" id="siom-form-os-bandeja">
          <div class="form-group">
            <div class="col-sm-5">
              <button type="button" class="btn btn-primary btn-default" id="LimpiarOsBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Limpiar Filtros
            </div>
            <div class="col-sm-7">
              <button type="button" class="btn btn-primary btn-default" id="BuscarOsBandeja">
                <span class="glyphicon" aria-hidden="true"></span> Buscar
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label for="id" class="col-sm-5 control-label">Nº O.S.</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="orse_id" name="orse_id" value="{{data.orse_id}}">
            </div>
          </div>
          <div class="form-group">
            <label for="tipo" class="col-sm-5 control-label">Tipo</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" name="orse_tipo" title="Todos" data-value="{{data.orse_tipo}}">
                <option value="">TODOS</option>
                {{#data.tipos}}
                <option value="{{.}}">{{.}}</option>
                {{/data.tipos}}
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="fecha_inicio" class="col-sm-5 control-label">Fecha Inicio Programada</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_inicio" class="form-control" size="14" type="text" data-date-autoclose="true" name="orse_fecha_solicitud_inicio"
                value="{{#if data.orse_fecha_solicitud_inicio}} {{formatDate data.orse_fecha_solicitud_inicio '%d-%m-%Y'}} {{/if}}"
              />
            </div>
          </div>
          <div class="form-group">
            <label for="fecha_fin" class="col-sm-5 control-label">Fecha Fin Programada</label>
            <div class="col-sm-7">
              <input autocomplete="off" id="fecha_fin" class="form-control" size="14" type="text" data-date-autoclose="true" name="orse_fecha_solicitud_termino"
                value="{{#if data.orse_fecha_solicitud_termino}} {{formatDate data.orse_fecha_solicitud_termino '%d-%m-%Y'}} {{/if}}"
              />
            </div>
          </div>
          <div class="form-group">
            <label for="estado" class="col-sm-5 control-label">Estado</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" multiple title='Todos' name="orse_estado" data-value="{{JSON data.orse_estado}}">
                <option value="CREADA">CREADA</option>
                <option value="ASIGNANDO">ASIGNANDO</option>
                <option value="ASIGNADA">ASIGNADA</option>
                <option value="EJECUTANDO">EJECUTANDO</option>
                <option value="VALIDANDO">VALIDANDO</option>
                <option value="RECHAZADA">RECHAZADA</option>
                <option value="APROBADA">APROBADA</option>
                <option value="DEVUELTA">DEVUELTA</option>
                <option value="ANULADA">ANULADA</option>
                <option value="NOACTIVO">NO ACTIVO</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="estado" class="col-sm-5 control-label">Empresa Responsable</label>
            <div class="col-sm-7">
              <select class="selectpicker" data-width="100%" title='Todas' name="orse_responsable" data-value="{{data.orse_responsable}}">
                <option value="">Todas</option>
                <option value="MOVISTAR">MOVISTAR</option>
                <option value="CONTRATISTA">CONTRATISTA</option>
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="nemonico" class="col-sm-5 control-label">Nemónico</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="empl_nemonico" name="empl_nemonico" value="{{data.empl_nemonico}}">
            </div>
          </div>
          <div class="form-group">
            <label for="nombre" class="col-sm-5 control-label">Nombre</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="empl_nombre" name="empl_nombre" value="{{data.empl_nombre}}">
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="tecnoclogia" class="col-sm-5 control-label">Tecnología</label>
            <div class="col-sm-7">
              <select id="tecnologia" class="selectpicker" data-width="100%" title='Todas' multiple name="tecn_id" data-value="{{data.tecn_id}}">
                {{#data.tecnologias}}
                <option value="{{tecn_id}}">{{tecn_nombre}}</option>
                {{/data.tecnologias}}
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="empl_direccion" class="col-sm-5 control-label">Dirección</label>
            <div class="col-sm-7">
              <input autocomplete="off" type="text" class="form-control" id="empl_direccion" name="empl_direccion" value="{{data.empl_direccion}}">
            </div>
          </div>

          <div class="form-group">
            <label for="zona" class="col-sm-5 control-label">Zona</label>
            <div class="col-sm-7">
              <select name="zona_id" class="selectpicker" data-width="100%" title='Todas' data-value="{{data.zona_id}}">
                <option value="">Todas</option>
                {{#data.zonas}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.zonas}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="region" class="col-sm-5 control-label">Región</label>
            <div class="col-sm-7">
              <select name="regi_id" class="selectpicker" data-width="100%" title='Todas' data-value="{{data.regi_id}}">
                <option value="">Todas</option>
                {{#data.regiones}}
                <option value="{{regi_id}}">{{regi_nombre}}</option>
                {{/data.regiones}}
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="cluster" class="col-sm-5 control-label">Cluster</label>
            <div class="col-sm-7">
              <select name="clus_id" class="selectpicker" data-width="100%" title='Todos' data-value="{{data.clus_id}}">
                <option value="">Todos</option>
                {{#data.clusters}}
                <option value="{{zona_id}}">{{zona_nombre}}</option>
                {{/data.clusters}}
              </select>
            </div>
          </div>

          <hr>

          <div class="form-group">
            <label for="cluster" class="col-sm-5 control-label">Usuario Creador</label>
            <div class="col-sm-7">
              <select name="usua_creador" class="selectpicker" data-width="100%" title='Todos' data-value="{{data.usua_creador}}" data-container="body">
                <option value="">Todos</option>
                {{#data.usuarios_creador}}
                <option value="{{usua_id}}">{{usua_nombre}}</option>
                {{/data.usuarios_creador}}
              </select>
            </div>
          </div>

          <hr>


          <div class="form-group">
            <label for="zona_movistar" class="col-sm-5 control-label">Zona Movistar</label>
            <div class="col-sm-7">
              <select name="zona_movistar_id" class="selectpicker" data-width="100%" title='Todas' data-value="{{data.zona_movistar_id}}">
                <option value="">Todas</option>
                {{#data.zona_movistar}}
                <option value="{{zona_movistar_id}}">{{zona_movistar_nombre}}</option>
                {{/data.zona_movistar}}
              </select>
            </div>
          </div>

          <!--
            <div class="text-center">
              <button type="submit" class="btn btn-primary" data-loading-text="Filtrando..." >Filtrar</button>
            </div>
            -->
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-9" id="os-bandeja-lista">
  </div>
</div>

<div class="modal fade" id="ModalSolicitud">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Solicitud</h4>
      </div>
      <div class="modal-body">
        <textarea class="form-control" rows="3" placeholder="Ingrese razón de solicitud"></textarea>
        <small>Se le enviará una notificación al encargado respectivo para autorizar solicitud.</small>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" data-loading-text="Solicitando..." data-complete-text="Solicitado!">Confirmar</button>
      </div>
    </div>
  </div>
</div>


{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}} {{#if data.debug}}
  <br>
  <small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/os_bandeja.js" type="text/javascript" charset="utf-8"></script>
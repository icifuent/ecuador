<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Editar Lpu Grupo</h5>
  </div>

</div>

{{#if status}}
<div id="wizardLPUGrupo" data-initialize="wizard" class="wizard complete">
  <ul class="steps">
    <li data-step="1" class="active" id="Primero">
      <span class="badge">1</span>Listado de Lpu Grupo
      <span class="chevron"></span>
    </li>
    <li data-step="2" id="Segundo">
      <span class="badge">2</span>Información
      <span class="chevron"></span>
    </li>
  </ul>

  <div class="step-content">
    <div class="step-pane sample-pane active" data-step="1">
      <div class="row">
        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-body">
              <form class="form-horizontal siom-form-tiny" role="form" action="#/core/contrato/lpu/filtro" method="POST" id="siom-form-lpu-grupo">
                <div class="form-group">
                  <label for="nombre" class="col-sm-5 control-label">Nombre</label>
                  <div class="col-sm-7">
                    <input autocomplete="off" type="text" class="form-control" name="lpgr_nombre" value="{{data.lpgr_nombre}}">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-9" id="core-lpu-grupo-lista"> </div>
      </div>
    </div>

    <div class="step-pane sample-pane" data-step="2">
      <div class="panel panel-default">

        <form class="form-horizontal siom-form-tiny" role="form" action="#/core/lpu/" method="POST" id="EditFormLPUGrupo">
          <input  type="hidden" id="lpgr_id" value="">
          <input  type="hidden" id="lpu_id" value="">

          <div class="col-md-12">
            <fieldset class="siom-fieldset" id="siom-lpu-grupo-info">
              <legend>Lpu</legend>
              <table width="100%">
                <tr>
                  <td width="40%" colspan="2">
                    <div class="form-group">
                      <label for="usua_nombre" class="col-sm-4 control-label">Nombre</label>
                      <div class="col-sm-6">
                        <input autocomplete="off" type="text" class="form-control" id="lpgr_nombre" placeholder="Nombre" value="">
                      </div>
                    </div>
                  </td>
                </tr>
              </table>
            </fieldset>
          </div>

          <div class="row text-center">
            <div class="col-md-12">
              <button type="submit" class="btn btn-primary" data-loading-text="Guardando..."  id="submit_Guardar">Guardar</button>
            </div>
          </div>

        </form>
      </div>
    </div>

  </div>
</div>
{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}} {{#if data.debug}}
  <br>
  <small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/core_lpu_grupo.js" type="text/javascript" charset="utf-8"></script>
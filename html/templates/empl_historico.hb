<table width="100%">
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">NOMBRE</div>
        <div class="siom-value">{{empty empl_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">NEMONICO</div>
        <div class="siom-value">{{empty empl_nemonico}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">CLASIFICACIÓN</div>
        <div class="siom-value">{{empty clas_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">TECNOLOGIA</div>
        <div class="siom-value">{{empty tecn_nombre}}</div>
      </td>
  </tr>
  <tr>
      <td width="40%" colspan="2">
        <div class="siom-label">DIRECCIÓN</div>
        <div class="siom-value">{{empty empl_direccion}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">COMUNA</div>
        <div class="siom-value">{{empty comu_nombre}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">REGIÓN</div>
        <div class="siom-value">{{empty regi_nombre}}</div>
      </td>
      <td width="20%">
       <!-- <div class="siom-label">ZONA</div>
        <div class="siom-value">{{empty zona_nombre}}</div>-->
      </td>
  </tr>
  <tr>
      <td width="20%">
        <div class="siom-label">MACROSITIO</div>
        <div class="siom-value">{{boolean empl_macrositio}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">SUBTEL</div>
        <div class="siom-value">{{boolean empl_subtel}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ESPACIO</div>
        <div class="siom-value">{{empty empl_espacio}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">TIPO ACCESO</div>
        <div class="siom-value">{{empty empl_tipo_acceso}}</div>
      </td>
      <td width="20%">
        <div class="siom-label">ACCESO</div>
        <div class="siom-value">{{empty empl_observacion_ingreso}}</div>
      </td>
      
  </tr>
  
</table>
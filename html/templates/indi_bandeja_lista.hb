<div class="panel panel-default">
  	<div class="panel-body siom-indi-lista siom-lista-full-height">
  	{{#indisponibilidades}}
  	<div class="indi">
	  <div class="estado text-center">
	  	<span class="id">Nº {{indi_id}}</span>
		<span class="label  label-{{indiColorStatus orse_fecha_solicitud orse_estado orse_tipo}} status">{{indi_estado}}</span>
	 </div>
	 
	  <div class="acciones text-right">
	  	<div class="dropdown">
	  			<a href="#/indisponibilidad/detalle/{{indi_id}}" class="detalle">Ver detalle</a>
	  	  	
	  	  	{{#indiChequearActiva indi_estado}}

		  	  	{{#siomChequearPerfil "#/indisponibilidad/mas_opciones"}}
				  	<a id="more_options_{{indi_id}}" href="#" class="dropdown-toggle mas_opciones" data-toggle="dropdown"> Más opciones</a>
					

					<ul class="dropdown-menu" role="menu" aria-labelledby="more_options_{{indi_id}}">

						{{#siomChequearPerfil "#/indisponibilidad/generarOS/*"}}
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#/indisponibilidad/generarOS/{{indi_id}}">GENERAR OS</a></li>
						{{/siomChequearPerfil}}

					</ul>
				{{/siomChequearPerfil}}

			{{/indiChequearActiva}}
		</div>
		
		{{#indiChequearActiva indi_estado}}

			{{#if tarea}}
				{{#siomChequearTarea tarea}}
					<a class="btn btn-primary btn-xs" href="{{@link}}" role="button">{{capitalizeFirst @texto}}</a>
				{{else}}
					<div class="esperando-tarea">{{uppercase @texto_alt}}</div>
				{{/siomChequearTarea}}
			{{else}}
			    <div class="sin-tarea">SIN TAREA PENDIENTE</div>
			{{/if}}



 	 	<!--div class="timeago fecha" title="{{indi_fuente_reportada}}">{{timeAgo indi_fuente_reportada}}</div-->

 	 	{{else}}
 	 		<div class="sin-tarea">FINALIZADA</div>
 	 		<!--div class="timeago fecha" title="{{indi_fuente_reportada}}">{{timeAgo indi_fuente_reportada}}</div-->
 	 	{{/indiChequearActiva}}
	  </div>

	  <div class="info text-left">
	  	<h4>
			  	<i class="glyphicon glyphicon-user"></i>&nbsp;
	  			<div class="info_indi">{{indi_prioridad}}</div>
        </h4>
        		<small>{{indi_resumen}}</small>
	   </div>
	</div>

	<hr>
	{{/indisponibilidades}}
  </div>
  <div class="panel-footer siom-paginacion">
  	<div class="row">
	  <div class="col-md-4 text-left">
	  	{{#if status}}
		 	<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
		{{/if}}

		{{#siomChequearPerfil "#/os/reporte/sap"}}
		    <button type="button" class="btn btn-default btn-xs" id="reporteSAP" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}"><span class="glyphicon glyphicon-download" aria-hidden="true"></span> Descargar reporte SAP</button>
		{{/siomChequearPerfil}}
	  </div>
	  <div class="col-md-8 text-right">
	  	<div class="pagination-info">
	    	Página {{pagina}}/{{paginas}} ({{total}} registros)
	    </div>
	    <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
	  </div>
	</div>
  </div>
</div>

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/indi_bandeja_lista.js" type="text/javascript" charset="utf-8"></script>
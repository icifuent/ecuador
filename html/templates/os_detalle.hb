<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Detalle orden de servicio</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>

{{#if data.status}}
<div class="row">
  <div class="col-sm-9">
    <div class="siom-section-subtitle">Resumen</div>

    <fieldset class="siom-fieldset">
      <legend>Emplazamiento</legend>
      {{{loadTemplate "empl_descripcion" data.os}}}
    </fieldset>

    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Orden de servicio</legend>
      {{{loadTemplate "os_descripcion" data.os}}}
    </fieldset>


    <div class="siom-section-subtitle">Asignación</div>
    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Asignaciones</legend>
      {{#each data.asignacion}}
        <div class="row">
          <label class="col-sm-2 siom-label">FECHA ASIGNACIÓN</label>
          <label class="col-sm-2 siom-value">{{oras_fecha_asignacion}}</label>
          <label class="col-sm-2 siom-label">ASIGNADA POR</label>
          <label class="col-sm-3 siom-value">{{usua_nombre}}</label>
          <label class="col-sm-1 siom-label">ESTADO</label>
          <label class="col-sm-2 siom-value">{{oras_estado}}</label>
        </div>
        <div class="row">
          <label class="col-sm-2 siom-label">JEFE CUADRILLA</label>
          <label class="col-sm-2 siom-value">{{detalle.jefe}}</label>
          <label class="col-sm-2 siom-label">ACOMPAÑANTE(S)</label>
          <label class="col-sm-4 siom-value">
          {{#each detalle.acompanantes}}
            {{.}}<br>
          {{/each}}
          </label>
        </div>
        {{#gt ../data.asignacion.length 1}}
        <hr>
        {{/gt}}

      {{else}}
      <p class="siom-no-data">Sin asignaciones</p>
      {{/each}}
    </fieldset>


    <div class="siom-section-subtitle">Presupuesto</div>
    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Presupuestos</legend>
      {{#each data.presupuesto}}
        <div class="row">
          <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
          <label class="col-sm-2 siom-value">{{pres_fecha_creacion}}</label>
          <label class="col-sm-2 siom-label">CREADO POR</label>
          <label class="col-sm-3 siom-value">{{usua_creador}}</label>
          <label class="col-sm-1 siom-label">ESTADO</label>
          <label class="col-sm-1 siom-value">{{pres_estado}}</label>
          <label class="col-sm-1 siom-link">
            <a href="#/os/presupuesto/{{../data.os.orse_id}}/ver/{{pres_id}}"> VER </a>
          </label>
        </div>

        {{#isnt pres_estado 'SINVALIDAR'}}
        <div class="row">
          <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
          <label class="col-sm-2 siom-value">{{pres_fecha_validacion}}</label>
          <label class="col-sm-2 siom-label">VALIDADO POR</label>
          <label class="col-sm-2 siom-value">{{usua_validador}}</label>
        </div>
        {{/isnt}}

        {{#gt ../data.presupuesto.length 1}}
        <hr>
        {{/gt}}
      {{else}}
      <p class="siom-no-data">Sin presupuestos</p>
      {{/each}}
    </fieldset>

    {{{loadTemplate "template_visitas" data.visitas}}}


    <div class="siom-section-subtitle">Informe</div>
    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Informes</legend>
      {{#each data.informes}}
        <table width="100%">
          <tr>
              <td width="30%">
                <div class="siom-label">FECHA CREACIÓN</div>
                <div class="siom-value">{{info_fecha_creacion}}</div>
              </td>
              <td width="30%">
                <div class="siom-label">CREADO POR</div>
                <div class="siom-value">{{usua_creador}}</div>
              </td>
              <td width="35%">
                <div class="siom-label">ESTADO</div>
                <div class="siom-value">{{info_estado}}</div>
              </td>
              <td width="5%">
                <a class="siom-link" href="#/os/informe/{{../data.os.orse_id}}/ver/{{info_id}}"> VER </a>
              </td>
          </tr>

        {{#isnt info_estado 'SINVALIDAR'}}
          <tr>
              <td width="30%">
                <div class="siom-label">FECHA VALIDACIÓN</div>
                <div class="siom-value">{{info_fecha_validacion}}</div>
              </td>
              <td width="30%">
                <div class="siom-label">VALIDADO POR</div>
                <div class="siom-value">{{usua_validador}}</div>
              </td>
              <td width="35%">
                <div class="siom-label">OBSERVACIÓN</div>
                <div class="siom-value">{{info_observacion}}</div>
              </td>
              <td width="5%">
              </td>
          </tr>
        {{/isnt}}
        </table>

        {{#gt ../data.informes.length 1}}
        <hr>
        {{/gt}}      
      {{else}}
      <p class="siom-no-data">Sin informes</p>
      {{/each}}
    </fieldset>

  </div>
  <div class="col-sm-3">
    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Ubicación</legend>
      <div id="siom-os-detalle-mapa" class="siom-detalle-mapa" data-latitud="{{data.os.empl_latitud}}" data-longitud="{{data.os.empl_longitud}}" data-distancia="{{data.os.empl_distancia}}"></div>
    </fieldset>

    <fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Eventos</legend>
      <table class="siom-events" width="100%">
      <tr>
        <th width="100px">Fecha</th>
        <th>Evento</th>
        <th width="65px">Estado</th>
      </tr>
      {{#each data.eventos}}
        <tr>
          <td width="100px" valign="top">{{even_fecha}}</td>
          <td width="65px" valign="top">{{parsearEvento even_evento}}</td>
          <td valign="top">
            {{#is even_estado "ERROR"}}
              {{even_estado}}: <small class="text-danger">{{even_comentario}}</small>
            {{else}}
              {{even_estado}}
            {{/is}}
          </td>
        </tr>
      {{else}}
        <tr><td class="siom-no-data">Sin eventos</td></tr>
      {{/each}}
      </table>
   </fieldset>
  </div>
</div>

{{else}}
  <div class="alert alert-danger" role="alert">
    {{data.error}}
    {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
  </div>
{{/if}}

<script src="js/os_detalle.js" type="text/javascript" charset="utf-8"></script>



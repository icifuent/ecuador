<fieldset class="siom-fieldset"  style="margin-top:20px;">
      <legend>Eventos</legend>
      <table class="siom-events" width="100%">
      <tr>
        <th width="100px">Fecha</th>
        <th>Evento</th>
        <th width="65px">Estado</th>
      </tr>
      {{#each this}}
        <tr>
          <td width="100px" valign="top">{{even_fecha}}</td>
          <td width="65px" valign="top">{{parsearEvento even_evento}}</td>
          <td valign="top">
            {{#is even_estado "ERROR"}}
              {{even_estado}}: <small class="text-danger">{{even_comentario}}</small>
            {{else}}
              {{even_estado}}
            {{/is}}
          </td>
        </tr>
      {{else}}
        <tr><td class="siom-no-data">Sin eventos</td></tr>
      {{/each}}
      </table>
   </fieldset>
<div class="row siom-section-header">
	<div class="col-md-5">
		<h5>Asignación orden de servicio</h5>
	</div>
	<div class="col-md-7">

	</div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
	<legend>Emplazamiento</legend>
	{{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Orden de servicio</legend>
	{{{loadTemplate "os_descripcion" data.os}}}
</fieldset>
<fieldset class="siom-fieldset">
	<legend>Formularios Relacionados</legend>
	<!--label class="col-sm-12 siom-label">NOMBRE FORMULARIOS</label-->
	{{#data.nomForm}}
	<label class="col-sm-0 siom-value">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{form_nombre}}</label>
	{{/data.nomForm}}
</fieldset>


<fieldset class="siom-fieldset" style="margin-top:20px;">
	<legend>Asignación</legend>

	<form class="form form-horizontal siom-form-tiny" role="form" action="#/os/asignacion/{{data.os.orse_id}}" method="POST"
	 id="siom-form-asignacion">
		<div class="row form-group">
			<div class="col-sm-2 col-md-offset-1">
				<label class="control-label">JEFE DE CUADRILLA</label>
			</div>
			<div class="col-sm-4">
				<select id="jefecuadrilla" class="selectpicker" data-width="100%" title='Seleccione jefe de cuadrilla' name="jefecuadrilla"
				 data-live-search="true">
					<option value=""></option>
					{{#data.jefes}}
					<option value="{{usua_id}}">{{usua_nombre}}</option>
					{{/data.jefes}}
				</select>
			</div>
		</div>

		<div class="row form-group">
			<div class="col-sm-2  col-md-offset-1">
				<label class="control-label">ACOMPAÑANTES</label>
			</div>
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-6">
						<input autocomplete="off" type="text" class="form-control input-sm" id="filtro_acompanantes" placeholder="Buscar acompañantes">
						<ul class="list-group siom-asignacion-list" id="lista_acompanantes">
							{{#data.acompanantes}}
							<li class="list-group-item" data-id="{{usua_id}}" data-nombre="{{usua_nombre}}">
								<span>{{usua_nombre}}</span>
								<i class="glyphicon glyphicon-chevron-right pull-right"></i>
							</li>
							{{/data.acompanantes}}
						</ul>
					</div>
					<div class="col-sm-6">
						<input autocomplete="off" type="text" class="form-control input-sm" id="filtro_acompanantes_seleccionados" placeholder="Buscar acompañantes seleccionados">
						<ul class="list-group siom-asignacion-list" id="lista_acompanantes_seleccionados">

						</ul>
					</div>
				</div>
			</div>
		</div>

		<hr>
		<div class="row text-center siom-form-actions">
			<div class="col-md-12">
				<a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
				<button type="submit" class="btn btn-primary" data-loading-text="Asignando..."  id="submit_asignar">Asignar O.S.</button>
			</div>
		</div>
	</form>
</fieldset>

{{else}}
<div class="alert alert-danger" role="alert">
	{{data.error}} {{#if data.debug}}
	<br>
	<small>{{data.debug}}</small>
	{{/if}}
</div>
{{/if}}

<script src="js/os_asignacion.js" type="text/javascript" charset="utf-8"></script>
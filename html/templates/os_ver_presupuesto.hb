<div class="row siom-section-header">
 <div class="col-md-5">
   <h5>Ver de presupuesto</h5>
 </div>
 <div class="col-md-7">

 </div>
</div>

{{#if status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>


<fieldset class="siom-fieldset">
   <legend>Validación de presupuesto</legend>
   {{#if data.presupuesto}}
	<form  action="#/os/detalle/{{data.orse_id}}" method="POST" id="siom-form-ver-presupuesto">
		<div class="row siom-os-presupuesto-header">
	        <div class="col-sm-4">Actividad</div>
	        <div class="col-sm-2 text-center">Categoria</div>
	        <div class="col-sm-1 text-center">Valor unitario</div>
	        <div class="col-sm-1 text-center">Cantidad</div>
	        <div class="col-sm-1 text-center">Subtotal</div>
	        <div class="col-sm-1 text-center">Estado</div>
	        <div class="col-sm-1 text-center">Comentario</div>
	      </div>

		<div class="siom-os-presupuesto-lista">
		{{#each data.presupuesto}}
			<div class="siom-os-presupuesto-lista-validar" id="{{prit_id}}">
    		   	<div class="row">
    		      <div class="col-sm-4">
                     <label class="nombre">{{lpit_nombre}}</label><br>
	  		         <label class="comentario">{{lpgr_nombre}}</label>
			      </div>
			      <div class="col-sm-2 text-center categoria">
			          {{lpgc_nombre}}
			      </div>
                  <div class="col-sm-1 text-center precio">
                      {{lpip_precio}} UF
                  </div>
			      <div class="col-sm-1 text-center cantidad">
			          {{prit_cantidad}}
			      </div>
			      <div class="col-sm-1 text-center subtotal">
			          {{presCalcSubtotal lpip_precio prit_cantidad}} UF
			      </div>
				      <div class="col-sm-1 text-center cantidad">
			          {{prit_estado}}
			      </div>
			      <div class="col-sm-1 text-center cantidad">
			          {{prit_comentario}}
			      </div>
		   		</div>
		   	</div>
		{{/each}}
		</div>
		<div class="row">
			<div class="col-sm-7 text-left">
				<button type="button" class="btn btn-default btn-xs siom-presupuesto-exportar" id="download" data-loading-text="Descargando..."data-processing-text="Procesando..." data-pres-id="{{data.pres_id}}">
	              Descargar en Excel
	            </button>
			</div>
			<div class="col-sm-1 text-right siom-os-presupuesto-total-validacion">
	        	TOTAL
	        </div>
	        <div class="col-sm-1 text-center siom-os-presupuesto-total-validacion">
	        	{{osSumItemsPresupuesto data.presupuesto}} UF
	        </div>
	        <div class="col-sm-3 text-right siom-os-presupuesto-estado-validacion" id="siom-os-presupuesto-estado-validacion" data-items="{{data.presupuesto.length}}">
				{{PresupuestosValidados data.presupuesto}} items validados de un total de {{data.presupuesto.length}}
			</div>
		</div>
		<hr>
		<div class="row text-center">
			<div class="col-md-12">
				<a href="#/os/detalle/{{data.os.orse_id}}" class="btn btn-primary">Volver</a>
			</div>
		</div>
	</form>
	{{else}}
		<div class="alert alert-warning" role="alert">
	  	<strong>Aviso</strong> ruta indicada no tiene un presupuesto definido.
		</div>
	{{/if}}
</fieldset>

{{else}}
	<div class="alert alert-danger" role="alert">
  	{{data.error}}
  	{{#if data.debug}}
 	<br><small>{{data.debug}}</small>
	{{/if}}
	</div>
{{/if}}

<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>    
<script src="js/os_ver_presupuesto.js" type="text/javascript" charset="utf-8"></script>


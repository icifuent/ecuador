<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Detalle Indisponibilidad</h5>
    </div>
    <div class="col-md-7 text-right">
      {{#siomChequearPerfil "#/indisponibilidad/generarOS"}}
        <a class="btn btn-primary btn-sm" href="#/indisponibilidad/generarOS">
          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Generar O.S.
        </a>
      {{/siomChequearPerfil}}
    </div>
</div>

{{#if data.status}}
    <div class="row">
        <div class="col-sm-12">
            {{#indisponibilidad}}
            <fieldset class="siom-fieldset">
                <legend>Informacion Indisponibilidad {{indi_incidencia}}</legend>
                <div class="row">
                  <label class="col-sm-2 siom-label">Resumen</label>
                  <label class="col-sm-2 siom-value">{{indi_resumen}}</label>
                  <label class="col-sm-2 siom-label">Estado</label>
                  <label class="col-sm-2 siom-value">{{indi_estado}}</label>
                  <label class="col-sm-2 siom-label">Motivo Estado</label>
                  <label class="col-sm-2 siom-value">{{indi_motivo_estado}}</label>
                </div>
                <div class="row">
                    <label class="col-sm-2 siom-label">Prioridad</label>
                    <label class="col-sm-2 siom-value">{{indi_prioridad}}</label>
                    <label class="col-sm-2 siom-label">Impacto</label>
                    <label class="col-sm-2 siom-value">test</label>
                    <label class="col-sm-2 siom-label">Urgencia</label>
                    <label class="col-sm-2 siom-value">{{indi_urgencia}}</label>
                </div>
                <div class="row">
                    <label class="col-sm-2 siom-label">Usuario</label>
                    <label class="col-sm-2 siom-value">test</label>
                    <label class="col-sm-2 siom-label">Cluster</label>
                    <label class="col-sm-2 siom-value">{{cluster_nombre}}</label>
                </div>
            </fieldset>
            <fieldset class="siom-fieldset">
                <legend>Resolución</legend>
                <div class="row">
                  <label class="col-sm-2 siom-label">Resolución</label>
                  <label class="col-sm-2 siom-value">{{indi_resolucion}}</label>
                </div>
                <div class="row">
                  <label class="col-sm-2 siom-label">Categoría 1</label>
                  <label class="col-sm-2 siom-value">{{indi_resolucion_n1}}</label>
                  <label class="col-sm-2 siom-label">Categoría 2</label>
                  <label class="col-sm-2 siom-value">{{indi_resolucion_n2}}</label>
                  <label class="col-sm-2 siom-label">Categoría 3</label>
                  <label class="col-sm-2 siom-value">{{indi_resolucion_n3}}</label>
                </div>
            </fieldset>
             {{/indisponibilidad}}
            <fieldset class="siom-fieldset">
                <legend>Relación</legend>

                <table class="table table-striped table-hover table-condensed"   data-sort-order="desc">
                  <thead>
                      <tr>
                        <th data-align="center">Resumen petición</th>
                        <th data-align="center">Prioridad</th>
                        <th data-align="center">Descripción</th>
                        <th data-align="center">Item</th>
                        <th data-align="center">Dirección Emplazamiento</th>
                        <th data-align="center">Fecha de inicio</th>
                        <th data-align="center">Fecha de fin</th>
                        <th data-align="center">Localidad</th>
                        <th data-align="center">Tipo de relación</th>
                        <th data-align="center">Tipo de petición</th>
                        <th data-align="center">Estado</th>
                      </tr>
                  </thead>
                  <tbody id="rela_lista"></tbody>
                  {{#relacion}}
                    <tr>
                      <td>{{inre_resumen}}</td>
                      <td>{{inre_prioridad}}</td>
                      <td>{{inre_descripcion}}</td>
                      <td>{{inre_item}}</td>
                      <td>{{inre_emplazamiento}}</td>
                      <td>{{inre_fecha_inicio}}</td>
                      <td>{{inre_fecha_fin}}</td>
                      <td>{{inre_localidad}}</td>
                      <td>{{inre_relacion}}</td>
                      <td>{{inre_peticion}}</td>
                      <td>{{inre_estado}}</td>
                    </tr>
                  {{/relacion}}
                </table>


                <!--div class="panel-body siom-indi-lista siom-lista-full-height">
                    <div class="indi">
                      <div class="estado text-center">
                        <span class="id">TEST</span>
                     </div>
                </div>
                <div class="panel-footer siom-paginacion">
                        <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
                </div-->
            </fieldset>
            <fieldset class="siom-fieldset">
                <legend>Bitácora</legend>
                <!--div class="panel-body siom-indi-lista siom-lista-full-height">
                    <div class="estado text-center">
                        <span class="id">TEST</span>
                     </div-->

                  <table class="table table-striped table-hover table-condensed"   data-sort-order="desc">
                    <thead>
                      <tr>
                        <th data-align="center" >Tipo</th>
                        <th data-align="center" >Resumen</th>
                        <th data-align="center" >A</th>
                        <th data-align="center" >Fecha</th>
                        <th data-align="center" >Remitente</th>
                        <th data-align="center" >Remitente SOTIE</th>
                      </tr>
                    </thead>
                    <tbody id="bita_lista"></tbody>
                    {{#bitacora}}
                    <tr>
                      <td>{{inbi_tipo}}</td>
                      <td>{{inbi_resumen}}</td>
                      <td>{{inbi_a}}</td>
                      <td>{{inbi_fecha}}</td>
                      <td>{{inbi_remitente}}</td>
                      <td>{{inbi_remitente_sotie}}</td>
                    </tr>
                    {{/bitacora}}
                  </table>
            </fieldset>
           
        </div>
    </div>
{{else}}
    <div class="alert alert-danger" role="alert">
        {{data.error}}
        {{#if data.debug}}
            <br><small>{{data.debug}}</small>
        {{/if}}
    </div>
{{/if}}
<script src="js/indi_detalle.js" type="text/javascript" charset="utf-8"></script>

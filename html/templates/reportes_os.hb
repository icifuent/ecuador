
<div class="row siom-section-header">
  <div class="col-md-5">
  </div>
  <div class="col-md-7">
  </div>
</div>

{{#if data.status}}

<fieldset class="siom-fieldset">
  <legend>Archivos Generados</legend>
  {{{loadTemplate "template_documentos_lista_os" data}}}
</fieldset>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/reportes_os.js" type="text/javascript" charset="utf-8"></script>
<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Bandeja Reporte de Falla</h5>
    </div>
</div>

{{#if data.status}}
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-body">
                <form class="form-horizontal siom-form-tiny" action="#/defaMovil/bandeja/filtro" method="POST" id="siom-form-defaMovil-filtro"
                    name="siom-form-defaMovil-filtro">
                    <div class="form-group row text-center">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-default" type="reset" value="Reset">Limpiar Filtros</button>
                            <!--span class="glyphicon" aria-hidden="true">Limpiar Filtros</span-->
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-primary btn-default" id="BuscarDefaMovilBandeja" name="BuscarDefaMovilBandeja">Buscar</button>
                            <!--span class="glyphicon" aria-hidden="true">Buscar</span-->
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label for="id" class="col-md-3 control-label">N° Falla</label>
                        <div class="col-md-9">
                            <input autocomplete="off" type="text" class="form-control" id="defa_id" name="defa_id" value="{{data.defa_id}}" maxlength="8" pattern="[0-9]" placeholder="Ingrese número reporte falla.">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tecnologia" class="col-md-3 control-label">Usuario</label>
                        <div class="col-md-9">
                            <select class="selectpicker" type="button" id="menu1" data-toggle="dropdown" data-width="100%" name="defaUser"
                                id="defaUser" data-container="body" data-live-search="true" style="width: 100%;">
                                <option value="0">Seleccione Usuario</option>
                                {{#data.defaUser}}
                                <option value="{{usua_id}}">{{usua_nombre}}</option>
                                {{/data.defaUser}}
                            </select>
                        </div>
                    </div>
                    <!--div class="form-group">
              <label for="tecnologia" class="col-sm-3 control-label">Empresa</label>
              <div class="col-sm-6">
                <select class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown" data-width="100%" name="defaEmpr" id="defaEmpr" data-container="body" data-live-search="true">
                  <option  value="0" >Seleccione Empresa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				          {{#data.defaEmpr}}
                  <option value="defa_empresa">{{defa_empresa}}</option>
				          {{/data.defaEmpr}}
                </select>
              </div>
            </div-->
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-9" id="defaMovil-bandeja-lista">
    </div>
</div>
<div class="modal fade" id="ModalSolicitud">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Solicitud</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="3" placeholder="Ingrese razón de solicitud"></textarea>
                <small>Se le enviará una notificación al encargado respectivo para autorizar solicitud.</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" data-loading-text="Solicitando..." data-complete-text="Solicitado!">Confirmar</button>
            </div>
        </div>
    </div>
</div>

{{else}}
<div class="alert alert-danger" role="alert">
    {{data.error}} {{#if data.debug}}
    <br>
    <small>{{data.debug}}</small>
    {{/if}}
</div>
{{/if}}

<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/defaMovil_bandeja.js" type="text/javascript" charset="utf-8"></script>
<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Cerrar mantenimiento</h5>
  </div>
  <div class="col-md-7">
    
  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset">
  <legend>Mantenimiento</legend>
  {{{loadTemplate "mnt_descripcion" data.mantenimiento}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Informe</legend>
  {{#each data.informe}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA CREACIÓN</label>
    <label class="col-sm-2 siom-value">{{info_fecha_creacion}}</label>
    <label class="col-sm-2 siom-label">CREADO POR</label>
    <label class="col-sm-3 siom-value">{{usua_creador}}</label>
    <label class="col-sm-1 siom-label">ESTADO</label>
    <label class="col-sm-1 siom-value">{{info_estado}}</label>
    <label class="col-sm-1 siom-link">
      <a href="#/mnt/informe/{{../data.mantenimiento.mant_id}}/validar/{{info_id}}" target="blank"> REVISAR </a>
    </label>
  </div>

  {{#isnt info_estado 'SINVALIDAR'}}
  <div class="row">
    <label class="col-sm-2 siom-label">FECHA VALIDACIÓN</label>
    <label class="col-sm-2 siom-value">{{info_fecha_validacion}}</label>
    <label class="col-sm-2 siom-label">VALIDADO POR</label>
    <label class="col-sm-2 siom-value">{{usua_validador}}</label>
  </div>
  {{/isnt}}
  
  {{#gt ../data.informe.length 1}}
  <hr>
  {{/gt}}
  {{/each}}
  
</fieldset>

<hr>

<form class="form form-horizontal" role="form" action="#/mnt/cerrar/{{data.mantenimiento.mant_id}}" method="POST" id="siom-cerrar-form">
  <!--
  <div class="row text-center">
    <div class="col-md-10 col-md-offset-1">
        [DEFINIR TEXTO MNT]<br><br><br>
    </div>
  </div>
  --> 
  <div class="row text-center">
    <div class="col-md-12">
      <input type="hidden" name="mant_estado" value=""/>
      <a href="#/mnt/bandeja" class="btn btn-default">Cancelar</a>
      <button class="btn btn-danger"  data-mant-estado="RECHAZADA">Rechazar</button>
      <button class="btn btn-success" data-mant-estado="APROBADA">Aprobar</button>
    </div>
  </div>
</form>

{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<script src="js/mnt_cerrar.js" type="text/javascript" charset="utf-8"></script>


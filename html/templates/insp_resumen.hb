<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Resumen inspecciones</h5>
  </div>
  <div class="col-md-7 text-right">
    <div class="btn-group btn-group-sm" data-toggle="buttons">
      <label class="btn btn-primary active" data-tipo="resumen">
        <input autocomplete="off" type="radio" name="options" checked> Resumen
      </label>
      <label class="btn btn-primary" data-tipo="detalle">
        <input autocomplete="off" type="radio" name="options" id="option2" href="#/inve/detalle/{{inve_id}}"> Detalle
      </label>
    </div>
  </div>
</div>


<form class="form-horizontal siom-form-tiny" role="form" action="#/insp/resumen" method="POST" id="siom-form-resumen-inspeccion-filtro">
  <input  type="hidden" id="tipo_resumen" name="tipo_resumen" value="resumen"> </input>
  <fieldset class="siom-fieldset">
    <legend>Filtros</legend>
    <table width="100%" cellpadding="5px" cellspacing="5px" class="table-filters">
      <tr>
        <td width="15%" valign="top">
          <div class="siom-label">RANGO FECHAS</div>
          <div class="siom-value">
            <input autocomplete="off" id="fecha_inicio" class="form-control" size="12" type="text" data-date-autoclose="true" name="fecha_inicio"
              placeholder="Fecha inicio" />
            <input autocomplete="off" id="fecha_fin" class="form-control" size="12" type="text" data-date-autoclose="true" name="fecha_fin"
              placeholder="Fecha termino" style="margin-top:5px;" />
          </div>
        </td>

        <td width="12%" valign="top">
          <div class="siom-label">PERÍODO</div>
          <div class="siom-value">
            <select class="selectpicker" data-width="100%" title='Todos' name="periodo" data-container="body" multiple="true">
              {{#periodo}}
              <option value="{{peri_filtro}}">{{peri_nombre}}</option>
              {{/periodo}}
            </select>
          </div>
        </td>

        <td width="1%" valign="top"></td>

        <td width="12%" valign="top">
          <div class="siom-label">ZONA CONTRATO</div>
          <div class="siom-value">
            <select class="selectpicker" data-width="100%" title='Todas' name="zona_contrato" data-container="body" multiple="true">
              {{#zona_contrato}}
              <option value="{{zona_id}}">{{zona_nombre}}</option>
              {{/zona_contrato}}
            </select>
            <input autocomplete="off" type="radio" name="agrupador" value="zona_contrato" checked="true">
            <span class="siom-label">Agrupar</span>
          </div>
        </td>

        <td width="12%" valign="top">
          <div class="siom-label">ZONA MOVISTAR</div>
          <div class="siom-value">
            <select class="selectpicker" data-width="100%" title='Todas' name="zona_movistar" data-container="body" multiple="true">
              {{#zona_movistar}}
              <option value="{{zona_id}}">{{zona_nombre}}</option>
              {{/zona_movistar}}
            </select>
            <input autocomplete="off" type="radio" name="agrupador" value="zona_movistar">
            <span class="siom-label">Agrupar</span>
          </div>
        </td>

        <td width="12%" valign="top">
          <div class="siom-label">CLUSTER</div>
          <div class="siom-value">
            <select name="zona_cluster" class="selectpicker" data-width="100%" title='Todas' data-container="body" multiple="true">
              {{#cluster}}
              <option value="{{zona_id}}">{{zona_nombre}}</option>
              {{/cluster}}
            </select>
            <input autocomplete="off" type="radio" name="agrupador" value="zona_cluster">
            <span class="siom-label">Agrupar</span>
          </div>
        </td>

        <td width="12%" valign="top">
          <div class="siom-label">REGIÓN</div>
          <div class="siom-value">
            <select class="selectpicker" data-width="100%" title='Todas' name="regi_id" data-container="body" multiple="true">
              {{#regiones}}
              <option value="{{regi_id}}">{{regi_nombre}}</option>
              {{/regiones}}
            </select>
            <input autocomplete="off" type="radio" name="agrupador" value="regi_id">
            <span class="siom-label">Agrupar</span>
          </div>
        </td>

        <td width="12%" valign="top">
          <div class="siom-label">COMUNA</div>
          <div class="siom-value">
            <select name="comu_id" class="selectpicker" data-width="100%" title='Todas' data-container="body" multiple="true">
              {{#comunas}}
              <option value="{{comu_id}}">{{comu_nombre}}</option>
              {{/comunas}}
            </select>
            <input autocomplete="off" type="radio" name="agrupador" value="comu_id">
            <span class="siom-label">Agrupar</span>
          </div>
        </td>

        <td width="12%" valign="top">
          <div class="siom-label">USUARIO</div>
          <div class="siom-value">
            <select name="usua_id" class="selectpicker" data-width="100%" title='Todos' data-container="body" multiple="true">
              {{#usuario}}
              <option value="{{usua_id}}">{{usua_nombre}}</option>
              {{/usuario}}
            </select>
            <input autocomplete="off" type="radio" name="agrupador" value="usua_id">
            <span class="siom-label">Agrupar</span>
          </div>
        </td>

      </tr>
    </table>
  </fieldset>
</form>

<div id="inspecciones-detalle" style="margin-top:10px;">
</div>

<button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..." data-processing-text="Generando...">Descargar en Excel</button>


<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/insp_resumen.js" type="text/javascript" charset="utf-8"></script>
<div class="row siom-section-header">
  <div class="col-md-5">
    <h5>Presupuesto orden de servicio</h5>
  </div>
  <div class="col-md-7">

  </div>
</div>

{{#if data.status}}
<fieldset class="siom-fieldset">
  <legend>Emplazamiento</legend>
  {{{loadTemplate "empl_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset"  style="margin-top:20px;">
  <legend>Orden de servicio</legend>
  {{{loadTemplate "os_descripcion" data.os}}}
</fieldset>

<fieldset class="siom-fieldset" style="margin-top:20px;">
  <legend>Presupuesto</legend>
  <form role="form" action="#/os/presupuesto/{{data.orse_id}}" method="POST" id="form_presupuesto" class="form siom-form-tiny">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <select id="grupos-lpu" class="selectpicker" data-width="100%" name="grup_id" title="Seleccione Grupo">
          <option value=""></option>
          {{#data.lpu_grupo}}
          <option value="{{lpgr_id}}">{{lpgr_nombre}}</option>
          {{/data.lpu_grupo}}

        </select>
      </div>

      <div class="form-group">
       <select id="subgrupo" class="selectpicker" data-width="100%" name="subg_id" data-hide-disabled="true" title="Seleccione subgrupo">
         <option value="" data-default="true"></option>
          {{#data.lpu_subgrupo}}
           <option value="{{lpgr_id}}" data-lpgr_id_padre="{{lpgr_id_padre}}">{{lpgr_nombre}}</option>
           {{/data.lpu_subgrupo}}
        </select>
      </div>

   <!--   <div class="form-group">
       <select id="subgrupodetalle" class="selectpicker" data-width="100%" name="subgdet_id" data-hide-disabled="true" title="Seleccione detalle subgrupo">
         <option value="" data-default="true"></option>
          {{#data.lpu}}
           <option value="{{lpit_id}}" data-subgrupodetalle="{{lpgr_id}}">{{lpgr_id_padre}} {{lpgr_id}} {{lpit_nombre}}</option>
           {{/data.lpu}}
        </select>
      </div> -->


      <div class="form-group">
    	 <input type="text" class="form-control input-sm" id="filtro_lpu">
      </div>

      <div class="form-group">
      	<div class="siom-os-lista siom-os-presupuesto-lista siom-lista-full-height-header" id="lista_lpu">
      		{{#data.lpu}}
            <div class="siom-os-presupuesto" data-grupo="{{lpgr_id}}">
                <div class="grupo">{{lpgr_nombre}}</div>
    	  				<div class="nombre">{{lpit_nombre}}</div>
    	  				<div class="comentario">{{lpit_comentario}}</div>
    	  				<div class="unidad">Unidad: <strong>{{lpit_unidad}}</strong></div>
                <div class="precios">
    			  		{{#precios}}
    			  		<button type="button" class="btn btn-primary btn-xs precio" data-lpit-id="{{../lpit_id}}" data-lpit-nombre="{{../lpit_nombre}}" data-lpit-comentario="{{../lpit_comentario}}" data-lpip-id="{{lpip_id}}" data-lpgr-nombre="{{../lpgr_nombre}}" data-lpip-precio="{{lpip_precio}}" data-lpgc-nombre="{{lpgc_nombre}}">
    			  			<small>{{lpgc_nombre}} | <strong>{{lpip_precio}} UF</strong></small>
    			  		</button>
    			  		{{/precios}}
                </div>
    			</div>
    	  	{{/data.lpu}}
      	</div>
     </div>



    </div>
    <div class="col-md-8">
      <div class="row siom-os-presupuesto-header">
        <div class="col-xs-1 less-padding">#</div>
        <div class="col-xs-6">Actividad</div>
        <div class="col-xs-1 less-padding">Categoria</div>
        <div class="col-xs-1 less-padding">Valor unitario</div>
        <div class="col-xs-1 less-padding">Cantidad</div>
        <div class="col-xs-1 less-padding">Subtotal</div>
      </div>
      <div class="siom-os-lista siom-os-presupuesto-lista siom-lista-full-height-header" id="lista_presupuesto">
      </div>
    	<div class="row">
        <div class="col-md-1 text-left">
            <!--
            <button type="button" class="btn btn-default btn-xs siom-presupuesto-exportar" id="download" data-loading-text="Exportando..."data-processing-text="Procesando...">
              Exportar a Excel
            </button>
            -->
        </div>
    		<div class="col-md-1 col-sm-offset-7  text-right">TOTAL</div>
    		<div class="col-md-2 siom-presupuesto-total text-right" id="total_presupuesto" data-total="0">0 UF</div>
    	</div>
    </div>
  </div>
  <hr>
  <div class="row text-center siom-form-actions">
  	<div class="col-md-12">
      <a href="#/os/bandeja" class="btn btn-default">Cancelar</a>
  		<button type="submit" id="submit_presupuesto" class="btn btn-primary" data-loading-text="Agregando presupuesto..." autocomplete="off">Agregar presupuesto</button>
  	</div>
  </div>
  </form>
</fieldset>
{{else}}
<div class="alert alert-danger" role="alert">
  {{data.error}}
  {{#if data.debug}}
  <br><small>{{data.debug}}</small>
  {{/if}}
</div>
{{/if}}

<style>
.inputNro{
	font-family: Tahoma, Verdana, Arial;
	font-size: 11px;
	color: #707070;
	background-color: #FFFFFF;
	border-width:0px 0px 0px 0px;
}
</style>


<script src="js/bootstrap-wizard.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-fileinput.min.js"  type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/os_presupuesto.js" type="text/javascript" charset="utf-8"></script>


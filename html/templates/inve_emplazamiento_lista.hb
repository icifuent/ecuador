<div class="panel panel-default">
    <div class="panel-body siom-mnt-lista siom-lista-full-height" style="overflow:auto !important">

        {{#if data}}
        <div class="panel-body siom-os-lista siom-lista-full-height">
            {{#data}}
            <div class="os-crear">
                <input type="hidden" value="bandeja" id="inventario"></input>
                <div class="estado text-center">
                    <span class="glyphicon glyphicon-stop status" aria-hidden="true"></span>

                </div>

                <div class="acciones col-md-4 text-right">
                    <div class="link"><a class="btn btn-primary btn-xs" href="#/inve/emplazamiento/{{empl_id}}/items" value="detalle" data-tipo="detalle">Ver detalle</a></div>
                </div>
                <div class="info text-left">
                    <h4>{{empl_nombre}} <small>{{empl_nemonico}}</small></h4>
                    <div class="info_adicional">Clasificación: <strong>{{clas_nombre}}</strong>, Dirección: <strong>{{empl_direccion}}</strong>, Responsable: <strong>{{empl_responsable}}</strong></div>
                </div>

            </div>
            <hr> {{/data}}
        </div>

        {{/if}}

        <hr>

    </div>

    <div class="panel-footer siom-paginacion">
        <div class="row">
            <div class="col-md-4 text-left">
                <!--
                {{#if status}}
                <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..." data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
                {{/if}}
                -->
            </div>

            <div class="col-md-8 text-right">
                <div class="pagination-info">
                    Página {{pagina}}/{{paginas}} ({{total}} registros)
                </div>
                <div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
            </div>
        </div>
    </div>

</div>



<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/inve_emplazamiento_lista.js" type="text/javascript" charset="utf-8"></script>
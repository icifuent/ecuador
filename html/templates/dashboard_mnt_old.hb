<div class="row siom-section-header">
    <div class="col-md-5">
        <h5>Dashboard Mantenimiento</h5>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <form class="form-horizontal siom-form-tiny" role="form" action="#/dashboard/mnt" method="POST" id="siom-form-dashboard-mnt">
            
                <fieldset class="siom-fieldset" style="padding-bottom:20px; margin-top:0px;">
                    <legend>Filtros</legend>
                    
                    <div class="col-md-4" id="FiltroEspecialidades" style="display:block">
                        <div>
                            <label>Especialidades</label>
                        </div>
                        <select class="selectpicker" data-width="100%" name="espe_id" multiple title="TODAS" data-container="body">
    						{{#especialidades}}
    						<option value="{{espe_id}}">{{espe_nombre}}</option>
    						{{/especialidades}}
    					</select>
                    </div>


                    <div class="col-md-4" id="filtroPeriodo" style="display:block">
                        <div>
                            <label>Periodos</label>
                        </div>
                        <select class="selectpicker" data-width="100%" name="peri_id" multiple title="TODOS" data-container="body">
    						{{#periodos}}
    						<option value="{{peri_id}}">{{peri_nombre}}</option>
    						{{/periodos}}
                            
    					</select>
                    </div>

                    <div class="col-md-4">
                        <div><label>Fecha de referencia</label></div>
                        <input id="mant_fecha_referencia" class="form-control" size="14" type="text" name="mant_fecha_referencia" value="{{#if data.mant_fecha_referencia}} {{formatDate data.mant_fecha_referencia '%d-%m-%Y'}} {{/if}}" />
                    </div>
                </fieldset>
        </form>
    </div>	
    <div class="col-md-6">  
            <img src="img/dashboardMNT.png" id="leyendaMNT">        
    </div>
    
</div>
<br/>
 
<div id="dashboard_mnt_graficos"></div>

<script src="js/dashboard_mnt.js" type="text/javascript" charset="utf-8"></script>

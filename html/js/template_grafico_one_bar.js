(function($) {
    function drawCharts() {
        
        console.log('drawCharts');

        var grafico = $("#siom-grafico-bar");
        var data_json = grafico.data("data");
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Nombre'); // Nombre Leyenda
        data.addColumn('number', ''); 
        data.addColumn({type: 'string', role: 'style'});   
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role:"annotation"});  
        data.addColumn('number', 'Umbral'); // Nombre Leyenda
        data.addColumn({type: 'string', role: 'style'});  
        data.addColumn({type: 'string', role: 'tooltip'});

        var color_aprobadas = '#00b050';
        var color_rechazadas = '#c00000';
        var color_umbral='#000000';
        var rows = [];

        for (var i = 0; i < data_json.length; i++) {
            console.log('total');
            data_json[i]['total'];
            //Tasa Disponibilidad
            var color_total = '#008000';

            /*
            9.5 ≤ y ≤ 10    Excelente
            8.5 ≤ y < 9.5   Bueno
            7.5 ≤ y < 8.5   Regular
            0 ≤ y < 7.5 Insuficiente
            */                          
            if(data_json[i]['total']>=9.5){
                color_total = '#00b050';            // Excelente      
            } else if  (data_json[i]['total']>=8.5){
                color_total = '#00b0f0';            // Bueno
            } else if(data_json[i]['total']>=7.5){
                color_total = '#ffc000';            // Regular
            } else {
                color_total = '#c00000';            // Insuficiente
            }         

            

            rows.push(  [data_json[i]['nombre']
                        , parseFloat(data_json[i]['total'])
                        , color_total
                        , parseFloat(data_json[i]['total']) +''
                        , '' + parseFloat(data_json[i]['total'])
                        , 8.5
                        , color_umbral
                        , 'Umbral: 8.5'
            ]);                        
        };

        console.log(rows);
        data.addRows(rows);

        var options = {
            title: '',
            titleTextStyle: {fontSize: 10},
            height: 300,
            width: 900,
            
            chartArea: {left:50,top:15,width:'95%',height:'70%'},
            //chartArea: {left: 200, top: 0, width: '90%', height: '75%'},
            vAxis: {minValue:0,maxValue: 10, gridlines: { count: 11 }},
            hAxis: {
                textStyle : {
                                fontSize:8
                },
                format: 'Q#'
            },
            //bar: {groupWidth: '50%'},
            bar: {groupWidth: '80%'},            
            legend:{ position:'none' /*position: 'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}*/},
            colors: [color_total, color_umbral],
            isStacked: true,
            seriesType: 'bars',
            annotations: {
                             textStyle: {
                                            fontSize: 9,
                                            color: '#871b47',
                                              /* The color of the text outline.*/
                                            auraColor: '#d799ae',
                                              /* The transparency of the text.*/
                                            opacity: 0.8
                             }
            },
            series: {1: {type: 'line',lineWidth: 2 } },
        };
        var chart2 = new google.visualization.ComboChart(grafico[0]);
        chart2.draw(data, options);
    }

    if (typeof google.visualization != "undefined" &&
        typeof google.visualization.PieChart != "undefined" &&
        typeof google.visualization.ColumnChart != "undefined" &&
        typeof google.visualization.ComboChart != "undefined") {
        drawCharts();
    }
    else {
        google.setOnLoadCallback(drawCharts);
    }

})(jQuery);
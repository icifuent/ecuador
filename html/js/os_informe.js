(function($) { 
	var validar = true;
	//inicializar componentes
    $('.datepicker').datepicker({
			    format: "dd-mm-yyyy",
			    viewMode: "days", 
			    minViewMode: "days",
			    language: "es"
			});
    $('.timepicker').timepicker({showMeridian:false,defaultTime: false});

    if($('#siom-form-informe').length){
     	window.scrollToElement($('#siom-form-informe'));
    }

    $('#siom-form-informe').submit(function() {
		if(validar){
			if($(this).find(':input[name="info_fecha_visita"]').val()==""){
				alert("Debe ingresar fecha de visita");
				return false;
			}
			if($(this).find(':input[name="info_hora_visita"]').val()==""){
				alert("Debe ingresar hora de visita");
				return false;
			}
			if($(this).find(':input[name="info_descripcion"]').val()==""){
				alert("Debe ingresar descripción");
				return false;
			}

			files = $(this).find('.file-preview .file-preview-thumbnails .file-preview-frame');
    		if(files.length==0){
    			alert("Debe incluir archivo con informe");
    			return false;
    		}
    		else{
    			for(i=0;i<files.length;i++){
    				textarea = $(files[i]).find("textarea");
    				if(textarea.val()==""){
    					textarea.addClass("error");
    					alert("Debe agregar una descripción para archivo");
    					return false;
    				}
    				textarea.removeClass("error");
    			}
    		}
		}

		//formatear fecha
    	fecha_visita   = FormatDatetime($(this).find(':input[name="info_fecha_visita"]').val(),$(this).find(':input[name="info_hora_visita"]').val())
		$(this).find(':input[name="info_fecha_visita"]').val(fecha_visita)
		

		//filtrar datos no relevantes
		$(this).find(':input[name="info_hora_visita"]').attr("disabled",true);
    	$(this).find(':input[name="hour"]').attr("disabled",true);
    	$(this).find(':input[name="minute"]').attr("disabled",true);

		$("#submit_informe").button('loading');
        return true;
	});

    $("#files").fileinput({
	    uploadUrl: "/site/file-upload",
	    uploadAsync: true,
	    maxFileCount: 10,
	    showUpload:false,
	    showCaption:false,
	    browseLabel: "Agregar archivo",
	    browseClass: "btn btn-sm btn-default",
	    removeLabel: "Descartar archivos",
	    removeClass: "btn btn-sm btn-default",
	    removeTitle: "Descartar todos los archivos",
	    previewClass: "siom-os-informe-preview-list",
	    progressCompleteClass: "progress-bar progress-bar-info",
	    dropZoneEnabled:false,
	    fileActionSettings:{
	    	removeIcon: '',
    		removeClass: 'glyphicon glyphicon-trash',
    		removeTitle: 'Descartar archivo'
	    },
	    layoutTemplates:{
	    	main2: '{browse}\n{remove}\n{cancel}\n{upload}\n{preview}\n<div class="kv-upload-progress hide"></div>',
	    	preview: '<div class="file-preview {class}">\n' +
					    '    <div class="close fileinput-remove hide">×</div>\n' +
					    '    <div class="{dropClass}">\n' +
					    '    <ul class="list-group file-preview-thumbnails">\n' +
					    '    </ul>\n' +
					    '    <div class="clearfix"></div>' +
					    '    <div class="file-preview-status text-center text-success"></div>\n' +
					    '    <div class="kv-fileinput-error"></div>\n' +
					    '    </div>\n' +
					    '</div>',
			footer: '<div class="file-thumbnail-footer">\n' +
				        '    {actions}\n' +
				        '</div>',
		    actions: '<div class="file-actions">\n' +
				        '    <div class="file-footer-buttons">\n' +
				        '        {delete}' +
				        '    </div>\n' +
				        '    <div class="file-upload-indicator hide" tabindex="-1" title="{indicatorTitle}">{indicator}</div>\n' +
				        '    <div class="clearfix"></div>\n' +
				        '</div>',
			actionDelete: '<i class="kv-file-remove {removeClass}" title="{removeTitle}"{dataUrl}{dataKey}>{removeIcon}</i>\n'
	    },
	    previewTemplates:{
	    	image: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-picture" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        	'<input type="hidden" name="docu_previewid" value="{previewId}">\n'+
			        	'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			html: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-file" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			text: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-file" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			video: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-film" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			audio: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-music" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			flash: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-film" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			object: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-file" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n',
			other: '<li class="file-preview-frame list-group-item" id="{previewId}" data-fileindex="{fileindex}">\n' +
			        '<div class="row">\n'+ 
			        	'<div class="col-md-1">\n'+
			        	'<span class="glyphicon glyphicon-file" aria-hidden="true" style="font-size:20px"></span>\n'+
			        	'</div>\n'+ 
			        	'<div class="col-md-10">\n'+
			        		'<textarea class="form-control" name="docu_descripcion" placeholder="Descripción" rows="1"></textarea><small style="word-break: break-all;"><strong>archivo: </strong>{caption}</small>\n' +
			        	'</div>\n'+ 
			        	'<div class="col-md-1">\n'+
			        		'{footer}\n' +
			        	'</div>\n'+ 
			        '</div>\n'+   
			        '</li>\n'
	    }
	});

	function FormatDatetime(fecha,hora){
    	fecha = fecha.split("/");
    	return fecha[2]+"-"+fecha[1]+"-"+fecha[0]+" "+hora;
    }
})(jQuery);
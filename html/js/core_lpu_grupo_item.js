(function($) {

	var filterTimer=null;
	var filterTimeout=500;

    $('.selectpicker').selectpicker();

    $('#siom-form-lpu-grupo-item input[name=lpgr_nombre]').keyup(function () {
         FiltrarConDelay();
    })

	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-lpu-grupo-item").submit();
         },filterTimeout);
	}

    $(document).on('click','button.editar',function(e){
	     LPU_data = $(this).data("data");
	     $.each(LPU_data, function( key, value ) {
	       $('#EditFormLPUGrupoItem').find('#'+key).val(value);
	     });

       $('select.input-sm option:selected').each(function(i, selected){
           cont_id = $(selected).val();
       });
    
      lpugr_id  = $("#lpgr_id").val();

	    $('#wizardLPUGrupoItem').wizard("next");
    });



    //Guarda datos Edición usuario
    //-----------------------------------------------
	$('#submit_Guardar').click(function(e){
	   	var button = $(this);
	   	params = {};
      $('select.input-sm option:selected').each(function(i, selected){
           cont_id = $(selected).val();
       });
      lpugr_id  = $("#lpgr_id").val();
      lpu_id    = $("#lpu_id").val();

	   $('#EditFormLPUGrupoItem input').each(
		    function(index){
		        var input = $(this);
		        params[input.attr('id')]= input.val();
		    }
		);

  		url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/item/add';
		if(params.lpugr_id){
			url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/item/upd/'+params.lpugr_id;
      console.log(url);
		}
		else{
			delete params["lpugr_id"];
		}
		button.button('loading');
		$.post(url,params,function(json) {
			button.button('reset');
  			if(json.status){

  			   $("#EditFormLPUGrupo").submit();
  			}
  			else{
  				alert(json.error);
  			}
	  	}).fail(function(){
	  		button.button('reset');
			alert("Error al guardar [codigo: 1001]");
		});
  	});

    $(document).on('click', 'button.eliminarLPU', function(e){
        lpu_id     = $(this).data("id");
        lpu_nombre = $(this).data("nombre");
        lpu_estado = $(this).data("estado");
        lpu_contra = $(this).data("contrato");

        if (lpu_estado=="ACTIVO"){
            nuevo_estado        = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
      	}
        else{
            nuevo_estado        = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("La LPU <b>"+lpu_estado+"</b> será <b>"+nombre_nuevo_estado+"</b>,<br>Desea continuar?",function(status){
              if(status==true){
                    $.get('rest/core/contrato/'+lpu_contra+'/lpu/upd/'+lpu_id+'/?lpu_estado='+nuevo_estado, null, function(json) {
                    }).fail(function(xhr, textStatus, errorThrown){
                      alert("Error "+xhr.status+": "+xhr.statusText);
                  });
                 FiltrarConDelay();
              }
        });

    });



})(jQuery);

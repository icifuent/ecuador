(function($) {
	$('#pagination').bootpag().on("page", function(event, num){
		//console.log("#pagination: "+num);
		//console.log($('#pagina'));
		page = num;
		$('#pagina').val(page);
		$("#siom-form-sla-cronograma").submit();
	});
	
	$('.BotonSLACronogramaValidacionEditar').click(function(event) {
		console.log('.BotonSLACronogramaValidacionEditar click:');

		if( $(this).hasClass('clase-editar') ){   
			//console.log("EDITAR");
			$(this).removeClass('clase-editar');
			$(this).addClass('clase-guardar');
			
			$(this).children("span").removeClass("glyphicon-pencil");
			$(this).children("span").addClass("glyphicon-floppy-disk");

			$(this).parent().parent().find(".clase-data").attr("disabled",false);
		}
		else{
			//console.log("GUARDAR");				
			var that = this;
			var data = {};
			var mant_id = $(this).data('mant_id');
			data['sla_cronograma_ajuste'] = $(this).parent().parent().find(".clase-data[name='sla_cronograma_ajuste']").val();
			data['sla_cronograma_exclusion'] = $(this).parent().parent().find(".clase-data[name='sla_cronograma_exclusion']").is(':checked')?1:0;
			data['sla_cronograma_observacion'] = $(this).parent().parent().find(".clase-data[name='sla_cronograma_observacion']").val();
			
                        if((data['sla_cronograma_ajuste'] == null || data['sla_cronograma_ajuste'] == "")){
                                alert("Debe ingresar un delta de ajuste");
				return;
			}
                   
                        if( data['sla_cronograma_ajuste'] % 1 !== 0 || data['sla_cronograma_ajuste']<0){
                                alert("El ajuste debe ser un entero mayor o igual a cero");
				return;
                        }			

                        if( data['sla_cronograma_exclusion'] ==1 && (data['sla_cronograma_observacion'] == null || data['sla_cronograma_observacion'] == "")){
				alert("Debe ingresar una observacion para excluir el mantenimiento");
				return;
			}  
			if((data['sla_cronograma_observacion'] == null || data['sla_cronograma_observacion'] == "")){
				data['sla_cronograma_observacion'] = " ";
			}
			
			url = 'rest/core/sla_mantenimiento/upd/'+mant_id;
			$.post(url, data, function(json) {
				if (json.status) {
					$(that).removeClass('clase-guardar');
					$(that).addClass('clase-editar');
					
					$(that).children("span").removeClass("glyphicon-floppy-disk");
					$(that).children("span").addClass("glyphicon-pencil");

					$(that).parent().parent().find(".clase-data").attr("disabled",true);                                        
                                        
					var sla_cronograma_ajuste = data['sla_cronograma_ajuste'];					
					var sla_cronograma_margen = $(that).data('sla_cronograma_margen');
                                        var sla_cronograma_margen_ajuste = (parseFloat(sla_cronograma_margen) + parseFloat(sla_cronograma_ajuste)).toFixed(1); 
					$(that).parent().parent().find(".clase-data-output1").html( (sla_cronograma_margen_ajuste)+"" );
                                        $(that).parent().parent().find(".clase-data-output2").html( (sla_cronograma_margen_ajuste>= 0)?'SI':'NO'  );
				}
				else {
					alert("Error actualizando mantenimiento: " + json.error + ", ruta:" + url);
				}
			}).fail(function(xhr, textStatus, errorThrown) {
				alert("Error actualizando mantenimiento " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
			});
		} 

	});
	
	
	$("button.download").click(function(e){
	  $btn = $(this);
	  $btn.button("loading");
	  $.fileDownload("rest/core/repo/informe/"+$(this).data("info-id"), {
		  prepareCallback:function(url) {
			  $btn.button("processing");
		  },
		  successCallback: function(url) {
			  $btn.button('reset')
		  },
		  failCallback: function(responseHtml, url) {
			  $btn.button('reset')
			  alert(responseHtml);
		  }
	  });
	});        
	
})(jQuery);
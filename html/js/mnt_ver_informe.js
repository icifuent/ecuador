$(document).ready(function() {
	window.scrollToElement($('#siom-ver-informe'));

    $("button.download").click(function(e){
      $btn = $(this);
      $btn.button("loading");
      $.fileDownload("rest/core/repo/informe/"+$(this).data("info-id"), {
          prepareCallback:function(url) {
              $btn.button("processing");
          },
          successCallback: function(url) {
              $btn.button('reset')
          },
          failCallback: function(responseHtml, url) {
              $btn.button('reset')
              alert(responseHtml);
          }
      });
    });

});

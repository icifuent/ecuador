(function($) { 
	$("#form_adjuntar").on('change','.btn-file :file', function() {
	    var input = $(this),
	   	id     = input.data("id");
	   	label  = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	   	nextId = parseInt(id)+1;
	   	size   = parseInt(input[0].files[0].size);
	   	maxSize=parseInt(window.config.max_file_upload);
        if(size!=null && size > maxSize){
            alert("Tamaño de archivo supera máximo soportado");
            return;
        }

	   	var html = '<li class="list-group-item">';
	   	html+='<a class="btn btn-default btn-xs" data-id="'+id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	   	html+='<div>';
	   	html+='<div><textarea class="form-control" name="archivos_descripciones" rows="1" placeholder="Ingresar descripción"></textarea></div>';
	   	html+='<small>'+label+'</small>';
	   	html+='</div>';
	   	html+='</li>';
	    $(html).appendTo($("#listado-archivos"));

    	$('<input type="file" name="archivos[]" id="archivo-'+nextId+'" data-id="'+nextId+'">').insertAfter($(this));
	
	
	});
	$("#listado-archivos").on('click','a.btn', function(e) {
		var id = $(this).data("id");
		$("#archivo-"+id).remove();
		$(this).parent().remove();
	});
})(jQuery);

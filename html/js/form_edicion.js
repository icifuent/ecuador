(function($) {
    var TRUE_VALUE = "_TRUE_";
    var FALSE_VALUE = "_FALSE_";

    var FORM_PROPERTIES  = {
                        "form_id":{"label":"ID","type":"STATIC","value":""},
                        "form_nombre":{"label":"Nombre","type":"TEXT","value":""},
						"form_orden":{"label":"Orden","type":"NUMERIC","value":""},
                        "form_alias":{"label":"Alias","type":"TEXT","value":""},
                        "form_descripcion":{"label":"Descripción","type":"TEXT","value":""}
                        };
    var FORM_OPTIONS  = {};

    var PAGE_PROPERTIES = {
                            "fogr_id":{"label":"ID","type":"STATIC","value":""},
                            "fogr_nombre":{"label":"Nombre","type":"TEXT","value":""},
                            "fogr_orden":{"label":"Orden","type":"NUMERIC","value":""},
							"fogr_estado":{"label":"Estado","type":"SELECT","options":{"ACTIVO":"ACTIVO","NO ACTIVO":"NOACTIVO"},"value":"ACTIVO"}
                            };

    var PAGE_OPTIONS  = {};


    var CONTROLS = [{"label":"Texto","type":"text"},
                    {"label":"Selector","type":"select"},
                    {"label":"Foto","type":"camera"},
                    {"label":"Agregador","type":"aggregator"},
                    {"label":"Guardar","type":"save"}];


    var CONTROLS_PROPERTIES = {
                                "foit_id":{"label":"ID","type":"STATIC","value":""},
                                "foit_nombre":{"label":"Nombre","type":"TEXT","value":""},
                                "foit_requerido":{"label":"Requerido","type":"SELECT","options":{"SI":"1","NO":"0"},"value":"0"},
                                "foit_estado":{"label":"Estado","type":"SELECT","options":{"ACTIVO":"ACTIVO","NO ACTIVO":"NOACTIVO"},"value":"ACTIVO"}
                                };

    var CONTROLS_OPTIONS =  {"text"  :{},
                             "select":{
                                         "data":{"label":"Datos","type":"ARRAY"}
                                       },
                             "camera":{
                                         "width":{"label":"Ancho","type":"TEXT"},
                                         "height":{"label":"Alto","type":"TEXT","hint":"Ingrese -1 para ajustar valor automáticamente"},
                                         "max_images":{"label":"Max. fotos","type":"TEXT"},
                                         "show_thumbs":{"label":"Miniaturas","type":"SELECT","options":{"SI":TRUE_VALUE,"NO":FALSE_VALUE},"value":FALSE_VALUE}
                                       },
                             "aggregator":{},
                             "save":{
                                       "show_email": {"label":"Mostrar email","type":"SELECT","options":{"SI":TRUE_VALUE,"NO":FALSE_VALUE},"value":FALSE_VALUE},
                                       "cancel_label": {"label":"Texto cancelar","type":"TEXT","value":"Cancelar"},
                                       "cancel_action": {"label":"Acción cancelar","type":"SELECT","options":{"GUARDAR":"save","CANCELAR":"cancel","GUARDAR Y CONTINUAR":"save_continue","GUARDAR Y CANCELAR":"save_cancel"},"value":"cancel"},
                                       "save_label": {"label":"Texto guardar","type":"TEXT","value":"Guardar"},
                                       "save_action": {"label":"Acción guardar","type":"SELECT","options":{"GUARDAR":"save","CANCELAR":"cancel","GUARDAR Y CONTINUAR":"save_continue","GUARDAR Y CANCELAR":"save_cancel"},"value":"save"}
                                      }
                            };

    var CONTROLS_AGGREGATOR = [ {"label":"Texto","type":"text"},
                                {"label":"Selector","type":"select"},
                                {"label":"Foto","type":"camera"}];

    var form = $("#form");
    var data = form.data("data");
    var pagesDrake = null;

    if(data){
        //console.log("Formulario");console.log(data);
        if (data.form) {
            properties = jQuery.extend(true, {}, FORM_PROPERTIES);
            $.each(properties, function(key) {
                if (data.form[key]) {
                    properties[key]['value'] = data.form[key];
                }
            });
            //options

            form.attr("data-properties", JSON.stringify(properties));
            form.attr("data-options", JSON.stringify(FORM_OPTIONS));
        }

        var data_pages_length = 0;
        if (data.pages != undefined) {
            data_pages_length = data.pages.length;
        }
        
        if( data.pages ){
            for(i=0;i<data_pages_length;i++){

                var page = data.pages[i];

                properties = jQuery.extend(true, {}, PAGE_PROPERTIES);
                $.each(properties, function(key) {
                    if(page[key]){
                        properties[key]['value'] = page[key];
                    }
                });

                //options

                page_data = {fogr_id:page.fogr_id,
                            fogr_nombre: page.fogr_nombre,
							fogr_orden: page.fogr_orden,
                            properties:properties,
                            options:PAGE_OPTIONS,
                            controls:CONTROLS};

                //console.log("Page "+page); console.log(page_data);
                
                LoadPage(form,page_data,function(p){
                    var controls = [];
                    var controls_aggregated = {};
                                        
                    for(j=0;j<page.controls.length;j++){
                        control   = page.controls[j];

                        foit_tipo     = control['foit_tipo'];
                        foit_opciones = {};

                        if(control['foit_opciones']!=""){
                            try{
                                foit_opciones = JSON.parse(control['foit_opciones']);
                            }
                            catch(e){
                                //console.log(e);
                                foit_opciones = {};
                            }
                        }

                        properties = jQuery.extend(true, {}, CONTROLS_PROPERTIES);
                        $.each(properties, function(key) {
                            if(control[key]){
                                properties[key]['value'] = control[key];
                            }
                        });


                        options = jQuery.extend(true, {}, CONTROLS_OPTIONS[foit_tipo.toLowerCase()]);
                        $.each(options, function(key) {
                            if(foit_opciones[key]){
                                options[key]['value'] = foit_opciones[key];
                            }
                        });

                        control_data = {foit_id:control.foit_id,
                                        foit_nombre: control.foit_nombre,
                                        foit_tipo: foit_tipo,
                                        indf_inventariable: (control.indf_genero)?(1):(0),
                                        indf_genero: (control.indf_genero)?(control.indf_genero):(""),
                                        indf_especie: (control.indf_especie)?(control.indf_especie):(""),
                                        indf_campo: (control.indf_campo)?(control.indf_campo):(""),
                                        indf_codigo: (control.indf_codigo)?(control.indf_codigo):(""),
                                        properties:properties,
                                        options:options,
                                        aggregated:foit_opciones.aggregated};

                        //console.log("Control"); console.log(control); console.log(control_data);
                        //console.log("foit_tipo"); console.log(foit_tipo);
                        //console.log("foit_opciones"); console.log(foit_opciones);

                        if(foit_opciones.aggregated){
                            controls_aggregated[control.foit_id] = control_data;
                        }
                        else{
                            controls.push(page.controls[j]);
                        }   
                    }
                    page.controls = controls;
                    
                    

                    for(j=0;j<page.controls.length;j++){
                        control   = page.controls[j];

                        foit_tipo     = control['foit_tipo'];
                        foit_opciones = {};

                        if(control['foit_opciones']!=""){
                            try{
                                foit_opciones = JSON.parse(control['foit_opciones']);
                            }
                            catch(e){
                                //console.log(e);
                                foit_opciones = {};
                            }
                        }

                        properties = jQuery.extend(true, {}, CONTROLS_PROPERTIES);
                        $.each(properties, function(key) {
                            if(control[key]){
                                properties[key]['value'] = control[key];
                            }
                        });


                        options = jQuery.extend(true, {}, CONTROLS_OPTIONS[foit_tipo.toLowerCase()]);
                        $.each(options, function(key) {
                            if(foit_opciones[key]){
                                options[key]['value'] = foit_opciones[key];
                            }
                        });

                        control_data = {foit_id:control.foit_id,
                                        foit_nombre: control.foit_nombre,
                                        foit_tipo: foit_tipo,
                                        indf_inventariable: (control.indf_genero)?(1):(0),
                                        indf_genero: (control.indf_genero)?(control.indf_genero):(""),
                                        indf_especie: (control.indf_especie)?(control.indf_especie):(""),
                                        indf_campo: (control.indf_campo)?(control.indf_campo):(""),
                                        indf_codigo: (control.indf_codigo)?(control.indf_codigo):(""),
                                        properties:properties,
                                        options:options,
                                        aggregated:foit_opciones.aggregated};

                        //console.log("Control"); console.log(control); console.log(control_data);
                        //console.log("foit_tipo"); console.log(foit_tipo);
                        //console.log("foit_opciones"); console.log(foit_opciones);

                        if(foit_opciones.aggregated){
                            controls_aggregated[control.foit_id] = control_data;
                        }
                        else{
                            if(foit_tipo=="AGGREGATOR"){
                                //console.log("agregador",foit_opciones,controls_aggregated)
                                LoadControl(p,foit_tipo,control_data,function(c){
                                    if(foit_opciones.controls){
                                        for(k=0;k<foit_opciones.controls.length;k++){
                                            if(controls_aggregated[foit_opciones.controls[k]]){
                                                cd = controls_aggregated[foit_opciones.controls[k]];

                                                LoadControl(c,cd.foit_tipo,cd);
                                            }
                                        }
                                    }
                                    //else{
                                    //    console.log(foit_opciones)
                                    //}
                                });
                            }
                            else{
                                LoadControl(p,foit_tipo,control_data);
                            }
                        }
                    }

                    dragula([p[0]], {
                      moves: function (el, source, handle, sibling) {
                        return $(el).hasClass("form-edicion-container-control");
                      }
                    });
                });
            }
        }
    }
    else{
        form.attr("data-properties",JSON.stringify(FORM_PROPERTIES));
        form.attr("data-options",JSON.stringify(FORM_OPTIONS));
    }

    //________________________________________________________________________________________________
    //Eventos
    $(document).off(".form-edicion");

    $(document).on('click.form-edicion','a.form-edicion-add-page',function(e){
        page_data = {fogr_id:new Date().getTime(),
                fogr_nombre:"",
				forg_orden:"",
                properties:PAGE_PROPERTIES,
                options:PAGE_OPTIONS,
                controls:CONTROLS};

        LoadPage(form,page_data,function(p){
            window.scrollToElement(p);
            dragula([p[0]], {
                      moves: function (el, source, handle, sibling) {
                        return $(el).hasClass("form-edicion-container-control");
                      }
                    });
        });
    });

    $(document).on('click.form-edicion','a.form-edicion-sort-pages',function(e){
        if($(this).hasClass("active")){
            if(pagesDrake){
                pagesDrake.destroy();
            }
            form.find("div.form-edicion-container-control,div.form-edicion-container-control-aggregator,a.form-edicion-add-page,a.form-edicion-properties,div.form-edicion-container-page div.menu").show();
            form.find("div.form-edicion-container-page div.title").removeClass("col-md-12").addClass("col-md-4");

            $(this).text("Ordenar páginas")
            $(this).removeClass("active")
        }
        else{
            pagesDrake = dragula([form[0]]);
            form.find("div.form-edicion-container-control,div.form-edicion-container-control-aggregator,a.form-edicion-add-page,a.form-edicion-properties,div.form-edicion-container-page div.menu").hide();
            form.find("div.form-edicion-container-page div.title").removeClass("col-md-4").addClass("col-md-12");
            $(this).text("Finalizar orden de páginas")
            $(this).addClass("active")
        }
    });

    $(document).on('click.form-edicion','a.form-edicion-add-control',function(e){
        console.log("a.form-edicion-add-control");
        elem 			= $(this);
        page_id 		= elem.data("id");
        control_type	= elem.data("type");
        control 		= {foit_id:new Date().getTime(),
                           foit_nombre:"",
                           properties:CONTROLS_PROPERTIES,
                           options:CONTROLS_OPTIONS[control_type.toLowerCase()],
                           aggregated:false
                            }
        if(control_type=="aggregator"){
            control.controls = CONTROLS_AGGREGATOR
        }
        console.log(control);
        LoadControl($("#"+page_id),control_type,control);
    });

    $(document).on('click.form-edicion','a.form-edicion-add-control-aggregator',function(e){
        console.log("a.form-edicion-add-control-aggregator");
        elem 			= $(this);
        page_id 		= elem.data("id");
        control_type	= elem.data("type");
        control 		= {foit_id:new Date().getTime(),
                           foit_nombre:"",
                           properties:CONTROLS_PROPERTIES,
                           options:CONTROLS_OPTIONS[control_type.toLowerCase()],
                           aggregated:true
                            }
        console.log(control);
        LoadControl($("#"+page_id),control_type,control);
    });

    $(document).on('click.form-edicion','a.form-edicion-remove',function(e){
        elem = $(this);
        id = elem.data("id");
        if(id.indexOf("page_")===0){
            msg = "¿Desea eliminar página? Se perderán todos los cambios realizados";
        }
        else{
            msg = "¿Desea eliminar control? Se perderán todos los cambios realizados";
        }
        confirm(msg,function(status){
            if(status){
                $("#"+id).remove();
            }
        });
        return false;
    });

    $(document).on('click.form-edicion','a.form-edicion-properties',function(e){
        elem = $(this);

        if(elem.is('[aria-describedby]')){ //popover opened
			console.log("IF CLICK FORM");
            id         = elem.data("id");
            popoverId  = elem.attr('aria-describedby')
            inputs     = $("#"+popoverId).find("input,select")
            properties = $("#"+id).data("properties");
            options    = $("#"+id).data("options")

            $.each(inputs, function(key) {
                input = $(inputs[key])
                name  = input.attr("name");
                type = input.data("type");
				value = (type=="array" || type=="ARRAY")?input.val().split(","):input.val();
				

                if(input.is(':checkbox')){
                    if(input.is(':checked')){
                        value=1;
                    }
                    else{
                        value=0;
                    }
                }

                //console.log(name,value)

                if(name in properties){
                    properties[name]['value'] = value;
                }
                else if(name in options){
                    options[name]['value'] = value;
					
                }
                else if(name.indexOf("indf-")===0){
                    $("#"+id).attr("data-"+name,value);
                }
            });

            UpdateOptions($("#"+id),properties,options);

            elem.popover('destroy')
        } else { //popover closed
			console.log("ELSE CLICK FORM");
            id   = elem.data("id");

            data = {
                "type":$("#"+id).attr("data-type"),
                "properties":JSON.parse($("#"+id).attr("data-properties")),
                "options":JSON.parse($("#"+id).attr("data-options")),
                "indf_inventariable":$("#"+id).attr("data-indf-inventariable"),
                "indf_genero":$("#"+id).attr("data-indf-genero"),
                "indf_especie":$("#"+id).attr("data-indf-especie"),
                "indf_campo":$("#"+id).attr("data-indf-campo"),
                "indf_codigo":$("#"+id).attr("data-indf-codigo")
            };

            //console.log("Propiedades para "+id,data)

            LoadOptions(data,function(html){
                elem.popover({html:true,
                              trigger:"manual",
                              title:"Propiedades",
                              content: html,
                              placement: "auto left"
                             });
                elem.popover("show");
                $('.selectpicker').selectpicker();
            });
        }

        return false;

    });

    $(document).on('click.form-edicion', '#BotonFormGuardar', function(e) {
            //console.log("#BotonFormGuardar");
            data  = {};
            form = $("#form");
            data.form               = Map(form.data("properties"));
            data.form.cont_id       = form.data("cont-id");
            data.form.form_opciones = Map(form.data("options"));
            data.form.pages         = [];

            form.find("[data-type='page']").each(function( index ) {
               page = Map($(this).data("properties"));
               page.fogr_opciones = Map($(this).data("options"));
               page.controls = [];

               $(this).find("div.form-edicion-container-control,div.form-edicion-container-control-aggregator").each(function( index ) {
                   control = Map($(this).data("properties"));
                   control.foit_tipo = $(this).data("type").toUpperCase();
                   control.foit_opciones = Map($(this).data("options"));
                   
                   if($(this).data("indf-inventariable")=="1"){
                       control.indf_inventariable = true;
                       control.indf_genero  = $(this).data("indf-genero");
                       control.indf_especie = $(this).data("indf-especie");
                       control.indf_campo   = $(this).data("indf-campo");
                       control.indf_codigo  = $(this).data("indf-codigo");
                   }
                   else{
                       control.indf_inventariable = false;
                   }

                   control.aggregated = ($(this).data("aggregated")==true)?true:false;
                   var aggregated = $(this).data("aggregated");                   
                   if(aggregated == true){                    
                        console.log("aggregated ");
                        console.log($(this).parent());
                    }

                   //console.log($(this));
                   //console.log(control);
                   page.controls.push(control);
               });
               /*
               if(page.controls.length==0){
                   alert("Debe al menos ingresar un control en cada página");
                   return;
               }
               */

               data.form.pages.push(page);
            });
           /*
           if(data.form.pages.length==0){
               alert("Debe al menos ingresar una página.");
               return;
           }
           */
            //console.log("antes de post");
            url = 'rest/core/formulario/add';
            //console.log(data);
            if(data.form.form_id!=""){
                    url = 'rest/core/formulario/update/'+data.form.form_id;
            }
            
            $.post(url,{form:JSON.stringify(data.form)},function(json) {
                if(json.status){
                    alert("Formulario guardado existosamente");

                    $( "#BotonFormCancelar" ).trigger( "click" );
                }
                else{
                    alert(json.error);
                }

            }).fail(function(xhr, textStatus, errorThrown){
                alert("Error "+xhr.status+": "+xhr.statusText);
            });

        });



    //________________________________________________________________________________________________
    //Utils
    function LoadPage(parent,data,callback){
        $.ajax({
          url: "templates/form_edicion_page.hb",
          async: false,
          success: function(src){
            template = Handlebars.compile(src);
            html     = template(data);
            var element  = $(html);
            var properties = element.data("properties");
            var options = element.data("options");
            //if(properties.fogr_id && properties.fogr_id.value>0) element.find("a.form-edicion-remove").text("Desactivar");
            if(properties.fogr_id && properties.fogr_id.value>0) element.find("a.form-edicion-remove").remove();
            UpdateOptions(element,properties,options);

            parent.append(element);

            if(callback){
                callback(element)
            }
          }
        });
    }

    function LoadControl(parent,control,data,callback){  
        if(data.foit_tipo=="AGGREGATOR"){
            data.controls = CONTROLS_AGGREGATOR;
        }
        $.ajax({
          url: "templates/form_edicion_control_"+control.toLowerCase()+".hb",
          async: false,
          success: function(src){
            template = Handlebars.compile(src);
            html     = template(data);

            var element  = $(html);
            var properties = element.data("properties");
            var options = element.data("options");
            //if(properties.foit_id && properties.foit_id.value>0) element.find("a.form-edicion-remove").text("Desactivar");
            if(properties.foit_id && properties.foit_id.value>0) element.find("a.form-edicion-remove").remove();

            UpdateOptions(element,properties,options);

            parent.append(element);
            $('.selectpicker').selectpicker();

            if(callback){
                callback(element)
            }
          },
        });
    }

    function LoadOptions(options,callback){
        $.ajax({
           url: "templates/form_edicion_propiedades.hb",
          async: false,
          success: function(src){
            template = Handlebars.compile(src);

            //console.log("options",options)

            html = template(options);
            if(callback){
                callback(html);
            }
          },
        });
    }

    function UpdateOptions(elem,properties,options){
        //console.log("UpdateOptions");
        //console.log(elem,properties,options)
        elem.attr("data-properties",JSON.stringify(properties));
        elem.attr("data-options",JSON.stringify(options));

        switch(elem.data("type")){
            case "form":{
                elem.find("div.title").first().text(properties.form_nombre.value);
                break;
            }
            case "page":{
                title = properties.fogr_nombre.value;
                if(properties.fogr_estado.value=="NOACTIVO"){
                    title += "<br><span class='text-danger'>NO ACTIVO</span>";
                    elem.find("div.title").addClass("noactive");
                }
                else{
                    elem.find("div.title").removeClass("noactive");
                }
                elem.find("div.title").first().html(title);
                break;
            }
            case "text":{
                title = properties.foit_nombre.value;
                if(properties.foit_requerido.value=="1"){
                    title += " (*)"
                }

                if(properties.foit_estado.value=="NOACTIVO"){
                    title += "<br><span class='text-danger'>NO ACTIVO</span>";
                    elem.find("div.form-group").addClass("noactive");
                }
                else{
                    elem.find("div.form-group").removeClass("noactive");
                }
                elem.find("label.control-label").first().html(title);
                break;
            }
            case "select":{
                title = properties.foit_nombre.value;

                if(properties.foit_requerido.value=="1"){
                    title += " (*)"
                }

                if(properties.foit_estado.value=="NOACTIVO"){
                    title += "<br><span class='text-danger'>NO ACTIVO</span>";
                    elem.find("div.form-group").addClass("noactive");
                }
                else{
                    elem.find("div.form-group").removeClass("noactive");
                }
                elem.find("label.control-label").first().html(title);

                var data_select = (typeof options.data.value === 'string' || options.data.value instanceof String)?options.data.value.split(","):options.data.value;
                if(data_select == undefined){
                    data_select = {};
                }
                selector_options = "";
                for(var i=0;i<data_select.length;i++){
                    selector_options += "<option>"+data_select[i]+"</option>";
                }

                selector = elem.find("select");

                //console.log(selector,selector_options)

                selector.html(selector_options);
                selector.selectpicker('refresh')
                break;
            }
            case "camera":{
                if(properties.foit_estado.value=="NOACTIVO"){
                    elem.find("div.form-group").addClass("noactive");
                }
                else{
                    elem.find("div.form-group").removeClass("noactive");
                }
                break;
            }
            case "save":{
                elem.find(".btn-danger").text(options.cancel_label.value);
                elem.find(".btn-success").text(options.save_label.value);


               if(properties.foit_estado.value=="NOACTIVO"){
                    elem.find("div.form-group").addClass("noactive");
                }
                else{
                    elem.find("div.form-group").removeClass("noactive");
                }
                break;
            }


        }

    }

    function Map(obj){
        //console.log("map",obj)
        res = {};
        $.each( obj, function( key, value ) {
            val = value.value;
            if(val==TRUE_VALUE){
                val = true;
            }
            else if(val==FALSE_VALUE){
                val = false;
            }

              res[key] = val;
        });
        return res;
    }

	function isNumberKey(evt){
			var charCode = (evt.which) ? evt.which : event.keyCode
			if (charCode > 31 && (charCode < 48 || charCode > 57))
				return false;
			return true;
	}
	
})(jQuery);

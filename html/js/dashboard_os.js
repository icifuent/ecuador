(function($) {
    $('.selectpicker').selectpicker();

    $('#mes').datepicker({
        format: "MM yyyy",
        viewMode: "months",
        minViewMode: "months",
        language: "es"
    }).on('changeDate', function(ev) {
        var month = ev.date.getMonth() + 1;
        var year = ev.date.getFullYear();
        $("#orse_fecha_mes").val(month);
        $("#orse_fecha_anio").val(year);
        //window.location = '#/dashboard/os/' + month + '/' + year;
    });
    
    $('#mes').datepicker('setDate', new Date());
    
    $('#orse_fecha_inicio, #orse_fecha_termino').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });
    
    $(document).on('click', '#BotonFiltrar', function (e) {
        //console.log('BotonFiltrar click');
        $("#siom-form-dashboard-os").submit();
    });
    
    $("#tipo_filtro").change(function(e) {
        var seleccion = $(this).val();
        if( seleccion == 'mes' ){
            $(".filtro_fecha").hide();
            $(".filtro_mes").show();
        }
        else{
            $(".filtro_mes").hide();
            $(".filtro_fecha").show();
        }
    });


})(jQuery);
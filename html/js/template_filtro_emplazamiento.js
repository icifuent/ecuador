(function($) { 
    var filterTimer=null;
    var filterTimeout=500;
    var filesCount = 0;

    $('select[name=empl_estado]').val("");
    $('select[name=empl_estado]').change(); 

    //inicializar componentes
    $('.selectpicker').selectpicker();   

    $('#siom-form-emplazamientos-filtro input[name=empl_nemonico]').keyup(function () {
         /*FiltrarConDelay();*/
    })
    $('#siom-form-emplazamientos-filtro input[name=empl_nombre]').keyup(function () {
         /*FiltrarConDelay();*/
    })
    $('#siom-form-emplazamientos-filtro input[name=empl_direccion]').keyup(function () {
         /*FiltrarConDelay();*/
    })
    $('#siom-form-emplazamientos-filtro select[name=tecn_id]').change(function(){
       /* $("#siom-form-emplazamientos-filtro").submit();*/
    })
    $('#siom-form-emplazamientos-filtro select[name=zona_id]').change(function(){
        /*$("#siom-form-emplazamientos-filtro").submit();*/
    })
    $('#siom-form-emplazamientos-filtro select[name=regi_id]').change(function(){
        /*$("#siom-form-emplazamientos-filtro").submit();*/
    })
    $('#siom-form-emplazamientos-filtro select[name=clus_id]').change(function(){
        /*$("#siom-form-emplazamientos-filtro").submit();*/
    })


    /*** Boton Limpiar ***/
    $(document).on('click', 'button#LimpiarInveEmplazamiento', function (e) {
        $('#nombre').val("");
        $('#nemonico').val("");

        $('select[name=empl_macrositio]').val("");
        $('select[name=empl_macrositio]').change();

        $('select[name=empl_subtel]').val("");
        $('select[name=empl_subtel]').change();
        
        $('select[name=empl_estado]').val("");
        $('select[name=empl_estado]').change();
        
        $('#empl_direccion').val("");

        $('select[name=tecn_id]').val("");
        $('select[name=tecn_id]').change();
        $('select[name=tecn_id]').selectpicker('refresh');

        $('#empl_direccion').val("");

        $('select[name=zona_id]').val("");
        $('select[name=zona_id]').change();

        $('select[name=regi_id]').val("");
        $('select[name=regi_id]').change();

        $('select[name=clus_id]').val("");
        $('select[name=clus_id]').change();
        
    });

    /*** Boton Buscar ***/
    $(document).on('click', 'button#BuscarInveEmplazamiento', function (e) {
        $("#siom-form-emplazamientos-filtro").submit();
        return true;
    });

        /*** Boton Limpiar ***/
    $(document).on('click', 'button#LimpiarInspCrear', function (e) {
        $('#nombre').val("");
        $('#nemonico').val("");

        $('select[name=empl_macrositio]').val("");
        $('select[name=empl_macrositio]').change();

        $('select[name=empl_subtel]').val("");
        $('select[name=empl_subtel]').change();
        
        $('select[name=empl_estado]').val("");
        $('select[name=empl_estado]').change();
        
        $('#empl_direccion').val("");

        $('select[name=tecn_id]').val("");
        $('select[name=tecn_id]').change();
        $('select[name=tecn_id]').selectpicker('refresh');

        $('#empl_direccion').val("");

        $('select[name=zona_id]').val("");
        $('select[name=zona_id]').change();

        $('select[name=regi_id]').val("");
        $('select[name=regi_id]').change();

        $('select[name=clus_id]').val("");
        $('select[name=clus_id]').change();
        
    });

    /*** Boton Buscar ***/
    $(document).on('click', 'button#BuscarInspCrear', function (e) {
        $("#siom-form-emplazamientos-filtro").submit();
        return true;
    });

    //funciones auxiliares
    function FiltrarConDelay(){
        if(filterTimer){
            clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
            $("#siom-form-emplazamientos-filtro").submit();
         },filterTimeout);
    }

 


   
})(jQuery);
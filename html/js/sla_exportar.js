(function($) {
    $('#BotonSLAExportar').click(function(event) {
        //console.log('#BotonSLAExportar click:');
        var data = {};
        $.each($("[id^='siom-form-sla-']").serializeArray(), function(i, field) {
            if (field.name == "orse_tipo") {
                if (!data["orse_tipo"])
                    data["orse_tipo"] = [];
                data["orse_tipo"].push(field.value);
            }
            else
                data[field.name] = field.value;
        });
        //console.log(data);
        delete data.zona_id;

        //Hacemos el POST
        event.stopImmediatePropagation();
		var that = $(this);
		var tipo = $(this).data('tipo');
		that.text("Descargando...");
			
		$.fileDownload("rest/contrato/"+window.contract+"/sla"+( (tipo=='MNT')?'/mnt':( (tipo=='INSP')?'/insp':'/os' ) )+"/exportar",{httpMethod:"POST",data:data,
			  prepareCallback:function(url) {
				  that.text("Descargando...");
			  },
			  successCallback: function(url) {
				  that.text("Exportar");
			  },
			  failCallback: function(responseHtml, url) {
				  that.text("Exportar");
				  alert(responseHtml);
			  }
		});
    })	
})(jQuery);

$(function() {

  contenedorMapa = $("#siom-os-detalle-mapa");
  setMap(contenedorMapa,contenedorMapa.data("latitud"),contenedorMapa.data("longitud"),contenedorMapa.data("distancia"));


  function setMap(element,lat,lng,geofence){
    if((typeof google != "undefined") && lat!="" && lng!=""){

      geofence_enabled = window.config['geofence_enabled'];
      if(geofence_enabled==null){
          geofence_enabled = false;
      }

      var gMap=null;
      var gMarker = null;
      var gMarkerGeofence = null;
      var latlng = new google.maps.LatLng(lat,lng);

      if(!gMap){
         var mapOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

          gMap = new google.maps.Map(element[0],mapOptions); 
          gMarker = new google.maps.Marker({map: gMap});  

          if(geofence_enabled){
            gMarkerGeofence = new google.maps.Circle({
              map: gMap,
              radius:0,
              strokeColor: '#AA0000',
              strokeOpacity: 0.8,
              strokeWeight: 1,
              fillColor: '#AA0000',
              fillOpacity: 0.35,
            });
          }
       }
        
       google.maps.event.trigger(gMap,'resize');
       gMarker.setPosition(latlng);

       if(gMarkerGeofence){
         if(geofence!="" && 0<geofence){
            gMarkerGeofence.setRadius(parseInt(geofence)); 
         }
         else{
            gMarkerGeofence.setRadius(0);
         }
         gMarkerGeofence.bindTo('center',gMarker, 'position');
       }
       gMap.setCenter(latlng);
    }
  }
});

(function($) { 

$('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/os/bandeja/filtro/'+page,data);
});

$('#ModalSolicitud').on('show.bs.modal', function (event) {
 	var modal = $(this)
  	var button = $(event.relatedTarget) 
  	var submit = modal.find('button.btn-primary');
  	var orse_id = button.data('orse-id')
  	var tipo    = button.data('tipo')
  	var title   = button.data('title')

  	modal.find('.modal-title').text(title)
  	modal.find("textarea").val("");

  	submit.data("orse-id",orse_id)
  	submit.data("tipo",tipo)
  	submit.data("button",button)
  	submit.button("reset");
  	submit.removeClass("btn-success");
})

$('#ModalSolicitud button.btn-primary').click(function(e){
	e.stopImmediatePropagation();

	var modal  = $('#ModalSolicitud');
	var submit = $(this);
	var button = $(submit.data("button"));
	var orse_id = submit.data('orse-id')
  	var tipo    = submit.data('tipo')
	var razon  = modal.find("textarea").val();

	if(razon==""){
		alert("Debe indicar una razon para la solicitud");
		return;
	}

	submit.button("loading");

	url = 'rest/contrato/'+window.contract+'/os/solicitud/'+tipo+'/'+orse_id
	$.post(url,{razon:razon},function(json,textStatus) {
		submit.button("reset");
		if(json.status){
			submit.button("complete");
			submit.addClass("btn-success");
			setTimeout(function(){
				if(tipo=="informe"){
					button.text("Solicitando ingreso de visita");
				}
				else if(tipo=="cambio"){
					button.text("Solicitando cambio"); 
				}
				button.prop('disabled',true);
				button.addClass("solicitando");
				button.parent().addClass("disabled");
				modal.modal('hide')	
			},1000);
		}	
		else{
			alert(json.error)
		}
	},'json').fail(function(){ 
		submit.button("reset");
		alert("No se pudo enviar solicitud");
	});

});

$("button#download").on("click",function(e){

	e.stopImmediatePropagation();
  	$btn = $(this);
  	$btn.button("loading");	
  	$.fileDownload("rest/core/repo/contrato/"+window.contract+"/os/bandeja",{httpMethod:"POST",data:$.param($btn.data("filters")),
	      prepareCallback:function(url) {
	          $btn.button("processing");
	      },
	      successCallback: function(url) {
	          $btn.button('reset')
	      },
	      failCallback: function(responseHtml, url) {
	          $btn.button('reset')
	          alert(responseHtml);
	      }
  	});
});

$("button#reporteSAP").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");
	  	$.fileDownload("rest/core/repo/contrato/"+window.contract+"/os/reporte/sap",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset')
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);
		      }
	  	});
});

$("a.siom-anular-os").on("click",function(e){
	e.stopImmediatePropagation();
	orse_id = $(this).data("orse-id");
	confirm("¿Desea anular OS N° "+orse_id+"?,<br>Esto cancelara las tareas asociadas a OS.",function(status){
		if(status){
			url = 'rest/contrato/'+window.contract+'/os/del/'+orse_id;
			$.post(url,{},function(json,textStatus) {
				if(json.status){
					setTimeout(function(){
						window.app.runRoute('post','#/os/bandeja/filtro/1',window.osBandejaFiltros);
					},200);
					alert("OS anulada exitosamente");
				}	
				else{
					alert(json.error)
				}
			},'json').fail(function(){ 
				alert("No se pudo enviar anulacion");
			});
		}

	})

	
});


})(jQuery);
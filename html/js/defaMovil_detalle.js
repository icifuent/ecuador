$(document).ready(function () {
    //contenedorMapa = $("#siom-os-detalle-mapa");
    //InitMap();
    //setMap(contenedorMapa, contenedorMapa.data("latitud"), contenedorMapa.data("longitud"), contenedorMapa.data("distancia"));
    InitMap();

    function InitMap() {
        var defa_lat = $('#siom-os-detalle-mapa').data('latitud');
        var defa_long = $('#siom-os-detalle-mapa').data('longitud');
        var emplazamientos = $('#siom-os-detalle-mapa').data('emplazamientos');

        var map = new google.maps.Map(document.getElementById('siom-os-detalle-mapa'), {
            zoom: 12,
            center: { lat: defa_lat, lng: defa_long }
        });

        var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
        var beachMarker = new google.maps.Marker({
            position: { lat: defa_lat, lng: defa_long },
            map: map,
            title: 'Denuncia Falla Reportada'
        });

        var beaches = [];
        for(var i = 0;i < emplazamientos.length; i++){
            var empl = [
                emplazamientos[i].empl_nombre, 
                emplazamientos[i].empl_direccion, 
                parseFloat(emplazamientos[i].empl_latitud), 
                parseFloat(emplazamientos[i].empl_longitud)
            ];
            beaches.push(empl);
            console.log(empl);
        }

        var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };
        
        for(var i = 0; i < beaches.length; i++){
            var beach =  beaches[i];
            var marker = new google.maps.Marker({
                position: {lat: beach[2], lng: beach[3]},
                map: map,
                shape: shape,
                icon: image,
                title: beach[0]               
            });
        }
    }

    $('#ModalSolicitudDerivar').on('show.bs.modal', function (event) {
        var modal = $(this)
        var button = $(event.relatedTarget)
        var submit = modal.find('button.btn-primary');
        var defa_id = button.data('defa-id')
        var tipo = button.data('tipo')
        var title = button.data('title')

        modal.find('.modal-title').text(title)
        //modal.find("textarea").val("");

        submit.data("defa-id", defa_id)
        submit.data("tipo", tipo)
        submit.data("button", button)
        submit.button("reset");
        submit.removeClass("btn-success");
    });

    var btnCrearOS = document.getElementById('btnCrearOS');
    var btnDerivar = document.getElementById('btnDerivar');
    var btnCancelar = document.getElementById('btnCancelar');
    var defaId = document.getElementById('defa_id').value;

    /*EVENTO CLICK DEL BOTON CANCELAR*/
    btnCancelar.addEventListener('click', function (e) {
        var modal = $('#ModalSolicitudCancelar');
        var submit = $(this);
        var button = $(submit.data('button'));
        var defa_id = $(submit.data('defa-id'));
        var tipo = submit.data('cancelar');
        var comentario = modal.find('#comentCancelar').val();

        if (comentario.length == 0 || comentario == null || /^\s+$/.test(comentario)) {
            alert("Debe indicar un comentario para cancelar");
            return false;
        } else {
            url = 'rest/defaMovil/cancelar/' + defaId;
            $.post(url, { comentario: comentario }, function (json) {
                submit.button("reset");
                if (1 == json.status) {
                    that.showSuccess("Reporte Falla cancelado", "Reporte Falla N° " + defaId + " cambió a estado CANCELADA", "#/defaMovil");
                    //alert("Reporte Falla N° " + defaId + " cambió a estado CANCELADA");
                    window.location.redirect("#/defaMovil");
                    $('#btnCancelarCan').click();
                    return true;
                } else {
                    that.showError("Error cancelando reporte falla", "Contacte a la mesa de ayuda SIOM", "#/defaMovil");
                    console.log(json.error + "ruta:" + url);
                    //alert(json.error);
                    return false;
                }
            }, 'json').fail(function () {
                alert("No se pudo cancelar reporte falla N° " + defaId);
                return false;
            });
        }
    });

    /*EVENTO CLICK DEL BOTON DERIVAR*/
    btnDerivar.addEventListener('click', function (e) {
        console.log("Listener");
        var modal = $('#ModalSolicitudDerivar');
        var submit = $(this);
        var button = $(submit.data("button"));
        var defa_id = $(submit.data('defa-id'));
        var tipo = submit.data('derivar');
        var contrato = modal.find("select").val();
        var comentario = modal.find('textarea').val();

        if (contrato == "") {
            alert("Debe indicar un contrato para derivar");
            return false;
        } else if (comentario.length == 0 || comentario == null || /^\s+$/.test(comentario)) {
            alert("Debe indicar un comentario para derivar");
            return;
        } else {
            submit.button("loading");

            url = 'rest/defaMovil/derivar/' + defaId + '/contrato/' + contrato
            $.post(url, { comentario: comentario }, function (json) {
                //submit.button("Derivando");
                if (1 == json.status) {
                    //alert("Reporte Falla N° " + defaId + " cambió a contrato " + contrato);
                    that.showSuccess("Reporte Falla derivado", "Reporte Falla N° " + defaId + " cambió a contrato " + contrato, "#/defaMovil/bandeja");
                    $('#btnDerivarCancelar').click();
                    return true;
                } else {
                    that.showError("Error derivando reporte falla", "Contacte a la mesa de ayuda SIOM", "#/defaMovil/bandeja");
                    console.log(json.error + "ruta:" + url);
                    //alert(json.error);
                    return false;
                }
            }, 'json').fail(function () {
                alert("No se pudo derivar reporte falla N° " + defaId);
                return false;
            });
        }
    });
    btnCrearOS.addEventListener('click', function (e) {
        var listaEmpl = document.getElementsByName('empl_id');
        var frmDefaMovil = document.getElementById('frmDefaMovil');
        var contador = 0;
        for (var i = 0; i < listaEmpl.length; i++) {
            if (listaEmpl[i].checked) {
                contador++;
            }
        }
        if (0 < contador) {
            $("#frmDefaMovil").submit();

        } else {
            alert("Debe elegir un emplazamiento");
        }
    });

    var defa_estado = $('#defa_estado').text();

    if (defa_estado != 'CREADA') {
        $('#divCrearOs').remove();
        $('#divDerivarOS').remove();
        $('#divCancelarOS').remove();
    }
});

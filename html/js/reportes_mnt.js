(function($) {
    console.log("INICIO JS");
    var url = '/rest/reportes_mnt';
    $.post(url, null, function(json) {
        if (json.status) { 
            if(json.archivos.length==0){
                html = '<div class="alert alert-notice" role="alert">';
                html+= 'No se han cargado archivos al contrato seleccionado';
                html+= '</div>';
                $("#ContratoDocumentosLista").html(html);
            }
            else{
                json.canEdit   = true;
                json.canDelete = true;
                $.ajax({
                      url: "templates/template_documentos_lista.hb",
                      async: false,
                      success: function(src){
                        template = Handlebars.compile(src);
                        html = template(json);
                        $("#ContratoDocumentosLista").html(html);
                      },
                    });
            }
        }
        else{
            alert(json.error);
        }
    });

    console.log("FIN JS");

})(jQuery);

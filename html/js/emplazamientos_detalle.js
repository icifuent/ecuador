
$(function() {

  $('.Btn-detalle').click(function(event){
   // console.log(this.id);
   // $('#emhi_'+this.id).css('display','block');
    if($('#emhi_'+this.id).data('open')==false){
      $('#emhi_'+this.id).css('display','block');
      $('#emhi_'+this.id).data('open',true);
      //console.log('btn_emhi_'+this.id);
      $('#span_'+this.id).text('ocultar');

    }else{
      $('#emhi_'+this.id).css('display','none');
      $('#emhi_'+this.id).data('open',false);
      $('#span_'+this.id).text('ver');

    }
    console.log($('#bla').data('datos'));
  });

  contenedorMapa = $("#siom-empl-detalle-mapa");
  setMap(contenedorMapa,contenedorMapa.data("latitud"),contenedorMapa.data("longitud"),contenedorMapa.data("distancia"));

  function setMap(element,lat,lng,geofence){
    if((typeof google != "undefined") && lat!="" && lng!=""){
      var gMap=null;
      var gMarker = null;
      var gMarkerGeofence = null;
      var latlng = new google.maps.LatLng(lat,lng);

      if(!gMap){
         var mapOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

          gMap = new google.maps.Map(element[0],mapOptions);
          gMarker = new google.maps.Marker({map: gMap});
          gMarkerGeofence = new google.maps.Circle({
            map: gMap,
            radius:0,
            strokeColor: '#AA0000',
            strokeOpacity: 0.8,
            strokeWeight: 1,
            fillColor: '#AA0000',
            fillOpacity: 0.35,
          });
       }

       google.maps.event.trigger(gMap,'resize');
       gMarker.setPosition(latlng);
       if(geofence!="" && 0<geofence){
          gMarkerGeofence.setRadius(parseInt(geofence));
       }
       else{
          gMarkerGeofence.setRadius(0);
       }
       gMarkerGeofence.bindTo('center',gMarker, 'pmntition');
       gMap.setCenter(latlng);
    }
  }
});

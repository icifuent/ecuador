(function($) {
		
    function drawCharts(){
        $('.siom-chart-columns-zonas').each(function(i, obj) {
            grafico_columns($(this),["Zona","Aprobadas", "Rechazadas", "No Realizadas", "Programadas", "Asignada Ezentis", "Asignada Movistar"]);
        });
    }

    function grafico_columns(element,labels,data_json){
        if(data_json==null){
            data_json = element.data("data");
        }
     
		data_array = [[labels[0],labels[1],{role:"annotation"},labels[2],{role:"annotation"},labels[3],{role:"annotation"},labels[4],{role:"annotation"},labels[5],{role:"annotation"},labels[6],{role:"annotation"}]];

        if(data_json.length==0){
          element.text("No hay datos");
        }
        else{
            for(i=0;i<data_json.length;i++){
                var aprobadas=parseInt(data_json[i]['aprobadas']);
                var rechazadas=parseInt(data_json[i]['rechazadas']);
                var contratista=parseInt(data_json[i]['contratista']);
                var movistar=parseInt(data_json[i]['movistar']);
                var programadas=parseInt(data_json[i]['programadas']);
                var norealizadas=parseInt(data_json[i]['norealizadas']);

                var textAprobadas='';
                var textRechazadas='';
                var textContratista='';
                var textMovistar='';
                var textProgramadas='';
                var textNoRealizadas='';

                if(0==aprobadas){
                textAprobadas=null;
                }else{
                textAprobadas=data_json[i]['aprobadas'];
                }

                if(0==rechazadas){
                	textRechazadas=null;
                }else{
                	textRechazadas= data_json[i]['rechazadas'];
                }

                if(0==contratista){
                	textContratista=null;
                }else{
                	textContratista=data_json[i]['contratista'];
                }

                if(0==movistar){
                	textMovistar=null;
                }else{
                	textMovistar=data_json[i]['movistar'];
                }

                if(0==programadas){
                	textProgramadas=null;
                }else{
                	textProgramadas=data_json[i]['programadas'];
                }

                if(0==norealizadas){
                	textNoRealizadas=null;
                }else{
                	textNoRealizadas=data_json[i]['norealizadas'];
                }

                console.log(parseInt(data_json[i]['norealizadas']));
                total = parseInt(data_json[i]['aprobadas']) + parseInt(data_json[i]['rechazadas']) + parseInt(data_json[i]['movistar']) + parseInt(data_json[i]['contratista']) + parseInt(data_json[i]['norealizadas']);
                data_array.push([data_json[i]['nombre'],
                Math.round(parseFloat((data_json[i]['aprobadas']/total)*100)*100)/100,
                textAprobadas,                 
                Math.round(parseFloat((data_json[i]['rechazadas']/total)*100)*100)/100,                               
                textRechazadas,  
                Math.round(parseFloat((data_json[i]['norealizadas']/total)*100)*100)/100,                               
                textNoRealizadas,   
                Math.round(parseFloat((data_json[i]['programadas']/total)*100)*100)/100,
                textProgramadas,
                Math.round(parseFloat((data_json[i]['contratista']/total)*100)*100)/100,
                textContratista,
                Math.round(parseFloat((data_json[i]['movistar']/total)*100)*100)/100,
                textMovistar]);
            } 
        
            var options2 = {
                title: '',
                titleTextStyle:{fontSize: 12},
		        width: 450, 
                /*height: 120,*/
                chartArea: {left:30,top:15,width:'95%',height:'80%'},
                hAxis : { textStyle : { fontSize: 7 }},
                vAxis: {minValue: 0, maxValue: 100, gridlines: { count: 11 }},
                legend:{position: 'none'/*'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}*/},
                bar: { groupWidth: '75%' },
                isStacked: true,
                seriesType: 'bars',
                series: {3: {type: 'line',lineWidth: 4 } },
                annotations: {
                    textStyle: {
                        fontSize: 8,
                    }
                },
                colors: ['#109618','#6c6b73', '#663300', '#000000',"#FF00FF", '#0000FF' ]		
            };

            var chart2 = new  google.visualization.ComboChart(element[0]);
            chart2.draw(google.visualization.arrayToDataTable(data_array), options2);
        }
    }

    /*
    function grafico_colums_line(id){

        chart_os_zona  = $(id);
        data_json      = chart_os_zona.data("data");
        data_array = [["Zona","Sin ejecutar",{role:"annotation"},"Ejecutadas",{role:"annotation"},"ejecuccion programada"]];

        if(data_json.length==0){
          chart_os_zona.text("No hay datos");
        }
        else{
          for(i=0;i<data_json.length;i++){
              total = parseInt(data_json[i]['noejecutadas']) + parseInt(data_json[i]['ejecutadas']);
              data_array.push([data_json[i]['zona_nombre'],
                               Math.round(parseFloat((data_json[i]['noejecutadas']/total)*100)*100)/100,
                               data_json[i]['noejecutadas'],
                               Math.round(parseFloat((data_json[i]['ejecutadas']/total)*100)*100)/100,
                               data_json[i]['ejecutadas'],
                               Math.round(parseFloat((data_json[i]['noejecutadas']/total)*100)*100)/100
                               
                               ])
          }

          var options2 = {
            title: '',
            titleTextStyle:{fontSize: 12},
            chartArea: {left:30,top:20,width:'90%',height:'70%'},
            vAxis: {maxValue: 100},
            legend:{position: 'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}},
            bar: { groupWidth: '75%' },
            isStacked: true,
            series: {0:{type: 'bars',color: '#dc3912'},
                     1:{type:'bars',color: '#109618'},
                     2:{type:'line',color: '#0000FF',displayAnnotations:false}
                    }
          };
       
          var chart2 = new  google.visualization.ComboChart(chart_os_zona[0]);
          chart2.draw(google.visualization.arrayToDataTable(data_array), options2);
        
        }
    }
    */

    if(typeof google.visualization != "undefined" && 
        typeof google.visualization.PieChart != "undefined" &&
        typeof google.visualization.ColumnChart != "undefined" &&
        typeof google.visualization.ComboChart != "undefined"){
        drawCharts();
    }
    else{
        google.setOnLoadCallback(drawCharts);
    }

})(jQuery);

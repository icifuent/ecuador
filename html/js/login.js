(function($) {

    $("#inputUsername").on('input', function()
    {
        $(this).val($(this).val().replace(/[^a-z0-9,._-]/gi, ''));
    });

    $("#inputUsername").attr('maxlength','20');
    $("#inputPassword").attr('maxlength','20');


    $('#btnSubmitLogin').click(function(){

         var googleResponse = jQuery('textarea[name="g-recaptcha-response"]').val();
         if (!googleResponse) {
             // alert("Marque la casilla de ReCaptcha");
             $('#lblRecaptchaError').text("Debe seleccionar la casilla de ReCaptcha");
             grecaptcha.reset();
             // $('<p style="color:red !important" class=error-captcha"><span class="glyphicon glyphicon-remove " ></span> Please fill up the captcha.</p>" ').insertAfter("#html_element");
              //grecaptcha.reset('#g-recaptcha-response');
             return false;
         } else {
         	return true;
         }

    });
    $(document).ready(function(){
      console.log("document ready");
      $('#g-recaptcha').html();
      $('#g-recaptcha').show();
    });


})(jQuery);

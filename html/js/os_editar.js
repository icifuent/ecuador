(function($) { 
	var filterTimer=null;
	var filterTimeout=500;
	var filesCount = 0;

    //inicializar componentes
    $('.selectpicker').selectpicker();
    $('#wizard').wizard();
    $('#orse_fecha_programada_fecha').datepicker({
                format: "dd-mm-yyyy",
                viewMode: "days", 
                minViewMode: "days",
                language: "es"
    });
    $('.timepicker').timepicker({showMeridian:false,defaultTime: false});
    $('.timepicker').click(function(){
        $(this).timepicker('showWidget');
    });     
    /*  
        Impide en tipo osi visualizar empresa contratista , solo se visualiza empresa Cliente 
        Felipe Azabache 19-07-2018
    */
    if ($('#tipo').val() == 'OSI') {
        $(".empresa option[empr_id !='1']").hide();
        $('.selectpicker').selectpicker('refresh');
    } else {
        $(".empresa option[empr_id !='1']").show();
        $('.selectpicker').selectpicker('refresh');
    }
   
    //inicializar eventos
    $(document).on('click','button.os-crear-selector',function(e){
    	empl_id = $(this).data("empl-id");
    	data    = $(this).data("data");
    	
    	$("#empl_id").val(empl_id);
    	$('#wizard').wizard("next");

    	$.ajax({
          url: "templates/empl_descripcion.hb",
          async: true,
          success: function(src){
            template = Handlebars.compile(src);
            $("#siom-empl-info").html(template(data));
          },
        });
    })

    $('#siom-form-os-crear-filtro input[name=empl_nemonico]').keyup(function () {
         FiltrarConDelay();
    })
    $('#siom-form-os-crear-filtro input[name=empl_nombre]').keyup(function () {
         FiltrarConDelay();
    })
    $('#siom-form-os-crear-filtro input[name=empl_direccion]').keyup(function () {
         FiltrarConDelay();
    })
    $('#siom-form-os-crear-filtro select[name=tecn_id]').change(function(){
		$("#siom-form-os-crear-filtro").submit();
	})
	$('#siom-form-os-crear-filtro select[name=zona_id]').change(function(){
		$("#siom-form-os-crear-filtro").submit();
	})
	$('#siom-form-os-crear-filtro select[name=regi_id]').change(function(){
		$("#siom-form-os-crear-filtro").submit();
	})
	$('#siom-form-os-crear-filtro select[name=clus_id]').change(function(){
		$("#siom-form-os-crear-filtro").submit();
	})



	$("#form_editar_os").on('change','.btn-file :file', function() {
	    var input = $(this);
	   	id     = input.data("id");
	   	label  = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        nextId = parseInt(id)+1;
        size   = input[0].files[0].size;
	   	
        if(size!=null && size > window.config.max_file_upload){
            alert("Tamaño de archivo supera maximo soportado");
            return;
        }


	   	var html = '<li class="list-group-item">';
	   	html+='<a class="btn btn-default btn-xs" data-id="'+id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	   	html+='<div>';
	   	html+='<div><textarea class="form-control" name="archivos_descripciones" rows="1"></textarea></div>';
	   	html+='<small>'+label+'</small>';
	   	html+='</div>';
	   	html+='</li>';
	    $(html).appendTo($("#listado-archivos"));

    	$('<input type="file" name="archivos[]" id="archivo-'+nextId+'" data-id="'+nextId+'">').insertAfter($(this));
	});
	$("#listado-archivos").on('click','a.btn', function(e) {
		var id = $(this).data("id");
		$("#archivo-"+id).remove();
		$(this).parent().remove();
	});


    $(".especialidad").change(function(e){
    	FiltrarSubespecialidad($(this).val());
        $('.subespecialidad').find('[id=primero]').remove();
        $("#subespecialidad").selectpicker('refresh');
    });
    $(".subespecialidad").click(function(){
        $('.subespecialidad').find('[id=primero]').remove();
       // console.log("asda");
       $("#subespecialidad").selectpicker('refresh');
    });
    $('#form_editar_os textarea[name=orse_descripcion]').keyup(function(e){
    	var text  = $(this).val();
    	var word_count = 0;
    	if(text!=""){
			word_count = text.trim().replace(/\s+/gi,' ').split(' ').length;
    	}
    	
		var palabras = $("#orse_descripcion_palabras");
		
		palabras.find("span").text(word_count);
		if(window.config.os.minDescription <= word_count){
			palabras.removeClass("text-danger").addClass("text-success");
		}
		else{
			palabras.removeClass("text-success").addClass("text-danger");
		}
	});

    $('#orse_fecha_solicitud_ahora').click( function(){
        disabled = false;
       if($(this).is(':checked')){
            disabled = true;
       }

       $('#fecha_programada').prop("disabled",disabled);
       $('.timepicker').prop("disabled",disabled);
    });


    $('#form_editar_os').submit(function() {
    	
       
            	$(this).find(':input[name="orse_fecha_solicitud"]').val(fecha_solicitud);
            	$(this).find(':input[name="orse_fecha_solicitud_fecha"]').attr("disabled",true);
            	$(this).find(':input[name="orse_fecha_solicitud_hora"]').attr("disabled",true);
                $(this).find(':input[name="orse_fecha_solicitud_ahora"]').attr("disabled",true);
            	$(this).find(':input[name="espe_id"]').attr("disabled",true);
            	//$(this).find(':input[name="hour"]').attr("disabled",true);
            	//$(this).find(':input[name="minute"]').attr("disabled",true);
                $(this).find(':input[name="orse_estado"]').attr("disabled",true);

                $("#submit_editar_os").button('loading');
                return true;
      
    });
    
    
    $("#tipo").on("change", function(e) {
        console.log("#tipo change"); 
        if($(this).val()=='OSI'){      
            $("#empresa option[data-coem_tipo!='CLIENTE']").prop('disabled', true);
            $("#empresa").val('');
            $('.selectpicker').selectpicker('refresh');
        }
        else{
            $("#empresa option[data-coem_tipo!='CLIENTE']").prop('disabled', false);
            $('.selectpicker').selectpicker('refresh');
        }        
    });

    //funciones auxiliares
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-os-crear-filtro").submit();
         },filterTimeout);
	}
    

    function FiltrarSubespecialidad(espe_id){
    	$("#subespecialidad option").attr("disabled",true);
    	$("#subespecialidad option[data-espe-id='"+espe_id+"']").attr("disabled",false);
    	$("#subespecialidad option[data-default='true']").attr("disabled",false);
    	$("#subespecialidad").selectpicker('refresh');
    }

    function FormatDatetime(fecha){
    	fecha = fecha.split("-");
    	return fecha[2]+"-"+fecha[1]+"-"+fecha[0];
    }
/*    function DatetimeNow(){
        now = new Date();
        var y  = now.getFullYear();
        var m  = (now.getMonth()  > 8)?(now.getMonth()+1):('0'+(now.getMonth()+1));
        var d  = (now.getDate()   > 9)?(now.getDate()    ):('0'+now.getDate());

        var H  = (now.getHours()  > 9)?(now.getHours()   ):('0'+now.getHours());
        var M  = (now.getMinutes()> 9)?(now.getMinutes() ):('0'+now.getMinutes());
        var S  = (now.getSeconds()> 9)?(now.getSeconds() ):('0'+now.getSeconds());

        return y.toString()+"-"+m.toString()+"-"+d.toString()+" "+H.toString()+":"+M.toString()+":"+S.toString();
    }
*/
    function IsDatetimeInFuture(datetime){
    	fecha     = datetime.split("-");

    	now = new Date();
    	dt  = new Date(fecha[0],parseInt(fecha[1])-1,fecha[2],23,59,0,0);
    	return (now.getTime() < dt.getTime());
    }
    function SeleccionarSubEspecialidad(valor){


        $('.selectpicker').selectpicker('val', [valor]);      
        $('select[name=mant_estado]').change();
        $('select[name=mant_estado]').selectpicker('refresh');
    }

    function CargaInicialDatos(){
        /*ESPECIALIDAD*/
        dataEspecialidad=$(".especialidad").data();

        $(".especialidad").selectpicker('val', dataEspecialidad.valueSelect);
        $(".especialidad").change();
        $(".especialidad").selectpicker('refresh');
        FiltrarSubespecialidad($(".especialidad").val());

        /*SUBESPECIALIDAD*/
        dataSubespecialidad=$(".subespecialidad").data();
        $(".subespecialidad").selectpicker('val', dataSubespecialidad.valueSelect);
        $(".subespecialidad").change();
        $(".subespecialidad").selectpicker('refresh');

        
        /*EMPRESA */
        dataSubespecialidad=$(".empresa").data();
        $(".empresa").selectpicker('val', dataSubespecialidad.valueSelect);
        $(".empresa").change();
        $(".empresa").selectpicker('refresh');

       

    }
    
    //FiltrarSubespecialidad($(".especialidad").val());
    CargaInicialDatos();
    console.log("CARGA INICIAL");

})(jQuery);
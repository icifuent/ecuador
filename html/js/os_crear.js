$(document).ready(function () {
	var filterTimer = null;
	var filterTimeout = 500;
	var filesCount = 0;

	//inicializar componentes
	$('.selectpicker').selectpicker();
	$('#wizard').wizard();
	$('#fecha_solicitud').datepicker({
		format: "dd-mm-yyyy",
		viewMode: "days",
		minViewMode: "days",
		language: "es"
	});
	$('.timepicker').timepicker({ showMeridian: false, defaultTime: false });
	$('.timepicker').click(function () {
		$(this).timepicker('showWidget');
	})

	//inicializar eventos
	$(document).on('click', 'button.os-crear-selector', function (e) {
		empl_id = $(this).data("empl-id");
		data = $(this).data("data");

		$("#empl_id").val(empl_id);
		$('#wizard').wizard("next");

		$.ajax({
			url: "templates/empl_descripcion.hb",
			async: true,
			success: function (src) {
				template = Handlebars.compile(src);
				$("#siom-empl-info").html(template(data));
			},
		});


	})

	$('#siom-form-os-crear-filtro input[name=empl_nemonico]').keyup(function () {
		/*FiltrarConDelay();*/
	})
	$('#siom-form-os-crear-filtro input[name=empl_nombre]').keyup(function () {
		/* FiltrarConDelay();*/
	})
	$('#siom-form-os-crear-filtro input[name=empl_direccion]').keyup(function () {
		/* FiltrarConDelay();*/
	})
	$('#siom-form-os-crear-filtro select[name=tecn_id]').change(function () {
		/*$("#siom-form-os-crear-filtro").submit();*/
	})
	$('#siom-form-os-crear-filtro select[name=zona_id]').change(function () {
		/*$("#siom-form-os-crear-filtro").submit();*/
	})
	$('#siom-form-os-crear-filtro select[name=regi_id]').change(function () {
		/*$("#siom-form-os-crear-filtro").submit();*/
	})
	$('#siom-form-os-crear-filtro select[name=clus_id]').change(function () {
		/*$("#siom-form-os-crear-filtro").submit();*/
	})


	$("#form_crear").on('change', '.btn-file :file', function () {
		var input = $(this),
			id = input.data("id");
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
		nextId = parseInt(id) + 1;
		size = input[0].files[0].size;

		if (size != null && size > window.config.max_file_upload) {
			alert("Tamaño de archivo supera maximo soportado");
			return;
		}


		var html = '<li class="list-group-item">';
		html += '<a class="btn btn-default btn-xs" data-id="' + id + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
		html += '<div>';
		html += '<div><textarea class="form-control" name="archivos_descripciones" rows="1"></textarea></div>';
		html += '<small>' + label + '</small>';
		html += '</div>';
		html += '</li>';
		$(html).appendTo($("#listado-archivos"));

		$('<input type="file" name="archivos[]" id="archivo-' + nextId + '" data-id="' + nextId + '">').insertAfter($(this));
	});
	$("#listado-archivos").on('click', 'a.btn', function (e) {
		var id = $(this).data("id");
		$("#archivo-" + id).remove();
		$(this).parent().remove();
	});


	$("#especialidad").change(function (e) {
		FiltrarSubespecialidad($(this).val())
		FiltrarFormularios($(this).val())
	});

	$('#form_crear textarea[name=orse_descripcion]').keyup(function (e) {
		var text = $(this).val();
		var word_count = 0;
		if (text != "") {
			word_count = text.trim().replace(/\s+/gi, ' ').split(' ').length;
		}

		var palabras = $("#orse_descripcion_palabras");

		palabras.find("span").text(word_count);
		if (window.config.os.minDescription <= word_count) {
			palabras.removeClass("text-danger").addClass("text-success");
		}
		else {
			palabras.removeClass("text-success").addClass("text-danger");
		}
	})

	$('#orse_fecha_solicitud_ahora').click(function () {
		disabled = false;
		if ($(this).is(':checked')) {
			disabled = true;
		}

		$('#fecha_solicitud').prop("disabled", disabled);
		$('.timepicker').prop("disabled", disabled);
	});


	$('#form_crear').submit(function () {
		fecha = $(this).find(':input[name="orse_fecha_solicitud_fecha"]').val();
		if (fecha == "") {
			alert("Debe ingresar fecha de solicitud");
			return false;
		}

		hora = $(this).find(':input[name="orse_fecha_solicitud_hora"]').val();
		if (hora == "") {
			alert("Debe ingresar hora de solicitud");
			return false;
		}

		tipo = $(this).find(':input[name="orse_tipo"]').val();
		if (tipo == "") {
			alert("Debe ingresar tipo de orden de servicio");
			return false;
		}

		empr_id = $(this).find(':input[name="empr_id"]').val();
		if (empr_id == "") {
			alert("Debe ingresar empresa");
			return false;
		}

		espe_id = $(this).find(':input[name="espe_id"]').val();
		if (espe_id == "") {
			alert("Debe ingresar especialidad");
			return false;
		}

		sube_id = $(this).find(':input[name="sube_id"]').val();
		sube_count = $(this).find(':input[name="sube_id"] option:enabled').length;

		if (1 < sube_count && sube_id == "") {
			alert("Debe ingresar alarma");
			return false;
		}

		indisponibilidad = $(this).find(':input[name="orse_indisponibilidad"]').val();
		if (indisponibilidad == "") {
			alert("Debe ingresar indisponibilidad");
			return false;
		}

		descripcion = $(this).find(':input[name="orse_descripcion"]').val();
		if (descripcion == "") {
			alert("Debe ingresar una descripcion");
			return false;
		}

		word_count = descripcion.trim().replace(/\s+/gi, ' ').split(' ').length;
		if (word_count < window.config.os.minDescription) {
			alert("Debe ingresar al menos " + window.config.os.minDescription + " palabras en descripcion");
			return false;
		}

		if ($(this).find(':input[name="orse_fecha_solicitud_ahora"]').is(':checked')) {
			fecha_solicitud = DatetimeNow();
		}
		else {
			fecha_solicitud = FormatDatetime(fecha, hora);
			if (!IsDatetimeInFuture(fecha_solicitud)) {
				alert("Debe indicar una fecha programada de atencion mayor a la fecha y hora actuales");
				return false;
			}
		}

		id_incidencia = $(this).find(':input[name="orse_tag"]').val();
		if (id_incidencia == "") {
			alert("Debe ingresar un codigo incidencia");
			return false;
		}

		archivos_descripciones = $(this).find(':input[name="archivos_descripciones"]');
		for (i = 0; i < archivos_descripciones.length; i++) {
			t = archivos_descripciones[i];
			if ($(t).val() == "") {
				alert("Debe ingresar una descripcion para cada archivo");
				return false;
			}
		}

		formulario_seleccionado = $(this).find(':input[name="form_id"]').val();
		if (!formulario_seleccionado) {
			alert("Debe seleccionar al menos un formulario");
			return false;
		}

		$(this).find(':input[name="orse_fecha_solicitud"]').val(fecha_solicitud);
		$(this).find(':input[name="orse_fecha_solicitud_fecha"]').attr("disabled", true);
		$(this).find(':input[name="orse_fecha_solicitud_hora"]').attr("disabled", true);
		$(this).find(':input[name="orse_fecha_solicitud_ahora"]').attr("disabled", true);
		$(this).find(':input[name="espe_id"]').attr("disabled", true);
		$(this).find(':input[name="hour"]').attr("disabled", true);
		$(this).find(':input[name="minute"]').attr("disabled", true);

		$("#submit_crear").button('loading');
		return true;
	});
	$("#formulario").on("change", function (e) {
		console.log($(this).val());
		console.log($("#tipo").val());
	});

	$("#tipo").on("change", function (e) {
		console.log("#tipo change");
		/*oculta la empresa ezentis  cuando es tipo OSI   ya que debe ser empresa telefonica Felipe Azabache
			19-07-2018*/
		if ($(this).val() == 'OSI') {
			$("#empresa option").each(function () {
				//$("#empresa option[data-coem_tipo!='CLIENTE']").prop('disabled', false);
				if ($(this).text() == 'EZENTIS') {
					$(this).hide();
				}
			});
			$("#empresa").val('1');
			$('.selectpicker').selectpicker('refresh');
		} else {
			$("#empresa option").each(function () {
				if ($(this).text() == 'EZENTIS') {
					$(this).show();
					$("#empresa").val('2');
					$('.selectpicker').selectpicker('refresh');
				}
			});

		}
	});

	/*** Boton Limpiar ***/
	$(document).on('click', 'button#LimpiarOsCrear', function (e) {
		$('#nombre').val("");
		$('#nemonico').val("");

		$('select[name=tecn_id]').val("");
		$('select[name=tecn_id]').change();
		$('select[name=tecn_id]').selectpicker('refresh');

		$('#empl_direccion').val("");

		$('select[name=zona_id]').val("");
		$('select[name=zona_id]').change();

		$('select[name=regi_id]').val("");
		$('select[name=regi_id]').change();

		$('select[name=clus_id]').val("");
		$('select[name=clus_id]').change();

	});

	/*** Boton Buscar ***/
	$(document).on('click', 'button#BuscarOsCrear', function (e) {
		$("#siom-form-os-crear-filtro").submit();
		return true;
	});

	function FiltrarFormularios(espe_id) {
		$("#formulario option").attr("disabled",true);
        $("#formulario option").removeAttr('selected');
        
        $("#formulario option[data-espe-id='"+espe_id+"']").attr("disabled",false);
        $("#formulario option[data-espe-id='"+espe_id+"'][data-refe-seleccion='1']").attr('selected', 'selected');
        $("#formulario option[data-default='true']").attr("disabled",false);

        $("#formulario").selectpicker('refresh');


	}
	//funciones auxiliares
	function FiltrarConDelay() {
		if (filterTimer) {
			clearTimeout(filterTimer);
		}
		filterTimer = setTimeout(function () {
			$("#siom-form-os-crear-filtro").submit();
		}, filterTimeout);
	}

	function FiltrarSubespecialidad(espe_id) {
		$("#subespecialidad option").attr("disabled", true);
		$("#subespecialidad option[data-espe-id='" + espe_id + "']").attr("disabled", false);
		$("#subespecialidad option[data-default='true']").attr("disabled", false);
		$("#subespecialidad").selectpicker('refresh');
	}

	function FormatDatetime(fecha, hora) {
		fecha = fecha.split("-");
		return fecha[2] + "-" + fecha[1] + "-" + fecha[0] + " " + hora;
	}

	function DatetimeNow() {
		now = new Date();
		var y = now.getFullYear();
		var m = (now.getMonth() > 8) ? (now.getMonth() + 1) : ('0' + (now.getMonth() + 1));
		var d = (now.getDate() > 9) ? (now.getDate()) : ('0' + now.getDate());

		var H = (now.getHours() > 9) ? (now.getHours()) : ('0' + now.getHours());
		var M = (now.getMinutes() > 9) ? (now.getMinutes()) : ('0' + now.getMinutes());
		var S = (now.getSeconds() > 9) ? (now.getSeconds()) : ('0' + now.getSeconds());

		return y.toString() + "-" + m.toString() + "-" + d.toString() + " " + H.toString() + ":" + M.toString() + ":" + S.toString();
	}

	function IsDatetimeInFuture(datetime) {
		fechahora = datetime.split(" ");
		fecha = fechahora[0].split("-");
		hora = fechahora[1].split(":");

		now = new Date();
		now.setHours(now.getHours(), now.getMinutes(), 0, 0);

		dt = new Date(fecha[0], parseInt(fecha[1]) - 1, fecha[2], hora[0], hora[1], 0, 0);
		return (now.getTime() <= dt.getTime());
	}
	$("#tipo").change();
	FiltrarSubespecialidad($("#especialidad").val())
	FiltrarFormularios($("#especialidad").val())
});
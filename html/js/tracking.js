
(function($) { 
	var map          = null;
	var lastPosition = [];
	var animInterval = null;
	var users        = [];
	var markers      = [];


	//inicializar componentes
    $('.selectpicker').selectpicker();
/*    $('#fecha_inicio,#fecha_fin').datepicker({
	    format: "dd-mm-yyyy",
	    viewMode: "days",
	    minViewMode: "days",
	    language: "es"
	}).on("changeDate", function(e) {
        $("#siom-form-tracking-filtro").submit();
    });
*/
	$('#fecha_inicio,#fecha_fin').datepicker({
	    format: "dd-mm-yyyy",
	    viewMode: "days",
	    minViewMode: "days",
	    language: "es"
	});

	//Eventos
	$(window).resize(function() {
	  resizeMap()
	});

    $('#siom-form-tracking-filtro select[name=ustr_usuario]').change(function(){
      /*  $("#siom-form-tracking-filtro").submit(); */
    })
    $('#siom-form-tracking-filtro select[name=mostrar_os]').change(function(){
      /*  $("#siom-form-tracking-filtro").submit(); */
    })
    $('#siom-form-tracking-filtro select[name=mostrar_mnt]').change(function(){
      /*  $("#siom-form-tracking-filtro").submit(); */
    })

    $(document).on('click','span.tracking-action-view',function(){
		i = $(this).data("id");

		if(users[i]){
			visible = true;
			if(users[i].poly.getVisible()){
				visible = false;
			}

			users[i].poly.setVisible(visible);
			for(j=0;j<users[i].markers.length;j++){
				users[i].markers[j].setVisible(visible);
			}

			$(this).toggleClass('active');
		}
	});


	$(document).on('click','span.tracking-action-time',function(){
		i = $(this).data("id");

		if(users[i]){
			visible = true;
			if($(this).hasClass('active')){
				visible = false;
			}

			for(j=0;j<users[i].markers.length;j++){
				google.maps.event.trigger(users[i].markers[j], 'click');
			}
			$(this).toggleClass('active');
		}
	});


	/** Funcion Limpiar Filtros **/
	$(document).on('click','button#LimpiarTracking', function(){
		/*if(window.trackingFiltros){
                dt = window.trackingFiltros.ustr_fecha_fin.split("-");
                if(parseInt(dt[0])<=31){
                    console.log("");
                    window.trackingFiltros.ustr_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    console.log("");
                    window.trackingFiltros.ustr_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
                }

                //window.trackingFiltros.ustr_fecha_fin     = dt[2]+"-"+dt[1]+"-"+dt[0];

                dt = window.trackingFiltros.ustr_fecha_inicio.split("-");
                if(parseInt(dt[0])<=31){
                    window.trackingFiltros.ustr_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    window.trackingFiltros.ustr_fecha_inicio= dt[2]+"-"+dt[1]+"-"+dt[0];
                }
        } else{*/
                window.trackingFiltros = {}
                now = new Date();
                console.log(now)
                d   = now.getDate();
                m   = now.getMonth()+1;
                y   = now.getFullYear();

				if(d < 10){
					d = "0"+d;
				}
				if(m < 10){
					m = "0"+m;
				}
                window.trackingFiltros.ustr_fecha_fin    = d+"-"+m+"-"+y;
                window.trackingFiltros.ustr_fecha_inicio = d+"-"+m+"-"+y;
          //  }
        $("#fecha_inicio").val(window.trackingFiltros.ustr_fecha_inicio);
        $("#fecha_fin").val(window.trackingFiltros.ustr_fecha_fin);
        $('select[name=ustr_usuario]').val("0");
		$('select[name=ustr_usuario]').change();
	});


	/** Funcion Buscar **/
	$(document).on('click','button#BuscarTracking', function(){
		var usu_seleccionado=$('#siom-form-tracking-filtro select[name=ustr_usuario]').val();
		var fecha_ini=$('#fecha_inicio').val();
		var fecha_f=$('#fecha_fin').val();

		var fecha_ini_format=parseDate(fecha_ini);
		var fecha_fin_format=parseDate(fecha_f);

		var diasDif=daydiff(fecha_ini_format,fecha_fin_format);

		if (60 < diasDif) {
			alert("No puede haber una diferencia mayor 60 dias entre ambas fechas");
			return false;
		}

		if(usu_seleccionado == "0"){
			alert("Debe seleccionar un Usuario");
			return false;
		}
		$("#siom-form-tracking-filtro").submit();
		return true;
	});


    //utils
    function resizeMap(){
		mapContainer = $('#tracking-map')
		wh  		 = $(window).height();
		mt           = mapContainer.offset().top;
		mapContainer.height(wh - mt - 50);
	}

	window.updateTracking = function(json){
		if(json.status){
			var info      = {};
			var locations = {};	
			totalUsers    = 0;

			for(i=0;i<json.data.length;i++){
				user_id = json.data[i].user_id;

				if(!locations[user_id]){
					locations[user_id] = [];
					info[user_id] = [];
					totalUsers++;
				}

				lat = parseFloat(json.data[i].usta_latitude.replace(',','.'));
				lng = parseFloat(json.data[i].usta_longitude.replace(',','.'));

				locations[user_id].push(new google.maps.LatLng(lat,lng));

				info[user_id].push(json.data[i]);
			}


			if(locations != {}){
				refresh = true;
				if(map==null){
					var mapOptions = {
					  zoom: 12
					}
					map = new google.maps.Map($("#tracking-map")[0], mapOptions);
					refresh = false;
				}
				else{
					for(i=0;i<users.length;i++){
						users[i].poly.setMap(null);
					}
					for (var i = 0; i < markers.length; i++) {
					    markers[i].setMap(null);
					 }
				}

				users 	   = [];
				var bounds = new google.maps.LatLngBounds();
				var colors = getColors(totalUsers);
				var center = null;
				
				u = 0;
				for (var user_id in locations) {
				  if (locations.hasOwnProperty(user_id)) {
				    ul = locations[user_id];
				    ui = info[user_id];
				    color= colors[u++];

				    if(center==null){
				    	center = ul[0];
				    }
				    
				    var poly = new google.maps.Polyline({
					    strokeColor: color,
					    strokeOpacity: 1,
					    strokeWeight: 2,
					    map: map,
					    icons: [{
                          icon: {
                                  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                                  strokeColor:color,
                                  fillColor:color,
                                  fillOpacity:1
                                },
                          repeat:'100px',
                          path:[]
                       }]
					  });

				    var path = poly.getPath();
				    var users_markers = [];

				    for(i=0;i<ul.length;i++){
						var marker = new google.maps.Marker({
						  position: ul[i],
						  icon: {
						    path: google.maps.SymbolPath.CIRCLE,
						    fillOpacity: (i==ul.length-1)?(1):(0.7),
						    fillColor: color,
						    strokeOpacity: 1.0,
						    strokeColor: color,
						    strokeWeight: 3.0, 
						    scale: 8 //pixels
						  },
					      title: ui[i].usta_time,
					      map:map
						});
						

					    path.push(ul[i]);
					    bounds.extend(ul[i]);
					    attachMarkerInfoWindow(marker,"<div class='tracking-marker'><div class='tracking-name'>"+ui[i].user_name+"</div><div class='tracking-time'>"+ui[i].usta_datetime+"</div></div>");

					    markers.push(marker)
					    users_markers.push(marker)
					}
					lastPosition.push(marker);
					
					users.push({name:ui[0].user_name,color:color,path:path,poly:poly,markers:users_markers});
				  }
				}

				if(!refresh && map!=null){
					map.setCenter(center);
					map.fitBounds(bounds);
				}


				if(0<users.length){
					$("#tracking-legend").show();
					$("#tracking-legend").empty();

					for(i=0;i<users.length;i++){
						html = "<div class='tracking-item'><div style='background-color:"+users[i].color+";' class='tracking-circle'></div>";
						
						html+= "<span class='tracking-action-view glyphicon glyphicon-eye-open active' data-id='"+i+"'></span>"
						html+= "<span class='tracking-action-time glyphicon glyphicon-time' data-id='"+i+"'></span>";
						
						html+= "<div class='tracking-name'>"+users[i].name+"</div></div>";
						$("#tracking-legend").append(html);


						//runSnapToRoad(users[i].poly,users[i].path,users[i].color)
					}
				}
				else{
					$("#tracking-legend").hide();
					$("#tracking-map").html("<div class='tracking-nodata'>El usuario seleccionado no posee datos de tracking entre las fechas seleccionadas</div>");
					map = null;
				}


				if(animInterval){
					clearInterval(animInterval);
				}
				if(0<lastPosition.length){
					for(i=0;i<lastPosition.length;i++){
						google.maps.event.trigger(lastPosition[i], 'click');
					}


					animInterval = setInterval(function() {
						for(i=0;i<lastPosition.length;i++){
							circle = lastPosition[i]; 
				            circle.setVisible(!circle.getVisible());
				         }
			        }, 500);
				}
				
				$('#tracking-map').show();
				return;
			}
		}
		else{
			alert(json.error)
		}

	}

	function attachMarkerInfoWindow(marker, info) {
	  	var infowindow = new google.maps.InfoWindow({
	    	content: info
	  	});
	  	google.maps.event.addListener(marker, 'click', function() {
	  		if(infowindow.getMap()){
	  			infowindow.close();
	  		}
	  		else{
	  			infowindow.open(marker.get('map'), marker);
	  		}
	  	});
	}

	function getColors(num_colors){
	    colors=[]
	    for(h = 1; h < 360; h += 360 / num_colors) {
	    	colors.push("hsl("+h+", 100%, 50%)")
	    }
	    return colors;
	}

	function parseDate(str) {
   		 var mdy = str.split('-');
    		return new Date(mdy[2], mdy[1]-1, mdy[0]);
	}

	function daydiff(first, second) {
   		 return Math.round((second-first)/(1000*60*60*24));
	}

	resizeMap();
})(jQuery);




/*
var apiKey       = "AIzaSyCKfOTzEL-zvcyryKcz_BdZWEIhWPWppjU"; //<= snapRoad
var map          = null;
var dtp          = $('#dt').datepicker().data('datepicker');
var lastPosition = [];
var animInterval = null;
var users        = [];
var markers      = [];


$('#update').click(function(e) {
    e.preventDefault();
    updateMap(true);
});

$('#user_id').change(function(e) {
    e.preventDefault();
    updateMap(false);
});

$('#dt').datepicker().on("changeDate", function(e) {
    updateMap(false);
});

$( window ).resize(function() {
  resizeMap()
});

$(document).on('click','span.tracking-action-view',function(){
	i = $(this).data("id");

	if(users[i]){
		visible = true;
		if(users[i].poly.getVisible()){
			visible = false;
		}

		users[i].poly.setVisible(visible);
		for(j=0;j<users[i].markers.length;j++){
			users[i].markers[j].setVisible(visible);
		}


		$(this).toggleClass('active');
	}
});


$(document).on('click','span.tracking-action-time',function(){
	i = $(this).data("id");

	if(users[i]){
		visible = true;
		if($(this).hasClass('active')){
			visible = false;
		}

		for(j=0;j<users[i].markers.length;j++){
			google.maps.event.trigger(users[i].markers[j], 'click');
		}
		$(this).toggleClass('active');
	}
});

 
var updateMap = function(refresh){
	user_id = $("#user_id").val();
	date    = convertDate(dtp.getDate());
	 
	$.get('api/?q=GetUserTrackings',{user_id:user_id,date:date},function(json) {		
		
		if(json.status){
			var info      = {};
			var locations = {};	
			totalUsers    = 0;

			for(i=0;i<json.data.length;i++){
				user_id = json.data[i].user_id;

				if(!locations[user_id]){
					locations[user_id] = [];
					info[user_id] = [];
					totalUsers++;
				}

				lat = parseFloat(json.data[i].usta_latitude.replace(',','.'));
				lng = parseFloat(json.data[i].usta_longitude.replace(',','.'));

				console.log(lat,lng)
				locations[user_id].push(new google.maps.LatLng(lat,lng));

				info[user_id].push(json.data[i]);
			}


			if(locations != {}){
				if(map==null){
					var mapOptions = {
					  zoom: 12
					}
					map = new google.maps.Map($("#tracking-map")[0], mapOptions);
				}
				else{
					for(i=0;i<users.length;i++){
						users[i].poly.setMap(null);
					}
					for (var i = 0; i < markers.length; i++) {
					    markers[i].setMap(null);
					 }
				}

				users 	   = [];
				var bounds = new google.maps.LatLngBounds();
				var colors = getColors(totalUsers);
				var center = null;
				
				u = 0;
				for (var user_id in locations) {
				  if (locations.hasOwnProperty(user_id)) {
				    ul = locations[user_id];
				    ui = info[user_id];
				    color= colors[u++];

				    if(center==null){
				    	center = ul[0];
				    }
				    
				    var poly = new google.maps.Polyline({
					    strokeColor: color,
					    strokeOpacity: 1,
					    strokeWeight: 2,
					    map: map,
					    icons: [{
                          icon: {
                                  path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
                                  strokeColor:color,
                                  fillColor:color,
                                  fillOpacity:1
                                },
                          repeat:'100px',
                          path:[]
                       }]
					  });

				    var path = poly.getPath();
				    var users_markers = [];

				    for(i=0;i<ul.length;i++){
						var marker = new google.maps.Marker({
						  position: ul[i],
						  icon: {
						    path: google.maps.SymbolPath.CIRCLE,
						    fillOpacity: (i==ul.length-1)?(1):(0.7),
						    fillColor: color,
						    strokeOpacity: 1.0,
						    strokeColor: color,
						    strokeWeight: 3.0, 
						    scale: 8 //pixels
						  },
					      title: ui[i].usta_time,
					      map:map
						});
						

					    path.push(ul[i]);
					    bounds.extend(ul[i]);
					    attachMarkerInfoWindow(marker,"<div class='tracking-marker'><div class='tracking-name'>"+ui[i].user_name+"</div><div class='tracking-time'>"+ui[i].usta_time+"</div></div>");

					    markers.push(marker)
					    users_markers.push(marker)
					}
					lastPosition.push(marker);
					
					users.push({name:ui[0].user_name,color:color,path:path,poly:poly,markers:users_markers});
				  }
				}

				if(!refresh && map!=null){
					map.setCenter(center);
					map.fitBounds(bounds);
				}


				if(0<users.length){
					$("#tracking-legend").show();
					$("#tracking-legend").empty();

					for(i=0;i<users.length;i++){
						html = "<div class='tracking-item'><div style='background-color:"+users[i].color+";' class='tracking-circle'></div>";
						
						html+= "<span class='tracking-action-view glyphicon glyphicon-eye-open active' data-id='"+i+"'></span>"
						html+= "<span class='tracking-action-time glyphicon glyphicon-time' data-id='"+i+"'></span>";
						
						html+= "<div class='tracking-name'>"+users[i].name+"</div></div>";
						$("#tracking-legend").append(html);


						//runSnapToRoad(users[i].poly,users[i].path,users[i].color)
					}
				}
				else{
					$("#tracking-legend").hide();
					$("#tracking-map").html("<div class='tracking-nodata'>No hay datos</div>");
					map = null;
				}


				if(animInterval){
					clearInterval(animInterval);
				}
				if(0<lastPosition.length){
					for(i=0;i<lastPosition.length;i++){
						google.maps.event.trigger(lastPosition[i], 'click');
					}


					animInterval = setInterval(function() {
						for(i=0;i<lastPosition.length;i++){
							circle = lastPosition[i]; 
				            circle.setVisible(!circle.getVisible());
				         }
			        }, 500);
				}
				
				$('#tracking-map').show();
				return;
			}
		}
		else{
			alert(json.error)
		}

		
	});
}

function convertDate(dt){
	var y  = dt.getFullYear();
	var m  = (dt.getMonth() > 8)?(dt.getMonth()+1):('0'+(dt.getMonth()+1));
	var d  = (dt.getDate()   > 9)?(dt.getDate()    ):('0'+dt.getDate());
	return y.toString()+"-"+m.toString()+"-"+d.toString();
}


*/
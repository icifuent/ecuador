(function($) {
    //INICIALIZACION
    $('.selectpicker').selectpicker();

    $('#fecha_validacion_inicio,#fecha_validacion_termino').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });
    
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m - 1, 1);
    var lastDay = new Date(y, m + 1, 0);
    $('#fecha_validacion_inicio').datepicker('setDate',firstDay);
    $('#fecha_validacion_termino').datepicker('setDate',lastDay);

    //EVENTOS
    $('#fecha_validacion_inicio,#fecha_validacion_termino').change(function() {
        $("#siom-form-sla-evaluacion").submit();
    });

    $('#siom-form-sla-evaluacion select[name=espe_id]').change(function() {
        $("#siom-form-sla-evaluacion").submit();
    });
	
    $('#siom-form-sla-evaluacion select[name=espe_idu]').change(function() {
        $("#siom-form-sla-evaluacion").submit();
    });

    $('#siom-form-sla-evaluacion select[name=zona_id]').change(function() {
        $("#siom-form-sla-evaluacion").submit();
    });
	
    $('#siom-form-sla-evaluacion select[name=zona_tipo]').change(function() {
        $("#siom-form-sla-evaluacion").submit();
    });

/*
    $('.BotonSLAEjecucionMenu').click(function(event) {
        event.preventDefault();
        //console.log('.BotonSLAEjecucionMenu click:' + $(this).data('tipo_datos'));
        $('.nav-tabs li').removeClass("active");
        $(this).parent().addClass("active");
        $('#tipo_datos').val($(this).data('tipo_datos'));
	$('#pagina').val("1");
*/		
        if( $(this).data('tipo_datos')==="grafico_zonas"  /*|| $(this).data('tipo_datos')==="validacion" */){
            $('#FiltroZonaTipo').hide();
            $('#FiltroZona').show();   
        } else {
            $('#FiltroZona').hide();
            $('#FiltroZonaTipo').show();            
        }
/*		
        if( $(this).data('tipo_datos')==="grafico_especialidades" ){
            $('#FiltroEspecialidades').hide();
            $('#FiltroEspecialidad').show();   
        }
		else{
            $('#FiltroEspecialidad').hide();
            $('#FiltroEspecialidades').show();            
        }
		
        $("#siom-form-sla-evaluacion").submit();
    })
*/
    
    $('.BotonSLAEvaluacion').click(function(event) {
        event.preventDefault();

        //console.log('.BotonSLAEvaluacion click:' + $(this).data('tipo_datos'));
        $('.nav-tabs li').removeClass("active");
        $(this).parent().addClass("active");
        $('#tipo_datos').val($(this).data('tipo_datos'));
	    $('#pagina').val("1");


        if ($(this).data('tipo_datos') == 'escalas') {
            //$('#siom-form-sla-evaluacion').css('display', 'none');
            console.log('ESCALAS');
        }
        if ($(this).data('tipo_datos') == 'detalle') {
            //$('#siom-form-sla-evaluacion').css('display', 'block');
            console.log('DETALLE');
        }

        $("#siom-form-sla-evaluacion").submit();        
    });
		
})(jQuery);
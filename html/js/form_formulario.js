(function($) { 
	$('.selectpicker').selectpicker();
    $('.datepicker').datepicker({
			    format: "dd/mm/yyyy",
			    viewMode: "days", 
			    minViewMode: "days",
			    language: "es"
			});
    $('.timepicker').timepicker({showMeridian:false,defaultTime: false});
    $('#wizard').wizard();

    $("form.siom-form").submit(function(e){
    	var form      	= $(this);
    	var btn 	 	= $(document.activeElement);

	    if(btn && btn.val().indexOf("cancel")!=-1){
	    	window.history.back();
	        return false;
	    }
	    var save_action	= btn.val();

	    var validated = true;
	    var route_to_save		= form.data("route");
    	var last                = form.data("last");
    	var form_id   			= form.data("form-id");
    	var form_name 			= form.data("form-name");
    	var foan_datetime_saved = formatDatetime(form.find(':input[name="fecha_visita"]').val(),form.find(':input[name="hora_visita"]').val());
    	var form_data        	= {};

    	btn.button('validating');
    	
    	form.data("form").pages.forEach(function(page){
    		page.controls.forEach(function(control){
    			if(["SELECT","TEXT"].indexOf(control.foco_type)!=-1){   		
    				msg   = "Debe ingresar <b>"+control.foco_label.toLowerCase()+"</b>";		
    				value = form.find(':input[name="'+control.foco_id+'"]').val()	
    				form_data[control.foco_id]= value;		
    			}
    			else if(["AGGREGATOR"].indexOf(control.foco_type)!=-1){   		
    				msg   = "Debe ingresar <b>"+control.foco_label.toLowerCase()+"</b>";		
    				value = form.find(':input[name="'+control.foco_id+'"]').val()	
    				if(value!=""){
    					form_data[control.foco_id]= JSON.parse(value);	
    				}	
    			}
    			else if(["CAMERA"].indexOf(control.foco_type)!=-1){
    				msg   = "Debe ingresar <b>"+control.foco_label.toLowerCase()+"</b>";

    				images   = form.find(':input[name="'+control.foco_id+'[]"]')
    				comments = form.find(':input[name="'+control.foco_id+'_comentario[]"]')
    				value    = [];

    				for(i=0;i<images.length;i++){
    					image = $(images[i]).val()

	    				if(image!=""){
							comment = $(comments[i]).val()
							if(comment=="" && control.foco_required=="1"){
	    						msg   = "Debe ingresar <b> descripción en "+control.foco_label.toLowerCase()+"</b>";
	    						value = "";
	    					}
	    					else{
	    						value.push({"image":image.replace(/^C:\\fakepath\\/,""),"comment":comment});
	    					}
						}
    				}
    				form_data[control.foco_id] = value;					
    			}

    			if(value=="" && control.foco_required=="1"){
					alert(msg);
					validated = false;
				}
    		});
    	});

		console.log("validated: "+validated);
		if(validated){
			var formData = new FormData();
			formData.append("form_id",form_id);
			formData.append("form_name",form_name);
			formData.append("foan_datetime_saved",foan_datetime_saved);
			formData.append("form_data",JSON.stringify(form_data));
			formData.append("status",GetFormStatus(form))
			formData.append("last",(last)?(1):(0));


			$.each(form.find("input:file"), function(i, file) {
				if($(file)[0].files[0]){
					formData.append($(this).attr("name"), $(file)[0].files[0]);
				}
			});
		
			console.log(formData);
			
			btn.button('loading');
			url = 'rest'+route_to_save;
			$.ajax({
			    url: url,
			    data: formData,
			    dataType: 'json',
			    cache: false,
			    contentType: false,
			    processData: false,
			    type: 'POST',
			    success: function(json, textStatus ){
			    	btn.button('reset');
			    	if(json.status){
			    		if(save_action=="save_continue"){
			    			form[0].reset();
			    		}
			    		else{
				    		if(last){
				    			alert("Visita guardada exitosamente",function(){
				    				window.history.back();
				    			});
				    			
				    			return true;
				    		}
				    		else{
					    		$('#wizard').wizard('next');
					    	}
					    }
			    	}
			    	else{
			    		alert(json.error);
			    		return false;
			    	}
			    },
			    error: function(xhr, textStatus, errorThrown){
			    	$("#save").button('reset');
			    	alert("No se pudo guardar formulario");
			    	return false;
	    		}
			});
		
	    	return last;
		}
		return false;
    });
	$("form.siom-form").on('click',':submit', function() {
		foco_id = $(this).data("foco-id");
		value   = $(this).val();

		$("form.siom-form").find(':input[name="'+foco_id+'"]').val(value);
	});

    $("form.siom-form").on('change','.btn-file :file', function() {
	    var input   = $(this),
	   	id     		= input.data("id");
	   	foco_id     = input.data("foco-id");
	   	list        = input.data("list");
	   	label       = input.data("label");
	   	required    = input.data("required");
	   	preview     = input.data("preview");
	   	filename  	= input.val().replace(/\\/g, '/').replace(/.*\//, '');
	   	nextId 		= parseInt(id)+1;

	   	var html = '<li class="list-group-item">';
	   	html+='<table width="100%" class="ctrl-camera-photo">';
	   	html+='<tr>';
	   	if(preview){
		   	html+='<td colspan="2">';
		   	//html+='<img class="preview" id="preview-'+foco_id+'-'+id+'" src="#"/>';
		   	html+='<div class="preview" id="preview-'+foco_id+'-'+id+'"></div>';
		   	html+='</td>';
		}
		html+='</tr>';
		html+='<tr>';
	   	html+='<td align="left">';
	   	html+='<div class="description"><textarea class="form-control" name="'+foco_id+'_comentario[]" rows="1" placeholder="Agregar descripción"></textarea></div>';
	   	html+='<div class="filename">'+filename+'</div>';
	   	html+='</td>';

	   	html+='<td width="20px">';
	   	html+='<a class="btn btn-xs remove" data-id="'+id+'" data-foco-id="'+foco_id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	   	html+='</td>';

	   	html+='</tr>';
	   	html+='</table>';

	   	html+='</li>';

		$(html).appendTo($("#"+list));

		if(preview){
		    previewImage($('#preview-'+foco_id+'-'+id),this);
		}

    	$('<input type="file" accept="image/*" name="'+foco_id+'[]" id="archivo-'+foco_id+'-'+nextId+'" data-id="'+nextId+'" data-foco-id="'+foco_id+'" data-list="'+list+'" data-label="'+label+'" data-required="'+required+'" data-preview="'+preview+'">').insertAfter($(this));
		
	});

	$("ul.ctrl-camera").on('click','a.btn', function(e) {
		var foco_id = $(this).data("foco-id");
		var id      = $(this).data("id");
		$("#archivo-"+foco_id+"-"+id).remove();
		$(this).closest("li").remove();
	});


	$("form.siom-form").on('click','button.aggregator', function() {
		var foco_id 	  = $(this).data("foco-id");
		var options 	  = $(this).data("options");
		var container 	  = $("#listado-aggregator-"+foco_id);
		var images_to_add = [];
		var data          = [];
		var index  		  =  container.children().length;


		console.log("agregador",foco_id,index,options)

		var html = '<li class="list-group-item" data-index="'+index+'" data-foco-id="'+foco_id+'">';
		html+='<table width="100%" class="ctrl-aggregator-item">';	   	
		$.each(options.labels, function(i, label) {
			control = $('form.siom-form [name="'+options.controls[i]+'"]');
			if(control){
				type  = control.data("type");
				value = control.val();
				
				html+='<tr>';
				html+='<td width="40%" align="left" valign="top">'+label+'</td>';

				if(value!=undefined){
					html+='<td width="60%" align="left" valign="top"><b>'+value+'</b></td>'; 

					if(type=="TEXT"){
						control.val("");
					}
					else if(type=="SELECT"){
						control.prop('selectedIndex',0);
						control.selectpicker('refresh');
					}
					data.push(value);	
				}
				else{ //asumo que es control camera
					html+='<td width="60%" align="left" valign="top">';
					html+='<ul id="listado-archivos-'+foco_id+'-'+options.controls[i]+'-'+index+'" class="list-group siom-file-list ctrl-aggregator-item-photo"></ul>';
					html+='</td>'; 	

					images_to_add.push({"ctrl_id":options.controls[i],"data_index":i});
					data.push({});
				}
				html+='</tr>';
			}
		});
		html+='<tr>';
		html+='<td colspan="2" align="right" valign="top">';
		html+='<a class="btn btn-xs remove" data-foco-id="'+foco_id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Eliminar</a>';
		html+='</td>';
   		html+='</tr>';
	   	html+='</table>';
		html+='</li>';

		$(html).appendTo(container);

		//cargar imagenes
		for(i=0;i<images_to_add.length;i++){
			ctrl_id    = images_to_add[i].ctrl_id;
			data_index = images_to_add[i].data_index;
			data_image  = {};

			images   = $('form.siom-form [name="'+ctrl_id+'[]"]')
			comments = $('form.siom-form [name="'+ctrl_id+'_comentario[]"]')
			value    = [];

			for(j=0;j<images.length;j++){
				if($(images[j])[0].files[0]){
					filename = $(images[j]).val().replace(/^C:\\fakepath\\/,"");
					comment  = $(comments[j]).val()

					//agrego el item 
					var html = '<li class="list-group-item">';
				   	html+='<table width="100%" class="photos">';
				   	html+='<tr>';
				   	html+='<td colspan="2" id="preview-'+foco_id+'-'+ctrl_id+'-'+index+'-'+j+'">';
					html+='</td>';
					html+='</tr>';
					html+='<tr>';
				   	html+='<td align="left">';
				   	html+='<div class="description">'+comment+'</div>';
				   	html+='<div class="filename">'+filename+'</div>';
				   	html+='</td>';
				   	html+='</tr>';
				   	html+='</table>';
				   	html+='<label class="file" id="file-'+foco_id+'-'+ctrl_id+'-'+index+'-'+j+'"></label>';
				   	html+='</li>';
					$(html).appendTo($("#listado-archivos-"+foco_id+"-"+ctrl_id+'-'+index));

					//muevo el preview
					$("#preview-"+ctrl_id+"-"+j).appendTo($('#preview-'+foco_id+'-'+ctrl_id+'-'+index+'-'+j));

					//muevo el input:file
					$(images[j]).removeAttr("id");
					$(images[j]).removeAttr("data-id");
					$(images[j]).attr("name",foco_id+"[]");
					$(images[j]).css("visibility","hidden");
					$(images[j]).appendTo($('#file-'+foco_id+'-'+ctrl_id+'-'+index+'-'+j));

					//agrego el valor
					value.push({"image":filename,"comment":comment});
				}
				else{
					$(images[j]).attr("id","archivo-"+$(images[j]).data("foco-id")+"-0")
					$(images[j]).attr('data-id', '0');
				}
			}

			$("#listado-archivos-"+ctrl_id).empty();
			data[data_index] = value;
		}

		curData = $(this).val();
		if(curData!=""){
			curData = JSON.parse(curData);
			curData.push(data);
		}
		else{
			curData = [data];
		}
		console.log(curData);
		$(this).val(JSON.stringify(curData));

	});

	$("ul.ctrl-aggregator").on('click','a.btn', function(e) {
		item = $(this).closest("li");
		foco_id = item.data("foco-id")
		index   = item.data("index")
		btn     = $('form.siom-form [name="'+foco_id+'"]')

		curData = JSON.parse(btn.val());
		if(curData!=null){
			curData.splice(index, 1);
		}
		console.log(curData);
		btn.val(JSON.stringify(curData));

		item.remove();
	});


	function previewImage(element,input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            //element.attr('src', e.target.result);
	            element.css("background-image", "url(" + e.target.result + ")");
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	}

	function formatDatetime(date,time){
		d = date.split("/");
		return d[2]+"-"+d[1]+"-"+d[0]+" "+time+":00";
	}

	function GetFormStatus(form){
		formDef = form.data("form");
		
		if(formDef.form_options && formDef.form_options.task_item_status_conditions){
			c = formDef.form_options.task_item_status_conditions.conditions;
			s = formDef.form_options.task_item_status_conditions.status;

			//console.log(c,s);

			for(i=0;i<c.length;i++){
				foco_id  = c[i][0];
				operator = c[i][1];
				value    = c[i][2];

				focoValue = form.find(':input[name="'+foco_id+'"]').val();

				switch(operator){
					case "=":{
						if(value==focoValue){
							//console.log("status: "+s[i]);
							return s[i];
						}
					}
					//TODO: más operadores...
				}
			}

		}
		return "";
	}

})(jQuery);
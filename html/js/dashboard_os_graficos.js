(function($) {
    
    $("#os_zona").change(function(e) {
        drawChartsOSZona($(this).val());
    });
  
    function drawCharts() {
        grafico_pie("#siom-chart-os-espe", ["Especialidad", "Ordenes de servicio"]);
        grafico_columns("#siom-chart-os-zona", ["Zona", "Aprobadas", "Asignadas Ezentis", "Asignadas Movistar" , "Rechazadas"]); /*MODIFICAR AQUI*/
        grafico_one_column("#siom-chart-os-gastos", ["Zona", "Gasto"]);

        drawChartsOSZona($("#os_zona").val());
    }

    function drawChartsOSZona(zona) {
        data = []
        data_espe = $("#os_zona").data("espe");
        if (data_espe != null) {
            if (data_espe[zona] != null) {
                data = data_espe[zona];
            }
        }

        $("#siom-chart-os-espe-zona-nombre").text("ZONA " + zona);
        grafico_pie("#siom-chart-os-espe-zona", ["Especialidad", "Ordenes de servicio"], data);

        data = []
        data_gestor = $("#os_zona").data("gestor");
        if (data_gestor != null) {
            if (data_gestor[zona] != null) {
                data = data_gestor[zona];
            }
        }
        $("#siom-chart-os-gestor-zona-nombre").text("ZONA " + zona);
        grafico_columns2("#siom-chart-os-gestor-zona", ["Gestor", "Aprobadas", "Asignadas Ezentis", "Asignadas Movistar" , "Rechazadas"], data);

        data = []
        data_gasto = $("#os_zona").data("gasto");
        if (data_gasto != null) {
            if (data_gasto[zona] != null) {
                data = data_gasto[zona];
            }
        }
        $("#siom-chart-os-gastos-zona-nombre").text("ZONA " + zona);
        grafico_one_column("#siom-chart-os-gastos-zona", ["Gestor", "Gasto"], data);
    }




    function grafico_pie(id, labels, data_json) {
        var chart_os_espe = $(id);
        if (data_json == null) {
            data_json = chart_os_espe.data("data");
        }
        
        //console.log(data_json);
        if (data_json.length == 0) {
            chart_os_espe.text("No hay datos");
        }
        else {

            var data_array = [];
            var total = 0;
            for (var i = 0; i < data_json.length; i++) {
                total += parseInt(data_json[i]['cantidad']);
            }

            var color =[];
            for (var i = 0; i < data_json.length; i++) {
                var etiqueta = data_json[i]['nombre'] + " (" + (parseInt(data_json[i]['cantidad']) / total * 100.0).toFixed(2) + "%)";
                var valor =  parseInt(data_json[i]['cantidad']);
                var tooltip = "<strong style='display: inline-block;overflow: hidden;white-space: nowrap;'>"+etiqueta+"</strong><br><div style='float:right'>" + data_json[i]['cantidad']+' de '+total+"</div>";
                  
                //COLORS  
                console.log("switch");
                console.log(String(data_json[i]['nombre']));
                switch(String(data_json[i]['nombre'])){
                  case "ENERGIA":
                                  //color=color +"#008000,"; //GREEN
                                  color.push("#008000");
                                  console.log("COLOR ENERGIA");
                                  break;
                  case "CLIMA":
                                  //color=color +"#0000FF,"; //BLUE
                                  color.push("#0000FF");
                                  console.log("COLOR CLIMA");
                                  break;
                  case "HARDWARE":
                                  //color=color +"#FFFF00," //YELLOW
                                  color.push("#FFFF00");
                                  console.log("COLOR HARDWARE");
                                  break;
                  case "SISTEMA RADIANTE":
                                  //color=color +"#FFA500,"; // ORANGE
                                  color.push("#FFA500");
                                  break;
                  case "TRANSMISION":
                                  //color=color +"#FF0000,"; // ORANGE
                                  color.push("#FF0000");
                                  break;
                  case "OBRAS CIVILES":
                                  //color=color +"#800080,"; //PURPLE
                                  color.push("#800080");
                                  break;
                  case "SINIESTRO":
                                  //color=color +"#A52A2A,"; //BROWN
                                  color.push("#A52A2A");
                                  break;
                  case "OTROS":
                                  //color=color +"#4169E1,"; //ROYAL BLUE
                                  color.push("#4169E1");
                                  break;
                  case "Otro":
                                  //color=color +"#808080,"; //GREY
                                  color.push("#808080");
                                  break;
                  default:
                                  color.push("#808080"); //GREY
                                  break;
                };
                console.log("COLOR");
                console.log(color);

                data_array.push([etiqueta,valor,tooltip]);
            }

            var options1 = {
                is3D: false,
                pieSliceText: 'label',
                pieSliceTextStyle: {fontSize: 8},
                chartArea: {left: 20, top: 20, width: '90%', height: '75%'},
                legend: {position: 'right', alignment: 'center', textStyle: {color: 'black', fontSize: 10}},
                tooltip: {isHtml: true},
                /*colors: ["#008000","#dc3912","#ff9900","#109618","#990099","#0099c6","#dd4477","#66aa00","#b82e2e","#316395",
          "#994499","#22aa99","#aaaa11","#6633cc","#e67300","#8b0707","#651067","#329262","#5574a6","#3b3eac","#b77322",
          "#16d620","#b91383","#f4359e","#9c5935","#a9c413","#2a778d","#668d1c","#bea413","#0c5922","#743411",],*/
                colors: color,
            };

            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn('string', labels[0]); //etiqueta
            dataTable.addColumn('number', labels[1]); //valor
            dataTable.addColumn({type: 'string', role: 'tooltip', p: {html: true}}); //tooltip
            //dataTable.addColumn({type: 'string', role: 'style'}); //color
            dataTable.addRows(data_array);


            var chart1 = new google.visualization.PieChart(chart_os_espe[0]);
            chart1.draw(dataTable, options1);
        }
    }

    function grafico_columns(id,labels,data_json){
        chart_os_zona  = $(id);
        if(data_json==null){
            data_json = chart_os_zona.data("data");
        }

        data_array     = [[labels[0],labels[1],{role:"annotation"},labels[2],{role:"annotation"},labels[3],{role:"annotation"},labels[4],{role:"annotation"}]];

        if(data_json.length==0){
          chart_os_zona.text("No hay datos");
        }
        else{
          for(i=0;i<data_json.length;i++){
              var aprobadas=data_json[i]['aprobadas'];
              var rechazadas=parseInt(data_json[i]['rechazadas']);
              var contratista=parseInt(data_json[i]['contratista']);
              var movistar=parseInt(data_json[i]['movistar']);

              var textAprobadas='';
              var textRechazadas='';
              var textContratista='';
              var textMovistar='';



              if(0==aprobadas){
                textAprobadas=null;
              }else{
                textAprobadas=data_json[i]['aprobadas'];
              }

              if(0==rechazadas){
                textRechazadas=null;
              }else{
                textRechazadas= data_json[i]['rechazadas'];
              }
              if(0==contratista){
                textContratista=null;
              }else{
                textContratista=data_json[i]['contratista'];
              }
              if(0==movistar){
                textMovistar=null;
              }else{
                textMovistar=data_json[i]['movistar'];
              }

              total = parseInt(data_json[i]['aprobadas']) + parseInt(data_json[i]['rechazadas']) + parseInt(data_json[i]['movistar']) + parseInt(data_json[i]['contratista']);
              data_array.push([data_json[i]['zona_nombre'],
                               Math.round(parseFloat((data_json[i]['aprobadas']/total)*100)*100)/100,
                               textAprobadas,
                               Math.round(parseFloat((data_json[i]['contratista']/total)*100)*100)/100,
                               textContratista,
                               Math.round(parseFloat((data_json[i]['movistar']/total)*100)*100)/100,
                               textMovistar,   
                               Math.round(parseFloat((data_json[i]['rechazadas']/total)*100)*100)/100,
                               textRechazadas
                               ])
          } 

          var options2 = {
            title: '',
            titleTextStyle:{fontSize: 12},
            chartArea: {left:30,top:20,width:'90%',height:'70%'},
            vAxis: {minValue: 0, maxValue: 100, gridlines: { count: 11 },
                        textStyle : {
                                  fontSize: 7 
                              }
            },
            hAxis : { 
                textStyle : {
                    fontSize: 7 
                }

            },
            legend:{ position : 'none'/*position: 'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}*/},
            bar: { groupWidth: '75%' },
            isStacked: true,
            seriesType: 'bars',
            annotations: {
                             textStyle: {
                                            fontSize: 8
                             }
            },
            colors: ['#109618', "#FF00FF" /*'#dc3912'*/, "#0000FF", "#6c6b73"/*'#FF8000'*/]
          };

          var chart2 = new  google.visualization.ComboChart(chart_os_zona[0]);
          chart2.draw(google.visualization.arrayToDataTable(data_array), options2);
        }
    }
	
	function grafico_columns2(id,labels,data_json){
        chart_os_zona  = $(id);
        if(data_json==null){
            data_json = chart_os_zona.data("data");
        }

        data_array     = [[labels[0],labels[1],{role:"annotation"},labels[2],{role:"annotation"},labels[3],{role:"annotation"},labels[4],{role:"annotation"}]];

        if(data_json.length==0){
          chart_os_zona.text("No hay datos");
        }
        else{
          for(i=0;i<data_json.length;i++){
              var aprobadas=data_json[i]['aprobadas'];
              var rechazadas=parseInt(data_json[i]['rechazadas']);
              var contratista=parseInt(data_json[i]['contratista']);
              var movistar=parseInt(data_json[i]['movistar']);

              var textAprobadas='';
              var textRechazadas='';
              var textContratista='';
              var textMovistar='';



              if(0==aprobadas){
                textAprobadas=null;
              }else{
                textAprobadas=data_json[i]['aprobadas'];
              }

              if(0==rechazadas){
                textRechazadas=null;
              }else{
                textRechazadas= data_json[i]['rechazadas'];
              }
              if(0==contratista){
                textContratista=null;
              }else{
                textContratista=data_json[i]['contratista'];
              }
              if(0==movistar){
                textMovistar=null;
              }else{
                textMovistar=data_json[i]['movistar'];
              }

              total = parseInt(data_json[i]['aprobadas']) + parseInt(data_json[i]['rechazadas']) + parseInt(data_json[i]['movistar']) + parseInt(data_json[i]['contratista']);
              data_array.push([data_json[i]['nombre'],
                               Math.round(parseFloat((data_json[i]['aprobadas']/total)*100)*100)/100,
                               textAprobadas,
                               Math.round(parseFloat((data_json[i]['contratista']/total)*100)*100)/100,
                               textContratista,
                               Math.round(parseFloat((data_json[i]['movistar']/total)*100)*100)/100,
                               textMovistar,   
                               Math.round(parseFloat((data_json[i]['rechazadas']/total)*100)*100)/100,
                               textRechazadas
                               ])
          } 

          var options2 = {
            title: '',
            titleTextStyle:{fontSize: 12},
            chartArea: {left:30,top:20,width:'90%',height:'70%'},
            vAxis: {minValue: 0, maxValue: 100, gridlines: { count: 11 },
                        textStyle : {
                                  fontSize: 7 
                              }
            },
            hAxis : { 
                textStyle : {
                    fontSize: 7 
                }

            },
            legend:{ position : 'none'/*position: 'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}*/},
            bar: { groupWidth: '75%' },
            isStacked: true,
            seriesType: 'bars',
            annotations: {
                             textStyle: {
                                            fontSize: 8
                             }
            },
            colors: ['#109618', "#FF00FF" /*'#dc3912'*/, "#0000FF", "#6c6b73"/*'#FF8000'*/]
          };
		  
          var chart2 = new  google.visualization.ComboChart(chart_os_zona[0]);
		      chart2.draw(google.visualization.arrayToDataTable(data_array), options2);
        }
    }


    function grafico_one_column(id,labels,data_json) {
      // body...
      chart_os_zona  = $(id);
      if(data_json==null){
        data_json      = chart_os_zona.data("data");
      }
      data_array     = [labels];
      if(data_json.length==0){
          chart_os_zona.text("No hay datos");
      }
      else{
        for(i=0;i<data_json.length;i++){
             data_array.push([data_json[i]['nombre'],parseFloat(data_json[i]['cantidad'])])
        }
        var options2 = {
          title: '',
          titleTextStyle:{fontSize: 12},
          chartArea: {left:25,top:20,width:'90%',height:'70%'},
          hAxis : { textStyle : { fontSize: 8 }},
          vAxis: {maxValue: 100},
          legend:{position: 'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}},
          bar: { groupWidth: '25%' },
          isStacked: true,
          colors: ['#dc3912','#109618']
        };
        var chart2 = new  google.visualization.ColumnChart(chart_os_zona[0]);
        chart2.draw(google.visualization.arrayToDataTable(data_array), options2);
      }
    }

    if(typeof google.visualization != "undefined" && 
       typeof google.visualization.PieChart != "undefined" &&
        typeof google.visualization.ColumnChart != "undefined" &&
        typeof google.visualization.ComboChart != "undefined"){
      drawCharts();
    }
    else{
      google.setOnLoadCallback(drawCharts);
    }

})(jQuery);

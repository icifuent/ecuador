(function($) {
    //INICIALIZACION
    
    var datos_ponderacion = $('#tabla_ponderacion').data('datos');
    var datos_rangos = $('#tabla_rangos').data('datos');
    var datos_calificaciones = $('#tabla_calificaciones').data('datos');
    var tabla_insert = '';

    for (var i = 0; i < datos_ponderacion.length; i++) {
        tabla_insert += 
                '<tr>' +
                    '<td rowspan="' + datos_ponderacion[i]['item'].length + '">' + datos_ponderacion[i]['texto'] + '</td>' +
                    '<td>' + datos_ponderacion[i]['item'][0]['texto'] + '</td>' +
                    '<td>' + datos_ponderacion[i]['item'][0]['peso_espe'] + '%</td>' +
                    '<td rowspan="' + datos_ponderacion[i]['item'].length + '">' + datos_ponderacion[i]['peso_espe'] + '%</td>' +                    
                '</tr>';

        for (var j = 1; j < datos_ponderacion[i]['item'].length; j++) {
            tabla_insert +=
                    '<tr>' + 
                        '<td>' + datos_ponderacion[i]['item'][j]['texto'] + '</td>' +
                        '<td>' + datos_ponderacion[i]['item'][j]['peso_espe'] + '%</td>' +
                    '</tr>';
        }
    }

    //console.log(tabla_insert);    
    $('#tabla_ponderacion_body').append(tabla_insert);
    
    tabla_insert = '<thead><tr><th rowspan="2">Nota</th>';
    for (var i = 0; i < datos_ponderacion.length; i++) {
        tabla_insert += '<th colspan="' + datos_ponderacion[i]['item'].length + '">' + datos_ponderacion[i]['texto'] + '</th>';             
    }    
    tabla_insert += '</tr><tr>';
    for (var i = 0; i < datos_ponderacion.length; i++) {
        for (var j = 0; j < datos_ponderacion[i]['item'].length; j++) {
            tabla_insert += '<th>' + datos_ponderacion[i]['item'][j]['texto'] + '</th>';
        }                   
    }    
    tabla_insert += '</tr></thead>';
    
    tabla_insert += '<tbody>';
    for (var i = 0; i < datos_rangos.length; i++) {
        tabla_insert += '<tr>';
        tabla_insert += '<td>'+datos_rangos[i][0]['nota']+'</td>';
        //console.log(datos_rangos[i]);
        for (var j = 0; j < datos_rangos[i].length; j++) {
            //console.log(datos_rangos[i][j]);
            for (var k = 0; k < datos_rangos[i][j]['item'].length; k++) {
                //console.log(datos_rangos[i][j]['item'][k]);
                var texto = (datos_rangos[i][j]['item'][k]['tasa_min'] == datos_rangos[i][j]['item'][k]['tasa_max'])?datos_rangos[i][j]['item'][k]['tasa_min']+'%':datos_rangos[i][j]['item'][k]['tasa_min'] + '%&le;y' + ((datos_rangos[i][j]['nota']==10)?'&le;':'<') + datos_rangos[i][j]['item'][k]['tasa_max']+'%';
                tabla_insert += '<td>' + texto + '</td>';
            }   
        }      
        tabla_insert += '</tr>';
    } 
    tabla_insert += '</tbody>';
    
    $('#tabla_rangos').append(tabla_insert);
    
    
    tabla_insert = '';
    for (var i = 0; i < datos_calificaciones.length; i++) {
        tabla_insert +=
                    '<tr>' + 
                        '<td>' + datos_calificaciones[i]['nota_min'] + ' &le; y ' + ((datos_calificaciones[i]['nota_max']==10)?'&le; ':'< ') + datos_calificaciones[i]['nota_max'] +  '</td>' +
                        '<td>' + datos_calificaciones[i]['calificacion'] + '</td>' +
                    '</tr>';
    }

    //console.log(tabla_insert);    
    $('#tabla_calificaciones_body').append(tabla_insert);

		
})(jQuery);        
        


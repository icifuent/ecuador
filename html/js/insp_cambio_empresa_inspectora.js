(function($) { 
	//inicializar componentes
    $('.selectpicker').selectpicker();
    window.scrollToElement($('#siom-form-cambio-empresa'));

    $('#siom-form-cambio-empresa').submit(function() {
        if($("#empr_id").val()==""){
            alert("Debe selecionar una empresa");
            return false;
        }

        $("#submit_cambiar").button('loading');
        return true;
    });
    
})(jQuery);
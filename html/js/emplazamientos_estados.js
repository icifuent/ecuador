(function($) { 
    var clusterStyles = [
          {textColor: 'black',url: 'img/cluster1-success.png',width: 53,height: 52},
          {textColor: 'black',url: 'img/cluster2-success.png',width: 56,height: 55},
          {textColor: 'black',url: 'img/cluster3-success.png',width: 66,height: 65},
          {textColor: 'black',url: 'img/cluster4-success.png',width: 77,height: 76},
          {textColor: 'black',url: 'img/cluster5-success.png',width: 90,height: 89},

          {textColor: 'black',url: 'img/cluster1-warning.png',width: 53,height: 52},
          {textColor: 'black',url: 'img/cluster2-warning.png',width: 56,height: 55},
          {textColor: 'black',url: 'img/cluster3-warning.png',width: 66,height: 65},
          {textColor: 'black',url: 'img/cluster4-warning.png',width: 77,height: 76},
          {textColor: 'black',url: 'img/cluster5-warning.png',width: 90,height: 89},

          {textColor: 'black',url: 'img/cluster1-danger.png',width: 53,height: 52},
          {textColor: 'black',url: 'img/cluster2-danger.png',width: 56,height: 55},
          {textColor: 'black',url: 'img/cluster3-danger.png',width: 66,height: 65},
          {textColor: 'black',url: 'img/cluster4-danger.png',width: 77,height: 76},
          {textColor: 'black',url: 'img/cluster5-danger.png',width: 90,height: 89}];

    window.resizeElements();

	//mapa
  function InitMap(){
      mapa           = $("#emplazamientos_mapa");
      emplazamientos = mapa.data("emplazamientos");

      var options = {
      'mapTypeId': google.maps.MapTypeId.ROADMAP
      };


      window.gEmplMap  = new google.maps.Map(mapa[0], options);
      if(gEmplMarkerCluster){
          gEmplMarkerCluster.clearMarkers();
      }   

      var gMarkers       = [];
      var gMarkerBounds = new google.maps.LatLngBounds();
      var gInfowindow   = new google.maps.InfoWindow({
        content: ""
      }); 

      for (var i = 0; i < emplazamientos.length; i++) {
          var latLng = new google.maps.LatLng(emplazamientos[i].empl_latitud,
                                            emplazamientos[i].empl_longitud);
          
          var info = "<h5>"+emplazamientos[i].empl_nombre+"</h5><b>"+emplazamientos[i].clas_nombre+"</b> | "+emplazamientos[i].empl_direccion +"<br>";
          
          //INICIO LOGICA NUEVA
          if(emplazamientos[i].empl_visitas==0){
            info += "<div class='link'><a href='#/emplazamientos/detalle/"+emplazamientos[i].empl_id+"'>Ver detalle</a></div>";
          }else{
            info += "<div class='link'><a href='#/"+emplazamientos[i].tipo_modulo_visita.toLowerCase()+"/detalle/"+emplazamientos[i].os_mnt_id_visita+"'>Ver detalle</a></div>";
          }
          //FIN LOGICA NUEVA
          var iconColor = "999999";
          if(emplazamientos[i].orse_indisponibilidad=="SI"){
              iconColor = "FF0000";
          }
          else if(emplazamientos[i].orse_indisponibilidad=="NO" || emplazamientos[i].orse_indisponibilidad==""){
              iconColor = "00FF00";
          }
          else {
              iconColor = "FFFF00";
          }


          if(0<emplazamientos[i].empl_visitas){
            var marker = new StyledMarker({'styleIcon':new StyledIcon(StyledIconTypes.MARKER,{color:iconColor}),
                                         'position': latLng,
                                         'html':info,
                                         'indisponibilidad':emplazamientos[i].orse_indisponibilidad});
            marker.setAnimation(google.maps.Animation.BOUNCE);
            marker.setMap(window.gEmplMap);
          }
          else{
            var marker = new StyledMarker({'styleIcon':new StyledIcon(StyledIconTypes.MARKER,{color:iconColor}),
                                         'position': latLng,
                                         'html':info,
                                         'indisponibilidad':emplazamientos[i].orse_indisponibilidad});                      
            gMarkers.push(marker);
          
          }
          
          google.maps.event.addListener(marker, 'click', function() {
              gInfowindow.setContent(this.html);
              gInfowindow.open(window.gEmplMap, this);
          });
          gMarkerBounds.extend(latLng);
      }

      if(0<gMarkers.length){
        var gEmplMarkerCluster = new MarkerClusterer(window.gEmplMap,gMarkers,{styles: clusterStyles,
                                                                        gridSize: 50, 
                                                                        maxZoom: 12,
                                                                        enableRetinaIcons:true});
        gEmplMarkerCluster.setCalculator(function(markers, numStyles) {
                                          var index = 0;
                                          var count = markers.length;
                                          var dv = count;
                                          while (dv !== 0) {
                                            dv = parseInt(dv / 10, 10);
                                            index++;
                                          }

                                          index = Math.min(index, 5);
                                          indisponibilidad = "NO"
                                          for(i=0;i<count ;i++){
                                            if(markers[i].indisponibilidad=="SI"){
                                                indisponibilidad = "SI";
                                                break;
                                            }
                                            else if(markers[i].indisponibilidad=="PARCIAL"){
                                                indisponibilidad = "PARCIAL";
                                            } 
                                          }

                                          if(indisponibilidad == "SI"){
                                            index += 10;
                                          }
                                          if(indisponibilidad == "PARCIAL"){
                                            index += 5;
                                          }
                                          
                                          return {
                                            text: count,
                                            index: index
                                          };
                                        });
      }

      window.gEmplMap.fitBounds(gMarkerBounds);
      window.gEmplMapBounds = gMarkerBounds;
    }

    //lista
    /*
    $('#pagination').bootpag().on("page", function(event, num){
        page = num;
        data = $.extend($('#pagination').data("filters"),{page:page});
        window.app.runRoute('post','#/emplazamientos/filtro/'+page,data);
    });
    */


    $("button#download").on("click",function(e){
        e.stopImmediatePropagation();
        $btn = $(this);
        $btn.button("loading");
        $.fileDownload("rest/core/repo/contrato/"+window.contract+"/emplazamientos",{httpMethod:"POST",data:$.param($btn.data("filters")),
            prepareCallback:function(url) {
                $btn.button("processing");
            },
            successCallback: function(url) {
                $btn.button('reset')
            },
            failCallback: function(responseHtml, url) {
                $btn.button('reset')
                alert(responseHtml);
            }
        });
    });

    InitMap();
})(jQuery);
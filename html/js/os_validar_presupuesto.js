(function($) {

   window.scrollToElement($('#siom-form-validar-presupuesto'));

   $("#siom-form-validar-presupuesto :checkbox").click(function(){
   		prit_id = $(this).data("prit-id");
     	subtotal = $(this).data("subtotal");

        if($(this).is(':checked')){
     		$("#observacion_"+prit_id).prop("disabled",true);
     		$("#"+prit_id).removeClass("bg-danger").addClass("bg-success");
        	ActualizarTotal(Number(subtotal), 0);
     	} else {
     		$("#observacion_"+prit_id).prop("disabled",false);
     		$("#"+prit_id).removeClass("bg-success").addClass("bg-danger");
        	ActualizarTotal(Number(subtotal), 1);
     	}
     	ActualizarEstado();  
   });

    $("#submit_presupuesto").click(function(){
    	button   = $(this);
    	form     = $("#siom-form-validar-presupuesto");
    	prit_id          = form.find(':input[name="prit_id"]').map(function(){ return $(this).val(); }).get();
    	prit_estado      = form.find(':input[name="prit_estado"]').map(function(){ return $(this); }).get();
    	prit_comentario  = form.find(':input[name="prit_comentario"]').map(function(){ return $(this); }).get();

    	var aprobado = true;
    	for(i=0;i<prit_id.length;i++){
    		checkbox = $("#estado_"+prit_id[i]);
    		if(checkbox.is(':checked')){
    			prit_estado[i].val("VALIDADO");
    		} else {
    			prit_estado[i].val("RECHAZADO");
    			observacion = $("#observacion_"+prit_id[i]);
    		
    			if(observacion.val()==""){
    				observacion.addClass("error")
    				alert("Debe indicar observación en item rechazado");
    				return false;
    			} else {
    				observacion.removeClass("error")
    			}
    			prit_comentario[i].val(observacion.val());
    			aprobado = false;
    		}
    	}

    	confirm("El presupuesto será <b>"+(aprobado?"PRE-APROBADO":"PREAPROBADO CON OBSERVACIONES")+"</b><br>Desea continuar?",function(status){
    		if(status){
    			button.button("loading");
    			form.submit();
    		}
    	});
    });

    $("#download").click(function(e){
		$btn = $(this);
		$btn.button("loading");
		$.fileDownload("rest/core/repo/presupuesto/"+$(this).data("pres-id"), {
		  prepareCallback:function(url) {
		      $btn.button("processing");
		  },
		  successCallback: function(url) {
		      $btn.button('reset')
		  },
		  failCallback: function(responseHtml, url) {
		      $btn.button('reset')
		      alert(responseHtml);
		  }
		});
    });


   	function ActualizarEstado(){
   		var element      = $("#siom-os-presupuesto-estado-validacion");
   		var countChecked = $("#siom-form-validar-presupuesto :checkbox").filter(':checked').length;

   		element.text(countChecked+" items aprobados de un total de "+element.data("items"));
   	}

    function ActualizarTotal(total, resta){

      var element      = $("#siom-os-presupuesto-total-validacion-valor");
      var totalNuevo = 0;
      if(resta==0){
        var totalNuevo = Number(element.html())+total;
      }else{
        var totalNuevo = Number(element.html())-total;
      }
      console.log(total);
      console.log(Number(element.html()));
      console.log(totalNuevo);
      if(0>=totalNuevo){
        var totalNuevo = 0;
      }
      element.text(totalNuevo);
    }

    function UpdateTotal(){
      console.log("UPDATE TOTAL");
      total = 0;
      console.log(total);
      $.each($("#siom-form-validar-presupuesto :checkbox"),function(index,row){
        	console.log("validar checkbox");
      		/*id = $(row).attr("id");
            console.log("id");
            console.log(id);
            console.log("#subtotal-"+id);
            console.log($("#subtotal-"+id).html());
      		//total += $("#subtotal-"+id).data("subtotal");
            total += Number($("#subtotal-"+id).html());
            console.log("total");
            console.log(total);
            */

        	prit_id = $(this).data("prit-id");
        	subtotal = $(this).data("subtotal");


            if($(this).is(':checked')){
				console.log("checked");
				$("#observacion_"+prit_id).prop("disabled",true);
				$("#"+prit_id).removeClass("bg-danger").addClass("bg-success");
				ActualizarTotal(Number(subtotal), 0);
            } /*else {
              console.log("unchecked");
              $("#observacion_"+prit_id).prop("disabled",false);
              $("#"+prit_id).removeClass("bg-success").addClass("bg-danger");
              ActualizarTotal(Number(subtotal), 1);
            }*/

      });
      ActualizarEstado();
    };

    UpdateTotal();

})(jQuery);

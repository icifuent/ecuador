var _isLogged = false;
var _appVersion = "1.4.1";
String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1
};
(function($) {
    window.app = $.sammy('#main', function() {
        this.use('Handlebars', 'hb');
        this.use('GoogleAnalytics');
        this.helpers({
            showMain: function(context, template) {
                context.user = window.user;
                context.client = client;
                context.sections = sections;
                context.contract = contract;
                context.contracts = contracts;
                context.tasks = window.tasks || [];
                context.version = _appVersion;
                $.each(context.sections, function(index, s) {
                    l = '#/' + template.replace("_", "/");
                    if (s.sections) {
                        $.each(s.sections, function(index, ss) {
                            if (l.indexOf(ss.link) == 0) {
                                ss['default'] = true
                            } else {
                                ss['default'] = false
                            }
                        })
                    } else {
                        if (l.indexOf(s.link) == 0) {
                            s['default'] = true
                        } else {
                            s['default'] = false
                        }
                    }
                });
                context.partial('templates/main.hb?' + _appVersion)
            },
            showSection: function(context, template, data, callback) {
                var that = this;
                this.load('templates/' + template + '.hb?' + _appVersion).then(function(partial) {
                    context.data = data;
                    context.partials = {
                        section_template: partial
                    };
                    context.partial('templates/main.hb?' + _appVersion, data, function() {
                        if (callback) {
                            callback(data)
                        }
                    })
                })
            },
            showUI: function(context, template, template_data, url, url_params, callback) {
                try {
                    var that = this;
                    if (!url_params) {
                        url_params = {}
                    };
                    this.showMain(context, template);
                    $.get(url, url_params, function(data) {
                        data = $.extend(data, template_data);
                        if (!data.status) {
                            if (data.isSessionExpired) {
                                that.showError("Sesión expirada", data.error, "", "#/logout");
                                return
                            }
                        };
                        that.showSection(context, template, data, callback)
                    }, 'json').fail(function() {
                        alert("(UI) La busqueda realizada no posee datos de respuesta");
                        that.showSection(context, template, data, callback)
                    })
                } catch (e) {
                    data = {
                        status: false,
                        error: e.message
                    };
                    that.showSection(context, template, data, callback)
                }
            },
            updateNotifications: function(data) {
                if (data.status) {
                    actualizar_tareas = true;
                    if (window.tasks) {
                        if (JSON.stringify(window.tasks) === JSON.stringify(data.tareas)) {
                            actualizar_tareas = false
                        }
                    };
                    if (actualizar_tareas) {
                        window.tasks = data.tareas;
                        num_tareas = data.tareas.length;
                        lista = $("#siom-lista-tareas");
                        if (num_tareas == 0) {
                            $("#siom-numero-tareas").text("0");
                            $("#siom-numero-tareas").removeClass("label-danger").addClass("label-success");
                            lista.html("<li><div class='sin-tarea'>Sin tareas pendientes</div></li>")
                        } else {
                            $("#siom-numero-tareas").text(num_tareas);
                            $("#siom-numero-tareas").removeClass("label-success").addClass("label-danger");
                            html = "";
                            for (i = 0; i < num_tareas; i++) {
                                tarea = data.tareas[i];
                                t = window.ProcesarTarea(tarea);
                                html += "<li>";
                                html += "<div class='row'>";
                                html += "<div class='col-xs-8 text-left'>";
                                html += "<div class='tarea'><a href='" + t.link + "'>" + t.texto + "</a></div>";
                                html += "<div class='emplazamiento'>" + tarea.empl_nombre + "</div>";
                                html += "<div class='direccion'>" + tarea.empl_direccion + "</div>";
                                html += "</div>";
                                html += "<div class='col-xs-4 text-right'>";
                                html += "<div class='id_relacionado'><b>" + tarea.tare_modulo + "</b> Nº " + tarea.tare_id_relacionado + "</div>";
                                html += "<div class='fecha'>" + $.timeago(tarea.tare_fecha_despacho) + "</div>";
                                html += "</div>";
                                html += "</div>";
                                html += "</li>"
                            };
                            lista.html(html)
                        }
                    };
                    if (0 < data.notificaciones.length) {
                        var stack_bottomright = {
                            "dir1": "up",
                            "dir2": "left",
                            "push": "top",
                            "firstpos1": 25,
                            "firstpos2": 0
                        };
                        for (i = 0; i < data.notificaciones.length; i++) {
                            noti = data.notificaciones[i];
                            n = window.ProcesarNotificacion(noti);
                            if (!window.notificaciones) {
                                window.notificaciones = {}
                            }
                        }
                    }
                } else {
                    console.log(data.error)
                }
            },
            updateElement: function(element, template, url_data, params_data, options) {
                try {
                    var that = this;
                    var msg = (options && options.loading_message) || "Cargando datos...";
                    element.text(msg);
                    $.post(url_data, params_data, function(data) {
                        if (data.status) {
                            data.options = options;
                            $.get("templates/" + template + ".hb").then(function(src) {
                                template = Handlebars.compile(src);
                                element.html(template(data));
                                window.resizeElements()
                            }).fail(function() {
                                that.showError(element, "Error al leer template", "template:" + template)
                            })
                        } else {
                            that.showError(element, data.error, "ruta:" + url_data)
                        }
                    }, 'json')
                } catch (e) {
                    this.showError(element, e.message)
                }
            },
            showError: function(title, message, debug_info, nextRoute) {
                var that = this;
                modal = $("#errorModal");
                modal.find(".modal-title").text("SIOM " + title);
                body = "<strong>ERROR </strong>" + message;
                if (debug_info) {
                    body += "<br><small>";
                    body += debug_info;
                    body += "</small>"
                };
                modal.find(".modal-body").html(body);
                modal.modal('show');
                modal.on('hidden.bs.modal', function(e) {
                    if (nextRoute) {
                        that.redirect(nextRoute)
                    }
                })
            },
            showSuccess: function(title, message, nextRoute) {
                var that = this;
                modal = $("#successModal");
                modal.find(".modal-title").text("SIOM " + title);
                modal.find(".modal-body").text(message);
                modal.modal('show');
                modal.on('hidden.bs.modal', function(e) {
                    if (nextRoute) {
                        that.redirect(nextRoute)
                    }
                })
            },
            formatDate: function(dt) {
                try {
                    if (dt == undefined || dt == "") {
                        return ""
                    };
                    if (typeof dt == 'string' || dt instanceof String) {
                        dt = dt.split("-");
                        if (31 < parseInt(dt[0])) {
                            return dt[0] + "-" + dt[1] + "-" + dt[2]
                        } else {
                            return dt[2] + "-" + dt[1] + "-" + dt[0]
                        }
                    } else {
                        var y = dt.getFullYear();
                        var m = (dt.getMonth() > 8) ? (dt.getMonth() + 1) : ('0' + (dt.getMonth() + 1));
                        var d = (dt.getDate() > 9) ? (dt.getDate()) : ('0' + dt.getDate());
                        return y.toString() + "-" + m.toString() + "-" + d.toString()
                    }
                } catch (e) {
                    console.log(e.message);
                    return ""
                }
            },
            checkProfile: function(verb, path) {
                if (verb == "get") {
                    if (path.indexOf("#") != 0) {
                        path = path.substr(path.indexOf("#"))
                    };
                    path = path.replace(/(\d+)/g, "*");
                    return window.profile.indexOf(path) != -1
                };
                return true
            },
            getReadableSize: function(size) {
                var i = -1;
                var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
                do {
                    size = size / 1024;
                    i++
                } while (size > 1024);
                return Math.max(size, 0.1).toFixed(1) + byteUnits[i]
            }
        });
        this.before({
            except: {
                path: ['#/login', '#/login/post', '#/logout', '#/no_permitido', '#/contrato/cambiar', '#/descargarapk', '#/descargarapkbeta']
            }
        }, function() {
            if (!_isLogged) {
                that = this;
                $.ajax({
                    url: "rest/islogged",
                    type: 'GET',
                    async: false,
                    success: function(json) {
                        if (json.status) {
                            _isLogged = json.logged;
                            window.user_id = json.user_id;
                            window.user = json.user_name;
                            client = json.client;
                            contract = json.contrato;
                            sections = json.sections;
                            contracts = json.contracts;
                            window.config = json.config;
                            window.profile = json.profile;
                            window.user_level = json.user_level;
                            if (!_isLogged) {
                                app.setLocation('#/login');
                                return false
                            } else {
                                if (!that.checkProfile(that.verb, that.path)) {
                                    console.log(that.path + ": NO PERMITIDO");
                                    app.setLocation('#/no_permitido');
                                    return false
                                }
                            }
                        } else {
                            app.setLocation('#/login');
                            return false
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        app.setLocation('#/login')
                    }
                })
            } else {
                if (!this.checkProfile(this.verb, this.path)) {
                    this.redirect('#/no_permitido');
                    return false
                }
            }
        });
        this.after(function() {
            if (!this.app.getLocation().endsWith("#/login")) {
                context = this;
                setTimeout(function() {
                    if (!window.SyncIsRunning()) {
                        window.SyncStart(function(data) {
                            context.updateNotifications(data)
                        })
                    }
                }, 500)
            }
        });
        this.get('#/descargarapk', function() {
            this.partial('templates/descargarapk.hb')
        });
        this.get('#/descargarapkbeta', function() {
            this.partial('templates/descargarapkbeta.hb')
        });
        this.get('#/', function() {
            this.redirect('#/login')
        });
        this.get('#/login', function() {
            this.partial('templates/login.hb')
        });
        this.post('#/login/post', function(context) {
            if (this.params.user == "") {
                context.error = "Debe indicar usuario";
                context.partial('templates/login.hb')
            } else if (this.params.pass == "") {
                context.user = this.params.user;
                context.error = "Debe indicar clave";
                context.partial('templates/login.hb')
            } else {
                context.user = this.params.user;
                context.validating = true;
                context.partial('templates/login.hb');
                $.post("rest/login", {
                    "user": this.params.user,
                    "pass": this.params.pass
                }, function(json, textStatus) {
                    context.validating = false;
                    if (json.status) {
                        isLogged = true;
                        window.user_id = json.user_id;
                        window.user = json.user_name;
                        client = json.client;
                        sections = json.sections;
                        contract = json.contrato;
                        contracts = json.contracts;
                        window.config = json.config;
                        window.profile = json.profile;
                        window.user_level = json.user_level;
                        def_section = 0;
                        if (0 < json.sections.length) {
                            if (json.sections[def_section].sections) {
                                if ($.inArray(json.sections[def_section].sections[0].link, window.profile) < 0) {
                                    context.redirect(window.profile[0])
                                } else {
                                    context.redirect(json.sections[def_section].sections[0].link)
                                }
                            } else {
                                if ($.inArray(json.sections[def_section].link, window.profile) < 0) {
                                    context.redirect(window.profile[0])
                                } else {
                                    context.redirect(json.sections[def_section].link)
                                }
                            }
                        };
                        window.SyncStart(function(data) {
                            context.updateNotifications(data)
                        })
                    } else {
                        context.error = json.error;
                        context.partial('templates/login.hb')
                    }
                }, "json").fail(function(xhr, textStatus, errorThrown) {
                    console.log(xhr, textStatus, errorThrown);
                    context.validating = false;
                    context.error = "Error " + xhr.status + ": " + xhr.statusText;
                    context.partial('templates/login.hb')
                })
            }
        });
        this.get('#/movil_base', function(context) {
            this.showUI(context, 'movil/movil_template', {}, '')
        });
        this.get('#/usuario', function(context) {
            this.showMain(context, "usuario");
            this.showSection(context, "usuario", {
                usua_id: window.user_id,
                usua_nombre: window.user
            })
        });
        this.post('#/usuario', function(context) {
            $.post("rest/core/usuario/" + this.params.usua_id + "/cambio_clave", {
                "usua_password": this.params.usua_password,
                "usua_password_nuevo": this.params.usua_password_nuevo
            }, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Configuración de usuario", "Contraseña cambiada exitosamente")
                };
                if (2 == json.status) {
                    that.showError("Password no permitida", "La password hace referencia al negocio o a las actividades de la compañia. ")
                }
            }, 'json').fail(function() {
                that.showError("Configuración de usuario", "No se pudo guardar cambio de contraseña")
            })
        });
        this.get('#/test', function(context) {
            this.showUI(context, 'test', {}, 'rest/test', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/test', {})
                }, 8000000)
            })
        });
        this.get('#/mnt/reportes_mnt', function(context) {
            this.showUI(context, 'reportes_mnt', {}, '/rest/reportes_mnt', {}, function() {
                setTimeout(function() {}, 8000000)
            })
        });
        this.post('#/mnt/reportes_mnt', function(context) {
            return true
        });
        this.get('#/os/reportes_os', function(context) {
            this.showUI(context, 'reportes_os', {}, '/rest/reportes_os', {}, function() {
                setTimeout(function() {}, 8000000)
            })
        });
        this.post('#/os/reportes_os', function(context) {
            return true
        });
        this.get('#/dashboard', function(context) {
            if (1 == contract) {
                this.redirect('#/dashboard/os')
            } else {
                this.redirect('#/dashboard/mnt')
            }
        });
        this.get('#/dashboard/os', function(context) {
            var month = (new Date()).getMonth() + 1;
            var year = (new Date()).getFullYear();
            this.showUI(context, 'dashboard_os', {
                "month": month,
                "year": year
            }, 'rest/contrato/' + contract + '/dashboard/os/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/dashboard/os', {
                        "orse_fecha_mes": month,
                        "orse_fecha_anio": year,
                        "tipo_filtro": "mes"
                    })
                }, 200)
            })
        });
        this.post('#/dashboard/os', function(context) {
            var data = this.params.toHash();
            if (data.tipo_filtro == "mes") {
                if (data.orse_fecha_mes < 10) data.orse_fecha_mes = "0" + data.orse_fecha_mes;
                data.orse_fecha_inicio = data.orse_fecha_anio + "-" + data.orse_fecha_mes + "-01";
                var aux = new Date(parseInt(data.orse_fecha_anio), parseInt(data.orse_fecha_mes), 0);
                data.orse_fecha_termino = data.orse_fecha_anio + "-" + data.orse_fecha_mes + "-" + aux.getDate()
            };
            data.orse_fecha_inicio += " 00:00:00";
            data.orse_fecha_termino += " 23:59:59";
            delete data.tipo_filtro;
            delete data.orse_fecha_mes;
            delete data.orse_fecha_anio;
            this.updateElement($("#dashboard_os_graficos"), "dashboard_os_graficos", 'rest/contrato/' + contract + '/dashboard/os', data, {})
        });
        this.get('#/dashboard/mnt', function(context) {
            this.showUI(context, 'dashboard_mnt', {}, 'rest/contrato/' + contract + '/dashboard/mnt/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/dashboard/mnt', {})
                }, 200)
            })
        });
        this.post('#/dashboard/mnt', function(context) {
            var data = this.params.toHash();
            this.updateElement($("#dashboard_mnt_graficos"), "dashboard_mnt_graficos", 'rest/contrato/' + contract + '/dashboard/mnt', data, {})
        });
        this.get('#/indisponibilidad', function(context) {
            this.redirect('#/indisponibilidad/bandeja')
        });
        this.get('#/indisponibilidad/bandeja', function(context) {
            that = this;
            this.showUI(context, 'indi_bandeja', window.indisponibilidadBandejaFiltros, 'rest/contrato/' + contract + '/indisponibilidad/bandeja/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/indisponibilidad/bandeja/filtro/1', window.indisponibilidadBandejaFiltros);
                    $("#siom-form-indisponibilidad-bandeja").submit()
                }, 200)
            })
        });
        this.post('#/indisponibilidad/bandeja/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.indisponibilidadBandejaFiltros = data;
            this.updateElement($("#indi-lista"), "indi_bandeja_lista", "rest/contrato/" + contract + "/indisponibilidad/list/" + page, data)
        });
        this.post('#/indisponibilidad/crear', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]))
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            $.each(form.find("input:file"), function(i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0])
                }
            });
            url = 'rest/contrato/' + contract + '/indisponibilidad/add';
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Creando Indisponibilidad", "Indisponibilidad creada exitosamente INDI Nº " + json.indi_id + " y OS N° " + json.orse_id, "#/indisponibilidad/bandeja")
                    } else {
                        that.showError("Error creando indisponibilidad", json.error, "ruta:" + url, "#/indisponibilidad/bandeja")
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    that.showError("Error creando indisponibilidad", "No se pudo guardar Indisponibilidad (" + textStatus + ")", "ruta:" + url, "#/indisponibilidad/bandeja")
                }
            })
        });
        this.get('#/indisponibilidad/detalle/:indi_id', function(context) {
            this.showUI(context, 'indi_detalle', {}, 'rest/indisponibilidad/detalle/' + this.params['indi_id'])
        });
        this.get('#/indisponibilidad/crear', function(context) {
            if (!window.indiCrearFiltros) {
                window.indiCrearFiltros = {}
            };
            this.showUI(context, 'indi_crear', {}, 'rest/contrato/' + contract + '/indisponibilidad/add/filtros', {}, function() {}, 200)
        });
        this.get('#/qr', function(context) {
            this.redirect('#/qr/bandeja')
        });
        this.get('#/qr/bandeja', function(context) {
            that = this;
            this.showUI(context, 'qr_bandeja', window.qrBandejaFiltros, 'rest/qr/bandeja/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/qr/bandeja/filtro/1', window.qrBandejaFiltros);
                    $("#siom-form-qr-bandeja").submit()
                }, 200)
            })
        });
        this.post('#/qr/bandeja/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.qrBandejaFiltros = data;
            this.updateElement($("#qr-bandeja-lista"), "qr_bandeja_lista", "rest/qr/list/" + page, data)
        });
        this.get('#/qr/detalle/:rlco_codigo_descifrado', function(context) {
            this.showUI(context, 'qr_detalle', {}, 'rest/qr/detalle/' + this.params['rlco_codigo_descifrado'])
        });
        this.get('#/defaMovil', function(context) {
            this.redirect('#/defaMovil/bandeja')
        });
        this.get('#/defaMovil/bandeja', function(context) {
            that = this;
            this.showUI(context, 'defaMovil_bandeja', window.defaMovilBandejaFiltros, 'rest/defaMovil/bandeja/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/defaMovil/bandeja/filtro/1', window.defaMovilBandejaFiltros);
                    $("#siom-form-defaMovil-bandeja").submit()
                }, 200)
            })
        });
        this.post('#/defaMovil/bandeja/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.defaMovilBandejaFiltros = data;
            this.updateElement($("#defaMovil-bandeja-lista"), "defaMovil_bandeja_lista", "rest/defaMovil/list/" + page, data)
        });
        this.get('#/defaMovil/detalle/:defa_descripcion', function(context) {
            this.showUI(context, 'defaMovil_detalle', {}, 'rest/defaMovil/detalle/' + this.params['defa_descripcion'])
        });
        this.get('#/tracking', function(context) {
            if (window.trackingFiltros) {
                dt = window.trackingFiltros.ustr_fecha_fin.split("-");
                if (parseInt(dt[0]) <= 31) {
                    window.trackingFiltros.ustr_fecha_fin = dt[0] + "-" + dt[1] + "-" + dt[2]
                } else {
                    window.trackingFiltros.ustr_fecha_fin = dt[2] + "-" + dt[1] + "-" + dt[0]
                };
                dt = window.trackingFiltros.ustr_fecha_inicio.split("-");
                if (parseInt(dt[0]) <= 31) {
                    window.trackingFiltros.ustr_fecha_inicio = dt[0] + "-" + dt[1] + "-" + dt[2]
                } else {
                    window.trackingFiltros.ustr_fecha_inicio = dt[2] + "-" + dt[1] + "-" + dt[0]
                }
            } else {
                window.trackingFiltros = {};
                now = new Date();
                d = now.getDate();
                m = now.getMonth() + 1;
                y = now.getFullYear();
                if (d < 10) {
                    d = "0" + d
                };
                if (m < 10) {
                    m = "0" + m
                };
                window.trackingFiltros.ustr_fecha_fin = d + "-" + m + "-" + y;
                window.trackingFiltros.ustr_fecha_inicio = d + "-" + m + "-" + y;
                window.trackingFiltros.ustr_usuario = "0"
            };
            this.showUI(context, 'tracking', window.trackingFiltros, 'rest/contrato/' + contract + '/tracking/filtros', {}, function() {
                setTimeout(function() {}, 200)
            })
        });
        this.post('#/tracking/filtro', function(context) {
            data = this.params.toHash();
            window.trackingFiltros = data;
            data.ustr_fecha_inicio = this.formatDate(data.ustr_fecha_inicio.trim());
            data.ustr_fecha_fin = this.formatDate(data.ustr_fecha_fin.trim());
            url = 'rest/contrato/' + contract + '/tracking/data';
            $.get(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error consultando datos de tracking", json.error, "ruta:" + url)
                } else {
                    window.updateTracking(json)
                }
            }).fail(function() {
                that.showError("Error consultando datos de tracking", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/emplazamientos', function(context) {
            if (!window.emplazamientosFiltros) {
                window.emplazamientosFiltros = {
                    tipo: "mapa"
                }
            };
            this.showUI(context, 'emplazamientos', {}, 'rest/core/contrato/' + contract + '/emplazamiento/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/emplazamientos/filtro/1', window.emplazamientosFiltros)
                }, 200)
            })
        });
        this.post('#/emplazamientos/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            options = {};
            options.tipo = data.tipo;
            if (null == options.tipo) {
                options.tipo = "mapa"
            };
            delete data.splat;
            delete data.page;
            delete data.tipo;
            this.updateElement($("#emplazamientos-estados"), "emplazamientos_estados", "rest/core/contrato/" + contract + "/emplazamiento/list?empl_estado=ACTIVO", data, options)
        });
        this.get('#/emplazamientos/detalle/:empl_id', function(context) {
            this.showUI(context, 'emplazamientos_detalle', {}, 'rest/core/contrato/' + contract + '/emplazamiento/detalle/' + this.params['empl_id'])
        });
        this.get('#/docs', function(context) {
            this.showUI(context, 'docs', {}, 'rest/contrato/' + contract + '/docs')
        });
        this.get('#/os', function(context) {
            this.redirect('#/os/bandeja')
        });
        this.get('#/os/bandeja', function(context) {
            that = this;
            if (window.osBandejaFiltros) {
                window.osBandejaFiltros.orse_fecha_solicitud_inicio = this.formatDate(window.osBandejaFiltros.orse_fecha_solicitud_inicio);
                window.osBandejaFiltros.orse_fecha_solicitud_termino = this.formatDate(window.osBandejaFiltros.orse_fecha_solicitud_termino)
            } else {
                window.osBandejaFiltros = {}
            };
            this.showUI(context, 'os_bandeja', window.osBandejaFiltros, 'rest/contrato/' + contract + '/os/bandeja/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/os/bandeja/filtro/1', window.osBandejaFiltros);
                    $("#siom-form-os-bandeja").submit()
                }, 200)
            })
        });
        this.post('#/os/bandeja/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.osBandejaFiltros = data;
            if (data.orse_id && data.orse_id != "") {
                data = {
                    orse_id: data.orse_id
                }
            } else {
                data.orse_fecha_solicitud_inicio = this.formatDate(data.orse_fecha_solicitud_inicio);
                data.orse_fecha_solicitud_termino = this.formatDate(data.orse_fecha_solicitud_termino)
            };
            this.updateElement($("#os-bandeja-lista"), "os_bandeja_lista", "rest/contrato/" + contract + "/os/list/" + page, data)
        });
        this.get('#/os/crear', function(context) {
            if (!window.osCrearFiltros) {
                window.osCrearFiltros = {}
            };
            this.showUI(context, 'os_crear', {
                max_words: window.config.os.minDescription,
                max_file_upload: this.getReadableSize(window.config.max_file_upload)
            }, 'rest/contrato/' + contract + '/os/add/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/os/crear/filtro/1', window.osCrearFiltros)
                }, 200)
            })
        });
        this.post('#/os/crear/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#os-crear-lista"), "os_crear_lista", "rest/core/contrato/" + contract + "/emplazamiento/list/" + page + "?empl_estado=ACTIVO", data)
        });
        this.post('#/os/crear', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]))
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            $.each(form.find("input:file"), function(i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0])
                }
            });
            url = 'rest/contrato/' + contract + '/os/add';
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Creando orden de servicio", "Orden de servicio creada exitosamente (OS Nº " + json.orse_id + ")", "#/os/bandeja")
                    } else {
                        that.showError("Error creando orden de servicio", json.error, "ruta:" + url, "#/os/bandeja")
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    that.showError("Error creando orden de servicio", "No se pudo guardar O.S. (" + textStatus + ")", "ruta:" + url, "#/os/bandeja")
                }
            })
        });
        this.get('#/os/editar/:orse_id', function(context) {
            that = this;
            data = this.params.toHash();
            orse_id = data['orse_id'];
            espe_id = data['espe_id'];
            this.showUI(context, 'os_editar', {
                max_words: window.config.os.minDescription,
                max_file_upload: this.getReadableSize(window.config.max_file_upload)
            }, 'rest/contrato/' + contract + '/os/get/' + orse_id)
        });
        this.post('#/os/editar/:orse_id', function(context) {
            that = this;
            data = this.params.toHash();
            orse_id = this.params['orse_id'];
            sube_id = this.params['sube_id'];
            if ("" == sube_id || null == sube_id) {
                alert("Por favor ingresar una sub especialidad ");
                return
            };
            confirm("¿Esta seguro de guardar los cambios?", function(status) {
                if (status == true) {
                    url = 'rest/contrato/' + contract + '/os/upd/' + orse_id;
                    $.post(url, data, function(json, textStatus) {
                        if (0 == json.status) {
                            that.showError("Error editando OS", json.error, "ruta:" + url)
                        } else {
                            that.showSuccess("Editando OS", "OS editada exitosamente", "#/os/bandeja");
                            return true
                        }
                    }).fail(function() {
                        that.showError("Error Editando item", "Error en POST", "ruta:" + url)
                    })
                } else {
                    that.redirect("#/os/bandeja")
                }
            })
        });
        this.get('#/os/adjuntar/:orse_id', function(context) {
            this.showUI(context, 'os_adjuntar', {
                max_file_upload: this.getReadableSize(window.config.max_file_upload)
            }, 'rest/contrato/' + contract + '/os/adjuntar/' + this.params['orse_id'])
        });
        this.post('#/os/adjuntar/:orse_id', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]))
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            $.each(form.find("input:file"), function(i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0])
                }
            });
            url = 'rest/contrato/' + contract + '/os/adjuntar/' + this.params['orse_id'];
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Adjuntando archivo(s) a orden de servicio", "Archivo(s) agregado(s) exitosamente a OS", "#/os/bandeja")
                    } else {
                        that.showError("Error adjuntando archivo(s) a orden de servicio", json.error, "ruta:" + url, "#/os/bandeja")
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    that.showError("Adjuntando archivo(s) a orden de servicio", "No se pudo guardar OS (" + textStatus + ")", "ruta:" + url, "#/os/bandeja")
                }
            })
        });
        this.get('#/os/detalle/:orse_id', function(context) {
            this.showUI(context, 'os_detalle', {}, 'rest/contrato/' + contract + '/os/detalle/' + this.params['orse_id'])
        });
        this.get('#/os/asignacion/:orse_id', function(context) {
            that = this;
            this.showUI(context, 'os_asignacion', {}, 'rest/contrato/' + contract + '/os/asignacion/' + this.params['orse_id'], {}, function(data) {
                if (null != data.asignacion) {
                    if (null != data.asignacion.emvi_fecha_ingreso && 'ACTIVO' == data.asignacion.emvi_estado) {
                        that.showError("OS Asignación", "No es posible reasignar OS cuando está en ejecución", "ruta:" + url, "#/os/bandeja")
                    } else {
                        confirm("OS ya fue asignada a <b>" + data.asignacion.usua_nombre + "</b> el " + data.asignacion.oras_fecha_asignacion + "<br>¿Desea cancelar asignación y volver a asignar OS?", function(status) {
                            if (status) {
                                url = 'rest/contrato/' + contract + '/os/asignacion/' + that.params['orse_id'] + '/cancelar/' + data.asignacion.oras_id;
                                $.post(url, data, function(json, textStatus) {
                                    if (!json.status) {
                                        that.showError("Error cancelando asignación", json.error, "ruta:" + url, "#/os/bandeja")
                                    }
                                }, 'json').fail(function() {
                                    that.showError("Error cancelando asignación", "No se pudo cancelar asignación O.S.", "ruta:" + url, "#/os/bandeja")
                                })
                            } else {
                                that.redirect("#/os/bandeja")
                            }
                        })
                    }
                }
            })
        });
        this.post('#/os/asignacion/:orse_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/asignacion/' + orse_id;
            $.post(url, data, function(json, textStatus) {
                form = $(context.target);
                if (json.status) {
                    form.find('.siom-form-actions').hide();
                    form.find(':input').prop('disabled', true);
                    that.showSuccess("Agregando asignación", "Orden de servicio asignada exitosamente", "#/os/bandeja")
                } else {
                    form.find(':submit').button('reset');
                    that.showError("Error agregando asignación", json.error, "ruta:" + url, "#/os/bandeja")
                }
            }, 'json').fail(function() {
                form = $(context.target);
                form.find(':submit').button('reset');
                that.showError("Error agregando asignación", "No se pudo asignar O.S.", "ruta:" + url, "#/os/bandeja")
            })
        });
        this.post('#/os/devolver/:orse_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/devolver/' + orse_id;
            confirm("¿Desea devolver la Orden de Servicio?", function(status) {
                if (status) {
                    $.post(url, data, function(json, textStatus) {
                        form = $(context.target);
                        if (json.status) {
                            form.find('.siom-form-actions').hide();
                            form.find(':input').prop('disabled', true);
                            that.showSuccess("Devolviendo OS", "Orden de servicio devuelta exitosamente", "#/os/bandeja")
                        } else {
                            form.find(':submit').button('reset');
                            that.showError("Error devolviendo OS", json.error, "ruta:" + url, "#/os/bandeja")
                        }
                    }, 'json').fail(function() {
                        form = $(context.target);
                        form.find(':submit').button('reset');
                        that.showError("Error devolviendo OS", "No se pudo devolver O.S.", "ruta:" + url, "#/os/bandeja")
                    })
                } else {
                    that.redirect("#/os/bandeja")
                }
            })
        });
        this.get('#/os/presupuesto/:orse_id', function(context) {
            orse_id = this.params['orse_id'];
            this.showUI(context, 'os_presupuesto', {
                orse_id: orse_id
            }, 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id)
        });
        this.post('#/os/presupuesto/:orse_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            data = this.params.toHash();
            delete data.splat;
            confirm("¿Agregar presupuesto?", function(status) {
                if (status) {
                    url = 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id;
                    $.post(url, data, function(json, textStatus) {
                        if (json.status) {
                            that.showSuccess("Agregando presupuesto", "Presupuesto guardado exitosamente", "#/os/bandeja")
                        } else {
                            that.showError("Error agregando presupuesto", json.error, "ruta:" + url, "#/os/bandeja")
                        }
                    }, 'json').fail(function() {
                        that.showError("Error agregando presupuesto", "No se pudo guardar presupuesto", "ruta:" + url, "#/os/bandeja")
                    })
                } else {
                    window.location.reload()
                }
            })
        });
        this.get('#/os/presupuesto/:orse_id/modificar', function(context) {
            orse_id = this.params['orse_id'];
            this.showUI(context, 'os_modificar_presupuesto', {
                orse_id: orse_id
            }, 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id + '/modificar')
        });
        this.post('#/os/presupuesto/:orse_id/modificar/:pres_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            pres_id = this.params['pres_id'];
            data = this.params.toHash();
            delete data.splat;
            confirm("¿Agregar presupuesto?", function(status) {
                if (status) {
                    url = 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id + "/modificar/" + pres_id;
                    $.post(url, data, function(json, textStatus) {
                        if (json.status) {
                            that.showSuccess("Modificando presupuesto", "Modificación de presupuesto guardada exitosamente", "#/os/bandeja")
                        } else {
                            that.showError("Error validando presupuesto", json.error, "ruta:" + url, "#/os/bandeja")
                        }
                    }, 'json').fail(function() {
                        that.showError("Error modificando presupuesto", "No se pudo guardar presupuesto", "ruta:" + url, "#/os/bandeja")
                    })
                } else {
                    window.location.reload()
                }
            })
        });
        this.get('#/os/presupuesto/:orse_id/validar/:pres_id', function(context) {
            orse_id = this.params['orse_id'];
            pres_id = this.params['pres_id'];
            this.showUI(context, 'os_validar_presupuesto', {
                orse_id: orse_id,
                pres_id: pres_id
            }, 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id + '/validar/' + pres_id)
        });
        this.get('#/os/presupuesto/:orse_id/ver/:pres_id', function(context) {
            orse_id = this.params['orse_id'];
            pres_id = this.params['pres_id'];
            this.showUI(context, 'os_ver_presupuesto', {
                orse_id: orse_id,
                pres_id: pres_id
            }, 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id + '/ver/' + pres_id)
        });
        this.post('#/os/presupuesto/:orse_id/validar/:pres_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            pres_id = this.params['pres_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/presupuesto/' + orse_id + "/validar/" + pres_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando presupuesto", "Validación de presupuesto guardada exitosamente", "#/os/bandeja")
                } else {
                    that.showError("Error validando presupuesto", json.error, "ruta:" + url, "#/os/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando presupuesto", "No se pudo guardar presupuesto", "ruta:" + url, "#/os/bandeja")
            })
        });
        this.get('#/os/visita/:orse_id', function(context) {
            orse_id = this.params['orse_id'];
            this.showUI(context, 'os_visita', {
                orse_id: orse_id
            }, 'rest/contrato/' + contract + '/os/visita/' + this.params['orse_id'])
        });
        this.post('#/os/visita/:orse_id', function(context) {
            this.showSuccess("Agregando informe", "Informe agregado exitosamente", "#/os/bandeja")
        });
        this.get('#/os/informe/:orse_id', function(context) {
            this.showUI(context, 'os_informe', {}, 'rest/contrato/' + contract + '/os/informe/' + this.params['orse_id'])
        });
        this.post('#/os/informe/:orse_id', function(context) {
            console.log(context)
        });
        this.get('#/os/informe/:orse_id/ver/:info_id', function(context) {
            orse_id = this.params['orse_id'];
            info_id = this.params['info_id'];
            this.showUI(context, 'os_ver_informe', {
                orse_id: orse_id,
                info_id: info_id
            }, 'rest/contrato/' + contract + '/os/informe/' + orse_id + '/ver/' + info_id)
        });
        this.get('#/os/informe/:orse_id/validar/:info_id', function(context) {
            orse_id = this.params['orse_id'];
            info_id = this.params['info_id'];
            this.showUI(context, 'os_validar_informe', {
                orse_id: orse_id,
                info_id: info_id
            }, 'rest/contrato/' + contract + '/os/informe/' + orse_id + '/validar/' + info_id)
        });
        this.post('#/os/informe/:orse_id/validar/:info_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            info_id = this.params['info_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/informe/' + orse_id + "/validar/" + info_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando informe", "Validación de informe guardada exitosamente", "#/os/bandeja")
                } else {
                    that.showError("Error validando informe", json.error, "ruta:" + url, "#/os/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando informe", "No se pudo guardar informe", "ruta:" + url, "#/os/bandeja")
            })
        });
        this.get('#/os/cerrar/:orse_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            url = 'rest/contrato/' + contract + '/os/cerrar/' + orse_id;
            $.get(url, {}, function(json, textStatus) {
                if (json.status) {
                    if ($.inArray(json.os.orse_tipo, ["OSEU", "OSEN"]) != -1) {
                        if (0 < json.presupuesto.length && 0 < json.informe.length) {
                            that.showMain(context, 'os_cerrar');
                            that.showSection(context, 'os_cerrar', json, null)
                        } else {
                            that.redirect('#/os/bandeja')
                        }
                    } else {
                        if (0 < json.informe.length) {
                            that.showMain(context, 'os_cerrar');
                            that.showSection(context, 'os_cerrar', json, null)
                        } else {
                            that.redirect('#/os/bandeja')
                        }
                    }
                } else {
                    that.showError("Error validando OS", json.error, "ruta:" + url, "#/os/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando OS", "No se pudo consultar por estado de OS", "ruta:" + url, "#/os/bandeja")
            })
        });
        this.post('#/os/cerrar/:orse_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            orse_estado = this.params['orse_estado'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/cerrar/' + orse_id;
            $.post(url, data, function(json, textStatus) {
                if ("CANCELADA" == orse_estado) {
                    that.redirect("#/os/bandeja")
                } else {
                    if (json.status) {
                        that.showSuccess("Validando OS", "Validación de OS guardada exitosamente", "#/os/bandeja")
                    } else {
                        that.showError("Error validando OS", json.error, "ruta:" + url, "#/os/bandeja")
                    }
                }
            }, 'json').fail(function() {
                that.showError("Error validando OS", "No se pudo cerrar OS", "ruta:" + url, "#/os/bandeja")
            })
        });
        this.get('#/os/solicitud/cambio/:orse_id/validar/:tare_id', function(context) {
            orse_id = this.params['orse_id'];
            tare_id = this.params['tare_id'];
            this.showUI(context, 'os_solicitud_validar_cambio', {
                orse_id: orse_id,
                tare_id: tare_id
            }, 'rest/contrato/' + contract + '/os/solicitud/cambio/' + orse_id + '/validar/' + tare_id)
        });
        this.post('#/os/solicitud/cambio/:orse_id/validar/:tare_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            tare_id = this.params['tare_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/solicitud/cambio/' + orse_id + '/validar/' + tare_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando solicitud de cambio OS", "Validación de solicitud de cambio guardada exitosamente", "#/os/bandeja")
                } else {
                    that.showError("Error solicitud de cambio OS", json.error, "ruta:" + url)
                }
            }, 'json').fail(function() {
                that.showError("Error solicitud de cambio OS", "No se pudo guardar respuesta de solicitud", "ruta:" + url)
            })
        });
        this.get('#/os/solicitud/informe/:orse_id/validar/:tare_id', function(context) {
            orse_id = this.params['orse_id'];
            tare_id = this.params['tare_id'];
            this.showUI(context, 'os_solicitud_validar_informe', {
                orse_id: orse_id,
                tare_id: tare_id
            }, 'rest/contrato/' + contract + '/os/solicitud/informe/' + orse_id + '/validar/' + tare_id)
        });
        this.post('#/os/solicitud/informe/:orse_id/validar/:tare_id', function(context) {
            that = this;
            orse_id = this.params['orse_id'];
            tare_id = this.params['tare_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/os/solicitud/informe/' + orse_id + '/validar/' + tare_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando solicitud de informe OS", "Validación de solicitud de informe guardada exitosamente", "#/os/bandeja")
                } else {
                    that.showError("Error solicitud de informe OS", json.error, "ruta:" + url)
                }
            }, 'json').fail(function() {
                that.showError("Error solicitud de informe OS", "No se pudo respuesta de solicitud", "ruta:" + url)
            })
        });
        this.get('#/mnt', function(context) {
            this.redirect('#/mnt/bandeja')
        });
        this.get('#/mnt/bandeja', function(context) {
            that = this;
            if (window.mntBandejaFiltros) {
                window.mntBandejaFiltros.mant_fecha_programada_inicio = this.formatDate(window.mntBandejaFiltros.mant_fecha_programada_inicio);
                window.mntBandejaFiltros.mant_fecha_programada_termino = this.formatDate(window.mntBandejaFiltros.mant_fecha_programada_termino)
            } else {
                window.mntBandejaFiltros = {}
            };
            this.showUI(context, 'mnt_bandeja', window.mntBandejaFiltros, 'rest/contrato/' + contract + '/mnt/bandeja/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/mnt/bandeja/filtro/1', window.mntBandejaFiltros);
                    $("#siom-form-mnt-bandeja").submit()
                }, 200)
            })
        });
        this.post('#/mnt/bandeja/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.mntBandejaFiltros = data;
            if (data.mant_id && "" != data.mant_id) {
                data = {
                    mant_id: data.mant_id
                }
            } else {
                if ("" != data.mant_fecha_programada_inicio) {
                    data.mant_fecha_programada_inicio = this.formatDate(data.mant_fecha_programada_inicio)
                };
                if ("" != data.mant_fecha_programada_termino) {
                    data.mant_fecha_programada_termino = this.formatDate(data.mant_fecha_programada_termino)
                }
            };
            this.updateElement($("#mnt-bandeja-lista"), "mnt_bandeja_lista", "rest/contrato/" + contract + "/mnt/list/" + page, data)
        });
        this.get('#/mnt/crear', function(context) {
            if (!window.mntCrearFiltros) {
                window.mntCrearFiltros = {}
            };
            this.showUI(context, 'mnt_crear', {
                max_words: window.config.mnt.minDescription
            }, 'rest/contrato/' + contract + '/mnt/add/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/mnt/crear/filtro/1', window.mntCrearFiltros)
                }, 200)
            })
        });
        this.post('#/mnt/crear/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#mnt-crear-lista"), "mnt_crear_lista", "rest/core/contrato/" + contract + "/emplazamiento/list/" + page + "?empl_estado=ACTIVO", data)
        });
        this.post('#/mnt/crear', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]))
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            $.each(form.find("input:file"), function(i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0])
                }
            });
            url = 'rest/contrato/' + contract + '/mnt/add';
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Creando MPS", "Mantenimiento preventivo a solicitud creado exitosamente (MNT Nº " + json.data[0].id + ")", "#/mnt/bandeja")
                    } else {
                        that.showError("Error creando MPS", json.error, "ruta:" + url, "#/mnt/bandeja")
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    that.showError("Error creando MPS", "No se pudo guardar MNT (" + textStatus + ")", "ruta:" + url, "#/mnt/bandeja")
                }
            })
        });
        this.get('#/mnt/editar/:mant_id', function(context) {
            data = this.params.toHash();
            mant_id = data['mant_id'];
            this.showUI(context, 'mnt_editar', {
                max_words: window.config.os.minDescription,
                max_file_upload: this.getReadableSize(window.config.max_file_upload)
            }, 'rest/contrato/' + contract + '/mnt/get/' + mant_id)
        });
        this.post('#/mnt/editar/:mant_id', function(context) {
            that = this;
            data = this.params.toHash();
            mant_id = this.params['mant_id'];
            url = 'rest/contrato/' + contract + '/mnt/upd/' + mant_id;
            $.post(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error editando MNT", json.error, "ruta:" + url)
                } else {
                    that.showSuccess("Editando MNT", "MNT editado exitosamente", "#/mnt/bandeja")
                }
            }).fail(function() {
                that.showError("Error Editando item", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/mnt/adjuntar/:mant_id', function(context) {
            this.showUI(context, 'mnt_adjuntar', {
                max_file_upload: this.getReadableSize(window.config.max_file_upload)
            }, 'rest/contrato/' + contract + '/mnt/adjuntar/' + this.params['mant_id'])
        });
        this.post('#/mnt/adjuntar/:mant_id', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]))
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            $.each(form.find("input:file"), function(i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0])
                }
            });
            url = 'rest/contrato/' + contract + '/mnt/adjuntar/' + this.params['mant_id'];
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Adjuntando archivo(s) a mantenimiento", "Archivo(s) agregado(s) exitosamente a mantenimiento", "#/mnt/bandeja")
                    } else {
                        that.showError("Error adjuntando archivo(s) a mantenimiento", json.error, "ruta:" + url, "#/mnt/bandeja")
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    that.showError("Adjuntando archivo(s) a mantenimiento", "No se pudo guardar mantenimiento (" + textStatus + ")", "ruta:" + url, "#/mnt/bandeja")
                }
            })
        });
        this.get('#/mnt/asignacion/:mant_id', function(context) {
            that = this;
            this.showUI(context, 'mnt_asignacion', {}, 'rest/contrato/' + contract + '/mnt/asignacion/' + this.params['mant_id'], {}, function(data) {
                if (null != data.asignacion) {
                    if (null != data.asignacion.emvi_fecha_ingreso) {
                        that.showError("MNT Asignación", "No es posible reasignar MNT cuando está en ejecución", "ruta:" + url, "#/mnt/bandeja")
                    } else {
                        confirm("MNT ya fue asignada a <b>" + data.asignacion.usua_nombre + "</b> el " + data.asignacion.maas_fecha_asignacion + "<br>¿Desea cancelar asignación y volver a asignar MNT?", function(status) {
                            if (status) {
                                url = 'rest/contrato/' + contract + '/mnt/asignacion/' + that.params['mant_id'] + '/cancelar/' + data.asignacion.maas_id;
                                $.post(url, data, function(json, textStatus) {
                                    if (!json.status) {
                                        that.showError("Error cancelando asignación", json.error, "ruta:" + url, "#/mnt/bandeja")
                                    }
                                }, 'json').fail(function() {
                                    that.showError("Error cancelando asignación", "No se pudo cancelar asignación MNT", "ruta:" + url, "#/mnt/bandeja")
                                })
                            } else {
                                that.redirect("#/mnt/bandeja")
                            }
                        })
                    }
                }
            })
        });
        this.post('#/mnt/asignacion/:mant_id', function(context) {
            that = this;
            mant_id = this.params['mant_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/mnt/asignacion/' + mant_id;
            $.post(url, data, function(json, textStatus) {
                form = $(context.target);
                if (json.status) {
                    form.find('.siom-form-actions').hide();
                    form.find(':input').prop('disabled', true);
                    that.showSuccess("Agregando asignación", "Mantención asignada exitosamente", "#/mnt/bandeja")
                } else {
                    form.find(':submit').button('reset');
                    that.showError("Error agregando asignación", json.error, "ruta:" + url, "#/mnt/bandeja")
                }
            }, 'json').fail(function() {
                form = $(context.target);
                form.find(':submit').button('reset');
                that.showError("Error agregando asignación", "No se pudo asignar MNT", "ruta:" + url, "#/mnt/bandeja")
            })
        });
        this.get('#/mnt/detalle/:mant_id', function(context) {
            this.showUI(context, 'mnt_detalle', {}, 'rest/contrato/' + contract + '/mnt/detalle/' + this.params['mant_id'])
        });
        this.get('#/mnt/informe/:mant_id', function(context) {
            this.showUI(context, 'mnt_informe', {}, 'rest/contrato/' + contract + '/mnt/informe/' + this.params['mant_id'])
        });
        this.get('#/mnt/informe/:mant_id/ver/:info_id', function(context) {
            mant_id = this.params['mant_id'];
            info_id = this.params['info_id'];
            this.showUI(context, 'mnt_ver_informe', {
                mant_id: mant_id,
                info_id: info_id
            }, 'rest/contrato/' + contract + '/mnt/informe/' + mant_id + '/ver/' + info_id)
        });
        this.get('#/mnt/informe/:mant_id/validar/:info_id', function(context) {
            mant_id = this.params['mant_id'];
            info_id = this.params['info_id'];
            this.showUI(context, 'mnt_validar_informe', {
                mant_id: mant_id,
                info_id: info_id
            }, 'rest/contrato/' + contract + '/mnt/informe/' + mant_id + '/validar/' + info_id)
        });
        this.post('#/mnt/informe/:mant_id/validar/:info_id', function(context) {
            that = this;
            mant_id = this.params['mant_id'];
            info_id = this.params['info_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/mnt/informe/' + mant_id + "/validar/" + info_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando informe", "Validación de informe guardada exitosamente", "#/mnt/bandeja")
                } else {
                    that.showError("Error validando informe", json.error, "ruta:" + url, "#/mnt/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando informe", "No se pudo guardar informe", "ruta:" + url, "#/mnt/bandeja")
            })
        });
        this.get('#/mnt/visita/:mant_id', function(context) {
            mant_id = this.params['mant_id'];
            this.showUI(context, 'mnt_visita', {
                mant_id: mant_id
            }, 'rest/contrato/' + contract + '/mnt/visita/' + mant_id)
        });
        this.post('#/mnt/visita/:mant_id', function(context) {
            alert("GUARDAR En desarrollo")
        });
        this.get('#/mnt/solicitud/informe/:mant_id/validar/:tare_id', function(context) {
            mant_id = this.params['mant_id'];
            tare_id = this.params['tare_id'];
            this.showUI(context, 'mnt_solicitud_validar_informe', {
                mant_id: mant_id,
                tare_id: tare_id
            }, 'rest/contrato/' + contract + '/mnt/solicitud/informe/' + mant_id + '/validar/' + tare_id)
        });
        this.post('#/mnt/solicitud/informe/:mant_id/validar/:tare_id', function(context) {
            that = this;
            mant_id = this.params['mant_id'];
            tare_id = this.params['tare_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/mnt/solicitud/informe/' + mant_id + '/validar/' + tare_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando solicitud de informe MNT", "Validación de solicitud de informe guardada exitosamente", "#/mnt/bandeja")
                } else {
                    that.showError("Error solicitud de informe MNT", json.error, "ruta:" + url)
                }
            }, 'json').fail(function() {
                that.showError("Error solicitud de informe MNT", "No se pudo guardar respuesta de solicitud", "ruta:" + url)
            })
        });
        this.get('#/mnt/solicitud/cambio_fecha_programada/:mant_id/validar/:tare_id', function(context) {
            mant_id = this.params['mant_id'];
            tare_id = this.params['tare_id'];
            this.showUI(context, 'mnt_solicitud_validar_cambio_fecha_programada', {
                mant_id: mant_id,
                tare_id: tare_id
            }, 'rest/contrato/' + contract + '/mnt/solicitud/cambio_fecha_programada/' + mant_id + '/validar/' + tare_id)
        });
        this.post('#/mnt/solicitud/cambio_fecha_programada/:mant_id/validar/:tare_id', function(context) {
            that = this;
            mant_id = this.params['mant_id'];
            tare_id = this.params['tare_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/mnt/solicitud/cambio_fecha_programada/' + mant_id + '/validar/' + tare_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando solicitud de cambio de fecha programada MNT", "Validación de solicitud de cambio de fecha programada guardada exitosamente", "#/mnt/bandeja")
                } else {
                    that.showError("Error solicitud de informe MNT", json.error, "ruta:" + url)
                }
            }, 'json').fail(function() {
                that.showError("Error solicitud de cambio de fecha programada MNT", "No se pudo guardar respuesta de solicitud", "ruta:" + url)
            })
        });
        this.get('#/mnt/cerrar/:mant_id', function(context) {
            that = this;
            mant_id = this.params['mant_id'];
            url = 'rest/contrato/' + contract + '/mnt/cerrar/' + mant_id;
            $.get(url, {}, function(json, textStatus) {
                if (json.status) {
                    if (0 < json.informe.length) {
                        that.showMain(context, 'mnt_cerrar');
                        that.showSection(context, 'mnt_cerrar', json, null)
                    } else {
                        that.redirect('#/mnt/bandeja')
                    }
                } else {
                    that.showError("Error validando MNT", json.error, "ruta:" + url, "#/mnt/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando MNT", "No se pudo consultar por estado de MNT", "ruta:" + url, "#/mnt/bandeja")
            })
        });
        this.post('#/mnt/cerrar/:mant_id', function(context) {
            that = this;
            mant_id = this.params['mant_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/mnt/cerrar/' + mant_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando MNT", "Validación de MNT guardada exitosamente", "#/mnt/bandeja")
                } else {
                    that.showError("Error validando MNT", json.error, "ruta:" + url, "#/mnt/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando MNT", "No se pudo cerrar MNT", "ruta:" + url, "#/mnt/bandeja")
            })
        });
        this.get('#/inve', function(context) {
            this.redirect('#/inve/emplazamiento')
        });
        this.get('#/inve/emplazamiento', function(context) {
            that = this;
            this.showUI(context, 'inve_emplazamiento', {}, 'rest/core/contrato/' + contract + '/emplazamiento/filtros', {}, function() {
                setTimeout(function(context) {
                    window.app.runRoute('post', '#/inve/emplazamiento/filtro/1', window.inveBandejaFiltros)
                })
            })
        });
        this.post('#/inve/emplazamiento/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.page;
            delete data.splat;
            this.updateElement($("#inve-emplazamiento-lista"), "inve_emplazamiento_lista", "rest/core/contrato/" + contract + "/emplazamiento/list/" + page + "?empl_estado=ACTIVO", data)
        });
        this.get('#/inve/emplazamiento/:empl_id/items', function(context) {
            that = this;
            empl_id = this.params['empl_id'];
            this.showUI(context, 'inve_emplazamiento_item', {
                empl_id: empl_id
            }, 'rest/contrato/' + contract + '/inve/emplazamiento/' + empl_id + '/items/filtro/', {}, function() {
                setTimeout(function(context) {
                    window.app.runRoute('post', '#/inve/emplazamiento/' + empl_id + '/items/filtro/1', window.inveBandejaFiltros)
                })
            })
        });
        this.post('#/inve/emplazamiento/item/guardar', function(context) {
            that = this;
            data = this.params.toHash();
            empl_id = data['empl_id'];
            url = 'rest/contrato/' + contract + '/inve/emplazamiento/' + empl_id + '/item/add';
            $.post(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error guardando Item", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Guardando Item", "Item guardado exitosamente", "#/inve/emplazamiento/" + empl_id + "/items")
                }
            }).fail(function() {
                that.showError("Error guardando item", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/inve/emplazamiento/:empl_id/item/:inel_id/editar', function(context) {
            empl_id = this.params['empl_id'];
            inel_id = this.params['inel_id'];
            this.showUI(context, 'inve_editar', {}, 'rest/contrato/' + contract + '/inve/emplazamiento/' + empl_id + '/item/' + inel_id + '/editar')
        });
        this.post('#/inve/emplazamiento/item/update', function(context) {
            data = this.params.toHash();
            inel_id = this.params['inel_id'];
            empl_id = this.params['empl_id'];
            inel_codigo = this.params['inel_codigo'];
            url = 'rest/contrato/' + contract + '/inve/emplazamiento/item/' + inel_id + '/editar/update';
            $.post(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error Editando Item", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Editando Item", "Item editado exitosamente", "#/inve/emplazamiento/" + empl_id + "/items/" + inel_codigo)
                }
            }).fail(function() {
                that.showError("Error Editando item", "Error en POST", "ruta:" + url)
            })
        });
        this.post('#/inve/emplazamiento/:empl_id/items/filtro(/:page)?', function(context) {
            page = this.params['page'];
            empl_id = this.params['empl_id'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.page;
            delete data.splat;
            this.updateElement($("#inve-item-lista"), "inve_items_lista", 'rest/contrato/' + contract + '/inve/emplazamiento/' + empl_id + '/items/list/' + page, data)
        });
        this.get('#/inve/emplazamiento/:empl_id/item/agregar', function(context) {
            empl_id = this.params['empl_id'];
            this.showUI(context, 'inve_agregar', {}, 'rest/contrato/' + contract + '/inve/emplazamiento/' + empl_id + '/item/agregar')
        });
        this.get('#/inve/emplazamiento/:empl_id/items/:inel_codigo', function(context) {
            empl_id = this.params['empl_id'];
            inel_id = this.params['inel_codigo'];
            this.showUI(context, 'inve_detalle', {}, 'rest/contrato/' + contract + '/inve/emplazamiento/' + empl_id + '/item/' + inel_id)
        });
        this.get('#/insp', function(context) {
            this.redirect('#/insp/bandeja')
        });
        this.get('#/insp/bandeja', function(context) {
            that = this;
            this.showUI(context, 'insp_bandeja', window.inspBandejaFiltros, 'rest/contrato/' + contract + '/insp/bandeja/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/insp/bandeja/filtro/1', window.inspBandejaFiltros);
                    $("#siom-form-insp-bandeja").submit()
                }, 200)
            })
        });
        this.post('#/insp/bandeja/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.page;
            window.inspBandejaFiltros = data;
            if (data.insp_id && data.insp_id != "") {
                data = {
                    insp_id: data.insp_id
                }
            } else {
                if (data.insp_fecha_ != "") {
                    data.mant_fecha_programada_inicio = this.formatDate(data.mant_fecha_programada_inicio)
                };
                if (data.mant_fecha_programada_termino != "") {
                    data.mant_fecha_programada_termino = this.formatDate(data.mant_fecha_programada_termino)
                }
            }
            delete data.splat;
            this.updateElement($("#insp-bandeja-lista"), "insp_bandeja_lista", "rest/contrato/" + contract + "/insp/list/" + page, data)
        });
        this.get('#/insp/crear', function(context) {
            if (!window.inspCrearFiltros) {
                window.inspCrearFiltros = {}
            };
            this.showUI(context, 'insp_crear', {
                max_words: window.config.mnt.minDescription
            }, 'rest/contrato/' + contract + '/insp/add/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/insp/crear/filtro/1', window.mntCrearFiltros)
                }, 200)
            })
        });
        this.post('#/insp/crear/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#insp-crear-lista"), "insp_crear_lista", "rest/core/contrato/" + contract + "/emplazamiento/list/" + page + "?empl_estado=ACTIVO", data)
        });
        this.post('#/insp/crear', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]))
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            $.each(form.find("input:file"), function(i, file) {
                if ($(file)[0].files[0]) {
                    formData.append($(this).attr("name"), $(file)[0].files[0])
                }
            });
            url = 'rest/contrato/' + contract + '/insp/add';
            $.ajax({
                url: url,
                data: formData,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function(json, textStatus) {
                    if (json.status) {
                        that.showSuccess("Creando inspección", "Inspección creada exitosamente (Inspección Nº " + json.data[0].id + ")", "#/insp/bandeja")
                    } else {
                        that.showError("Error creando inspección", json.error, "ruta:" + url, "#/insp/bandeja")
                    }
                },
                error: function(xhr, textStatus, errorThrown) {
                    that.showError("Error creando inspección", "No se pudo guardar inspección (" + textStatus + ")", "ruta:" + url, "#/insp/bandeja")
                }
            })
        });
        this.get('#/insp/detalle/:insp_id', function(context) {
            this.showUI(context, 'insp_detalle', {}, 'rest/contrato/' + contract + '/insp/detalle/' + this.params['insp_id'])
        });
        this.get('#/insp/cambio_empresa_inspectora/:insp_id', function(context) {
            this.showUI(context, 'insp_cambio_empresa_inspectora', {}, 'rest/contrato/' + contract + '/insp/cambio_empresa_inspectora/' + this.params['insp_id'])
        });
        this.post('#/insp/cambio_empresa_inspectora/:insp_id', function(context) {
            that = this;
            insp_id = this.params['insp_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/insp/cambio_empresa_inspectora/' + insp_id;
            $.post(url, data, function(json, textStatus) {
                form = $(context.target);
                form.find(':submit').button('reset');
                if (json.status) {
                    that.showSuccess("Cambiando empresa inspectora", "Empresa inspectora cambiada exitosamente", "#/insp/bandeja")
                } else {
                    that.showError("Error cambiando empresa inspectora", json.error, "ruta:" + url, "#/insp/bandeja")
                }
            }, 'json').fail(function() {
                form = $(context.target);
                form.find(':submit').button('reset');
                that.showError("Error cambiando empresa inspectora", "No se pudo cambiar empresa inspectora", "ruta:" + url, "#/insp/bandeja")
            })
        });
        this.get('#/insp/asignacion/:insp_id', function(context) {
            that = this;
            this.showUI(context, 'insp_asignacion', {}, 'rest/contrato/' + contract + '/insp/asignacion/' + this.params['insp_id'], {}, function(data) {
                if (data.asignacion != null) {
                    if (data.asignacion.emvi_fecha_ingreso != null) {
                        that.showError("Inspección Asignación", "No es posible reasignar la Inspección cuando está en ejecución", "ruta:" + url, "#/insp/bandeja")
                    } else {
                        confirm("Esta inspección ya fue asignada a <b>" + data.asignacion.usua_nombre + "</b> el " + data.asignacion.inas_fecha_asignacion + "<br>¿Desea cancelar asignación y volver a asignar la inspección?", function(status) {
                            if (status) {
                                url = 'rest/contrato/' + contract + '/insp/asignacion/' + that.params['insp_id'] + '/cancelar/' + data.asignacion.inas_id;
                                $.post(url, data, function(json, textStatus) {
                                    if (!json.status) {
                                        that.showError("Error cancelando asignación", json.error, "ruta:" + url, "#/insp/bandeja")
                                    }
                                }, 'json').fail(function() {
                                    that.showError("Error cancelando asignación", "No se pudo cancelar asignación", "ruta:" + url, "#/insp/bandeja")
                                })
                            } else {
                                that.redirect("#/insp/bandeja")
                            }
                        })
                    }
                }
            })
        });
        this.post('#/insp/asignacion/:insp_id', function(context) {
            that = this;
            insp_id = this.params['insp_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/insp/asignacion/' + insp_id;
            $.post(url, data, function(json, textStatus) {
                form = $(context.target);
                if (json.status) {
                    form.find('.siom-form-actions').hide();
                    form.find(':input').prop('disabled', true);
                    that.showSuccess("Agregando asignación", "Inspección asignada exitosamente", "#/insp/bandeja")
                } else {
                    form.find(':submit').button('reset');
                    that.showError("Error agregando asignación", json.error, "ruta:" + url, "#/insp/bandeja")
                }
            }, 'json').fail(function() {
                form = $(context.target);
                form.find(':submit').button('reset');
                that.showError("Error agregando asignación", "No se pudo asignar ", "ruta:" + url, "#/insp/bandeja")
            })
        });
        this.get('#/insp/informe/:insp_id/ver/:info_id', function(context) {
            insp_id = this.params['insp_id'];
            info_id = this.params['info_id'];
            this.showUI(context, 'insp_ver_informe', {
                insp_id: insp_id,
                info_id: info_id
            }, 'rest/contrato/' + contract + '/insp/informe/' + insp_id + '/ver/' + info_id)
        });
        this.get('#/insp/informe/:insp_id/validar/:info_id', function(context) {
            insp_id = this.params['insp_id'];
            info_id = this.params['info_id'];
            this.showUI(context, 'insp_validar_informe', {
                insp_id: insp_id,
                info_id: info_id
            }, 'rest/contrato/' + contract + '/insp/informe/' + insp_id + '/validar/' + info_id)
        });
        this.post('#/insp/informe/:insp_id/validar/:info_id', function(context) {
            that = this;
            insp_id = this.params['insp_id'];
            info_id = this.params['info_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/insp/informe/' + insp_id + "/validar/" + info_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando informe", "Validación de informe guardada exitosamente", "#/insp/cerrar/" + insp_id)
                } else {
                    that.showError("Error validando informe", json.error, "ruta:" + url, "#/insp/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando informe", "No se pudo guardar informe", "ruta:" + url, "#/insp/bandeja")
            })
        });
        this.get('#/insp/adjuntar/:insp_id', function(context) {
            this.showUI(context, 'insp_adjuntar', {
                max_file_upload: this.getReadableSize(window.config.max_file_upload)
            }, 'rest/contrato/' + contract + '/insp/adjuntar/' + this.params['insp_id'])
        });
        this.post('#/insp/adjuntar/:insp_id', function(context) {
            that = this;
            form = $(context.target);
            data = this.params.toHash();
            delete data.splat;
            var string_value;
            var formData = new FormData();
            $.each(data, function(key, value) {
                if ($.isArray(value)) {
                    formData.append(key, JSON.stringify(value))
                } else {
                    if (key == "archivos_descripciones") {
                        formData.append(key, JSON.stringify([value]));
                        string_value = value
                    } else {
                        formData.append(key, value)
                    }
                }
            });
            if (string_value == "") {
                alert("Falta una descripcion para el archivo seleccionado")
            } else {
                $.each(form.find("input:file"), function(i, file) {
                    if ($(file)[0].files[0]) {
                        formData.append($(this).attr("name"), $(file)[0].files[0])
                    }
                });
                url = 'rest/contrato/' + contract + '/insp/adjuntar/' + this.params['insp_id'];
                $.ajax({
                    url: url,
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(json, textStatus) {
                        if (json.status) {
                            that.showSuccess("Adjuntando archivo(s) a inspección", "Archivo(s) agregado(s) exitosamente a Inspección", "#/insp/bandeja")
                        } else {
                            that.showError("Error adjuntando archivo(s) a inspección", json.error, "ruta:" + url, "#/insp/bandeja")
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        that.showError("Adjuntando archivo(s) a inspección", "No se pudo guardar mantenimiento (" + textStatus + ")", "ruta:" + url, "#/insp/bandeja")
                    }
                })
            }
        });
        this.get('#/insp/cerrar/:insp_id', function(context) {
            insp_id = this.params['insp_id'];
            this.showUI(context, 'insp_cerrar', {}, 'rest/contrato/' + contract + '/insp/cerrar/' + insp_id)
        });
        this.post('#/insp/cerrar/:insp_id', function(context) {
            that = this;
            insp_id = this.params['insp_id'];
            insp_estado = this.params['insp_estado'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/contrato/' + contract + '/insp/cerrar/' + insp_id;
            $.post(url, data, function(json, textStatus) {
                if (json.status) {
                    that.showSuccess("Validando Inspección", "Validación de inspección guardada exitosamente", "#/insp/bandeja")
                } else {
                    that.showError("Error validando Inspección", json.error, "ruta:" + url, "#/insp/bandeja")
                }
            }, 'json').fail(function() {
                that.showError("Error validando Inspección", "No se pudo cerrar Inspección", "ruta:" + url, "#/insp/bandeja")
            })
        });
        this.get('#/insp/resumen', function(context) {
            this.showUI(context, 'insp_resumen', {}, 'rest/contrato/' + contract + '/insp/resumen/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/insp/resumen')
                }, 200)
            })
        });
        this.post('#/insp/resumen', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            if ("resumen" == data.tipo_resumen || null == data.tipo_resumen) {
                if (null == data.agrupador) {
                    data.agrupador = "zona_contrato"
                }
            }
        });
        this.get('#/sla', function(context) {
            this.redirect('#/sla/tiempos')
        });
        this.post('#/sla', function(context) {
            this.redirect('#/sla/tiempos')
        });
        this.get('#/sla/tiempos', function(context) {
            this.showUI(context, 'sla_tiempos', null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        orse_fecha_validacion_inicio: firstDay,
                        orse_fecha_validacion_termino: lastDay,
                        zona_tipo: 'CONTRATO'
                    };
                    window.app.runRoute('post', '#/sla/tiempos/filtro', data)
                })
            })
        });
        this.post('#/sla/tiempos/filtro(/:page)?', function(context) {
            console.log('#/sla/tiempos/filtro');
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.pagina;
            delete data.tipo_datos;
            delete data.zona_id;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] == 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                pagina = (tipo_datos === "validacion") ? pagina : null;
                if (tipo_datos === "graficos_oseu_osgu") {
                    this.updateElement($("#sla-tiempos-datos"), "sla_tiempos_graficos", "rest/contrato/" + contract + "/sla/tiempos/graficos_oseu_osgu" + ((pagina) ? "/" + pagina : ""), data);
                    $(".ct option[value='X']").remove()
                } else {
                    this.updateElement($("#sla-tiempos-datos"), "sla_tiempos_" + tipo_datos, "rest/contrato/" + contract + "/sla/tiempos/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
                }
            } else {
                this.updateElement($("#sla-tiempos-datos"), "sla_tiempos_graficos", "rest/contrato/" + contract + "/sla/tiempos/graficos" + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/fallas', function(context) {
            this.showUI(context, 'sla_fallas', null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        orse_fecha_validacion_inicio: firstDay,
                        orse_fecha_validacion_termino: lastDay,
                        zona_tipo: 'CONTRATO'
                    };
                    window.app.runRoute('post', '#/sla/fallas/filtro', data)
                })
            })
        });
        this.post('#/sla/fallas/filtro(/:page)?', function(context) {
            var sla_section = "fallas";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                }
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/reiteradas', function(context) {
            console.log(context);
            this.showUI(context, 'sla_reiteradas', null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        orse_fecha_validacion_inicio: firstDay,
                        orse_fecha_validacion_termino: lastDay,
                        zona_tipo: 'CONTRATO'
                    };
                    window.app.runRoute('post', '#/sla/reiteradas/filtro', data)
                })
            })
        });
        this.post('#/sla/reiteradas/filtro(/:page)?', function(context) {
            var sla_section = "reiteradas";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                };
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data);
                data.tipo_datos = 'grafico_zonas'
            }
        });
        this.get('#/sla/disponibilidad', function(context) {
            this.showUI(context, 'sla_disponibilidad', null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        orse_fecha_validacion_inicio: firstDay,
                        orse_fecha_validacion_termino: lastDay,
                        zona_tipo: 'CONTRATO'
                    };
                    window.app.runRoute('post', '#/sla/disponibilidad/filtro', data)
                })
            })
        });
        this.post('#/sla/disponibilidad/filtro(/:page)?', function(context) {
            var sla_section = "disponibilidad";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                };
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos_disponibilidad" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos_disponibilidad" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/ejecucion', function(context) {
            this.showUI(context, 'sla_ejecucion', null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        mant_fecha_validacion_inicio: firstDay,
                        mant_fecha_validacion_termino: lastDay,
                        zona_tipo: 'CONTRATO'
                    };
                    window.app.runRoute('post', '#/sla/ejecucion/filtro', data)
                })
            })
        });
        this.post('#/sla/ejecucion/filtro(/:page)?', function(context) {
            var sla_section = "ejecucion";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                };
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/cronograma', function(context) {
            this.showUI(context, 'sla_cronograma', null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        mant_fecha_validacion_inicio: firstDay,
                        mant_fecha_validacion_termino: lastDay,
                        zona_tipo: 'CONTRATO'
                    };
                    window.app.runRoute('post', '#/sla/cronograma/filtro', data)
                })
            })
        });
        this.post('#/sla/cronograma/filtro(/:page)?', function(context) {
            var sla_section = "cronograma";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                };
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/calidad', function(context) {
            var sla_section = "calidad";
            var sla_prefijo = "mant";
            this.showUI(context, 'sla_' + sla_section, null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        zona_tipo: 'CONTRATO'
                    };
                    data[sla_prefijo + '_fecha_validacion_inicio'] = firstDay;
                    data[sla_prefijo + '_fecha_validacion_termino'] = lastDay;
                    window.app.runRoute('post', '#/sla/' + sla_section + '/filtro', data)
                })
            })
        });
        this.post('#/sla/calidad/filtro(/:page)?', function(context) {
            var sla_section = "calidad";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                };
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/calidadycronograma', function(context) {
            var sla_section = "calidadycronograma";
            var sla_prefijo = "mant";
            this.showUI(context, 'sla_' + sla_section, null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        zona_tipo: 'CONTRATO'
                    };
                    data[sla_prefijo + '_fecha_validacion_inicio'] = firstDay;
                    data[sla_prefijo + '_fecha_validacion_termino'] = lastDay;
                    window.app.runRoute('post', '#/sla/' + sla_section + '/filtro', data)
                })
            })
        });
        this.post('#/sla/calidadycronograma/filtro(/:page)?', function(context) {
            var sla_section = "calidadycronograma";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (tipo_datos != null && tipo_datos != "") {
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos, data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/sla/evaluacion', function(context) {
            var sla_section = "evaluacion";
            this.showUI(context, 'sla_' + sla_section, null, 'rest/contrato/' + contract + '/sla/filtros', {}, function() {
                setTimeout(function(context) {
                    date = new Date(), y = date.getFullYear(), m = date.getMonth();
                    firstDay = new Date(y, m - 1, 1).toISOString().slice(0, 10);
                    lastDay = new Date(y, m + 1, 0).toISOString().slice(0, 10);
                    data = {
                        zona_tipo: 'CONTRATO'
                    };
                    data['fecha_validacion_inicio'] = firstDay;
                    data['fecha_validacion_termino'] = lastDay;
                    window.app.runRoute('post', '#/sla/' + sla_section + '/filtro', data)
                })
            })
        });
        this.post('#/sla/evaluacion/filtro(/:page)?', function(context) {
            var sla_section = "evaluacion";
            var data = this.params.toHash();
            var tipo_datos = data.tipo_datos;
            var pagina = data.pagina;
            var zona = data.zona_id;
            delete data.splat;
            delete data.tipo_datos;
            delete data.zona_id;
            pagina = (tipo_datos === "validacion") ? pagina : null;
            if (zona != null && zona != "") {
                zona = zona.split(",");
                (zona[1] === 'zona') ? data.zona_id = zona[0]: data.regi_id = zona[0]
            };
            if (tipo_datos != null && tipo_datos != "") {
                if (tipo_datos === "grafico_especialidades") {
                    data.espe_id = data.espe_idu
                };
                this.updateElement($("#sla-" + sla_section + "-datos"), ((tipo_datos.startsWith("grafico")) ? "sla_graficos_evaluacion" : "sla_" + sla_section + "_" + tipo_datos), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + tipo_datos + ((pagina) ? "/" + pagina : ""), data)
            } else {
                this.updateElement($("#sla-" + sla_section + "-datos"), (('grafico_zonas'.startsWith("grafico")) ? "sla_graficos_evaluacion" : "sla_" + sla_section + "_" + 'grafico_zonas'), "rest/contrato/" + contract + "/sla/" + sla_section + "/" + 'grafico_zonas' + ((pagina) ? "/" + pagina : ""), data)
            }
        });
        this.get('#/core', function(context) {
            this.redirect('#/core/contrato')
        });
        this.get('#/core/contrato', function(context) {
            that = this;
            this.showUI(context, 'core_contrato', {}, 'rest/core/contrato/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/contrato/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/contrato/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#core-contrato-lista"), "core_contrato_lista", "rest/core/contrato/list/" + page, data)
        });
        this.post('#/core/contrato/editar', function(context) {
            that = this;
            cont_id = this.params['cont_id'];
            data = this.params.toHash();
            delete data.splat;
            var fecha = data['cont_fecha_inicio'].split("-");
            var fechaTermino = data['cont_fecha_termino'].split("-");
            if (fecha[0] > 31) {
                data['cont_fecha_inicio'] = fecha[0] + "-" + fecha[1] + "-" + fecha[2]
            } else {
                data['cont_fecha_inicio'] = fecha[2] + "-" + fecha[1] + "-" + fecha[1]
            };
            if (fechaTermino[0] > 31) {
                data['cont_fecha_termino'] = fechaTermino[0] + "-" + fechaTermino[1] + "-" + fechaTermino[2]
            } else {
                data['cont_fecha_termino'] = fechaTermino[2] + "-" + fechaTermino[1] + "-" + fechaTermino[0]
            };
            url = 'rest/core/contrato/add';
            if (data.cont_id > 0) {
                url = 'rest/core/contrato/upd/' + data.cont_id
            } else {
                delete data["cont_id"]
            };
            $.post(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error guardando contrato", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Guardando contrato", "Contrato guardado exitosamente", "#/core/contrato")
                }
            }).fail(function() {
                that.showError("Error guardando contrato", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/core', function(context) {
            this.redirect('#/core/empresa')
        });
        this.get('#/core/empresa', function(context) {
            that = this;
            this.showUI(context, 'core_empresa', {}, 'rest/core/empresa/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/empresa/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/empresa/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.coreEmplazamientoFiltros = data;
            data.empr_estado = this.params['empr_estado'];
            this.updateElement($("#core-empresa-lista"), "core_empresa_lista", "rest/core/empresa/contrato/" + contract + "/list/" + page, data)
        });
        this.get('#/core', function(context) {
            this.redirect('#/core/curso')
        });
        this.get('#/core/curso', function(context) {
            that = this;
            this.showUI(context, 'core_curso', {}, 'rest/core/curso/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/curso/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/curso/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            data.curs_estado = this.params['curs_estado'];
            this.updateElement($("#core-curso-lista"), "core_curso_lista", "rest/core/curso/list/" + page, data)
        });
        this.post('#/core/curso/editar', function(context) {
            that = this;
            curs_id = this.params['curs_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/core/curso/add';
            if (data.curs_id > 0) {
                url = 'rest/core/curso/upd/' + data.curs_id
            } else {
                delete data["curs_id"]
            };
            $.post(url, data, function(json) {
                console.log("post: " + url);
                console.log(json);
                if (json.status == 0) {
                    that.showError("Error guardando curso", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Guardando curso", "Curso guardado exitosamente", "#/core/curso")
                }
            }).fail(function() {
                that.showError("Error guardando curso", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/core/formulario', function(context) {
            that = this;
            this.showUI(context, 'core_formulario', {}, 'rest/core/formulario/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/formulario/filtro/1', {});
                    $('#FormFormularioLista').submit()
                }, 200)
            })
        });
        this.post('#/core/formulario/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            data.form_estado = this.params['form_estado'];
            this.updateElement($("#core-formulario-lista"), "core_formulario_lista", "rest/core/contrato/" + contract + "/formulario/list/" + page, data)
        });
        this.post('#/core/formulario/editar', function(context) {
            that = this;
            curs_id = this.params['curs_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/core/formulario/add';
            if (data.curs_id > 0) {
                url = 'rest/core/formulario/upd/' + data.curs_id
            } else {
                delete data["form_id"]
            };
            $.post(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error guardando formulario", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Guardando formulario", "Curso guardado exitosamente", "#/core/formulario")
                }
            }).fail(function() {
                that.showError("Error guardando formulario", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/core/periodicidad', function(context) {
            that = this;
            this.showUI(context, 'core_periodicidad', {}, 'rest/core/periodicidad/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/periodicidad/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/periodicidad/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            data.curs_estado = this.params['form_estado'];
            this.updateElement($("#core-periodicidad-lista"), "core_periodicidad_lista", "rest/core/periodicidad/contrato/" + contract + "/list/" + page, data)
        });
        this.post('#/core/periodicidad/editar', function(context) {
            that = this;
            curs_id = this.params['curs_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/core/periodicidad/add';
            console.log(data);
            if (data.peri_id > 0) {
                url = 'rest/core/periodicidad/upd/' + data.peri_id
            } else {
                delete data["form_id"]
            };
            $.post(url, data, function(json) {
                console.log("post: " + url);
                console.log(json);
                if (json.status == 0) {
                    that.showError("Error guardando periodicidad", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Guardando periodicidad", "periodicidad guardado exitosamente", "#/core/periodicidad")
                }
            }).fail(function() {
                that.showError("Error guardando periodicidad", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/core/usuario', function(context) {
            that = this;
            this.showUI(context, 'core_usuario', {}, 'rest/core/usuario/contrato/' + contract + '/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/usuario/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/usuario/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            data.usua_estado = this.params['usua_estado'];
            this.updateElement($("#core-usuario-lista"), "core_usuario_lista", "rest/core/usuario/contrato/" + contract + "/list/" + page, data)
        });
        this.get('#/core/emplazamiento_core', function(context) {
            if (typeof window.coreEmplazamientoFiltros === undefined) {
                window.coreEmplazamientoFiltros = {};
                now = "ACTIVO";
                window.coreEmplazamientoFiltros.empl_estado = this.now
            }
            this.showUI(context, 'core_emplazamiento_core', window.coreEmplazamientoFiltros, 'rest/core/emplazamiento/contrato/' + contract + '/filtros', {}, function() {
                setTimeout(function() {
                    $("#FormEmplazamientoLista").submit()
                }, 200)
            })
        });
        this.get('#/core/emplazamiento', function(context) {
            if (typeof window.coreEmplazamientoFiltros === undefined) {
                window.coreEmplazamientoFiltros = {};
                now = "ACTIVO";
                window.coreEmplazamientoFiltros.empl_estado = this.now
            }
            this.showUI(context, 'core_emplazamiento', window.coreEmplazamientoFiltros, 'rest/core/emplazamiento/contrato/' + contract + '/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/emplazamiento/filtro/1', window.coreLpuFiltros)
                }, 200)
            })
        });
        this.post('#/core/emplazamiento/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            window.coreEmplazamientoFiltros = data;
            data.empl_estado = this.params['empl_estado'];
            this.updateElement($("#core-emplazamiento-lista"), "core_emplazamiento_lista", "rest/core/emplazamiento/" + contract + "/list/" + page, data)
        });
        this.post('#/core/emplazamiento/edit', function(context) {
            that = this;
            data = this.params.toHash();
            delete data.splat;
            delete data.page
        });
        this.post('#/core/emplazamiento/edit/contrato_zonas', function(context) {
            that = this;
            data = this.params.toHash();
            delete data.splat;
            delete data.page
        });
        this.get('#/core/lpu', function(context) {
            that = this;
            this.showUI(context, 'core_lpu', {}, 'rest/core/contrato/' + contract + '/lpu/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/contrato/lpu/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/contrato/lpu/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            data.lpu_nombre = this.params['lpu_nombre'];
            data.lpu_estado = this.params['lpu_estado'];
            this.updateElement($("#core-lpu-lista"), "core_lpu_lista", "rest/core/contrato/" + contract + "/lpu/list/" + page, data)
        });
        this.get('#/core/lpu/:lpu_id/grupo', function(context) {
            that = this;
            lpu_id = this.params['lpu_id'];
            this.showUI(context, 'core_lpu_grupo', {}, 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/list', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/contrato/lpu_grupo/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/contrato/lpu_grupo/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            data.lpgr_nombre = this.params['lpgr_nombre'];
            this.updateElement($('#core-lpu-grupo-lista'), 'core_lpu_grupo_lista', 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/list/' + page, data)
        });
        this.get('#/core/lpu/:lpu_id/grupo/:lpu_grupo/clase', function(context) {
            that = this;
            lpu_id = this.params['lpu_id'];
            lpu_grupo = this.params['lpu_grupo'];
            this.showUI(context, 'core_lpu_grupo_clase', {}, 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/' + lpu_grupo + '/clase/list', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/contrato/lpu_grupo_clase/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/contrato/lpu_grupo_clase/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($('#core-lpu-grupo-clase-lista'), 'core_lpu_grupo_clase_lista', 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/' + lpu_grupo + '/clase/list/' + page)
        });
        this.get('#/core/lpu/:lpu_id/grupo/:lpu_grupo/item', function(context) {
            that = this;
            lpu_id = this.params['lpu_id'];
            lpu_grupo = this.params['lpu_grupo'];
            this.showUI(context, 'core_lpu_grupo_item', {}, 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/' + lpu_grupo + '/item/list', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/contrato/lpu_grupo_item/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/contrato/lpu_grupo_item/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($('#core-lpu-grupo-item-lista'), 'core_lpu_grupo_item_lista', 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/' + lpu_grupo + '/item/list/' + page)
        });
        this.get('#/core/lpu/:lpu_id/grupo/:lpu_grupo/item/:lpu_item', function(context) {
            that = this;
            lpu_id = this.params['lpu_id'];
            lpu_grupo = this.params['lpu_grupo'];
            lpu_item = this.params['lpu_item'];
            this.showUI(context, 'core_lpu_grupo_item_precio', {}, 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/' + lpu_grupo + '/item/' + lpu_item + '/precio/list', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/contrato/lpu_grupo_item_precio/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/contrato/lpu_grupo_item_precio/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($('#core-lpu-grupo-item-precio-lista'), 'core_lpu_grupo_item_precio_lista', 'rest/core/contrato/' + contract + '/lpu/' + lpu_id + '/grupo/' + lpu_grupo + '/item/' + lpu_item + '/precio/list/' + page)
        });
        this.get('#/core/formulario', function(context) {
            that = this;
            this.showUI(context, 'core_formulario', {}, 'rest/core/formulario/filtros', {}, function() {
                setTimeout(function() {
                    window.app.runRoute('post', '#/core/formulario/filtro/1', {})
                }, 200)
            })
        });
        this.post('#/core/formulario/filtro(/:page)?', function(context) {
            page = this.params['page'];
            page = (page) ? (page.substring(1)) : (1);
            data = this.params.toHash();
            delete data.splat;
            delete data.page;
            this.updateElement($("#core-formulario-lista"), "core_formulario_lista", "rest/core/formulario/list/" + page, data)
        });
        this.post('#/core/formulario/editar', function(context) {
            that = this;
            curs_id = this.params['curs_id'];
            data = this.params.toHash();
            delete data.splat;
            url = 'rest/core/formulario/add';
            if (data.curs_id > 0) {
                url = 'rest/core/formulario/upd/' + data.curs_id
            } else {
                delete data["form_id"]
            };
            $.post(url, data, function(json) {
                if (json.status == 0) {
                    that.showError("Error guardando formulario", json.error, "ruta:" + url);
                    return
                } else {
                    that.showSuccess("Guardando formulario", "Curso guardado exitosamente", "#/core/formulario")
                }
            }).fail(function() {
                that.showError("Error guardando formulario", "Error en POST", "ruta:" + url)
            })
        });
        this.get('#/logout', function() {
            _isLogged = false;
            $.post("rest/logout");
            window.SyncStop();
            window.emplazamientosFiltros = null;
            window.osBandejaFiltros = null;
            window.mntBandejaFiltros = null;
            localStorage.clear();
            window.localStorage.clear();
            this.redirect('#/login')
        });
        this.get('#/no_permitido', function(context) {
            this.showMain(context, "no_permitido");
            this.showSection(context, "no_permitido", {})
        });
        this.post('#/contrato/cambiar', function(context) {
            cont_id = this.params["cont_id"];
            if (!cont_id) {
                alert("Id de Contrato Inválido");
                return
            };
            if (contract != cont_id) {
                contract = cont_id;
                window.app.refresh()
            }
        });
        this.notFound = function() {}
    });
    window.app.raise_errors = true;
    window.app.run('#/login')
})(jQuery);

(function($) {

	var filterTimer=null;
	var filterTimeout=500;

    $('.selectpicker').selectpicker();

    $('#siom-form-lpu input[name=lpu_nombre]').keyup(function () {
         FiltrarConDelay();
    })

    $('#siom-form-lpu select[name=lpu_estado]').change(function () {
         FiltrarConDelay();
    })

	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-lpu").submit();
         },filterTimeout);
	}

    $(document).on('click','button.editar',function(e){
	  LPU_data = $(this).data("data");
	  $.each(LPU_data, function( key, value ) {
	       $('#EditFormLPU').find('#'+key).val(value);
	  });
      cont_id = $("#cont_id").val();
      lpu_id  = $("#lpu_id").val();


	  $('#wizardLPU').wizard("next");
    });



    //Guarda datos Edición usuario
    //-----------------------------------------------
	$('#submit_Guardar').click(function(e){
	   	var button = $(this);
	   	params = {};
        cont_id = $("#cont_id").val();
        lpu_id  = $("#lpu_id").val();

	   $('#EditFormLPU input').each(
		    function(index){
		        var input = $(this);
		        params[input.attr('id')]= input.val();
		    }
		);

  		url = 'rest/core/contrato/'+cont_id+'/lpu/add';
		if(params.lpu_id){
			url = 'rest/core/contrato/'+cont_id+'/lpu/upd/'+params.lpu_id;
		}
		else{
			delete params["lpu_id"];
		}
		button.button('loading');
		$.post(url,params,function(json) {
			button.button('reset');
  			if(json.status){

  			   $("#EditFormLPU").submit();
  			}
  			else{
  				alert(json.error);
  			}
	  	}).fail(function(){
	  		button.button('reset');
			alert("Error al guardar [codigo: 1001]");
		});
  	});

    $(document).on('click', 'button.eliminarLPU', function(e){
        lpu_id     = $(this).data("id");
        lpu_nombre = $(this).data("nombre");
        lpu_estado = $(this).data("estado");
        lpu_contra = $(this).data("contrato");

        if (lpu_estado=="ACTIVO"){
            nuevo_estado        = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
      	}
        else{
            nuevo_estado        = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("La LPU <b>"+lpu_estado+"</b> será <b>"+nombre_nuevo_estado+"</b>,<br>Desea continuar?",function(status){
              if(status==true){
                    $.get('rest/core/contrato/'+lpu_contra+'/lpu/upd/'+lpu_id+'/?lpu_estado='+nuevo_estado, null, function(json) {
                    }).fail(function(xhr, textStatus, errorThrown){
                      alert("Error "+xhr.status+": "+xhr.statusText);
                  });
                 FiltrarConDelay();
              }
        });

    });



})(jQuery);

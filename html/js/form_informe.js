$(document).ready(function() {
	$('div.siom-informe-mapa').each(function() {
	   setMap($(this),$(this).data("latitud"),$(this).data("longitud"));
	});

  
  function setMap(element,lat,lng){
    if((typeof google != "undefined") && lat!="" && lng!=""){
      var gMap=null;
      var gMarker = null;
      var latlng = new google.maps.LatLng(lat,lng);

      if(!gMap){
         var mapOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        gMap = new google.maps.Map(element[0],mapOptions); 
        gMarker = new google.maps.Marker({map: gMap});  
      }
        
       google.maps.event.trigger(gMap,'resize');
       gMarker.setPosition(latlng);
       gMap.setCenter(latlng);
    }
  }

});

(function($) { 
	//inicializar componentes
    $('.selectpicker').selectpicker();
    window.scrollToElement($('#siom-form-asignacion'));

    //Eventos
    $('#filtro_acompanantes').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#lista_acompanantes li').hide();
            $('#lista_acompanantes li').filter(function () {
                return rex.test($(this).text());
            }).show();
        })
    $('#filtro_acompanantes_seleccionados').keyup(function () {
            var rex = new RegExp($(this).val(), 'i');
            $('#lista_acompanantes_seleccionados li').hide();
            $('#lista_acompanantes_seleccionados li').filter(function () {
                return rex.test($(this).text());
            }).show();
        })


    $("#lista_acompanantes li").click(function(e){
        if($(this).hasClass("disabled")){
            return;
        }

    	id     = $(this).data("id");
    	nombre = $(this).data("nombre");

    	html = '<li class="list-group-item" id="'+id+'">'+
    		   '<span>'+nombre+'</span>'+
    		   '<i class="glyphicon glyphicon-trash pull-right eliminar" data-id="'+id+'"></i>'+
    		   '<input type="hidden" name="acompanantes" value="'+id+'">'+
			   '</li>';

        $(this).addClass("disabled");
    	$("#lista_acompanantes_seleccionados").append(html);
    });

    $(document).on('click','#lista_acompanantes_seleccionados i',function(e){
    	id  = $(this).data("id");
    	$("#lista_acompanantes_seleccionados #"+id).remove();	
        $("#lista_acompanantes #"+id).removeClass("disabled");
    })

    $('#siom-form-asignacion').submit(function() {
        if($("#jefecuadrilla").val()==""){
            alert("Debe selecionar un jefe de cuadrilla");
            return false;
        }

		acompanantes = $("#lista_acompanantes_seleccionados li").length;	
        if(acompanantes==0){
        	alert("Debe agregar al menos un acompañante.");
        	return false;
        }

        $("#submit_asignacion").button('loading');
        return true;
    });
    
})(jQuery);
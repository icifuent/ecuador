(function($) {
    console.log("HERE");
	var filterTimer=null;
	var filterTimeout=500;
    function InitSelectPickers(){
        sp = $('select.selectpicker');
        for(i=0;i<sp.length;i++){
            s = $(sp[i]);

            name = s.attr('name');
            val  = s.data('value');
            
            s.selectpicker();
            s.selectpicker('val',val);
            s.selectpicker('refresh');
        }
    }
    
    console.log("INIT");
    InitSelectPickers();
    console.log("POST INIT");

    console.log("MNTFILTROS");
	if(window.mntFiltros){
            /*dt = window.mntFiltros.usmnt_fecha_fin.split("-");

            if(parseInt(dt[0])<=31){
                console.log("");
                window.mntFiltros.usmnt_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
            }else{
                console.log("");
                window.mntFiltros.usmnt_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
            }

            window.mntFiltros.usmnt_fecha_fin     = dt[0]+"-"+dt[1]+"-"+dt[2];

            dt = window.mntFiltros.usmnt_fecha_inicio.split("-");

            if(parseInt(dt[0])<=31){
                console.log("");
                window.mntFiltros.usmnt_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
            }else{
                console.log("");
                window.mntFiltros.usmnt_fecha_inicio= dt[2]+"-"+dt[1]+"-"+dt[0];
            }
            
            window.mntFiltros.usmnt_fecha_inicio  = dt[0]+"-"+dt[1]+"-"+dt[2];
            */
    } else{
            window.mntFiltros = {}
        /*    now = new Date();
            d   = now.getDate();
            m   = now.getMonth()+1;
            y   = now.getFullYear();

			if(d < 10){
				d = "0"+d;
			}
			if(m < 10){
				m = "0"+m;
			}
            window.mntFiltros.usmnt_fecha_fin    = d+"-"+m+"-"+y;
            window.mntFiltros.usmnt_fecha_inicio = d+"-"+m+"-"+y;
            */
    }
  
    console.log("datepicker");
    $('#fecha_inicio,#fecha_fin').datepicker({
	    format: "dd-mm-yyyy",
	    viewMode: "days",
	    minViewMode: "days",
	    language: "es"
	})

    /*** Boton Limpiar ***/
    $(document).on('click', 'button#LimpiarMntBandeja', function (e) {
        
        $('#mant_id').val("");
        $('#mant_id').change();

        $('select[name=peri_id]').val("");
        $('select[name=peri_id]').change();
        /*
        if(window.mntFiltros){
                dt = window.mntFiltros.usmnt_fecha_fin.split("-");

                if(parseInt(dt[0])<=31){
                    console.log("");
                    window.mntFiltros.usmnt_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    console.log("");
                    window.mntFiltros.usmnt_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
                }

                //window.mntFiltros.usmnt_fecha_fin     = dt[0]+"-"+dt[1]+"-"+dt[2];

                dt = window.mntFiltros.usmnt_fecha_inicio.split("-");

                if(parseInt(dt[0])<=31){
                    console.log("");
                    window.mntFiltros.usmnt_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    console.log("");
                    window.mntFiltros.usmnt_fecha_inicio= dt[2]+"-"+dt[1]+"-"+dt[0];
                }
        } else{
                window.mntFiltros = {}
                now = new Date();
                d   = now.getDate();
                m   = now.getMonth()+1;
                y   = now.getFullYear();

				if(d < 10){
					d = "0"+d;
				}
				if(m < 10){
					m = "0"+m;
				}
                window.mntFiltros.usmnt_fecha_fin    = d+"-"+m+"-"+y;
                window.mntFiltros.usmnt_fecha_inicio = d+"-"+m+"-"+y;
        }
   		//$("#fecha_inicio").val(window.mntFiltros.usmnt_fecha_inicio);
    	//$("#fecha_fin").val(window.mntFiltros.usmnt_fecha_fin);
        */
        $("#fecha_inicio").val("");
        $("#fecha_inicio").change();
        $("#fecha_fin").val("");
        $("#fecha_fin").change();

    	$('#fecha_inicio,#fecha_fin').datepicker({
		    format: "dd-mm-yyyy",
		    viewMode: "days",
		    minViewMode: "days",
		    language: "es"
		});

        

/*        $('select[name=mant_estado]').val("");
        $('select[name=mant_estado]').change();
        $('select[name=mant_estado]').selectpicker('refresh');
  */
        $('.selectpicker').selectpicker('val', ['CREADA', 'ASIGNADA', 'ASIGNANDO', 'VALIDANDO', 'EJECUTANDO']);      
        $('select[name=mant_estado]').change();
        $('select[name=mant_estado]').selectpicker('refresh');

        $('select[name=mant_responsable]').val("");
        $('select[name=mant_responsable]').change();

        $('#empl_nombre').val("");
        $('#empl_nombre').change();

        $('select[name=clas_id]').val("");
        $('select[name=clas_id]').change();

        $('select[name=empl_macrositio]').val("");
        $('select[name=empl_macrositio]').change();

        $('select[name=empl_subtel]').val("");
        $('select[name=empl_subtel]').change();

        $('select[name=espe_id]').val("");
        $('select[name=espe_id]').change();

        $('select[name=usua_id]').val("");
        $('select[name=usua_id]').change();

        $('select[name=subg_id]').val("");
        $('select[name=subg_id]').change();

        $('select[name=zona_id]').val("");
        $('select[name=zona_id]').change();

        $('select[name=regi_id]').val("");
        $('select[name=regi_id]').change();

        $('select[name=clus_id]').val("");
        $('select[name=clus_id]').change();

        $('select[name=usua_creador]').val("");
        $('select[name=usua_creador]').change();

        localStorage.clear();
        window.localStorage.clear();
        
    });

    /*** Boton Buscar ***/
    $(document).on('click', 'button#BuscarMntBandeja', function (e) {
        $("#siom-form-mnt-bandeja").submit();
        
    });


	//Actualización de fechas
	setInterval(function(){
		$(".timeago").timeago();
	},60000);

	//funciones utiles
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-mnt-bandeja").submit();
         },filterTimeout);
	}

    //FILTROS
    console.log("FILTROS");	
    $('.selectpicker').selectpicker();
    $('.selectpicker').selectpicker('val', ['CREADA', 'ASIGNADA', 'ASIGNANDO', 'VALIDANDO', 'EJECUTANDO']);


    if(localStorage.getItem("mant_fecha_inicio")){
        console.log(localStorage.getItem("mant_fecha_inicio"));
        
        $("#fecha_inicio").selectpicker();
        dt = localStorage.getItem("mant_fecha_inicio").split("-");
        if(parseInt(dt[0])<=31){
            $("#fecha_inicio").val(dt[0]+"-"+dt[1]+"-"+dt[2]);
            console.log(dt[0]+"-"+dt[1]+"-"+dt[2]);
        } else {
            $("#fecha_inicio").val(dt[2]+"-"+dt[1]+"-"+dt[0]);
            console.log(dt[2]+"-"+dt[1]+"-"+dt[0]);
        }

        
        //$("#fecha_inicio").val(localStorage.getItem("mant_fecha_inicio"));
    }

    if(localStorage.getItem("mant_fecha_fin")){
        console.log(localStorage.getItem("mant_fecha_fin"));
        
        $("#fecha_fin").selectpicker();
        dt = localStorage.getItem("mant_fecha_fin").split("-");
        if(parseInt(dt[0])<=31){
            $("#fecha_fin").val(dt[0]+"-"+dt[1]+"-"+dt[2]);
            console.log(dt[0]+"-"+dt[1]+"-"+dt[2]);
        } else {
            $("#fecha_fin").val(dt[2]+"-"+dt[1]+"-"+dt[0]);
            console.log(dt[2]+"-"+dt[1]+"-"+dt[0]);
        }

        
        //$("#fecha_fin").val(localStorage.getItem("mant_fecha_fin"));
    }
    if(localStorage.getItem("mant_peri_id")){
        console.log(localStorage.getItem("mant_peri_id"));
        $('#siom-form-mnt-bandeja select[name=peri_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=peri_id]').selectpicker('val', localStorage.getItem("mant_peri_id"));
    }
    if(localStorage.getItem("mant_id")){
        console.log(localStorage.getItem("mant_id"));
        $('#siom-form-mnt-bandeja input[name=mant_id]').selectpicker();
        $('#siom-form-mnt-bandeja input[name=mant_id]').selectpicker('val', localStorage.getItem("mant_id"));
    }


    if(localStorage.getItem("mant_estado")){
        console.log(localStorage.getItem("mant_estado"));
        $('#siom-form-mnt-bandeja select[name=mant_estado]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=mant_estado]').selectpicker('val', localStorage.getItem("mant_estado"));
    }

    if(localStorage.getItem("mant_responsable")){
        console.log(localStorage.getItem("mant_responsable"));
        $('#siom-form-mnt-bandeja select[name=mant_responsable]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=mant_responsable]').selectpicker('val', localStorage.getItem("mant_responsable"));
    }

    if(localStorage.getItem("mant_empl_nombre")){
        console.log(localStorage.getItem("mant_empl_nombre"));
        $('#siom-form-mnt-bandeja input[name=empl_nombre]').val(localStorage.getItem("mant_empl_nombre"));
    }

    if(localStorage.getItem("mant_clas_id")){
        console.log(localStorage.getItem("mant_clas_id"));
        $('#siom-form-mnt-bandeja select[name=clas_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=clas_id]').selectpicker('val', localStorage.getItem("mant_clas_id"));
    }

    if(localStorage.getItem("mant_empl_macrositio")){
        console.log(localStorage.getItem("mant_empl_macrositio"));
        $('#siom-form-mnt-bandeja select[name=empl_macrositio]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=empl_macrositio]').selectpicker('val', localStorage.getItem("mant_empl_macrositio"));
    }

    if(localStorage.getItem("mant_empl_subtel")){
        console.log(localStorage.getItem("mant_empl_subtel"));
        $('#siom-form-mnt-bandeja select[name=empl_subtel]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=empl_subtel]').selectpicker('val', localStorage.getItem("mant_empl_subtel"));
    }

    if(localStorage.getItem("mant_espe_id")){
        console.log(localStorage.getItem("mant_espe_id"));
        $('#siom-form-mnt-bandeja select[name=espe_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=espe_id]').selectpicker('val', localStorage.getItem("mant_espe_id"));
    }

    if(localStorage.getItem("mant_usua_id")){
        console.log(localStorage.getItem("mant_usua_id"));
        $('#siom-form-mnt-bandeja select[name=usua_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=usua_id]').selectpicker('val', localStorage.getItem("mant_usua_id"));
    }

    if(localStorage.getItem("mant_subg_id")){
        console.log(localStorage.getItem("mant_subg_id"));
        $('#siom-form-mnt-bandeja select[name=subg_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=subg_id]').selectpicker('val', localStorage.getItem("mant_subg_id"));
    }

    if(localStorage.getItem("mant_zona_id")){
        console.log(localStorage.getItem("mant_zona_id"));
        $('#siom-form-mnt-bandeja select[name=zona_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=zona_id]').selectpicker('val', localStorage.getItem("mant_zona_id"));
    }

    if(localStorage.getItem("mant_regi_id")){
        console.log(localStorage.getItem("mant_regi_id"));
        $('#siom-form-mnt-bandeja select[name=regi_id]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=regi_id]').selectpicker('val', localStorage.getItem("mant_regi_id"));
    }

    if(localStorage.getItem("mant_clus_id")){
        console.log(localStorage.getItem("mant_clus_id"));
        $('#siom-form-mnt-bandeja select[name=clus_id]').selectpicker()
        $('#siom-form-mnt-bandeja select[name=clus_id]').selectpicker('val', localStorage.getItem("mant_clus_id"));
    }

    if(localStorage.getItem("mant_usua_creador")){
        console.log(localStorage.getItem("mant_usua_creador"));
        $('#siom-form-mnt-bandeja select[name=usua_creador]').selectpicker();
        $('#siom-form-mnt-bandeja select[name=usua_creador]').selectpicker('val', localStorage.getItem("mant_usua_creador"));
    }

    //Eventmnt
    console.log("EVENT");
    
    $('#siom-form-mnt-bandeja input[name=mant_id]').keyup(function () {
        localStorage.setItem("mant_id", $("input[name=mant_id]").val());
    })

    $('#fecha_inicio,#fecha_fin').change(function(){
        localStorage.setItem("mant_fecha_inicio", $("#fecha_inicio").val());
        localStorage.setItem("mant_fecha_fin", $("#fecha_fin").val());

    })
    

    $('#siom-form-mnt-bandeja select[name=mant_estado]').change(function(){
        localStorage.setItem("mant_estado", $("select[name=mant_estado]").val());
    })

    $('#siom-form-mnt-bandeja select[name=mant_responsable]').change(function(){
        localStorage.setItem("mant_responsable", $("select[name=mant_responsable]").val());
    })

    $('#siom-form-mnt-bandeja select[name=zona_id]').change(function(){
        localStorage.setItem("mant_zona_id", $("select[name=zona_id]").val());
    })

    $('#siom-form-mnt-bandeja select[name=regi_id]').change(function(){
        localStorage.setItem("mant_regi_id", $("select[name=regi_id]").val());
    })

    $('#siom-form-mnt-bandeja select[name=clus_id]').change(function(){
        localStorage.setItem("mant_clus_id", $("select[name=clus_id]").val());
    })

    $('#siom-form-mnt-bandeja select[name=empl_macrositio]').change(function(){
        localStorage.setItem("mant_empl_macrositio", $("select[name=empl_macrositio]").val());
    })

    $('#siom-form-mnt-bandeja select[name=empl_subtel]').change(function(){
        localStorage.setItem("mant_empl_subtel", $("select[name=empl_subtel]").val());
    })

    $('#siom-form-mnt-bandeja select[name=peri_id]').change(function(){
        localStorage.setItem("mant_peri_id", $("select[name=peri_id]").val());
    })

    $('#siom-form-mnt-bandeja select[name=clas_id]').change(function(){
        localStorage.setItem("mant_clas_id", $("select[name=clas_id]").val());
    })
    
    $('#siom-form-mnt-bandeja select[name=espe_id]').change(function(){
        localStorage.setItem("mant_espe_id", $("select[name=espe_id]").val());
    })

    $('#siom-form-mnt-bandeja input[name=empl_nombre]').keyup(function () {
        localStorage.setItem("mant_empl_nombre", $("input[name=empl_nombre]").val());
    })

    $('#siom-form-mnt-bandeja select[name=usua_creador]').change(function(){
        localStorage.setItem("mant_usua_creador", $("select[name=usua_creador]").val());
    })

    $('#siom-form-mnt-bandeja select[name=usua_id]').change(function(){
        localStorage.setItem("mant_usua_id", $("select[name=usua_id]").val());
    })

    $('#siom-form-mnt-bandeja select[name=subg_id]').change(function(){
        localStorage.setItem("mant_subg_id", $("select[name=subg_id]").val());
    })

    $("#mant_id").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^0-9]/gi,''));
    });

    $("#mant_id").attr('maxlength','11');

    $("#empl_nombre").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^a-z0-9ñÑ°.-\s]/gi, ''));
    });

    $("#empl_nombre").attr('maxlength','100');
    
})(jQuery);
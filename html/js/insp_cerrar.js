(function($) { 
	$('#siom-cerrar-form button').click(function(e) {
        form   = $('#siom-cerrar-form');
        estado = $(this).data("insp-estado");
        form.find(':input[name="insp_estado"]').val(estado);
    });

    $("#siom-cerrar-form").submit(function(e){
    	 form   = $(this);
    	 estado = form.find(':input[name="insp_estado"]').val();

    	 if(estado=="RECHAZADA" || estado=="ACEPTADA_REPAROS"){
    	 	if(form.find(':input[name="insp_observacion"]').val()==""){
    	 		alert("Debe ingresar una observación");
    	 		return false;
    	 	}
    	 }
    	 return true;
    });
})(jQuery);
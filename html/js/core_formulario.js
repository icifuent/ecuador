(function($) {

    $('#FormFormularioLista select[name=form_estado]').val('ACTIVO');
    $('#FormFormularioLista select[name=form_estado]').change();
	var filterTimer=null;
	var filterTimeout=500;
    $('.selectpicker').selectpicker();
   
    $('#FormFormularioLista input[name=form_nombre]').keyup(function() {
        FiltrarConDelay();
    })
    $('#FormFormularioLista select[name=cont_id]').change(function() {
        $("#FormFormularioLista").submit();
    })
    $('#FormFormularioLista select[name=form_estado]').change(function() {
        $("#FormFormularioLista").submit();
    })
    
    $(document).on('click', '#Wizardformulario li#Primero', function(e) {
        $("a#BotonFormAgregar").show();
        $("#FormFormularioLista").submit();
    });

  

    //BOTONES___________________________________________________________________
    $(document).on('click', 'a.add-form', function(e) {
        $("a#BotonFormAgregar").hide();
        $('#Wizardformulario').wizard('selectedItem', {step:2});
        $("#core-formulario-form").empty();
        $("#core-formulario-form").text("Cargando...");

        CargarFormulario({form:{cont_id:$(this).data("cont-id")}});
    });

    $(document).on('click', '#BotonFormEditar', function(e) {
        $("a#BotonFormAgregar").hide();
        $('#Wizardformulario').wizard('selectedItem', {step:2});
        $("#core-formulario-form").empty();
        $("#core-formulario-form").text("Cargando...");

        form_id = $(this).data("form-id");
        $.get('rest/core/formulario/get/'+form_id,{},function(json) {
            if(json.status){
                CargarFormulario(json.data);
            }
            else{
                alert(json.error);
            }
        
        }).fail(function(xhr, textStatus, errorThrown){
            alert("Error "+xhr.status+": "+xhr.statusText);
        });
    });
    

    $(document).on('click', '#BotonFormCancelar', function(e) {
        $("a#BotonFormAgregar").show();
        $('#Wizardformulario').wizard('selectedItem', {step:1});
        $("#FormFormularioLista").submit();
    });


    $(document).on('click','button#BotonFormCambiarEstado',function(e){
        form_id     = $(this).data("id");
        form_nombre = $(this).data("nombre");
        form_estado = $(this).data("estado");
           
        if (form_estado=="ACTIVO"){
            nuevo_estado        = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
        }
        else{
            nuevo_estado        = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("El Formulario <b>"+form_nombre+"</b> será <b>"+nombre_nuevo_estado+"</b>,<br>Desea continuar?",function(status){
              if(status==true){
                    $.get('rest/core/formulario/upd/'+form_id,{form_estado:nuevo_estado},function(json) {
                        $("#FormFormularioLista").submit();
                    
                    }).fail(function(xhr, textStatus, errorThrown){
                        alert("Error "+xhr.status+": "+xhr.statusText);
                    });
              }
        });

    });


   
    //FUNCIONES AUXILIARES______________________________________________________
    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormFormularioLista").submit();
        }, filterTimeout);
    }

    function CargarFormulario(data){
        $.ajax({
          url: "templates/form_edicion.hb",
          async: false,
          success: function(src){
            template = Handlebars.compile(src);
            html = template(data);
            $("#core-formulario-form").html(html);
          },
        });

        return html;
    }
})(jQuery);

(function($) {
    var filterTimer=null;
    var filterTimeout=500;

    $('.selectpicker').selectpicker();

    $('#WizardUsuario').wizard('destroy'); //PREGUNTAR
    $('#WizardCurso').wizard('destroy');
    $('#WizardEmpresa').wizard('destroy');
    $('#WizardContratos').wizard('destroy');
    $('#WizardEmplazamiento').wizard();

    $('#FormEmplazamientoLista input[name=empl_nombre]').keyup(function () {
         FiltrarConDelay();
    })
    $('#FormEmplazamientoLista input[name=empl_nemonico]').keyup(function () {
         FiltrarConDelay();
    })
    $('#FormEmplazamientoLista select[name=empl_estado]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=empl_macrositio]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=empl_subtel]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=tecn_id]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista input[name=empl_direccion]').keyup(function () {
        FiltrarConDelay();
    })
    $('#FormEmplazamientoLista select[name=regi_id]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=cate_id]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=clas_id]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=duto_id]').change(function () {
        $("#FormEmplazamientoLista").submit();
    })
    
    /*
    $('#FormEmplazamientoLista select[name=orse_indisponibilidad]').change(function(){
        $("#FormEmplazamientoLista").submit();
    })    
    $('#FormEmplazamientoLista select[name=zona_id]').change(function(){
        $("#FormEmplazamientoLista").submit();
    })
    $('#FormEmplazamientoLista select[name=clus_id]').change(function(){
        $("#FormEmplazamientoLista").submit();
    })
    */

    $(document).on('click', '#WizardEmplazamiento li#Primero', function (e) {
        $("#AgregarEmplazamiento").show();
        $('div#siom-empl-detalle-mapa').remove();
        $("#FormEmplazamientoLista").submit();
    });

    $(document).on('click', '#WizardEmplazamiento li#Segundo', function (e) {

    });
    


    //BOTONES___________________________________________________________________
    
    //Emplazamiento Agregar
    $(document).on('click', 'a#BotonEmplazamientoAgregar', function (e) {
        $("#AgregarEmplazamiento").hide();
        $("#FormEmplazamientoEditar").trigger('reset');
        $('.spEmplEditar').selectpicker('refresh');
        $("input#empl_id").val("");
        $('input#usua_creador').val(window.user_id);
        $("#BotonEmplazamientoContrato").addClass("disabled");
        $('#WizardEmplazamiento').wizard('selectedItem', {step:2});
    });

    //Emplazamiento Editar
    $(document).on('click', 'button#BotonEmplazamientoEditar', function (e) {
        $("#AgregarEmplazamiento").hide();
        $("#BotonEmplazamientoContrato").removeClass("disabled");
        
        $('#WizardEmplazamiento').wizard('selectedItem', {step:2});
        
        CargarDatosEmplazamiento($(this).data("id"),$(this).data("data"));   
    });
    
    
        //Emplazamiento cambiar estado
    $(document).on('click', 'button#BotonEmplazamientoCambiarEstado', function(e) {
      
        empl_id = $(this).data("id");
        empl_nombre = $(this).data("nombre");
        empl_estado = $(this).data("estado");

                if (empl_estado == "ACTIVO") {
            nuevo_estado = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
        }
        else {
            nuevo_estado = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("El Emplazamiento <b>" + empl_nombre + "</b> sera <b>" + nombre_nuevo_estado + "</b>,<br>Desea continuar?", function(status) {
            if (status == true) {
                $.get('rest/core/emplazamiento/upd/' + empl_id, {empl_estado: nuevo_estado}, function(json) {
                    if (json.status == 0) {
                        alert("Error guardando emplazamiento:" + json.error + " , ruta:" + url);
                    }                       
                    $("#FormEmplazamientoLista").submit();
                }).fail(function(xhr, textStatus, errorThrown) {
                    alert("Error " + xhr.status + ": " + xhr.statusText);
                });
            }
        });
    });

    //Emplazamiento guardar
    $(document).on('click', 'button#BotonEmplazamientoGuardar', function (e) {
        //Comprobacion de valores de variables
        form = $("#FormEmplazamientoEditar");
        btn  = $(this);
        
        var valor;
        valor = form.find(':input[name="tecn_id"]').val();
        if (valor == null) {
            alert("Debe seleccionar al menos una tecnologia");
            return false;
        }
        
        valor = form.find(':input[name="empl_nombre"]').val();
        if (valor == "") {
            alert("Debe ingresar un nombre");
            return false;
        }
        
        valor = form.find(':input[name="empl_nemonico"]').val();
        if (valor == "") {
            alert("Debe ingresar un nemonico");
            return false;
        }
        
        valor = form.find(':input[name="empl_direccion"]').val();
        if (valor == "") {
            alert("Debe ingresar una direccion");
            return false;
        }
       
       
        //Hacemos el POST
        var data = {};
        $.each(form.serializeArray(), function(i, field) {
            if( field.name == "tecn_id" ){
                if( !data["tecn_id"] ) data["tecn_id"] = [];
                data["tecn_id"].push(field.value);
            }
            else{
                data[field.name] = field.value;
            }
        });

        var url = 'rest/core/emplazamiento/add';
        if (data.empl_id > 0) {
            url = 'rest/core/emplazamiento/upd/' + data.empl_id;
        }
        else {
            delete data["empl_id"];
        }
        
        
        btn.button('loading');
        $.post(url, data, function (json) {   
            btn.button('reset');
            
            if (json.status == 0) {
                alert("Error guardando emplazamiento:" + json.error + " , ruta:" + url);
                return false;
            }             
            if (json.data[0].id > 0) {
                $('#empl_id').val(json.data[0].id);
            }            
            
            $("#BotonEmplazamientoContrato").removeClass("disabled");
            
            //Actualizar mapa
            $('#siom-empl-detalle-mapa').remove();
            $('i#ubicacion').append($('<div id="siom-empl-detalle-mapa" class="siom-detalle-mapa" data-latitud="'+data.empl_latitud+'" data-longitud="'+data.empl_longitud+'" data-distancia="'+data.empl_distancia+'"></div>'));
            contenedorMapa = $("#siom-empl-detalle-mapa");
            setMap(contenedorMapa, contenedorMapa.data("latitud"), contenedorMapa.data("longitud"), contenedorMapa.data("distancia"));
            
            alert("Emplazamiento guardado exitosamente");

        }).fail(function () {
            alert("Error guardando emplazamiento: Error en POST, ruta:" + url);
        });
        
        return false;        
    });
    
    //Emplazamiento guardar
    $(document).on('click', 'button#BotonEmplazamientoGuardar', function (e) {
        $('#WizardEmplazamiento').wizard('selectedItem', {step:1});
    });
    
    
    
    //Emplazamiento Contratos 
    $(document).on('click', 'button#BotonEmplazamientoContrato', function (e) {
        $('#WizardEmplazamiento').wizard('selectedItem', {step:3});
        CargaDatosContratosZonas();
    });  

    $(document).on('click', 'button#BotonAgregarEmplazamientoContrato', function (e) {
        option = $("#ListaContratos").find("option:selected");
        empl_id = $('#empl_id').val();
        cont_id = option.val();
        cont_nombre = option.data("name");
            
        $("#TabsContratosEmplazamiento").html("");
        $("#TabsContentContratosEmplazamiento").html("");
        ModificaContrato(cont_id, empl_id,"add",function(status){
            if(status){
                AgregarContrato(empl_id,cont_id,cont_nombre);
            }
        })      
        
        option.remove();
        $("#ListaContratos").selectpicker('refresh');
    });  
    
    
    $(document).on('click', '#TabsContratosEmplazamiento .remove', function (e) {
        empl_id = $('#empl_id').val();
        cont_id   = $(this).data("id");
        cont_nombre = $(this).data("name");
        
        confirm("¿Desea eliminar a emplazamiento de contrato '"+cont_nombre+"'?",function(status){
            if(status){
                ModificaContrato(cont_id, empl_id,"del",function(status){
                    if(status){
                        EliminarContrato(cont_id);
                    }
                })
                
            }
            
        });
        
    });  
    
    //Emplazamiento Contratos Zonas/MNT
    $(document).on('click',"form.emplazamiento_contrato_zonas button[type='submit']",function(event){
        var btn  = $(this);
        var form = $("#FormEmplazamientoContratosZonas_"+btn.data("cont-id"));
        console.log(form);
        
        var data = {};
        $.each(form.serializeArray(), function(i, field) {
            if( field.name == "zonas" ){
                if( !data["zonas"] ) data["zonas"] = [];
                data["zonas"].push(field.value);
            }
            else{
                data[field.name] = field.value;
            }
        });
        if( !data["rece_mpp"] ) data["rece_mpp"] = "0";
        
        btn.button('loading');
        $.post("rest/core/emplazamiento/"+data.empl_id+"/contrato/"+data.cont_id+"/mantenimiento_zonas", data, function (json) {   
            btn.button('reset');
            console.log(json);
            
            if (json.status == 0) {
                alert("Error guardando configuración:" + json.error);
            }             
        }).fail(function () {
            btn.button('reset');
            alert("Error guardando configuración");
        });
    });

    
    //____________________________________________________________________________________________________________
    
    function CargarDatosEmplazamiento(empl_id,data){
        $.each(data, function (key, value) {
            if ("tecn_id" === key) {
                value = value.split(",");
            }
            $('#' + key).val(value);
            if ("empl_latitud" === key) {
                latitud = value;
            }
            else if ("empl_longitud" === key) {
                longitud = value;
            }
            else if ("empl_distancia" === key) {
                distancia = value;
            }
        });

        $('input#empl_id').val(data["empl_id"]);
        $('input#usua_creador').val(data["usua_creador"]);
        $('.spEmplEditar').selectpicker('refresh');

         $('#siom-empl-detalle-mapa').remove();
         $('i#ubicacion').append($('<div id="siom-empl-detalle-mapa" class="siom-detalle-mapa" data-latitud="'+latitud+'" data-longitud="'+longitud+'" data-distancia="'+distancia+'">Cargando...</div>'));
         setMap($("#siom-empl-detalle-mapa"),latitud,longitud, distancia);
    }
    
     
    function CargaDatosContratosZonas() {         
        empl_id = $('#empl_id').val();
        
        $("#TabsContratosEmplazamiento").html("");
        $("#TabsContentContratosEmplazamiento").html("");
        $("#ListaContratos").html("");
//        var contratoLista = $("#ListaContratos").length;
//        while(contratoLista.length > 0){
//            $("#ListaContratos").remove(contratoLista-1);
//            contratoLista = $("#ListaContratos").length;
//            console.log("***Lista de contratos***" + $("#ListaContratos"));
//        }
//        $("#ListaContratos") = [];
        $("#ListaContratos").selectpicker('refresh');
        
//      $.get('rest/core/contrato/list', null, function (json) {
        $.get('rest/core/emplazamiento/contratos/listar', null, function (json) {
            $("#ListaContratos").html("");
            $("#ListaContratos").selectpicker('refresh');
             if (json.status) {
                $.each(json.data, function (i, contrato) {  
                    html = '<option value="' + contrato["cont_id"] + '" data-name="' + contrato["cont_nombre"] + '">' +
                            contrato["cont_nombre"]+
                            '</option>';
                    $("#ListaContratos").append(html); 
                }); 
                $("#ListaContratos").selectpicker('refresh');
                CargaListaContratosEmplazamiento(empl_id);                
            }
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener la lista de contratos");
        });
    }
    
    
    function CargaListaContratosEmplazamiento(empl_id) { 
        
        $.get('rest/core/emplazamiento/' + empl_id + '/contrato/list', null, function (json) {
            if (json.status) {

                $("#TabsContratosEmplazamiento").html("");   
                
                var contracts = json.data;
                if(0<contracts.length){
                    var index     = 0;

                    function loadContract(){
                        if(index < contracts.length){
                            var contrato = contracts[index];
                            $("#TabsContratosEmplazamiento").html("");
                            $("#TabsContentContratosEmplazamiento").html("");
                            AgregarContrato(empl_id,contrato["cont_id"],contrato["cont_nombre"],function(status){
                                if(status){
                                    $("#ListaContratos").find('[value='+contrato["cont_id"]+']').remove();
                                    index++;
                                    loadContract();
                                }
                            });
                            
                        }
                        else{
                            $("#ListaContratos").selectpicker('refresh');
                        }
                    }   
                    loadContract();
                }
                
                /*
                $.each(json.data, function (i, contrato) {
                    AgregarContrato(empl_id,contrato["cont_id"],contrato["cont_nombre"]);

                    
                    $("#ListaContratos").find('[value='+contrato["cont_id"]+']').remove();
                    $("#ListaContratos").selectpicker('refresh');
                });
                */
                
                
                
            }
            else{
                alert("No se pudo cargar contratos de emplazamiento: "+json.error);
            }
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener los contratos del emplazamiento");
        });
    }
    
    function ModificaContrato(cont_id, empl_id, operacion,callback){ 
        var url = 'rest/core/contrato/' + cont_id + '/emplazamiento/'+ operacion + '/' + empl_id;        
        $.get(url, null, function(json) {
            if (json.status) {
                callback(true);
            }
            else{
                alert("No se pudo guardar el contrato: " + json.error);
                callback(false);
            }
        }).fail(function () {
            alert("Error guardando contrato: Error en GET, ruta:" + url);
            callback(false);
        });
    }
    
    //FUNCIONES AUXILIARES______________________________________________________
    function AgregarContrato(empl_id,id,nombre,callback){
        console.log("Agregando contrato",empl_id,id,nombre)

        
        tab     = $("#TabsContratosEmplazamiento");
        content = $("#TabsContentContratosEmplazamiento");
            
        $('<li id="tab_'+id+'" data-name="'+nombre+'"><a href="#tab_content'+id+'" data-toggle="tab">'+nombre+' <span class="glyphicon glyphicon-trash btn-link remove" aria-hidden="true" data-id="'+id+'" data-name="'+nombre+'"></span></a></li>').appendTo(tab);

        data = {empl_id:empl_id,cont_id:id};
        
        $.get('rest/core/zona/filtros', null, function (json) {
            if (json.status) {
                zonas_tipos = json.tipos;
                console.log(zonas_tipos);
                
                $.get('rest/core/emplazamiento/' + data.empl_id + '/contrato/' + data.cont_id + '/zona/list?zona_estado=ACTIVO', null, function (json) {
                    if (json.status) {                        
                        zonas_emplazamiento = json.data;
                        console.log(zonas_emplazamiento);

                        $.get('rest/core/zona/list?zona_estado=ACTIVO&cont_id= ' + data.cont_id, null, function(json) {
                            if (json.status) {
                                zonas_contrato = json.data;
                                console.log(zonas_contrato);
                                
                                
                                $.get('rest/core/emplazamiento/' + data.empl_id + '/contrato/' + data.cont_id + '/mantenimiento', null, function(json) {
                                    if (json.status) {
                                        data.mantenimiento = json.data;
                                        console.log(data.mantenimiento);
                                
                                        zonas = {}
                                
                                        $.each(zonas_tipos, function(i, zona_tipo) {
                                            zonas[zona_tipo] = [];
                                        });

                                        $.each(zonas_contrato, function(i, zona_contrato) {

                                            var isSelected = false;
                                            $.each(zonas_emplazamiento, function(j, zona_emplazamiento) {
                                                if (zona_emplazamiento['zona_id'] == zona_contrato["zona_id"]) {
                                                    isSelected = true;
                                                    delete zona_emplazamiento['zona_id'];
                                                }
                                            });

                                            zonas[zona_contrato["zona_tipo"]].push({
                                                id: zona_contrato['zona_id'],
                                                name: zona_contrato["zona_nombre"],
                                                selected: isSelected
                                            });
                                        });

                                        data.zonas = zonas;
                                        
                                        $.ajax({
                                              url: "templates/core_emplazamieto_contrato.hb",
                                              async: true,
                                              success: function(src){
                                                    console.log("Cargando template",data);
                                            
                                                    template = Handlebars.compile(src);
                                                    html = template(data);

                                                    $('<div class="tab-pane" id="tab_content'+id+'">'+html+'</div>').appendTo(content);

                                                    $("#FormEmplazamientoContratosZonas_"+id+" select.selectpicker").selectpicker("render");
                                                    tab.find('a:last').tab('show');
                                            
                                                    callback(true);
                                                }
                                        }); 
                                    }
                                    else{
                                        alert(json.error);
                                        callback(false);
                                    }
                                }).fail(function() {
                                    alert("Contratos y Zonas: No se pudo obtener configuracion de mantenimiento");
                                    callback(false);
                                });
                            }
                            else{
                                alert(json.error);
                                callback(false);
                            }
                                                        
                        }).fail(function() {
                            alert("Contratos y Zonas: No se pudo obtener las zonas");
                            callback(false);
                        });

                    }
                    else{
                        alert(json.error);
                        callback(false);
                    }
                }).fail(function() {
                    alert("Contratos y Zonas: No se pudo obtener las zonas");
                    callback(false);
                });
            }
            else{
                alert(json.error);
                callback(false);
            }
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener las zonas");
            callback(false);
        });
    }
    
    function EliminarContrato(id){
        console.log("Eliminar contrato "+id)
        tab = $("#tab_"+id);
        
        html = '<option value="' + id + '" data-name="' + tab.data("name") + '">' + tab.data("name")+ '</option>';
        $("#ListaContratos").append(html); 
        $("#ListaContratos").selectpicker('refresh');

        
        tab.remove();
        $("#tab_content_"+id).remove();
        $("#TabsContratosEmplazamiento").find('a:last').tab('show');
    }
    
    function setMap(element, lat, lng, geofence) {
        if ((typeof google != "undefined") && lat != "" && lng != "") {

            geofence_enabled = window.config['geofence_enabled'];
            if (geofence_enabled == null) {
                geofence_enabled = false;
            }

            var gMap = null;
            var gMarker = null;
            var gMarkerGeofence = null;
            var latlng = new google.maps.LatLng(lat, lng);

            if (!gMap) {
                var mapOptions = {
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                gMap = new google.maps.Map(element[0], mapOptions);
                gMarker = new google.maps.Marker({map: gMap});

                if (geofence_enabled) {
                    gMarkerGeofence = new google.maps.Circle({
                        map: gMap,
                        radius: 0,
                        strokeColor: '#AA0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 1,
                        fillColor: '#AA0000',
                        fillOpacity: 0.35,
                    });
                }
            }

            google.maps.event.trigger(gMap, 'resize');
            gMarker.setPosition(latlng);

            if (gMarkerGeofence) {
                if (geofence != "" && 0 < geofence) {
                    gMarkerGeofence.setRadius(parseInt(geofence));
                }
                else {
                    gMarkerGeofence.setRadius(0);
                }
                gMarkerGeofence.bindTo('center', gMarker, 'position');
            }
            gMap.setCenter(latlng);
        }
    }

    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormEmplazamientoLista").submit();
        }, filterTimeout);
    }


})(jQuery);

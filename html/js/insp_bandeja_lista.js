(function($) {

$('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/insp/bandeja/filtro/'+page,data);
});

$("button#download").on("click",function(e){
	 console.log(e);
      $btn = $(this);
      $btn.button("loading");
      $.fileDownload("rest/core/repo/contrato/"+window.contract+"/insp/bandeja",{httpMethod:"POST",data:$.param($btn.data("filters")),
          prepareCallback:function(url) {
              $btn.button("processing");
          },
          successCallback: function(url) {
              $btn.button('reset')
          },
          failCallback: function(responseHtml, url) {
              $btn.button('reset')
              alert(responseHtml);
          }
      });
    });


$("a.siom-anular-insp").on("click",function(e){
	e.stopImmediatePropagation();
	insp_id = $(this).data("insp-id");
	confirm("¿Desea anular inspección N° "+insp_id+"?,<br>Esto cancelara las tareas asociadas.",function(status){
		if(status){
			url = 'rest/contrato/'+window.contract+'/insp/del/'+insp_id;
			$.post(url,{},function(json,textStatus) {
				if(json.status){
					setTimeout(function(){
						window.app.runRoute('post','#/insp/bandeja/filtro/1',window.inspBandejaFiltros);
					},200);
					alert("Inspección anulada exitosamente");
				}	
				else{
					alert(json.error)
				}
			},'json').fail(function(){ 
				alert("No se pudo enviar anulacion");
			});
		}

	})
});

})(jQuery);

(function($) {
    function drawCharts(){
        chart_os_espe  = $("#siom-template-chart-pie");
        data_json      = chart_os_espe.data("data");
        data_array     = [["cumple","no cumple"]];
        ;
        if(data_json.length==0){
          chart_os_espe.text("No hay datos");
        }
        else{

          for(i=0;i<data_json.length;i++){
              data_array.push([data_json[i]['nombre']+" ("+data_json[i]['valor']+")",parseInt(data_json[i]['valor'])])       
          }
          var options1 = {
            title: '',
            titleTextStyle:{fontSize: 12},
            is3D: false,
            pieSliceText: 'label',
            pieSliceTextStyle:{fontSize: 8},
            chartArea: {left:20,top:20,width:'90%',height:'75%'},
            legend:{position: 'right',alignment: 'center', textStyle: {color: 'black', fontSize: 10}},
          slices: {  0: {offset: 0.1}
                    
          },
          };

          var chart1 = new google.visualization.PieChart(chart_os_espe[0]);
          chart1.draw(google.visualization.arrayToDataTable(data_array), options1);
        }

       

    }

    if(typeof google.visualization != "undefined" && 
       typeof google.visualization.PieChart != "undefined" &&
        typeof google.visualization.ColumnChart != "undefined"){
      drawCharts();
    }
    else{
      google.setOnLoadCallback(drawCharts);
    }
    

})(jQuery);


(function($) { 
	var filterTimer=null;
	var filterTimeout=500;
	var numerosCajas = 0;

	//inicializar componentes
    $('.selectpicker').selectpicker();
  
    $('#fecha_solicitud').datepicker({
			    format: "dd-mm-yyyy",
			    viewMode: "days", 
			    minViewMode: "days",
			    language: "es"
			});
     

     $('#addItem').click(function(e){
                                
       $('#item').append('<input type="hidden" name="inec_id[]" value=""></input>'+
                            '<div class="form-group">'+
                            '<label class="col-sm-2 control-label">Genero</label>'+
                            '<div class="col-sm-10">'+
                            '<input type="text" class="form-control" id="inec_genero'+numerosCajas+'" name="inec_genero[]"></input>'+
                            '</div>'+
                            '</div>'+

                            '<div class="form-group">'+
                            '<label class="col-sm-2 control-label">Especie</label>'+
                            '<div class="col-sm-10"><input type="text" class="form-control" id="inec_especie'+numerosCajas+'" name="inec_especie[]"></input>'+
                            '</div>'+
                            '</div>'+

                            '<div class="form-group">'+
                            '<label class="col-sm-2 control-label">Campo</label>'+
                            '<div class="col-sm-10">'+
                            '<input type="text" class="form-control" id="inec_campo'+numerosCajas+'" name="inec_campo[]"></input>'+
                            '</div>'+
                            '</div>'+

                            '<div class="form-group">'+
                            '<label class="col-sm-2 control-label">Valor</label>'+
                            '<div class="col-sm-10"><input type="text" class="form-control" id="inec_valor'+numerosCajas+'" name="inec_valor[]"></input>'+
                            '</div>'+
                            '</div>'+

                            '<hr>');
           numerosCajas++;
     });


    //inicializar eventos
    $('#siom-form-add-item').submit(function (e) {

        var valor;
        valor = $(this).find(':input[name="inel_codigo"]').val();
        
        if (valor == "") {
            alert("falta el campo codigo");
            return false;
        }
        valor = $(this).find(':input[name="inel_nombre"]').val();
        if (valor == "") {
            alert("Debe llenar campo nombre");
            return false;
        }
        valor = $(this).find(':input[name="inel_ubicacion"]').val();
        if (valor == "") {
            alert("Debe llenar campo ubicacion");
            return false;
        }
        valor = $(this).find(':input[name="inel_descripcion"]').val();
        if (valor == "") {
            alert("Debe llenar campo Descripcion");
            return false;
        }
        if (numerosCajas>0) {
            
            for (var i = 0; i < numerosCajas; i++) {

                valor = $(this).find(':input[id="inec_campo'+i+'"]').val();
                if (valor == "") {
                    alert("Debe llenar el formulario creado");
                    return false;
                }
                
                valor = $(this).find(':input[id="inec_valor'+i+'"]').val();
                if (valor == "") {
                    alert("Debe llenar el formulario creado");
                    return false;
                }                
            };
        };
       
      return true;
    });


    function FormatDatetime(fecha){
    	fecha = fecha.split("-");
    	return fecha[2]+"-"+fecha[1]+"-"+fecha[0];
    }

    function IsDatetimeInFuture(datetime){
    	fecha     = datetime.split("-");

    	now = new Date();
    	dt  = new Date(fecha[0],parseInt(fecha[1])-1,fecha[2],23,59,0,0);
    	return (now.getTime() < dt.getTime());
    }
})(jQuery);
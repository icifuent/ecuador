(function($) {

	var filterTimer=null;
	var filterTimeout=500;

    $('.selectpicker').selectpicker();
    $('#WizardEmplazamiento').wizard('destroy');
    $('#WizardUsuario').wizard('destroy');
    $('#WizardEmpresa').wizard('destroy');
    $('#WizardContratos').wizard('destroy');
    $('#Wizardperiodicidad').wizard();
   
    $('#FormPeriodicidadLista input[name=peri_nombre]').keyup(function() {
        FiltrarConDelay();
    })
    

    $(document).on('click', '#Wizardperiodicidad li#Primero', function(e) {
        $("#Agregarperiodicidad").show();
        $("#FormperiodicidadLista").submit();
    });

  

  

    //BOTONES___________________________________________________________________

    $(document).on('click', 'a#BotonPeriodicidadAgregar', function(e) {
        $("#Agregarperiodicidad").hide();
        $("#FormperiodicidadEditar").trigger('reset');
        $("#FormperiodicidadEditar").find(':input[name="peri_id"]').val("");
        $('#FormperiodicidadEditar.selectpicker').selectpicker('refresh');
             

        $('#Wizardperiodicidad').wizard('selectedItem', {step:2});
    });

    $(document).on('click', '#BotonPeriodicidadEditar', function(e) {
        //limpiarformulario("#FormperiodicidadEditar");
        $("#AgregarPeriodicidad").hide();
       
        peri_id = $(this).data("peri_id");

        var data = $(this).data("data");
       console.log(data);
        $.each(data, function(key, value) {
           
            obj = $('#FormperiodicidadEditar').find('#' + key);
            obj.val(value);
            if( obj.attr('type')==="checkbox" ){
               obj.prop( "checked", (value>0)?true:false );
            }
        });

      

        $('input#peri_id').val(peri_id);
        $('#Wizardperiodicidad').wizard('selectedItem', {step:2});
    });



    //SUBMITS DE FORMS__________________________________________________________
    
    //GUARDAR periodicidad
    $('#FormperiodicidadEditar').submit(function (e) {
        //Comprobacion de valores de variables        
        var valor;

        valor = $(this).find(':input[name="peri_nombre"]').val();

        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un nombre");  return false;
        }
        valor = $(this).find(':input[name="peri_meses"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un numero en meses");  return false;
        }
        valor = $(this).find(':input[name="peri_orden"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un orden");  return false;
        }
        
       return true;
    });
    
   
    //FUNCIONES AUXILIARES______________________________________________________
    
    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormPeriodicidadLista").submit();
        }, filterTimeout);
    }
    
})(jQuery);

(function($) {
	var filterTimer=null;
	var filterTimeout=500;

	//inicialización
    $('select.selectpicker').selectpicker();

	$('#siom-form-inventario-filtro input[name=inve_id]').keyup(function () {
        /* FiltrarConDelay();*/
    })

	$('#siom-form-inventario-filtro input[name=inve_marca]').keyup(function () {
        /* FiltrarConDelay();*/
    })

	$('#siom-form-inventario-filtro input[name=inve_modelo]').keyup(function () {
        /* FiltrarConDelay();*/
    })
	$('#siom-form-inventario-filtro input[name=inve_serie]').keyup(function () {
        /* FiltrarConDelay();*/
    })
    	
    $('#siom-form-inventario-filtro select[name=inve_estado]').change(function(){
		/*$("#siom-form-inventario-filtro").submit();*/
	})

	//funciones utiles
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-inventario-filtro").submit();
         },filterTimeout);
	}

	function InitSelectPickers(){
		sp = $('select.selectpicker');
		for(i=0;i<sp.length;i++){
			s = $(sp[i]);

			name = s.attr('name');
			val  = s.data('value');

			s.selectpicker('val',val);
			s.selectpicker('refresh');
		}
	}
	
	InitSelectPickers();

})(jQuery);

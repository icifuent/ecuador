(function($) {
	$("#download").click(function(e){
        $btn = $(this);
        $btn.button("loading");
        $.fileDownload("rest/core/repo/presupuesto/"+$(this).data("pres-id"), {
            prepareCallback:function(url) {
                $btn.button("processing");
            },
            successCallback: function(url) {
                $btn.button('reset')
            },
            failCallback: function(responseHtml, url) {
                $btn.button('reset')
                alert(responseHtml);
            }
        });
    });
})(jQuery);

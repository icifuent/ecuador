(function ($) {
	/*captura el momento antes de la accion de cerrar la ventana*/
	window.onbeforeunload = function () {
		$.ajax({
			async: false,
			cache: false,
			dataType: "html",
			type: 'POST',
			url: "rest/logout",
		});
	};

	window.resizeElements = function () {
		$('.siom-lista-full-height').each(function () {
			$(this).height($("#footer").offset().top - $(this).offset().top - 50);
		});
		$('.siom-mapa-full-height').each(function () {
			$(this).height($("#footer").offset().top - $(this).offset().top - 70);
		});
		$('.siom-lista-full-height-header').each(function () {
			$(this).height($("#footer").offset().top - $(this).offset().top + 220);
		});
	};

	window.scrollToElement = function (elem) {
		$('div.main-content').animate({
			scrollTop: elem.offset().top
		}, 1000);
	};

	window.alert = function (message, callback) {
		$("#alertModal").find(".modal-body").html(message);
		$("#alertModal").modal('show');
		if (callback) {
			$("#alertModal").on('hidden.bs.modal', callback);
		}
	};

	window.confirm = function (message, callback) {
		modal = $("#confirmModal");
		modal.find(".modal-body").html(message);
		modal.modal('show');
		modal.find("button").click(function (e) {
			modal.modal('hide');
			$(this).unbind();
			if (callback) {
				callback($(this).data("status"));
			}
		})
	};

	//Eventos
	$(window).resize(function () {
		console.log("resizing...");
		window.resizeElements();
	});
})(jQuery);

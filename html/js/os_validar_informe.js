(function($) { 
  $('.selectpicker').selectpicker();

 window.scrollToElement($('#siom-validar-informe'));

  $('#siom-validar-informe-form').submit(function() {
      estado = $(this).find(':input[name="info_estado"]').val();
      if(estado==""){
        alert("Debe ingresar estado de validación");
        return false;
      }

      if(estado=="RECHAZADO"){
        observacion = $(this).find(':input[name="info_observacion"]').val();
        if(observacion==""){
          alert("Debe ingresar observacion");
          return false;
        }
      }

      return true;
  });

  $("button.download").click(function(e){
      $btn = $(this);
      $btn.button("loading");
      $.fileDownload("rest/core/repo/informe/"+$(this).data("info-id"), {
          prepareCallback:function(url) {
              $btn.button("processing");
          },
          successCallback: function(url) {
              $btn.button('reset')
          },
          failCallback: function(responseHtml, url) {
              $btn.button('reset')
              alert(responseHtml);
          }
      });
    });
  
})(jQuery);

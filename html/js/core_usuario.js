(function($) {
	
	//Selfie Tecnico BWBB INICIO
	//Validar Perfil
	$("#UsuarioPerfil").change(function(){
		$("#div_selfie_tecnico").hide();
		var selected = $(this).find("option:selected");
	    var arrSelected = [];
	    
	    selected.each(function(){
	       arrSelected.push($(this).text());
	       if("JEFE_CUADRILLA" == $(this).text() || "TECNICO" == $(this).text()){
	    	   $("#div_selfie_tecnico").show();
	       }else{
	    	   $("#div_selfie_tecnico").hide();
	       }
	    });
		
		//alert($("#UsuarioPerfil option:selected" ).text()+" \\n arrSelected: "+arrSelected);
	});
	////Selfie Tecnico BWBB FIN

	var filterTimer=null;
	var filterTimeout=500;
    $("#usua-Responsabilidad").html("");
    $("#usua-Alcance").html("");

    $('.selectpicker').selectpicker();

    $('#WizardCurso').wizard('destroy');
    $('#WizardEmpresa').wizard('destroy');
    $('#WizardEmplazamiento').wizard('destroy');
    $('#WizardUsuario').wizard();

    $('#FormUsuarioLista input[name=usua_id]').keyup(function() {
        FiltrarConDelay();
        return true;
    })
    $('#FormUsuarioLista input[name=usua_nombre]').keyup(function() {
        FiltrarConDelay();
        return true;
    })
    $('#FormUsuarioLista select[name=usua_estado]').change(function() {
        FiltrarConDelay();
        return true;
    })

    $(document).off('click', '#WizardUsuario li#Primero');
    $(document).on('click', '#WizardUsuario li#Primero', function(e) {
        $("#AgregarUsuario").show();
        $("#FormUsuarioLista").submit();
        return true;
    });

    $(document).off('click', '#WizardUsuario li#Segundo');
    $(document).on('click', '#WizardUsuario li#Segundo', function(e) {
        return true;
    });

    $(document).off('click', '#WizardUsuario li#Tercero');
    $(document).on('click', '#WizardUsuario li#Tercero', function(e) {
        return true;
    });

    $(document).off('click', '#WizardUsuario li#Cuarto');
    $(document).on('click', '#WizardUsuario li#Cuarto', function(e) {
        return true;
    });

    $(document).off('click', '#WizardUsuario li#Quinto');
    $(document).on('click', '#WizardUsuario li#Quinto', function(e) {
        return true;
    });

    //BOTONES___________________________________________________________________
    $(document).off('click', 'a#BotonUsuarioAgregar');
    $(document).on('click', 'a#BotonUsuarioAgregar', function(e) {
    	//Selfie Tecnico BWBB INICIO
    	$("#div_selfie_tecnico").hide();
    	//Selfie Tecnico BWBB FIN
        $("#AgregarUsuario").hide();
        $('#empr_id.selectpicker').removeAttr("disabled");

        $('#agregar').show();
        $('#editar').hide();

        $("#UsuarioPerfil option").attr("selected", false);
        $("#FormUsuarioEditar").trigger('reset');
        $('#FormUsuarioEditar .selectpicker').selectpicker('refresh');

        $("input#usua_id").val("");
        $('input#usua_creador').val(window.user_id);

        $('#usua_estado.selectpicker').selectpicker('refresh');

        $("#BotonUsuarioContratoZonas").addClass("disabled");
        $('#WizardUsuario').wizard('selectedItem', {step:2});
        return true;
    });

    //$(document).off('click', 'button#BotonUsuarioEditar');
    $(document).on('click', 'button#BotonUsuarioEditar', function(e) {
        //limpiarformulario("#FormUsuarioEditar");
        //INICIO EPGM
        //$("#AgregarUsuario").hide();
        
        //$('#empr_id.selectpicker').attr('disabled',true);     
        //FIN EPGM   
        $("#BotonUsuarioContratoZonas").removeClass("disabled");

        usua_id = $(this).data("id");

        var data = $(this).data("data");
        $.each(data, function(key, value) {
            obj = $('#FormUsuarioEditar').find('#' + key);
            obj.val(value);
            
            //Selfie Tecnico BWBB INICIO
            if("usua_selfie_tecnico" === obj.attr('id')){
            	if("SI"=== obj.val()){
            		$("#usua_selfie_tecnico").val(value).change();
            	}else{
            		$("#usua_selfie_tecnico").val("NO").change();
            	}            	
            }
            
            //Selfie Tecnico BWBB FIN
            
            if( obj.attr('type')==="checkbox" ){
               obj.prop( "checked", (value>0)?true:false );
            }
        });
        $('#empr_id.selectpicker').selectpicker('refresh');

        //Perfiles del usuario
        //$("#UsuarioPerfil option").attr("selected", false);
        //$('#UsuarioPerfil.selectpicker').selectpicker();
        //$('#UsuarioPerfil.selectpicker').selectpicker('refresh');
        //$('#UsuarioPerfil.selectpicker').change();

        $('select[name=perf_id]').val("");
        $('select[name=perf_id]').change();
        $('select[name=perf_id]').selectpicker('refresh');

        //""
        //$('#UsuarioPerfil.selectpicker').selectpicker('val', []);
        //console.log("HERE");
        //$('#UsuarioPerfil').selectpicker('refresh');
        //$('#UsuarioPerfil').change();
        var perfiles = [];
        $.get('rest/core/usuario/' + usua_id + '/perfil/list', null, function(json) {
            if (json.status) {
                $.each(json.data, function(key, value) {
                    console.log("selected");
                    console.log(value["perf_id"]);
                    //$("#UsuarioPerfil option[value='" + value["perf_id"] + "']").attr("selected", true);
                    //alert("value: "+json.data[key]["perf_nombre"]);
                    //Selfie Tecnico BWBB INICIO
                    if("JEFE_CUADRILLA" == value["perf_nombre"] || "TECNICO" == value["perf_nombre"]){
         	    	   $("#div_selfie_tecnico").show();
         	       	}else{
         	    	   $("#div_selfie_tecnico").hide();
         	       	}
                    //Selfie Tecnico BWBB FIN
		    perfiles.push(value["perf_id"]);
                });
                $('#UsuarioPerfil').selectpicker('val', perfiles);
                $('select[name=perf_id]').change();
                  $('select[name=perf_id]').selectpicker('refresh');
                $('#UsuarioPerfil.selectpicker').selectpicker('refresh');
            }
        });

        $('#UsuarioPerfil.selectpicker').selectpicker('refresh');

        $('#usua_estado.selectpicker').selectpicker('refresh');

        $('input#usua_id').val(usua_id);
        $('#WizardUsuario').wizard('selectedItem', {step:2});

        return true;
    });

    $(document).off('click', 'button#BotonUsuarioContratoZonas');
    $(document).on('click', 'button#BotonUsuarioContratoZonas', function(e) {
        CargaDatosContratosZonas();
        $('#WizardUsuario').wizard('selectedItem', {step:3});
        return true;
    });

    $(document).off('click', 'button#BotonUsuarioNotificaciones');
    $(document).on('click', 'button#BotonUsuarioNotificaciones', function(e) {
        usua_id = $('#usua_id').val();
        $('#FormUsuarioNotificaciones').find('#usua_id').val(usua_id);
        CargaDatosNotificaciones();
        $('#WizardUsuario').wizard('selectedItem', {step:4});
        return true;
    });

    $(document).off('click', 'button#BotonUsuarioIMEI');
    $(document).on('click', 'button#BotonUsuarioIMEI', function(e) {
        usua_id = $('#usua_id').val();
        $('#FormUsuarioIMEI').find('#usua_id').val(usua_id);
        CargaUsuarioImei();
        $('#WizardUsuario').wizard('selectedItem', {step:5});
        return true;
    });

    $(document).off('click', 'button#BotonUsuarioCursos');
    $(document).on('click', 'button#BotonUsuarioCursos', function(e) {
        usua_id = $('#usua_id').val();
        $('#FormUsuarioCursos').find('#usua_id').val(usua_id);
        CargaUsuarioCursos();
        $('#WizardUsuario').wizard('selectedItem', {step:6});
        return true;
    });

    $(document).off('click','button#BotonUsuarioCambiarEstado');
    $(document).on('click','button#BotonUsuarioCambiarEstado',function(e){
        usua_id     = $(this).data("id");
        usua_nombre = $(this).data("nombre");
        usua_estado = $(this).data("estado");

        if (usua_estado=="ACTIVO"){
            nuevo_estado        = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';

      	}
        else if(usua_estado=="NOACTIVO"){
            nuevo_estado        = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';

        }

        confirm("El usuario <b>"+usua_nombre+"</b> será <b>"+nombre_nuevo_estado+"</b>,<br>Desea continuar?",function(status){
              if(status==true){
                    $.post('rest/core/usuario/updEstado/'+usua_id+'/'+nuevo_estado,function(json) {
                   $("#FormUsuarioLista").submit();
                    }).fail(function(xhr, textStatus, errorThrown){
                      alert("Error "+xhr.status+": "+xhr.statusText);
                      return false;
                  });
              }
        });
    });

    //CONTRATOS Y ZONAS_________________________________________________________

    function CargaDatosContratosZonas() {
        usua_id = $('#usua_id').val();
        $.get('rest/core/contrato/list', null, function (json) {
             if (json.status) {
                $("#ListaContratos").html("");
                $('#ListaContratosUsuario').html("");
                $('#ListaContratoUsuarioZonas').html("");
                
                $.each(json.data, function (i, contrato) {  
                    html = '<li class="list-group-item" id="' + contrato["cont_id"] + '">' +
                            '<span>' + contrato["cont_nombre"] + '</span>' +
                            '<i class="glyphicon glyphicon-chevron-right pull-right" data-id="' + contrato["cont_id"] + '" data-nombre="' + contrato["cont_nombre"] + '"></i>' +
                            '</li>';
                    $("#ListaContratos").append(html);                    
                    
                    //CargaListaZonas(contrato["cont_id"]);
                });                
                CargaListaContratosUsuario(usua_id);                
            }
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener la lista de contratos");
            return false;
        });
    }

    function CargaListaContratosUsuario(usua_id) {
        $.get('rest/core/usuario/' + usua_id + '/contrato/list?recu_estado=ACTIVO', null, function (json) {
            if (json.status) {
                $("ul#ListaContratosUsuario").html("");
                $.each(json.data, function (i, contrato) {
                    html = '<li class="list-group-item" id="' + contrato["cont_id"] + '">' +
                            '<a data-id="' + contrato["cont_id"] + '" data-nombre="' + contrato["cont_nombre"] + '" style="cursor: pointer;">' + contrato["cont_nombre"] + '</a>' +
                            '<i data-id="' + contrato["cont_id"] + '" class="glyphicon glyphicon-trash pull-right eliminar" ></i>' +
                            '</li>';

                    $("ul#ListaContratos #" + contrato["cont_id"]).addClass("disabled");
                    $("ul#ListaContratosUsuario").append(html);
                });
            }
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener los contratos del usuario");
            return false;
        });
    }
    
    function CargaListaZonas(cont_id) {
        html="";
        usua_id = $('#usua_id').val();
        $('#ListaContratoUsuarioZonas').append($('<div id="ZonasUsuarioContrato' + cont_id + '" style="display:none"></div>'));

        var zonas_tipos = ['ALCANCE', 'RESPONSABILIDAD','CLUSTER'];

        $.get('rest/core/contrato/' + cont_id + '/usuario/' + usua_id + '/alcance/list?zona_estado=ACTIVO', null, function(json) {
            if (json.status) {
                var zonas_usuario = json.data;

                $.get('rest/core/contrato/' + cont_id + '/usuario/' + usua_id + '/responsabilidad/list?zona_estado=ACTIVO', null, function(json) {
                    if (json.status) {
                        zonas_usuario = zonas_usuario.concat(json.data);

                        $.get('rest/core/contrato/' + cont_id + '/usuario/' + usua_id + '/cluster/list?zona_estado=ACTIVO', null, function(json) {
                            if (json.status) {
                                zonas_usuario = zonas_usuario.concat(json.data);


                                $.get('rest/core/zona/list?zona_estado=ACTIVO&cont_id= ' + cont_id, null, function(json) {
                                    if (json.status) {
                                        var zonas_contrato = json.data;
                                        $("#ZonasUsuarioContrato" + cont_id).append("<div style='height:10px'></div>");

           
                                        $.each(zonas_tipos, function(i, zona_tipo) {
                                            html = '<div class="form-group">' +
                                                   '<label class="col-md-3 control-label">' + zona_tipo + '</label>' +
                                                   '<div class="col-md-8">' +
                                                   '<select id="' + zona_tipo + '" name="zona_id" data-cont_id="' + cont_id + '" class="selectpicker" multiple="multiple" title="Seleccione..." data-container="body">';
                                                                                               
                                            $.each(zonas_contrato, function(i, zona_contrato) {
                                                if (zona_contrato["zona_tipo"] === zona_tipo) {
                                                    var isSelected = false;
                                                    $.each(zonas_usuario, function(j, zona_usuario) {
                                                        if (zona_usuario['zona_id'] == zona_contrato["zona_id"]) {
                                                            isSelected = true;
                                                            delete zona_usuario['zona_id'];
                                                        }
                                                    });
                                                    html += "<option " + ((isSelected) ? "selected" : "") + " data-content=\"<span id='data' data-zona_tipo='" + zona_tipo + "' data-zona_id='" + zona_contrato['zona_id'] + "' data-cont_id='" + cont_id + "'>" + zona_contrato["zona_nombre"] + "</span>\" >" + zona_contrato["zona_nombre"] + "</option>";
                                                }
                                            });
                                                    
                                            html += '</select></div></div>';
                                            $("#ZonasUsuarioContrato" + cont_id).append(html);                                    
                                            $("#ZonasUsuarioContrato" + cont_id + " .selectpicker#"+zona_tipo).selectpicker();
                                            $("#ZonasUsuarioContrato" + cont_id + " .selectpicker#"+zona_tipo).data('selectpicker').$menu.on('click', 'li a',function(e){
                                                var isSelected = $(this).parent('li').hasClass("selected");
                                                var usua_id    = $('#usua_id').val();
                                                var zona_tipo  = $(this).children('span').attr('data-zona_tipo').toLowerCase();
                                                var zona_id    = $(this).children('span').attr('data-zona_id');
                                                var cont_id    = $(this).children('span').attr('data-cont_id'); 
                                                ModificaZona(cont_id, usua_id, zona_id, zona_tipo, isSelected);
                                            });
                                        });
                                    }
                                }).fail(function() {
                                    alert("Contratos y Zonas: No se pudo obtener las zonas");
                                    return false;
                                });
                    }

                        }).fail(function() {
                            alert("Contratos y Zonas: No se pudo obtener las zonas");
                            return false;
                        });
                    }
                }).fail(function() {
                    alert("Contratos y Zonas: No se pudo obtener las zonas");
                    return false;
                });
            }
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener las zonas");
            return false;
        });
    }



    $(document).off('click', '#ListaContratos i');
    $(document).on('click', '#ListaContratos i', function (e) {
        if ($(this).parent().hasClass("disabled")) {
            return;
        }
        var usua_id = $('#usua_id').val();
        var cont_id = $(this).data("id");
        var cont_nombre = $(this).data("nombre");
        html = '<li class="list-group-item" id="' + cont_id + '">' +
                '<a data-id="' + cont_id + '" data-nombre="' + cont_nombre + '" style="cursor: pointer;">' + cont_nombre + '</a>' +
                '<i data-id="' + cont_id + '" class="glyphicon glyphicon-trash pull-right eliminar"></i>' +                
                '</li>';

        $("#ListaContratos #" + cont_id).addClass("disabled");
        $("#ListaContratosUsuario").append(html);
        ModificaContrato(cont_id, usua_id, true);
        return true;
    });
    
    $(document).off('click','#ListaContratosUsuario a');
    $(document).on('click','#ListaContratosUsuario a',function(e){
        //console.log("ListaContratosUsuario a");
    	var cont_id  = $(this).data("id");
    	var cont_nombre  = $(this).data("nombre"); 
        $("i#NombreContratoZonas").html(cont_nombre);   
        
        $("div#ZonasUsuarioContrato"+cont_id).remove();
        CargaListaZonas(cont_id);
        $('div[id^="ZonasUsuarioContrato"]').css("display", "none");
        $("div#ZonasUsuarioContrato"+cont_id).css("display", "block");
        return true;
    });
    
    $(document).off('click','#ListaContratosUsuario i');
    $(document).on('click','#ListaContratosUsuario i',function(e){
        var usua_id = $('#usua_id').val();
        var cont_id = $(this).data("id");
    	$("#ListaContratosUsuario #"+cont_id).remove();
        $("#ListaContratos #"+cont_id).removeClass("disabled");
        
        $("i#NombreContratoZonas").html("");        
        $('div[id^="ZonasUsuarioContrato"]').css("display", "none");
        $("div#ZonasUsuarioContrato"+cont_id).remove();
        ModificaContrato(cont_id, usua_id, false);
        return true;
    });


    function ModificaZona(cont_id, usua_id, zona_id, zona_tipo, addOrDel) {
        //console.log("ModificaZona");
        var url = 'rest/core/contrato/' + cont_id + '/usuario/' + usua_id + '/'+zona_tipo+'/'+((addOrDel)?"add":"del")+'/' + zona_id;
        $.get(url, null, function(json) {
            if (json.status) {
                //console.log("zona "+zona_id+((addOrDel)?" add":" del"));
            }
            else{
                alert("No se pudo guardar la zona: " + json.error);
                return false;
            }
        }).fail(function () {
            alert("Error guardando zona: Error en GET, ruta:" + url);
            return false;
        });
    }
    
    function ModificaContrato(cont_id, usua_id, addOrDel){
    // /core/rel/contrato/@cont_id:[0-9]+/usuario/del/@usua_id:[0-9]+ 
        //var url = 'rest/core/contrato/' + cont_id + '/usuario/'+ ((addOrDel)?"add":"del") + '/' + usua_id ;
        var url = 'rest/core/usuario/rel/contrato/' + cont_id + '/usuario/'+ ((addOrDel)?"add":"del") + '/' + usua_id;
        $.get(url, null, function(json) {
            if (json.status) {
                console.log(json.status);
                //console.log("contrato "+cont_id+((addOrDel)?" add":" del"));
            }
            else{
                alert("No se pudo guardar el contrato: " + json.error);
                return false;
            }
        }).fail(function () {
            alert("Error guardando contrato: Error en GET, ruta:" + url);
            return false;
        });
        return true;
    }



    //NOTIFICACIONES____________________________________________________________

    function CargaDatosNotificaciones() {
        //console.log("CargaDatosNotificaciones");
        usua_id = $("input#usua_id").val();

        $("#ListaContratosNotificaciones").html("");
        $('#NotificacionesOS :checkbox').removeAttr('checked');
        $('#NotificacionesMNT :checkbox').removeAttr('checked');
        $('input#cont_id').val("");
        $(".NombreContratoNotificaciones").html(""); 
        
        

        $.get('rest/core/usuario/' + usua_id + '/contrato/list?recu_estado=ACTIVO', null, function(json) {
            if (json.status) {
                //console.log("CargaDatosNotificaciones");
                //console.log(json.data);
                $.each(json.data, function(i, contrato) {                    
                    html = '<li class="list-group-item">'+
    		           '<a data-cont_id="' + contrato["cont_id"] + '"  data-cont_nombre="' + contrato["cont_nombre"] + '" style="cursor: pointer;">' + contrato["cont_nombre"] + '</a>'+
			   '</li>';
                    $("#ListaContratosNotificaciones").append(html);
                });
            }
            else{
                alert("Notificaciones: No se pudo obtener las notificaciones del usuario");
                return false;
            }
        }).fail(function() {
            alert("Notificaciones: No se pudo obtener las notificaciones del usuario");
            return false;
        });
        return true;
    }
    $(document).off('click', '#ListaContratosNotificaciones a');
    $(document).on('click', '#ListaContratosNotificaciones a', function(e) {
        usua_id = $("input#usua_id").val();
        cont_id = $(this).data("cont_id");
        cont_nombre = $(this).data("cont_nombre");
        //console.log("cont_id:"+cont_id+" cont_nombre:"+cont_nombre);        
        CargaListaNotificaciones(cont_id, cont_nombre,  usua_id);
        return true;
    });
    
    
    function CargaListaNotificaciones(cont_id, cont_nombre, usua_id) {
        //console.log("CargaListaNotificaciones");

        $('input#cont_id').val(cont_id);
        $('#NotificacionesOS :checkbox').removeAttr('checked');
        $('#NotificacionesMNT :checkbox').removeAttr('checked'); 
        $(".NombreContratoNotificaciones").html(cont_nombre);  

        $.get('rest/core/contrato/' + cont_id + '/usuario/' + usua_id + '/notificacion/list', function(json) {
            if (json.status) {

                $.each(json.data, function(key, notif) {
                    //console.log(notif);
                    //console.log("#NotificacionesOS input[type='checkbox'][data-modu_alias='"+notif["modu_alias"]+"'][data-rcun_evento='"+notif["rcun_evento"]+"'][data-rcun_accion='"+notif["rcun_accion"]+"']");
                    $("#Notificaciones"+notif["modu_alias"]+" input[type='checkbox'][data-modu_alias='"+notif["modu_alias"]+"'][data-rcun_evento='"+notif["rcun_evento"]+"'][data-rcun_accion='"+notif["rcun_accion"]+"']").prop("checked", true);
                    $("#Notificaciones"+notif["modu_alias"]+" input[type='checkbox'][data-modu_alias='"+notif["modu_alias"]+"'][data-rcun_evento='"+notif["rcun_evento"]+"'][data-rcun_accion='"+notif["rcun_accion"]+"']").val(notif["rcun_id"]);
                });
            }
            else{
                alert("Notificaciones: No se pudo obtener las notificaciones del usuario");
                return false;
            }
        }).fail(function() {
            alert("Notificaciones: No se pudo obtener las notificaciones del usuario");
            return false;
        });
        return true;
    }
        
    $('#FormUsuarioNotificaciones input:checkbox').change(function() {
        var cont_id = $('input#cont_id').val();
        if( !(cont_id > 0) ){
            alert("Debe seleccionar un contrato para modificar sus notificaciones");
            $(this).prop('checked', false);
            return false;
        }

        var that = $(this);
        var usua_id = $("input#usua_id").val();
        var modu_alias = $(this).data("modu_alias");
        var rcun_evento = $(this).data("rcun_evento");
        var rcun_accion = $(this).data("rcun_accion");
        var addOrDel = $(this).is(':checked');
        var rcun_id = $(this).val();
        var url;
        if( addOrDel ){
            url = 'rest/core/contrato/'+cont_id+'/usuario/'+usua_id+'/notificacion/add?modu_alias='+modu_alias+'&rcun_evento='+rcun_evento+'&rcun_accion='+rcun_accion;
        }
        else{
            url = 'rest/core/contrato/'+cont_id+'/usuario/'+usua_id+'/notificacion/del/' + rcun_id;
        }
        //console.log(url);

        $.get(url, null, function(json) {
            if (json.status) {

                if( addOrDel ){
                    that.val(json.data[0].id);
                    //console.log("notificacion "+json.data[0].id+ " add");
                } else{
                    //console.log("notificacion "+rcun_id+ " del");
                }
            } else {
                alert("Error guardando notificacion: " + json.error + ", ruta: "+ url);
            }
        }).fail(function() {
            alert("Error guardando notificacion: Error en GET, ruta:" + url);
            return false;
        });
    });


    //IMEI______________________________________________________________________

    function CargaUsuarioImei() {
        $("#ListaIMEI").html("");
        usua_id = $('#usua_id').val(); //usua_id = $('#FormUsuarioIMEI input#usua_id').val();

        var url = 'rest/core/rel_usuario_movil/usuario/' +usua_id +'/list';
        $.get(url, null, function(json) {
            if (json.status) {
                $.each(json.data, function(key, imei) {
                    var isActive = (imei['reum_estado']==='ACTIVO');
                    var html = '<li class="list-group-item" style="font-size: 1.3em">' +
                                '<span class="glyphicon glyphicon-stop" style="color:'+((isActive)?'green':'red')+';font-size: 1.3em"></span>' +
                                '<label style="margin-left:10px">' + imei['reum_imei'] + ' (' + imei['reum_estado'] + ')</label>' +
                                '<span class="btn btn-default btn-xs pull-right" data-reum_id="' + imei['reum_id'] + '" data-reum_estado="' + imei['reum_estado'] + '" style="padding-left:5px;padding-right:5px">'+
                                    '<i class="glyphicon glyphicon-'+((isActive)?'trash':'plus')+'"></i>'+
                                '</span>'+
                                '</li>';
                    $("#ListaIMEI").append(html);
                    /*$.ajax({
                        url: "templates/core_usuario_lista_imei.hb",
                        async: true,
                        success: function(src) {
                            template = Handlebars.compile(src);
                            $("#lista_imei").append(template(json.data[key]));
                        },
                    });*/
                });
            }
            else{
                alert("Error obteniendo lista de IMEI: " + json.error + ", ruta: "+ url);
                return false;
            }
        }).fail(function() {
            alert("Error obteniendo lista de IMEI: Error en GET, ruta:" + url);
            return false;
        });
    }

    $(document).off('click', '#ListaIMEI .btn');
    $(document).on('click', '#ListaIMEI .btn', function(e) {
        //console.log("ListaIMEI .botonModificarIMEI");
        var reum_id = $(this).data("reum_id");
        var reum_estado = $(this).data("reum_estado");

        if (reum_estado === "ACTIVO") {
            nuevo_estado = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
        }
        else {
            nuevo_estado = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        var url = 'rest/core/rel_usuario_movil/upd/' + reum_id + '?reum_estado=' + nuevo_estado;
        $.get(url, null, function(json) {
            if (json.status) {
                CargaUsuarioImei();
            } else {
                alert("Error guardando IMEI; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error guardando IMEI " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
            return false;
        });
/*
        confirm("El IMEI sera <b>" + nombre_nuevo_estado + "</b>,<br>Desea continuar?", function(status) {
            if (status === true) {
                var url = 'rest/core/rel_usuario_movil/upd/' + reum_id + '?reum_estado=' + nuevo_estado;
                console.log("ListaIMEI .botonModificarIMEI OK");
                console.log(url);
                console.log(reum_id);
                $.get(url, null, function(json) {
                    if (json.status) {
                        CargaUsuarioImei();
                    } else {
                        alert("Error guardando IMEI; " + json.error + ", ruta:" + url);
                    }
                }).fail(function(xhr, textStatus, errorThrown) {
                    alert("Error guardando IMEI " + xhr.status + ": " + xhr.statusText);
                });
            }

        });*/
    });

    $('#FormUsuarioIMEI').submit(function() {

        var imei = $(this).find(':input[name="reum_imei"]').val();
        if (imei === "") {
            alert("Debe ingresar IMEI");
        }

        var data = {};
        data["reum_imei"] = $('#reum_imei').val();
        data["usua_id"] = $('#usua_id').val();
        data["reum_estado"] = 'ACTIVO';

        url = 'rest/core/rel_usuario_movil/add';

        $.post(url, data, function(json) {
            if (json.status) {
                $("#ListaIMEI").html(" ");
                CargaUsuarioImei();
            }
            else {
                alert("Error guardando IMEI; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error guardando IMEI " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
            return false;
        });
    });

    //CURSOS____________________________________________________________________

    function CargaUsuarioCursos() {
        $("#TableUsuarioCursos").html("");
        usua_id = $('#usua_id').val(); //usua_id = $('#FormUsuarioIMEI input#usua_id').val();

        var url = 'rest/core/usuario/'+usua_id+'/curso/list?curs_estado=ACTIVO';
        $.get(url, null, function(json) {
            if (json.status) {
                var cursos_usuario = json.data;

                url = 'rest/core/curso/list?curs_estado=ACTIVO';
                $.get(url, null, function(json) {
                    if (json.status) {
                        var cursos = json.data;

                        var html = '<table class="table table-striped" style="margin:10px">' +
                                        '<tr>' +
                                                '<th>NOMBRE CURSO</th>' +
                                                '<th>INSTITUCION</th>' +
                                                '<th>FECHA REALIZACION</th>' +
                                                '<th>FECHA EXPIRACION</th>' +
                                                '<th style="width:30%">COMENTARIO</th>' +
                                                '<th></th>' +
                                        '</tr>';

                        $.each(cursos_usuario, function(key, curso) {
                            html += '<tr>' +
                                            '<td>' + curso['curs_nombre'] + '</td>' +
                                            '<td>' + curso['uscu_institucion'] + '</td>' +
                                            '<td>' + ParseDateTime(curso['uscu_fecha_realizacion']) + '</td>' +
                                            '<td>' + ParseDateTime(curso['uscu_fecha_expiracion']) + '</td>' +
                                            '<td>' + ((curso['uscu_comentario']===null)?"":curso['uscu_comentario']) + '</td>' +
                                            '<td><button type="button" class="btn btn-default btn-xs" data-uscu_id="' + curso['uscu_id'] + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>' +
                                    '</tr>';
                        });

                        html += '<tr>' +
                                        '<td><select name="curs_id" class="selectpicker" title="Seleccione curso" data-container="body">' +
                                            '<option value="" disabled selected>Seleccione Curso</option>';
                        $.each(cursos, function(key, curso) {
                                    html += '<option value="' +curso['curs_id']+ '">' +curso['curs_nombre']+ '</option>';
                        });
                        html +='</select></td>';

                        html +=         '<td><input type="text" class="form-control" name="uscu_institucion" placeholder="Institucion"></td>' +
                                        '<td><input type="text" class="form-control fecha-curso" name="uscu_fecha_realizacion" data-date-autoclose="true" placeholder="dd-mm-yyyy"></td>' +
                                        '<td><input type="text" class="form-control fecha-curso" name="uscu_fecha_expiracion"  data-date-autoclose="true" placeholder="dd-mm-yyyy"></td>' +
                                        '<td><input type="text" class="form-control" name="uscu_comentario" placeholder="Comentario de la realizacion del curso"></td>' +
                                        '<td><button type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></button></td>' +
                                '</tr>';
                        html += '</table>';

                        $("#TableUsuarioCursos").append(html);

                        $( ".fecha-curso" ).each( function( index, element ){
                            $( this ).datepicker({
                                format: "dd-mm-yyyy",
                                viewMode: "days",
                                minViewMode: "days",
                                language: "es"
                            });
                        });
                    } else {
                        alert("Error obteniendo lista de cursos: " + json.error + ", ruta: " + url);
                    }
                }).fail(function() {
                    alert("Error obteniendo lista de cursos: Error en GET, ruta:" + url);
                });
            }
            else{
                alert("Error obteniendo lista de cursos: " + json.error + ", ruta: "+ url);
            }
        }).fail(function() {
            alert("Error obteniendo lista de cursos: Error en GET, ruta:" + url);
            return false;
        });


         //

                /*
                data = $(this).data();
                $.ajax({
                    url: "templates/os_presupuesto_item.hb",
                    async: true,
                    success: function(src){
                        template = Handlebars.compile(src);
                        $("#lista_presupuesto").append(template(data));

                        UpdateCorrelativo();
                        UpdateSubtotal(data.lpipId+"-"+(nroCorrelativo-1));
                        UpdateTotal();
                  },
                });*/
    }

    $(document).off('click', '#TableUsuarioCursos button[type="button"]');
    $(document).on('click', '#TableUsuarioCursos button[type="button"]', function(e) {
        //console.log('#TableUsuarioCursos button[type="button"] click');
        var uscu_id = $(this).data('uscu_id');

        url = 'rest/core/rel_usuario_curso/del/'+uscu_id;

        $.get(url, data, function(json) {
            if (json.status) {
                CargaUsuarioCursos();
            } else {
                alert("Error borrando curso; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error borrando curso " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
            return false;
        });
    });

    $('#FormUsuarioCursos').submit(function() {
        //console.log("FormUsuarioCursos submit");

        var data = {};
        $.each($('#FormUsuarioCursos').serializeArray(), function(i, field) {
            data[field.name] = field.value;
        });

        //console.log(data);

        if ( data['curs_id'] === undefined || data['curs_id'] === null || data['curs_id'].length === 0) {
            alert("Debe seleccionar un curso");  return false;
        }

        if (data['uscu_institucion'] === null || data['uscu_institucion'].length === 0) {
            alert("Debe ingresar la institucion donde se realizo el curso");  return false;
        }

        if (data['uscu_fecha_realizacion'] === null || data['uscu_fecha_realizacion'].length === 0) {
            alert("Debe ingresar una fecha de realizacion");  return false;
        }

        if (data['uscu_fecha_expiracion'] === null || data['uscu_fecha_expiracion'].length === 0) {
            alert("Debe ingresar una fecha de expiracion");  return false;
        }

        data['uscu_fecha_realizacion'] = FormatDateTime(data['uscu_fecha_realizacion']);
        data['uscu_fecha_expiracion'] = FormatDateTime(data['uscu_fecha_expiracion']);

        url = 'rest/core/usuario/'+data['usua_id']+'/curso/add/'+data['curs_id'];

        $.post(url, data, function(json) {
            if (json.status) {
                CargaUsuarioCursos();
            } else {
                alert("Error guardando cursos; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error guardando cursos " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
            return false;
        });

    });

    //SUBMITS DE FORMS__________________________________________________________

    //GUARDAR USUARIO
    $('#FormUsuarioEditar').submit(function (e) {
        //Comprobacion de valores de variables
        console.log("ADD USUARIO");
        var valor;

        valor = $(this).find(':input[name="usua_nombre"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un nombre");  return false;
        }
        valor = $(this).find(':input[name="usua_alias"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un alias");  return false;
        }
        valor = $(this).find(':input[name="usua_rut"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un RUT");  return false;
        }
        valor = $(this).find(':input[name="usua_login"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un login");  return false;
        }
        valor = $(this).find(':input[name="usua_password"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un password");  return false;
        }

        //Hacemos el POST
        var data = {};
        $.each($('#FormUsuarioEditar').serializeArray(), function(i, field) {
            if( field.name == "perf_id[]" ){
                if( !data["perf_id"] ) data["perf_id"] = [];
                data["perf_id"].push(field.value);
            } else {
                data[field.name] = field.value;
            }

        });

        $("#UsuarioTipoAcceso input[type='checkbox']").each(function(i, checkbox) {
            data[checkbox.name] = ($(checkbox).is(':checked'))?"1":"0";

        });

        var url = 'rest/core/usuario/add';
        if (data.usua_id > 0) {
            url = 'rest/core/usuario/upd/' + data.usua_id;
        } else {
            delete data["usua_id"];
        }

        //console.log("FormUsuarioEditar submit");
        //console.log(url);
        //console.log(data);
        //return false;

        console.log("INICIO POST URL");
        $.post(url, data, function(json) {
            if (json.status == 0) {
                alert("Error guardando usuario:" + json.error + " , ruta:" + url);
            }
            if (json.data[0].id > 0) {
                $('#usua_id').val(json.data[0].id);
            }

            $("#BotonUsuarioContratoZonas").removeClass("disabled");
            alert("Usuario guardado exitosamente");

        }).fail(function() {
            alert("Error guardando usuario: Error en POST, ruta:" + url);
                  });
            console.log("FIN POST URL");

    });


    //FUNCIONES AUXILIARES______________________________________________________

    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormUsuarioLista").submit();
        }, filterTimeout);
        return true;
    }

    function FormatDateTime(fecha){
    	aux = fecha.split("-");
    	return aux[2]+"-"+aux[1]+"-"+aux[0];
    }

    function ParseDateTime(datetime){
        aux = datetime.substring(0, 10).split('-');
    	return aux[2]+"-"+aux[1]+"-"+aux[0];
    }

})(jQuery);

(function($) { 
	var filterTimer=null;
	var filterTimeout=500;

    //inicializar componentes
    $('.selectpicker').selectpicker();

    $('div.btn-group .btn-primary').on('click', function(e){
        ShowView($(this).data("tipo"));
    });

    $('#siom-form-emplazamientos-filtro input[name=empl_nemonico]').keyup(function () {
        /* FiltrarConDelay(); */
    })
    $('#siom-form-emplazamientos-filtro input[name=empl_nombre]').keyup(function () {
        /* FiltrarConDelay(); */
    })
    $('#siom-form-emplazamientos-filtro input[name=empl_direccion]').keyup(function () {
       /*  FiltrarConDelay(); */
    })
    $('#siom-form-emplazamientos-filtro select[name=empl_visitas]').change(function(){
       /* $("#siom-form-emplazamientos-filtro").submit(); */
    })
    $('#siom-form-emplazamientos-filtro select[name=orse_indisponibilidad]').change(function(){
       /* $("#siom-form-emplazamientos-filtro").submit(); */
    })
    $('#siom-form-emplazamientos-filtro select[name=empl_macrositio]').change(function(){
       /* $("#siom-form-emplazamientos-filtro").submit(); */
    })
    $('#siom-form-emplazamientos-filtro select[name=empl_subtel]').change(function(){
       /* $("#siom-form-emplazamientos-filtro").submit(); */
    })
    $('#siom-form-emplazamientos-filtro select[name=tecn_id]').change(function(){
	   /* $("#siom-form-emplazamientos-filtro").submit(); */
	})
	$('#siom-form-emplazamientos-filtro select[name=zona_id]').change(function(){
		/*$("#siom-form-emplazamientos-filtro").submit(); */
	})
	$('#siom-form-emplazamientos-filtro select[name=regi_id]').change(function(){
		/*$("#siom-form-emplazamientos-filtro").submit(); */
	})
	$('#siom-form-emplazamientos-filtro select[name=clus_id]').change(function(){
		/*$("#siom-form-emplazamientos-filtro").submit(); */
	})
    $('#siom-form-emplazamientos-filtro select[name=cate_id]').change(function(){
        /*$("#siom-form-emplazamientos-filtro").submit(); */
    })


    /*** Boton Limpiar ***/
    $(document).on('click', 'button#LimpiarEmplazamientos', function (e) {
        $('#nombre').val("");
        $('#nemonico').val("");
        
        $('select[name=empl_visitas]').val("");
        $('select[name=empl_visitas]').change();

        $('select[name=orse_indisponibilidad]').val("");
        $('select[name=orse_indisponibilidad]').change();

        $('select[name=empl_macrositio]').val("");
        $('select[name=empl_macrositio]').change();

        $('select[name=empl_subtel]').val("");
        $('select[name=empl_subtel]').change();

        $('select[name=tecn_id]').val("");
        $('select[name=tecn_id]').change();
        $('select[name=tecn_id]').selectpicker('refresh');

        $('#empl_direccion').val("");

        $('select[name=zona_id]').val("");
        $('select[name=zona_id]').change();
        $('select[name=zona_id]').selectpicker('refresh');

        $('select[name=regi_id]').val("");
        $('select[name=regi_id]').change();
        $('select[name=regi_id]').selectpicker('refresh');

        $('select[name=clus_id]').val("");
        $('select[name=clus_id]').change();
        $('select[name=clus_id]').selectpicker('refresh');

        $('select[name=cate_id]').val("");
        $('select[name=cate_id]').change();
        $('select[name=cate_id]').selectpicker('refresh');
        
    });

    /*** Boton Buscar ***/
    $(document).on('click', 'button#BuscarEmplazamientos', function (e) {
        $("#siom-form-emplazamientos-filtro").submit();
        return true;
    });

    //funciones auxiliares
    function ShowView(type){
        if(type=="mapa"){
            $('#mapa')[0].style.display="block";//.css("display","block");
            $('#lista')[0].style.display="none";//.css("display","none");

            if(window.gEmplMap){ //refrescar mapa para actualizar tamaño
                console.log("resizing map");
                window.resizeElements();
                google.maps.event.trigger(window.gEmplMap, 'resize');
                window.gEmplMap.fitBounds(window.gEmplMapBounds);
            }
        }
        else{
            $('#mapa')[0].style.display="none";//.css("display","none");
            $('#lista')[0].style.display="block";//.css("display","block");
            window.resizeElements();
        }
        $('#siom-form-emplazamientos-filtro input[name=tipo]').val(type);
    }
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-emplazamientos-filtro").submit();
         },filterTimeout);
    }
    
    $("#nombre").on('input', function(){
        $(this).val($(this).val().replace(/[^a-z0-9.ñÑ\s-]/gi, ''));
    });

    $("#nombre").attr('maxlength','100');

    $("#nemonico").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^a-z0-9ñÑ-]/gi, ''));
    });

    $("#nemonico").attr('maxlength','30');

    $("#empl_nombre").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^0-9a-z°ñÑ.\s]/gi, ''));
    });

    $("#empl_nombre").attr('maxlength','100');

    $("#empl_direccion").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^0-9a-z°ñÑ\s]/gi, ''));
	});

    $("empl_direccion").attr('maxlength','150');
    
})(jQuery);

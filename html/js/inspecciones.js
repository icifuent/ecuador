(function($) { 
	var filterTimer=null;
	var filterTimeout=500;

    //inicializar componentes
    $('.selectpicker').selectpicker();

    
    $('#siom-form-resumen-filtro input[name=empl_nemonico]').keyup(function () {
         FiltrarConDelay();
    })
    $('#siom-form-resumen-filtro input[name=empl_nombre]').keyup(function () {
         FiltrarConDelay();
    })
    $('#siom-form-emplazamientos-filtro input[name=empl_direccion]').keyup(function () {
         FiltrarConDelay();
    })
    $('#siom-form-emplazamientos-filtro select[name=empl_visitas]').change(function(){
        $("#siom-form-resumen-filtro").submit();
    })
    $('#siom-form-resumen-filtro select[name=orse_indisponibilidad]').change(function(){
        $("#siom-form-resumen-filtro").submit();
    })
    $('#siom-form-resumen-filtro select[name=empl_macrositio]').change(function(){
        $("#siom-form-resumen-filtro").submit();
    })
    $('#siom-form-resumen-filtro select[name=empl_subtel]').change(function(){
        $("#siom-form-resumen-filtro").submit();
    })
    $('#siom-form-resumen-filtro select[name=tecn_id]').change(function(){
		$("#siom-form-resumen-filtro").submit();
	})
	$('#siom-form-resumen-filtro select[name=zona_id]').change(function(){
		$("#siom-form-resumen-filtro").submit();
	})
	$('#siom-form-resumen-filtro select[name=regi_id]').change(function(){
		$("#siom-form-resumen-filtro").submit();
	})
	$('#siom-form-resumen-filtro select[name=clus_id]').change(function(){
		$("#siom-form-resumen-filtro").submit();
	})


    //funciones auxiliares
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-resumen-filtro").submit();
         },filterTimeout);
	}
})(jQuery);
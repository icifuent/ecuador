(function($) {
	$('#pagination').bootpag().on("page", function(event, num){
		//console.log("#pagination: "+num);
		//console.log($('#pagina'));
		page = num;
		$('#pagina').val(page);
		$("#siom-form-sla-calidad").submit();
	});
	
	$('.BotonSLACalidadValidacionEditar').click(function(event) {
		//console.log('.BotonSLACalidadValidacionEditar click:');

		if( $(this).hasClass('clase-editar') ){   
			//console.log("EDITAR");
			$(this).removeClass('clase-editar');
			$(this).addClass('clase-guardar');
			
			$(this).children("span").removeClass("glyphicon-pencil");
			$(this).children("span").addClass("glyphicon-floppy-disk");

			$(this).parent().parent().find(".clase-data").attr("disabled",false);
		}
		else{
			//console.log("GUARDAR");				
			var that = this;
			var data = {};
			var insp_id = $(this).data('insp_id');
			data['sla_calidad_exclusion'] = $(this).parent().parent().find(".clase-data[name='sla_calidad_exclusion']").is(':checked')?1:0;
			data['sla_calidad_observacion'] = $(this).parent().parent().find(".clase-data[name='sla_calidad_observacion']").val();
			                                              
                        if( data['sla_calidad_exclusion'] ==1 && (data['sla_calidad_observacion'] == null || data['sla_calidad_observacion'] == "")){
				alert("Debe ingresar una observacion para excluir la inspeccion");
				return;
			}  
			if((data['sla_calidad_observacion'] == null || data['sla_calidad_observacion'] == "")){
				data['sla_calidad_observacion'] = " ";
			}
			
			url = 'rest/core/sla_inspeccion/upd/'+insp_id;
			$.post(url, data, function(json) {
				if (json.status) {
					$(that).removeClass('clase-guardar');
					$(that).addClass('clase-editar');
					
					$(that).children("span").removeClass("glyphicon-floppy-disk");
					$(that).children("span").addClass("glyphicon-pencil");

					$(that).parent().parent().find(".clase-data").attr("disabled",true);
				}
				else {
					alert("Error actualizando inpeccion: " + json.error + ", ruta:" + url);
				}
			}).fail(function(xhr, textStatus, errorThrown) {
				alert("Error actualizando inpeccion " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
			});
		} 

	});
	
	
	$("button.download").click(function(e){
	  $btn = $(this);
	  $btn.button("loading");
	  $.fileDownload("rest/core/repo/informe/"+$(this).data("info-id"), {
		  prepareCallback:function(url) {
			  $btn.button("processing");
		  },
		  successCallback: function(url) {
			  $btn.button('reset')
		  },
		  failCallback: function(responseHtml, url) {
			  $btn.button('reset')
			  alert(responseHtml);
		  }
	  });
	});        
	
})(jQuery);
(function($) {
	var filterTimer=null;
	var filterTimeout=500;

	//inicialización
    $('select.selectpicker').selectpicker();

	if(window.inspFiltros){
            dt = window.inspFiltros.usin_fecha_fin.split("-");
                if(parseInt(dt[0])<=31){
                    console.log("");
                    window.inspFiltros.usin_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    console.log("");
                    window.inspFiltros.usin_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
                }

                dt = window.inspFiltros.usin_fecha_inicio.split("-");
                if(parseInt(dt[0])<=31){
                    window.inspFiltros.usin_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    window.inspFiltros.usin_fecha_inicio= dt[2]+"-"+dt[1]+"-"+dt[0];
                }
        }
        else{
            window.inspFiltros = {}
            now = new Date();
            d   = now.getDate();
            m   = now.getMonth()+1;
            y   = now.getFullYear();

			if(d < 10){
				d = "0"+d;
			}
			if(m < 10){
				m = "0"+m;
			}
            window.inspFiltros.usin_fecha_fin    = d+"-"+m+"-"+y;
            window.inspFiltros.usin_fecha_inicio = d+"-"+m+"-"+y;
        }
    $("#fecha_inicio").val(window.inspFiltros.usin_fecha_inicio);
    $("#fecha_fin").val(window.inspFiltros.usin_fecha_fin);

    $('#fecha_inicio,#fecha_fin').datepicker({
	    format: "dd-mm-yyyy",
	    viewMode: "days",
	    minViewMode: "days",
	    language: "es"
	})
		$("#periodo").click(function() {  
        if($("#periodo").is(':checked')) {
		$("#siom-form-insp-bandeja").submit();
        } else {
		$("#siom-form-insp-bandeja").submit();
        }  
    });  
 

	//Eventinsp
	$('#fecha_inicio,#fecha_fin').change(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja input[name=insp_id]').keyup(function () {
        /* FiltrarConDelay();*/
    })
	$('#siom-form-insp-bandeja select[name=insp_estado]').change(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja input[name=mant_id]').keyup(function () {
        /* FiltrarConDelay();*/
    })
	$('#siom-form-insp-bandeja select[name=usuario_inspeccion]').change(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja input[name=orse_id]').keyup(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja select[name=orse_tipo]').change(function(){
		/*$("#siom-form-insp-bandeja").submit();*/	
	})
	$('#siom-form-insp-bandeja input[name=empl_nombre]').keyup(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja input[name=empl_nemonico]').keyup(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja input[name=empl_direccion]').keyup(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})
	$('#siom-form-insp-bandeja select[name=comu_id]').change(function(){
		/*$("#siom-form-insp-bandeja").submit();*/
	})

	/*** Boton Limpiar ***/
    $(document).on('click', 'button#LimpiarInspBandeja', function (e) {
        $('#insp_id').val("");
        $('#insp_id').change();

        $('select[name=insp_estado]').val("");
        $('select[name=insp_estado]').change();

        /*	if(window.inspFiltros){
	            dt = window.inspFiltros.usin_fecha_fin.split("-");
                if(parseInt(dt[0])<=31){
                    console.log("");
                    window.inspFiltros.usin_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    console.log("");
                    window.inspFiltros.usin_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
                }

                dt = window.inspFiltros.usin_fecha_inicio.split("-");
                if(parseInt(dt[0])<=31){
                    window.inspFiltros.usin_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    window.inspFiltros.usin_fecha_inicio= dt[2]+"-"+dt[1]+"-"+dt[0];
                }
        	}
        	else{
	            window.inspFiltros = {}
	            now = new Date();
	            d   = now.getDate();
	            m   = now.getMonth()+1;
	            y   = now.getFullYear();

				if(d < 10){
					d = "0"+d;
				}
				if(m < 10){
					m = "0"+m;
				}
	            window.inspFiltros.usin_fecha_fin    = d+"-"+m+"-"+y;
	            window.inspFiltros.usin_fecha_inicio = d+"-"+m+"-"+y;
	        }
	        */
    	//$("#fecha_inicio").val(window.inspFiltros.usin_fecha_inicio);
   		//$("#fecha_fin").val(window.inspFiltros.usin_fecha_fin);
   		$("#fecha_inicio").val("");
   		$("#fecha_inicio").change();
		$("#fecha_fin").val("");
		$("#fecha_fin").change();
        $('select[name=usuario_inspeccion]').val("");
        $('select[name=usuario_inspeccion]').change();

        $('#mant_id').val("");

        $('select[name=tecnico_mantenimiento]').val("");
        $('select[name=tecnico_mantenimiento]').change();

        $('#orse_id').val("");

        $('select[name=orse_tipo]').val("");
        $('select[name=orse_tipo]').change();

        $('#empl_nombre').val("");
        $('#empl_nemonico').val("");
        $('#empl_direccion').val("");

        $('select[name=comu_id]').val("");
        $('select[name=comu_id]').change();
        
    });

    /*** Boton Buscar ***/
    $(document).on('click', 'button#BuscarInspBandeja', function (e) {
        $("#siom-form-insp-bandeja").submit();
        return true;
    });


	//Actualización de fechas
	setInterval(function(){
		$(".timeago").timeago();
	},60000);

	//funciones utiles
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-insp-bandeja").submit();
         },filterTimeout);
	}

	function InitSelectPickers(){
		sp = $('select.selectpicker');
		for(i=0;i<sp.length;i++){
			s = $(sp[i]);

			name = s.attr('name');
			val  = s.data('value');

			s.selectpicker('val',val);
			s.selectpicker('refresh');
		}
	}
	
	InitSelectPickers();


    $("#insp_id").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9]/gi, ''));
    });

	$("#insp_id").attr('maxlength','11');

    $("#mant_id").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9]/gi, ''));
	});

	$("#mant_id").attr('maxlength','11');


    $("#empl_nombre").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9a-z°ñÑ.\s]/gi, ''));
	});
	
	$("#empl_nombre").attr('maxlength','100');

	$("#empl_nemonico").on('input', function() {
        $(this).val($(this).val().replace(/[^a-z0-9ñÑ-]/gi, ''));
    });

	$("#empl_nemonico").attr('maxlength','30');

	$("#empl_direccion").on('input', function() {
        $(this).val($(this).val().replace(/[^0-9a-z°ñÑ.\s-]/gi, ''));
	});

	$("empl_direccion").attr('maxlength','150');
	
})(jQuery);

(function($) {

    var filterTimer = null;
    var filterTimeout = 500;
    var currentContId = null;
    var refe_id=[];
    

    $('.selectpicker').selectpicker();
    $('#WizardEmplazamiento').wizard('destroy');
    $('#WizardUsuario').wizard('destroy');
    $('#WizardEmpresa').wizard('destroy');
    $('#WizardCurso').wizard('destroy');
    $('#WizardContrato').wizard();

    $('#cont_fecha_inicio').datepicker({
        format: "dd-mm-yyyy",
        viewMode: "days",
        minViewMode: "days",
        language: "es"
    });
    $('#cont_fecha_termino').datepicker({
        format: "dd-mm-yyyy",
        viewMode: "days",
        minViewMode: "days",
        language: "es"
    });

    $('#FormContratoLista input[name=cont_nombre]').keyup(function() {
        FiltrarConDelay();
    })
    $('#FormContratoLista select[name=cont_estado]').change(function() {
        //console.log($("#FormContratoLista"));
        $("#FormContratoLista").submit();
    })

    $(document).off('click', '#WizardUsuario li#Primero');
    $(document).on('click', '#WizardContrato li#Primero', function(e) {
        $("#AgregarContrato").show();
        $("#FormContratoLista").submit();
    });

    $(document).off('click', '#WizardUsuario li#Segundo');
    $(document).on('click', '#WizardUsuario li#Segundo', function(e) {

    });



    //BOTONES___________________________________________________________________

    $(document).off('click', 'a#BotonContratoAgregar');
    $(document).on('click', 'a#BotonContratoAgregar', function(e) {
        $("#AgregarContrato").hide();
        $("#FormContratoEditar").trigger('reset');
        $('#FormContratoEditar.selectpicker').selectpicker('refresh');

        $("input#cont_id_form").val("");
        $('input#usua_creador').val(window.user_id);

        $('#WizardContrato').wizard('selectedItem', {step: 2});
    });

    $(document).off('click', '#BotonContratoEditar');
    $(document).on('click', '#BotonContratoEditar', function(e) {
        //limpiarformulario("#FormContratoEditar");
        $("#AgregarContrato").hide();

        cont_id = $(this).data("cont_id_form");

        var data = $(this).data("data");


        data['cont_fecha_inicio'] = FormatoFecha(data['cont_fecha_inicio']);
        data['cont_fecha_termino'] = FormatoFecha(data['cont_fecha_termino']);


        $.each(data, function(key, value) {
            obj = $('#FormContratoEditar').find('#' + key);
            obj.val(value);
            if (obj.attr('type') === "checkbox") {
                obj.prop("checked", (value > 0) ? true : false);
            }
        });

        $('input#cont_id_form').val(cont_id);
        $('#WizardContrato').wizard('selectedItem', {step: 2}); 
    });

    $(document).off('click', '#BotonContratoZonas');
    $(document).on('click', '#BotonContratoZonas', function(e) {
        cont_id = $('#cont_id_form').val();
		console.log("VALORES");
		console.log(cont_id);
		cont_nombre = $('#cont_nombre').val();
		console.log(cont_nombre);
		
		if((cont_id != null && cont_id.trim().length != 0) && (cont_nombre != null && cont_nombre.trim().length != 0 )){
			CargaContratoZonas(cont_id);
			$('#WizardContrato').wizard('selectedItem', {step: 3});
		} else {
			alert("Para avanzar al siguiente paso primero debe de guardar el nuevo contrato");
			return false;
		}
        
    });
    
    $(document).off('click', '#BotonContratoLPU');
    $(document).on('click', '#BotonContratoLPU', function(e) {
        cont_id = $('#cont_id_form').val();
        CargaContratoLPU(cont_id);
        $('#WizardContrato').wizard('selectedItem', {step: 4});
    });

    $(document).off('click', '#BotonContratoDocumentos');
    $(document).on('click', '#BotonContratoDocumentos', function(e) {
        cont_id = $('#cont_id_form').val();
        CargaContratoDocumentos(cont_id);
        $('#WizardContrato').wizard('selectedItem', {step: 5});
    });

    $(document).off('click', '#BotonContratoEspecialidad');
    $(document).on('click','#BotonContratoEspecialidad',function(e){
		cont_id = $('#cont_id_form').val();
		CargarContratoEspecialidad(cont_id);
        CargarContratoFormulario(cont_id); 
        $('#WizardContrato').wizard('selectedItem',{step: 6});
    });

    $(document).off('click', '#BotonContratoPeriodicidad');
    $(document).on('click','#BotonContratoPeriodicidad',function(e){
        
		cont_id = $('#cont_id_form').val();
        $('#WizardContrato').wizard('selectedItem',{step: 7});
        cargarListaPeriodicidad();
        CargarContratoPeriodicidad(cont_id);
    });
    
    $(document).off('click', '#BotonContratoMantenimiento');
    $(document).on('click','#BotonContratoMantenimiento',function(e){
        
        $('#WizardContrato').wizard('selectedItem',{step: 8});
        CargarmantDefFormulario(cont_id);

        CargarSelectpickerMantenimiento();
        
    });

    $(document).off('click', 'button#BotonContratoCambiarEstado');
    $(document).on('click', 'button#BotonContratoCambiarEstado', function(e) {
        cont_id = $(this).data("id");
        cont_nombre = $(this).data("nombre");
        cont_estado = $(this).data("estado");

        if (cont_estado == "ACTIVO") {
            nuevo_estado = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
        }
        else {
            nuevo_estado = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("El Contrato <b>" + cont_nombre + "</b> sera <b>" + nombre_nuevo_estado + "</b>,<br>Desea continuar?", function(status) {
            if (status == true) {
                $.get('rest/core/contrato/upd/' + cont_id, {cont_estado: nuevo_estado}, function(json) {
                    $("#FormContratoLista").submit();

                }).fail(function(xhr, textStatus, errorThrown) {
                    alert("Error " + xhr.status + ": " + xhr.statusText);
                });
            }
        });

    });
    
    $(document).off('click', '.refe_preseleccion');
    $(document).on('click','.refe_preseleccion',function(e){
        var id=this['id'].split('_');
        $('input:checkbox[id=form_'+id[1]+']').prop('checked',true);

    });
   
    $(document).off('click', '.form_id');
    $(document).on('click','.form_id',function(e){
        var marcado = $(this).prop("checked");
        var id=this['id'].split('_');
        espe=$("#Contratoespe :selected").text();
        
        if(espe==""){
            alert("Seleccione una especialidad");
        }else{
            if(marcado==false){
                 $('input:checkbox[id=refe_'+id[1]+']').prop('checked',false);

            }
        }        

    });

    $(document).off('click', '#BotonGuardarEspecialidadFormulario');
    $(document).on('click','#BotonGuardarEspecialidadFormulario',function(e){ 
       
            var form_espe=new Array();
            var refe_data=new Array();
            var uncheck=new Array();
            //check para la preseleccion manda el id de preseleccionado
            $('input:checkbox[name=refe_preseleccion]:checked').each(
                function() {
                    var id=this['id'].split("_");
                    refe_data.push({form_id:id[1]});  
                }
            );

            $('input:checkbox[name=form_id]:checked').each(
                    
                function() {
                   id=this['id'].split("_");
                   refe=this['value'].split("_");
                   form_espe.push({espe_id: $('#Contratoespe').val(), form_id: id[1],refe_preseleccion:0 });

                }
            );
          
            for(var i=0;i<refe_data.length;i++){
                refe_data[i]['form_id'];
                for(var j=0;j<form_espe.length;j++){
                    form_espe[j]['form_id']
                    if (refe_data[i]['form_id']==form_espe[j]['form_id']) {
                        form_espe[j]['refe_preseleccion']=1;
                    }
                }
            }
            
            
            url='rest/core/contrato/1/especialidad/formulario/add';
            data={form:form_espe};
            $.post(url,data,function(json){
                if (json.status) {
                    alert("Formulario modificado exitosamente");
                }else{
                    alert(json.error);

                    console.log(json.error);
                }
            });

    });
 
    //ZONAS_____________________________________________________________________

    function CargaContratoZonas(cont_id) {
        //console.log("CargaContratoZonas");  
        $("#TableContratoZonasLista").html("");        
        $("#TableContratoZonasCrear").html("");        
        
        
        
        var url = 'rest/core/zona/filtros';
        $.get(url, null, function(json) {
            if (json.status) { 
                var tipos   = json.tipos;
                var estados = json.estados;
                
                var html = '<table class="table" style="margin:10px; margin-bottom:25px">' +							
                                '<tr>' +
                                        '<td>' +
                                                '<select name="zona_tipo" class="selectpicker" title="Seleccione tipo" data-container="body">' +
                                                        '<option value="" disabled selected>Seleccione tipo</option>';                                                
                $.each(tipos, function(key, tipo) {
                                                html += '<option value="' + tipo + '">' + tipo + '</option>';
                });
                                        html += '</select>' +
                                        '</td>' +
                                        '<td>' +
                                                '<input type="text" class="form-control" name="zona_nombre" placeholder="Nombre">' +
                                        '</td>' +
                                        '<td>' +
                                                '<input type="text" class="form-control" name="zona_alias" placeholder="Alias">' +
                                        '</td>' +
                                        '<td style="width:40%">' +
                                                '<input type="text" class="form-control" name="zona_descripcion" placeholder="Descripcion">' +
                                        '</td>' +
                                        '<td>' + 
                                                '<select name="zona_estado" class="selectpicker" title="Seleccione estado" data-container="body">';
                $.each(estados, function(key, estado) {
                                                html += '<option value="' + estado + '">' + estado + '</option>';
                });
                                        html += '</select>' +
                                        '</td>' +
                                        '<td>' +
                                                '<button type="submit" class="btn btn-primary btn-xs">CREAR ZONA</button>' +
                                        '</td>' +
                                '</tr>' +
                        '</table>';
                $("#TableContratoZonasCrear").append(html);    
                
                
                
                url = 'rest/core/contrato/'+cont_id+'/zona/list';
                $.get(url, null, function(json) {
                    if (json.status) {
                        var zonas = json.data;
                        var html = "";                
                        var tipos = [];
                        $.each(zonas, function(i , zona){
                            if($.inArray(zona['zona_tipo'], tipos) === -1) tipos.push(zona['zona_tipo']);
                        });

                        $.each(tipos, function(i , tipo){                    
                            html += '<h5>' + tipo + '</h5>' +
                                    '<table class="table table-striped" style="margin:10px">	' +						
                                            '<tr>' +
                                                    '<th>NOMBRE</th>' +
                                                    '<th>ALIAS</th>' +
                                                    '<th>DESCRIPCION</th>' +
                                                    '<th>ESTADO</th>' +
                                                    '<th></th>' +
                                            '</tr>';

                            $.each(zonas, function(j , zona){
                                if( zona['zona_tipo']==tipo ){
                                    html += '<tr>' +
                                                    '<td><input type="text" class="form-control zona-data" disabled id="zona_nombre" placeholder="Nombre" value="' + zona['zona_nombre'] + '"></input></td>' +
                                                    '<td><input type="text" class="form-control zona-data" disabled id="zona_alias" placeholder="Alias" value="' + zona['zona_alias'] + '"></input></td>' +
                                                    '<td><input type="text" class="form-control zona-data" disabled id="zona_descripcion" placeholder="Descripcion" value="' + ((zona['zona_descripcion']===null)?"":zona['zona_descripcion']) + '"></input></td>' +
                                                    '<td>' + 
                                                        '<span class="glyphicon glyphicon-stop" aria-hidden="true" style="color:' + ((zona['zona_estado']=='ACTIVO')?"green":((zona['zona_estado']=='NOACTIVO')?"red":"yellow")) + '; margin-right:5px; font-size:1.5em"></span>'+
                                                        '<select id="zona_estado" class="selectpicker zona-data" disabled title="Seleccione estado" data-container="body">';
                                    $.each(estados, function(key, estado) {
                                                         html += '<option value="' + estado + '" '+ ((zona['zona_estado']==estado)?"selected":"") +'>' + estado + '</option>';
                                    });
                                    html +=             '</select>' +
                                                    '</td>' +
                                                    '<td style="text-align:right">' +
                                                            '<button id="BotonContratoZonaEditar" type="button" class="btn btn-default btn-xs zona-editar" data-zona_id="' + zona['zona_id'] + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
                                                            //'<button id="BotonContratoZonaBorrar" type="button" class="btn btn-default btn-xs" data-zona_id="' + zona['zona_id'] + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>' +
                                                    '</td>' +
                                            '</tr>';     
                                }                         
                            });  

                            html += '</table>';
                        });

                        $("#TableContratoZonasLista").append(html);
                    }
                    else{
                        alert("Error obteniendo lista de zonas: " + json.error + ", ruta: "+ url);
                    }
                }).fail(function() {
                    alert("Error obteniendo lista de zonas: Error en GET, ruta:" + url);
                });                  
                
            }
            else {
                alert("Error obteniendo lista de zonas: " + json.error + ", ruta: " + url);
            }
        }).fail(function() {
            alert("Error obteniendo lista de zonas: Error en GET, ruta:" + url);
        }); 
        
        
         //
                
                /*
                data = $(this).data();
                $.ajax({
                    url: "templates/os_presupuesto_item.hb",
                    async: true,
                    success: function(src){
                        template = Handlebars.compile(src);
                        $("#lista_presupuesto").append(template(data));

                        UpdateCorrelativo();
                        UpdateSubtotal(data.lpipId+"-"+(nroCorrelativo-1));
                        UpdateTotal();    
                  },
                });*/ 
    }
    
    $(document).off('click', '#BotonContratoZonaEditar');
    $(document).on('click', '#BotonContratoZonaEditar', function(e) {
        //console.log('#BotonContratoZonaEditar click');
        //console.log($(this));
        
        if( $(this).hasClass('zona-editar') ){   
            //console.log("EDITAR");
            $(this).removeClass('zona-editar');
            $(this).addClass('zona-guardar');
            
            $(this).children("span").removeClass("glyphicon-pencil");
            $(this).children("span").addClass("glyphicon-floppy-disk");

            $(this).parent().parent().find(".zona-data").each(function(index) {
                $(this).prop("disabled", false);
            });
        }
        else{
            //console.log("GUARDAR");
            $(this).removeClass('zona-guardar');
            $(this).addClass('zona-editar');
            
            $(this).children("span").removeClass("glyphicon-floppy-disk");
            $(this).children("span").addClass("glyphicon-pencil");

            var data = {}; 
            $(this).parent().parent().find(".zona-data").each(function(index) {
                $(this).prop("disabled", true);
                data[$(this).attr('id')] = $(this).val();
            });
            
            var zona_id = $(this).data('zona_id'); 
            var cont_id = $('#cont_id_form').val();
            //console.log(data);           
            
            url = 'rest/core/contrato/'+cont_id+'/zona/upd/'+zona_id;
            
            $.post(url, data, function(json) {
                console.log("URL POST");
                if (json.status) {
                    console.log("IF URL POST");
                    CargaContratoZonas(cont_id);
                    alert("Zona guardada exitosamente");
                }
                else {
                    console.log("ELSE URL POST");
                    alert("Error guardando zona; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                console.log("FAIL URL POST");
                alert("Error guardando zona " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        }        
        
    });
    /*
    $(document).on('click', '#BotonContratoZonaBorrar', function(e) {
        console.log('#BotonContratoZonaBorrar click');

        var zona_id = $(this).data('zona_id');
        var cont_id = $('#cont_id_form').val();
        
        url = 'rest/core/contrato/'+cont_id+'/zona/del/'+zona_id;

        $.get(url, data, function(json) {
            if (json.status) {
                CargaContratoZonas(cont_id);
            } 
            else {
                alert("Error desactivando zona; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error desactivando zona " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
        });
    });*/
    
    $('#FormContratoZonas').submit(function() {
        //console.log("FormContratoZonas submit");
        
        var data = {};
        $.each($('#FormContratoZonas').serializeArray(), function(i, field) {
            data[field.name] = field.value;
        });
        
        //console.log(data);
        
        if ( data['zona_estado'] === undefined || data['zona_estado'] === null || data['zona_estado'].length === 0) {
            alert("Debe seleccionar un estado");  return false;
        }
        
        if ( data['zona_tipo'] === undefined || data['zona_tipo'] === null || data['zona_tipo'].length === 0) {
            alert("Debe seleccionar un tipo");  return false;
        }
        
        if (data['zona_nombre'] === null || data['zona_nombre'].length === 0) {
            alert("Debe ingresar un nombre");  return false;
        }
        
        if (data['zona_alias'] === null || data['zona_alias'].length === 0) {
            alert("Debe ingresar un alias");  return false;
        }

        //cont_id=$("#cont_id_form").val();
        url = 'rest/core/contrato/'+cont_id+'/zona/add';
        console.log("URL");
        //url = 'rest/core/contrato/'+data['cont_id']+'/zona/add';
        $.post(url, data, function(json) {
            console.log(data);
            console.log("POST URL");
            if (json.status) {
                console.log("IF POST URL");
                //CargaContratoZonas(data['cont_id']);
                CargaContratoZonas(cont_id);
            } 
            else {
                alert("Error guardando zona; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error guardando zona " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
        });
        
    });

    function CargarContratoFormularioEspecialidad(cont_id,espe_id){
       
        var url="rest/core/contrato/"+cont_id+"/especialidad/"+espe_id+"/formulario/list";
        $.get(url,null,function(json){

            if(json.status){
                form_ids=[];
                refe_id=[];
                $('input:checkbox[name=form_id]').prop('checked',false);
                $('input:checkbox[name=refe_preseleccion]').prop('checked',false);
                $('input:checkbox[name=refe_preseleccion]').val('0');
                
                $.each(json.data,function(key,form){
                    
                    if(form['refe_preseleccion']=="1"){
                        $('input:checkbox[id=refe_'+form['form_id']+']').prop('checked',true);
                        $('input:checkbox[id=refe_'+form['form_id']+']').val(form['refe_preseleccion']);
                        
                    }
                    //se agrega la preseleccion dentro del value del check + el refe id
                    $('input:checkbox[id=form_'+form['form_id']+']').prop('checked',true);
                    $('input:checkbox[id=form_'+form['form_id']+']').val(form['refe_preseleccion']);                  



                    
                });
               
            }

        });
    }

    

    function CargarContratoFormulario(cont_id){

        var url="rest/core/formulario/list?cont_id="+cont_id+"&form_estado=ACTIVO";

        $.get(url,null,function(json){
           
            if(json.status){
                var html='';
                
                $.each(json.data,function(key,form){
                 
                  //  html += '<option value="'+form['form_id']+'"  >'+form['form_nombre']+'</option>';
                   html += '<div class="col-md-12">'+
                    '<div class="info text-left col-md-8"><h5>'+form['form_nombre']+'</h5>'+
                    '</div>'+
                    '<div class="col-md-2"><input type="checkbox" class="form_id" name="form_id" id="form_'+form['form_id']+'" value="'+form['form_id']+'"></input></div>'+
                    '<div class="col-md-2"><input type="checkbox" class="refe_preseleccion" name="refe_preseleccion" id="refe_'+form['form_id']+'" value="0"></div>'+
                    '</div>';
                });
                 
                $("#form_id").html(html);
            }
        });
    }

    function CargarContratoEspecialidad(cont_id){
        var url ='rest/core/especialidad/list?cont_id='+cont_id;
        
        $.get(url,null,function(json){

            if(json.status){
                var html='<option value=""></option>';
                $.each(json.data,function(key,espe){
                    html += '<option value="'+espe['espe_id']+'" name="'+espe['espe_nombre']+'" >'+espe['espe_nombre']+'</option>';

                });
                 
                $("#Contratoespe").html(html).selectpicker('refresh');
            }
        });
    }

    //LPU_______________________________________________________________________
    
    function CargaContratoLPU(cont_id) {
        //console.log("CargaContratoLPU cont_id:" + cont_id );  

//        url = 'rest/contrato/lpu/list';
//        url = 'rest/contrato/lpu/1/grupo/list';
//        url = 'rest/contrato/'+cont_id+'/os/presupuesto/30';
//        url = 'rest/contrato/'+cont_id+'/os/presupuesto/30';
        
        var url = 'rest/core/contrato/'+cont_id+'/lpu/list';
        var option_lpu_selected=0;

        $.get(url, null, function(json) {
            //console.log(url);console.log(json);
            if (json.status) {
                var html = '<option value=""></option>';
                $.each(json.data, function(key, lpu) {
                    
                    if(json.total>0 && option_lpu_selected==0){
                        option_lpu_selected=lpu['lpu_id'];
                        $("#ContratoLPUAgregar")[0].style.display="none";
                    }
                    html += '<option value="' + lpu['lpu_id'] + '" data-lpu_nombre="' + lpu['lpu_nombre'] + '" data-lpu_estado="' + lpu['lpu_estado'] + '"  data-content="<span class=\'glyphicon glyphicon-stop\' aria-hidden=\'true\' style=\'color:'+((lpu['lpu_estado']==='ACTIVO')?'green':'red')+';font-size: 1.3em; padding-right:10px\' ></span>' + lpu['lpu_nombre'] + '"></option>';
                
                });
                $("#ContratoLPU").html(html).selectpicker('refresh');
                
                $('#ContratoLPU').val(option_lpu_selected);
                $("#ContratoLPU").trigger("change");

            } 
            else {
                alert("Error cargando LPUs; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error cargando LPUs " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
        });
        /*
        $("#ContratoLPUGrupo").html('<option value=""></option>').selectpicker('refresh');
        $("#ContratoLPUSubgrupo").html('<option value=""></option>').selectpicker('refresh');
        $("#ContratoLPUSubgrupoClase").html("");
        $("#ContratoLPUSubgrupoClaseGuardarForm").val(""); 
        
        $("#ContratoLPUGuardarForm").fadeOut('slow');
        $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
        $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow');
        
        $("#ContratoLPUSubgrupoItems").html("");
*/
    }

    function CargaContratoLPUGrupo(cont_id,lpu_id) {
        //console.log("CargaContratoLPUGrupo lpu_id:" + lpu_id );  

        var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/list';
        
        $.get(url, null, function(json) {
            //console.log(url);console.log(json);
            if (json.status) {               
                var html = '<option value=""></option>';
                $.each(json.data, function(key, lpu_grupo) {
                    if( !(lpu_grupo['lpgr_padre']>0) ){
                        html += '<option value="' + lpu_grupo['lpgr_id'] + '">' + lpu_grupo['lpgr_nombre'] + '</option>';
                    }                    
                });
                $("#ContratoLPUGrupo").html(html).selectpicker('refresh');
                
            } 
            else {
                alert("Error cargando grupos; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error cargando grupos " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
        });
        
        $("#ContratoLPUSubgrupo").html('<option value=""></option>').selectpicker('refresh');
        $("#ContratoLPUSubgrupoClase").html("");
        $("#ContratoLPUSubgrupoClaseGuardarForm").val("");
        
        $("#ContratoLPUGuardarForm").fadeOut('slow');
        $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
        $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow')
        
        $("#ContratoLPUSubgrupoItems").html("");
    }
        
    function CargaContratoLPUSubgrupo(cont_id, lpu_id, lpgr_padre) {
        //console.log("CargaContratoLPUSubgrupo lpu_id:" + lpu_id + " lpgr_padre:" + lpgr_padre );  

        var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/list?lpgr_padre='+lpgr_padre;
        
        $.get(url, null, function(json) {
            //console.log(url);console.log(json);
            if (json.status) {               
                var html  = '<option value=""></option>';
                $.each(json.data, function(key, lpu_grupo) {
                    if( lpu_grupo['lpgr_padre'] === lpgr_padre ){
                        html += '<option value="' + lpu_grupo['lpgr_id'] + '">' + lpu_grupo['lpgr_nombre'] + '</option>';
                    }                    
                });
                $("#ContratoLPUSubgrupo").html(html).selectpicker('refresh');
            } 
            else {
                alert("Error cargando subgrupos; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error cargando subgrupos " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
        });
        
        $("#ContratoLPUSubgrupoClase").html("");
        $("#ContratoLPUSubgrupoClaseGuardarForm").val("");
        
        $("#ContratoLPUGuardarForm").fadeOut('slow');
        $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
        $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow')
        
        $("#ContratoLPUSubgrupoItems").html("");
    }
    
    function CargaContratoLPUSubgrupoClase(cont_id, lpu_id,lpgr_id) {
        //console.log("CargaContratoLPUSubgrupoClase lpgr_id:" + lpgr_id);  

        var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/clase/list';
        
        $.get(url, null, function(json) {
            //console.log(url);console.log(json);
            if (json.status) {               
                var html = '';
                $.each(json.data, function(key, lpu_grupo_clase) {                    
                    html += '<li>' +
                                '<div class="input-group">' +
                                        '<input disabled type="text" class="form-control clase-data" placeholder="Nombre clase" value="'+lpu_grupo_clase['lpgc_nombre']+'"/>' +
                                        '<span class="input-group-btn">' +
                                                '<button type="button" class="btn btn-default btn-xs clase-editar ContratoLPUSubgrupoClaseEditar" data-lpgc_id="'+lpu_grupo_clase['lpgc_id']+'"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
                                        '</span>	' +
                                '</div>' +
                        '   </li>';                                    
                });
                $("#ContratoLPUSubgrupoClase").html(html);
                $("#ContratoLPUSubgrupoClaseGuardarForm").val("");
            } 
            else {
                alert("Error cargando clases; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error cargando clases " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
        });
        
    }
    
    function CargaContratoLPUSubgrupoItems(cont_id, lpu_id, lpgr_id) {
        console.log("CargaContratoLPUSubgrupoItems lpgr_id:" + lpgr_id);  
        
        var url = 'rest/core/contrato/' + cont_id + '/zona/list?zona_tipo=CONTRATO';

        $.get(url, null, function(json) {
           // console.log(url);
           // console.log(json);
            var zonas = json.data;
            if (json.status) {
                
                url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/clase/list';

                $.get(url, null, function(json) {
                    console.log(url);
                    console.log(json);
                    
                    var clases = json.data;
                    if (json.status) {

                        $("#ContratoLPUSubgrupoItems").data("items_zonas", zonas);
                        $("#ContratoLPUSubgrupoItems").data("items_clases", clases);
                                                
                        url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/item/list';
        
                        $.get(url, null, function(json) {
                            console.log(url);
                            console.log(json);
                            if (json.status) {

                                var html = '';
                                $.each(json.data, function(key, lpu_grupo_item) {
                                    html += '<div class="siom-os-presupuesto" style="margin:10px; padding-bottom:5px">' +
                                            '<button type="button" class="btn btn-primary pull-right ContratoLPUSubgrupoItemEditar" data-lpit_id="' + lpu_grupo_item['lpit_id'] + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>' +
                                            '<div class="nombre">' + lpu_grupo_item['lpit_nombre'] + '</div>' +
                                            '<div class="grupo">' + lpu_grupo_item['lpit_comentario'] + '</div>' +
                                            '<div class="unidad">Unidad: <strong>' + lpu_grupo_item['lpit_unidad'] + '</strong></div>' +
                                            '</div>';
                                });
                                $("#ContratoLPUSubgrupoItems").html(html);
                            }
                            else {
                                alert("Error cargando items; " + json.error + ", ruta:" + url);
                            }
                        }).fail(function(xhr, textStatus, errorThrown) {
                            alert("Error cargando items " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
                        });                        
                    }
                    else {
                        alert("Error cargando clases; " + json.error + ", ruta:" + url);
                    }
                }).fail(function(xhr, textStatus, errorThrown) {
                    alert("Error cargando clases " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
                });
            }
            else {
                alert("Error cargando zonas; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error cargando zonas " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
        });


        
    }
    
    
    function CargaContratoLPUSubgrupoItemPrecios(cont_id, lpu_id, lpgr_id, lpit_id) {
        
        var clases = $("#ContratoLPUSubgrupoItems").data('items_clases');
        var zonas  = $("#ContratoLPUSubgrupoItems").data('items_zonas');
        //console.log("clases"); console.log(clases);
        //console.log("zonas"); console.log(zonas);
        
        if( lpit_id > 0 ){
            var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/item/'+lpit_id+'/precio/list';

            $.get(url, null, function(json) {
                //console.log(url); console.log(json);
                if (json.status) {  
                    var precios = json.data;

                    url = 'rest/core/contrato/' + cont_id + '/lpu/' + lpu_id + '/grupo/' + lpgr_id + '/item/list?lpit_id=' + lpit_id;

                    $.get(url, null, function(json) {
                        console.log("aquiii");
                        console.log(url); console.log(json);
                        var item = json.data[0];

                        if (json.status) {

                            var html = '<table class="table table-striped table-hover table-condensed">' +
                                            '<tr>' +
                                                    '<th>Clase</th>' +
                                                    '<th>Zonas</th>' +
                                                    '<th>Precio</th>' +
                                                    '<th>CAPEX</th>' +
                                                    '<th>OPEX</th>' +
                                                    '<th>Code1</th>' +
                                                    '<th>Code2</th>' +
                                                    '<th>Code3</th>' +
                                                    '<th>Code4</th>' +
                                                    '<th>Code5</th>' +                                        
                                                    '<th>Estado</th>' +
                                                    '<th></th>' +
                                            '</tr>';

                            $.each(clases, function(key, clase) {
                                html += '<tr>' +
                                                '<td nowrap rowspan="'+zonas.length+'">'+clase['lpgc_nombre']+'</td>';
                                var first = true;
                                $.each(zonas, function(key, zona) {
                                    if( first ){ first = false; } else{ html += '<tr>';}
                                    html +=     '<td nowrap>'+zona['zona_nombre']+'</td>';
                                    var found = false;
                                    $.each(precios, function(key, precio) {
                                        if( precio['zona_id']==zona['zona_id'] && precio['lpgc_id']==clase['lpgc_id'] ){
                                            found = true;

                                        html += '<td nowrap>'+precio['lpip_precio']+'</td>' +
                                                '<td nowrap>'+((precio['lpip_sap_capex']==null)?'':precio['lpip_sap_capex'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_sap_opex']==null)?'':precio['lpip_sap_opex'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_code1']==null)?'':precio['lpip_code1'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_code2']==null)?'':precio['lpip_code2'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_code3']==null)?'':precio['lpip_code3'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_code4']==null)?'':precio['lpip_code4'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_code5']==null)?'':precio['lpip_code5'])+'</td>' +
                                                '<td nowrap>'+((precio['lpip_estado']==null)?'':precio['lpip_estado'])+'</td>' +
                                                '<td nowrap><button type="button" class="btn btn-default btn-xs ContratoLPUSubgrupoItemPrecioEditar" data-lpit_id="' + precio['lpit_id'] + '" data-lpip_id="' + precio['lpip_id'] + '" data-zona_nombre="' + zona['zona_nombre'] + '" data-lpgc_nombre="' + clase['lpgc_nombre'] + '" data-zona_id="' + zona['zona_id'] + '" data-lpgc_id="' + clase['lpgc_id'] + '" style="padding:2px;height:20px"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></td>' +
                                        '</tr>'; 
                                            /*
                                        html += '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+precio['lpip_precio']+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_sap_capex']==null)?'':precio['lpip_sap_capex'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_sap_opex']==null)?'':precio['lpip_sap_opex'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_code1']==null)?'':precio['lpip_code1'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_code2']==null)?'':precio['lpip_code2'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_code3']==null)?'':precio['lpip_code3'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_code4']==null)?'':precio['lpip_code4'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_code5']==null)?'':precio['lpip_code5'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><input type="text" class="form-control precio-data" disabled id="lpip_precio" placeholder="" value="'+((precio['lpip_estado']==null)?'':precio['lpip_estado'])+'" style="border:none;background:white;font-size:1em;padding:0px"></input></td>' +
                                                '<td nowrap><button type="button" class="btn btn-default btn-xs ContratoLPUSubgrupoItemPrecioEditar" data-lpip_id="' + precio['lpip_id'] + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></td>' +
                                        '</tr>';*/
                                            return;
                                        }
                                    });                        
                                    if( !found ){
                                        html += '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td></td>' +
                                                '<td nowrap><button type="button" class="btn btn-default btn-xs ContratoLPUSubgrupoItemPrecioEditar" data-lpit_id="' + lpit_id + '" data-lpip_id="" data-zona_nombre="' + zona['zona_nombre'] + '" data-lpgc_nombre="' + clase['lpgc_nombre'] + '" data-zona_id="' + zona['zona_id'] + '" data-lpgc_id="' + clase['lpgc_id'] + '" style="padding:2px;height:20px"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button></td>' +
                                        '</tr>';
                                    }

                                });

                            });

                            html += '</table>';
                            $("#ContratoLPUSubgrupoItemEditarInfoPrecios").html(html);  

                            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_nombre").val(item['lpit_nombre']);
                            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_comentario").val(item['lpit_comentario']);  
                            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_unidad").val(item['lpit_unidad']);  
                            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_id").val(item['lpit_id']);
                            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").hide();

                            if(!$('#ContratoLPUSubgrupoItemEditarModal').hasClass('in')){
                                $('#ContratoLPUSubgrupoItemEditarModal').modal('show'); 
                            }
                        }
                        else {
                            alert("Error cargando item; " + json.error + ", ruta:" + url);
                        }
                    }).fail(function(xhr, textStatus, errorThrown) {
                        alert("Error cargando item " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
                    });
                } 
                else {
                    alert("Error cargando item; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error cargando item " + xhr.status + ": " + xhr.statusText+ ", ruta:" + url);
            });    
        }
        else{
            
            $("#ContratoLPUSubgrupoItemEditarInfoPrecios").html("");  

            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_nombre").val("");
            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_comentario").val("");  
            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_unidad").val("");  
            $("#ContratoLPUSubgrupoItemEditarInfo input#lpit_id").val("");
            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").hide();

            if(!$('#ContratoLPUSubgrupoItemEditarModal').hasClass('in')){
                $('#ContratoLPUSubgrupoItemEditarModal').modal('show'); 
            }
        }
    }



    
    $("#ContratoLPU").on('change', function() {
        //console.log("#ContratoLPU change lpu_id:" + $(this).val());
        var cont_id = $("#cont_id_form").val();
        var lpu_id = $(this).val();
        if( lpu_id>0 ){
            CargaContratoLPUGrupo(cont_id, lpu_id);
        }
        $("#ContratoLPUGuardarForm").fadeOut('slow');
        $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
        $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow');
    });
    
    $(document).off('click', '#ContratoLPUEditar');
    $("#ContratoLPUEditar").on('click', function() {
        //console.log("#ContratoLPUEditar click lpu_id:" + $("#ContratoLPU").val());
        var lpu_id =  $("#ContratoLPU").val();
        if( lpu_id>0 ){            
            $("#ContratoLPUGuardarForm input[type='hidden']").val(lpu_id);   
            $("#ContratoLPUGuardarForm input[type='text']").val($("#ContratoLPU").find("option:selected").data('lpu_nombre')); 
            $("#ContratoLPUGuardarForm").fadeIn('slow');
        }
        else{
            alert("Debe seleccionar una LPU para editar");
        }
    });
    
    $(document).off('click', '#ContratoLPUAgregar');
    $("#ContratoLPUAgregar").on('click', function() {
        //console.log("#ContratoLPUAgregar click");
        $("#ContratoLPUGuardarForm input[type='hidden']").val(""); 
        $("#ContratoLPUGuardarForm input[type='text']").val("");
        $("#ContratoLPUGuardarForm").fadeIn('slow'); 
    });
    

    //---------especialidad-------------------///
    $(document).off('click', '#ContratoESPEditar');
    $("#ContratoESPEditar").on('click',function(){
        var espe_id=$('#Contratoespe').val();
        var name=$("#Contratoespe :selected").text();

        if(espe_id > 0){
            $("#ContratoESPuardarForm input[type='hidden']").val(espe_id); 
            $("#ContratoESPuardarForm input[type='text']").val(name);
            $("#ContratoESPuardarForm").fadeIn('slow'); 
        }else{
            alert("Debe seleccionar una Especialidad para editar");
        }
    });

    $(document).off('click', '#ContratoESPAgregar');
    $("#ContratoESPAgregar").on('click', function() {
        console.log("#ContratoLPUAgregar click");

        $("#ContratoESPuardarForm input[type='hidden']").val(""); 
        $("#ContratoESPuardarForm input[type='text']").val("");
        $("#ContratoESPuardarForm").fadeIn('slow'); 
    });
    
    $(document).off('click', '#CancelarEspecialidad');
    $("#CancelarEspecialidad").on('click', function() {    
        $("#ContratoESPuardarForm input[type='hidden']").val(""); 
        $("#ContratoESPuardarForm input[type='text']").val("");
        $("#ContratoESPuardarForm").fadeOut('slow'); 
    });

    $(document).off('click', '#ContratoESPGuardar');
    $('#ContratoESPGuardar').on('click',function(){
        var cont_id = $('#cont_id_form').val();
        var espe_id = $("#hidden_esp_guardar").val();
        var espe_nombre = $("#ContratoESPuardarForm input[type='text']").val();
        var espe_estado = $("#ContratoESPuardarForm select").val();
        
        if (espe_id=="") {

            if(espe_nombre.length > 0){
                var url='rest/core/contrato/'+cont_id+'/especialidad/add?espe_nombre='+espe_nombre+'&espe_estado='+espe_estado+'&cont_id='+cont_id;
                $.get(url,null,function(json){
                    if (json.status) {
                        alert("Especialidad guardada exitosamente");
                        $("#ContratoESPuardarForm").fadeOut('slow');
                        $("#ContratoESPuardarForm input").val("");
                        CargarContratoEspecialidad(cont_id);

                    }else{
                        alert("Error guardando Especialidad; "+json.error+" , ruta:"+url);
                    }
                });
            }else{
                alert("El nombre de la Especialidad no puede ser vacio");
            } 

        }else{
            var url='rest/core/contrato/'+cont_id+'/especialidad/'+espe_id+'/upd?espe_nombre='+espe_nombre;
            $.get(url,null,function(json){
                if(json.status){
                    alert("Especialidad modificada exitosamente");
                    $("#ContratoESPuardarForm").fadeOut('slow');
                    $("#ContratoESPuardarForm input").val("");
                    CargarContratoEspecialidad(cont_id);
                }else{
                    alert("Error modificando especialidad "+json.error+" , ruta: "+url);
                }
            });

        }

    });
    
    $(document).off('click', '#Contratoespe');
    $('#Contratoespe').on('change',function(){
        //console.log($(this).val());
        var cont_id = $("#cont_id_form").val();
        var espe_id=$('#Contratoespe').val();
        var name=$("#Contratoespe :selected").text();

            $("#ContratoESPuardarForm").fadeOut('slow'); 
        
        CargarContratoFormularioEspecialidad(cont_id,$(this).val());


    });
    

    //-------------------------------------//
    
//------no permitir letras en los text box de periodicidad contrato--------//

    $(".rcpe").keydown(function(event) {
    if(event.shiftKey){
        event.preventDefault();
    }
 
   if (event.keyCode == 46 || event.keyCode == 8)    {
   
   }
   else {
        if (event.keyCode < 95) {
          if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
          }
        } 
        else {
              if (event.keyCode < 96 || event.keyCode > 105) {
                  event.preventDefault();
              }
        }
      }
   });
    //---------periodicidad-----------//

    $(document).off('click', '#ContratoPeriAgregar');
    $("#ContratoPeriAgregar").on('click',function(){
        //console.log($('#peri_contrato').val());
        if ($('#peri_contrato').val()=="") {
            alert('seleccione una periodicidad');
        }else{
            $("#formularioPeri").fadeIn('slow');
        }
        
    });

    $(document).off('click', '#CancelarContratoPeri');
    $('#CancelarContratoPeri').on('click',function(){
        $('#formularioPeri').fadeOut('slow');
    });

    $("#GuardarContratoPeri").on('click',function(){

        var rcpe_dias_apertura= $('#rcpe_dias_apertura').val();
        var peri_id=$("#peri_contrato :selected").val();
        var rcpe_dias_post_cierre=$("#rcpe_dias_post_cierre").val();
        var rcpe_participa_mpp=$("#rcpe_participa_mpp :selected").val();
        if (rcpe_dias_apertura=="" || rcpe_dias_post_cierre=="") {
            alert("Rellene los campos faltantes");
        }else{
            confirm("Una vez ingresada una periodicidad en el contrato no podrá ser eliminada,<br>Desea continuar?",function(status){
                if(status){
                    var url='rest/core/contrato/'+cont_id+'/periodicidad/add?cont_id='+cont_id+'&peri_id='+peri_id+'&rcpe_dias_apertura='+rcpe_dias_apertura+'&rcpe_dias_post_cierre='+rcpe_dias_post_cierre+'&rcpe_participa_mpp='+rcpe_participa_mpp;
            
                    $.get(url,null,function(json){
                        if (json.status) {
                            $("#formularioPeri").fadeOut('slow');
                            cargarListaPeriodicidad();
                            $('#rcpe_dias_apertura').val("");
                            $('#rcpe_dias_post_cierre').val("");
                            alert("Periodicidad guardada correctamente");

                        }else{
                            alert("Error en guardar periodicidad :" + json.error + ", ruta:" + url);
                        }
                    });
                    return false;
                }
            })
        }
    });

    function CargarContratoPeriodicidad(cont_id){
        var url='rest/core/periodicidad/contrato/'+cont_id+'/list/';
        
        $.get(url,null,function(json){
            
            if(json.status){
                var html="";
                var participa='no';
                console.log(json.data);
                $.each(json.data,function(key,periodicidad){
                    if(periodicidad['rcpe_participa_mpp']==1){
                        participa='SI';
                    }else{
                        participa='NO';
                    }
                    html+='<tr><td>'+periodicidad['peri_nombre']+'</td><td>'+periodicidad['peri_meses']+'</td><td>'+periodicidad['peri_orden']+'</td><td>'+periodicidad['rcpe_dias_apertura']+'</td><td>'+periodicidad['rcpe_dias_post_cierre']+'</td><td>'+participa+'</td></tr>';
                });
                $("#peri_lista").html(html);  

            }else{

            }
        });
    }

    function cargarListaPeriodicidad(){
        console.log("cargarListaPeriodicidad");
        var url='rest/core/periodicidad/contrato';
        console.log("FIN VAR");
        $.get(url,null,function(json){
            console.log("INICIO JSON");
            if(json.status){
                console.log("INICIO IF");
                var html='<option value=""></option>';
              //  console.log(json.data);
              
                $.each(json.data,function(key,peri){
                    console.log("EACH");
                    html += '<option value="'+peri['peri_id']+'" name="'+peri['peri_nombre']+'" >'+peri['peri_nombre']+'</option>';
                }); 
                $("#peri_contrato").html(html).selectpicker('refresh');
                console.log("FIN IF");
            }
            console.log("FIN JSON");
        });

    }

    //--------------------------------//
    //---------contrato mantenimiento--------------//

    $(document).off('click', '#mant_peri');
    $('#mant_peri').on('change',function(){
        console.log($(this).val());
        
        var name=$("#mant_peri :selected").text();
        var i=1;
        var mes=[];
        var html='<option value=""></option>'; 
        
            if($(this).val()>12){
                mes.push({mes:i});
            }  

            if($(this).val()>=1){
                var total=12;
                var dividor=$(this).val();
                total=total/dividor;
                console.log(total);
            }
            while(i<=total){
                mes.push({mes:i});
                i++;
            }

            console.log(mes);
            $.each(mes,function(key,mant_mes){
                html +='<option value="'+mant_mes['mes']+'" >'+mant_mes['mes']+'</option>';
            });
            $("#mant_mes").html(html).selectpicker('refresh');

    });


    function CargarSelectpickerMantenimiento(){
        var url='rest/core/periodicidad/contrato/';
        $.get(url,null,function(json){

            if(json.status){
                var html='<option value=""></option>';
         
                $.each(json.periodicidad,function(key,peri){
                    html += '<option value="'+peri['peri_id']+'" name="'+peri['peri_nombre']+'" >'+peri['peri_nombre']+'</option>';
                }); 
                $("#mant_peri").html(html).selectpicker('refresh');
            }
        });

        var url ='rest/core/especialidad/list?cont_id='+cont_id;
        
        $.get(url,null,function(json){

            if(json.status){
                var html='<option value=""></option>';
                $.each(json.data,function(key,espe){
                    html += '<option value="'+espe['espe_id']+'" name="'+espe['espe_nombre']+'" >'+espe['espe_nombre']+'</option>';

                });
                 
                $("#mant_espe").html(html).selectpicker('refresh');
            }
        });
        ///core/@tabla/list(/@page:[0-9]+
        var url='rest/core/formulario/list';
        $.get(url,null,function(json){

            if(json.status){
                var html='<option value=""></option>';
                $.each(json.data,function(key,form){
                    html += '<option value="'+form['form_id']+'" name="'+form['form_nombre']+'" >'+form['form_nombre']+'</option>';

                });
                 
                $("#mant_form").html(html).selectpicker('refresh');
            }
        });

        var url='rest/core/contrato/'+cont_id+'/clasificacion_programacion/list';
        $.get(url,null,function(json){

            if(json.status){
                var html='<option value=""></option>';
                $.each(json.data,function(key,clas){
                    html += '<option value="'+clas['clpr_id']+'" name="'+clas['clpr_nombre']+'" >'+clas['clpr_nombre']+'</option>';

                });
                 
                $("#mant_clasi").html(html).selectpicker('refresh');
            }
        });
		
		
		var html='<option value=""></option>';
		for(i=1;i<13;i++){
			html += '<option value="'+i+'" name="'+i+'" >'+i+'</option>';
		};

		$("#mant_mes").html(html).selectpicker('refresh');
    }
    
    var madf_id;
    function CargarmantDefFormulario(cont_id){
        
        var url='rest/core/contrato/'+cont_id+'/mant_def_form/list';

        $.get(url,null,function(json){
           
            if(json.status){
                var html='';
                /*
                    <span class="input-group-btn">
                                        <button id="CancelarContratoPeri" type="button" class="btn btn-default" ><span class="glyphicon glyphicon-trash" aria-hidden="true">Cancelar</span></button>
                            </span>
                */
                $.each(json.data,function(key,mant_def){
                    html +='<tr id="'+mant_def['madf_id']+'">'+
                                '<td>'+mant_def['peri_nombre']+'</td>'+
                                '<td>'+mant_def['clpr_nombre']+'</td>'+
                                '<td>'+mant_def['espe_nombre']+'</td>'+
                                '<td>'+mant_def['form_nombre']+'</td>'+
                                '<td>'+mant_def['peri_mes']+'</td>'+
                                '<td>'+
                                '<button type="button" value="'+mant_def['madf_id']+'" class="btn botonBorrar btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'+ 
                                '</td>'+
                            '</tr>';
                            madf_id=mant_def['madf_id'];
                });
                $("#mant_form_list").append(html);
                $(".botonBorrar").on('click',function(){
                    del_mant_def_form($(this).val());
                });
            }
        });

    }
   
    function del_mant_def_form(madf_id){
       var url='rest/core/contrato/'+cont_id+'/mantenimiento_definicion_formulario/'+madf_id+'/del';
       $.post(url,null,function(json){
            if(json.status){
                $("#"+madf_id).remove();
                alert("Definición de mantenimiento eliminada");
            }
       });
    }
   
    $(document).off('click', '#AgregarMantDefForm');
    $("#AgregarMantDefForm").on('click',function(){
        var peri_id=$('#mant_peri').val();
        var clpr_id=$('#mant_clasi').val();
        var espe_id=$('#mant_espe').val();
        var form_id=$('#mant_form').val();
        var peri_mes=$('#mant_mes').val();
        if(peri_id=="" || clpr_id=="" || espe_id=="" || form_id=="" || peri_mes==null || peri_mes==""){
            alert("Debe seleccionar todos los datos");
        }else{        
        
            url='rest/core/contrato/'+cont_id+'/mant_def_form/add?cont_id='+cont_id+'&peri_id='+peri_id+'&clpr_id='+clpr_id+'&espe_id='+espe_id+'&form_id='+form_id+'&peri_mes='+peri_mes;
            $.post(url,null,function(json){
                if(json.status){
                    alert("Definición de mantenimiento agregada");
					CargarmantDefFormulario(cont_id);
                }
				else{
					alert(json.error);
				}
           });
       }  
    });

    //------------------------------//
    $(document).off('click', '#ContratoLPUGuardar');
    $("#ContratoLPUGuardar").on('click', function() {
        //console.log("#ContratoLPUGuardar click"); 
        var cont_id = $("#cont_id_form").val();
        var lpu_id = $("#ContratoLPUGuardarForm input[type='hidden']").val();
        var lpu_nombre = $("#ContratoLPUGuardarForm input[type='text']").val();
        var lpu_estado = $("#ContratoLPUGuardarForm select").val();
        
        if( lpu_nombre.length >0 ){
            //console.log("lpu_id:"+lpu_id);
            
            var url = 'rest/core/contrato/'+cont_id+'/lpu/add?lpu_nombre=' + lpu_nombre;
            if( lpu_id > 0 ){
                url = 'rest/core/contrato/'+cont_id+'/lpu/upd/'+lpu_id+'?lpu_nombre=' + lpu_nombre+'&lpu_estado=' + lpu_estado;
            }
            $.get(url, null, function(json) {
                //console.log(url);console.log(json);
                if (json.status) {
                    CargaContratoLPU(cont_id);
                    $("#ContratoLPUGuardarForm").fadeOut('slow');
                    $("#ContratoLPUGuardarForm input").val("");
                }
                else {
                    alert("Error guardando LPU; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error guardando LPU " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        }
        else{
            alert("El nombre de la LPU no puede ser vacio");
        }        
    });
    
    $(document).off('click', '#ContratoLPUGrupo');
    $("#ContratoLPUGrupo").on('change', function() {
        //console.log("#ContratoLPUGrupo change lpgr_id:" + $(this).val());
        var cont_id = $("#cont_id_form").val();
        var lpgr_id = $(this).val();
        var lpu_id  = $("#ContratoLPU").val();
        if( lpgr_id>0 ){
            CargaContratoLPUSubgrupo(cont_id, lpu_id, lpgr_id);
        }
        $("#ContratoLPUGuardarForm").fadeOut('slow');
        $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
        $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow');
    });
    
    $(document).off('click', '#ContratoLPUGrupoEditar');
    $("#ContratoLPUGrupoEditar").on('click', function() {
        //console.log("#ContratoLPUGrupoEditar click lpgr_id:" + $("#ContratoLPUGrupo").val());
        var lpgr_id =  $("#ContratoLPUGrupo").val();
        if( lpgr_id>0 ){            
            $("#ContratoLPUGrupoGuardarForm input[type='hidden']").val(lpgr_id);   
            $("#ContratoLPUGrupoGuardarForm input[type='text']").val($("#ContratoLPUGrupo").find("option:selected").text()); 
            $("#ContratoLPUGrupoGuardarForm").fadeIn('slow');
        }
        else{
            alert("Debe seleccionar un grupo para editar");
        }
    });
           
    $(document).off('click', '#ContratoLPUGrupoAgregar');
    $("#ContratoLPUGrupoAgregar").on('click', function() {
        //console.log("#ContratoLPUGrupoAgregar click");        
        if( !($("#ContratoLPU").val()>0) ){
            alert("Debe seleccionar una LPU para agregar un grupo");
            return;
        }
        
        $("#ContratoLPUGrupoGuardarForm input[type='hidden']").val(""); 
        $("#ContratoLPUGrupoGuardarForm input[type='text']").val("");
        $("#ContratoLPUGrupoGuardarForm").fadeIn('slow'); 
    });
    
    $(document).off('click', '#ContratoLPUGrupoGuardar');
    $("#ContratoLPUGrupoGuardar").on('click', function() {
        //console.log("#ContratoLPUGrupoGuardar click");  
        var cont_id = $("#cont_id_form").val();
        var lpu_id = $("#ContratoLPU").val();
        var lpgr_id = $("#ContratoLPUGrupoGuardarForm input[type='hidden']").val();
        var lpgr_nombre = $("#ContratoLPUGrupoGuardarForm input[type='text']").val();
        
        if( lpgr_nombre.length >0 ){
            //console.log("lpgr_id:"+lpgr_id);
            
            var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/add?lpgr_nombre=' + lpgr_nombre;
            if( lpgr_id > 0 ){
                url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/upd/'+lpgr_id+'?lpgr_nombre=' + lpgr_nombre;
            }
            $.get(url, null, function(json) {
                //console.log(url);console.log(json);
                if (json.status) {
                    CargaContratoLPUGrupo(cont_id,lpu_id);
                    $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
                }
                else {
                    alert("Error guardando grupo; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error guardando grupo " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        }
        else{
            alert("El nombre del grupo no puede ser vacio");
        }        
    });
    
    $(document).off('click', '#ContratoLPUSubgrupo');
    $("#ContratoLPUSubgrupo").on('change', function() {
        //console.log("#ContratoLPUSubgrupo change lpgr_id:" + $(this).val());
        var cont_id = $("#cont_id_form").val();
        var lpgr_id = $(this).val();
        var lpu_id  = $("#ContratoLPU").val();
        if( lpgr_id>0 ){
            CargaContratoLPUSubgrupoClase(cont_id, lpu_id,lpgr_id);
            CargaContratoLPUSubgrupoItems(cont_id, lpu_id,lpgr_id);
        }
        $("#ContratoLPUGuardarForm").fadeOut('slow');
        $("#ContratoLPUGrupoGuardarForm").fadeOut('slow');
        $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow');
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoEditar');
    $("#ContratoLPUSubgrupoEditar").on('click', function() {
        //console.log("#ContratoLPUSubgrupoEditar click lpgr_id:" + $("#ContratoLPUSubgrupo").val());
        var lpgr_id =  $("#ContratoLPUSubgrupo").val();
        if( lpgr_id>0 ){            
            $("#ContratoLPUSubgrupoGuardarForm input[type='hidden']").val(lpgr_id);   
            $("#ContratoLPUSubgrupoGuardarForm input[type='text']").val($("#ContratoLPUSubgrupo").find("option:selected").text()); 
            $("#ContratoLPUSubgrupoGuardarForm").fadeIn('slow');
        }
        else{
            alert("Debe seleccionar un subgrupo para editar");
        }
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoAgregar');
    $("#ContratoLPUSubgrupoAgregar").on('click', function() {
        //console.log("#ContratoLPUSubgrupoAgregar click");        
        if( !($("#ContratoLPU").val()>0) ){
            alert("Debe seleccionar una LPU para agregar un grupo");
            return;
        }
        
        if( !($("#ContratoLPUGrupo").val()>0) ){
            alert("Debe seleccionar un grupo para agregar un subgrupo");
            return;
        }
        
        $("#ContratoLPUSubgrupoGuardarForm input[type='hidden']").val(""); 
        $("#ContratoLPUSubgrupoGuardarForm input[type='text']").val("");
        $("#ContratoLPUSubgrupoGuardarForm").fadeIn('slow'); 
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoGuardar');
    $("#ContratoLPUSubgrupoGuardar").on('click', function() {
        //console.log("#ContratoLPUSubgrupoGuardar click");   
        var cont_id = $("#cont_id_form").val();
        var lpu_id = $("#ContratoLPU").val();
        var lpgr_padre = $("#ContratoLPUGrupo").val();
        var lpgr_id = $("#ContratoLPUSubgrupoGuardarForm input[type='hidden']").val();
        var lpgr_nombre = $("#ContratoLPUSubgrupoGuardarForm input[type='text']").val();
        
        if( lpgr_nombre.length >0 ){
            //console.log("lpgr_id:"+lpgr_id);
            
            var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/add?lpgr_nombre=' + lpgr_nombre+'&lpgr_padre='+lpgr_padre;
            if( lpgr_id > 0 ){
                url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/upd/'+lpgr_id+'?lpgr_nombre=' + lpgr_nombre;
            }

            $.get(url, null, function(json) {
                //console.log(url);console.log(json);
                if (json.status) {
                    CargaContratoLPUSubgrupo(cont_id, lpu_id,lpgr_padre);
                    $("#ContratoLPUSubgrupoGuardarForm").fadeOut('slow');
                }
                else {
                    alert("Error guardando subgrupo; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error guardando subgrupo " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        }
        else{
            alert("El nombre del subgrupo no puede ser vacio");
        }        
    });
    
    $(document).off('click', '.ContratoLPUSubgrupoClaseEditar');
    $(document).on('click', '.ContratoLPUSubgrupoClaseEditar', function(e) {
        var lpgc_id = $(this).data('lpgc_id');
        //console.log("#ContratoLPUSubgrupoClaseEditar click lpgc_id:" + lpgc_id);
        
        if( $(this).hasClass('clase-editar') ){   
            //console.log("EDITAR");
            $(this).removeClass('clase-editar');
            $(this).addClass('clase-guardar');
            
            $(this).children("span").removeClass("glyphicon-pencil");
            $(this).children("span").addClass("glyphicon-floppy-disk");

            $(this).parent().parent().find(".clase-data").attr("disabled",false);
        }
         else{
            //console.log("GUARDAR");
            $(this).removeClass('clase-guardar');
            $(this).addClass('clase-editar');
            
            $(this).children("span").removeClass("glyphicon-floppy-disk");
            $(this).children("span").addClass("glyphicon-pencil");

            var data = {};
            data['lpgc_nombre'] = $(this).parent().parent().find(".clase-data").val();
            
            var cont_id = $('#cont_id_form').val();
            var lpu_id  = $("#ContratoLPU").val();
            var lpgr_id = $("#ContratoLPUSubgrupo").val();
            
            url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/clase/upd/'+lpgc_id;

            $.post(url, data, function(json) {
                if (json.status) {
                    CargaContratoLPUSubgrupoClase(cont_id, lpu_id,lpgr_id);
                    //alert("Zona guardada exitosamente");
                }
                else {
                    alert("Error guardando clase; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error guardando clase " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        } 
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoClaseGuardar');
    $("#ContratoLPUSubgrupoClaseGuardar").on('click', function() {
        //console.log("#ContratoLPUSubgrupoClaseGuardar click");   
        var cont_id = $('#cont_id_form').val();
        var lpu_id = $("#ContratoLPU").val();
        var lpgr_id = $("#ContratoLPUSubgrupo").val();
        var lpgc_nombre = $("#ContratoLPUSubgrupoClaseGuardarForm").val();

        if( lpgc_nombre.length >0 ){            
            var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/clase/add?lpgc_nombre='+lpgc_nombre;
            
            $.get(url, null, function(json) {
                //console.log(url);console.log(json);
                if (json.status) {
                    CargaContratoLPUSubgrupoClase(cont_id, lpu_id,lpgr_id);
                }
                else {
                    alert("Error guardando clase; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error guardando clase " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        }
        else{
            alert("El nombre de la clase no puede ser vacio");
        }        
    });
    
    $(document).off('click', '.ContratoLPUSubgrupoItemEditar');
    $(document).on('click', '.ContratoLPUSubgrupoItemEditar', function(e) {
        //console.log(".ContratoLPUSubgrupoItemEditar click");
        
        var cont_id = $("#cont_id_form").val();
        var lpu_id  = $("#ContratoLPU").val();
        var lpgr_id = $("#ContratoLPUSubgrupo").val();
        var lpit_id = $(this).data('lpit_id');
        
        CargaContratoLPUSubgrupoItemPrecios(cont_id, lpu_id, lpgr_id, lpit_id);
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoItemAgregar');
    $("#ContratoLPUSubgrupoItemAgregar").on('click', function() {
        console.log(".ContratoLPUSubgrupoItemAgregar click");
        
        var cont_id = $("#cont_id_form").val();
        var lpu_id  = $("#ContratoLPU").val();
        var lpgr_id = $("#ContratoLPUSubgrupo").val();
        var lpit_id = 0;
        
        CargaContratoLPUSubgrupoItemPrecios(cont_id, lpu_id, lpgr_id, lpit_id);
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoItemPrecioInfoEditar');
    $("#ContratoLPUSubgrupoItemPrecioInfoEditar").on('click', function() {
        console.log('#ContratoLPUSubgrupoItemPrecioInfoEditar click');
        //console.log($(this));
        
        if( $(this).hasClass('item-editar') ){   
            //console.log("EDITAR");
            $(this).removeClass('item-editar');
            $(this).addClass('item-guardar');
            
            $(this).children("span").removeClass("glyphicon-pencil");
            $(this).children("span").addClass("glyphicon-floppy-disk");

            $("#ContratoLPUSubgrupoItemEditarInfo").find(".item-data").each(function(index) {
                $(this).prop("disabled", false);
            });
        }
        else{
            //console.log("GUARDAR");
            $(this).removeClass('item-guardar');
            $(this).addClass('item-editar');
            
            $(this).children("span").removeClass("glyphicon-floppy-disk");
            $(this).children("span").addClass("glyphicon-pencil");

            var data = {}; 
            $("#ContratoLPUSubgrupoItemEditarInfo").find(".item-data").each(function(index) {
                $(this).prop("disabled", true);
                data[$(this).attr('id')] = $(this).val();
            });
            if( data['lpit_nombre']===undefined || data['lpit_nombre']==null || data['lpit_nombre']=="" ){
                alert("Debe ingresar un nombre");
                return;
            }
            
            var cont_id = $('#cont_id_form').val();
            var lpu_id  = $("#ContratoLPU").val();
            var lpgr_id = $("#ContratoLPUSubgrupo").val();
                        
            var url = 'rest/core/contrato/' + cont_id + '/lpu/' + lpu_id + '/grupo/' + lpgr_id + '/item/add';
            if( data['lpit_id']>0 ){
                url = 'rest/core/contrato/' + cont_id + '/lpu/' + lpu_id + '/grupo/' + lpgr_id + '/item/upd/' + data['lpit_id'];
            }

            $.post(url, data, function(json) {
                if (json.status) {
                    CargaContratoLPUSubgrupoItems(cont_id, lpu_id, lpgr_id)
                    
                    if( !(data['lpit_id']>0) ){
                        var lpit_id = json.data[0]['id'];
                        CargaContratoLPUSubgrupoItemPrecios(cont_id, lpu_id, lpgr_id, lpit_id );
                    }
                    //alert("Item guardado exitosamente");
                }
                else {
                    alert("Error guardando item; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error guardando item " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });
        }        
        
    });

    $(document).off('click', '.ContratoLPUSubgrupoItemPrecioEditar');
    $(document).on('click', '.ContratoLPUSubgrupoItemPrecioEditar', function(e) {
        console.log(".ContratoLPUSubgrupoItemPrecioEditar click");  
        var cont_id = $("#cont_id_form").val();
        var lpu_id  = $("#ContratoLPU").val();
        var lpgr_id = $("#ContratoLPUSubgrupo").val();
        var lpit_id = $(this).data('lpit_id');
        var lpip_id = $(this).data('lpip_id');
        var lpgc_nombre = $(this).data('lpgc_nombre');
        var zona_nombre = $(this).data('zona_nombre');
        var lpgc_id = $(this).data('lpgc_id');
        var zona_id = $(this).data('zona_id');
        
        if( lpip_id>0 ){
            var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/item/'+lpit_id+'/precio/list?lpip_id=' + lpip_id;
            $.get(url, null, function(json) {
                console.log(url);console.log(json);
                if (json.status) {
                    if (json.data.length > 0) {
                        var precio = json.data[0];
                        $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find(".precio-data").each(function(index) {
                            $(this).val((precio[$(this).attr('id')] == null) ? '' : precio[$(this).attr('id')]);
                        });
                    }
                }
                else {
                    alert("Error cargando precio; " + json.error + ", ruta:" + url);
                }
            }).fail(function(xhr, textStatus, errorThrown) {
                alert("Error cargando precio " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
            });  
        }
        else {
            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find(".precio-data").each(function(index) {
                $(this).val("");
            });
            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find("#lpit_id").val(lpit_id);
            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find("#zona_id").val(zona_id);
            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find("#lpgc_id").val(lpgc_id);
            $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find("#lpip_estado").val("ACTIVO");
        }
        
        $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find("#lpgc_nombre").text(lpgc_nombre);
        $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find("#zona_nombre").text(zona_nombre);
        $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").fadeIn('slow');
    });
    
    $(document).off('click', '#ContratoLPUSubgrupoItemEditarInfoPreciosGuardar');
    $("#ContratoLPUSubgrupoItemEditarInfoPreciosGuardar").on('click', function() {
        console.log("#ContratoLPUSubgrupoItemEditarInfoPreciosGuardar click");   

        var data = {};
        $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").find(".precio-data").each(function(index) {
            data[$(this).attr('id')] = $(this).val();
        });
        
        if( data['lpip_precio'] === undefined || data['lpip_precio'] === "" ){
            alert("Debe ingresar un precio");
            return;
        }

        var cont_id = $('#cont_id_form').val();
        var lpu_id = $("#ContratoLPU").val();
        var lpgr_id = $("#ContratoLPUSubgrupo").val();
        var lpit_id = data['lpit_id'];
        var lpip_id = data['lpip_id'];

        var url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/item/'+lpit_id+'/precio/add';
        
        if( lpip_id>0 ){
            url = 'rest/core/contrato/'+cont_id+'/lpu/'+lpu_id+'/grupo/'+lpgr_id+'/item/'+lpit_id+'/precio/upd/'+lpip_id;
        }
        //console.log(url); console.log(data);
        
        $.post(url, data, function(json) {
            //console.log(json);
            if (json.status) {  
                if( !(lpip_id>0) ){
                    lpip_id = json.data[0]["id"];
                }
                CargaContratoLPUSubgrupoItemPrecios(cont_id, lpu_id, lpgr_id, lpit_id);
                $("#ContratoLPUSubgrupoItemEditarInfoPreciosEditar").fadeOut('slow');
            }
            else {
                alert("Error guardando precio; " + json.error + ", ruta:" + url);
            }
        }).fail(function(xhr, textStatus, errorThrown) {
            alert("Error guardando precio " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
        });
        
    });


    function CargaContratoDocumentos(cont_id){
        currentContId = cont_id;

        var url = 'rest/contrato/'+cont_id+'/docs';
        $.get(url, null, function(json) {
            if (json.status) { 
                if(json.archivos.length==0){
                    html = '<div class="alert alert-notice" role="alert">';
                    html+= 'No se han cargado archivos al contrato seleccionado';
                    html+= '</div>';
                    $("#ContratoDocumentosLista").html(html);
                }
                else{
                    json.canEdit   = true;
                    json.canDelete = true;
                    $.ajax({
                          url: "templates/template_documentos_lista.hb",
                          async: false,
                          success: function(src){
                            template = Handlebars.compile(src);
                            html = template(json);
                            $("#ContratoDocumentosLista").html(html);
                          },
                        });
                }
            }
            else{
                alert(json.error);
            }
        });
    }

    $(document).off('click', '#BotonContratoDocumentosAgregar');
    $(document).on('click', '#BotonContratoDocumentosAgregar', function(e) {

        $('#ModalDocumentosEditar').off('show.bs.modal').on('show.bs.modal', function (e) {
            var modal = $(this);

            modal.find('h4.modal-title').text("Agregar documento");
            modal.find(':input[name="repo_id"]').val("");
            modal.find(':input[name="repo_nombre"]').val("");
            modal.find(':input[name="repo_descripcion"]').val("");
            modal.find(':input[name="repo_tipo_doc"]').val("");
            modal.find(':input[name="archivo"]').val("");
            modal.find('div.archivo').show();

            modal.find('button.btn-primary').off('click').on('click',function(e){
                var button = $(this);
                var formData = new FormData();
                formData.append("repo_tabla"      ,"contrato");
                formData.append("repo_tabla_id"   ,currentContId);
                formData.append("repo_nombre"     ,modal.find(':input[name="repo_nombre"]').val());
                formData.append("repo_nombre"     ,modal.find(':input[name="repo_nombre"]').val());
                formData.append("repo_descripcion",modal.find(':input[name="repo_descripcion"]').val());
                formData.append("repo_tipo_doc"   ,modal.find(':input[name="repo_tipo_doc"]').val());
                
                $.each(modal.find("input:file"), function(i, file) {
                    if($(file)[0].files[0]){
                        formData.append($(this).attr("name"), $(file)[0].files[0]);
                    }
                });

                button.button("loading");

                url = 'rest/core/repo/add';
                $.ajax({
                    url: url,
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(json, textStatus ){
                        button.button("reset");
                        modal.modal('hide');

                        if(json.status){
                            alert("Archivo agregado exitosamente.");
                            CargaContratoDocumentos(currentContId);
                        }
                        else{
                            alert(json.error);
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        button.button("reset");
                        modal.modal('hide');
                       alert("Error guardando archivo en repositorio");
                    }
                });


            });
        })

        $("#ModalDocumentosEditar").modal("show");

    });

    $(document).off('click', '#ContratoDocumentosLista div.siom-docu-acciones button');
    $(document).on('click', '#ContratoDocumentosLista div.siom-docu-acciones button', function(e) {
        action  = $(this).data("action");
        repo_id = $(this).data("repo_id");

        if(action=="edit"){
            data = $(this).data("data");

            $('#ModalDocumentosEditar').off('show.bs.modal').on('show.bs.modal', function (e) {
                var modal = $(this);

                modal.find('h4.modal-title').text("Editar documento");
                modal.find(':input[name="repo_id"]').val(data.repo_id);
                modal.find(':input[name="repo_nombre"]').val(data.repo_nombre);
                modal.find(':input[name="repo_descripcion"]').val(data.repo_descripcion);
                modal.find(':input[name="repo_tipo_doc"]').val(data.repo_tipo_doc);
                modal.find('div.archivo').hide();

                modal.find('button.btn-primary').off('click').on('click',function(e){
                    var button = $(this);

                    repo_id          = modal.find(':input[name="repo_id"]').val();
                    repo_nombre      = modal.find(':input[name="repo_nombre"]').val();
                    repo_descripcion = modal.find(':input[name="repo_descripcion"]').val();
                    repo_tipo_doc    = modal.find(':input[name="repo_tipo_doc"]').val();

                    data = {"repo_nombre":repo_nombre,
                            "repo_descripcion":repo_descripcion,
                            "repo_tipo_doc":repo_tipo_doc}

                    button.button("loading");
                    var url = 'rest/core/repo/upd/'+repo_id;
                    $.post(url,data, function(json) { 
                        button.button("reset");
                        modal.modal('hide');

                        if(json.status){
                            //alert("Archivo actualizado exitosamente.");
                            CargaContratoDocumentos(currentContId);
                        }
                        else{
                            alert(json.error);
                        }
                    });

                });
            })

            $("#ModalDocumentosEditar").modal("show");
        }
        else if(action=="delete"){
            repo_nombre = $(this).data("repo_nombre");
            confirm("Desea eliminar archivo <b>"+repo_nombre+"</b>?<br><span class='text-danger'>Esta acción será irreversible!.</span>",
                function(status){
                    if(status){
                        var url = 'rest/core/repo/del/'+repo_id;
                        $.get(url, null, function(json) { 
                            if(json.status){
                                //alert("Archivo eliminado exitosamente.");
                                CargaContratoDocumentos(currentContId);
                            }
                            else{
                                alert(json.error);
                            }
                        });
                    }
                });
        }
    });

    
    //SUBMITS DE FORMS__________________________________________________________

    //GUARDAR contrato
    $('#FormContratoEditar').submit(function(e) {
        //Comprobacion de valores de variables        
        var valor;

        valor = $(this).find(':input[name="cont_nombre"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un nombre");
            return false;
        }
        valor = $(this).find(':input[name="cont_alias"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un alias");
            return false;
        }
        valor = $(this).find(':input[name="cont_descripcion"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar una descripcion");
            return false;
        }

        return true;



    });


    //FUNCIONES AUXILIARES______________________________________________________

    function FormatoFecha(fecha) {
        var fechaSalida = fecha.slice(0, 10);
        fechaSalida = fechaSalida.split("-");
		if(fechaSalida[0]>31){
			fechaSalida = fechaSalida[2] + "-" + fechaSalida[1] + "-" + fechaSalida[0];
        }else{
			fechaSalida = fechaSalida[0] + "-" + fechaSalida[1] + "-" + fechaSalida[2];
		}
		return fechaSalida;
    }
    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormContratoLista").submit();
        }, filterTimeout);
    }

})(jQuery);

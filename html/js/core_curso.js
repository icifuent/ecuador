(function($) {

	var filterTimer=null;
	var filterTimeout=500;

    $('.selectpicker').selectpicker();
    $('#WizardEmplazamiento').wizard('destroy');
    $('#WizardUsuario').wizard('destroy');
    $('#WizardEmpresa').wizard('destroy');
    $('#WizardContratos').wizard('destroy');
    $('#WizardCurso').wizard();
   
    $('#FormCursoLista input[name=curs_nombre]').keyup(function() {
        FiltrarConDelay();
    })
    $('#FormCursoLista select[name=curs_estado]').change(function() {
        $("#FormCursoLista").submit();
    })
    

    $(document).on('click', '#WizardCurso li#Primero', function(e) {
        $("#AgregarCurso").show();
        $("#FormCursoLista").submit();
    });

  

  

    //BOTONES___________________________________________________________________

    $(document).on('click', 'a#BotonCursoAgregar', function(e) {
        $("#AgregarCurso").hide();
        $("#FormCursoEditar").trigger('reset');
        $('#FormCursoEditar.selectpicker').selectpicker('refresh');
        
        $("input#curs_id").val("");
        $('input#usua_creador').val(window.user_id); 
             
        $('#WizardCurso').wizard('selectedItem', {step:2});
    });

    $(document).on('click', '#BotonCursoEditar', function(e) {
        //limpiarformulario("#FormCursoEditar");
        $("#AgregarCurso").hide();
       
        curs_id = $(this).data("curs_id");

        var data = $(this).data("data");

        $.each(data, function(key, value) {
            obj = $('#FormCursoEditar').find('#' + key);
            obj.val(value);
            if( obj.attr('type')==="checkbox" ){
               obj.prop( "checked", (value>0)?true:false );
            }
        });

      

        $('input#curs_id').val(curs_id);
        $('#WizardCurso').wizard('selectedItem', {step:2});
    });

   

     $(document).on('click','button#BotonCursoCambiarEstado',function(e){
        curs_id     = $(this).data("id");
        curs_nombre = $(this).data("nombre");
        curs_estado = $(this).data("estado");
           
        if (curs_estado=="ACTIVO"){
            nuevo_estado        = 'NOACTIVO';
            nombre_nuevo_estado = 'DESACTIVADO';
      	}
        else{
            nuevo_estado        = 'ACTIVO';
            nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("El Curso <b>"+curs_nombre+"</b> será <b>"+nombre_nuevo_estado+"</b>,<br>Desea continuar?",function(status){
              if(status==true){
                    $.get('rest/core/curso/upd/'+curs_id,{curs_estado:nuevo_estado},function(json) {
                        $("#FormCursoLista").submit();
                    
                    }).fail(function(xhr, textStatus, errorThrown){
                        alert("Error "+xhr.status+": "+xhr.statusText);
                    });
              }
        });

    });
    
    

    //SUBMITS DE FORMS__________________________________________________________
    
    //GUARDAR CURSO
    $('#FormCursoEditar').submit(function (e) {
        //Comprobacion de valores de variables        
        var valor;

        valor = $(this).find(':input[name="curs_nombre"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un nombre");  return false;
        }
        valor = $(this).find(':input[name="curs_alias"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar un alias");  return false;
        }
        valor = $(this).find(':input[name="curs_descripcion"]').val();
        if (valor === null || valor.length === 0) {
            alert("Debe ingresar una descripcion");  return false;
        }
        
       return true;

        
        
    });
    
   
    //FUNCIONES AUXILIARES______________________________________________________
    
    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormCursoLista").submit();
        }, filterTimeout);
    }
    
})(jQuery);

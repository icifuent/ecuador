(function($) {
    var filterTimer=null;
    var filterTimeout=500;

    $('#WizardContratos').wizard('destroy');
    $('#WizardUsuario').wizard('destroy'); 
    $('#WizardCurso').wizard('destroy');
    $('#WizardEmplazamiento').wizard('destroy');
    $('#WizardEmpresa').wizard();
    
    $('#FormEmpresaLista input[name=empr_nombre]').keyup(function () { 
         FiltrarConDelay();
    })
    $('#FormEmpresaLista select[name=empr_estado]').change(function() {
        $("#FormEmpresaLista").submit();
    })
   
    $(document).on('click', '#WizardEmpresa li#Primero', function (e) {
        $("#AgregarEmpresa").show();
        $('div#siom-empl-detalle-mapa').remove();
        $("#FormEmpresaLista").submit();
        return true;
    });

    $(document).on('click', '#WizardEmpresa li#Segundo', function (e) {
        return true;
    });

    /*Botones*/
    
    /*Emplazamiento Agregar*/
    $(document).on('click', 'a#BotonEmpresaAgregar', function (e) {

        $("#AgregarEmpresa").hide();
        $("#FormEmpresaEditar").trigger('reset');
        $('.spEmprEditar').selectpicker('refresh');
        $("input#empr_id").val("");
        $('input#usua_creador').val(window.user_id);
        $("#BotonEmpresaContrato").addClass("disabled");
        $('#WizardEmpresa').wizard('selectedItem', {step:2});
        return true;
    });

    /*EmpresaEditar*/
    $(document).on('click', 'button#BotonEmpresaEditar', function (e) {
        $("#AgregarEmpresa").hide();
        $("#BotonEmpresaContrato").removeClass("disabled");

        var empr_id = $(this).data("id");
        var empr_data = $(this).data("data");
        
        $.each(empr_data, function (key, value) {
            
            obj = $('#FormEmpresaEditar').find('#' + key);
            obj.val(value);
            if ("empr_id" === key) {
                value = value.split(",");
            }
         
        });
       
        $('input#empr_id').val(empr_data["empr_id"]);
        $('input#usua_creador').val(empr_data["usua_creador"]);
        $('.spEmprEditar').selectpicker('refresh');
        $('#WizardEmpresa').wizard('selectedItem', {step:2});
        return true;
    });
    
    /*Emplazamiento Contratos y Zonas*/
    $(document).on('click', 'button#BotonEmpresaContrato', function (e) {
        CargaDatosContratos();
        $('#WizardEmpresa').wizard('selectedItem', {step:3});
        return true;
    });  

    /*Emplazamiento cambiar estado*/
    $(document).on('click', 'button#BotonEmpresaCambiarEstado', function(e) {
        var empr_id = $(this).data("id");

        var empr_nombre = $(this).data("nombre");
        var empr_estado = $(this).data("estado");

        if (empr_estado == "ACTIVO") {
            var nuevo_estado = 'NOACTIVO';
            var nombre_nuevo_estado = 'DESACTIVADO';
        }
        else {
            var nuevo_estado = 'ACTIVO';
            var nombre_nuevo_estado = 'ACTIVADO';
        }

        confirm("La empresa <b>" + empr_nombre + "</b> sera <b>" + nombre_nuevo_estado + "</b>,<br>Desea continuar?", function(status) {
            if (status == true) {
                $.get('rest/core/empresa/upd/' + empr_id, {empr_estado: nuevo_estado}, function(json) {
                    $("#FormEmpresaLista").submit();
                    return true;
                }).fail(function(xhr, textStatus, errorThrown) {
                    alert("Error " + xhr.status + ": " + xhr.statusText);
                    return false;
                });
            }
        });
        return false;

    });

    /*Contratos y zonas*/
    function CargaDatosContratos() {         
        empr_id = $('#empr_id').val();
        $.get('rest/core/contrato/list', null, function (json) {
             if (json.status) {
                $("#ListaContratos").html("");
                $('#ListaContratosEmpresa').html("");
                
                $.each(json.data, function (i, contrato) {  
                    html = '<li class="list-group-item"  id="' + contrato["cont_id"] + '">' +
                            '<span>' + contrato["cont_nombre"] + '</span>' +
                            '<i class="glyphicon glyphicon-chevron-right pull-right" data-id="' + contrato["cont_id"] + '" data-nombre="' + contrato["cont_nombre"] + '"></i>' +
                            '</li>';
                    $("#ListaContratos").append(html);                    
                          
                });                
                CargaListaContratosEmpresa(empr_id);                
            }
            return true;
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener la lista de contratos");
            return false;
        });
        return false;
    }

    /*Carga la lista de contratos por empresa */
    function CargaListaContratosEmpresa(empr_id) { 
        $.get('rest/core/empresa/' + empr_id + '/contrato/lista', null, function (json) {
            if (json.status) {
                $("ul#ListaContratosEmpresa").html("");   

                $.each(json.data, function (i, contrato) {
            
                    html = '<li class="list-group-item"   id="' + contrato["cont_id"] + '">' +
                            '<label>' + contrato["cont_nombre"] + '</label>' +
                            '<i class="glyphicon glyphicon-trash pull-right eliminar"   data-id="' + contrato["cont_id"] + '"></i>' +
                            '<button class="glyphicon pull-right glyphicon-floppy-disk guardar" data-id="' + contrato["cont_id"] + '"></button>' +
                            '<select id="coem_tipo" multiple class="selectpicker pull-right" name="coem_tipo[]" title='+contrato["coem_tipo"]+'>'+
                                '<option value="CLIENTE">CLIENTE</option>'+
                                '<option value="CONTRATISTA">CONTRATISTA</option>'+
                                '<option value="SUBCONTRATO">SUBCONTRATO</option>'+
                                '<option value="TERCEROS">TERCEROS</option>'+
                            '</select>'+
                            '</li>';

                    $("#ListaContratos").children("#"+contrato["cont_id"]).addClass("disabled");
                    $("#ListaContratosEmpresa").append(html);
                  
                    $("#ListaContratosEmpresa li#" + contrato["cont_id"] + "  select").val(contrato['coem_tipo']);
                    $('.selectpicker').selectpicker();
                });
            }
            return true;
        }).fail(function() {
            alert("Contratos y Zonas: No se pudo obtener los contratos del usuario");
            return false;
        });
        return false;
    }

    /*Carga la lista de contratos por empresa */
    $(document).on('click', '#ListaContratos i', function (e) {
        if ($(this).parent().hasClass("disabled")) {
            return;
        }
        var empr_id = $('#empr_id').val();
        var cont_id = $(this).data("id");
        var cont_nombre = $(this).data("nombre");
        html =  '<li class="list-group-item" id="' + cont_id + '">' +
                '<label>' + cont_nombre + '</label>' +
                '<i class="glyphicon glyphicon-trash pull-right eliminar" data-id="' + cont_id + '"></i>' +
                 '<button  data-width="100%" class="glyphicon pull-right glyphicon-floppy-disk guardar"  data-id="' + cont_id+ '"></button>' +
                '<select id="coem_tipo" multiple class="selectpicker pull-right" name="coem_tipo[]"  title="Seleccione un tipo...">'+
                    '<option value="CLIENTE">CLIENTE</option>'+
                    '<option value="CONTRATISTA">CONTRATISTA</option>'+
                    '<option value="SUBCONTRATO">SUBCONTRATO</option>'+
                    '<option value="TERCEROS">TERCEROS</option>'+
                    '</select>'+
                '</li>';

        $("#ListaContratos").children("#"+cont_id).addClass("disabled");
        $("#ListaContratosEmpresa").append(html);
        $('.selectpicker').selectpicker();
        ModificaContrato(cont_id, empr_id,"CLIENTE","add");
        return true;
    });   
    
    /*Cambia a estado inactivo */
    $(document).on('click','.eliminar',function(e){

        var empr_id = $('#empr_id').val();
        var cont_id = $(this).data("id");
        var coem_tipo = $('#coem_tipo').val();
       
        $(this).parents("li").find("#coem_tipo").each(function(){
            coem_tipo= $(this).val();
            console.log("el coem_tipo es "+coem_tipo );
        });
        confirm("El contrato sera removido  <b></b><br>Desea continuar?",function(status){
            if (status == true) {
                alert("El contrato fue removido exitosamente");
                $("#ListaContratosEmpresa #"+cont_id).remove();
                $("#ListaContratos #"+cont_id).removeClass("disabled");
                var url = 'rest/core/empresa/' +empr_id+ '/contrato/' +cont_id+ '/eliminar';        
            
                $.post(url, null, function(json) {
                    if (json.status) {
                       return true;
                    }else{
                        alert("La accion no se ejecuto correctamente");
                        console.log("No se pudo guardar el contrato: " + json.error);
                        return false;
                    }
                }).fail(function () {
                    alert("La accion no se ejecuto correctamente");
                    console.log("Error guardando contrato: Error , ruta:" + url);
                    return false;
                });
            }
        });
   
    });
    
   /*Guarda los  tipos de contratos existentes */    
    $(document).on('click','.guardar',function(e){
        var empr_id = $('#empr_id').val();
        var cont_id = $(this).data("id");
        var coem_tipo = $('#coem_tipo').val();
       
        $(this).parents("li").find("#coem_tipo").each(function(){
            coem_tipo= $(this).val();
            console.log("el coem_tipo es "+coem_tipo );
        });

        confirm("El contrato sera modificado  <b></b><br>Desea continuar?",function(status){
            if (status == true) {
                alert("El contrato fue modificado exitosamente");
                var url = 'rest/core/empresa/' +empr_id+ '/contrato/' +cont_id+ '/editar/'+coem_tipo+'/tipo';        
         
                $.post(url, null, function(json) {
                    if (json.status) {
                       return true;
                    }else{
                        alert("La accion no se ejecuto correctamente");
                        console.log("No se pudo guardar el contrato: " + json.error);
                        return false;
                    }
                    
                }).fail(function () {
                    alert("La accion no se ejecuto correctamente");
                    console.log("Error guardando contrato: Error , ruta:" + url);
                    return false;
                });
            }
        });              
    });

    /*Modifica el contrato */
    function ModificaContrato(cont_id, empr_id, coem_tipo, operacion){ 
       
        var url = 'rest/core/contrato/' + cont_id + '/empresa/'+ operacion + '/' + empr_id + '?coem_tipo=' + coem_tipo;        
        
        $.get(url, null, function(json) {
            if (json.status) {
                return true;
            }else{
                alert("No se pudo guardar el contrato: " + json.error);
                return false;
            }
           
        }).fail(function () {
            alert("Error guardando contrato: Error en GET, ruta:" + url);
            return false;
        });
    }

    /*SUBMITS DE FORMS__________________________________________________________
    //GUARDAR CONTRATOS Y ZONAS
    $('#FormEmplazamientoContratosZonas').submit(function (e) {
        
        //Obtenemos los datos
        var data = {};        
        data["zona_id"] = [];
        data["cont_id"] = [];
        
        $.each($('#FormEmplazamientoContratosZonas').serializeArray(), function(i, field) {
            if( field.name == "zona_id" ){
                if( !data["zona_id"] ) data["zona_id"] = [];
                data["zona_id"].push(field.value);
            }
            else
                data[field.name] = field.value;
        });        
        
        $("#ListaContratosEmpresa input").each(function(i, checkbox) {
            var contrato = {};
            //console.log(checkbox);
            contrato["cont_id"] = $(checkbox).data("cont_id");
            contrato["rece_mpp"] = ($(checkbox).is(':checked'))?"1":"0";  
            data["cont_id"].push(contrato);
        });
        
        if( data["zona_id"].length === 0 ){
            data["zona_id"] = -1;            
        }
        
        if( data["cont_id"].length === 0 ){
            data["cont_id"] = -1;    
            data["zona_id"] = -1;            
        }
        
        var empr_id = $('#empr_id').val();
        
        //Hacemos el POST
        url = "rest/core/emplazamiento/upd/"+empr_id;
        
        //console.log("Guardando Contratos y Zonas");
        //console.log(url);
        //console.log(data);
                
        $.post(url, data, function (json) {             
            if (json.status == 0) {
                alert("Error guardando emplazamiento:" + json.error + " , ruta:" + url);
                return false;
            }
            alert("Emplazamiento guardado exitosamente");

        }).fail(function () {
            alert("Error guardando emplazamiento: Error en POST, ruta:" + url);
        });
        
        return false;
    });
    */

    /*GUARDAR EMPLAZAMIENTO*/
    $('#FormEmpresaEditar').submit(function (e) {
      
        var valor;
        var valor = $(this).find(':input[name="empr_direccion"]').val();
        if (null == valor) {
            alert("Debe ingresar una direccion");
            return false;
        }
        
        var valor = $(this).find(':input[name="empr_nombre"]').val();
        if ("" == valor) {
            alert("Debe ingresar un nombre");
            return false;
        }
        
        var valor = $(this).find(':input[name="empr_alias"]').val();
        if ("" == valor) {
            alert("Debe ingresar un alias");
            return false;
        }
        
        var valor = $(this).find(':input[name="empr_rut"]').val();
        if ("" == valor) {
            alert("Debe ingresar un rut");
            return false;
        }
        var valor = $(this).find(':input[name="empr_giro"]').val();
        if ("" == valor) {
            alert("Debe ingresar un giro");
            return false;
        }
        var valor = $(this).find(':input[name="empr_observacion"]').val();
        if ("" == valor) {
            alert("Debe ingresar una observacion");
            return false;
        }
        var data = {};
        $.each($('#FormEmpresaEditar').serializeArray(), function(i, field) {
            
            data[field.name] = field.value;

        });
        var url = 'rest/core/empresa/add';
        if (data.empr_id > 0) {
            url = 'rest/core/empresa/upd/' + data.empr_id;
        }else {
            delete data["empr_id"];
        }
        
        console.log("Guardando Empresa");
        console.log(url);
        console.log(data);

        $.post(url, data, function (json) {            
            if (0 == json.status) {
                alert("Error guardando empresa:" + json.error + " , ruta:" + url);
                return false;
            }             
            if ( 0 < json.data[0].id) {
                $('#empr_id').val(json.data[0].id);
            }            
            
            $("#BotonEmpresaContrato").removeClass("disabled");
            
            alert("Empresa guardada exitosamente");

            return true;
        }).fail(function () {
            alert("Error guardando empresa: Error en POST, ruta:" + url);
            return false;
        });
        
        return false;        
    });
    
    /*Funciones auxiliares*/
    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#FormEmpresaLista").submit();
        }, filterTimeout);
    }

    $('.selectpicker').selectpicker();

})(jQuery);

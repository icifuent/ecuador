(function($) {
    var listaLPU = null;
    var listaLPULength = 0;
    var listaLPUGroups = [];
    var filtroLPUTimer = null;
    var nroCorrelativo=1;

    $('.selectpicker').selectpicker();

    window.resizeElements();
    window.scrollToElement($('#form_presupuesto'));

    //agregar item
    $('div.siom-os-presupuesto-lista button').click(function(e){
    	data = $(this).data();
        $.ajax({
            url: "templates/os_presupuesto_item.hb",
            async: true,
            success: function(src){
                template = Handlebars.compile(src);
                $("#lista_presupuesto").append(template(data));

                UpdateCorrelativo();
                UpdateSubtotal(data.lpipId+"-"+(nroCorrelativo-1));
                UpdateTotal();    
          },
        });
    });

    //eliminar item
    $(document).on('click','#lista_presupuesto table.siom-os-presupuesto-item i.eliminar',function(e){
        id = $(this).data("id");

        $("#"+id).remove();
        UpdateCorrelativo();
        UpdateTotal();
    })


    $(document).on('keyup','#lista_presupuesto table.siom-os-presupuesto-item .cantidad input',function(e){
    	id = $(this).data("id");
    	UpdateSubtotal(id);
    	UpdateTotal();
    })


    $("#grupo-lpu").change(function(e){
        ActualizarSubGrupoLPU();
    })

    $("#subgrupo-lpu").change(function(e){
        ActualizarListaLPU();
    })

    $('#filtro-lpu').keyup(function () {
        if(filtroLPUTimer){
            clearTimeout(filtroLPUTimer);
        }
        filtroLPUTimer = setTimeout(function(){
            ActualizarListaLPU();
        },800);
    });

    $('#form_presupuesto').submit(function() {
        total = $("#total_presupuesto").data("total");
        if(total==0){
            alert("Debe agregar al menos un item para guardar presupuesto.");
            return false;
        }
        $("#submit_presupuesto").button('loading');
        return true;
    });


    //functions utiles
    function ActualizarSubGrupoLPU(){
        grupo = $("#grupo-lpu").val();
        $("#subgrupo-lpu option").attr("disabled",true);
   	    $("#subgrupo-lpu option[data-grupo='"+grupo+"']").attr("disabled",false);
        $("#subgrupo-lpu option[data-default='true']").attr("disabled",false);
        $("#subgrupo-lpu").selectpicker('refresh');

        ActualizarListaLPU();
    }

   function ActualizarListaLPU(){
        grupo    = $("#grupo-lpu").val();
        subgrupo = $("#subgrupo-lpu").val();
        filtro   = $('#filtro-lpu').val();

        init = new Date().getTime();
        console.log("filtrando "+grupo+" > "+subgrupo+ "("+filtro+")");
        
        if(listaLPU==null){
            listaLPU = $("#lista-lpu div.siom-os-presupuesto").toArray(); 
            listaLPULength = listaLPU.length; 

            for(i=0;i<listaLPULength;i++){
                row   = $(listaLPU[i]);
                listaLPUGroups[i] = [row.data("grupo"),row.data("subgrupo"),row.data("id"),row.data("nombre")];
            }
        }
        
        for(i=0;i<listaLPULength;i++){
            var grupoItem    = listaLPUGroups[i][0];
            var subgrupoItem = listaLPUGroups[i][1];
            var id           = listaLPUGroups[i][2];
            var el           = document.getElementById("item_"+id);
            var display = "none";

            //filtro de grupo/subgrupo
            if(grupo==null || grupo==""){
                 display = "block";
            }
            else{
                if( grupo == grupoItem){
                    if(subgrupo==null || subgrupo==""){
                         display = "block";
                    }
                    else{
                        if(subgrupo == subgrupoItem){
                            display = "block";
                        }
                    }
                 }
            }

            //filtro de texto
            if(display=="block" && filtro!=""){
                var rex     = new RegExp(filtro,'i');
                var nombre  = listaLPUGroups[i][3];
                if(!rex.test(nombre)){
                    display = "none";
                }
             }

             el.style.display=display;
        }
        console.log("filtrado en "+((new Date().getTime())-init)+" ms"); 
    }

    function UpdateCorrelativo(){
        nroCorrelativo = 1;
        $("#lista_presupuesto table.siom-os-presupuesto-item").each(function(){
            var table = $(this);
            var lpipId = table.data("lpip-id");
            
            table.attr("id",lpipId+"-"+nroCorrelativo);
            table.find("td.item").html(nroCorrelativo);
            table.find("td.precio").attr("id","precio-"+lpipId+"-"+nroCorrelativo);
            table.find("td.cantidad input[type=text]").attr("id","cantidad-"+lpipId+"-"+nroCorrelativo).attr("data-id",lpipId+"-"+nroCorrelativo);
            table.find("td.subtotal").attr("id","subtotal-"+lpipId+"-"+nroCorrelativo);
            table.find("td.eliminar i").attr("data-id",lpipId+"-"+nroCorrelativo);

            ++nroCorrelativo;
        });
        $("#total_items").text((nroCorrelativo-1)+" items ingresados");
    }

    function UpdateSubtotal(id){
		cantidad = $("#cantidad-"+id).val();
    	precio   = $("#precio-"+id).data("precio");
    	subtotal = +(cantidad*precio).toFixed(2);
    	$("#subtotal-"+id).data("subtotal",subtotal);
    	$("#subtotal-"+id).text(subtotal+" UF");
    }

    function UpdateTotal(){
    	total = 0;
    	$.each($("#lista_presupuesto table.siom-os-presupuesto-item"),function(index,row){
			id = $(row).attr("id");
			total += $("#subtotal-"+id).data("subtotal");
		});
		total = +total.toFixed(2);
		$("#total_presupuesto").data("total",total);
		$("#total_presupuesto").text("TOTAL "+total+" UF");
    }

})(jQuery);

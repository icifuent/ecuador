(function($) {

$('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/mnt/bandeja/filtro/'+page,data);
});

$('#ModalSolicitud').on('show.bs.modal', function (event) {
 	var modal = $(this)
  	var button = $(event.relatedTarget)
  	var submit = modal.find('button.btn-primary');
  	var mant_id = button.data('mant-id')
  	var tipo    = button.data('tipo')
  	var title   = button.data('title')

  	modal.find('.modal-title').text(title)
  	modal.find("textarea").val("");

  	submit.data("mant-id",mant_id)
  	submit.data("tipo",tipo)
  	submit.data("button",button)
  	submit.button("reset");
  	submit.removeClass("btn-success");
})

$('#ModalSolicitud button.btn-primary').click(function(e){
	e.stopImmediatePropagation();

	var modal  = $('#ModalSolicitud');
	var submit = $(this);
	var button = $(submit.data("button"));
	var mant_id = submit.data('mant-id')
  	var tipo    = submit.data('tipo')
	var razon  = modal.find("textarea").val();

	if(razon==""){
		alert("Debe indicar una razón para la solicitud");
		return;
	}

	submit.button("loading");

	url = 'rest/contrato/'+window.contract+'/mnt/solicitud/'+tipo+'/'+mant_id
	$.post(url,{razon:razon},function(json,textStatus) {
		submit.button("reset");
		if(json.status){
			submit.button("complete");
			submit.addClass("btn-success");
			setTimeout(function(){
				if(tipo=="informe"){
					button.text("Solicitando ingreso de informe");
				}
				else if(tipo=="cambio"){
					button.text("Solicitando cambio");
				}
				button.prop('disabled',true);
				button.addClass("solicitando");
				button.parent().addClass("disabled");
				modal.modal('hide')
			},1000);
		}
		else{
			alert(json.error)
		}
	},'json').fail(function(){
		submit.button("reset");
		alert("No se pudo enviar solicitud");
	});

});


$('#ModalSolicitudCambioFecha').on('show.bs.modal', function (event) {
	console.log('show.bs.modal',event)

 	var modal = $(this)
  	var button = $(event.relatedTarget);
  	var fecha  = modal.find('#fecha_programada');
  	var submit = modal.find('button.btn-primary');
  	var mant_id = button.data('mant-id')

  	fecha.datepicker({
	    format: "dd-mm-yyyy",
	    viewMode: "days",
	    minViewMode: "days",
	    language: "es"
	}).on('show.bs.modal', function(event) {
		event.stopPropagation();
	})
	fecha.val("");

  	modal.find("textarea").val("");
  	submit.data("mant-id",mant_id)
  	submit.data("button",button)
  	submit.button("reset");
  	submit.removeClass("btn-success");

  	console.log("guardado",submit.data("button"));
})


$('#ModalSolicitudCambioFecha button.btn-primary').click(function(e){
	console.log('click',e)

	e.stopImmediatePropagation();

	var modal  = $('#ModalSolicitudCambioFecha');
	var submit = $(this);
	var button = $(submit.data("button"));
	var mant_id = submit.data('mant-id')
	var fecha   = modal.find('#fecha_programada').val();
  	var razon   = modal.find("textarea").val();

  	if(fecha==""){
		alert("Debe indicar una nueva fecha de programada");
		return;
	}
	fecha = fecha.split("-");
	fecha = fecha[2]+"-"+fecha[1]+"-"+fecha[0];

	if(razon==""){
		alert("Debe indicar una razón para la solicitud");
		return;
	}

	console.log("obtenido",button);


	submit.button("loading");

	url = 'rest/contrato/'+window.contract+'/mnt/solicitud/cambio_fecha_programada/'+mant_id
	$.post(url,{fecha:fecha,razon:razon},function(json,textStatus) {
		submit.button("reset");
		if(json.status){
			submit.button("complete");
			submit.addClass("btn-success");
			setTimeout(function(){
				button.text("Solicitando cambio de fecha");
				button.prop('disabled',true);
				button.addClass("solicitando");
				button.parent().addClass("disabled");
				modal.modal('hide')
			},500);
		}
		else{
			alert(json.error)
		}
	},'json').fail(function(){
		submit.button("reset");
		alert("No se pudo enviar solicitud");
	});

});


$("button#download").on("click",function(e){
	 console.log(e);
      $btn = $(this);
      $btn.button("loading");
      $.fileDownload("rest/core/repo/contrato/"+window.contract+"/mnt/bandeja",{httpMethod:"POST",data:$.param($btn.data("filters")),
          prepareCallback:function(url) {
              $btn.button("processing");
          },
          successCallback: function(url) {
              $btn.button('reset')
          },
          failCallback: function(responseHtml, url) {
              $btn.button('reset')
              alert(responseHtml);
          }
      });
});

$("#confirm_anular").on("click",function(e){
	var comment=$.trim($("#coment_anulacion").val());
	if(comment==""){
		alert("Debe ingresar una observación");
	}else{
		
		url = 'rest/contrato/'+window.contract+'/mnt/del/'+mant_id;
			$.post(url,{mant_observacion:comment},function(json,textStatus) {
				if(json.status){
					setTimeout(function(){
						window.app.runRoute('post','#/mnt/bandeja/filtro/1',window.mntBandejaFiltros);
					},200);
					alert("MNT anulada exitosamente");
				}	
				else{
					alert(json.error)
				}
			},'json').fail(function(){ 
				alert("No se pudo enviar anulacion");
			});
			
	}
})
    
$("a.siom-anular-mnt").on("click",function(e){
	e.stopImmediatePropagation();
	mant_id = $(this).data("mant-id");
	confirm("¿Desea anular MNT N° "+mant_id+"?,<br>Esto cancelara las tareas asociadas.",function(status){
		if(status){
			  $("#confirmModalAnular").modal();
		}

	})

	
});



})(jQuery);

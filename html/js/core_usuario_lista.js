(function($) {
  $('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/core/usuario/filtro/'+page,data);
  });

})(jQuery);

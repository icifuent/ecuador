(function($) {
	var filterTimer=null;
	var filterTimeout=500;

	//inicialización
    $('select.selectpicker').selectpicker();

	$('#siom-form-items-filtro input[name=inel_codigo]').keyup(function () {
		console.log("sdasdasd");
         FiltrarConDelay();
    })

	$('#siom-form-items-filtro input[name=inel_nombre]').keyup(function () {
         FiltrarConDelay();
    })

	$('#siom-form-items-filtro input[name=inel_descripcion]').keyup(function () {
         FiltrarConDelay();
    })
	$('#siom-form-items-filtro input[name=inel_ubicacion]').keyup(function () {
         FiltrarConDelay();
    })
    	
    $('#siom-form-items-filtro select[name=inel_estado]').change(function(){
		$("#siom-form-items-filtro").submit();
	})

	//funciones utiles
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-items-filtro").submit();
         },filterTimeout);
	}

	function InitSelectPickers(){
		sp = $('select.selectpicker');
		for(i=0;i<sp.length;i++){
			s = $(sp[i]);

			name = s.attr('name');
			val  = s.data('value');

			s.selectpicker('val',val);
			s.selectpicker('refresh');
		}
	}
	
	InitSelectPickers();

})(jQuery);

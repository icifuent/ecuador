(function($) { 
    var filterTimer=null;
    var filterTimeout=500;
    var filesCount = 0;
  
    $('.selectpicker').selectpicker();
  
    $('#fecha_inicio,#fecha_fin').datepicker({
        format: "yyyy-mm-dd",
        viewMode: "days",
        minViewMode: "days",
        language: "es"
    })
    


   $('div.btn-group .btn-primary').on('click', function(e){
        tipo = $(this).data("tipo");
        $("#tipo_resumen").val(tipo);
        $("#siom-form-resumen-inspeccion-filtro").submit();

        disabled = (tipo=="detalle")?true:false;

        $("#siom-form-resumen-inspeccion-filtro input[name=agrupador]").attr('disabled',disabled);
        $("#siom-form-resumen-inspeccion-filtro input[name=agrupador]").selectpicker('refresh');

    });

   
    $('#fecha_inicio,#fecha_fin').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro input[name=agrupador]').click(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=periodo]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=zona_contrato]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=zona_movistar]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=regi_id]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=zona_cluster]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=comu_id]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=usua_id]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })
    $('#siom-form-resumen-inspeccion-filtro select[name=group_by]').change(function(){
        $("#siom-form-resumen-inspeccion-filtro").submit();
    })

    //core/repo/contrato/1/insp/resumen/resumen
    $("button#download").on("click",function(e){
        e.stopImmediatePropagation();
        $btn = $(this);
        data = $("#siom-form-resumen-inspeccion-filtro").serialize();
        tipo = $("#tipo_resumen").val();

        $btn.button("loading"); 
        $.fileDownload("rest/core/repo/contrato/"+window.contract+"/insp/resumen/"+tipo,{"httpMethod":"POST","data":data,
              prepareCallback:function(url) {
                  $btn.button("processing");
              },
              successCallback: function(url) {
                  $btn.button('reset')
              },
              failCallback: function(responseHtml, url) {
                  $btn.button('reset')
                  alert(responseHtml);
              }
        });
    });


    
})(jQuery);
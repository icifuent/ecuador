(function($) { 
	var filterTimer=null;
	var filterTimeout=500;
	var filesCount = 0;

	//inicializar componentes
    $('.selectpicker').selectpicker();
    $('#wizard').wizard();
    $('#fecha_solicitud').datepicker({
			    format: "dd-mm-yyyy",
			    viewMode: "days", 
			    minViewMode: "days",
			    language: "es"
			});
     

     
    //inicializar eventos
    $(document).on('click','button.os-crear-selector',function(e){
    	empl_id = $(this).data("empl-id");
    	data    = $(this).data("data");
    	
    	$("#empl_id").val(empl_id);
    	$('#wizard').wizard("next");

    	$.ajax({
          url: "templates/empl_descripcion.hb",
          async: true,
          success: function(src){
            template = Handlebars.compile(src);
            $("#siom-empl-info").html(template(data));
          },
        });


    })
   
    $("#tipo_inspecion").change(function(){
        if($(this).val()=="MPP"){
            $("#mant_id").attr("disabled",false);
        }
        else{
            $("#mant_id").attr("disabled",true);
        }
    })


	$("#form_crear").on('change','.btn-file :file', function() {
	    var input = $(this),
	   	id     = input.data("id");
	   	label  = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	   	nextId = parseInt(id)+1;

	   	var html = '<li class="list-group-item">';
	   	html+='<a class="btn btn-default btn-xs" data-id="'+id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	   	html+='<div>';
	   	html+='<div><textarea class="form-control" name="archivos_descripciones" rows="1"></textarea></div>';
	   	html+='<small>'+label+'</small>';
	   	html+='</div>';
	   	html+='</li>';
	    $(html).appendTo($("#listado-archivos"));

    	$('<input type="file" name="archivos[]" id="archivo-'+nextId+'" data-id="'+nextId+'">').insertAfter($(this));
	});
	$("#listado-archivos").on('click','a.btn', function(e) {
		var id = $(this).data("id");
		$("#archivo-"+id).remove();
		$(this).parent().remove();
	});


    $('#form_crear').submit(function() {
    	fecha = $(this).find(':input[name="insp_fecha_solicitud_fecha"]').val();
    	if(fecha==""){
    		alert("Debe ingresar fecha de solicitud");
    		return false;
    	}
        fecha_solicitud = FormatDatetime(fecha);
        if(!IsDatetimeInFuture(fecha_solicitud)){
            alert("Debe indicar una fecha programada mayor a la fecha actual");
            return false;
        }

        insp_tipo = $(this).find(':input[name="insp_tipo"]').val();
        if(insp_tipo==""){
            alert("Debe ingresar tipo");
            return false;
        }

        if(insp_tipo=="MPP"){
            mant_id = $(this).find(':input[name="mant_id"]').val();
            if(mant_id==""){
                alert("Debe ingresar ID de mantenimiento");
                return false;
            }
        }

        espe_id = $(this).find(':input[name="espe_id"]').val();
        if(espe_id==""){
            alert("Debe ingresar especialidad");
            return false;
        }

    	empr_id = $(this).find(':input[name="empr_id"]').val();
    	if(empr_id==""){
    		alert("Debe ingresar empresa");
    		return false;
    	}

    	descripcion = $(this).find(':input[name="insp_descripcion"]').val();
    	if(descripcion==""){
    		alert("Debe ingresar una descripción");
    		return false;
    	}


    	archivos_descripciones = $(this).find(':input[name="archivos_descripciones"]');
    	for(i=0;i<archivos_descripciones.length;i++){
    		t = archivos_descripciones[i];
    		if($(t).val()==""){
    			alert("Debe ingresar una descripción para cada archivo");
    			return false;
    		}
    	}

    	$(this).find(':input[name="insp_fecha_solicitud"]').val(fecha_solicitud);
        $(this).find(':input[name="insp_fecha_solicitud_fecha"]').attr("disabled",true);

        $("#submit_crear").button('loading');
        return true;
    });


    //funciones auxiliares
    function FormatDatetime(fecha){
    	fecha = fecha.split("-");
    	return fecha[2]+"-"+fecha[1]+"-"+fecha[0];
    }

    function IsDatetimeInFuture(datetime){
    	fecha     = datetime.split("-");

    	now = new Date();
    	dt  = new Date(fecha[0],parseInt(fecha[1])-1,fecha[2],23,59,0,0);
    	return (now.getTime() < dt.getTime());
    }
})(jQuery);
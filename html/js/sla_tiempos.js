(function($) {
    //INICIALIZACION
	var filterTimer=null;
	var filterTimeout=500;
	
    $('.selectpicker').selectpicker();

    $('#orse_fecha_validacion_inicio,#orse_fecha_validacion_termino').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });
    
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m - 1, 1);
    var lastDay = new Date(y, m + 1, 0);
    $('#orse_fecha_validacion_inicio').datepicker('setDate',firstDay);
    $('#orse_fecha_validacion_termino').datepicker('setDate',lastDay);


    //EVENTOS
    $('#siom-form-sla-tiempos input[name=orse_id]').keyup(function() {
        FiltrarConDelay();
    });
    
    $('#orse_fecha_validacion_inicio,#orse_fecha_validacion_termino').change(function() {
        $("#siom-form-sla-tiempos").submit();
    });

    $('#siom-form-sla-tiempos select[name=orse_tipo]').change(function() {
        $("#siom-form-sla-tiempos").submit();
    });

    $('#siom-form-sla-tiempos select[name=zona_id]').change(function() {
        $("#siom-form-sla-tiempos").submit();
    });
	
    $('#siom-form-sla-tiempos select[name=zona_tipo]').change(function() {
        $("#siom-form-sla-tiempos").submit();
    });

    $('#siom-form-sla-tiempos select[name=sla_cumple]').change(function() {
        $("#siom-form-sla-tiempos").submit();
    });

    $('.BotonSLATiemposMenu').click(function(event) {
        event.preventDefault();
        //console.log('.BotonSLATiemposMenu click:' + $(this).data('tipo_datos'));
        $('.nav-tabs li').removeClass("active");
        $(this).parent().addClass("active");
        $('#tipo_datos').val($(this).data('tipo_datos'));
        $('#pagina').val("1");
/*
        if ($(this).data('tipo_datos') == "graficos" || $(this).data('tipo_datos') == "graficos_oseu_osgu") {
            $('#FiltroTipo').hide();
            $('#FiltroTipo').show();
        }else{
            $('#FiltroTipo').hide();
            $('#FiltroTipo').show();
        }
*/
        if ($(this).data('tipo_datos') == "validacion") {
            $('#os_id').show();
            $('#sla_cumple').show();

        } else {
            $('#os_id').hide();
            $('#sla_cumple').hide();
        }


        if ( "resumen" == $(this).data('tipo_datos')   ||  "validacion"== $(this).data('tipo_datos')  ) { /*|| $(this).data('tipo_datos') === "detalle" || $(this).data('tipo_datos') === "resumen"*/
            $('#FiltroZona').show();
            $('#FiltroZonaTipo').hide();

        } else {
            $('#FiltroZona').hide();
            $('#FiltroZonaTipo').show();
        }

        $("#siom-form-sla-tiempos").submit();
    });

    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#siom-form-sla-tiempos").submit();
        }, filterTimeout);
    }
})(jQuery);

(function($) {
    //
    window.ProcesarTarea = function(tarea) {
        data = {};
        if(tarea == null){
            return;
        }

        if (tarea.tare_data != null) {
            data = JSON.parse(tarea.tare_data.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t"));
        }

        switch (tarea.tare_modulo) {
            case 'OS':
                {
                    switch (tarea.tare_tipo) {
                        case 'ASIGNAR':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Asignar",
                                        link: "#/os/asignacion/" + tarea.tare_id_relacionado,
                                        ruta: "#/os/asignacion/*",
                                        texto_alt: "Asignando",
                                        datos: data
                                    };
                                }
                            }
                        case 'REASIGNAR':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Reasignar",
                                        link: "#/os/asignacion/" + tarea.tare_id_relacionado,
                                        ruta: "#/os/asignacion/*",
                                        texto_alt: "Asignando",
                                        datos: data
                                    };
                                }
                            }
                        
                        case 'VALIDAR_PRESUPUESTO':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar presupuesto",
                                        link: "#/os/presupuesto/" + tarea.tare_id_relacionado + "/validar/" + data['pres_id'],
                                        ruta: "#/os/presupuesto/*/validar/*",
                                        texto_alt: "Validando presupuesto",
                                        datos: data
                                    };
                                }
                            }


                        case 'MODIFICA_PRESUPUESTO':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Modificar presupuesto",
                                        link: "#/os/presupuesto/" + tarea.tare_id_relacionado + "/modificar",
                                        ruta: "#/os/presupuesto/*/modificar/*",
                                        texto_alt: "Presupuesto con Observaciones",
                                        datos: data
                                    };
                                }
                            }

                        case 'VALIDAR_INFORME':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar informe",
                                        link: "#/os/informe/" + tarea.tare_id_relacionado + "/validar/" + data['info_id'],
                                        ruta: "#/os/informe/*/validar/*",
                                        texto_alt: "Validando informe",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_SOLICITUD_CAMBIO':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar solicitud de cambio",
                                        link: "#/os/solicitud/cambio/" + tarea.tare_id_relacionado + "/validar/" + tarea.tare_id,
                                        ruta: "#/os/solicitud/cambio/*/validar/*",
                                        texto_alt: "Validando solicitud de cambio",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_SOLICITUD_INFORME':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar solicitud informe Web",
                                        link: "#/os/solicitud/informe/" + tarea.tare_id_relacionado + "/validar/" + tarea.tare_id,
                                        ruta: "#/os/solicitud/informe/*/validar/*",
                                        texto_alt: "Validando solicitud subida de informe via Web",
                                        datos: data
                                    };
                                }
                            }
                        case 'VISITAR_SITIO':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Visitar sitio",
                                        link: "/#/os/detalle/" + tarea.tare_id_relacionado,
                                        texto_alt: "EN PROCESO",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_OS':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar OS",
                                        link: "#/os/cerrar/" + tarea.tare_id_relacionado,
                                        ruta: "#/os/cerrar/*",
                                        texto_alt: "Validando OS",
                                        datos: data
                                    };
                                }
                            }
                        case 'INGRESAR_INFORME_WEB':
                            {
                             if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Ingresar Informe Web",
                                        // #/os/visita/{{orse_id}}
                                        link: "#/os/visita/" + tarea.tare_id_relacionado,
                                        ruta: "#/os/visita/*",
                                        texto_alt: "Ingresando Informe via web",
                                        datos: data
                                    };
                                }   
                            }

                        case 'INGRESAR_PRESUPUESTO':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Ingresar Presupuesto",
                                        // /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+
                                        link: "#/os/presupuesto/" + tarea.tare_id_relacionado,
                                        ruta: "#/os/presupuesto/*",
                                        texto_alt: "Ingresando Presupuesto",
                                        datos: data
                                    };
                                }
                            }
                    }
                    break;
                }
            case 'MNT':
                {
                    switch (tarea.tare_tipo) {
                        case 'ASIGNAR':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Asignar",
                                        link: "#/mnt/asignacion/" + tarea.tare_id_relacionado,
                                        ruta: "#/mnt/asignacion/*",
                                        texto_alt: "Asignando",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_INFORME':
                            {
                                if (tarea.tare_estado == 'CREADA') {
                                    return {
                                        texto: "Validar informe",
                                        link: "#/mnt/informe/" + tarea.tare_id_relacionado + "/validar/" + data['info_id'],
                                        ruta: "#/mnt/informe/*/validar/*",
                                        texto_alt: "Validando informe",
                                        datos: data
                                    };
                                } else {
                                    return {
                                        texto: "Validar Informe",
                                        link: "#/mnt/cerrar/" + tarea.tare_id_relacionado,
                                        ruta: "#/mnt/cerrar/*",
                                        texto_alt: "Validar Informe",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_INFORME_DESPACHADOR':
                            {
                                if (tarea.tare_estado == 'CREADA') {
                                    return {
                                        texto: "Validar informe",
                                        link: "#/mnt/informe/" + tarea.tare_id_relacionado + "/validar/despachador/" + data['info_id'],
                                        ruta: "#/mnt/informe/*/validar/*",
                                        texto_alt: "Validando informe despachador",
                                        datos: data
                                    };
                                } else {
                                    return {
                                        texto: "Validar Informe",
                                        link: "#/mnt/cerrar/" + tarea.tare_id_relacionado,
                                        ruta: "#/mnt/cerrar/*",
                                        texto_alt: "Validar Informe",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_SOLICITUD_INFORME':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar solicitud informe Web",
                                        link: "#/mnt/solicitud/informe/" + tarea.tare_id_relacionado + "/validar/" + tarea.tare_id,
                                        ruta: "#/mnt/solicitud/informe/*/validar/*",
                                        texto_alt: "Validando solicitud de subida de informe via Web",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_SOLICITUD_CAMBIO_FECHA_PROGRAMADA':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar cambio de fecha programada",
                                        link: "#/mnt/solicitud/cambio_fecha_programada/" + tarea.tare_id_relacionado + "/validar/" + tarea.tare_id,
                                        ruta: "#/mnt/solicitud/cambio_fecha_programada/*/validar/*",
                                        texto_alt: "Validando cambio de fecha programada",
                                        datos: data
                                    };
                                }
                            }
                        case 'VISITAR_SITIO':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Visitar sitio",
                                        link: "#/mnt/detalle/" + tarea.tare_id_relacionado,
                                        texto_alt: "EN PROCESO",
                                        datos: data
                                    };
                                }
                            }
                        case 'VALIDAR_MNT':
                            {
                                if (tarea.tare_estado != 'REALIZADA') {
                                    return {
                                        texto: "Validar MNT",
                                        link: "#/mnt/cerrar/" + tarea.tare_id_relacionado,
                                        ruta: "#/mnt/cerrar/*",
                                        texto_alt: "Validando MNT",
                                        datos: data
                                    };
                                }
                            }
                    }
                    break;
                }
            case 'INSP':
                {
                    switch (tarea.tare_tipo) {
                        case 'ASIGNAR':
                            {
                                return {
                                    texto: "Asignar",
                                    link: "#/insp/asignacion/" + tarea.tare_id_relacionado,
                                    ruta: "#/insp/asignacion/*",
                                    texto_alt: "Asignando",
                                    datos: data
                                };
                            }
                        case 'VALIDAR_INFORME':
                            {
                                return {
                                    texto: "Validar informe",
                                    link: "#/insp/informe/" + tarea.tare_id_relacionado + "/validar/" + data['info_id'],
                                    ruta: "#/insp/informe/*/validar/*",
                                    texto_alt: "Validando informe",
                                    datos: data
                                };
                            }

                        case 'VISITAR_SITIO':
                            {
                                return {
                                    texto: "Visitar sitio",
                                    link: "",
                                    texto_alt: "EN PROCESO",
                                    datos: data
                                };
                            }
                        case 'VALIDAR_INSP':
                            {
                                return {
                                    texto: "Validar Inspección",
                                    link: "#/insp/cerrar/" + tarea.tare_id_relacionado,
                                    ruta: "#/insp/cerrar/*",
                                    texto_alt: "Validando Inspección",
                                    datos: data
                                };
                            }
                    }
                    break;
                }
        }
        return {
            texto: "N/D",
            link: "",
            texto_alt: "",
            datos: {}
        };
    }

    window.ProcesarNotificacion = function(notif) {
        data = {};
        if (notif.noti_data != null) {
            data = JSON.parse(notif.noti_data.replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t"));
        }
        texto = notif.noti_tipo;
        tipo = "notice";

        switch (notif.noti_modulo) {
            case 'OS':
                {
                    switch (notif.noti_tipo) {
                        case 'SOLICITUD_CAMBIO':
                            {
                                texto = "SOLICITUD DE CAMBIO";
                                break;
                            }

                        case 'PRESUPUESTO_AGREGADO':
                            {
                                texto = "PRESUPUESTO AGREGADO";
                                break;
                            }
                        case 'PRESUPUESTO_VALIDADO':
                            {
                                estado = data.pres_estado;
                                if (estado == "PREAPROBADO") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "PRESUPUESTO <b>" + estado + "</b>";
                                break;
                            }
                        case 'INFORME_AGREGADO':
                            {
                                texto = "INFORME AGREGADO";
                                break;
                            }
                        case 'INFORME_VALIDADO':
                            {
                                estado = data.info_estado;
                                if (estado == "PREAPROBADO") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "INFORME <b>" + estado + "</b>";
                                break;
                            }
                        case 'SOLICITUD_CAMBIO_VALIDADA':
                            {
                                estado = data.orse_solicitud_cambio;
                                if (estado == "APROBADA") {
                                    tipo = "success";
                                    texto = "SOLICITUD DE CAMBIO <b>" + estado + "</b><br><br>Tipo OS cambio de <b>" + data.orse_tipo_anterior + "</b> a <b>" + data.orse_tipo_actual + "</b><hr>";
                                } else {
                                    tipo = "error";
                                    texto = "SOLICITUD DE CAMBIO <b>" + estado + "</b>";

                                }
                                break;
                            }
                        case 'SOLICITUD_INFORME_VALIDADA':
                            {
                                estado = data.orse_solicitud_informe_web;
                                if (estado == "APROBADA") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "SOLICITUD DE SUBIDA DE INFORME VIA WEB <b>" + estado + "</b>";
                                break;
                            }
                        case 'OS_FINALIZADA':
                            {
                                texto = "OS FINALIZADA,<br>En espera de validación";
                                break;
                            }
                        case 'OS_VALIDADO':
                            {
                                estado = data.orse_estado;
                                if (estado == "APROBADA") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "ORDEN DE SERVICIO <b>" + estado + "</b>";
                                break;
                            }
                        case 'VISITA_EJECUTANDO':
                            {
                                texto = "INGRESO A SITIO";
                                break;
                            }
                    }
                    break;
                }
            case 'MNT':
                {
                    switch (notif.noti_tipo) {
                        case 'INFORME_AGREGADO':
                            {
                                texto = "INFORME AGREGADO";
                                break;
                            }
                        case 'INFORME_VALIDADO':
                            {
                                estado = data.info_estado;
                                if (estado == "PREAPROBADO") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "INFORME <b>" + estado + "</b>";
                                break;
                            }
                        case 'MNT_VALIDADO':
                            {
                                estado = data.mant_estado;
                                if (estado == "APROBADA") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "MANTENIMIENTO <b>" + estado + "</b>";
                                break;
                            }
                        case 'VISITA_EJECUTANDO':
                            {
                                texto = "INGRESO A SITIO";
                                break;
                            }
                        case 'SOLICITUD_INFORME_VALIDADA':
                            {
                                estado = data.mant_solicitud_informe_web;
                                if (estado == "APROBADA") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "SOLICITUD DE INFORME <b>" + estado + "</b>";
                                break;
                            }
                        case 'SOLICITUD_CAMBIO_FECHA_PROGRAMADA':
                            {
                                texto = "SOLICITANDO CAMBIO DE FECHA PROGRAMADA";
                                break;
                            }
                        case 'SOLICITUD_CAMBIO_FECHA_PROGRAMADA_VALIDADA':
                            {
                                estado = data.mant_solicitud_cambio_fecha_programada;
                                if (estado == "APROBADA") {
                                    tipo = "success";
                                } else {
                                    tipo = "error";
                                }
                                texto = "SOLICITUD DE CAMBIO DE FECHA PROGRAMADA <b>" + estado + "</b>";
                                break;
                            }
                    }
                    break;
                }
        }
        return {
            texto: texto,
            tipo: tipo
        };
    }



    Swag.registerHelpers(window.Handlebars);

    Handlebars.registerHelper('JSON', JSON.stringify);

    Handlebars.registerHelper('timeAgo', function(fecha) {
        return $.timeago(fecha);
    });

    Handlebars.registerHelper('empty', function(text) {
        return (text == null || text == "") ? ("-") : (text);
    });

    Handlebars.registerHelper('boolean', function(value) {
        return (0 < value) ? ("SI") : ("NO");
    });


    Handlebars.registerHelper('loadTemplate', function(template, data) {
        var html = "Error al cargar template '" + template + "'";
        $.ajax({
            url: "templates/" + template + ".hb",
            async: false,
            success: function(src) {
                template = Handlebars.compile(src);
                html = template(data);
            },
        });

        return html;
    });


    Handlebars.registerHelper('siomChequearPerfil', function(ruta, options) {
        if (window.profile) {
            if (window.profile.indexOf(ruta) != -1) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    });

	Handlebars.registerHelper('visibility', function(valor, options) {
		console.log(valor);
		console.log("visibility");
		if ('1' == valor) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}

    });

    Handlebars.registerHelper('siomChequearTipoOS', function(tipo, options) {
        if (window.profile) {
            if (tipo != "OSGU" && tipo != "OSGN") {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    });

    Handlebars.registerHelper('ListarTareas', function(tareas, options) {
        var ret = "";
        for (var i = 0, j = tareas.length; i < j; i++) {
            tarea = tareas[i];
            t = window.ProcesarTarea(tarea);
            ret = ret + options.fn(tarea, {
                data: Handlebars.createFrame(t)
            });
        }
        return ret;
    });


    Handlebars.registerHelper('siomChequearTarea', function(tarea, options) {
        if (window.profile) {
            if ($.isArray(tarea)) {
                for (var i = 0; i < tarea.length; i++) {
                    t = window.ProcesarTarea(tarea[i]);
                    if (window.profile.indexOf(t.ruta) != -1) {
                        return options.fn(this, {
                            data: Handlebars.createFrame(t)
                        });
                    }
                }
                t = window.ProcesarTarea(tarea[0]);
                return options.inverse(this, {
                    data: Handlebars.createFrame({
                        texto_alt: t.texto_alt
                    })
                });
            } else {
                t = window.ProcesarTarea(tarea);
                if (window.profile.indexOf(t.ruta) != -1) {
                    return options.fn(this, {
                        data: Handlebars.createFrame(t)
                    });
                } else {
                    return options.inverse(this, {
                        data: Handlebars.createFrame({
                            texto_alt: t.texto_alt
                        })
                    });
                }
            }
        }
    });

    Handlebars.registerHelper('siomChequearTareaAlt', function(tarea, options) {
        if (window.profile) {
            if ($.isArray(tarea)) {
                tareaExists = 0;
                for (var i = 0; i < tarea.length; i++) {
                    if (window.profile.indexOf(t.ruta) != -1) {
                        console.log("TAREA EXISTS");
                        tareaExists = 1;
                        return null;
                    }
                }
                if(0 == tareaExists ){
                    console.log("TAREA NOT EXISTS");
                    t = window.ProcesarTarea(tarea[0]);
                    return options.fn(this, {
                        data: Handlebars.createFrame({
                            texto_alt: t.texto_alt
                        })
                    });
                }
            } else {
                t = window.ProcesarTarea(tarea);
                if (window.profile.indexOf(t.ruta) != -1) {
                    return null;
                } else {
                    return options.fn(this, {
                        data: Handlebars.createFrame({
                            texto_alt: t.texto_alt
                        })
                    });
                }
            }
        }
    });


    Handlebars.registerHelper('emplColorStatus', function(indisponibilidad) {
        if (indisponibilidad == "SI") {
            return "danger";
        } else if (indisponibilidad == "NO" || indisponibilidad == "") {
            return "success";
        }
        return "warning";
    });


    Handlebars.registerHelper('indiColorStatus', function(fecha, estado, tipo) {
        return "danger";
    });

	Handlebars.registerHelper('qrColorStatus', function(leco_id, leco_fecha) {
        return "danger";
    });

    Handlebars.registerHelper('osColorStatus', function(fecha, estado, tipo) {
        if (estado == "APROBADA") {
            return "success";
        } else if (estado == "RECHAZADA") {
            return "danger";
        } else if (estado == "NOACTIVO") {
            return "warning";
        } else if (estado == "CREADA" || estado == "ASIGNANDO" || estado == "ASIGNADA" || estado == "DEVUELTA") {
            now = (new Date()).getTime();
            date = (new Date(fecha)).getTime();

            //console.log(fecha,estado,tipo,window.config);
            offset = (window.config.os.colorStatus[tipo]["warning_offset"]) * 60 * 60 * 1000; //a milisegundos

            if (now < (date - offset)) {
                //console.log("success")
                return "success"
            } else if ((date - offset) <= now && now < date) {
                //console.log("warning")
                return "warning"
            } else {
                //console.log("danger")
                return "danger"
            }
        } else {
            //console.log("> success");
            return "success"
        }
    });

	Handlebars.registerHelper('defaColorStatus', function(estado) {
		if(estado == "CREADA") {
		  return "warning";
		} else if (estado == "ACTIVO") {
		  return "warning";
		} else if (estado == "ATENDIDA") {
		  return "info";
		} else if(estado == "FINALIZADA") {
		  return "success";
		} else if(estado == "CANCELADA") {
		  return "success";
		} else if(estado == "RECHAZADA") {
		  return "danger";
		}
	});

    Handlebars.registerHelper('mntColorStatus', function(fecha, estado) {
        if (estado == "APROBADA") {
            return "success";
        } else if (estado == "RECHAZADA" || estado == "FINALIZADA") {
            return "danger";
        } else if (estado == "CREADA" || estado == "ASIGNANDO" || estado == "ASIGNADA") {
            now = (new Date()).getTime();
            date = (new Date(fecha)).getTime();

            //console.log(fecha,estado,tipo,window.config);
            offset = 0; //(window.config.os.colorStatus[tipo]["warning_offset"])*60*60*1000; //a milisegundos

            if (now < (date - offset)) {
                //console.log("success")
                return "success"
            } else if ((date - offset) <= now && now < date) {
                //console.log("warning")
                return "warning"
            } else {
                //console.log("danger")
                return "danger"
            }
        } else {
            //console.log("> success");
            return "success"
        }
    });

    Handlebars.registerHelper('osSumItemsPresupuesto', function(list) {
        var tot = 0;
        for (var i = 0; i < list.length; i++) {
            tot += parseFloat(list[i].lpip_precio * list[i].prit_cantidad);
        }

        return tot.toFixed(2);
    });

    Handlebars.registerHelper('validarDefaCreada'), function(estado, options){
        if(estado == "CREADA"){
            return options.fn(this);
        }
    }

    Handlebars.registerHelper('osConSolicitudCambio', function(tipo, options) {
        if (tipo == "OSGU" || tipo == "OSGN") {
            return options.fn(this);
        }
    });

    Handlebars.registerHelper('osConPresupuesto', function(tipo, options) {
        if (tipo == "OSEU" || tipo == "OSEN" || tipo == "OSI") {
            return options.fn(this);
        }
    });

    Handlebars.registerHelper('osChequearActiva', function(estado, options) {
        if (estado != "APROBADA" && estado != "FINALIZADA" && estado != "RECHAZADA" && estado != "ANULADA" && estado != "NOACTIVO" ) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
     Handlebars.registerHelper('osChequearActivaResponsable', function(responsable, options) {
        if (responsable !="MOVISTAR") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('osChequearDevuelta', function(estado, options) {
        if (estado != "DEVUELTA") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('mntChequearActivo', function(estado, options) {
        if (estado != "APROBADA" && estado != "FINALIZADA" && estado != "RECHAZADA" && estado != "ANULADA" && estado != "NO REALIZADO") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });
     Handlebars.registerHelper('mntChequearActivaResponsable', function(responsable, options) {
        if (responsable !="MOVISTAR") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('inspChequearActivo', function(estado, options) {
        if (estado != "ACEPTADO" && estado != "FINALIZADA" ) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    //REVISAR LOGICA RESPECTO AL ESTADO, POR EL TIPO DE ESTADO
    Handlebars.registerHelper('indiChequearActiva', function(estado, options) {
        if (estado != "APROBADA" && estado != "RECHAZADA" && estado != "FINALIZADA" && estado != "ANULADA" && estado != "NOACTIVO") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('inspColorStatus', function(fecha, estado) {
        if (estado == "APROBADA") {
            return "success";
        } else if (estado == "RECHAZADA" || estado == "FINALIZADA") {
            return "danger";
        } else if (estado == "CREADA" || estado == "ASIGNANDO" || estado == "ASIGNADA") {
            now = (new Date()).getTime();
            date = (new Date(fecha)).getTime();

            //console.log(fecha,estado,tipo,window.config);
            offset = 0; //(window.config.os.colorStatus[tipo]["warning_offset"])*60*60*1000; //a milisegundos

            if (now < (date - offset)) {
                //console.log("success")
                return "success"
            } else if ((date - offset) <= now && now < date) {
                //console.log("warning")
                return "warning"
            } else {
                //console.log("danger")
                return "danger"
            }
        } else {
            //console.log("> success");
            return "success"
        }
    });

    Handlebars.registerHelper('PresupuestosValidados', function(ary) {
        var tot = 0;
        for (var i = 0; i < ary.length; i++) {
            alert("options.fn(ary[i] " + ary[i].prit_estado);
            if (ary[i].prit_estado == "VALIDADO") {
                tot++;
            }
        }
        return tot;
    });

    Handlebars.registerHelper('CantidadEditable', function(estado) {
        if (estado != "VALIDADO") {
            return "";
        } else {
            return "readonly";
        }
    });

	//DESARROLLO PARA PEXT
    Handlebars.registerHelper('OSEtiquetaIncidencia', function(especialidad) {
        console.log("INCIDENCIA");
        console.log(especialidad);
        if (especialidad == 63) {
            return "Nro Aviso";
        } else {
            return "REMEDY";
        }
    });

    Handlebars.registerHelper('PresupuestoItemEstado', function(estado, options) {
        if (estado != "VALIDADO") {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('CarruselArreglo', function(context, options) {
        var tot = 0;
        var out = "";
        for (var i = 0; i < context.length; i++) {
            out = "<div class='col-sm-3'>";
            out += "<a href= '" + options.fn(context[i]) + "'><img src= '" + options.fn(context[i]) + "' class='img-responsive'></a>";
            out += "</div>";
        }

        return out;
    });

    Handlebars.registerHelper('CreaContenedor', function(max, ary, options) {
        var result = "";
        for (var i = 0; i < Math.ceil(ary.length / max); i++) {
            if (options.data) {
                data = Handlebars.createFrame(options.data || {});
                data.index = i;
            }
            result += options.fn(ary[i], {
                data: data
            });
        }
        return result;
    });

    Handlebars.registerHelper('CantImagenes', function(max, ary, options) {
        if (!ary || ary.length == 0)
            return options.inverse(this);

        var result = "",
            currentItemIndex = "";

        for (var j = 0; j < max; j++) {
            currentItemIndex = (options.data.index * max) + j;
            if (currentItemIndex < ary.length) {
                result += options.fn(ary[currentItemIndex]);
            }
        }

        return result;
    });

    Handlebars.registerHelper('osEstadoDetalleVisita', function(estado) {
        if (estado == 'DONE') {
            return 'REALIZADA';
        } else if (estado == 'DOING') {
            return 'EN PROCESO';
        } else if (estado == 'NOT_DONE') {
            return 'NO REALIZADA';
        } else {
            return ""
        }
    });

    Handlebars.registerHelper('getJobLink', function(modulo, tipo, id_rel, data) {
        return window.getJobLink(modulo, tipo, id_rel, data);
    });

    Handlebars.registerHelper('parsearEvento', function(evento) {
        return evento.replace(/_/g, " ");

    });

    Handlebars.registerHelper('presCalcSubtotal', function(precio, cantidad) {
        return (precio * cantidad).toFixed(2);

    });


    Handlebars.registerHelper('formAggregatorLabel', function(opciones, indice) {
        console.log(opciones, indice)

        label = "";
        if (opciones && opciones.labels && indice < opciones.labels.length) {
            label = opciones.labels[indice];
        }

        return label;

    });

    Handlebars.registerHelper('formAggregatorIsImage', function(value, options) {
        if ($.isArray(value)) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('formNotAggregated', function(opciones, fova_valor, options) {
        if (opciones && opciones.aggregated) {
            if("" == fova_valor || fova_valor == null){
                return options.inverse(this);    
            }else{
                return options.fn(this);
            }
        } else {
            return options.fn(this);
        }
    });

    Handlebars.registerHelper('emptyForm', function(text) {
        return (text == null || text == "" || text == "[object Window]") ? ("NO REVISADO") : (text);
    });

    Handlebars.registerHelper('estadoColorStatus', function(estado) {
        if (estado == "NOACTIVO") {
            return "danger";
        } else if (estado == "ACTIVO") {
            return "success";
        }
        return "warning";
    });

    Handlebars.registerHelper('ifnull', function(field, value) {
        if (field == null || field == "") {
            return value;
        } else {
            return field;
        }
    });

    Handlebars.registerHelper('ifequal', function(a, b, opts) {
        if (a == b) // puede ser === dependiendo de lo que se necesite
            return opts.fn(this);
        else
            return opts.inverse(this);
    });

    Handlebars.registerHelper('inspFormatEstado', function(estado) {
        if (estado == 'ACEPTADA_REPAROS') {
            return 'ACEPTADA CON REPAROS';
        }
        return estado;
    });

    Handlebars.registerHelper('in', function(field, value, options) {
        arr = JSON.parse(value);
        console.log(field, value, arr);

        if ($.inArray(field, arr) != -1) {
            console.log("in")
            return options.fn(this);
        } else {
            console.log("out")
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('ifNotEmptyObject', function(obj, options) {
        console.log("obj=>", obj)
        if (!$.isEmptyObject(obj)) {
            console.log("in")
            return options.fn(this);
        } else {
            console.log("out")
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('ifNoZero', function(val, options) {
        if (val != 0) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    Handlebars.registerHelper('parseInventarioElementoCaracteristica', function(inec_campo, inec_valor) {
        //console.log('parseInventarioElementoCaracteristica');
        if (inec_campo != "Imagen") return inec_valor;
        try {
            var data = JSON.parse(inec_valor);
            //console.log("data");
            //console.log(data);
            if (data && typeof data === "object" && data !== null) {
                //se arma la respuesta para im⨥nes                
                var out = "<div>";
                $.each(data, function(index, value) {
                    //console.log(value);
                    out += '<img src="uploads/' + value['image'] + '" alt="imagen" height="200px"><br>'; //height="42" width="42"
                    out += '<span>' + value['comment'] + '</span><br>';
                });
                out += "</div>";
                return new Handlebars.SafeString(out);
            }
        } catch (e) {}
        return inec_valor;

    });


    Handlebars.registerHelper('ocultarElementoPorPerfil', function(perfil,options) {
        console.log(window.user_level);
        if(window.user_level.includes(perfil)){

            return options.inverse(this);
        }
        else{
            return options.fn(this);
        }

    });
    /* FORMULA PARA CALCULAR DISTANCIA ENTRE EMPLAZAMIENTO Y DENUNCIA FALLA */
    Handlebars.registerHelper('calcularDistancia', function(lat1, lon1, lat2, lon2) {
        var R = 6371; // km
        var dLat = (lat2-lat1) * Math.PI / 180;
        var dLon = (lon2-lon1) * Math.PI / 180;
        var lat1 = lat1 * Math.PI / 180;
        var lat2 = lat2 * Math.PI / 180;

        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        var d = R * c;
        return d.toFixed(3) * 1000;
    });

})(jQuery);

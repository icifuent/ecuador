(function($) { 
  $('.selectpicker').selectpicker();

 window.scrollToElement($('#siom-validar-informe'));

  $('div.siom-informe-mapa').each(function() {
      setMap($(this),$(this).data("latitud"),$(this).data("longitud"));
  });

  $('#siom-validar-informe-form').submit(function() {
      estado = $(this).find(':input[name="info_estado"]').val();
      if(estado==""){
        alert("Debe ingresar estado de validación");
        return false;
      }

      if(estado=="RECHAZADO"){
        observacion = $(this).find(':input[name="info_observacion"]').val();
        if(observacion==""){
          alert("Debe ingresar observacion");
          return false;
        }
      }

      return true;
  });

  $("button.download").click(function(e){
      $btn = $(this);
      $btn.button("loading");
      $.fileDownload("rest/core/repo/informe/"+$(this).data("info-id"), {
          prepareCallback:function(url) {
              $btn.button("processing");
          },
          successCallback: function(url) {
              $btn.button('reset')
          },
          failCallback: function(responseHtml, url) {
              $btn.button('reset')
              alert(responseHtml);
          }
      });
    });
  

  function setMap(element,lat,lng){
    if((typeof google != "undefined") && lat!="" && lng!=""){
      var gMap=null;
      var gMarker = null;
      var latlng = new google.maps.LatLng(lat,lng);

      if(!gMap){
         var mapOptions = {
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        gMap = new google.maps.Map(element[0],mapOptions); 
        gMarker = new google.maps.Marker({map: gMap});  
      }
        
       google.maps.event.trigger(gMap,'resize');
       gMarker.setPosition(latlng);
       gMap.setCenter(latlng);
    }
  }
})(jQuery);

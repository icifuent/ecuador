(function($) { 
	var filterTimer=null;
	var filterTimeout=500;
	var filesCount = 0;

	//inicializar componentes
    $('.selectpicker').selectpicker();
    $('#wizard').wizard();
    $('#mant_fecha_programada_fecha').datepicker({
                format: "dd-mm-yyyy",
                viewMode: "days", 
                minViewMode: "days",
                language: "es"
            });
    $('.timepicker').timepicker({showMeridian:false,defaultTime: false});
    $('.timepicker').click(function(){
        $(this).timepicker('showWidget');
    });
  
     
	$("#form_editar_mnt").on('change','.btn-file :file', function() {
	    var input = $(this),
	   	id     = input.data("id");
	   	label  = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	   	nextId = parseInt(id)+1;

	   	var html = '<li class="list-group-item">';
	   	html+='<a class="btn btn-default btn-xs" data-id="'+id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
	   	html+='<div>';
	   	html+='<div><textarea class="form-control" name="archivos_descripciones" rows="1"></textarea></div>';
	   	html+='<small>'+label+'</small>';
	   	html+='</div>';
	   	html+='</li>';
	    $(html).appendTo($("#listado-archivos"));

    	$('<input type="file" name="archivos[]" id="archivo-'+nextId+'" data-id="'+nextId+'">').insertAfter($(this));
	});
	$("#listado-archivos").on('click','a.btn', function(e) {
		var id = $(this).data("id");
		$("#archivo-"+id).remove();
		$(this).parent().remove();
	});


/*    $("#estado").change(function(e){
    	FiltrarFormularios($(this).val())
    }); */
    $("#estado").change(function(e){
        FiltrarEstado($(this).val());
        $('.estado').find('[id=primero]').remove();
        $("#estado").selectpicker('refresh');
    });
    $(".estado").click(function(){
        $('.estado').find('[id=primero]').remove();
        $("#estado").selectpicker('refresh');
    });

    $('#form_editar_mnt textarea[name=mant_descripcion]').keyup(function(e){
    	var text  = $(this).val();
    	var word_count = 0;
    	if(text!=""){
			word_count = text.trim().replace(/\s+/gi,' ').split(' ').length;
    	}
    	
		var palabras = $("#mant_descripcion_palabras");
		
		palabras.find("span").text(word_count);
		if(window.config.mnt.minDescription <= word_count){
			palabras.removeClass("text-danger").addClass("text-success");
		}
		else{
			palabras.removeClass("text-success").addClass("text-danger");
		}
	});

    $('#mant_fecha_solicitud_ahora').click( function(){
        disabled = false;
       if($(this).is(':checked')){
            disabled = true;
       }

       $('#fecha_programada').prop("disabled",disabled);
       $('.timepicker').prop("disabled",disabled);
    });


    $('#form_editar_mnt').submit(function() {

        fecha = $(this).find(':input[name="mant_fecha_programada_fecha"]').val();
        if(fecha==""){
            alert("Debe ingresar fecha de programada");
            return false;
        }

        hora = $(this).find(':input[name="mant_fecha_solicitud_hora"]').val();
        console.log("hora");
        console.log(hora);
        if(hora==""){
            alert("Debe ingresar hora de solicitud");
            return false;
        }

        if($(this).find(':input[name="mant_fecha_solicitud_ahora"]').is(':checked')) {
            fecha_programada = DatetimeNow();
        }
        else{
            fecha_programada = FormatDatetime(fecha,hora);
            if(!IsDatetimeInFuture(fecha_programada)){
                alert("Debe indicar una fecha programada de atencion mayor a la fecha y hora actuales");
                return false;
            }
        }

        empr_id = $(this).find(':input[name="empr_id"]').val();
        if(empr_id==""){
            alert("Debe ingresar empresa");
            return false;
        }
    	
        $("#submit_guardar").button('loading');
        return true;
    });


    //funciones auxiliares
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-os-crear-filtro").submit();
         },filterTimeout);
	}

    function FiltrarEstado(mant_estado){
        $("#estado option").attr("disabled",true);        
        $("#estado option[data-espe-id='"+mant_estado+"']").attr("disabled",false);
        $("#estado option[data-default='true']").attr("disabled",false);
        $("#estado").selectpicker('refresh');
    }

 /*   function FiltrarFormularios(esta_id){
    	$("#formulario option").attr("disabled",true);
        $("#formulario option").removeAttr('selected');
        
        $("#formulario option[data-espe-id='"+esta_id+"']").attr("disabled",false);
        $("#formulario option[data-espe-id='"+esta_id+"'][data-refe-seleccion='1']").attr('selected', 'selected');
    	$("#formulario option[data-default='true']").attr("disabled",false);

    	$("#formulario").selectpicker('refresh');
    } */

    function FormatDatetime(fecha){
    	fecha = fecha.split("-");
    	return fecha[2]+"-"+fecha[1]+"-"+fecha[0];
    }

    function IsDatetimeInFuture(datetime){
    	fecha     = datetime.split("-");

    	now = new Date();
    	dt  = new Date(fecha[0],parseInt(fecha[1])-1,fecha[2],23,59,0,0);
    	return (now.getTime() < dt.getTime());
    }
    
    FiltrarEstado($("#estado").val())

    function CargaInicialDatos(){
       
        /*EMPRESA */
        dataEmpresa=$(".empresa").data();
        $(".empresa").selectpicker('val', dataEmpresa.valueSelect);
        $(".empresa").change();
        $(".empresa").selectpicker('refresh');

    }
    
    CargaInicialDatos();
})(jQuery);
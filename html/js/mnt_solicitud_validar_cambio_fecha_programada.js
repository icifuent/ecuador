(function($) { 
  	$('.selectpicker').selectpicker();

 	window.scrollToElement($('#form_solicitud_cambio_fecha'));

 	$("#form_solicitud_cambio_fecha").submit(function(e){
    	solicitud = $(this).find(':input[name="mant_solicitud_cambio_fecha_programada"]').val();
    	if(solicitud==""){
    		alert("Debe ingresar resolución de solicitud");
    		return false;
    	}
        
    	return true;
 	});
  
 })(jQuery);
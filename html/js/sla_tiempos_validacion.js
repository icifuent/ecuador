(function($) {
	$('#pagination').bootpag().on("page", function(event, num){
		//console.log("#pagination: "+num);
		//console.log($('#pagina'));
		page = num;
		$('#pagina').val(page);
		$("#siom-form-sla-tiempos").submit();
	});
	
	$('.BotonSLATiemposValidacionEditar').click(function(event) {
		//console.log('.BotonSLATiemposValidacionEditar click:');
		
		if( $(this).hasClass('clase-editar') ){   
			//console.log("EDITAR");
			$(this).removeClass('clase-editar');
			$(this).addClass('clase-guardar');
			
			$(this).children("span").removeClass("glyphicon-pencil");
			$(this).children("span").addClass("glyphicon-floppy-disk");

			$(this).parent().parent().find(".clase-data").attr("disabled",false);
		}
		else{
			//console.log("GUARDAR");				
			var that = this;
			var data = {};
			var orse_id = $(this).data('orse_id');
            var sla_tiempo_respuesta = $(this).data('sla_tiempo_respuesta');
			data['sla_tiempo_respuesta_ajuste'] = $(this).parent().parent().find(".clase-data[name='sla_tiempo_respuesta_ajuste']").val();
			data['sla_tiempo_respuesta_exclusion'] = $(this).parent().parent().find(".clase-data[name='sla_tiempo_respuesta_exclusion']").is(':checked')?1:0;
			data['sla_tiempo_respuesta_observacion'] = $(this).parent().parent().find(".clase-data[name='sla_tiempo_respuesta_observacion']").val();
			
                        if((data['sla_tiempo_respuesta_ajuste'] == null || data['sla_tiempo_respuesta_ajuste'] == "")){
                                alert("Debe ingresar un delta de ajuste");
				return;
			}
                   
                        if( data['sla_tiempo_respuesta_ajuste'] % 1 !== 0 || data['sla_tiempo_respuesta_ajuste']<0){
                                alert("El ajuste debe ser un entero mayor o igual a cero");
				return;
                        }
			
                        if( data['sla_tiempo_respuesta_ajuste'] > sla_tiempo_respuesta){
                                alert("El ajuste no puede ser mayor que el tiempo de solucion");
				return;
                        }
                        
                        if( data['sla_tiempo_respuesta_exclusion'] ==1 && (data['sla_tiempo_respuesta_observacion'] == null || data['sla_tiempo_respuesta_observacion'] == "")){
				alert("Debe ingresar una observacion para excluir la orden de servicio");
				return;
			}  
			if((data['sla_tiempo_respuesta_observacion'] == null || data['sla_tiempo_respuesta_observacion'] == "")){
				data['sla_tiempo_respuesta_observacion'] = " ";
			}
			
			url = 'rest/core/sla_orden_servicio/upd/'+orse_id;
			$.post(url, data, function(json) {
				if (json.status) {
					$(that).removeClass('clase-guardar');
					$(that).addClass('clase-editar');
					
					$(that).children("span").removeClass("glyphicon-floppy-disk");
					$(that).children("span").addClass("glyphicon-pencil");

					$(that).parent().parent().find(".clase-data").attr("disabled",true);
					var sla_tiempo_respuesta_ajuste = data['sla_tiempo_respuesta_ajuste'];
					var sla = $(that).data('sla');
					var sla_tiempo_respuesta = $(that).data('sla_tiempo_respuesta');
					$(that).parent().parent().find(".clase-data-output").html( ( sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste  <= sla )?'SI':'NO'  );
				}
				else {
					alert("Error actualizando orden de servicio: " + json.error + ", ruta:" + url);
				}
			}).fail(function(xhr, textStatus, errorThrown) {
				alert("Error actualizando orden de servicio " + xhr.status + ": " + xhr.statusText + ", ruta:" + url);
			});
		} 

	});
	
	
	$("button.download").click(function(e){
	  $btn = $(this);
	  $btn.button("loading");
	  $.fileDownload("rest/core/repo/informe/"+$(this).data("info-id"), {
		  prepareCallback:function(url) {
			  $btn.button("processing");
		  },
		  successCallback: function(url) {
			  $btn.button('reset')
		  },
		  failCallback: function(responseHtml, url) {
			  $btn.button('reset')
			  alert(responseHtml);
		  }
	  });
	});        
	
})(jQuery);
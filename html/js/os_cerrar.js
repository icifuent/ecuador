(function($) { 
	$('#siom-cerrar-form button').click(function(e) {
        form   = $('#siom-cerrar-form');
        estado = $(this).data("orse-estado");
        console.log(estado);

        form.find(':input[name="orse_estado"]').val(estado);
    });

    $("#siom-cerrar-form").submit(function(e){
    	 form   = $(this);
    	 estado = form.find(':input[name="orse_estado"]').val();

    	 console.log(estado);
    	 if(estado=="RECHAZADA"){
    	 	if(form.find(':input[name="orse_comentario"]').val()==""){
    	 		alert("Debe ingresar comentario si rechaza OS");
    	 		return false;
    	 	}
    	 }
    	 return true;
    });
})(jQuery);
(function($) { 
    var filterTimer=null;
    var filterTimeout=500;
    var filesCount = 0;

    //inicializar componentes
    $('.selectpicker').selectpicker();
    $('#wizard').wizard();
    $('#fecha_solicitud').datepicker({
                format: "dd-mm-yyyy",
                viewMode: "days", 
                minViewMode: "days",
                language: "es"
            });
    $('.timepicker').timepicker({showMeridian:false,defaultTime: false});
    $('.timepicker').click(function(){
        $(this).timepicker('showWidget');
    })
     
    //inicializar eventos
    $(document).on('click','button.os-crear-selector',function(e){
        empl_id = $(this).data("empl-id");
        data    = $(this).data("data");
        
        $("#empl_id").val(empl_id);
        $('#wizard').wizard("next");

        $.ajax({
          url: "templates/empl_descripcion.hb",
          async: true,
          success: function(src){
            template = Handlebars.compile(src);
            $("#siom-empl-info").html(template(data));
          },
        });


    })

    $('#siom-form-mnt-crear-filtro input[name=empl_nemonico]').keyup(function () {
        /* FiltrarConDelay();*/
    })
    $('#siom-form-mnt-crear-filtro input[name=empl_nombre]').keyup(function () {
        /* FiltrarConDelay();*/
    })
    $('#siom-form-mnt-crear-filtro input[name=empl_direccion]').keyup(function () {
        /* FiltrarConDelay();*/
    })
    $('#siom-form-mnt-crear-filtro select[name=tecn_id]').change(function(){
        /*$("#siom-form-mnt-crear-filtro").submit();*/
    })
    $('#siom-form-mnt-crear-filtro select[name=zona_id]').change(function(){
        /*$("#siom-form-mnt-crear-filtro").submit();*/
    })
    $('#siom-form-mnt-crear-filtro select[name=regi_id]').change(function(){
       /* $("#siom-form-mnt-crear-filtro").submit();*/
    })
    $('#siom-form-mnt-crear-filtro select[name=clus_id]').change(function(){
       /* $("#siom-form-mnt-crear-filtro").submit();*/
    })


    $("#form_crear").on('change','.btn-file :file', function() {
		alert("CREAR ONCHANGE");
        var input = $(this),
        id     = input.data("id");
        label  = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        nextId = parseInt(id)+1;

        var html = '<li class="list-group-item">';
        html+='<a class="btn btn-default btn-xs" data-id="'+id+'"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
        html+='<div>';
        html+='<div><textarea class="form-control" name="archivos_descripciones" rows="1"></textarea></div>';
        html+='<small>'+label+'</small>';
        html+='</div>';
        html+='</li>';
        $(html).appendTo($("#listado-archivos"));

        $('<input type="file" name="archivos[]" id="archivo-'+nextId+'" data-id="'+nextId+'">').insertAfter($(this));
    });
    $("#listado-archivos").on('click','a.btn', function(e) {
        var id = $(this).data("id");
        $("#archivo-"+id).remove();
        $(this).parent().remove();
    });


    $("#especialidad").change(function(e){
        FiltrarFormularios($(this).val())
    });

    $('#form_crear textarea[name=mant_descripcion]').keyup(function(e){
        var text  = $(this).val();
        var word_count = 0;
        if(text!=""){
            word_count = text.trim().replace(/\s+/gi,' ').split(' ').length;
        }
        
        var palabras = $("#mant_descripcion_palabras");
        
        palabras.find("span").text(word_count);
        if(window.config.mnt.minDescription <= word_count){
            palabras.removeClass("text-danger").addClass("text-success");
        }
        else{
            palabras.removeClass("text-success").addClass("text-danger");
        }
    })

     $('#mant_fecha_solicitud_ahora').click( function(){
        disabled = false;
       if($(this).is(':checked')){
            disabled = true;
       }

       $('#fecha_programada').prop("disabled",disabled);
       $('.timepicker').prop("disabled",disabled);
    });


    $('#form_crear').submit(function() {
        fecha = $(this).find(':input[name="mant_fecha_programada_fecha"]').val();
        if(fecha==""){
            alert("Debe ingresar fecha de programada");
            return false;
        }

        hora = $(this).find(':input[name="mant_fecha_solicitud_hora"]').val();
		console.log("hora");
		console.log(hora);
        if(hora==""){
            alert("Debe ingresar hora de solicitud");
            return false;
        }

        empr_id = $(this).find(':input[name="empr_id"]').val();
        if(empr_id==""){
            alert("Debe ingresar empresa");
            return false;
        }

        espe_id = $(this).find(':input[name="espe_id"]').val();
        if(espe_id==""){
            alert("Debe ingresar especialidad");
            return false;
        }

        form_id = $(this).find(':input[name="form_id"]').val();
        console.log(form_id);
        if(form_id=="" || form_id==null){
            alert("Debe ingresar formulario(s)");
            return false;
        }

        
        descripcion = $(this).find(':input[name="mant_descripcion"]').val();
        if(descripcion==""){
            alert("Debe ingresar una descripción");
            return false;
        }

        word_count = descripcion.trim().replace(/\s+/gi,' ').split(' ').length;
        if(word_count < window.config.mnt.minDescription){
            alert("Debe ingresar al menos "+window.config.mnt.minDescription+" palabras en descripción");
            return false;
        }

        if($(this).find(':input[name="mant_fecha_solicitud_ahora"]').is(':checked')) {
            fecha_programada = DatetimeNow();
        }
        else{
            fecha_programada = FormatDatetime(fecha,hora);
            if(!IsDatetimeInFuture(fecha_programada)){
                alert("Debe indicar una fecha programada de atencion mayor a la fecha y hora actuales");
                return false;
            }
        }

 /*     fecha_programada = FormatDatetime(fecha);
        if(!IsDatetimeInFuture(fecha_programada)){
            alert("Debe indicar una fecha programada mayor a la fecha actual");
            return false;
        } */

        archivos_descripciones = $(this).find(':input[name="archivos_descripciones"]');
        for(i=0;i<archivos_descripciones.length;i++){
            t = archivos_descripciones[i];
            if($(t).val()==""){
                alert("Debe ingresar una descripción para cada archivo");
                return false;
            }
        }

        $(this).find(':input[name="mant_fecha_programada"]').val(fecha_programada);
        $(this).find(':input[name="mant_fecha_programada_fecha"]').attr("disabled",true);
        $(this).find(':input[name="mant_fecha_solicitud_hora"]').attr("disabled",true);
        $(this).find(':input[name="mant_fecha_solicitud_ahora"]').attr("disabled",true);
        $(this).find(':input[name="hour"]').attr("disabled",true);
        $(this).find(':input[name="minute"]').attr("disabled",true);

        $("#submit_crear").button('loading');
        return true;
    });

    /*** Boton Limpiar ***/
    $(document).on('click', 'button#LimpiarMntCrear', function (e) {
        $('#nombre').val("");
        $('#nemonico').val("");

        $('select[name=tecn_id]').val("");
        $('select[name=tecn_id]').change();
        $('select[name=tecn_id]').selectpicker('refresh');

        $('#empl_direccion').val("");
        
        $('select[name=zona_id]').val("");
        $('select[name=zona_id]').change();

        $('select[name=regi_id]').val("");
        $('select[name=regi_id]').change();

        $('select[name=clus_id]').val("");
        $('select[name=clus_id]').change();
        
    });

    /*** Boton Buscar ***/
    $(document).on('click', 'button#BuscarMntCrear', function (e) {
        $("#siom-form-mnt-crear-filtro").submit();
        return true;
    });


    //funciones auxiliares
    function FiltrarConDelay(){
        if(filterTimer){
            clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
            $("#siom-form-mnt-crear-filtro").submit();
         },filterTimeout);
    }

    function FiltrarFormularios(espe_id){
        $("#formulario option").attr("disabled",true);
        $("#formulario option").removeAttr('selected');
        
        $("#formulario option[data-espe-id='"+espe_id+"']").attr("disabled",false);
        $("#formulario option[data-espe-id='"+espe_id+"'][data-refe-seleccion='1']").attr('selected', 'selected');
        $("#formulario option[data-default='true']").attr("disabled",false);

        $("#formulario").selectpicker('refresh');
    }

    function FormatDatetime(fecha, hora){
        fecha = fecha.split("-");
        return fecha[2]+"-"+fecha[1]+"-"+fecha[0]+" "+hora;
    }

    function DatetimeNow(){
        now = new Date();
        var y  = now.getFullYear();
        var m  = (now.getMonth()  > 8)?(now.getMonth()+1):('0'+(now.getMonth()+1));
        var d  = (now.getDate()   > 9)?(now.getDate()    ):('0'+now.getDate());

        var H  = (now.getHours()  > 9)?(now.getHours()   ):('0'+now.getHours());
        var M  = (now.getMinutes()> 9)?(now.getMinutes() ):('0'+now.getMinutes());
        var S  = (now.getSeconds()> 9)?(now.getSeconds() ):('0'+now.getSeconds());

        return y.toString()+"-"+m.toString()+"-"+d.toString()+" "+H.toString()+":"+M.toString()+":"+S.toString();
    }

    function IsDatetimeInFuture(datetime){
        fechahora = datetime.split(" ");
        fecha     = fechahora[0].split("-");
        hora      = fechahora[1].split(":");

        now = new Date();
        now.setHours(now.getHours(),now.getMinutes(),0,0);
        
        dt  = new Date(fecha[0],parseInt(fecha[1])-1,fecha[2],hora[0],hora[1],0,0);
        return (now.getTime() <= dt.getTime());
    }
    
    FiltrarFormularios($("#especialidad").val())
})(jQuery);
(function($) {
  	$("#form-usuario-contrasena").submit(function(e){
    	actual = $(this).find(':input[name="usua_password"]').val();
    	if ("" == actual){
    		alert("Debe ingresar contraseña actual");
    		return false;
    	}

        nueva = $(this).find(':input[name="usua_password_nuevo"]').val();
        if (null != nueva && 0 != nueva.length) {
            console.log("el valor es " + nueva);
            /*ExpReg solicita una o mas mayúsculas*/
            var letras = /[A-Z]/;
            /*ExpReg Solicita   uno o mas caracteres especiáles*/
            var caracteres = /[!@#\$%\^&\*\?¿_~\/]/;
            /*ExpReg solicita uno  mas números*/
            var numeros = /[0-9]/;
            if (8 > valor.length) {
                alert("Al ingresar la password, debe ingresar mas de 8 caracteres.");
                return false;
            } else if (!letras.test(valor)) {
                alert("La Paswword, debe tener al menos una mayuscula.");
                return false;
            } else if (!caracteres.test(valor)) {
                alert("La Paswword, debe tener al menos un simbolo,");
                return false;
            } else if (!numeros.test(valor)) {
                alert("La clave, debe tener almenos un número.");
                return false;
            }
        } else {
            alert("Debe ingresar un password");
            return false;
        }

        nueva2 = $(this).find(':input[name="usua_password_nuevo2"]').val();
        if (0 == nueva2.length) {
            alert("Debe ingresar repetición de contraseña nueva");
            return false;
        }

        if (nueva != nueva2) {
            alert("Las contraseñas nuevas ingresadas no coinciden");
            return false;
        }

        if (actual == nueva) {
            alert("La contraseña nueva y la actual deben ser distintas");
            return false;
        }

        return true;
    });
})(jQuery);
(function($) {
	var filterTimer=null;
	var filterTimeout=500;

    function InitSelectPickers(){
        sp = $('select.selectpicker');
        for(i=0;i<sp.length;i++){
            s = $(sp[i]);

            name = s.attr('name');
            val  = s.data('value');
            s.selectpicker();

            s.selectpicker('val',val);
            s.selectpicker('refresh');
        }
    }
    
    InitSelectPickers();

    if(window.osFiltros){
        /*
        dt = window.osFiltros.usos_fecha_fin.split("-");
        if(parseInt(dt[0])<=31){
            window.osFiltros.usos_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
        } else {
            window.osFiltros.usos_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
        }

        //window.osFiltros.usos_fecha_fin     = dt[2]+"-"+dt[1]+"-"+dtE
        dt = window.osFiltros.usos_fecha_inicio.split("-");
        if(parseInt(dt[0])<=31){
            window.osFiltros.usos_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
        } else {
            window.osFiltros.usos_fecha_inicio= dt[2]+"-"+dt
            console.log("5");[1]+"-"+dt[0];

console.log("6");        }
        */
    } else {
        window.osFiltros = {};
    }

    /*else{
        window.osFiltros = {}
        now = new Date();
        d   = now.getDate();
        m   = now.getMonth()+1;
        y   = now.getFullYear();

		if(d < 10){
			d = "0"+d;
		}

		if(m < 10){
			m = "0"+m;
		}

        window.osFiltros.usos_fecha_fin    = d+"-"+m+"-"+y;
        window.osFiltros.usos_fecha_inicio = d+"-"+m+"-"+y;
        window.osFiltros.ustr_usuario      = "0";
    }
    */
//   	$("#fecha_inicio").val(window.osFiltros.usos_fecha_inicio);
 //   $("#fecha_fin").val(window.osFiltros.usos_fecha_fin);

    $('#fecha_inicio,#fecha_fin').datepicker({
        format: "dd-mm-yyyy",
        viewMode: "days",
        minViewMode: "days",
        language: "es"
    });

    //$('#os-bandeja-filtro').btsListFilter('#os-bandeja-lista', {dataSearch:true,initial:false,minLength:3});

    //Eventos
    $('#fecha_inicio,#fecha_fin').change(function(){
        /*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_fecha_inicio", $("#fecha_inicio").val());
        localStorage.setItem("orse_fecha_fin", $("#fecha_fin").val());
    })
    $('#siom-form-os-bandeja select[name=orse_tipo]').change(function(){
        /*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_tipo", $("select[name=orse_tipo]").val());
    })

    $('#siom-form-os-bandeja select[name=orse_estado]').change(function(){
        /*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_estado", $("select[name=orse_estado]").val());
    })
	$('#siom-form-os-bandeja select[name=orse_responsable]').change(function(){
		/*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_responsable", $("select[name=orse_responsable]").val());
	})

	$('#siom-form-os-bandeja input[name=orse_id]').keyup(function () {
        /* FiltrarConDelay(); */
        localStorage.setItem("orse_id", $("select[name=orse_id]").val());
    })

    $('#siom-form-os-bandeja input[name=empl_nemonico]').keyup(function () {
        /* FiltrarConDelay(); */
        localStorage.setItem("orse_empl_nemonico", $("select[name=empl_nemonico]").val());
    })

    $('#siom-form-os-bandeja input[name=empl_nombre]').keyup(function () {
        /* FiltrarConDelay(); */
        localStorage.setItem("orse_empl_nombre", $("select[name=empl_nombre]").val());
    })

	$('#siom-form-os-bandeja select[name=tecn_id]').change(function(){
		/*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_tecn_id", $("select[name=tecn_id]").val());
	})

    $('#siom-form-os-bandeja input[name=empl_direccion]').keyup(function () {
        /* FiltrarConDelay(); */
        localStorage.setItem("orse_empl_direccion", $("select[name=empl_direccion]").val());
    })

	$('#siom-form-os-bandeja select[name=zona_id]').change(function(){
		/*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_zona_id", $("select[name=zona_id]").val());
	})
	$('#siom-form-os-bandeja select[name=regi_id]').change(function(){
		/*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_regi_id", $("select[name=regi_id]").val());
	})

	$('#siom-form-os-bandeja select[name=clus_id]').change(function(){
		/*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_clus_id", $("select[name=clus_id]").val());
	})

	$('#siom-form-os-bandeja select[name=usua_creador]').change(function(){
		/*$("#siom-form-os-bandeja").submit(); */
        localStorage.setItem("orse_usua_creador", $("select[name=usua_creador]").val());
	})

        /*** Boton Limpiar ***/
    $(document).off('click', 'button#LimpiarOsBandeja');
    $(document).on('click', 'button#LimpiarOsBandeja', function (e) {
        $('#orse_id').val("");
        $('#orse_id').change();

        $('select[name=orse_tipo]').val("");
        $('select[name=orse_tipo]').change();

        if(window.osFiltros){
            /*
                dt = window.osFiltros.usos_fecha_fin.split("-");
                if(parseInt(dt[0])<=31){
                    console.log("");
                    window.osFiltros.usos_fecha_fin= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    console.log("");
                    window.osFiltros.usos_fecha_fin= dt[2]+"-"+dt[1]+"-"+dt[0];
                }

                
                dt = window.osFiltros.usos_fecha_inicio.split("-");
                if(parseInt(dt[0])<=31){
                    window.osFiltros.usos_fecha_inicio= dt[0]+"-"+dt[1]+"-"+dt[2];
                }else{
                    window.osFiltros.usos_fecha_inicio= dt[2]+"-"+dt[1]+"-"+dt[0];
                }
            }
            else{
                window.osFiltros = {}
                now = new Date();
                d   = now.getDate();
                m   = now.getMonth()+1;
                y   = now.getFullYear();

				if(d < 10){
					d = "0"+d;
				}
				if(m < 10){
					m = "0"+m;
				}
                window.osFiltros.usos_fecha_fin    = d+"-"+m+"-"+y;
                window.osFiltros.usos_fecha_inicio = d+"-"+m+"-"+y;
                */
            }
   		$("#fecha_inicio").val("");
    	$("#fecha_fin").val("");
        
    	$('#fecha_inicio,#fecha_fin').datepicker({
		    format: "dd-mm-yyyy",
		    viewMode: "days",
		    minViewMode: "days",
		    language: "es"
		});

        $('.selectpicker').selectpicker('val', ['CREADA', 'ASIGNADA', 'ASIGNANDO', 'VALIDANDO', 'EJECUTANDO']);
        $('select[name=orse_estado]').change();
        $('select[name=orse_estado]').selectpicker('refresh');

        $('select[name=orse_responsable]').val("");
        $('select[name=orse_responsable]').change();

        $('#empl_nemonico').val("");
        $('#empl_nombre').val("");

        $('select[name=tecn_id]').val("");
        $('select[name=tecn_id]').change();
        $('select[name=tecn_id]').selectpicker('refresh');

        $('#empl_direccion').val("");

        $('select[name=zona_id]').val("");
        $('select[name=zona_id]').change();

        $('select[name=regi_id]').val("");
        $('select[name=regi_id]').change();

        $('select[name=clus_id]').val("");
        $('select[name=clus_id]').change();

        $('select[name=usua_creador]').val("");
        $('select[name=usua_creador]').change();
        
        localStorage.clear();
        window.localStorage.clear();
    });

    /*** Boton Buscar ***/
    $(document).off('click', 'button#BuscarOsBandeja');
    $(document).on('click', 'button#BuscarOsBandeja', function (e) {
        $("#siom-form-os-bandeja").submit();
        return true;
    });



	//Actualización de fechas
	setInterval(function(){
		$(".timeago").timeago();
	},60000);

	//funciones utiles
	function FiltrarConDelay(){
		if(filterTimer){
         	clearTimeout(filterTimer);
         }
         filterTimer = setTimeout(function(){
         	$("#siom-form-os-bandeja").submit();
         },filterTimeout);
	}

	
    $('.selectpicker').selectpicker('val', ['CREADA', 'ASIGNADA', 'ASIGNANDO', 'VALIDANDO', 'EJECUTANDO', 'DEVUELTA']);

    if(localStorage.getItem("orse_fecha_inicio")){
        console.log(localStorage.getItem("orse_fecha_inicio"));

        dt = localStorage.getItem("orse_fecha_inicio").split("-");
        if(parseInt(dt[0])<=31){
            $("#fecha_inicio").val(dt[0]+"-"+dt[1]+"-"+dt[2]);
            console.log(dt[0]+"-"+dt[1]+"-"+dt[2]);
        } else {
            $("#fecha_inicio").val(dt[2]+"-"+dt[1]+"-"+dt[0]);
            console.log(dt[2]+"-"+dt[1]+"-"+dt[0]);
        }


        //window.osFiltros.usos_fecha_fin     = dt[2]+"-"+dt[1]+"-"+dtE
        //$("#fecha_inicio").val(localStorage.getItem("orse_fecha_inicio"));
    }

    if(localStorage.getItem("orse_fecha_fin")){
        console.log(localStorage.getItem("orse_fecha_fin"));
        
        dt = localStorage.getItem("orse_fecha_fin").split("-");
        if(parseInt(dt[0])<=31){
            $("#fecha_fin").val(dt[0]+"-"+dt[1]+"-"+dt[2]);
            console.log(dt[0]+"-"+dt[1]+"-"+dt[2]);
        } else {
            $("#fecha_fin").val(dt[2]+"-"+dt[1]+"-"+dt[0]);
            console.log(dt[2]+"-"+dt[1]+"-"+dt[0]);
        }

        //$("#fecha_fin").val(localStorage.getItem("orse_fecha_fin"));
    }

    if(localStorage.getItem("orse_estado")){
        console.log(localStorage.getItem("orse_estado"));
        $('.selectpicker').selectpicker('val', localStorage.getItem("orse_estado"));
    }

    if(localStorage.getItem("orse_tipo")){
        console.log(localStorage.getItem("orse_tipo"));
        $('#siom-form-os-bandeja select[name=orse_tipo]').selectpicker('val', localStorage.getItem("orse_tipo"));
    }

    if(localStorage.getItem("orse_responsable")){
        console.log(localStorage.getItem("orse_responsable"));
        $('#siom-form-os-bandeja select[name=orse_responsable]').selectpicker('val', localStorage.getItem("orse_responsable"));
    }

    if(localStorage.getItem("orse_id")){
        console.log(localStorage.getItem("orse_id"));
        $('#siom-form-os-bandeja input[name=orse_id]').selectpicker('val', localStorage.getItem("orse_id"));
    }

    if(localStorage.getItem("empl_nemonico")){
        console.log(localStorage.getItem("empl_nemonico"));
        $('#siom-form-os-bandeja input[name=empl_nemonico]').selectpicker('val', localStorage.getItem("empl_nemonico"));
    }

    if(localStorage.getItem("orse_empl_nombre")){
        console.log(localStorage.getItem("orse_empl_nombre"));
        $('#siom-form-os-bandeja input[name=empl_nombre]').selectpicker('val', localStorage.getItem("orse_empl_nombre"));
    }

    if(localStorage.getItem("orse_tecn_id")){
        console.log(localStorage.getItem("orse_tecn_id"));
        $('#siom-form-os-bandeja select[name=tecn_id]').selectpicker('val', localStorage.getItem("orse_tecn_id"));
    }

    if(localStorage.getItem("orse_empl_direccion")){
        console.log(localStorage.getItem("orse_empl_direccion"));
        $('#siom-form-os-bandeja input[name=empl_direccion]').selectpicker('val', localStorage.getItem("orse_empl_direccion"));
    }

    if(localStorage.getItem("orse_zona_id")){
        console.log(localStorage.getItem("orse_zona_id"));
        $('#siom-form-os-bandeja select[name=zona_id]').selectpicker('val', localStorage.getItem("orse_zona_id"));
    }

    if(localStorage.getItem("orse_regi_id")){
        console.log(localStorage.getItem("orse_regi_id"));
        $('#siom-form-os-bandeja select[name=regi_id]').selectpicker('val', localStorage.getItem("orse_regi_id"));
    }

    if(localStorage.getItem("orse_clus_id")){
        console.log(localStorage.getItem("orse_clus_id"));
        $('#siom-form-os-bandeja select[name=clus_id]').selectpicker('val', localStorage.getItem("orse_clus_id"));
    }


    if(localStorage.getItem("orse_usua_creador")){
        console.log(localStorage.getItem("orse_usua_creador"));
        $('#siom-form-os-bandeja select[name=usua_creador]').selectpicker('val', localStorage.getItem("orse_usua_creador"));
    }
    
    $("#orse_id").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^0-9]/gi,''));
    });

    $("#empl_nemonico").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^a-z0-9ñÑ°.-\s]/gi, ''));
    });    

    $("#empl_nombre").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^a-z0-9ñÑ°.-\s]/gi, ''));
    });    

    $("#empl_direccion").on('input', function() 
    {
        $(this).val($(this).val().replace(/[^a-z0-9ñÑ°.-\s]/gi, ''));
    });   
    
    $("#orse_id").attr('maxlength','11');
    $("#empl_nemonico").attr('maxlength','30');
    $("#empl_nombre").attr('maxlength','100');
    $("#empl_direccion").attr('maxlength','150');

})(jQuery);

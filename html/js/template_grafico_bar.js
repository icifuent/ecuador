(function($) {
    function drawCharts() {
        console.log('drawCharts');

        var grafico = $("#siom-grafico-bar");
        var data_json = grafico.data("data");
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Nombre'); // Nombre Leyenda
        data.addColumn('number', ''); 
        data.addColumn({type: 'string', role: 'style'});   
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role:"annotation"});  
        data.addColumn('number', ''); // Nombre Leyenda
        data.addColumn({type: 'string', role: 'style'});   
        data.addColumn({type: 'string', role: 'tooltip'});
        data.addColumn({type: 'string', role:"annotation"});
        data.addColumn('number', 'Umbral'); // Nombre Leyenda
        data.addColumn({type: 'string', role: 'style'});  
        data.addColumn({type: 'string', role: 'tooltip'}); 
  
        var color_aprobadas = '#00b050';
        var color_rechazadas = '#c00000';
        var color_umbral='#000000';
        var rows = [];

        for (var i = 0; i < data_json.length; i++) {
            console.log('total:');
            data_json[i]['aprobadas'];
            //Tasa Disponibilidad
            if( data_json[i]['rechazadas'] == null ){
                console.log("RECHAZADAS NULL");
                var color_total = '#00b050';
                if((data_json[i]['total']*100)>=10){
                    color_total = '#c00000';
                }
                
                rows.push(  [data_json[i]['nombre']
                            , Math.round(parseFloat((data_json[i]['total'])*100)*100)/100
                            , color_total
                            , Math.round(parseFloat((data_json[i]['total'])*100)*100)/100 +'%'
                            , Math.round(parseFloat((data_json[i]['total'])*100)*100)/100 +'%'
                            , null
                            , null
                            , null
                            , null
                            , data_json[i]['umbral']
                            , color_umbral
                            , 'Umbral:'  +data_json[i]['umbral'] + ' %']
                );
                console.log("Fin Push");
            } else {
                console.log(data_json[i]['aprobadas']);
                console.log(data_json[i]['rechazadas']);

                var aprobadas = Math.round(parseFloat((data_json[i]['aprobadas']/data_json[i]['total'])*100)*100)/100;
                var rechazadas = Math.round(parseFloat((data_json[i]['rechazadas']/data_json[i]['total'])*100)*100)/100;
                var numeroAprobadas = data_json[i]['total'];
                if(0==aprobadas){
                    textAprobadas=null;
                } else {
                    textAprobadas=Math.round(parseFloat((data_json[i]['aprobadas']/data_json[i]['total'])*100)*100)/100+"%";
                }

                if(0==rechazadas){
                    textRechazadas=null;
                } else {
                    textRechazadas=Math.round(parseFloat((data_json[i]['rechazadas']/data_json[i]['total'])*100)*100)/100 +"%";
                }

                if(0==data_json[i]['total']){
                    numeroAprobadas = 100;
                    color_aprobadas="#00b0f0";
					textAprobadas='100%';
                }else{
                    numeroAprobadas =Math.round(parseFloat((data_json[i]['aprobadas']/data_json[i]['total'])*100)*100)/100;
                    color_aprobadas = '#00b050';
                }

                //PRINCIPAL
                rows.push(  [data_json[i]['nombre']
                            , numeroAprobadas
                            , color_aprobadas
                            , data_json[i]['textoAprobadas'] + ': ' + data_json[i]['aprobadas']
                            , textAprobadas
                            , Math.round(parseFloat((data_json[i]['rechazadas']/data_json[i]['total'])*100)*100)/100
                            , color_rechazadas
                            , data_json[i]['textoRechazadas'] + ': ' + data_json[i]['rechazadas']
                            , textRechazadas
                            , data_json[i]['umbral']
                            , color_umbral
                            , 'Umbral:' +data_json[i]['umbral'] +'%']
                );
            }            

        };

        console.log(rows);
        data.addRows(rows);

        var options = {
                            title: '',
                            titleTextStyle: {fontSize: 10},
                            height: 300,
                            width: 900,
                            chartArea: {left:50,top:15,width:'95%',height:'80%'},
                            //chartArea: {left: 200, top: 0, width: '90%', height: '75%'},
                            vAxis: {minValue:0,maxValue: 100, gridlines: { count: 11 }
                                /*titl: '',
                                ticks: [0, 20, 40, 60, 80, 100]*/
                            },
                            hAxis: {
                                textStyle : {
                                                fontSize:8
                                },
                                format: 'Q#'
                            },
                            //bar: {groupWidth: '50%'},
                            bar: {groupWidth: '80%'},            
                            legend:{ position:'none' /*position: 'bottom',alignment: 'center', textStyle: {color: 'black', fontSize: 10}*/},
                            colors: [color_aprobadas, color_rechazadas, color_umbral],
                            isStacked: true,
                            seriesType: 'bars',
                            annotations: {
                                            textStyle: {
                                                            fontSize: 9
                                            }
                            },
                            series: {2: {type: 'line',lineWidth: 2 } },
        };
        var chart2 = new google.visualization.ComboChart(grafico[0]);
        chart2.draw(data, options);
    }

    if (typeof google.visualization != "undefined" &&
        typeof google.visualization.PieChart != "undefined" &&
        typeof google.visualization.ColumnChart != "undefined" &&
        typeof google.visualization.ComboChart != "undefined") {
                    drawCharts();
    } else {
        google.setOnLoadCallback(drawCharts);
    }

})(jQuery);
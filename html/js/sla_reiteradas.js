(function($) {
    //INICIALIZACION
    var filterTimer=null;
    var filterTimeout=500;
    
    $('.selectpicker').selectpicker();

    $('#orse_fecha_validacion_inicio,#orse_fecha_validacion_termino').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true
    });
    
    var date = new Date(), y = date.getFullYear(), m = date.getMonth();
    var firstDay = new Date(y, m - 1, 1);
    var lastDay = new Date(y, m + 1, 0);
    $('#orse_fecha_validacion_inicio').datepicker('setDate',firstDay);
    $('#orse_fecha_validacion_termino').datepicker('setDate',lastDay);


    //EVENTOS
    $('#siom-form-sla-reiteradas input[name=orse_id]').keyup(function () {
         FiltrarConDelay();
    });
    
    $('#orse_fecha_validacion_inicio,#orse_fecha_validacion_termino').change(function() {
        $("#siom-form-sla-reiteradas").submit();
    });

    $('#siom-form-sla-reiteradas select[name=espe_id]').change(function() {
        $("#siom-form-sla-reiteradas").submit();
    });

    $('#siom-form-sla-reiteradas select[name=espe_idu]').change(function() {
        $("#siom-form-sla-reiteradas").submit();
    });

    $('#siom-form-sla-reiteradas select[name=zona_id]').change(function() {
        $("#siom-form-sla-reiteradas").submit();
    });
	
    $('#siom-form-sla-reiteradas select[name=zona_tipo]').change(function() {
        $("#siom-form-sla-reiteradas").submit();
    });

    $('.BotonSLAReiteradasMenu').click(function(event) {
        event.preventDefault();
        //console.log('.BotonSLAReiteradasMenu click:' + $(this).data('tipo_datos'));
        $('.nav-tabs li').removeClass("active");
        $(this).parent().addClass("active");
        $('#tipo_datos').val($(this).data('tipo_datos'));

        $('#pagina').val("1");
        if ($(this).data('tipo_datos') == "validacion") {
            $('#os_id').show();
        } else {
            $('#os_id').hide();
        }
        
        if ( $(this).data('tipo_datos') === "validacion" /*|| $(this).data('tipo_datos') == "detalle"*/) {
            $('#FiltroZonaTipo').hide();
            $('#FiltroZona').show();
        } else {
            $('#FiltroZona').hide();
            $('#FiltroZonaTipo').show();
        }

        if ($(this).data('tipo_datos') === "grafico_especialidades") {
            $('#FiltroEspecialidades').hide();
            $('#FiltroEspecialidad').show();
        }
        else {
            $('#FiltroEspecialidad').hide();
            $('#FiltroEspecialidades').show();
        }

        $("#siom-form-sla-reiteradas").submit();
    })
    function FiltrarConDelay() {
        if (filterTimer) {
            clearTimeout(filterTimer);
        }
        filterTimer = setTimeout(function() {
            $("#siom-form-sla-reiteradas").submit();
        }, filterTimeout);
    }
})(jQuery);

(function($) {
  $('#pagination').bootpag().on("page", function(event, num){
	page = num;
	data = $.extend($('#pagination').data("filters"),{page:page});
	window.app.runRoute('post','#/core/contrato/lpu_grupo/filtro/'+page, data);
  });

})(jQuery);

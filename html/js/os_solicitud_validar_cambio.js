(function($) { 
  	$('.selectpicker').selectpicker();

 	window.scrollToElement($('#form_solicitud_cambio'));

 	$("#form_solicitud_cambio").submit(function(e){
    	solicitud = $(this).find(':input[name="orse_solicitud_cambio"]').val();
    	if(solicitud==""){
    		alert("Debe ingresar resolución de solicitud");
    		return false;
    	}
        
        if(solicitud=="APROBADA"){
            tipo = $(this).find(':input[name="orse_tipo"]').val();
            if(tipo==""){
                alert("Debe ingresar tipo");
                return false;
            }
        }
    	return true;
 	});
  
 })(jQuery);
<?php

//Helpers
Flight::map('ObtenerDetalleEmplazamiento', function($db,$cont_id,$empl_id){
    
    $usua_id = $_SESSION['user_id'];  

    $query = "SELECT
                    em.empl_id
                    ,em.comu_id
                    ,em.clas_id
                    ,em.cate_id
                    ,em.duto_id
                    ,em.empl_nemonico
                    ,em.empl_nombre
                    ,em.empl_direccion
                    ,em.empl_referencia
                    ,em.empl_tipo
                    ,em.empl_tipo_acceso
                    ,em.empl_nivel_criticidad
                    ,em.empl_espacio
                    ,em.empl_macrositio
                    ,em.empl_subtel
                    ,em.empl_distancia
                    ,em.empl_latitud
                    ,em.empl_longitud
                    ,em.empl_observacion
                    ,em.empl_id_emplazamiento_atix
                    ,em.empl_observacion_ingreso
                    ,em.empl_fecha_creacion
                    ,em.empl_estado
                    ,em.empl_subestado
                    ,em.usua_creador
                    ,em.riva_id
                    ,co.comu_nombre
                    ,r.regi_nombre
                    ,emcl.clas_nombre
                    ,emca.cate_nombre
                    ,emdt.duto_nombre
                    ,uc.usua_nombre AS usua_creador_nombre
                    ,zco.zona_nombre
                FROM
                    contrato c,
                    usuario uc,
                    rel_contrato_emplazamiento rce,
                    emplazamiento em,
                    comuna co,
                    region r,
                    provincia p,
                    emplazamiento_clasificacion emcl,
                    emplazamiento_categoria emca,
                    emplazamiento_dueno_torre emdt,
                    usuario u, rel_contrato_usuario rcu,
                    rel_contrato_usuario_alcance rcua , rel_zona_emplazamiento rzeal, zona zal,
                    zona zco, rel_zona_emplazamiento rzeco
                WHERE
                    c.cont_id=$cont_id
                    AND em.empl_id=$empl_id
                    AND rce.cont_id=c.cont_id
                    AND rce.empl_id=em.empl_id
                    AND em.comu_id=co.comu_id
                    AND p.prov_id = co.prov_id
                    AND r.regi_id = p.regi_id
                    AND em.clas_id=emcl.clas_id
                    AND em.cate_id=emca.cate_id                              
                    AND em.duto_id=emdt.duto_id
                    AND em.usua_creador=uc.usua_id

                AND rzeal.empl_id=em.empl_id
                AND rzeal.zona_id=zal.zona_id
                AND zal.zona_tipo='ALCANCE'

                AND u.usua_id = $usua_id
                AND rcu.usua_id = u.usua_id
                AND rcu.cont_id = c.cont_id
                AND rcua.recu_id = rcu.recu_id
                AND rcua.zona_id = rzeal.zona_id

                AND rzeco.empl_id = em.empl_id
                AND rzeco.zona_id = zco.zona_id
                AND zco.zona_tipo = 'CONTRATO'
        ";
        
        $res = $db->ExecuteQuery($query);
        if (0 == $res['status']){
            return $res;
        }
        
        if(0 < $res['rows']){
            $res_resp = $db->ExecuteQuery("SELECT
                                            usuario.usua_nombre,
                                            perfil.perf_nombre
                                            FROM 
                                            rel_zona_emplazamiento
                                            INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                                            INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id AND rel_contrato_usuario.cont_id=$cont_id)
                                            INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_estado='ACTIVO')
                                            INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                                            INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('GESTOR','DESPACHADOR'))
                                            WHERE
                                            rel_zona_emplazamiento.empl_id=$empl_id");

        if ($res_resp['status'] == 0){ return $res_resp;}

        $res['data'][0]['gestor_responsable'] = array();
        $res['data'][0]['despachador_responsable'] = array();
        foreach($res_resp['data'] AS $row){
            if($row['perf_nombre'] == "GESTOR"){
                array_push($res['data'][0]['gestor_responsable'],$row['usua_nombre']);
            }
            else{
                array_push($res['data'][0]['despachador_responsable'],$row['usua_nombre']);
            }
        }
        
        $res_tecn = $db->ExecuteQuery("SELECT tecn_nombre FROM tecnologia t, rel_emplazamiento_tecnologia ret WHERE ret.empl_id = $empl_id AND t.tecn_id = ret.tecn_id ");
        if ($res_tecn['status'] == 0){ return $res_tecn;}
        $tecn = array();
        foreach($res_tecn['data'] AS $row){
            $tecn[] = $row['tecn_nombre'];
        }
        $res['data'][0]['tecn_nombre'] = implode(",",$tecn);

        // ----
        $res_riva = $db->ExecuteQuery("SELECT  r.riva_id, r.riva_nombre FROM riesgo_vandalico r, emplazamiento em 
                                        WHERE em.empl_id = $empl_id 
                                        AND  r.riva_id = em.riva_id");
        if ($res_riva['status'] == 0){ return $res_riva;}
        $riva = array();
        foreach($res_riva['data'] AS $row){
            $riva[] = $row['riva_nombre'];
        }
        $res['data'][0]['riva_nombre'] = implode(",",$riva);
        // --
        
        // $res_riva = $db->ExecuteQuery("SELECT riva_id, riva_nombre FROM riesgo_vandalico");
        // if ($res_riva['status'] == 0){ return $res_riva;}
        // $riva = array();
        // foreach($res_riva['data'] AS $row){
        //     $riva[] = $row['riva_nombre'];
        // }
        // $res['data'][0]['riva_nombre'] = implode(",",$riva);

        /*ENCARGADO EMPLAZAMIENTO*/

        $res_resp= array();
        $res_resp = $db->ExecuteQuery("SELECT      
                                              (select u.usua_nombre
                                              from rel_zona_usuario rezu,
                                                usuario u,
                                                zona zo
                                                where rezu.zona_id = z.zona_id
                                                AND rezu.zona_id = zo.zona_id
                                                AND zo.zona_tipo = 'CLUSTER'
                                                AND zo.cont_id = rce.cont_id
                                                AND rezu.usua_id = u.usua_id
                                                group by zo.zona_id 
                                                order by u.usua_nombre DESC 
                                            ) AS usuario

                                            FROM rel_contrato_emplazamiento rce 
                                            , rel_zona_emplazamiento  rempl 
                                            ,zona z
                                            WHERE rce.cont_id = $cont_id
                                            AND rce.empl_id = $empl_id
                                            AND rempl.empl_id = rce.empl_id 
                                            AND z.zona_id = rempl.zona_id 
                                            AND z.zona_tipo = 'CLUSTER'
                                            AND z.cont_id = rce.cont_id
        ");

        if ($res_resp['status'] == 0){
            return $res_resp;
        }

        if($res_resp['rows']>0){
            $res['data'][0]['encargado_emplazamiento'] = $res_resp['data'][0]['usuario'];
        } else {
            $res['data'][0]['encargado_emplazamiento'] = "";
        }
    }

    return $res;
});

Flight::map('ObtenerDetalleEmplazamientoHistorico', function($db,$cont_id,$empl_id){
    
    $usua_id = $_SESSION['user_id'];  

    $query = "SELECT
                    em.emhi_id,
                    CONVERT_TZ(em.emhi_fecha,'UTC','America/Santiago') AS emhi_fecha,
                    em.usua_id,
                    em.emhi_tipo,
                    em.empl_id,
                    em.comu_id,
                    em.clas_id,
                    em.cate_id,
                    em.duto_id,
                    em.empl_nemonico,
                    em.empl_nombre,
                    em.empl_direccion,
                    em.empl_referencia,
                    em.empl_tipo,
                    em.empl_tipo_acceso,
                    em.empl_nivel_criticidad,
                    em.empl_espacio,
                    em.empl_macrositio,
                    em.empl_subtel,
                    em.empl_distancia,
                    em.empl_latitud,
                    em.empl_longitud,
                    em.empl_observacion,
                    em.empl_id_emplazamiento_atix,
                    em.empl_observacion_ingreso,
                    em.empl_fecha_creacion,
                    em.empl_estado,
                    em.empl_subestado,
                    em.usua_creador,
                    co.comu_nombre,
                    r.regi_nombre,
                    emcl.clas_nombre,
                    emca.cate_nombre,
                    emdt.duto_nombre,
                    uh.usua_nombre AS usua_historico,
                    uc.usua_nombre AS usua_creador
                    
                FROM
                    contrato c,
                    rel_contrato_emplazamiento rce,
                    emplazamiento_historico em,
                    comuna co,
                    region r,
                    provincia p,
                    emplazamiento_clasificacion emcl,
                    emplazamiento_categoria emca,
                    emplazamiento_dueno_torre emdt,
                    usuario uh, usuario uc
                WHERE
                    c.cont_id=$cont_id
                    AND em.empl_id=$empl_id
                    AND rce.cont_id=c.cont_id
                    AND rce.empl_id=em.empl_id
                    AND em.comu_id=co.comu_id
                    AND p.prov_id = co.prov_id
                    AND r.regi_id = p.regi_id
                    AND em.clas_id=emcl.clas_id
                    AND em.cate_id=emca.cate_id                              
                    AND em.duto_id=emdt.duto_id
                    
                    AND em.usua_creador=uc.usua_id
                    AND uh.usua_id = em.usua_id
                ;";
    
    $res = $db->ExecuteQuery($query);
    return $res;
});

Flight::route('GET|POST /core/emplazamiento/contrato/@cont_id:[0-9]+/filtros', function($cont_id)
{    
    $out = array();
    $dbo = new MySQL_Database();

    /*$res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=1 ORDER BY regi_orden ASC");*/
	$res = $dbo->ExecuteQuery("SELECT r.regi_id, r.regi_nombre FROM region r, contrato cn  WHERE cn.cont_id= " . $cont_id . "  AND cn.pais_id=r.pais_id  ORDER BY r.regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT prov_id, prov_nombre FROM provincia");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['provincias'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT comu_id, comu_nombre FROM comuna");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['comunas'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT tecn_id, tecn_nombre FROM tecnologia");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['tecnologias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO'");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['especialidad'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT sube_id, sube_nombre FROM subespecialidad WHERE sube_estado='ACTIVO'");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['subespecialidad'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT emca.cate_id,emca.cate_nombre FROM emplazamiento_categoria emca , rel_contrato_emplazamiento_categoria rcec
                                    WHERE emca.cate_id=rcec.cate_id
                                    AND emca.cate_estado='ACTIVO' 
                                    AND rcec.cont_id=" . $cont_id);
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['categorias'] = $res['data'];
    
    /*where cont_id=$cont_id*/
    $res = $dbo->ExecuteQuery("SELECT clas_id, clas_nombre FROM emplazamiento_clasificacion WHERE cont_id=" . $cont_id);
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['clasificaciones'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT edto.duto_id,edto.duto_nombre FROM emplazamiento_dueno_torre edto, rel_contrato_emplazamiento_dueno_torre rced  
                                WHERE 
                                    rced.duto_id = edto.duto_id
                                    AND rced.cont_id = " . $cont_id);
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['duenos_torres'] = $res['data'];

    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM emplazamiento WHERE Field = 'empl_tipo'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['tipos'] = explode("','", $matches[1]);    
    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM emplazamiento WHERE Field = 'empl_tipo_acceso'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['tipos_acceso'] = explode("','", $matches[1]);    

    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM emplazamiento WHERE Field = 'empl_nivel_criticidad'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['criticidades'] = explode("','", $matches[1]);
    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM emplazamiento WHERE Field = 'empl_espacio'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['espacios'] = explode("','", $matches[1]);   
    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM emplazamiento WHERE Field = 'empl_estado'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['estados'] = explode("','", $matches[1]);  
    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM emplazamiento WHERE Field = 'empl_subestado'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['subestados'] = explode("','", $matches[1]);  
    
    $res = $dbo->ExecuteQuery("SELECT riva_id, riva_nombre FROM riesgo_vandalico where cont_id = $cont_id");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['riesgo_vandalico'] = $res['data'];

    $out['status'] = 1;
    Flight::json($out);
});


Flight::route('GET|POST /core/emplazamiento/add', function($id){
    
    $dbo = new MySQL_Database();
    $dbo->startTransaction();
    $data = array_merge($_GET,$_POST);
    

    $tecnologias = null;
    $contratos = null;
    $zonas = null;
        
    if( isset($data['tecn_id']) && $data['tecn_id']!="" ){  
        $tecnologias = $data['tecn_id'];
        unset($data['tecn_id']);
    }
    
    if( isset($data['cont_id']) && $data['cont_id']!="" ){ 
        $contratos = $data['cont_id'];
        unset($data['cont_id']);
    }
    
    if( isset($data['zona_id']) && $data['zona_id']!="" ){  
        $zonas = $data['zona_id'];
        unset($data['zona_id']);
    }
    
    if( !isset($data['empl_fecha_creacion'])){  
        $data['empl_fecha_creacion'] = 'NOW()';
    }
    
    //VARIABLES EMPLAZAMIENTO
    $usua_creador = $data["usua_creador"];
    $cate_id = $data["cate_id"];
    $clas_id = $data["clas_id"];
    $duto_id = $data["duto_id"];
    $empl_nemonico = $data["empl_nemonico"];
    $empl_nombre = $data["empl_nombre"];
    $empl_direccion = $data["empl_direccion"];
    $comu_id = $data["comu_id"];
    $empl_tipo = $data["empl_tipo"];
    $empl_tipo_acceso = $data["empl_tipo_acceso"];
    $empl_nivel_criticidad = $data["empl_nivel_criticidad"];
    $empl_espacio = $data["empl_espacio"];
    $empl_macrositio = $data["empl_macrositio"];
    $empl_subtel = $data["empl_subtel"];
    $empl_distancia = $data["empl_distancia"];
    $empl_latitud = $data["empl_latitud"];
    $empl_longitud = $data["empl_longitud"];
    $empl_estado = $data["empl_estado"];
    $empl_observacion = $data["empl_observacion"];
    $empl_fecha_creacion = $data["empl_fecha_creacion"];
	
	if( !isset($data["riva_id"])){ 
        $riva_id = 'null';
    }else{
		$riva_id = $data["riva_id"];
	}
	 
    $emhi_tipo = 'CREATE';
	$empl_referencia = $data["empl_referencia"];
	$empl_id_emplazamiento_atix = $data["empl_id_emplazamiento_atix"];
	$empl_observacion_ingreso = $data["empl_observacion_ingreso"];
   //$usua_id = 12;

	if ($empl_id_emplazamiento_atix == NULL || $empl_id_emplazamiento_atix == ""){
		$empl_id_emplazamiento_atix = 0;
	}

    $query = "INSERT INTO emplazamiento (                           usua_creador,
                                                                    cate_id,
                                                                    clas_id,
                                                                    duto_id,
                                                                    empl_nemonico,
                                                                    empl_nombre,
                                                                    empl_direccion,
                                                                    comu_id,
                                                                    empl_tipo,
                                                                    empl_tipo_acceso,
                                                                    empl_nivel_criticidad,
                                                                    empl_espacio,
                                                                    empl_macrositio,
                                                                    empl_subtel,
                                                                    empl_distancia,
                                                                    empl_latitud,
                                                                    empl_longitud,
                                                                    empl_estado,
                                                                    empl_observacion,
                                                                    empl_fecha_creacion,
                                                                    riva_id,
																	empl_referencia,
																	empl_id_emplazamiento_atix,
																	empl_observacion_ingreso)
                                                                    
                                                                    
                                                            values( $usua_creador, 
                                                                    $cate_id,
                                                                    $clas_id, 
                                                                    $duto_id, 
                                                                    '$empl_nemonico', 
                                                                    '$empl_nombre', 
                                                                    '$empl_direccion', 
                                                                    $comu_id, 
                                                                    '$empl_tipo', 
                                                                    '$empl_tipo_acceso', 
                                                                    '$empl_nivel_criticidad',
                                                                    '$empl_espacio', 
                                                                    $empl_macrositio, 
                                                                    $empl_subtel, 
                                                                    $empl_distancia,  
                                                                    $empl_latitud, 
                                                                    $empl_longitud,
                                                                    '$empl_estado',
                                                                    '$empl_observacion', 
                                                                    $empl_fecha_creacion, 
                                                                    $riva_id,
																	'$empl_referencia',
																	'$empl_id_emplazamiento_atix',
																	'$empl_observacion_ingreso');";


    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }     
    //insert en historico
    $empl_id = $res['data'][0]["id"]; // Esto lo retorna el insert 
    $dbo->Commit();
	
	$dbo->startTransaction();
    $query_h = "INSERT INTO emplazamiento_historico (
                                emhi_fecha, 
                                usua_id, 
                                emhi_tipo,
                                empl_id, 
                                comu_id, 
                                clas_id, 
                                cate_id, 
                                duto_id, 
                                empl_nemonico, 
                                empl_nombre, 
                                empl_direccion, 
                                empl_referencia, 
                                empl_tipo, 
                                empl_tipo_acceso, 
                                empl_nivel_criticidad, 
                                empl_espacio, 
                                empl_macrositio, 
                                empl_subtel, 
                                empl_distancia, 
                                empl_latitud, 
                                empl_longitud, 
                                empl_observacion, 
                                empl_id_emplazamiento_atix, 
                                empl_observacion_ingreso, 
                                empl_fecha_creacion, 
                                empl_estado, 
                                empl_subestado, 
                                usua_creador,
                                riva_id) 
            SELECT              now(),
                                $usua_creador, 
                                'CREATE',
                                empl_id, /* ###### */
                                comu_id, 
                                clas_id, 
                                cate_id, 
                                duto_id, 
                                empl_nemonico, 
                                empl_nombre, 
                                empl_direccion, 
                                empl_referencia, 
                                empl_tipo, 
                                empl_tipo_acceso, 
                                empl_nivel_criticidad, 
                                empl_espacio, 
                                empl_macrositio, 
                                empl_subtel, 
                                empl_distancia, 
                                empl_latitud, 
                                empl_longitud, 
                                empl_observacion, 
                                empl_id_emplazamiento_atix, 
                                empl_observacion_ingreso, 
                                empl_fecha_creacion, 
                                empl_estado, 
                                empl_subestado, 
                                usua_creador,
                                $riva_id 
                                from emplazamiento
                                where empl_id = $empl_id;
    ";

  

//---------------------
#region
    // $query_h = "INSERT INTO emplazamiento_historico (
                                                                    
    //                                                                 empl_id,
    //                                                                 usua_id,
    //                                                                 emhi_tipo,
    //                                                                 emhi_fecha,
    //                                                                 usua_creador,
    //                                                                 cate_id,
    //                                                                 clas_id,
    //                                                                 duto_id,
    //                                                                 empl_nemonico,
    //                                                                 empl_nombre,
    //                                                                 empl_direccion,
    //                                                                 comu_id,
    //                                                                 empl_tipo,
    //                                                                 empl_tipo_acceso,
    //                                                                 empl_nivel_criticidad,
    //                                                                 empl_espacio,
    //                                                                 empl_macrositio,
    //                                                                 empl_subtel,
    //                                                                 empl_distancia,
    //                                                                 empl_latitud,
    //                                                                 empl_longitud,
    //                                                                 empl_estado,
    //                                                                 empl_observacion,
    //                                                                 empl_fecha_creacion,
    //                                                                 riva_id)
                                                                    
                                                                    
    //                                                             values( 
    //                                                                 $empl_id,
    //                                                                 $usua_creador,
    //                                                                 '$empl_tipo',
    //                                                                 $empl_fecha_creacion,
    //                                                                 $usua_creador, 
    //                                                                 $cate_id,
    //                                                                 $clas_id, 
    //                                                                 $duto_id, 
    //                                                                 '$empl_nemonico', 
    //                                                                 '$empl_nombre', 
    //                                                                 '$empl_direccion', 
    //                                                                 $comu_id, 
    //                                                                 '$empl_tipo', 
    //                                                                 '$empl_tipo_acceso', 
    //                                                                 '$empl_nivel_criticidad',
    //                                                                 '$empl_espacio', 
    //                                                                 $empl_macrositio, 
    //                                                                 $empl_subtel, 
    //                                                                 $empl_distancia,  
    //                                                                 $empl_latitud, 
    //                                                                 $empl_longitud,
    //                                                                 '$empl_estado',
    //                                                                 '$empl_observacion', 
    //                                                                 $empl_fecha_creacion, 
    //                                                                 $riva_id);";
#endregion

        $res2 = $dbo->ExecuteQuery($query_h);
        if ($res2['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res2['error']));
        return;
        }     


        /*
    $id = $res["data"][0]["id"];
          */

    if( $contratos!=null ){
        if( !is_array($contratos) )  $contratos = array($contratos);                    
        foreach ($contratos as $contrato) {           
            $res_cont = $dbo->ExecuteQuery("INSERT INTO rel_contrato_emplazamiento (empl_id,cont_id,rece_mpp) VALUES ($empl_id,".$contrato["cont_id"].",".$contrato["rece_mpp"].")");
            if ($res_cont['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res_cont['error']));
                return;
            }
        }
    }
    
    
    if( $tecnologias!=null ){
        if( !is_array($tecnologias) )  $tecnologias = array($tecnologias);       
        foreach ($tecnologias as $tecn_id) {           
            $res_tecn = $dbo->ExecuteQuery("INSERT INTO rel_emplazamiento_tecnologia (empl_id,tecn_id) VALUES ($empl_id, $tecn_id)");
            if ($res_tecn['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res_tecn['error']));
                return;
            }
        }
    }
    
    
    if( $zonas!=null ){
        if( !is_array($zonas) )  $zonas = array($zonas);       
        foreach ($zonas as $zona_id) {              
            $res_zona = $dbo->ExecuteQuery("INSERT INTO rel_zona_emplazamiento (empl_id,zona_id) VALUES ($empl_id,$zona_id)");
            if ($res_zona['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res_zona['error']));
                return;
            }
        }
    }
    
    $dbo->Commit();
    Flight::json($res);
});



Flight::route('GET|POST /core/emplazamiento/upd/@id:[0-9]+', function($id){
    
    $dbo = new MySQL_Database();
    $dbo->startTransaction();
    $data = array_merge($_GET,$_POST);
    $tecnologias = null;
    $contratos = null;
    $zonas = null;
	$empl_distancia = 200;
	$empl_latitud = 0;
	$empl_longitud = 0;
	$empl_direccion = "";
    
	
	if( isset($data['empl_distancia']) && $data['empl_distancia']!="" ){  
		$empl_distancia = $data['empl_distancia'];
	}
	if( isset($data['empl_latitud']) && $data['empl_latitud']!="" ){ 
		$empl_latitud = $data['empl_latitud'];	
	}
	if( isset($data['empl_longitud']) && $data['empl_longitud']!="" ){ 
		$empl_longitud = $data['empl_longitud'];
	}
	if( isset($data['empl_direccion']) && $data['empl_direccion']!="" ){  
		$empl_direccion = $data['empl_direccion'];
	}
	
    if( isset($data['tecn_id']) && $data['tecn_id']!="" ){  
        $res_tecn = $dbo->ExecuteQuery("DELETE FROM rel_emplazamiento_tecnologia WHERE empl_id=$id");
        if( $res_tecn['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res_tecn['error']));
            return;
        }
        $tecnologias = $data['tecn_id'];
        unset($data['tecn_id']);
    }
    
    if( isset($data['zona_id']) && $data['zona_id']!="" ){  
        $res_zona = $dbo->ExecuteQuery("DELETE FROM rel_zona_emplazamiento WHERE empl_id=$id");
        if( $res_zona['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res_zona['error']));
            return;
        }
        $zonas = $data['zona_id'];
        unset($data['zona_id']);
    }
	
    /*if( isset($data['cont_id']) && $data['cont_id']!="" ){  
        $res_cont = $dbo->ExecuteQuery("DELETE FROM rel_contrato_emplazamiento WHERE empl_id=$id");
        if( $res_cont['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res_cont['error']));
            return;
        }       
        $contratos = $data['cont_id'];
        unset($data['cont_id']);
    }*/
	
	if( isset($data['cont_id']) && $data['cont_id']!="" ){  
        $res_cont = $dbo->ExecuteQuery("INSERT FROM rel_contrato_emplazamiento WHERE empl_id=$id");
        if( $res_cont['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res_cont['error']));
            return;
        }       
        $contratos = $data['cont_id'];
        unset($data['cont_id']);
    }
    
    if( isset($data['clas_id']) && $data['clas_id']!="" ){         
        //Verificamos si existe un cambio de clase de emplazamiento
        $res_clas = $dbo->ExecuteQuery("SELECT clas_id FROM emplazamiento WHERE empl_id=$id");
        if( $res_clas['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res_clas['error']));
            return;
        }        
        $clas_id = $res_clas["data"][0]["clas_id"];

        //Si la clase es distinta, eliminamos los mantenimientos periodicos del 
        //emplazamiento cuya fecha es posterior a la actual y sus tareas
        if( $clas_id != $data['clas_id']){

            //Eliminamos tareas
            $query = "UPDATE tarea SET tare_estado='ANULADA' 
                        WHERE 
                            tare_modulo='MNT' 
                        AND tare_id_relacionado IN (
                        SELECT mant_id FROM mantenimiento 
                        WHERE 
                        empl_id=$id 
                        AND mant_estado='CREADA' 
                        AND mant_fecha_programada >= NOW()
                );";
            $res_clas = $dbo->ExecuteQuery($query);
            if ($res_clas['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res_clas['error']));
                return;
            }

            //Enviamos eventos de anulacion
            $usua_id = 1;
            if (isset($_SESSION['user_id'])) {
                $usua_id = $_SESSION['user_id'];
            }
            $query = "INSERT INTO evento (even_modulo, even_evento, even_id_relacionado, even_fecha, even_estado, even_datos, usua_creador) 
                            SELECT 'MNT','ANULADA', mant_id, NOW(), 'DESPACHADO', '{\"razon\":\"Cambio de categoria Emplazamiento\"}', $usua_id
                            FROM mantenimiento 
                            WHERE 
                            empl_id=$id 
                            AND mant_estado='CREADA' 
                            AND mant_fecha_programada >= NOW()
                            ;";
            $res_clas = $dbo->ExecuteQuery($query);
            if ($res_clas['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res_clas['error']));
                return;
            }

            //Eliminamos mantenimientos
            $query = "UPDATE mantenimiento 
                        SET 
                            mant_estado='ANULADA' 
                            ,mant_descripcion='Cambio de categoria Emplazamiento'
                        WHERE 
                            empl_id=$id 
                            AND mant_estado='CREADA' 
                            AND mant_fecha_programada >= NOW()
                        ;";
            $res_clas = $dbo->ExecuteQuery($query);
            if ($res_clas['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res_clas['error']));
                return;
            }            

        }
		$dbo->Commit();
    }
    
    if( isset($data['empl_estado']) && $data['empl_estado']!="" && $data['empl_estado']=='NOACTIVO'){   
        //Si el estado es NOACTIVO y existen mantenimientos u ordenes de servicio
        //en proceso, entonces no se puede actualizar 
        $query = "SELECT count(mant_id) as mant_proceso FROM mantenimiento 
            WHERE empl_id=$id AND mant_estado NOT IN ('CREADA', 'APROBADA','RECHAZADA','FINALIZADA','ANULADA','NO REALIZADO');";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0 || count($res_estado["data"]) == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }                        
        $mant_proceso = $res_estado["data"][0]["mant_proceso"];

        $query = "SELECT count(orse_id) as orse_proceso FROM orden_servicio 
            WHERE empl_id=$id AND orse_estado NOT IN ('CREADA', 'APROBADA','RECHAZADA','FINALIZADA','ANULADA','NOACTIVO');";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0 || count($res_estado["data"]) == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }
        $orse_proceso = $res_estado["data"][0]["orse_proceso"];                    

        if ($orse_proceso > 0 || $mant_proceso > 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => "No se puede cambiar estado del emplazamiento a NO ACTIVO ya que existen $orse_proceso ordenes de servicio y $mant_proceso mantenimientos en proceso. Cierrelos e intente nuevamente."));
            return;
        }

        //actualizamos todas las tablas relacionadas
        //emplazamiento_visita
        $query = "UPDATE emplazamiento_visita SET emvi_estado = 'NOACTIVO' WHERE empl_id=$id;";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }

        //tarea
        $query = "UPDATE tarea SET tare_estado='ANULADA' 
                  WHERE 
                        tare_modulo='MNT' 
                        AND tare_id_relacionado IN (
                                SELECT mant_id FROM mantenimiento 
                                WHERE empl_id=$id AND mant_estado = 'CREADA' );";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }

        $query = "UPDATE tarea SET tare_estado='ANULADA' 
                  WHERE 
                        tare_modulo='OS' 
                        AND tare_id_relacionado IN (
                                SELECT orse_id FROM orden_servicio 
                                WHERE empl_id=$id AND orse_estado = 'CREADA' );";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }

        //notificacion
        $query = "UPDATE notificacion SET noti_estado='ENTREGADA' 
                 WHERE 
                        noti_modulo='MNT'
                        AND noti_estado='DESPACHADA'
                        AND noti_id_relacionado IN (
                                SELECT mant_id FROM mantenimiento 
                                WHERE empl_id=$id AND mant_estado = 'CREADA' );";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }

        $query = "UPDATE notificacion SET noti_estado='ENTREGADA' 
                 WHERE 
                        noti_modulo='OS'
                        AND noti_estado='DESPACHADA'
                        AND noti_id_relacionado IN (
                                SELECT orse_id FROM orden_servicio 
                                WHERE empl_id=$id AND orse_estado = 'CREADA' );";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }


        //evento
        $usua_id = 1;
        if (isset($_SESSION['user_id'])) {
            $usua_id = $_SESSION['user_id'];
        }
        //  eliminamos los eventos no atendidos
        $query = "UPDATE evento SET even_estado='ATENDIDO' 
                 WHERE 
                        even_modulo='MNT'
                        AND even_estado='DESPACHADO'
                        AND even_id_relacionado IN (
                                SELECT mant_id FROM mantenimiento 
                                WHERE empl_id=$id AND mant_estado = 'CREADA' );";        
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }
        
        $query = "UPDATE evento SET even_estado='ATENDIDO' 
                 WHERE 
                        even_modulo='OS'
                        AND even_estado='DESPACHADO'
                        AND even_id_relacionado IN (
                                SELECT orse_id FROM orden_servicio 
                                WHERE empl_id=$id AND orse_estado = 'CREADA' );";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }    

        //  insertamos los eventos de anulacion
        $query = "INSERT INTO evento (even_modulo, even_evento, even_id_relacionado, even_fecha, even_estado, even_datos, usua_creador) 
                SELECT 'MNT','ANULADA', mant_id, NOW(), 'DESPACHADO', '{\"razon\":\"Emplazamiento puesto en estado NO ACTIVO\"}', $usua_id
                FROM mantenimiento 
                WHERE empl_id=$id AND mant_estado = 'CREADA';";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }

        $query = "INSERT INTO evento (even_modulo, even_evento, even_id_relacionado, even_fecha, even_estado, even_datos, usua_creador) 
                SELECT 'OS','ANULADA', orse_id, NOW(), 'DESPACHADO', '{\"razon\":\"Emplazamiento puesto en estado NO ACTIVO\"}', $usua_id
                FROM orden_servicio 
                WHERE empl_id=$id AND orse_estado = 'CREADA' ;";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }            

        //mantenimiento
        $query = "UPDATE mantenimiento SET mant_estado = 'ANULADA', mant_descripcion = 'Emplazamiento puesto en estado NOACTIVO' WHERE empl_id=$id  AND mant_estado = 'CREADA' ;";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        }

        //orden_servicio
        $query = "UPDATE orden_servicio SET orse_estado = 'ANULADA', orse_descripcion = 'Emplazamiento puesto en estado NOACTIVO' WHERE empl_id=$id AND orse_estado = 'CREADA' ;";
        $res_estado = $dbo->ExecuteQuery($query);
        if ($res_estado['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_estado['error']));
            return;
        } 

        
    }
	
    if( count($data) > 0 ){

        
       $usua_id = $_SESSION['user_id']; 
       $query="INSERT INTO emplazamiento_historico (SELECT 0,now(),".$usua_id.",'UPDATE', e.* FROM emplazamiento e WHERE e.empl_id=".$id.")";
         
        $res = $dbo->ExecuteQuery($query);
        if ($res['status'] == 0) {   
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        $query = "UPDATE emplazamiento SET ".Flight::dataToUpdateString($data)." WHERE empl_id =".$id; 
        $res = $dbo->ExecuteQuery($query);
        if ($res['status'] == 0) {   
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
		
        //Codigo que realizara la actualizacion masiva del emplazamiento para cubrir las direcc repetidas por cada 
        //emplazamiento en contrato MTTO.
        $query ="SELECT cont_id FROM rel_contrato_emplazamiento WHERE empl_id =".$id;
        $res = $dbo->ExecuteQuery($query);
        if ($res['status'] == 0) {   
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }else {
			if ($res['rows'] > 0) {
				$cont_id = $res["data"][0]["cont_id"];
                /* SOLO SE REALIZA ACTUALIZACION PARA CONTRATO MTTO */
				if(18 == $cont_id)  {
				    $query = "UPDATE emplazamiento e
				    		  JOIN rel_contrato_emplazamiento r on e.empl_direccion = '$empl_direccion'
				    			AND e.empl_id = r.empl_id
				    			AND r.cont_id =$cont_id
				    		SET
				    			e.empl_distancia = '$empl_distancia'
				    			  , e.empl_latitud = $empl_latitud
				    			  , e.empl_longitud = $empl_longitud";
				    $res = $dbo->ExecuteQuery($query);
				    if ($res['status'] == 0) {   
				    	$dbo->Rollback();
				    	Flight::json(array("status" => 0, "error" => $res['error']));
				    	return;
				    }
                }
			}
		}
		
        
		
		
    }
    
    if( $contratos!=null ){
        if( !is_numeric($contratos) || intval($contratos) > 0  ){
            if (!is_array($contratos)) $contratos = array($contratos);
            foreach ($contratos as $contrato) {
                $res_cont = $dbo->ExecuteQuery("INSERT INTO rel_contrato_emplazamiento (empl_id,cont_id,rece_mpp) VALUES ($id," . $contrato["cont_id"] . "," . $contrato["rece_mpp"] . ")");
                if ($res_cont['status'] == 0) {
                    $dbo->Rollback();
                    Flight::json(array("status" => 0, "error" => $res_cont['error']));
                    return;
                }
            }
        }
        
        //Borramos las zonas del emplazamiento que quedan sin contrato
        $query = "DELETE FROM rel_zona_emplazamiento WHERE empl_id=$id AND zona_id IN (
                  SELECT z.zona_id FROM zona z 
                  WHERE z.cont_id NOT IN (SELECT cont_id FROM rel_contrato_emplazamiento WHERE empl_id=$id));";
        $res_cont = $dbo->ExecuteQuery($query);
        if ($res_cont['status'] == 0) {
            $dbo->Rollback();
            Flight::json(array("status" => 0, "error" => $res_cont['error']));
            return;
        }
    }

    if( $tecnologias!=null ){
        if( !is_numeric($tecnologias) || intval($tecnologias) > 0  ){
            if( !is_array($tecnologias) )  $tecnologias = array($tecnologias);
            foreach ($tecnologias as $tecn_id) {              
                $res_tecn = $dbo->ExecuteQuery("INSERT INTO rel_emplazamiento_tecnologia (empl_id,tecn_id) VALUES ($id,$tecn_id)");
                if ($res_tecn['status'] == 0) {
                    $dbo->Rollback();
                    Flight::json(array("status" => 0, "error" => $res_tecn['error']));
                    return;
                }
            }
        }
    }
               
    if( $zonas!=null ){
        if( !is_numeric($zonas) || intval($zonas) > 0  ){
            if( !is_array($zonas) )  $zonas = array($zonas); 
            foreach ($zonas as $zona_id) {              
                $res_zona = $dbo->ExecuteQuery("INSERT INTO rel_zona_emplazamiento (empl_id,zona_id) VALUES ($id,$zona_id)");
                if ($res_zona['status'] == 0) {
                    $dbo->Rollback();
                    Flight::json(array("status" => 0, "error" => $res_zona['error']));
                    return;
                }
            }
        }
    }

    $dbo->Commit();

    Flight::json(array("status"=>1,"rows"=>1,"data"=>array(array("status"=>1,"id"=>0)), "error"=>"" ));
});

Flight::route('GET|POST /core/emplazamiento/@id:[0-9]+/contrato/list(/@page:[0-9]+)', function($id, $page){
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("emplazamiento", "contrato"), $filtros_ini);
    
    $dbo = new MySQL_Database();    
    $query = "SELECT c.*, rece_mpp
                FROM emplazamiento e, contrato c, rel_contrato_emplazamiento rce
                WHERE 
                    e.empl_id = ".$id."  
                    AND rce.empl_id = e.empl_id 
                    AND rce.cont_id = c.cont_id 
                    AND $filtros ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))
            ;
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if (!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total'] / $results_by_page);
    }
    Flight::json($res);
});


Flight::route('GET|POST /core/emplazamiento/@id:[0-9]+/contrato/@idc:[0-9]+/zona/list(/@page:[0-9]+)', function($id, $idc, $page){
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("emplazamiento", "contrato"), $filtros_ini);
    
    $dbo = new MySQL_Database();    
    $query = "SELECT z.* 
                FROM emplazamiento e, zona z, rel_zona_emplazamiento rze
                WHERE 
                    e.empl_id = ".$id."  
                    AND z.cont_id = ".$idc."  
                    AND rze.empl_id = e.empl_id 
                    AND rze.zona_id = z.zona_id 
                    AND $filtros ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))
            ;
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if (!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total'] / $results_by_page);
    }
    Flight::json($res);
});


Flight::route('GET /core/emplazamiento/@empl_id:[0-9]+/contrato/@cont_id:[0-9]+/mantenimiento', function($empl_id, $cont_id){
	$out = array("status"=>true);
	$data = array();
    
	$dbo = new MySQL_Database();    
    
    $res = $dbo->ExecuteQuery("SELECT 
								clasificacion_programacion.clpr_id,
								clpr_nombre
                            FROM clasificacion_programacion
                            WHERE clasificacion_programacion.cont_id=$cont_id AND clpr_activo='ACTIVO'");
    if ($res['status'] == 0){
		Flight::json(array("status" => 0, "error" => $res['error']));
	}
	$data['clasificacion_programacion'] = $res['data'];
	
	
	$res = $dbo->ExecuteQuery(  "SELECT 
                                    rece_mpp,
                                    clpr_id
                                FROM rel_contrato_emplazamiento
                                WHERE cont_id=$cont_id AND empl_id=$empl_id");
    if ($res['status'] == 0){
		Flight::json(array("status" => 0, "error" => $res['error']));
	}
	
	if(0<count($res['data'])){
		$data['contrato_emplazamiento'] = $res['data'][0];
	}
	
	$out['data'] = $data;
    Flight::json($out);
});


Flight::route('POST /core/emplazamiento/@empl_id:[0-9]+/contrato/@cont_id:[0-9]+/mantenimiento_zonas', function($empl_id, $cont_id){
	$out = array("status"=>true);
	$data = $_POST;
    
	$dbo = new MySQL_Database();  
	$dbo->startTransaction();
	
	if($data['clpr_id']==""){
		$query = "UPDATE rel_contrato_emplazamiento SET
					  rece_mpp='".$data['rece_mpp']."',
					  clpr_id=NULL
					WHERE cont_id=$cont_id AND empl_id=$empl_id";
	}
	else{
		$query = "UPDATE rel_contrato_emplazamiento SET
					  rece_mpp='".$data['rece_mpp']."',
					  clpr_id='".$data['clpr_id']."'
					WHERE cont_id=$cont_id AND empl_id=$empl_id";
	}
	
	
	$res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0){
		$dbo->Rollback();
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}
	
	
	$res = $dbo->ExecuteQuery("DELETE rel_zona_emplazamiento
								FROM rel_zona_emplazamiento
								INNER JOIN zona ON (rel_zona_emplazamiento.zona_id=zona.zona_id)
								WHERE zona.cont_id=$cont_id AND rel_zona_emplazamiento.empl_id=$empl_id");
    if ($res['status'] == 0){
		$dbo->Rollback();
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}
	
	if(isset($data['zonas'])){
		$query = "INSERT INTO rel_zona_emplazamiento (zona_id,empl_id) VALUES ";
		for($i=0;$i<count($data['zonas']);$i++){
			$zona_id = $data['zonas'][$i];
			$query .= "($zona_id,$empl_id)";
			if($i<count($data['zonas'])-1){
				$query .= ",";
			}
		}
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0){
			$dbo->Rollback();
			Flight::json(array("status" => 0, "error" => $res['error']));
			return;
		}
	}
	$dbo->Commit();
	
    Flight::json($res);
});


/* Listado de core_emplazamiento_core*/
Flight::route('POST /core/emplazamiento/@cont_id:[0-9]+/list(/@page:[0-9]+)', function($cont_id, $page)
{     
    $results_by_page = Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";
        
    //Sacamos los filtros especiales del arreglo de filtros
    if( isset($filtros_ini['tecn_id']) ){
        if(is_array($filtros_ini['tecn_id'])){
          $filtros .= " AND t.tecn_id IN ('".implode("','",$filtros_ini['tecn_id'])."')";
        }
        else{
          $filtros .= " AND t.tecn_id=".$filtros_ini['tecn_id']; 
        }
        
        unset( $filtros_ini['tecn_id'] );
    } 
    
    if( isset($filtros_ini['comu_id']) ){
        if( strlen(trim($filtros_ini['comu_id']))>0 ){
            $filtros .= " AND co.comu_id=".$filtros_ini['comu_id']." ";
        }
        unset( $filtros_ini['comu_id'] );            
    }
    
    if( isset($filtros_ini['regi_id']) ){
        if( strlen(trim($filtros_ini['regi_id']))>0 ){
            $filtros .= " AND r.regi_id=".$filtros_ini['regi_id']." ";
        }
        unset( $filtros_ini['regi_id'] );            
    }

     if( isset($filtros_ini['clas_id']) ){
        if( strlen(trim($filtros_ini['clas_id']))>0 ){
            $filtros .= " AND em.clas_id=".$filtros_ini['clas_id']." ";
        }
        unset( $filtros_ini['clas_id'] );            
    }

     if( isset($filtros_ini['duto_id']) ){
        if( strlen(trim($filtros_ini['duto_id']))>0 ){
            $filtros .= " AND em.duto_id=".$filtros_ini['duto_id']." ";
        }
        unset( $filtros_ini['duto_id'] );            
    }

    // prueba
    if( isset($filtros_ini['cate_id']) ){
        if(is_array($filtros_ini['cate_id'])){
            $filtros .= " AND emca.cate_id in (".implode(",",$filtros_ini['cate_id']).")";
        } else {
            if( strlen(trim($filtros_ini['cate_id']))>0 ){
                $filtros .= " AND emca.cate_id=".$filtros_ini['cate_id'];    
            }
        }
        unset( $filtros_ini['cate_id'] );
    }

    //Obtenemos el resto de filtros
    $filtros = Flight::filtersToWhereString( array("emplazamiento"), $filtros_ini).$filtros;
    
    
    $query = "SELECT "
                . "SQL_CALC_FOUND_ROWS "
                . "em.empl_id, "
                . "em.comu_id, "
                . "em.clas_id, "
                . "em.cate_id, "
                . "em.duto_id, "
                . "em.empl_nemonico, "
                . "em.empl_nombre, "
                . "em.empl_direccion, "
                . "em.empl_referencia, "
                . "em.empl_tipo, "
                . "em.empl_tipo_acceso, "
                . "em.empl_nivel_criticidad, "
                . "em.empl_espacio, "
                . "em.empl_macrositio, "
                . "em.empl_subtel, "
                . "em.empl_distancia, "
                . "em.empl_latitud, "
                . "em.empl_longitud, "
                . "em.empl_observacion, "
                . "em.empl_id_emplazamiento_atix, "
                . "em.empl_observacion_ingreso, "
                . "em.empl_fecha_creacion, "
                . "em.empl_estado, "
                . "em.empl_subestado, "
                . "em.usua_creador, "

            

                . "co.comu_nombre, "
                . "emcl.clas_nombre, "
                . "emca.cate_nombre, "
                . "emdt.duto_nombre, "
                . "uc.usua_nombre AS usua_creador_nombre, "
                . "GROUP_CONCAT(t.tecn_nombre) as tecn_nombre, "
                . "GROUP_CONCAT(t.tecn_id) as tecn_id, "
                . "c.cont_nombre "
            . "FROM "

              

                . "emplazamiento em, "
                . "usuario uc, "            
                . "comuna co, "
                . "region r,"
                . "provincia p, "
                . "emplazamiento_clasificacion emcl, "
                . "emplazamiento_categoria emca, "
                . "emplazamiento_dueno_torre emdt, "
                . "tecnologia t, "
                . "rel_emplazamiento_tecnologia ret, "
                . "rel_contrato_emplazamiento rce, "
                . "contrato c "
            . "WHERE "
                . "c.cont_id = " .$cont_id ." " 
                . " AND em.comu_id=co.comu_id "                
                . "AND p.prov_id = co.prov_id "
                . "AND r.regi_id = p.regi_id "
                . "AND em.clas_id=emcl.clas_id "
                . "AND em.cate_id=emca.cate_id "                              
                . "AND em.duto_id=emdt.duto_id "
                . "AND em.usua_creador=uc.usua_id "
                . "AND ret.empl_id=em.empl_id "
                . "AND ret.tecn_id=t.tecn_id " 
                . "AND rce.empl_id=em.empl_id " 
                . "AND rce.cont_id=c.cont_id "

                  

                . "AND ".$filtros." "
            . "GROUP BY em.empl_id " 
            . ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
               /* . "riva.riva_id, "*/
             /* . "riesgo_vandalico_riva, "*/
     /*. "AND em.empl_id=riva.riva_id "*/  
    $dbo = new MySQL_Database();
    //Flight::json($query);
    $res = $dbo->ExecuteQuery($query);    
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page) && $res['status']==1) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }
    $res['filtros'] = $filtros_ini;   

    /*
    foreach($res['data'] as &$e) {
      $e['tecn_id'] = explode(',',$e['tecn_id']);
      $e['tecn_nombre'] = explode(',',$e['tecn_nombre']);
    }
    */
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/emplazamiento/filtros', function($cont_id)
{    
    $out = array();
    $dbo = new MySQL_Database();

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CONTRATO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zonas'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CLUSTER'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['clusters'] = $res['data'];

    /* RECORDAR CAMBIAR COMO SE OBTIENE EL PAIS*/
    /*$res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=1 ORDER BY regi_orden ASC");*/
	$res = $dbo->ExecuteQuery("SELECT r.regi_id, r.regi_nombre FROM region r, contrato cn  WHERE cn.cont_id= " . $cont_id . "  AND cn.pais_id=r.pais_id  ORDER BY r.regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT tecn_id, tecn_nombre FROM tecnologia");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['tecnologias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT e.empr_id, e.empr_nombre "
                            . "FROM empresa e, rel_contrato_empresa rce "
                            . "WHERE "
                                . "rce.cont_id=" . $cont_id . " "
                                //. "AND rce.coem_tipo='CONTRATISTA' "
                                . "AND e.empr_estado='ACTIVO' "
                                . "AND e.empr_id=rce.empr_id"
    );
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['empresa'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO'");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['especialidad'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT * FROM subespecialidad WHERE sube_estado='ACTIVO'");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['subespecialidad'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT ec.cate_id,ec.cate_nombre 
										FROM emplazamiento_categoria ec
										INNER JOIN rel_contrato_emplazamiento_categoria rcec on (rcec.cate_id=ec.cate_id)
										WHERE ec.cate_estado='ACTIVO' 
										AND rcec.cont_id=" . $cont_id);
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['categorias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT   riva_id, 
                                        riva_nombre
                                FROM    riesgo_vandalico
                                WHERE   cont_id = ". $cont_id );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['riesgo'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT edto.duto_id,edto.duto_nombre FROM emplazamiento_dueno_torre edto, rel_contrato_emplazamiento_dueno_torre rced  
                                WHERE 
                                    rced.duto_id = edto.duto_id
                                    AND rced.cont_id = " . $cont_id);
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['duenos_torres'] = $res['data'];

    $out['status'] = 1;
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/emplazamiento/list(/@page:[0-9]+)', function($cont_id, $page)
{    
    //session_start();
    $out     = array("status"=>1);
    $usua_id = $_SESSION['user_id'];   
    
    $results_by_page = Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";
    $filtros_zonas_contrato_flag = FALSE;
    $filtros_zonas_cluster_flag = FALSE;

    $out['filtros'] = $filtros_ini;  
        
    //Sacamos los filtros especiales del arreglo de filtros
    if( isset($filtros_ini['zona_id']) ){
        if( strlen(trim($filtros_ini['zona_id']))>0 ){
            $filtros .= " AND rzeco.zona_id=".$filtros_ini['zona_id']." AND rzeco.empl_id=em.empl_id AND rzeco.zona_id=zco.zona_id AND zco.zona_tipo='CONTRATO' ";
            $filtros_zonas_contrato_flag = TRUE;
        }
        unset( $filtros_ini['zona_id'] );
    }
    
    if( isset($filtros_ini['clus_id']) ){
        if( strlen(trim($filtros_ini['clus_id']))>0 ){
            $filtros .= " AND rzecl.zona_id=".$filtros_ini['clus_id']."  AND rzecl.empl_id=em.empl_id AND rzecl.zona_id=zcl.zona_id AND  zcl.zona_tipo='CLUSTER' ";
            $filtros_zonas_cluster_flag = TRUE;
        }
        unset( $filtros_ini['clus_id'] );
    }   
    
    if( isset($filtros_ini['tecn_id']) ){
        if(is_array($filtros_ini['tecn_id'])){
          $filtros .= " AND t.tecn_id IN ('".implode("','",$filtros_ini['tecn_id'])."')";
        }
        else{
          $filtros .= " AND t.tecn_id=".$filtros_ini['tecn_id']; 
        }
        
        unset( $filtros_ini['tecn_id'] );
    } 
    
    if( isset($filtros_ini['regi_id']) ){
        if( strlen(trim($filtros_ini['regi_id']))>0 ){
            $filtros .= " AND r.regi_id=".$filtros_ini['regi_id']." ";
        }
        unset( $filtros_ini['regi_id'] );            
    }

    if( isset($filtros_ini['cate_id']) ){
        if(is_array($filtros_ini['cate_id'])){
            $filtros .= " AND emca.cate_id in (".implode(",",$filtros_ini['cate_id']).")";
        } else {
            if( strlen(trim($filtros_ini['cate_id']))>0 ){
            $filtros .= " AND emca.cate_id=".$filtros_ini['cate_id'];    
            }
        }
        unset( $filtros_ini['cate_id'] );
    }

    $orse_indisponibilidad = "";
    if( isset($filtros_ini['orse_indisponibilidad'])){
        $orse_indisponibilidad = $filtros_ini['orse_indisponibilidad'];
        unset( $filtros_ini['orse_indisponibilidad']);  
    }

    $empl_visitas = "";
    if( isset($filtros_ini['empl_visitas'])){
        $empl_visitas = $filtros_ini['empl_visitas'];
        unset( $filtros_ini['empl_visitas']);  
    }
    
    //Obtenemos el resto de filtros
    $filtros = Flight::filtersToWhereString( array("emplazamiento"), $filtros_ini).$filtros;
     
    
    $query = "SELECT "
                . "SQL_CALC_FOUND_ROWS "
                . "em.*, "
                . "co.comu_nombre, "
                . "emcl.clas_nombre, "
                . "emca.cate_nombre, "
                . "emdt.duto_nombre, "
                . "uc.usua_nombre AS usua_creador_nombre, "
                . "GROUP_CONCAT(t.tecn_nombre) as tecn_nombre, "
                . "'NO' as orse_indisponibilidad, "
                . "0 as empl_visitas, "
                . "0 as os_mnt_id_visita, "
                . " '' as tipo_modulo_visita "
            . "FROM "
                . "contrato c, "
                . "usuario uc, "
                . "rel_contrato_emplazamiento rce, "
                . "emplazamiento em, "
                . "comuna co, "
                . "region r,"
                . "provincia p, "
                . "emplazamiento_clasificacion emcl, "
                . "emplazamiento_categoria emca, "
                . "emplazamiento_dueno_torre emdt, "
                . "tecnologia t, "
                . "rel_emplazamiento_tecnologia ret, "
                . "usuario u, rel_contrato_usuario rcu, "
                . "rel_contrato_usuario_alcance rcua , rel_zona_emplazamiento rzeal, zona zal "
                .(($filtros_zonas_contrato_flag)?", rel_zona_emplazamiento rzeco, zona zco ":"" )
                .(($filtros_zonas_cluster_flag)?", rel_zona_emplazamiento rzecl, zona zcl ":"" )
            . "WHERE "
                . "c.cont_id=".$cont_id." "
                . "AND rce.cont_id=c.cont_id "
                . "AND rce.empl_id=em.empl_id "
                . "AND em.comu_id=co.comu_id "
                . "AND p.prov_id = co.prov_id "
                . "AND r.regi_id = p.regi_id "
                . "AND em.clas_id=emcl.clas_id "
                . "AND em.cate_id=emca.cate_id "                              
                . "AND em.duto_id=emdt.duto_id "
                . "AND em.usua_creador=uc.usua_id "
                . "AND ret.empl_id=em.empl_id "
                . "AND ret.tecn_id=t.tecn_id "
                    
                . "AND rzeal.empl_id=em.empl_id "
                . "AND rzeal.zona_id=zal.zona_id "
                . "AND zal.zona_tipo='ALCANCE' "

                . "AND u.usua_id = $usua_id "
                . "AND rcu.usua_id = u.usua_id "
                . "AND rcu.cont_id = c.cont_id "
                . "AND rcua.recu_id = rcu.recu_id "
                . "AND rcua.zona_id = rzeal.zona_id "
                . "AND ".$filtros." "
            . "GROUP BY em.empl_id " 
            . ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))
            ;
    
    $dbo = new MySQL_Database();
    //Flight::json($query);
    $res = $dbo->ExecuteQuery($query);    
    if ($res['status'] == 0){
      Flight::json(array("status" => 0, "error" => $res['error']));
    }
    
    $emplazamientos = array();
    foreach($res['data'] as $e) {
      $emplazamientos[$e['empl_id']] = $e;
    }


    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0){
      Flight::json(array("status" => 0, "error" => $res['error']));
    }
    $out['total'] = intval($res_count['data'][0]['total']);  
    

    //Agregamos la indisponibilidad de los emplazamientos
    $resOS = $dbo->ExecuteQuery("SELECT 
                                        orse_id
                                        ,empl_id
                                        ,orse_indisponibilidad
                                FROM 
                                        orden_servicio 
                                WHERE
                                        cont_id = $cont_id
                                        AND orse_estado NOT IN ('NOACTIVO', 'APROBADA', 'RECHAZADA')	
                                        AND orse_indisponibilidad != 'NO'	
                                ORDER BY 
                                        empl_id ASC");    
    if ($resOS['status'] == 0) Flight::json(array("status" => 0, "error" => $resOS['error']));
        
    foreach($resOS['data'] as $os) {
      if(isset($emplazamientos[$os['empl_id']])){
        if( $emplazamientos[$os['empl_id']]['orse_indisponibilidad'] != 'SI' ){
            $emplazamientos[$os['empl_id']]['orse_indisponibilidad'] = $os['orse_indisponibilidad'];
        }
      }
    }  


    //Agregamos visitas a los emplazamientos
    $TIMEOUT_VISITA = 2;
    $resEV = $dbo->ExecuteQuery("SELECT 
                                        empl_id,
                                        emvi_id_relacionado,
                                        emvi_modulo,
                                        count(*) AS count
                                     
                                FROM 
                                        emplazamiento_visita 
                                WHERE
                                        emvi_estado != 'NOACTIVO' AND emvi_fecha_ingreso >= date_sub(now(), INTERVAL $TIMEOUT_VISITA HOUR) 
                                GROUP BY empl_id
                                ORDER BY empl_id, emvi_id_relacionado, emvi_modulo ASC");    
    if($resEV['status'] == 0) Flight::json(array("status" => 0, "error" => $resEV['error']));
        
    $empl_con_visitas = 0;
    foreach($resEV['data'] as $key => $ev) {
      if(isset($emplazamientos[$ev['empl_id']])){

        $emplazamientos[$ev['empl_id']]['os_mnt_id_visita'] = $ev['emvi_id_relacionado'];
        $emplazamientos[$ev['empl_id']]['tipo_modulo_visita'] = $ev['emvi_modulo'];
        $emplazamientos[$ev['empl_id']]['empl_visitas'] = $ev['count'];
        $empl_con_visitas++;
      }
    }
    $out['con_visita'] = $empl_con_visitas;


    if($orse_indisponibilidad!="" || $empl_visitas!=""){
      foreach($emplazamientos as $id => &$e) {
        if($orse_indisponibilidad!=""){
          if($e['orse_indisponibilidad']!=$orse_indisponibilidad){
            unset($emplazamientos[$id]);
            $out['total']--;
          }
        }

        if($empl_visitas!=""){
          if($empl_visitas=="SI" && $e['empl_visitas']==0){
            unset($emplazamientos[$id]);
            $out['total']--;
          }
          if($empl_visitas=="NO" && 0<$e['empl_visitas']){
            unset($emplazamientos[$id]);
            $out['total']--;
          }
        }
      } 
    }


    if(!is_null($page) && $res['status']==1) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    $out['data'] = array_values($emplazamientos);

    Flight::json($out);
});





Flight::route('GET /core/contrato/@cont_id:[0-9]+/emplazamiento/detalle/@empl_id:[0-9]+', function($cont_id,$empl_id)
{    
    global $PERFILES;
    $out = array();
    $db = new MySQL_Database();
    
    //detalle
    $res = Flight::ObtenerDetalleEmplazamiento($db,$cont_id,$empl_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    
    if( count($res['data'])==0 ){
        Flight::json(array("status"=>0, "error"=>"Emplazamiento $empl_id no existe"));
        return;
    }    

    $out['status'] = 1;
    $out['emplazamiento'] = $res['data'][0];
    
    //historico
    $res=Flight::ObtenerDetalleEmplazamientoHistorico($db,$cont_id,$empl_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['emplazamiento_historico'] = $res['data'];

    Flight::json($out);
});


Flight::route('GET|POST /core/contrato/@id:[0-9]+/emplazamiento/upd/@ids:[0-9]+', function($id, $ids)
{
    $data = array_merge($_GET,$_POST,array("cont_id"=>$id, "empl_id"=>$ids));
    $query = "UPDATE rel_contrato_emplazamiento SET " . Flight::dataToUpdateString($data) . " WHERE cont_id=$id AND empl_id=$ids";
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/emplazamiento/contratos/listar', function(){
    
    $query = "SELECT cont_id, cont_nombre FROM contrato WHERE cont_estado='ACTIVO' order by cont_nombre";
    
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);   
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    Flight::json($res);
});

?>

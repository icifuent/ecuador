<?php
   // esta funcion se utiliza para extraer los nombres y ID de los contratos activos
   Flight::route('GET /pago',function(){

    $out = array();
     $out['status'] = 1;
     $dbo = new MySQL_Database();

      $consulta = $dbo->ExecuteQuery("SELECT
                                     cont_nombre,
                                     cont_id
                                     FROM contrato
                                     where cont_estado = 'ACTIVO'
                                   ");

       if( $consulta['status']==0 ){
           Flight::json(array("status"=>0, "error"=>$consulta['error']));
           return;
       }
       $out['contratos'] = $consulta['data'];

        Flight::json($out);
   });


   // esta funcion se utiliza para poder ingresar los nuevos presupuestos en la tabla de pagos
   Flight::route('POST /detalle_pago/add',function(){

     $out = array();
     $out['status'] = 1;
     $dbo = new MySQL_Database();

     $id_cont= mysql_real_escape_string($_POST['detapag_contratos']);
     $anio= mysql_real_escape_string($_POST['detapag_anio']);
     $monto= mysql_real_escape_string($_POST['detapag_monto']);
     $moneda= mysql_real_escape_string($_POST['detapag_moneda']);

     $res = $dbo->ExecuteQuery("SELECT   count(1) as pago_anio
                                FROM siompago.pago
                                WHERE pago_anio =$anio");

     $pago_anio= $res['data'][0]['pago_anio'];


      if($pago_anio<=0)
      {

           $res = $dbo->ExecuteQuery("INSERT INTO siompago.pago SET
                                   cont_id=$id_cont,
                                 pago_anio=$anio,
                                 pago_monto=$monto,
                          pago_tipo_moneda='$moneda'");

             if( $res['status']==0 ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
       }


       Flight::json($res);

      }
      else {

         Flight::json(array("status"=>0, "error"=>$res['error']));
           return;
      }


   //falta crear el validador re tiene q contenre el resultado y debo extrarlo y decir si esta repetido ps tengo
      // =========================================================================


     // $res = $dbo->ExecuteQuery("INSERT INTO siompago.pago SET
     //                               cont_id=$id_cont,
     //                             pago_anio=$anio,
     //                             pago_monto=$monto,
     //                      pago_tipo_moneda='$moneda'");

     //   if( $res['status']==0 ){
     //       Flight::json(array("status"=>0, "error"=>$res['error']));
     //       return;
     //   }


       // Flight::json($res);
   });

   // esta funcion se utiliza para poder buscar  el detalle  por fecha  y por id de contrato extrayendo el monto y la id
   //del contrato solicitado

   Flight::route('POST /buscar_aprobado',function(){
     $out = array();
     $out['status'] = 1;
     $dbo = new MySQL_Database();


           $id_cont= mysql_real_escape_string($_POST['papre_contrato']);
           $anio= mysql_real_escape_string($_POST['papre_anio']);

        if($anio==""){
           $anio="2017";
        }

       $res = $dbo->ExecuteQuery(" SELECT pago.cont_id,pago.pago_monto
                                         FROM siompago.pago
                                         INNER JOIN siom2.contrato sp ON (sp.cont_id = pago.cont_id )
                                         WHERE pago.cont_id = $id_cont
                                         and pago.pago_anio = $anio
                                       ");

        if( $res['status']==0 ){
           Flight::json(array("status"=>0, "error"=>$res['error']));
           return;
       }



       $out['monto'] = $res['data'];
       Flight::json($out);



   });

   ?>

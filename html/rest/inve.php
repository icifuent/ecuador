<?php

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/inve/emplazamiento/@empl_id:[0-9]+/item/agregar',function($cont_id,$empl_id){
  $usua_id = $_SESSION['user_id'];   

    $out = array();
    $out['status'] = 1;
  
    $dbo = new MySQL_Database();
    

    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];


Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/inve/emplazamiento/@empl_id:[0-9]+/item/@inel_id:[0-9]+/editar',function($cont_id,$empl_id,$inel_id){
    $out=array();
    $out['status']=1;
    $data = array_merge($_GET,$_POST); 
    $dbo = new MySQL_Database();


    $res=Flight::ObtenerDetalleEmplazamiento($dbo,$cont_id,$empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];

    $res_inve=$dbo->ExecuteQuery("SELECT * FROM inventario_elemento WHERE inel_id=$inel_id");
    if ($res_inve['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    $out['item']=$res_inve['data'];
    
    $res_invc=$dbo->ExecuteQuery("SELECT * FROM inventario_elemento_caracteristica WHERE inel_id=$inel_id");
    if ($res_invc['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }
    $out['caracteristicas']=$res_invc['data'];


 





    Flight::json($out);

});


Flight::route('POST /contrato/@cont_id:[0-9]+/inve/emplazamiento/@empl_id:[0-9]+/item/add',function($cont_id,$empl_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $dbo->startTransaction();
    $res= $dbo->ExecuteQuery("SELECT inve_id FROM inventario WHERE cont_id=$cont_id AND inve_estado='ACTIVO'");
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }   
    if ($res['rows']==0){
        $dbo->Rollback();
        Flight::json(array(
            "status"=>0,
            "error"=>"El contrato no tiene asociado un inventario activo"
            ));
        return;
    }

    $inve_id        = $res['data'][0]['inve_id'];
    $empl_id        = mysql_real_escape_string($_POST['empl_id']);
    $inel_nombre    = mysql_real_escape_string($_POST['inel_nombre']);
    $inel_descripcion = mysql_real_escape_string($_POST['inel_descripcion']);
    $inel_ubicacion = mysql_real_escape_string($_POST['inel_ubicacion']);
    $inel_codigo    = mysql_real_escape_string($_POST['inel_codigo']);
    $inel_estado    = mysql_real_escape_string($_POST['inel_estado']);

    //Desactivar codigo anterior
    $res = $dbo->ExecuteQuery("UPDATE inventario_elemento SET 
                                inel_estado = 'NOACTIVO'
                                WHERE inel_codigo='$inel_codigo'");
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //Agregar elemento nuevo
    $res = $dbo->ExecuteQuery("INSERT INTO inventario_elemento SET 
                                inve_id = $inve_id,
                                empl_id = $empl_id,
                                inel_nombre = '$inel_nombre',
                                inel_descripcion = '$inel_descripcion',
                                inel_ubicacion = '$inel_ubicacion',
                                inel_codigo = '$inel_codigo',
                                inel_fecha = NOW(),
                                inel_estado = '$inel_estado'");
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }  

   if ( isset($_POST['inec_genero']) ){
        $inel_id = $res['data'][0]['id'];
        
        for ($i=0; $i < count($_POST['inec_genero']) ; $i++) { 
            $inec_genero   = mysql_real_escape_string($_POST['inec_genero'][$i]);
            $inec_especie  = mysql_real_escape_string($_POST['inec_especie'][$i]);
            $inec_campo    = mysql_real_escape_string($_POST['inec_campo'][$i]);
            $inec_valor    = mysql_real_escape_string($_POST['inec_valor'][$i]);
          
           $res = $dbo->ExecuteQuery("INSERT INTO inventario_elemento_caracteristica SET 
                                inel_id = $inel_id,
                                inec_genero = '$inec_genero',
                                inec_especie = '$inec_especie',
                                inec_campo = '$inec_campo',
                                inec_valor = '$inec_valor'");
            if ($res['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }
    }

    $dbo->Commit();  
    Flight::json($res);

});

Flight::route('POST /contrato/@cont_id:[0-9]+/inve/emplazamiento/item/@inel_id:[0-9]+/editar/update',function($cont_id,$inel_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $dbo->startTransaction();
    
    $empl_id        = mysql_real_escape_string($_POST['empl_id']);
    $inel_nombre    = mysql_real_escape_string($_POST['inel_nombre']);
    $inel_descripcion = mysql_real_escape_string($_POST['inel_descripcion']);
    $inel_ubicacion = mysql_real_escape_string($_POST['inel_ubicacion']);
    $inel_codigo    = mysql_real_escape_string($_POST['inel_codigo']);
    $inel_estado    = mysql_real_escape_string($_POST['inel_estado']);


    //Agregar elemento nuevo
    $res = $dbo->ExecuteQuery("UPDATE inventario_elemento SET 
                                empl_id = $empl_id,
                                inel_nombre = '$inel_nombre',
                                inel_descripcion = '$inel_descripcion',
                                inel_ubicacion = '$inel_ubicacion',
                                inel_codigo = '$inel_codigo',
                                inel_estado = '$inel_estado'
                                WHERE inel_id=$inel_id");
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }  

   if ( isset($_POST['inec_id']) ){
        
        for ($i=0; $i < count($_POST['inec_id']) ; $i++) { 
            $inec_id       = mysql_real_escape_string($_POST['inec_id'][$i]);
            $inec_genero   = mysql_real_escape_string($_POST['inec_genero'][$i]);
            $inec_especie  = mysql_real_escape_string($_POST['inec_especie'][$i]);
            $inec_campo    = mysql_real_escape_string($_POST['inec_campo'][$i]);
            $inec_valor    = mysql_real_escape_string($_POST['inec_valor'][$i]);
          
            if($inec_id==""){
                $query = "INSERT INTO inventario_elemento_caracteristica SET 
                            inel_id = $inel_id,
                            inec_genero = '$inec_genero',
                            inec_especie = '$inec_especie',
                            inec_campo = '$inec_campo',
                            inec_valor = '$inec_valor'";
            }
            else{
                $query = "UPDATE inventario_elemento_caracteristica SET 
                            inec_genero = '$inec_genero',
                            inec_especie = '$inec_especie',
                            inec_campo = '$inec_campo',
                            inec_valor = '$inec_valor'
                            WHERE inec_id=$inec_id";
            }
            $res = $dbo->ExecuteQuery($query);
            if ($res['status'] == 0) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }
    }

    $dbo->Commit();  
    Flight::json($res);

});




Flight::route('GET /contrato/@cont_id:[0-9]+/inve/emplazamiento/@empl_id:[0-9]+/item/@inel_id:[0-9]+',function($cont_id,$empl_id,$inel_id){
    $usua_id = $_SESSION['user_id'];    
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();


    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];

    $res_inve= $dbo->ExecuteQuery("SELECT 
                                    ie.*,
                                    em.empl_nombre
                                   FROM inventario_elemento ie 
                                   INNER JOIN emplazamiento em ON (ie.empl_id=em.empl_id)
                                   WHERE ie.inel_id=$inel_id ORDER BY inel_fecha DESC");

    $out['inventario']=$res_inve['data'];

    foreach ($out['inventario'] as &$elemento) {
        # code...
        $res= $dbo->ExecuteQuery("SELECT * from inventario_elemento_caracteristica ic 
                                  WHERE ic.inel_id=".$elemento['inel_id']);
        if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }
        $elemento['caracteristicas'] = $res['data'];
    }
   
    Flight::json($out);
});


Flight::route('GET|POST /contrato/@cont_id:[0-9]+/inve/emplazamiento/@empl_id:[0-9]+/items/filtro',function($cont_id,$empl_id){

        
    $out=array();
    $out['status'] = 1;
     
    $dbo=new MySQL_Database();
    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM inventario_elemento WHERE Field = 'inel_estado'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);    
    $out['estados'] = explode("','", $matches[1]); 


    Flight::json($out);

});



Flight::route('GET|POST /contrato/@cont_id:[0-9]+/inve/emplazamiento/@empl_id:[0-9]+/items/list/@page:[0-9]+', function($cont_id,$empl_id,$page){
    
    $out=array();
    $results_by_page=Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";


    //Obtenemos el resto de filtros
    $filtros = Flight::filtersToWhereString( array("inventario_elemento"), $filtros_ini).$filtros;

    $query="SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT * FROM inventario_elemento WHERE $filtros ORDER BY inel_codigo,inel_fecha DESC) AS t GROUP BY inel_codigo".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
   // Flight::json(array("status" => 0, "error" => $query));    
    $dbo=new MySQL_Database();
    $out=$dbo->ExecuteQuery($query);
   
    if($out['status']==0){Flight::json(array("status"=>0, "error"=>$out['error']));return;}  


    $res_count=$dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if($out['status']==0){Flight::json(array("status"=>0, "error"=>$out['error']));return;}  
    $out['total']=intval($res_count['data'][0]['total']);

    if (!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total'] / $results_by_page);
    }
    $out['filtros'] = $filtros_ini;
   
    
    
    Flight::json($out);

});
?>
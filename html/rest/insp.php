<?php

Flight::set('MODU_ID', 3);
Flight::set('PAIS_ID', 1);
Flight::set('flight.log_errors', true);

//____________________________________________________________
//Helpers
Flight::map('ObtenerDetalleInspeccion', function($dbo,$cont_id,$insp_id){
    $res =  $dbo->ExecuteQuery("SELECT 
                                    inspeccion.*,
                                    empr_nombre,
                                    espe_nombre,
                                    uc.usua_nombre AS usua_creador_nombre,
                                    uv.usua_nombre AS usua_validador_nombre
                                FROM inspeccion 
                                INNER JOIN empresa ON (empresa.empr_id=inspeccion.empr_id)
                                INNER JOIN especialidad ON (especialidad.espe_id=inspeccion.espe_id)
                                INNER JOIN usuario AS uc ON (uc.usua_id = inspeccion.usua_creador)
                                LEFT JOIN usuario AS uv ON (uv.usua_id = inspeccion.usua_validador)
                                WHERE 
                                    insp_id =$insp_id");
    if ($res['status'] == 0){ return $res; }

    if (0 < $res['rows']) {

        $res_repo = $dbo->ExecuteQuery("SELECT * FROM repositorio r WHERE r.repo_tabla_id = $insp_id AND r.repo_tabla = 'inspeccion';");
        if ($res_repo['status'] == 0){
            return $res_repo;
        }
        if( 0 < $res_repo['rows']){
            $res['data'][0]['archivos'] = $res_repo['data'];
        }

        $mant_id = $res['data'][0]['mant_id'];
        $orse_id = $res['data'][0]['orse_id'];

        if ($mant_id != "") {
            $res_mant = Flight::ObtenerDetalleMantenimiento($dbo, $cont_id, $mant_id);
            if ($res_mant['status'] == 0) { 
                Flight::json(array("status" => 0, "error" => $res_mant['error'])); 
                return; 
            }

            if ($res_mant['rows'] == 0) {
                Flight::json(array("status" => 0, "error" => "Mantenimiento asociado Nº $mant_id no existe"));
                return;
            }
            $res['data'][0]['mantenimiento'] = $res_mant['data'][0];
        }

        

        if ($orse_id != "") {
            $res_orse = Flight::ObtenerDetalleOS($dbo, $cont_id, $orse_id);
            if ($res_orse['status'] == 0) { 
                Flight::json(array("status" => 0, "error" => $res_orse['error'])); 
                return; 
            }
            if ($res_orse['rows'] == 0) {
                Flight::json(array("status" => 0, "error" => "Orden de servicio asociada NÂº $orse_id no existe"));
                return;
            }
            $res['data'][0]['os'] = $res_orse['data'][0];
        }
        
    }


    return $res;
});


Flight::map('ObtenerResumenInspeccionesResumen', function($dbo,$cont_id,$filtros_ini){
    global $DEF_CONFIG;

    $FORM_INGRESO = $DEF_CONFIG['insp']['formIngreso'];
    $FORM_SALIDA  = $DEF_CONFIG['insp']['formSalida'];

    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $filtros = "";
    if(isset($filtros_ini['periodo'])){
        if(is_array($filtros_ini['periodo'])){
            foreach ($filtros_ini['periodo'] as $periodo) {
                if($filtros != ""){
                    $filtros .= " AND ";
                }
                $filtros .= "(insp_fecha_creacion ".$periodo.")";
            }
        }
        else{
            $filtros .= "(insp_fecha_creacion ".$filtros_ini['periodo'].")";
        }
    }

    if(isset($filtros_ini['fecha_inicio']) && $filtros_ini['fecha_inicio']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }
        $filtros .= "'".$filtros_ini['fecha_inicio']."' <= insp_fecha_creacion";
    }

    if(isset($filtros_ini['fecha_fin']) && $filtros_ini['fecha_fin']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }
        $filtros .= "insp_fecha_creacion <= '".$filtros_ini['fecha_fin']."'";
    }

    if(isset($filtros_ini['zona_contrato'])){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['zona_contrato'])){
            $filtros .= "zona_contrato.zona_id IN ('".implode("','",$filtros_ini['zona_contrato'])."')";
        }
        else{
            $filtros .= "zona_contrato.zona_id = ".$filtros_ini['zona_contrato'];
        }
    }

    if(isset($filtros_ini['zona_movistar'])){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['zona_movistar'])){
            $filtros .= "zona_movistar.zona_id IN ('".implode("','",$filtros_ini['zona_movistar'])."')";
        }
        else{
            $filtros .= "zona_movistar.zona_id = ".$filtros_ini['zona_movistar'];
        }
    }

    if(isset($filtros_ini['zona_cluster'])){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['zona_cluster'])){
            $filtros .= "zona_cluster.zona_id IN ('".implode("','",$filtros_ini['zona_cluster'])."')";
        }
        else{
            $filtros .= "zona_cluster.zona_id = ".$filtros_ini['zona_cluster'];
        }
    }

    if(isset($filtros_ini['regi_id'])){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['regi_id'])){
            $filtros .= "regi_id IN ('".implode("','",$filtros_ini['regi_id'])."')";
        }
        else{
            $filtros .= "regi_id = ".$filtros_ini['regi_id'];
        }
    }

    if(isset($filtros_ini['comu_id'])){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['comu_id'])){
            $filtros .= "comu_id IN ('".implode("','",$filtros_ini['comu_id'])."')";
        }
        else{
            $filtros .= "comu_id = ".$filtros_ini['comu_id'];
        }
    }

    if(isset($filtros_ini['usua_id'])){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['usua_id'])){
            $filtros .= "usua_id IN ('".implode("','",$filtros_ini['usua_id'])."')";
        }
        else{
            $filtros .= "usua_id = ".$filtros_ini['usua_id'];
        }
    }

    $group_by = $filtros_ini['agrupador'];
    switch($group_by){
        case "zona_contrato": 
            $group_by_field = "zona_contrato_nombre AS `Zona contrato`";
            break;
        case "zona_movistar": 
            $group_by_field = "zona_movistar_nombre AS `Zona Movistar`";
            break;
        case "zona_cluster": 
            $group_by_field = "zona_cluster_nombre  AS `Cluster`";
            break;
        case "regi_id": 
            $group_by_field = "regi_nombre AS `Region`";
            break;
        case "comu_id": 
            $group_by_field = "comu_nombre AS `Comuna`";
            break;
        case "usua_id": 
            $group_by_field = "usua_nombre AS `Usuario`";
            break;
    }

    $query = "SELECT
                $group_by_field,

                SUM(OKS) AS `Nº OK`,
                SUM(NOKS) AS `Nº NOK`,
                SUM(NAS) AS `Nº NA`,
                SUM(NRS) AS `Nº NR`
                FROM
                (
                SELECT 

                min(zona_contrato.zona_id) AS zona_contrato,
                min(zona_contrato.zona_nombre) AS zona_contrato_nombre,

                min(zona_movistar.zona_id) AS zona_movistar,
                min(zona_movistar.zona_nombre) AS zona_movistar_nombre,

                min(zona_cluster.zona_id) AS zona_cluster,
                min(zona_cluster.zona_nombre) AS zona_cluster_nombre,

                region.regi_id,
                region.regi_nombre,

                comuna.comu_id,
                comuna.comu_nombre,

                usua_creador.usua_id,
                usua_creador.usua_nombre,

                formulario_respuesta.fore_id,
                (SELECT 
                                                count(*)
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='OK') AS OKS,
                (SELECT 
                                                count(*) 
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NOK') AS NOKS,
                (SELECT 
                                                count(*) 
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NA') AS NAS,
                (SELECT 
                                                count(*) 
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NR') AS NRS
                FROM inspeccion
                LEFT JOIN mantenimiento  ON (inspeccion.mant_id = mantenimiento.mant_id)


                INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=inspeccion.empl_id)
                LEFT JOIN zona AS zona_contrato ON (zona_contrato.zona_id=rel_zona_emplazamiento.zona_id AND zona_contrato.zona_tipo='CONTRATO')
                LEFT JOIN zona AS zona_movistar ON (zona_movistar.zona_id=rel_zona_emplazamiento.zona_id AND zona_movistar.zona_tipo='MOVISTAR')
                LEFT JOIN zona AS zona_cluster ON (zona_cluster.zona_id=rel_zona_emplazamiento.zona_id AND zona_cluster.zona_tipo='CLUSTER')
                INNER JOIN emplazamiento ON (emplazamiento.empl_id=inspeccion.empl_id)
                INNER JOIN comuna ON (comuna.comu_id=emplazamiento.comu_id)
                INNER JOIN provincia ON (provincia.prov_id=comuna.prov_id)
                INNER JOIN region ON (region.regi_id=provincia.regi_id)
                INNER JOIN usuario AS usua_creador ON (usua_creador.usua_id=inspeccion.usua_creador)

                LEFT JOIN informe ON(informe.info_modulo='INSP' AND informe.info_id_relacionado=inspeccion.insp_id)
                LEFT JOIN rel_informe_formulario_respuesta ON (rel_informe_formulario_respuesta.info_id = informe.info_id)
                LEFT JOIN formulario_respuesta ON (formulario_respuesta.fore_id=rel_informe_formulario_respuesta.fore_id AND formulario_respuesta.form_id NOT IN (1,4))

                WHERE inspeccion.cont_id=$cont_id 
                 $filtros  
                 GROUP BY fore_id
                ) AS T
                GROUP BY $group_by";


    return $dbo->ExecuteQuery($query);

});

Flight::map('ObtenerResumenInspeccionesDetalle', function($dbo,$cont_id,$filtros_ini){
    global $DEF_CONFIG;

    $FORM_INGRESO = $DEF_CONFIG['insp']['formIngreso'];
    $FORM_SALIDA  = $DEF_CONFIG['insp']['formSalida'];

    $filtros = "";
    if(isset($filtros_ini['periodo'])){
        if(is_array($filtros_ini['periodo'])){
            foreach ($filtros_ini['periodo'] as $periodo) {
                if($filtros != ""){
                    $filtros .= " AND ";
                }
                $filtros .= "(insp_fecha_creacion ".$periodo.")";
            }
        }
        else{
            $filtros .= "(insp_fecha_creacion ".$filtros_ini['periodo'].")";
        }
    }

    if(isset($filtros_ini['fecha_inicio']) && $filtros_ini['fecha_inicio']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }
        $filtros .= "'".$filtros_ini['fecha_inicio']."' <= insp_fecha_creacion";
    }

    if(isset($filtros_ini['fecha_fin']) && $filtros_ini['fecha_fin']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }
        $filtros .= "insp_fecha_creacion <= '".$filtros_ini['fecha_fin']."'";
    }

    if(isset($filtros_ini['zona_contrato']) && $filtros_ini['zona_contrato']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['zona_contrato'])){
            $filtros .= "zona_contrato.zona_id IN ('".implode("','",$filtros_ini['zona_contrato'])."')";
        }
        else{
            $filtros .= "zona_contrato.zona_id = ".$filtros_ini['zona_contrato'];
        }
    }

    if(isset($filtros_ini['zona_movistar']) && $filtros_ini['zona_movistar']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['zona_movistar'])){
            $filtros .= "zona_movistar.zona_id IN ('".implode("','",$filtros_ini['zona_movistar'])."')";
        }
        else{
            $filtros .= "zona_movistar.zona_id = ".$filtros_ini['zona_movistar'];
        }
    }

    if(isset($filtros_ini['zona_cluster']) && $filtros_ini['zona_cluster']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['zona_cluster'])){
            $filtros .= "zona_cluster.zona_id IN ('".implode("','",$filtros_ini['zona_cluster'])."')";
        }
        else{
            $filtros .= "zona_cluster.zona_id = ".$filtros_ini['zona_cluster'];
        }
    }

    if(isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['regi_id'])){
            $filtros .= "regi_id IN ('".implode("','",$filtros_ini['regi_id'])."')";
        }
        else{
            $filtros .= "regi_id = ".$filtros_ini['regi_id'];
        }
    }

    if(isset($filtros_ini['comu_id']) && $filtros_ini['comu_id']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['comu_id'])){
            $filtros .= "comu_id IN ('".implode("','",$filtros_ini['comu_id'])."')";
        }
        else{
            $filtros .= "comu_id = ".$filtros_ini['comu_id'];
        }
    }

    if(isset($filtros_ini['usua_id']) && $filtros_ini['usua_id']!=""){
        if($filtros != ""){
            $filtros .= " AND ";
        }

        if(is_array($filtros_ini['usua_id'])){
            $filtros .= "usua_id IN ('".implode("','",$filtros_ini['usua_id'])."')";
        }
        else{
            $filtros .= "usua_id = ".$filtros_ini['usua_id'];
        }
    }

    $query = "SELECT
                insp_id AS `Nº Insp.`,
                empl_nemonico AS `Nemonico`,
                empl_nombre AS `Nombre`,
                mant_fecha_ejecucion AS `Fecha MNT`,
                tecnico AS `Técnico`,
                insp_fecha_creacion AS `Fecha insp.`,
                insp_tipo AS `Tipo`,
                responsable AS `Responsable`,
                insp_estado AS 'Estado',
                insp_conformidad AS `% Conformidad`,
                orse_id AS `Nº OS`,
                SUM(OKS) AS `Nº OK`,
                SUM(NOKS) AS `Nº NOK`,
                SUM(NAS) AS `Nº NA`,
                SUM(NRS) AS `Nº NR`
                FROM
                (
                SELECT 
                insp_id,
                empl_nemonico,
                empl_nombre,
                IFNULL(mant_fecha_ejecucion,'') AS mant_fecha_ejecucion,
                IFNULL((SELECT
                    usua_asignado.usua_nombre
                    FROM
                        inspeccion_asignacion 
                    INNER JOIN rel_inspeccion_asignacion_usuario ON (inspeccion_asignacion.inas_id = rel_inspeccion_asignacion_usuario.inas_id AND rel_inspeccion_asignacion_usuario.rinu_tipo = 'JEFECUADRILLA') 
                    INNER JOIN usuario AS usua_asignado
                    WHERE inspeccion.insp_id = inspeccion_asignacion.insp_id AND inas_estado='ACTIVO'
                    ORDER BY inas_fecha_asignacion DESC LIMIT 1),'Sin asignar') AS tecnico,
                insp_fecha_creacion,
                insp_tipo,
                (SELECT DISTINCT
                                   GROUP_CONCAT(DISTINCT usuario.usua_nombre)
                                 FROM 
                                   rel_zona_emplazamiento
                                 INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                                 INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                                 INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id  AND usuario.usua_estado='ACTIVO')
                                 INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                                 INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre='GESTOR')
                                 WHERE
                                     rel_zona_emplazamiento.empl_id=inspeccion.empl_id) AS responsable,
                insp_estado,
                IFNULL(insp_conformidad,'') AS insp_conformidad,
                IFNULL(inspeccion.orse_id,'') AS orse_id,
                formulario_respuesta.fore_id,
                (SELECT 
                                                count(*)
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='OK') AS OKS,
                (SELECT 
                                                count(*) 
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NOK') AS NOKS,
                (SELECT 
                                                count(*) 
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NA') AS NAS,
                (SELECT 
                                                count(*) 
                                                FROM 
                                                  formulario_valor 
                                                WHERE 
                                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NR') AS NRS
                FROM inspeccion
                LEFT JOIN mantenimiento  ON (inspeccion.mant_id = mantenimiento.mant_id)


                INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=inspeccion.empl_id)
                LEFT JOIN zona AS zona_contrato ON (zona_contrato.zona_id=rel_zona_emplazamiento.zona_id AND zona_contrato.zona_tipo='CONTRATO')
                LEFT JOIN zona AS zona_movistar ON (zona_movistar.zona_id=rel_zona_emplazamiento.zona_id AND zona_movistar.zona_tipo='MOVISTAR')
                LEFT JOIN zona AS zona_cluster ON (zona_cluster.zona_id=rel_zona_emplazamiento.zona_id AND zona_cluster.zona_tipo='CLUSTER')
                INNER JOIN emplazamiento ON (emplazamiento.empl_id=inspeccion.empl_id)
                INNER JOIN comuna ON (comuna.comu_id=emplazamiento.comu_id)
                INNER JOIN provincia ON (provincia.prov_id=comuna.prov_id)
                INNER JOIN region ON (region.regi_id=provincia.regi_id)
                INNER JOIN usuario AS usua_creador ON (usua_creador.usua_id=inspeccion.usua_creador)

                LEFT JOIN informe ON(informe.info_modulo='INSP' AND informe.info_id_relacionado=inspeccion.insp_id)
                LEFT JOIN rel_informe_formulario_respuesta ON (rel_informe_formulario_respuesta.info_id = informe.info_id)
                LEFT JOIN formulario_respuesta ON (formulario_respuesta.fore_id=rel_informe_formulario_respuesta.fore_id AND formulario_respuesta.form_id NOT IN ($FORM_INGRESO,$FORM_SALIDA)) 
                 WHERE inspeccion.cont_id=$cont_id 
                  $filtros  
                GROUP BY fore_id
                ) AS T
                GROUP BY `Nº Insp.`";

    return $dbo->ExecuteQuery($query);
});

//____________________________________________________________
//Bandeja
Flight::route('GET /contrato/@id:[0-9]+/insp/bandeja/filtros', function($id){
    //session_start();

    $pais_id = Flight::get('PAIS_ID');
    $modu_id = Flight::get('MODU_ID');

    $usua_id = $_SESSION['user_id']; 
    $empr_id  = $_SESSION['empr_id'];
    
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();


    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM inspeccion WHERE Field = 'insp_estado'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['estados'] = explode("','", $matches[1]); 

    //Listado de usuarios jefes de cuadrilla asignados a inspecciones, limitado a la empresa del usuario actual si no es cliente
    $res = $dbo->ExecuteQuery("SELECT rce.coem_tipo FROM rel_contrato_empresa rce WHERE rce.empr_id=$empr_id AND rce.cont_id=$id");
    if ($res['status'] == 0 || count($res['data'])==0 ) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $isClient = ($res['data'][0]['coem_tipo']=='CLIENTE');
    
    $res = $dbo->ExecuteQuery("SELECT 
                                        u.usua_id
                                        , u.usua_nombre 
                                FROM 
                                        contrato c 
                                        INNER JOIN inspeccion i ON c.cont_id = i.cont_id
                                        INNER JOIN inspeccion_asignacion ia ON i.insp_id = ia.insp_id
                                        INNER JOIN rel_inspeccion_asignacion_usuario riau ON ia.inas_id = riau.inas_id 
                                        INNER JOIN usuario u ON riau.usua_id = u.usua_id
                                        INNER JOIN rel_usuario_perfil rup ON u.usua_id = rup.usua_id
                                        INNER JOIN perfil p ON rup.perf_id = p.perf_id 
                                        ".(($isClient)?"":"INNER JOIN empresa e ON u.empr_id = e.empr_id")."
                                WHERE
                                        c.cont_id = $id
                                        AND p.perf_nombre = 'JEFE_CUADRILLA'
                                        ".(($isClient)?"":"AND e.empr_id = $empr_id")."
                                GROUP BY u.usua_id
                                ORDER BY u.usua_nombre  ASC
                                ;");

    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['usuarios'] = $res['data'];   
    
    //Listado de usuarios jefes de cuadrilla asignados a mantenimientos a inspeccionar, limitado a la empresa del usuario si no es cliente
    $res = $dbo->ExecuteQuery("SELECT 
                                        u.usua_id
                                        , u.usua_nombre 
                                FROM 
                                        contrato c 
                                        INNER JOIN inspeccion i ON c.cont_id = i.cont_id
                                        INNER JOIN mantenimiento m ON i.mant_id = m.mant_id
                                        INNER JOIN mantenimiento_asignacion ma ON m.mant_id = ma.mant_id
                                        INNER JOIN rel_mantenimiento_asignacion_usuario rmau ON ma.maas_id = rmau.maas_id
                                        INNER JOIN usuario u ON rmau.usua_id = u.usua_id
                                        INNER JOIN rel_usuario_perfil rup ON u.usua_id = rup.usua_id
                                        INNER JOIN perfil p ON rup.perf_id = p.perf_id 
                                        ".(($isClient)?"":"INNER JOIN empresa e ON u.empr_id = e.empr_id")."
                                WHERE
                                        c.cont_id = $id
                                        AND p.perf_nombre = 'JEFE_CUADRILLA'
                                        ".(($isClient)?"":"AND e.empr_id = $empr_id")."
                                GROUP BY u.usua_id
                                ORDER BY u.usua_nombre  ASC
                                ;");
    
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros" ));
    $out['tecnicos'] = $res['data'];

//    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM orden_servicio WHERE Field = 'orse_tipo'" );
//    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
//    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
//    $out['tipos'] = explode("','", $matches[1]); 
//    if( !$isClient ){
//        $out['tipos'] = array_diff($out['tipos'], array('OSI'));
//    }
    $out['tipos'] = array( "OSGN", "OSGU");    
    
    $res = $dbo->ExecuteQuery("SELECT * FROM comuna");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['comunas'] = $res['data'];
        
    Flight::json($out);
});


Flight::route('GET|POST /contrato/@id:[0-9]+/insp/list(/@page:[0-9]+)', function($id, $page) {
    $usua_id = $_SESSION['user_id'];    
    
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";

    $out = array();
    $out['pagina'] = $page;
    $out['filtros'] = $filtros_ini;
    $out['status'] = 1;
    
    //Eliminamos las llaves vacias
    foreach ($filtros_ini as $key => $value) {
        if( is_null($value) ){
            unset($filtros_ini[$key]);
        }
        if( !is_array($value) && strlen(trim($value.""))==0 ){
            unset($filtros_ini[$key]);
        }
    }

    //Sacamos los filtros especiales del arreglo de filtros
    
    if( isset($filtros_ini['insp_fecha_solicitud_inicio']) ){
        $filtros .= " AND i.insp_fecha_solicitud >= '".$filtros_ini['insp_fecha_solicitud_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['insp_fecha_solicitud_inicio']);
    }
    
    if( isset($filtros_ini['insp_fecha_solicitud_termino']) ){
        $filtros .= " AND i.insp_fecha_solicitud <= '".$filtros_ini['insp_fecha_solicitud_termino']." 23:59:59' "    ; 
        unset($filtros_ini['insp_fecha_solicitud_termino']);
    }
    
    
    if( isset($filtros_ini['usuario_inspeccion']) ){
        $filtros .= " AND riau.usua_id=".$filtros_ini['usuario_inspeccion']." ";
        unset( $filtros_ini['usuario_inspeccion'] );            
    }    
    
    if( isset($filtros_ini['tecnico_mantenimiento']) ){
        $filtros .= " AND rmau.usua_id=".$filtros_ini['tecnico_mantenimiento']." ";
        unset( $filtros_ini['tecnico_mantenimiento'] );            
    }   
    
    if( isset($filtros_ini['insp_id']) ){
        $filtros .= " AND i.insp_id=".$filtros_ini['insp_id']." ";
        unset( $filtros_ini['insp_id'] );            
    }
    
    if( isset($filtros_ini['mant_id']) ){
        $filtros .= " AND m.mant_id=".$filtros_ini['mant_id']." ";
        unset( $filtros_ini['mant_id'] );            
    }
    
    if( isset($filtros_ini['orse_id']) ){
        $filtros .= " AND o.orse_id=".$filtros_ini['orse_id']." ";
        unset( $filtros_ini['orse_id'] );            
    }
  
//    if( isset($filtros_ini['mant_estado']) ){
//        if(is_array($filtros_ini['mant_estado'])){ 
//            $estados = array();
//            foreach($filtros_ini['mant_estado'] as $a){
//                array_push($estados,"'".$a."'");
//            }
//            $estados = implode(",",$estados);
//        }
//        else{
//            $estados = "'".$filtros_ini['mant_estado']."'";
//        }
//        
//        $filtros .= " AND mnt.mant_estado IN (".$estados.")";
//        unset( $filtros_ini['mant_estado'] );
//    }
      

    //Obtenemos el resto de filtros
    $filtros = Flight::filtersToWhereString( array("inspeccion", "orden_servicio", "emplazamiento"), $filtros_ini).$filtros;
    /*
    $query = "SELECT 
                    SQL_CALC_FOUND_ROWS 
                    i.*
                    ,e.empl_nombre
                    ,m.mant_fecha_ejecucion
            FROM 
                    inspeccion i
                    LEFT JOIN mantenimiento m ON i.mant_id = m.mant_id
                    LEFT JOIN orden_servicio o ON i.orse_id = o.orse_id	

                    LEFT JOIN inspeccion_asignacion ia ON i.insp_id = ia.insp_id
                    LEFT JOIN rel_inspeccion_asignacion_usuario riau ON ia.inas_id = riau.inas_id 

                    LEFT JOIN mantenimiento_asignacion ma ON m.mant_id = ma.mant_id
                    LEFT JOIN rel_mantenimiento_asignacion_usuario rmau ON ma.maas_id = rmau.maas_id

                    INNER JOIN emplazamiento e ON i.empl_id = e.empl_id
            WHERE
                    i.cont_id = $id
                    AND ".$filtros
                    .((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))." 
            ;";
    
    */
    $query = "SELECT SQL_CALC_FOUND_ROWS * FROM
                (
                        SELECT 
                                i.*
                                ,e.empl_nombre
                                ,e.empl_nemonico
                                ,m.mant_fecha_ejecucion
                                ,empr.empr_nombre                    			
                        FROM 
                                inspeccion i
                        LEFT JOIN mantenimiento m ON i.mant_id = m.mant_id
                        LEFT JOIN orden_servicio o ON i.orse_id = o.orse_id	

                        LEFT JOIN inspeccion_asignacion ia ON i.insp_id = ia.insp_id
                        LEFT JOIN rel_inspeccion_asignacion_usuario riau ON (ia.inas_id = riau.inas_id AND riau.rinu_tipo = 'JEFECUADRILLA') 

                        LEFT JOIN mantenimiento_asignacion ma ON m.mant_id = ma.mant_id
                        LEFT JOIN rel_mantenimiento_asignacion_usuario rmau ON (ma.maas_id = rmau.maas_id AND rmau.rmau_tipo = 'JEFECUADRILLA')

                        INNER JOIN emplazamiento e ON i.empl_id = e.empl_id

                        INNER JOIN empresa empr ON (empr.empr_id = i.empr_id)
                        WHERE
                        i.cont_id = $id
                        AND ".$filtros."
                ) t
            GROUP BY insp_id"
            .((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))." 
            ;";
    //Flight::json(array("status" => 0, "error" => $query));   
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['inspecciones'] = $res['data'];

    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    $out['paginas'] = ceil($out['total']/$results_by_page);
    

    foreach($out['inspecciones'] as &$insp) {
        $insp_id = $insp['insp_id'];
        $res = $dbo->ExecuteQuery("SELECT
                                        tarea.*
                                    FROM
                                        tarea
                                    WHERE 
                                    tare_modulo='INSP' AND tare_estado IN ('CREADA','DESPACHADA') 
                                    AND tare_id_relacionado = $insp_id
                                    ORDER BY tare_fecha_despacho ASC
                                    ");
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        if(0<$res['rows']){
            $insp['tarea'] = $res['data'];
        }
    }


    Flight::json($out);    
});


//____________________________________________________________
//Crear
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/add/filtros', function($cont_id){
    $modu_id = Flight::get('MODU_ID');

    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CONTRATO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zonas'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CLUSTER'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['clusters'] = $res['data'];

    /*$res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=1 ORDER BY regi_orden ASC");*/
	$res = $dbo->ExecuteQuery("SELECT r.regi_id, r.regi_nombre FROM region r, contrato cn  WHERE cn.cont_id= " . $cont_id . "  AND cn.pais_id=r.pais_id  ORDER BY r.regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT tecn_id, tecn_nombre FROM tecnologia");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['tecnologias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT e.empr_id, e.empr_nombre, rce.coem_tipo 
                                FROM empresa e, rel_contrato_empresa rce 
                                WHERE 
                                    rce.cont_id= $cont_id 
                                    AND rce.coem_tipo IN ('CLIENTE','TERCEROS') 
                                    AND e.empr_estado='ACTIVO'
                                    AND e.empr_id=rce.empr_id");

    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['empresa'] = $res['data'];


    $res = $dbo->ExecuteQuery("SELECT 
                                  especialidad.espe_id, espe_nombre 
                                FROM especialidad 
                                INNER JOIN rel_modulo_especialidad ON (rel_modulo_especialidad.espe_id=especialidad.espe_id AND 
                                                                       rel_modulo_especialidad.modu_id=$modu_id)
                                WHERE espe_estado='ACTIVO' AND cont_id=$cont_id");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['especialidad'] = $res['data'];

    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM inspeccion WHERE Field = 'insp_tipo'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['tipos'] = explode("','", $matches[1]); 

    $out['status'] = 1;
    $out['modu_id'] = $modu_id;
    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/add', function($cont_id){
    $db = new MySQL_Database();
    $db->startTransaction();

    $usua_creador           = $_SESSION['user_id'];
    $empr_id                = mysql_real_escape_string($_POST['empr_id']);
    $empl_id                = mysql_real_escape_string($_POST['empl_id']);
    $espe_id                = mysql_real_escape_string($_POST['espe_id']);
    $insp_tipo              = mysql_real_escape_string($_POST['insp_tipo']);
    $insp_fecha_solicitud   = mysql_real_escape_string($_POST['insp_fecha_solicitud']);
    $insp_descripcion       = mysql_real_escape_string($_POST['insp_descripcion']);
    $mant_id = "NULL";
    if(isset($_POST['mant_id'])){
        $mant_id = "'".mysql_real_escape_string($_POST['mant_id'])."'";
    }
    
    $query = "INSERT INTO inspeccion SET 
                cont_id='$cont_id',
                empr_id='$empr_id',
                empl_id='$empl_id',
                espe_id='$espe_id',
                insp_tipo='$insp_tipo',
                insp_descripcion='$insp_descripcion',
                insp_fecha_solicitud='$insp_fecha_solicitud',
                insp_fecha_creacion=NOW(),
                usua_creador='$usua_creador',
                insp_estado='CREADA',
                mant_id=$mant_id";
    $res = $db->ExecuteQuery($query);
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $insp_id = $res['data'][0]['id'];

    //Subida de archivos
    if (isset($_FILES["archivos"])) { 
        $files = array(); 
        foreach($_FILES["archivos"] as $key1 => $value1) {
            foreach($value1 as $key2 => $value2) {
                $files[$key2][$key1] = $value2; 
            }
        }
        $descriptions = json_decode($_POST['archivos_descripciones'],true);

        for($i=0; $i<count($files); $i++){
            $nombre      = $files[$i]['name'];
            $descripcion = $descriptions[$i];

            $filename = date('ymdHis')."_insp_".$insp_id."_".str_replace(" ","_",$nombre);
            $resFile = Upload::UploadFile($files[$i],$filename); 
            if( !$resFile['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resFile['error']));
                return;
            }
            
            $query = "INSERT INTO repositorio (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
                        VALUES(
                            'DOCUMENTO',
                            '".$nombre."',
                            '".$descripcion."',
                            'inspeccion',
                            $insp_id,
                            '".$resFile['data']['filename']."',
                            NOW(),
                            NULL,
                            $usua_creador
                    )";
            $resUpload = $db->ExecuteQuery($query);
            if( $resUpload['status']==0 ) {
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resUpload['error']));
                return;
            }            
        }
    }

    //Agregar eventos
    $resEvent = Flight::AgregarEvento($db,"INSP","CREADA",$insp_id,null);
    if( $resEvent['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $db->Commit();
    
    Flight::json($res);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/insp/del/@insp_id:[0-9]+', function($cont_id, $insp_id){
    
    $db = new MySQL_Database();

    $db->startTransaction();
    $res = $db->ExecuteQuery("UPDATE inspeccion 
                                SET insp_estado='ANULADA' 
                                WHERE cont_id= $cont_id AND insp_id = $insp_id");
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    //tarea
    $query = "UPDATE tarea SET tare_estado='ANULADA' 
              WHERE tare_modulo='INSP' AND tare_id_relacionado = $insp_id";
    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $db->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //notificacion
    $query = "UPDATE notificacion SET noti_estado='ENTREGADA' 
              WHERE 
                    noti_modulo='INSP'
                    AND noti_estado='DESPACHADA'
                    AND noti_id_relacionado = $insp_id";
    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $db->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //evento
    $usua_id = 1;
    if (isset($_SESSION['user_id'])) {
        $usua_id = $_SESSION['user_id'];
    }

    $query = "UPDATE evento SET even_estado='ATENDIDO' 
              WHERE 
                    even_modulo='INSP'
                    AND even_estado='DESPACHADO'
                    AND even_id_relacionado = $insp_id";
    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $db->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //insertamos los eventos de anulacion
    $res = Flight::AgregarEvento($db,"INSP","ANULADA",$insp_id);
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $db->Commit();

    Flight::json($res);
    
});


//____________________________________________________________
//Detalle
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/detalle/@insp_id:[0-9]+', function($cont_id, $insp_id) {

    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();
    
    $res = Flight::ObtenerDetalleInspeccion($dbo, $cont_id, $insp_id);
    if ($res['status'] == 0) { 
        Flight::json(array("status" => 0, "error" => $res['error'])); 
        return; 
    } 

    if ($res['rows'] == 0) {
        Flight::json(array("status" => 0, "error" => "InspecciÃ³n $insp_id no existe"));
        return;
    }
    $out['inspeccion'] = $res['data'][0];
    
    $empl_id = $out['inspeccion']['empl_id'];    
    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];
    

    //asignacion    
    $res = $dbo->ExecuteQuery("SELECT 
                                    ia.inas_id,
                                    DATE_FORMAT(ia.inas_fecha_asignacion,'%d-%m-%Y %T') AS inas_fecha_asignacion,
                                    ia.usua_creador,
                                    usua_creador.usua_nombre AS usua_creador,
                                    usua_asignado.usua_nombre AS usua_asignado,
                                    ia.inas_estado
                               FROM 
                                    inspeccion_asignacion ia
                               INNER JOIN usuario AS usua_creador ON (ia.usua_creador=usua_creador.usua_id)
                               INNER JOIN rel_inspeccion_asignacion_usuario AS riau ON (riau.inas_id = ia.inas_id)
                               INNER JOIN usuario AS usua_asignado ON (riau.usua_id=usua_asignado.usua_id)
                               WHERE
                                    insp_id=$insp_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['asignacion'] = $res['data'];    


    //visitas
    $res = $dbo->ExecuteQuery("SELECT
                                tare_id,
                                usua_nombre,
                                tare_fecha_despacho,
                                tare_fecha_descarga,
                                tare_estado
                              FROM tarea
                              INNER JOIN usuario ON (usuario.usua_id=tarea.usua_id)
                              WHERE tare_modulo='INSP' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado=$insp_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['visitas'] = $res['data'];

    //visitas detalle
    for($i=0;$i<count($out['visitas']);$i++){
        $tare_id = $out['visitas'][$i]['tare_id'];
        $res = $dbo->ExecuteQuery("SELECT
                                        rel_tarea_formulario_respuesta.*
                                      FROM rel_tarea_formulario_respuesta 
                                      WHERE tare_id=$tare_id");
        if( $res['status']==0 ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['visitas'][$i]['detalle'] = $res['data'];
    }


    //informe
    $res = $dbo->ExecuteQuery("SELECT 
                                info_id,
                                DATE_FORMAT(info_fecha_creacion,'%d-%m-%Y %T') AS info_fecha_creacion,
                                uc.usua_nombre AS usua_creador,
                                info_estado,
                                uv.usua_nombre AS usua_validador,
                                DATE_FORMAT(info_fecha_validacion,'%d-%m-%Y %T') AS info_fecha_validacion,
                                info_observacion,
                                info_id_relacionado
                               FROM 
                                informe
                               INNER JOIN usuario uc ON (informe.usua_creador=uc.usua_id)
                               LEFT JOIN usuario uv ON (informe.usua_validador=uv.usua_id)
                               WHERE info_modulo='INSP' AND info_id_relacionado=$insp_id");
    
    if( $res['status']==0 ){ Flight::json(array("status"=>0, "error"=>$res['error'])); return; }
    $out['informes'] = $res['data'];


    //eventos
    $res = $dbo->ExecuteQuery("SELECT 
                                even_evento,
                                even_fecha,
                                even_estado,
                                even_comentario
                               FROM 
                                evento
                               WHERE
                                even_modulo='INSP' AND even_id_relacionado=$insp_id
                               ORDER BY even_fecha DESC");
    
    if( $res['status']==0 ){ Flight::json(array("status"=>0, "error"=>$res['error'])); return;}
    $out['eventos'] = $res['data'];

    Flight::json($out);
});

//____________________________________________________________
//Cambio de empresa inspectora
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/cambio_empresa_inspectora/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['inspeccion'] = $res['data'][0];
    
    $empl_id = $out['inspeccion']['empl_id'];    
    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];

    //Obtener listado de empresas
    $empr_id = $out['inspeccion']['empr_id']; 
    $res = $dbo->ExecuteQuery("SELECT
                                empresa.empr_id,
                                empr_nombre
                               FROM 
                                 empresa
                               INNER JOIN rel_contrato_empresa ON (cont_id=$cont_id AND rel_contrato_empresa.empr_id=empresa.empr_id)
                               WHERE
                                empresa.empr_id != $empr_id;");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['empresas'] = $res['data'];

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/cambio_empresa_inspectora/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $usua_creador = $_SESSION['user_id'];

    $db = new MySQL_Database();   
    
    $old_empr_id = $_POST['old_empr_id'];
    $empr_id     = $_POST['empr_id'];
    $res = $db->ExecuteQuery("UPDATE inspeccion SET empr_id=$empr_id WHERE insp_id=$insp_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($db,"INSP","CAMBIO_EMPRESA_INSPECTORA",$insp_id,array("from"=>$old_empr_id,"to"=>$empr_id));
    if( $resEvent['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    Flight::json($res);
});



//____________________________________________________________
//Asignacion
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/asignacion/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['inspeccion'] = $res['data'][0];
    
    $empl_id = $out['inspeccion']['empl_id'];    
    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }

    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];

    //Obtener asignacion previa (si la hay)
    $res = $dbo->ExecuteQuery("SELECT 
                                        ia.inas_id,
                                        u.usua_nombre,
                                        ia.inas_fecha_asignacion,
                                        ev.emvi_fecha_ingreso
                                FROM 
                                        inspeccion_asignacion ia
                                        INNER JOIN rel_inspeccion_asignacion_usuario riau ON ia.inas_id=riau.inas_id
                                        INNER JOIN usuario u ON riau.usua_id=u.usua_id
                                        LEFT JOIN emplazamiento_visita ev ON (ev.emvi_modulo='INSP' AND ev.emvi_id_relacionado=$insp_id AND ev.emvi_estado='ACTIVO')
                                WHERE
                                        ia.insp_id=$insp_id
                                        AND ia.inas_estado='ACTIVO'
                                        AND riau.rinu_tipo = 'JEFECUADRILLA'
                                ORDER BY ia.inas_fecha_asignacion DESC 
                                LIMIT 1");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['asignacion'] = null;
    if(0<$res['rows']){
        $out['asignacion'] = $res['data'][0];
    }

    //Obtener listado de usuarios
    $res = $dbo->ExecuteQuery("SELECT
                                        u.usua_id,
                                        u.usua_nombre,
                                        p.perf_nombre
                                FROM 
                                        inspeccion i
                                        INNER JOIN usuario u ON u.empr_id=i.empr_id
                                        INNER JOIN rel_contrato_usuario rcu ON rcu.usua_id=u.usua_id AND rcu.cont_id=i.cont_id
                                        INNER JOIN rel_usuario_perfil rup ON rup.usua_id=u.usua_id
                                        INNER JOIN perfil p ON p.perf_id=rup.perf_id 
                                WHERE
                                        i.insp_id=$insp_id
                                        AND p.perf_nombre = 'JEFE_CUADRILLA'");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['jefes'] = $res['data'];

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/asignacion/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $usua_creador = $_SESSION['user_id'];

    $db = new MySQL_Database();   
    $db->startTransaction();

    $res = $db->ExecuteQuery("INSERT INTO inspeccion_asignacion SET
                                insp_id=$insp_id,
                                inas_fecha_asignacion=NOW(),
                                usua_creador=$usua_creador,
                                inas_estado='ACTIVO'");
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $inas_id = $res['data'][0]['id'];

    $jefe = $_POST['jefecuadrilla'];
    $res = $db->ExecuteQuery("INSERT INTO rel_inspeccion_asignacion_usuario SET
                                inas_id=$inas_id,
                                usua_id=$jefe,
                                rinu_tipo='JEFECUADRILLA'");
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($db,"INSP","ASIGNAR",$insp_id);
    if( $resEvent['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($db,"INSP","ASIGNADA",$insp_id,array("inas_id"=>$inas_id));
    if( $resEvent['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $db->Commit();

    Flight::json($res);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/asignacion/@insp_id:[0-9]+/cancelar/@inas_id:[0-9]+', function($cont_id,$insp_id,$inas_id){
    $db = new MySQL_Database();   
    $db->startTransaction();

    $res = $db->ExecuteQuery("UPDATE inspeccion_asignacion SET
                                inas_estado='NOACTIVO' WHERE inas_id=$inas_id");
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    
    $resEvent = Flight::AgregarEvento($db,"INSP","ASIGNACION_CANCELADA",$insp_id,array("inas_id"=>$inas_id));
    if( $resEvent['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $db->Commit();
    Flight::json($res);

});


//____________________________________________________________
//Informe
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/informe/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['inspeccion'] = $res['data'][0];

    Flight::json($out);

});

Flight::route('GET /contrato/@cont_id:[0-9]+/insp/informe/@insp_id:[0-9]+/ver/@info_id:[0-9]+', function($cont_id,$insp_id,$info_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    
    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['inspeccion'] = $res['data'][0];


    $res = $dbo->ExecuteQuery("SELECT
                                    uc.usua_nombre AS usua_creador,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
                                LEFT JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
                                WHERE
                                    info_modulo='INSP' AND info_id=$info_id AND info_id_relacionado=$insp_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['informe'] = array();
    if(0<$res['rows']){
        $out['informe'] = $res['data'][0];

        $out['formularios'] = array();

        //Obtener visitas
        $info_data = json_decode($out['informe']['info_data'],true);
        if($info_data!=null){
            $tare_id = $info_data['tare_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                            rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                        FROM
                                            rel_tarea_formulario_respuesta
                                        INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                        INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
                                        WHERE
                                            tare_id=$tare_id");
            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }

            $out['formularios'] = $res['data'];

            for($i=0;$i<count($out['formularios']);$i++){
                $fore_id = $out['formularios'][$i]['fore_id'];

                if($out['formularios'][$i]['fore_ubicacion']!="" && $out['formularios'][$i]['fore_ubicacion']!="web"){
                    $out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
                }

                $res = $dbo->ExecuteQuery("SELECT
                                                fogr_nombre,
                                                formulario_item.foit_id,
                                                foit_nombre,
                                                foit_tipo,
                                                fova_valor,
                                                foit_opciones
                                            FROM
                                                formulario_respuesta
                                            INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
                                            INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
                                            LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
                                            WHERE
                                                formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
                                            ORDER BY fogr_orden,foit_orden;");
                if( $res['status']==0 ){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }

                $data = array();

                foreach ($res['data'] as &$row) {
                    $row['fova_valor'] = $row['fova_valor'];//utf8_encode($row['fova_valor']);
                    
                    if(!isset($data[$row['fogr_nombre']])){
                        $data[$row['fogr_nombre']] = array();
                    }
                    if($row['foit_tipo']=="CAMERA"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }
                    if($row['foit_tipo']=="AGGREGATOR"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_opciones']!=""){
                        $row['foit_opciones'] = json_decode($row['foit_opciones'],true);
                    }

                    array_push($data[$row['fogr_nombre']],$row);   
                }
                $out['formularios'][$i]['data'] = $data;
            }

        }
    }
    
    Flight::json($out);
});

Flight::route('GET /contrato/@cont_id:[0-9]+/insp/informe/@insp_id:[0-9]+/validar/@info_id:[0-9]+', function($cont_id,$insp_id,$info_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    
    //Chequear si ya fue validado
    $res = $dbo->ExecuteQuery("SELECT 
                                info_estado
                               FROM 
                                informe
                               WHERE info_id=$info_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $info_estado = $res['data'][0]['info_estado'];
    if($info_estado!="SINVALIDAR"){
        Flight::json(array("status"=>0, "error"=>"Informe ya fue validado"));
        return;
    }


    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['inspeccion'] = $res['data'][0];


    $res = $dbo->ExecuteQuery("SELECT
                                    uc.usua_nombre AS usua_creador,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
                                LEFT JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
                                WHERE
                                    info_modulo='INSP' AND info_id=$info_id AND info_id_relacionado=$insp_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['informe'] = array();
    if(0<$res['rows']){
        $out['informe'] = $res['data'][0];

        $out['formularios'] = array();

        //Obtener visitas
        $info_data = json_decode($out['informe']['info_data'],true);
        if($info_data!=null){
            $tare_id = $info_data['tare_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                            rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                        FROM
                                            rel_tarea_formulario_respuesta
                                        INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                        INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
                                        WHERE
                                            tare_id=$tare_id");
            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }

            $out['formularios'] = $res['data'];

            for($i=0;$i<count($out['formularios']);$i++){
                $fore_id = $out['formularios'][$i]['fore_id'];

                if($out['formularios'][$i]['fore_ubicacion']!="" && $out['formularios'][$i]['fore_ubicacion']!="web"){
                    $out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
                }

                $res = $dbo->ExecuteQuery("SELECT
                                                fogr_nombre,
                                                formulario_item.foit_id,
                                                foit_nombre,
                                                foit_tipo,
                                                fova_valor,
                                                foit_opciones
                                            FROM
                                                formulario_respuesta
                                            INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
                                            INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
                                            LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
                                            WHERE
                                                formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
                                            ORDER BY fogr_orden,foit_orden;");
                if( $res['status']==0 ){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }

                $data = array();

                foreach ($res['data'] as &$row) {
                    $row['fova_valor'] = $row['fova_valor'];//utf8_encode($row['fova_valor']);

                    if(!isset($data[$row['fogr_nombre']])){
                        $data[$row['fogr_nombre']] = array();
                    }
                    if($row['foit_tipo']=="CAMERA"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }
                    if($row['foit_tipo']=="AGGREGATOR"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_opciones']!=""){
                        $row['foit_opciones'] = json_decode($row['foit_opciones'],true);
                    }

                    array_push($data[$row['fogr_nombre']],$row);   
                }
                $out['formularios'][$i]['data'] = $data;
            }

        }
    }
    
    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/informe/@insp_id:[0-9]+/validar/@info_id:[0-9]+', function($cont_id,$insp_id,$info_id){
    //session_start();
    $info_estado      = $_POST['info_estado'];
    $info_observacion = $_POST['info_observacion'];
    $usua_validador   = $_SESSION['user_id'];

    $dbo = new MySQL_Database();

    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("UPDATE informe SET
                                info_estado='$info_estado',
                                info_observacion='$info_observacion',
                                usua_validador='$usua_validador',
                                info_fecha_validacion=NOW()
                                WHERE
                                info_id='$info_id'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"INSP","VALIDAR_INFORME",$insp_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }
    

    $resEvent = Flight::AgregarEvento($dbo,"INSP","INFORME_VALIDADO",$insp_id,array("info_id"=>$info_id,"info_estado"=>$info_estado));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();
    Flight::json($res);
});

//____________________________________________________________
//Adjuntar
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/adjuntar/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    //detalle
    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    if( count($res['data'])==0 ){
        Flight::json(array("status"=>0, "error"=>"Inspeccion $insp_id no existe"));
        return;
    }
    
    $out['inspeccion'] = $res['data'][0];
    
    $empl_id = $out['inspeccion']['empl_id'];    
    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }
    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];
    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/adjuntar/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $db = new MySQL_Database();
    $db->startTransaction();

    $usua_creador           = $_SESSION['user_id'];
    if (isset($_FILES["archivos"])) { 
        $files = array(); 
        foreach($_FILES["archivos"] as $key1 => $value1) {
            foreach($value1 as $key2 => $value2) {
                $files[$key2][$key1] = $value2; 
            }
        }
        $descriptions = json_decode($_POST['archivos_descripciones'],true);

        for($i=0; $i<count($files); $i++){
            $nombre      = $files[$i]['name'];
            $descripcion = $descriptions[$i];

            $filename = date('ymdHis')."_insp_".$insp_id."_".str_replace(" ","_",$nombre);
            $resFile = Upload::UploadFile($files[$i],$filename); 
            if( !$resFile['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resFile['error']));
                return;
            }
            
            $query = "INSERT INTO repositorio (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
                        VALUES(
                            'DOCUMENTO',
                            '".$nombre."',
                            '".$descripcion."',
                            'inspeccion',
                            $insp_id,
                            '".$resFile['data']['filename']."',
                            NOW(),
                            NULL,
                            $usua_creador
                    )";
            $resUpload = $db->ExecuteQuery($query);
            if( $resUpload['status']==0 ) {
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resUpload['error']));
                return;
            }            
        }
    }

    $db->Commit();
    
    Flight::json(array("status" => 1));
});

//____________________________________________________________
//Cerrar
Flight::route('GET /contrato/@cont_id:[0-9]+/insp/cerrar/@insp_id:[0-9]+', function($cont_id,$insp_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleInspeccion($dbo,$cont_id,$insp_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['inspeccion'] = $res['data'][0];

    $empl_id = $out['inspeccion']['empl_id'];    
    $res = Flight::ObtenerDetalleEmplazamiento($dbo, $cont_id, $empl_id);
    if ($res['status'] == 0) { Flight::json(array("status" => 0, "error" => $res['error'])); return; }
    if (count($res['data']) == 0) {
        Flight::json(array("status" => 0, "error" => "Emplazamiento $empl_id no existe"));
        return;
    }
    $out['emplazamiento'] = $res['data'][0];

    $res = $dbo->ExecuteQuery("SELECT
                                    info_id,
                                    uc.usua_nombre AS usua_creador,
                                    info_fecha_creacion,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado,
                                    info_observacion
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id = informe.usua_creador)
                                LEFT JOIN usuario uv ON (uv.usua_id = informe.usua_validador)
                                WHERE info_modulo='INSP' AND info_id_relacionado=$insp_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['informe'] = $res['data'];


    Flight::json($out);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/insp/cerrar/@insp_id:[0-9]+', function($cont_id,$insp_id){
    //session_start();
    $insp_estado      = $_POST['insp_estado'];
    $insp_observacion = $_POST['insp_observacion'];
    $usua_validador   = $_SESSION['user_id'];

    $dbo = new MySQL_Database();

    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("UPDATE inspeccion SET
                                insp_estado='$insp_estado',
                                usua_validador='$usua_validador',
                                insp_fecha_validacion=NOW(),
                                insp_observacion='$insp_observacion'
                               WHERE
                                insp_id='$insp_id'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $res = $dbo->ExecuteQuery("UPDATE informe SET
                                info_estado='APROBADO'
                                WHERE
                                info_modulo='INSP' AND info_id_relacionado='$insp_id' AND info_estado='PREAPROBADO'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($dbo,"INSP","INSP_VALIDADA",$insp_id,array("insp_estado"=>$insp_estado));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"INSP","VALIDAR_INSP",$insp_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();
    Flight::json($res);
});


Flight::route('GET /contrato/@cont_id:[0-9]+/insp/resumen/filtros',function($cont_id){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $pais_id=Flight::get('PAIS_ID');

    $periodos = array("Bimestral"=>2,"Trimestral"=>3,"Semestral"=>6,"Anual"=>12);

    $out['periodo'] = array(); 
    $res = $dbo->ExecuteQuery("SELECT cont_fecha_inicio FROM contrato WHERE cont_id=" . $cont_id);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    if (0 < $res['rows']){
        $fecha_inicio_contrato = new DateTime($res['data'][0]['cont_fecha_inicio']);

        foreach ($periodos as $nombre => $meses) {
            $fecha_inicio = clone $fecha_inicio_contrato;
            while($fecha_inicio < new DateTime()){
                $peri_filtro = "BETWEEN '".$fecha_inicio->format('Y-m-d')."' AND ";
                $peri_nombre = $fecha_inicio->format('MY')." - ";

                $fecha_inicio->add(new DateInterval("P".$meses."M"));
                $fecha_inicio->sub(new DateInterval('P1D'));

                $peri_filtro.= "'".$fecha_inicio->format('Y-m-d')."'";
                $peri_nombre.= $fecha_inicio->format('MY')." ($nombre)";

                array_push($out['periodo'],array("peri_filtro"=>$peri_filtro,"peri_nombre"=>$peri_nombre));

                $fecha_inicio->add(new DateInterval('P1D'));
            }
        }
    }

    
    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CONTRATO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zona_contrato'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='MOVISTAR'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zona_movistar'] = $res['data'];

    /*$res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=".$pais_id." ORDER BY regi_orden ASC");*/
	$res = $dbo->ExecuteQuery("SELECT r.regi_id, r.regi_nombre FROM region r, contrato cn  WHERE cn.cont_id= " . $cont_id . "  AND cn.pais_id=r.pais_id  ORDER BY r.regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CLUSTER'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['cluster'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT * FROM comuna");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['comunas'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT DISTINCT usua_id, usua_nombre 
                                FROM usuario 
                                INNER JOIN inspeccion ON (inspeccion.usua_creador = usuario.usua_id)
                                WHERE inspeccion.cont_id=$cont_id");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['usuario'] = $res['data'];
    
    
    Flight::json($out);

});


Flight::route('POST /contrato/@cont_id:[0-9]+/insp/resumen/resumen/list',function($cont_id){
    $out = array();
    $out['status'] = 1;
    $out['campos'] = array();
    $out['datos']  = array();

    $dbo = new MySQL_Database();

    $res = Flight::ObtenerResumenInspeccionesResumen($dbo,$cont_id,$_POST);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    if(0<$res['rows']){
        $out['campos'] = array_keys($res['data'][0]);
        foreach($res['data'] as $row) {
            array_push($out['datos'],array_values($row));
        }
    }
    
    Flight::json($out);

});

Flight::route('POST /contrato/@cont_id:[0-9]+/insp/resumen/detalle/list',function($cont_id) {
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    
    $res = Flight::ObtenerResumenInspeccionesDetalle($dbo,$cont_id,$_POST);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['campos'] = array();
    $out['datos']  = array();
    if(0<$res['rows']){
        $out['campos'] = array_keys($res['data'][0]);
        foreach($res['data'] as $row) {
            array_push($out['datos'],array_values($row));
        }
    }
    
    Flight::json($out);
});

?>

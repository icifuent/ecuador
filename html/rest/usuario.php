<?php
	
	/*
	    Recupera el valor de los filtros que deben de ser mostrados a través de la pagina
	*/
	Flight::route('GET /core/usuario/contrato/@cont_id:[0-9]+/filtros', function($cont_id){
	
	    $out = array();
	    $dbo = new MySQL_Database();
	
	    $res = $dbo->ExecuteQuery(" SELECT  perf_id
	                                        , perf_nombre
	                                FROM perfil
	                                ORDER BY perf_nombre ASC");
	    if( 0==$res['status'] ) {
	        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
	    }
	    $out['perfiles'] = $res['data'];
	
	    $res = $dbo->ExecuteQuery(" SELECT  DISTINCT (empr.empr_id),
	                                        empr.empr_nombre
	                                FROM empresa empr, rel_contrato_empresa rcem
	                                WHERE empr.empr_id=rcem.empr_id
	                                    AND rcem.cont_id=$cont_id
	                                ORDER BY empr_nombre ASC");
	    if( 0 == $res['status'] ) {
	        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
	    }
	    $out['empresas'] = $res['data'];
	
	    $res = $dbo->ExecuteQuery("SHOW COLUMNS FROM usuario WHERE Field = 'usua_estado'" );
	    if (0 == $res['status']){
	        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	    }
	    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
	    $out['estados'] = explode("','", $matches[1]);
	
        $out['status'] = 1;
	    Flight::json($out);
	});
	
    /*
    *  Recupera el listado de los usuario usando los filtros desde la pagina    
    */
	Flight::route('GET|POST /core/usuario/contrato/@cont_id:[0-9]+/list(/@page:[0-9]+)', function($cont_id, $page){
	    $results_by_page = Flight::get('results_by_page');
	    $filtros_ini = array_merge($_GET,$_POST);
	    $filtros = "";
	
        /* Sacamos los filtros especiales del arreglo de filtros */
	    if( isset($filtros_ini['perf_id']) ){
	        if(is_array($filtros_ini['perf_id'])){
	          $filtros .= " AND p.perf_id IN ('".implode("','",$filtros_ini['perf_id'])."')";
	        } else {
	          $filtros .= " AND p.perf_id=".$filtros_ini['perf_id'];
	        }
	        unset( $filtros_ini['perf_id'] );
	    }
	
           
        /* Obtenemos el resto de filtros */
	    $filtros = Flight::filtersToWhereString( array("usuario"), $filtros_ini).$filtros;
	
	    $query = "SELECT
	                SQL_CALC_FOUND_ROWS
	                    u.usua_id
	                    ,u.empr_id
	                    ,u.usua_login
	                    ,u.usua_password
	                    ,u.usua_rut
	                    ,u.usua_alias
	                    ,u.usua_nombre
	                    ,u.usua_cargo
	                    ,u.usua_correo_electronico
	                    ,u.usua_telefono_fijo
	                    ,u.usua_telefono_movil
	                    ,u.usua_direccion
	                    ,u.usua_fecha_creacion
	                    ,u.usua_creador
	                    ,u.usua_acceso_web
	                    ,u.usua_acceso_movil
	                    ,u.usua_acceso_api
	                    ,u.usua_estado
	                    ,u.usua_opciones
	                    , e.empr_nombre
	              FROM
	                usuario u
	                , empresa e
	                , rel_contrato_usuario rcu
	              WHERE rcu.usua_id=u.usua_id
	              AND rcu.cont_id=$cont_id
	              AND u.empr_id = e.empr_id
	              AND ".$filtros." "
	              .((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
	
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery($query);
	    if (0 == $res['status']) {
	        Flight::json(array("status" => 0, "error" => $res['error']));
	    }
	
	    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
        if (0 == $res_count['status'] ) {
	        Flight::json(array("status" => 0, "error" => $res_count['error']));
	    }
	
	    $res['total'] = intval($res_count['data'][0]['total']);
	
	    if(!is_null($page)) {
	        $res['pagina'] = intval($page);
	        $res['paginas'] = ceil($res['total']/$results_by_page);
	    }
	    $res['filtros'] = $filtros_ini;
	
	    Flight::json($res);
	});

	/*Agrega un usuario desde el core>usuario*/	
	Flight::route('GET|POST /core/usuario/add', function($id){
	    global $BASEDIR;
	
	    $dbo = new MySQL_Database();
	    $dbo->startTransaction();
	    $data = array_merge($_GET,$_POST);
	    $perfiles = null;
	    $data["usua_fecha_creacion"]="NOW()";
	    require $BASEDIR."../libs/password.php";
	
        if( isset($data['perf_id']) && ""!=$data['perf_id'] ){  
	        $perfiles = $data['perf_id'];
	        unset($data['perf_id']);
	    }
	
	    $empr_id = $data["empr_id"];
	    $usua_nombre = $data["usua_nombre"];
	    $usua_alias = $data["usua_alias"];
	    $usua_rut = $data["usua_rut"];
	    $usua_login = $data["usua_login"];
	    $usua_crypt_pass= password_hash($usua_crypt_pass = $data["usua_password"], PASSWORD_DEFAULT);
	    $usua_password=$data["usua_password"];
	    $usua_cargo = $data["usua_cargo"];
	    $usua_correo_electronico = $data["usua_correo_electronico"];
	    $usua_telefono_fijo = $data["usua_telefono_fijo"];
	    $usua_telefono_movil = $data["usua_telefono_movil"];
	    $usua_direccion = $data["usua_direccion"];
	    $usua_estado = $data["usua_estado"];
	    $usua_creador = $data["usua_creador"];
        /* Acceso */
	    $usua_acceso_api = $data["usua_acceso_api"];
	    $usua_acceso_movil = $data["usua_acceso_movil"];
	    $usua_acceso_web = $data["usua_acceso_web"];
    	$cont_id = $_SESSION['cont_id'];

	    /*Validador de password / si existe en las excepciones no permite el ingreso */
	    $query = ("     SELECT exlo_palabra_clave
	                    FROM excepciones_login
	                    WHERE cont_id = $cont_id
	                    AND  exlo_estado = 'ACTIVO'
	                    AND  soundex(exlo_palabra_clave) =  soundex('$usua_password')");
	
	    $res = $dbo->ExecuteQuery($query);
	    if( 0 == $res['status'] ) {
	        $dbo->Rollback();
	        Flight::json(array("status" => 0, "error" => $res['error']));
	        return;
	    }
	     /*Si existe el registro arrojara un error */
	    if( 0 <$res['rows']){
			Flight::json(array("status"=>2,"rows"=>1,"data"=>array(array("status"=>2,"id"=>0)), "error"=>"error" ));
	        return;
	    }
	   
	    /*$query = "INSERT INTO usuario ".Flight::dataToInsertString($data);*/
	    $query = "INSERT INTO   usuario (   empr_id
	                                        ,usua_nombre
	                                        ,usua_alias
	                                        ,usua_rut
	                                        ,usua_login
	                                        ,usua_crypt_pass
	                                        ,usua_cargo
	                                        ,usua_correo_electronico
	                                        ,usua_telefono_fijo
	                                        ,usua_telefono_movil
	                                        ,usua_direccion
	                                        ,usua_fecha_creacion
	                                        ,usua_estado
	                                        ,usua_password
	                                        ,usua_acceso_api
	                                        ,usua_acceso_movil
	                                        ,usua_acceso_web
	                                        ,usua_creador
	                                    )
	                            VALUES  (
	                                        $empr_id
	                                        ,'$usua_nombre'
	                                        ,'$usua_alias'
	                                        ,'$usua_rut'
	                                        ,'$usua_login'
	                                        ,'$usua_crypt_pass'
	                                        ,'$usua_cargo'
	                                        ,'$usua_correo_electronico'
	                                        ,'$usua_telefono_fijo'
	                                        ,'$usua_telefono_movil'
	                                        ,'$usua_direccion'
	                                        ,NOW()
	                                        ,'$usua_estado'
	                                        ,'$usua_password'
	                                        ,$usua_acceso_api
	                                        ,$usua_acceso_movil
	                                        ,$usua_acceso_web
	                                        ,$usua_creador
	                                    )";
	
	    $res = $dbo->ExecuteQuery($query);
	    if (0 == $res['status']) {
	        $dbo->Rollback();
	        Flight::json(array("statu" => 0, "error" => $res['error']));
	        return;
	    }
	    $id = $res["data"][0]["id"];
	
	    if( null!=$perfiles ){
	        if( !is_array($perfiles) )  $perfiles = array($perfiles);
	        foreach ($perfiles as $perf_id) {
	            $res_perf = $dbo->ExecuteQuery("INSERT INTO rel_usuario_perfil (usua_id,perf_id) VALUES ($id,$perf_id)");
                if (0 == $res_perf['status'] ) {
	                $dbo->Rollback();
	                Flight::json(array("status" => 0, "error" => $res_perf['error']));
	                return;
	            }
	        }
	    }
	
	    $dbo->Commit();
	    Flight::json($res);
	});
	
	  
	/*funcion usuario NOACTIVO de core_usuario_lista*/
	Flight::route('POST /core/usuario/updEstado/@usua_id:[0-9]+/@usua_estado', function($usua_id,$usua_estado){
	     $dbo = new MySQL_Database();
	     $dbo->startTransaction();
	     $query="UPDATE usuario SET usua_estado = '".$usua_estado."' WHERE usua_id =".$usua_id;
	     $res = $dbo->ExecuteQuery($query);
	     /*control de error update usuario*/
	     if (0 == $res['status']) {
	         $dbo->Rollback();
	         Flight::json(array("status" => 0, "error" => "Error al cambio estado de usuario".$res['error']));
	         return;
	     }
	     $dbo->Commit();
	    /*$dbo->Disconnect();*/
	    /*$dbo->refresh();*/
	     Flight::json($res);
	
	});
	/*Actualiza los datos del usuario desde el core>usuario*/
	Flight::route('GET|POST /core/usuario/upd/@id:[0-9]+', function($id){
	
	    $dbo = new MySQL_Database();
	    $dbo->startTransaction();
	    $data = array_merge($_GET,$_POST);
	    $perfiles = null;
    	$cont_id = $_SESSION['cont_id'];

	    if( isset($data['perf_id']) && $data['perf_id']!="" ){
	        $res_perf = $dbo->ExecuteQuery("DELETE FROM rel_usuario_perfil WHERE usua_id=$id");
        if( 0==$res_perf['status'] ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res_perf['error']));
            return;
        }
        $perfiles = $data['perf_id'];
        unset($data['perf_id']);
    }
    if( 0 < count($data)  ){
            global $BASEDIR;
            require $BASEDIR."../libs/password.php";
            echo $BASEDIR."../libs/password.php";

            $usua_password = $data["usua_password"];

             /*Validador de password / si existe en las excepciones no permite el ingreso */
            $query = ("     SELECT exlo_palabra_clave
                            FROM excepciones_login
                            WHERE cont_id = $cont_id
                            AND  exlo_estado = 'ACTIVO'
                            AND soundex(exlo_palabra_clave) =  soundex('$usua_password')");

            $res = $dbo->ExecuteQuery($query);
            if( 0 == $res['status'] ) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
             /*Si existe el registro arrojara un error */
            if( 0 <$res['rows']){
               Flight::json(array("status"=>2,"rows"=>1,"data"=>array(array("status"=>2,"id"=>0)), "error"=>"error" ));
                return;
                
            }

            $query = "UPDATE usuario SET ".Flight::dataToUpdateString($data)." WHERE usua_id =".$id;
            $res = $dbo->ExecuteQuery($query);
            if (0 == $res['status']) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => "Error en los datos".$res['error']));
                return;
            }

            $password_encriptada = password_hash($usua_password, PASSWORD_DEFAULT);
            $res2 = $dbo->ExecuteQuery("UPDATE usuario 
                                        SET usua_password='$usua_password'
                                        ,usua_crypt_pass='$password_encriptada' 
                                        WHERE usua_id=$id");
            if (0 == $res2['status']) {
                $dbo->Rollback();
                Flight::json(array("status" => 0, "error" => "Error en la encriptacion password".$res2['error']));
                return;
            }
        }
	
	    if( null!=$perfiles ){
	        if( !is_array($perfiles) )  $perfiles = array($perfiles);
	        foreach ($perfiles as $perf_id) {
	            $res_perf = $dbo->ExecuteQuery("INSERT INTO rel_usuario_perfil (usua_id,perf_id) VALUES ($id,$perf_id)");
                if (0 == $res_perf['status']) {
	                $dbo->Rollback();
	                Flight::json(array("status" => 0, "error" => $res_perf['error']));
	                return;
	
	            }
	        }
	    }
	
	    $dbo->Commit();
	
	    Flight::json(array("status"=>1,"rows"=>1,"data"=>array(array("status"=>1,"id"=>0)), "error"=>"" ));
	});
	
	Flight::route('GET|POST /core/usuario/@id:[0-9]+/contrato/list(/@page:[0-9]+)', function($id, $page){
	    $results_by_page = Flight::get('results_by_page');
	    $filtros_ini = array_merge($_GET,$_POST);
	    $filtros = Flight::filtersToWhereString( array("usuario", "contrato"), $filtros_ini);
	
	    $dbo = new MySQL_Database();
	    $query = "SELECT c.cont_id
	                    ,c.cont_nombre
	                    ,c.cont_alias
	                    ,c.cont_fecha_inicio
	                    ,c.cont_fecha_termino
	                    ,c.cont_fecha_creacion
	                    ,c.cont_observacion
	                    ,c.cont_descripcion
	                    ,c.cont_estado
	                    ,c.usua_creador
	                    ,c.pais_id
	              FROM usuario u, contrato c, rel_contrato_usuario rcu
	              WHERE
	                    u.usua_id = ".$id."
					AND rcu.usua_id = u.usua_id
					AND rcu.cont_id = c.cont_id
					AND c.cont_estado='ACTIVO'
					AND $filtros ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
	    $res = $dbo->ExecuteQuery($query);
	    if (0 == $res['status']) {
	        Flight::json(array("status" => 0, "error" => $res['error']));
	    }
	
	    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
	    if ($res_count['status'] == 0) {
	        Flight::json(array("status" => 0, "error" => $res_count['error']));
	    }
	    $res['total'] = intval($res_count['data'][0]['total']);
	
	    if (!is_null($page)) {
	        $res['pagina'] = intval($page);
	        $res['paginas'] = ceil($res['total'] / $results_by_page);
	    }
	    Flight::json($res);
	});
		
	Flight::route('POST /core/usuario/@id:[0-9]+/cambio_clave', function($usua_id){
	    global $BASEDIR;
	    require $BASEDIR."../libs/password.php";
		$cont_id = $_SESSION['cont_id'];
	    $usua_password       = $_POST['usua_password'];
	    $usua_password_nuevo = $_POST['usua_password_nuevo'];
	    $db = new MySQL_Database();

	    /*Validador de password / si existe en las excepciones no permite el ingreso */
	    $query = ("     SELECT exlo_palabra_clave
                    FROM excepciones_login
                    WHERE cont_id = $cont_id
                    AND  exlo_estado = 'ACTIVO'
                    AND soundex(exlo_palabra_clave) =  soundex('$usua_password_nuevo')");

    	$res = $db->ExecuteQuery($query);
        if (0 == $res['status']){
	        $db->Rollback();
	        Flight::json(array("status" => 1, "error" => $res['error']));
	        return;
    	}
     	/*Si existe el registro arrojara un error */
    	if( 0 <$res['rows']){
       		Flight::json(array("status"=>2,"rows"=>1,"data"=>array(array("status"=>2,"id"=>0)), "error"=>"error" ));
        	return;
        }
	    /*
	    if($res['rows']==0){
	    	Flight::json(array("status" => 0, "error" => "Contraseï¿½a ingresada no corresponde"));
	            return;
	        }
	    */

    	/*usua_crypt_passs*/
        $password_encriptada = password_hash($usua_password_nuevo, PASSWORD_DEFAULT);

    $res = $db->ExecuteQuery("UPDATE usuario SET usua_crypt_pass='$password_encriptada', usua_password='$usua_password_nuevo' WHERE usua_id=$usua_id");
        Flight::json($res);
    });

	Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/usuario/list(/@pagina:[0-9]+)', function($cont_id,$pagina){
	    $results_by_page = Flight::get('results_by_page');
	    $filtros_ini     = array_merge($_GET,$_POST);
	
	    $filtros         = "";
	    if(isset($filtros_ini['usua_nombre']) && ""!=$filtros_ini['usua_nombre']){
	    	$filtros .= " AND usua_nombre LIKE '%".$filtros_ini['usua_nombre']."%'";
	    }
	
	    if(isset($filtros_ini['usua_estado']) && ""!=$filtros_ini['usua_estado']){
	    	$filtros .= " AND usua_estado='".$filtros_ini['usua_estado']."'";
	    }
	
	    /*Limitar acceso a usuarios de otras empresas*/
	    if(!in_array('SYS_ADMIN',$_SESSION['usua_cargo'])){
	    	$filtros .= " AND empresa.empr_id='".$_SESSION['empr_id']."'";
	    }
	
	    $limit = "";
	    if(null!=$pagina){
	    	$limit = " LIMIT ".$results_by_page." OFFSET ".(($pagina-1)*$results_by_page);
	    }
	
	    $db = new MySQL_Database();
	    $res = $db->ExecuteQuery("SELECT SQL_CALC_FOUND_ROWS
	        				    			usuario.usua_id
	                                        , usuario.empr_id
	                                        , usuario.usua_login
	                                        , usuario.usua_password
	                                        , usuario.usua_rut
	                                        , usuario.usua_alias
	                                        , usuario.usua_nombre
	                                        , usuario.usua_cargo
	                                        , usuario.usua_correo_electronico
	                                        , usuario.usua_telefono_fijo
	                                        , usuario.usua_telefono_movil
	                                        , usuario.usua_direccion
	                                        , usuario.usua_fecha_creacion
	                                        , usuario.usua_creador
	                                        , usuario.usua_acceso_web
	                                        , usuario.usua_acceso_movil
	                                        , usuario.usua_acceso_api
	                                        , usuario.usua_estado
	                                        , usuario.usua_opciones
	                                        , usuario.usua_crypt_pass
	                                        , empresa.empr_id
	                                        , empresa.comu_id
	                                        , empresa.empr_alias
	                                        , empresa.empr_nombre
	                                        , empresa.empr_rut
	                                        , empresa.empr_giro
	                                        , empresa.empr_direccion
	                                        , empresa.empr_fecha_creacion
	                                        , empresa.empr_observacion
	                                        , empresa.empr_contacto_nombre
	                                        , empresa.empr_contacto_email
	                                        , empresa.empr_contacto_telefono_fijo
	                                        , empresa.empr_contacto_telefono_movil
	                                        , empresa.empr_contacto_direccion
	                                        , empresa.empr_estado
	                                        , empresa.usua_creador
					              FROM rel_contrato_usuario
					              INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id)
					              INNER JOIN empresa ON (empresa.empr_id=usuario.empr_id)
					              WHERE
					              	rel_contrato_usuario.cont_id=$cont_id
					                $filtros
					              ORDER BY usua_nombre
					                $limit");
	    if (0 == $res['status']){
	    	Flight::json($res);
	    	return;
	    }
	
	    if(null!=$pagina){
		    $res_count = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
		    if ($res_count['status'] == 0){
		    	Flight::json($res_count);
		    	return;
		    }
		    $res['total'] = intval($res_count['data'][0]['total']);
	        $res['pagina'] = intval($pagina);
	        $res['paginas'] = ceil($res['total'] / $results_by_page);
	    }
	
	    $res['usua'] = array($_SESSION['usua_cargo'],$_SESSION['empr_id']);
	    Flight::json($res);
	});
	
	Flight::route('GET /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/*', function($id, $idu){
	
	    $query = "SELECT recu_id FROM rel_contrato_usuario WHERE cont_id=".$id." AND usua_id=".$idu;
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery($query);
		if ($res['rows'] == 0) {
	        Flight::json(array("status" => 0, "error" => "No se encontro el usuario en el contrato"));
	    }
	    Flight::set('recu_id', $res['data'][0]['recu_id']);
	    return true;
	});
	
	Flight::route('GET /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/modulo/list', function($id, $idu){
		$dbo = new DBObject();
		$res = $dbo->read("modulo m ,rel_contrato_usuario rcu ,rel_contrato_usuario_modulo rcum",
	                          "m.*",
	                          array("rcu.cont_id"=>"=".$id, "rcu.usua_id"=>"=".$idu, "rcu.recu_id"=>"=rcum.recu_id", "rcum.modu_id"=>"=m.modu_id") );
		Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/modulo/add/@idm:[0-9]+', function($id, $idu, $idm){
		$dbo = new DBObject();
		$res = $dbo->create("rel_contrato_usuario_modulo", array("recu_id"=>$res['data'][0]['recu_id'], "modu_id"=>$idm));
		Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/modulo/del/@idm:[0-9]+', function($id, $idu, $idm){
		$dbo = new DBObject();
		$res = $dbo->delete("rel_zona_emplazamiento", array("zona_id"=>"=".$idz, "empl_id"=>"=".$ide));
		Flight::json($res);
	});
	
	/*CONTRATO_USUARIO_NOTIFICACION*/
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idx:[0-9]+/notificacion/list', function($id, $idx){
	    $filtros_ini = array_merge($_GET,$_POST);
	    $filtros = Flight::filtersToWhereString( array("rel_contrato_usuario_notificacion"), $filtros_ini);
	    $recu_id = Flight::get('recu_id');
	    $query = " SELECT   rcun.rcun_id
	                        , rcun.recu_id
	                        , rcun.modu_alias
	                        , rcun.rcun_evento
	                        , rcun.rcun_accion
	               FROM rel_contrato_usuario_notificacion rcun
	               WHERE
	                    rcun.recu_id = $recu_id
	                    AND ".$filtros;
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/notificacion/add', function($id, $idu){
	    $recu_id = Flight::get('recu_id');
	    $data = array_merge($_GET,$_POST);
	    $data['recu_id'] = $recu_id;
	
	    $dbo = new MySQL_Database();
	    $query = "INSERT INTO rel_contrato_usuario_notificacion ".Flight::dataToInsertString($data);
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/notificacion/del/@idx:[0-9]+', function($id, $idu, $idx){
	    $recu_id = Flight::get('recu_id');
	
	    $dbo = new MySQL_Database();
	    $query = "DELETE FROM rel_contrato_usuario_notificacion WHERE recu_id=$recu_id AND rcun_id=$idx";
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	/*CONTRATO_USUARIO_ALCANCE
	CONTRATO_USUARIO_RESPONSABILIDAD
	CONTRATO_USUARIO_CLUSTER*/
	Flight::route('GET /core/contrato/@id:[0-9]+/usuario/@idx:[0-9]+/@subtabla/list', function($id, $idx, $subtabla){
	
	    if( $subtabla!="alcance" && $subtabla!="responsabilidad" && $subtabla!="cluster"){
		   Flight::notFound();
	    }
	
	    $query = "  SELECT  z.zona_id
	                        , z.cont_id
	                        , z.zona_nombre
	                        , z.zona_alias
	                        , z.zona_descripcion
	                        , z.zona_estado
	                        , z.zona_tipo
	                        , z.zona_padre
	                FROM zona z , rel_contrato_usuario rcu , rel_contrato_usuario_$subtabla rcux
	                WHERE
	                        rcu.cont_id = $id
	                        AND rcu.usua_id = $idx
	                        AND rcu.recu_id = rcux.recu_id
	                        AND rcux.zona_id = z.zona_id
	                        AND z.zona_tipo = '".strtoupper($subtabla)."' ";
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/@subtabla/add/@idx:[0-9]+', function($id, $idu, $subtabla, $idx){
	    if( $subtabla!="alcance" && $subtabla!="responsabilidad" && $subtabla!="cluster" ){
		   Flight::notFound();
	    }
	    $recu_id = Flight::get('recu_id');
	
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery("SELECT zona_id FROM zona WHERE zona_id=$idx AND zona_tipo='".strtoupper($subtabla)."'");
	    if($res['rows']==0 ){ Flight::json(array("status"=>0, "error"=>"La zona asignada no existe o no tiene el tipo correcto"));}
	
	
	    $query = "INSERT INTO rel_contrato_usuario_$subtabla (recu_id,zona_id) VALUES ($recu_id,$idx)";
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	Flight::route('GET|POST /core/contrato/@id:[0-9]+/usuario/@idu:[0-9]+/@subtabla/del/@idx:[0-9]+', function($id, $idu, $subtabla, $idx){
	    if( $subtabla!="alcance" && $subtabla!="responsabilidad" && $subtabla!="cluster" ){
		   Flight::notFound();
	    }
	    $recu_id = Flight::get('recu_id');
	
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery("SELECT zona_id FROM zona WHERE zona_id=$idx AND zona_tipo='".strtoupper($subtabla)."'");
	    if($res['rows']==0 ){ Flight::json(array("status"=>0, "error"=>"La zona asignada no existe o no tiene el tipo correcto"));}
	
	
	
	    $query = "DELETE FROM rel_contrato_usuario_$subtabla WHERE recu_id=$recu_id AND zona_id=$idx";
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	
	Flight::route('GET|POST /core/usuario/rel/contrato/@cont_id:[0-9]+/usuario/add/@usua_id:[0-9]+', function($cont_id, $usua_id){
	    /*rest/core/contrato/' + cont_id + '/usuario/'+ ((addOrDel)?"add":"del") + '/' + usua_id*/
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery("SELECT recu_id FROM rel_contrato_usuario WHERE cont_id=$cont_id AND usua_id=$usua_id LIMIT 1");
	    if( 0 != $res['status'] ){
	        if(0 == $res['rows']){
	        $res = $dbo->ExecuteQuery("INSERT INTO rel_contrato_usuario(usua_id, cont_id, recu_estado)
	                                values ($usua_id, $cont_id, 'ACTIVO')");
	        }else{
	            $recu_id=$res['data'][0]['recu_id'];
	            $res = $dbo->ExecuteQuery("UPDATE rel_contrato_usuario SET recu_estado='ACTIVO' WHERE recu_id=$recu_id ");
	        }
	    }
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/usuario/rel/contrato/@cont_id:[0-9]+/usuario/del/@usua_id:[0-9]+', function($cont_id, $usua_id){
	    /*rest/core/contrato/' + cont_id + '/usuario/'+ ((addOrDel)?"add":"del") + '/' + usua_id*/
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery("SELECT recu_id FROM rel_contrato_usuario WHERE cont_id=$cont_id AND usua_id=$usua_id");
	    if( 0 != $res['status'] ){
	        if( 0 != $res['rows']){
	            $recu_id=$res['data'][0]['recu_id'];
	            $res = $dbo->ExecuteQuery("UPDATE rel_contrato_usuario SET recu_estado='NOACTIVO' WHERE recu_id=$recu_id ");
	
	        }
	    }
	    Flight::json($res);
	});
	
	/*RECUPERAR PERFILES
	http://siom.movistar.cl/rest/core/usuario/65/perfil/list*/
	Flight::route('GET|POST /core/usuario/@usua_id:[0-9]+/perfil/list', function($usua_id){
	    //rest/core/contrato/' + cont_id + '/usuario/'+ ((addOrDel)?"add":"del") + '/' + usua_id
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery(" SELECT   perf.perf_id
	                                       , perf.perf_nombre
	                                FROM usuario u
	                                    , perfil perf
	                                    , rel_usuario_perfil rup
	                                WHERE
	                                        u.usua_id = $usua_id
	                                        AND rup.usua_id = u.usua_id
	                                        AND perf.perf_id = rup.perf_id"
	    );
	    Flight::json($res);
	});
	
	
	
	/* /core/rel_usuario_movil/list*/
	Flight::route('GET|POST /core/rel_usuario_movil/usuario/@usua_id:[0-9]+/list', function($usua_id){
	    //rest/core/contrato/' + cont_id + '/usuario/'+ ((addOrDel)?"add":"del") + '/' + usua_id
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery(" SELECT      reum_id
	                                            ,usua_id
	                                            ,reum_imei
	                                            ,reum_estado
	                                FROM rel_usuario_movil
	                                WHERE
	                                     usua_id = $usua_id"
	    );
	    if(0 == $res['rows'] ){
	        Flight::json(array("status"=>0, "error"=>"La zona asignada no existe o no tiene el tipo correcto"));
	    }
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/usuario/imei/add', function($usua_id){
	    $dbo = new MySQL_Database();
	    $dbo->startTransaction();
	    $data = array_merge($_GET,$_POST);
	
	    $query = "INSERT INTO rel_usuario_movil ".Flight::dataToInsertString($data);
	    $res = $dbo->ExecuteQuery($query);
	    if (0 == $res['status']) {
	        $dbo->Rollback();
	        Flight::json(array("status" => 0, "error" => $res['error']));
	        return;
	    }
	    $id = $res["data"][0]["id"];
	    $dbo->Commit();
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/usuario/imei/upd/@reum_id:[0-9]+/@reum_estado', function($reum_id, $reum_estado){
	    $dbo = new MySQL_Database();
	    $dbo->startTransaction();
	    $data = array_merge($_GET,$_POST);
	    $query = "UPDATE rel_usuario_movil SET reum_estado='$reum_estado' WHERE reum_id=$reum_id";
	    $res = $dbo->ExecuteQuery($query);
	    if (0 == $res['status']) {
	        $dbo->Rollback();
	        Flight::json(array("status" => 0, "error" => $res['error']));
	        return;
	    }
	
	    $dbo->Commit();
	    Flight::json($res);
	});
?>


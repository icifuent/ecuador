<?php

Flight::set('flight.log_errors', true);

// <editor-fold defaultstate="collapsed" desc="PARAMETROS SLA POR CONTRATO"> 
Flight::set('SLA_PARAMETROS', array(
    // cont_id = 1
    1 => array(     
        'SLA_TPO_RESP_UMBRAL_URGENTE' => 4,
        'SLA_TPO_RESP_UMBRAL_NORMAL' => 24,
        'SLA_TPO_RESP_UMBRAL_PPTO_ENTREGA' => 24,
        'SLA_TPO_RESP_UMBRAL_PPTO_VALIDACION' => 48,
        'SLA_TPO_RESP_UMBRAL_INFORME_ENTREGA' => 24,
        'SLA_TPO_RESP_UMBRAL_INFORME_VALIDACION' => 48,
        'SLA_TPO_RESP_UMBRAL_SOLUCION_FINAL' => 12,
        'SLA_EVALUACION_PESOS_NOTAS' => array(
                                            array(
                                                'nombre' => 'ejecucion_mpp',
                                                'texto' => 'Ejecucion Mantenimiento Preventivo Periodico',
                                                'peso_espe' => 50,
                                                'item' => array(
                                                    array(
                                                        'nombre' => 'aa_ab_ac_ad'
                                                        ,'texto' => 'AA,AB,AC,AD'
                                                        ,'peso_espe' => 60
                                                    ),
                                                    array(
                                                        'nombre' => 'ba_bb_bc_bd'
                                                        ,'texto' => 'BA,BB,BC,BD'
                                                        ,'peso_espe' => 30
                                                    ),
                                                    array(
                                                        'nombre' => 'cronograma_mpp'
                                                        ,'texto' => 'Cronograma MPP'
                                                        ,'peso_espe' => 10
                                                    )
                                                )
                                            ),
                                            array(
                                                'nombre' => 'sla',
                                                'texto' => 'Cumplimiento SLAs',
                                                'peso_espe' => 20,
                                                'item' => array(
                                                    array(
                                                        'nombre' => 'osu'
                                                        ,'texto' => 'OS urgente'
                                                        ,'peso_espe' => 70
                                                    ),
                                                    array(
                                                        'nombre' => 'osn'
                                                        ,'texto' => 'OS normal&nbsp;'
                                                        ,'peso_espe' => 30
                                                    )
                                                )
                                            ),
                                            array(
                                                'nombre' => 'calidad',
                                                'texto' => 'Calidad',
                                                'peso_espe' => 20,
                                                'item' => array(
                                                    array(
                                                        'nombre' => 'auditoria_mpp'
                                                       ,'texto' => 'Auditoria MPP'
                                                       ,'peso_espe' => 60
                                                    ),
                                                    array(
                                                        'nombre' => 'auditoria_os'
                                                        ,'texto' => 'Auditoria OS'
                                                        ,'peso_espe' => 40
                                                    )
                                                )
                                            ),
                                            array(
                                                'nombre' => 'pprr_imagen',
                                                'texto' => 'PPRR e imagen',
                                                'peso_espe' => 10,
                                                'item' => array(
                                                    array(
                                                        'nombre' => 'auditoria_pprr'
                                                        ,'texto' => 'Auditoria PPRR<br>(examenes ocupacionales<br>vigentes ,curso vigente)'
                                                        ,'peso_espe' => 60
                                                    ),
                                                    array(
                                                        'nombre' => 'auditoria_actualizado'
                                                        ,'texto' => 'Personal <br>(nomina actualizada)'
                                                        ,'peso_espe' => 30
                                                    ),
                                                    array(
                                                        'nombre' => 'presentacion_personal'
                                                        ,'texto' => 'Presentacion e <br>imagen de Personal<br>y reclamos'
                                                        ,'peso_espe' => 10
                                                    )
                                                )
                                            )
                                        ),
        'SLA_EVALUACION_ESCALA_NOTAS' => array(
                                            array(
                                                array(
                                                    'nombre' => 'ejecucion_mpp',
                                                    'nota' => '10',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'aa_ab_ac_ad'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '100'
                                                        ),
                                                        array(
                                                            'nombre' => 'ba_bb_bc_bd'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '100'
                                                        ),
                                                        array(
                                                            'nombre' => 'cronograma_mpp'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '100'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'sla',
                                                    'nota' => '10',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'osu'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        ),
                                                        array(
                                                            'nombre' => 'osn'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '90'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'calidad',
                                                    'nota' => '10',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_mpp'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_os'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '90'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'pprr_imagen',
                                                    'nota' => '10',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_pprr'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '100'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_actualizado'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        ),
                                                        array(
                                                            'nombre' => 'presentacion_personal'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        )
                                                    )
                                                )
                                            ), array(
                                                array(
                                                    'nombre' => 'ejecucion_mpp',
                                                    'nota' => '9',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'aa_ab_ac_ad'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        ),
                                                        array(
                                                            'nombre' => 'ba_bb_bc_bd'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        ),
                                                        array(
                                                            'nombre' => 'cronograma_mpp'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'sla',
                                                    'nota' => '9',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'osu'
                                                            , 'tasa_max' => '95'
                                                            , 'tasa_min' => '90'
                                                        ),
                                                        array(
                                                            'nombre' => 'osn'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'calidad',
                                                    'nota' => '9',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_mpp'
                                                            , 'tasa_max' => '95'
                                                            , 'tasa_min' => '90'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_os'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'pprr_imagen',
                                                    'nota' => '9',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_pprr'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '95'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_actualizado'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '90'
                                                        ),
                                                        array(
                                                            'nombre' => 'presentacion_personal'
                                                            , 'tasa_max' => '100'
                                                            , 'tasa_min' => '90'
                                                        )
                                                    )
                                                )
                                            ),
                                            array(
                                                array(
                                                    'nombre' => 'ejecucion_mpp',
                                                    'nota' => '8',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'aa_ab_ac_ad'
                                                            , 'tasa_max' => '95'
                                                            , 'tasa_min' => '90'
                                                        ),
                                                        array(
                                                            'nombre' => 'ba_bb_bc_bd'
                                                            , 'tasa_max' => '95'
                                                            , 'tasa_min' => '90'
                                                        ),
                                                        array(
                                                            'nombre' => 'cronograma_mpp'
                                                            , 'tasa_max' => '95'
                                                            , 'tasa_min' => '90'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'sla',
                                                    'nota' => '8',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'osu'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        ),
                                                        array(
                                                            'nombre' => 'osn'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '83'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'calidad',
                                                    'nota' => '8',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_mpp'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_os'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '83'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'pprr_imagen',
                                                    'nota' => '8',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_pprr'
                                                            , 'tasa_max' => '95'
                                                            , 'tasa_min' => '90'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_actualizado'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        ),
                                                        array(
                                                            'nombre' => 'presentacion_personal'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        )
                                                    )
                                                )
                                            ),
                                            array(
                                                array(
                                                    'nombre' => 'ejecucion_mpp',
                                                    'nota' => '7',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'aa_ab_ac_ad'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        ),
                                                        array(
                                                            'nombre' => 'ba_bb_bc_bd'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        ),
                                                        array(
                                                            'nombre' => 'cronograma_mpp'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '85'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'sla',
                                                    'nota' => '7',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'osu'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '83'
                                                        ),
                                                        array(
                                                            'nombre' => 'osn'
                                                            , 'tasa_max' => '83'
                                                            , 'tasa_min' => '80'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'calidad',
                                                    'nota' => '7',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_mpp'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '83'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_os'
                                                            , 'tasa_max' => '83'
                                                            , 'tasa_min' => '80'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'pprr_imagen',
                                                    'nota' => '7',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_pprr'
                                                            , 'tasa_max' => '90'
                                                            , 'tasa_min' => '80'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_actualizado'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '80'
                                                        ),
                                                        array(
                                                            'nombre' => 'presentacion_personal'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '80'
                                                        )
                                                    )
                                                )
                                            ), array(
                                                array(
                                                    'nombre' => 'ejecucion_mpp',
                                                    'nota' => '6',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'aa_ab_ac_ad'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '80'
                                                        ),
                                                        array(
                                                            'nombre' => 'ba_bb_bc_bd'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '80'
                                                        ),
                                                        array(
                                                            'nombre' => 'cronograma_mpp'
                                                            , 'tasa_max' => '85'
                                                            , 'tasa_min' => '80'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'sla',
                                                    'nota' => '6',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'osu'
                                                            , 'tasa_max' => '83'
                                                            , 'tasa_min' => '80'
                                                        ),
                                                        array(
                                                            'nombre' => 'osn'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '70'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'calidad',
                                                    'nota' => '6',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_mpp'
                                                            , 'tasa_max' => '83'
                                                            , 'tasa_min' => '80'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_os'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '70'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'pprr_imagen',
                                                    'nota' => '6',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_pprr'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '70'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_actualizado'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '70'
                                                        ),
                                                        array(
                                                            'nombre' => 'presentacion_personal'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '70'
                                                        )
                                                    )
                                                )
                                            ),
                                            array(
                                                array(
                                                    'nombre' => 'ejecucion_mpp',
                                                    'nota' => '1',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'aa_ab_ac_ad'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '0'
                                                        ),
                                                        array(
                                                            'nombre' => 'ba_bb_bc_bd'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '0'
                                                        ),
                                                        array(
                                                            'nombre' => 'cronograma_mpp'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '0'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'sla',
                                                    'nota' => '1',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'osu'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '0'
                                                        ),
                                                        array(
                                                            'nombre' => 'osn'
                                                            , 'tasa_max' => '70'
                                                            , 'tasa_min' => '0'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'calidad',
                                                    'nota' => '1',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_mpp'
                                                            , 'tasa_max' => '80'
                                                            , 'tasa_min' => '0'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_os'
                                                            , 'tasa_max' => '70'
                                                            , 'tasa_min' => '0'
                                                        )
                                                    )
                                                ),
                                                array(
                                                    'nombre' => 'pprr_imagen',
                                                    'nota' => '1',
                                                    'item' => array(
                                                        array(
                                                            'nombre' => 'auditoria_pprr'
                                                            , 'tasa_max' => '70'
                                                            , 'tasa_min' => '0'
                                                        ),
                                                        array(
                                                            'nombre' => 'auditoria_actualizado'
                                                            , 'tasa_max' => '70'
                                                            , 'tasa_min' => '0'
                                                        ),
                                                        array(
                                                            'nombre' => 'presentacion_personal'
                                                            , 'tasa_max' => '70'
                                                            , 'tasa_min' => '0'
                                                        )
                                                    )
                                                )
                                            )
                                        ),
        
        'SLA_EVALUACION_ESCALA_CALIFICACION' => array(
                                                    array(
                                                        'calificacion' => 'Excelente'
                                                        ,'nota_max' => 10
                                                        ,'nota_min' => 9.5
                                                    ),
                                                    array(
                                                        'calificacion' => 'Bueno'
                                                        ,'nota_max' => 9.5
                                                        ,'nota_min' => 8.5
                                                    ),
                                                    array(
                                                        'calificacion' => 'Regular'
                                                        ,'nota_max' => 8.5
                                                        ,'nota_min' => 7.5
                                                    ),
                                                    array(
                                                        'calificacion' => 'Insuficiente'
                                                        ,'nota_max' => 7.5
                                                        ,'nota_min' => 0
                                                    )           
                                                )
                                           
        
                                       
    )
));
// </editor-fold>

//Helpers_______________________________________________________________________

Flight::map('ObtenerSLADisponibilidad', function($cont_id,$filtros_ini){
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    $filtros = "TRUE ";
    $filtrosEspecialidad = " ";
    $filtrosZonaConsultada = " ";
    $filtrosTipoZona = " ";
    $filtrosFecha = " ";
    $todasEspecialidades = false;
       
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND sla.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
            $filtrosEspecialidad .= " AND sos.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND sla.espe_id = '".$filtros_ini['espe_id']."' ";
            $filtrosEspecialidad .= " AND sos.espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }     
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }    
        
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND zona.regi_id = ".$filtros_ini['regi_id'];
    }
    
    
    if( $isFiltroPorZona ){
        $query_equi = "SELECT 
                                sla.espe_id
                                ,sla.espe_nombre
                                ,sla.zona_id
                                ,sla.zona_nombre
                                ,sla.slpf_total_equipos as total_equipos
                                ,sla.slpf_tolerancia_fallas as tolerancia_fallas
                                ,sla.slpf_tolerancia_fallas_reiteradas as tolerancia_fallas_reiteradas
                        FROM 
                                sla_parametros_fallas sla
                                INNER JOIN zona ON (zona.cont_id = $cont_id AND sla.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                        WHERE
                                sla.cont_id = $cont_id
                                AND sla.zona_id IS NOT NULL
                                AND $filtros
                        GROUP BY sla.zona_id, espe_id
                        ORDER BY sla.zona_id ASC, espe_id ASC
                        ;";
        
        if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
            $filtrosFecha .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
            $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ;
        }

        if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
            $filtrosFecha .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ;
            $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        } 
        
        $query_orse =  "SELECT  zona.zona_id
                                , zona.zona_nombre
                                , espe_id
                                , espe_nombre
                                , ifnull(total_os_indisponibilidad,0) total_os_indisponibilidad
                                , ifnull(total_os_indisponibilidad_horas,0) total_os_indisponibilidad_horas
                                , ifnull(os_fecha_periodo_minima, 0) os_fecha_periodo_minima
                                , ifnull(os_fecha_periodo_maxima,0) os_fecha_periodo_maxima
                        FROM zona
                        LEFT JOIN (
                                    SELECT  
                                            sos.espe_id
                                            ,sos.espe_nombre
                                            ,zona_consultada.zona_id
                                            ,zona_consultada.zona_nombre
                                            ,COUNT(1) AS total_os_indisponibilidad
                                            ,SUM( IF((FLOOR(sos.sla_disponibilidad/3600)-sos.sla_disponibilidad_ajuste)>=0,(FLOOR(sos.sla_disponibilidad/3600)-sos.sla_disponibilidad_ajuste),0) ) AS total_os_indisponibilidad_horas
                                            ,MIN( sos.orse_fecha_creacion ) AS os_fecha_periodo_minima
                                            ,NOW() AS os_fecha_periodo_maxima
                                    FROM sla_orden_servicio sos 
                                        INNER JOIN emplazamiento e ON  (sos.empl_id = e.empl_id)
                                        INNER JOIN rel_contrato_emplazamiento rce ON (rce.empl_id = e.empl_id AND rce.cont_id = sos.cont_id)
                                        INNER JOIN  rel_zona_emplazamiento rze ON (rze.empl_id = e.empl_id)
                                        INNER JOIN zona zona_consultada ON (zona_consultada.zona_id = rze.zona_id 
                                                                            AND zona_consultada.cont_id =sos.cont_id
                                                                            AND zona_consultada.zona_estado = 'ACTIVO' 
                                                                            $filtrosZonaConsultada #### valor que entra por parametro 
                                        ) 
                                    WHERE sos.cont_id = $cont_id 
                                        AND sos.orse_indisponibilidad IN ('SI','PARCIAL')
                                        AND sos.sla_disponibilidad_exclusion = 0 
                                        $filtrosEspecialidad
                                        $filtrosFecha
                                    GROUP BY zona_consultada.zona_id
                                            ,zona_consultada.zona_nombre
                        ) x ON x.zona_id = zona.zona_id
                        WHERE zona.cont_id = $cont_id  #### valor que entra por parametro
                            AND zona.zona_estado = 'ACTIVO' 
                        $filtrosTipoZona 
        ";
/*
        $query_orse = " SELECT 
                                sla.espe_id
                                ,sla.espe_nombre
                                ,zona.zona_id
                                ,zona.zona_nombre
                                ,COUNT(1) AS total_os_indisponibilidad
                                ,SUM( IF((FLOOR(sla.sla_disponibilidad/3600)-sla.sla_disponibilidad_ajuste)>=0,(FLOOR(sla.sla_disponibilidad/3600)-sla.sla_disponibilidad_ajuste),0) ) AS total_os_indisponibilidad_horas
                                ,MIN( sla.orse_fecha_creacion ) as os_fecha_periodo_minima
                                ,NOW() AS os_fecha_periodo_maxima
                        FROM
                                sla_orden_servicio sla 
                                INNER JOIN (  
                                    SELECT empl.empl_id
                                    FROM 
                                        rel_contrato_usuario rcu 
                                        INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                        INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                        INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                    WHERE 
                                        rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                    GROUP BY empl_id
                                ) alcance ON (alcance.empl_id = sla.empl_id)
                                
                                INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                                INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                        WHERE
                                sla.cont_id = $cont_id
                                AND sla.orse_indisponibilidad IN ('SI','PARCIAL')
                                AND sla.sla_disponibilidad_exclusion = 0
                                AND $filtros
                        GROUP BY  zona.zona_id, sla.espe_id
                        ORDER BY zona.zona_id ASC, sla.espe_id ASC
        ";
        */
    } else {
        $query_equi = "SELECT 
                                sla.espe_id
                                ,sla.espe_nombre
                                ,sla.regi_id AS zona_id
                                ,r.regi_nombre AS zona_nombre
                                ,sla.slpf_total_equipos as total_equipos
                                ,sla.slpf_tolerancia_fallas as tolerancia_fallas
                                ,sla.slpf_tolerancia_fallas_reiteradas as tolerancia_fallas_reiteradas
                        FROM 
                                sla_parametros_fallas sla
                                INNER JOIN region r ON sla.regi_id = r.regi_id
                        WHERE
                                cont_id = $cont_id
                                AND sla.regi_id IS NOT NULL
                                AND $filtros
                        GROUP BY sla.regi_id , espe_id
                        ORDER BY regi_orden ASC, espe_id ASC
                        ;";
                
        if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
            $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        }

        if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
            $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        }

        $query_orse = "  SELECT  region.regi_id as zona_id
                            , region.regi_nombre as zona_nombre
                            , espe_id
                            , espe_nombre
                            , ifnull(total_os_indisponibilidad,0) total_os_indisponibilidad
                            , ifnull(total_os_indisponibilidad_horas,0) total_os_indisponibilidad_horas
                            , ifnull(os_fecha_periodo_minima, 0) os_fecha_periodo_minima
                            , ifnull(os_fecha_periodo_maxima,0) os_fecha_periodo_maxima
                    FROM region
                    INNER JOIN pais ON pais.pais_id = region.pais_id
                    INNER JOIN contrato cont ON cont.pais_id =  pais.pais_id
                    LEFT JOIN (
                                SELECT  
                                        r.regi_id
                                        ,r.regi_nombre
                                        , sos.espe_id
                                        , sos.espe_nombre
                                        ,COUNT(1) AS total_os_indisponibilidad
                                        ,SUM( IF((FLOOR(sos.sla_disponibilidad/3600)-sos.sla_disponibilidad_ajuste)>=0,(FLOOR(sos.sla_disponibilidad/3600)-sos.sla_disponibilidad_ajuste),0) ) AS total_os_indisponibilidad_horas
                                        ,MIN( sos.orse_fecha_creacion ) AS os_fecha_periodo_minima
                                        ,NOW() AS os_fecha_periodo_maxima
                                FROM sla_orden_servicio sos 
                                INNER JOIN emplazamiento e ON  (sos.empl_id = e.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce ON (rce.empl_id = e.empl_id AND rce.cont_id = sos.cont_id)
                                INNER JOIN comuna c ON (c.comu_id = e.comu_id)
                                INNER JOIN provincia p ON (p.prov_id = c.prov_id)
                                INNER JOIN region r ON (r.regi_id = p.regi_id)
                                WHERE sos.cont_id = $cont_id #### valor que entra por parametro
                                AND sos.orse_indisponibilidad IN ('SI','PARCIAL')
                                        AND sos.sla_disponibilidad_exclusion = 0 
                                        $filtrosEspecialidad
                                        $filtrosFecha
                                GROUP BY r.regi_id
                                        ,r.regi_nombre
                    ) x ON (x.regi_id = region.regi_id)
                    WHERE cont.cont_id = $cont_id  #### valor que entra por parametro
        ";
        /*
        $query_orse = "
                        SELECT 
                                sla.espe_id
                                ,sla.espe_nombre
                                ,sla.regi_id AS zona_id
                                ,sla.regi_nombre AS zona_nombre
                                ,COUNT(1) AS total_os_indisponibilidad
                                ,SUM( IF((FLOOR(sla.sla_disponibilidad/3600)-sla.sla_disponibilidad_ajuste)>=0,(FLOOR(sla.sla_disponibilidad/3600)-sla.sla_disponibilidad_ajuste),0) ) AS total_os_indisponibilidad_horas
                                ,MIN( sla.orse_fecha_creacion ) as os_fecha_periodo_minima
                                ,NOW() AS os_fecha_periodo_maxima
			FROM
                                sla_orden_servicio sla  
                                INNER JOIN (  
                                    SELECT empl.empl_id
                                    FROM 
                                        rel_contrato_usuario rcu 
                                        INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                        INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                        INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                    WHERE 
                                        rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                    GROUP BY empl_id
                                ) alcance ON (alcance.empl_id = sla.empl_id)
                                
                                INNER JOIN region regi ON sla.regi_id = regi.regi_id
                        WHERE
                                sla.cont_id = $cont_id
                                AND sla.orse_indisponibilidad IN ('SI','PARCIAL')
                                AND sla.sla_disponibilidad_exclusion = 0
                                AND $filtros
                        GROUP BY sla.regi_id, sla.espe_id
                        ORDER BY regi.regi_orden ASC, sla.espe_id ASC
                        ;"; 
        */
    }
        
    $dbo = new MySQL_Database();    
    //Flight::json(array("status" => 0, "error" => $query_empl));
    $res = $dbo->ExecuteQuery($query_equi);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];
    
    $res = $dbo->ExecuteQuery($query_orse);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $detalle_orses = $res['data'];
    
    foreach ($out AS &$detalle) {
        $detalle['total_os_indisponibilidad'] = 0;
        $detalle['total_os_indisponibilidad_horas'] = 0;
        $detalle['os_fecha_periodo_minima'] = '';
        $detalle['os_fecha_periodo_maxima'] = '';
                
        foreach ($detalle_orses AS $detalle_orse) {
            if( $detalle_orse['espe_id']==$detalle['espe_id'] && $detalle_orse['zona_id']==$detalle['zona_id'] ){
                $detalle['total_os_indisponibilidad'] = $detalle_orse['total_os_indisponibilidad'];
                $detalle['total_os_indisponibilidad_horas'] = $detalle_orse['total_os_indisponibilidad_horas'];
                $detalle['os_fecha_periodo_minima'] = $detalle_orse['os_fecha_periodo_minima'];
                $detalle['os_fecha_periodo_maxima'] = $detalle_orse['os_fecha_periodo_maxima'];
                break;
            }
        }
        if (isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio'] != "") {
            $detalle['os_fecha_periodo_minima'] = $filtros_ini['orse_fecha_validacion_inicio'];
        }
        if (isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino'] != "") {
            $detalle['os_fecha_periodo_maxima'] = $filtros_ini['orse_fecha_validacion_termino'];
        }
        
        $detalle['total_horas_periodo'] = round((strtotime($detalle['os_fecha_periodo_maxima']) - strtotime($detalle['os_fecha_periodo_minima']))/(3600))*$detalle['total_equipos'];
        $detalle['tasa_disponibilidad'] = ($detalle['total_horas_periodo']>0)?round((1-$detalle['total_os_indisponibilidad_horas']/$detalle['total_horas_periodo'])*100,2):100;
    }
     
//    if ($todasEspecialidades && count($out > 0)) {
//        $out_filtered = array();
//        $out_filtered[] = $out[0];
//        $out_filtered[0]['espe_id'] = 0;
//        $out_filtered[0]['espe_nombre'] = 'TODAS';
//
//        for ($i = 1; $i < count($out); $i++) {
//            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
//                //$out_filtered[count($out_filtered) - 1]['total_emplazamientos'] = $out_filtered[count($out_filtered) - 1]['total_emplazamientos'] + $out[$i]['total_emplazamientos'];
//                $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad'] = $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad'] + $out[$i]['total_os_indisponibilidad'];
//                $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad_horas'] = $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad_horas'] + $out[$i]['total_os_indisponibilidad_horas'];
//           } else {
//                $out_filtered[] = $out[$i];
//                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
//                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
//            }
//        }
//        
//        $out = $out_filtered;
//        foreach ($out AS &$detalle) {      
//            //$detalle['tasa_falla'] = ($detalle['total_emplazamientos']>0)?round($detalle['total_os_indisponibilidad']/$detalle['total_emplazamientos']*100,2):0;
//            $detalle['tasa_disponibilidad'] = ($detalle['total_horas_periodo']>0)?round((1-$detalle['total_os_indisponibilidad_horas']/$detalle['total_horas_periodo'])*100,2):100;
//        }
//    }
    
    return $out;

});

Flight::map('ObtenerSLAFallas', function($cont_id,$filtros_ini){
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    $filtros = "TRUE ";
    $filtros_equipos = "TRUE ";
    $todasEspecialidades = false;
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND sos.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
            $filtros_equipos .= " AND spf.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        } else {
            $filtros .= " AND sos.espe_id = '".$filtros_ini['espe_id']."' ";
            $filtros_equipos .= " AND spf.espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }     
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
        
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtros_equipos .= " AND spf.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtros_equipos .= " AND spf.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND sos.regi_id = ".$filtros_ini['regi_id'];
        $filtros_equipos .= " AND spf.regi_id = ".$filtros_ini['regi_id'];
    }
    
    if( $isFiltroPorZona ){
        
        $query_equi = "SELECT 
                                spf.espe_id
                                ,spf.espe_nombre
                                ,spf.zona_id
                                ,spf.zona_nombre
                                ,spf.slpf_total_equipos as total_equipos
                                ,spf.slpf_tolerancia_fallas as tolerancia_fallas
                                ,spf.slpf_tolerancia_fallas_reiteradas as tolerancia_fallas_reiteradas
                        FROM 
                                sla_parametros_fallas spf
                                INNER JOIN zona ON (zona.cont_id = $cont_id AND spf.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                        WHERE
                                spf.cont_id = $cont_id
                                AND spf.zona_id IS NOT NULL
                                AND $filtros_equipos
                        GROUP BY spf.zona_id, espe_id
                        ORDER BY spf.zona_id ASC, espe_id ASC
                        ;";
        
        if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
            $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        }

        if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
            $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        } 
        
        $query_orse = "
                        SELECT 
                                sos.espe_id
                                ,sos.espe_nombre
                                ,zona.zona_id
                                ,zona.zona_nombre
                                ,COUNT(1) AS total_os_indisponibilidad
                        FROM
                                sla_orden_servicio sos
                                INNER JOIN (  
                                    SELECT empl.empl_id
                                    FROM 
                                        rel_contrato_usuario rcu 
                                        INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                        INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                        INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                    WHERE 
                                        rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                    GROUP BY empl_id
                                ) alcance ON (alcance.empl_id = sos.empl_id)
                                
                                INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                                INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                        WHERE
                                sos.cont_id = $cont_id
                                AND sos.sla_tasa_fallas_exclusion = 0
                                AND $filtros
                        GROUP BY  zona.zona_id, sos.espe_id
                        ORDER BY zona.zona_id ASC, sos.espe_id ASC
                        "; 
    } else{       
        $query_equi = "SELECT 
                                spf.espe_id
                                ,spf.espe_nombre
                                ,spf.regi_id AS zona_id
                                ,r.regi_nombre AS zona_nombre
                                ,spf.slpf_total_equipos as total_equipos
                                ,spf.slpf_tolerancia_fallas as tolerancia_fallas
                                ,spf.slpf_tolerancia_fallas_reiteradas as tolerancia_fallas_reiteradas
                        FROM 
                                sla_parametros_fallas spf
                                INNER JOIN region r ON spf.regi_id = r.regi_id
                        WHERE
                                cont_id = $cont_id
                                AND spf.regi_id IS NOT NULL
                                AND $filtros_equipos
                        GROUP BY spf.regi_id , espe_id
                        ORDER BY regi_orden ASC, espe_id ASC
                    ";
                
        if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
            $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        }

        if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
            $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        }         
        
        $query_orse = "
                        SELECT 
                                sos.espe_id
                                ,sos.espe_nombre
                                ,sos.regi_id AS zona_id
                                ,sos.regi_nombre AS zona_nombre
                                ,COUNT(1) AS total_os_indisponibilidad
			            FROM
                                sla_orden_servicio sos  
                                INNER JOIN (  
                                    SELECT empl.empl_id
                                    FROM 
                                        rel_contrato_usuario rcu 
                                        INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                        INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                        INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                    WHERE 
                                        rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                    GROUP BY empl_id
                                ) alcance ON (alcance.empl_id = sos.empl_id)
                                    
                                INNER JOIN region regi ON sos.regi_id = regi.regi_id
                        WHERE
                                sos.cont_id = $cont_id
                                AND sos.sla_tasa_fallas_exclusion = 0
                                AND $filtros
                        GROUP BY sos.regi_id, sos.espe_id
                        ORDER BY regi.regi_orden ASC, sos.espe_id ASC
                        "; 
    }
        
    $dbo = new MySQL_Database();   
    
//    $filtros = str_replace("\n"," ",$filtros);
//    $filtros = str_replace("\t"," ",$filtros);
//    $filtros = str_replace("\r"," ",$filtros);
//    Flight::json(array("status" => 0, "error" => $query_equi));
    
    $res = $dbo->ExecuteQuery($query_equi);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];
    
    $res = $dbo->ExecuteQuery($query_orse);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $detalle_orses = $res['data'];
    
    foreach ($out AS &$detalle) {
        $detalle['total_os_indisponibilidad'] = 0;

        foreach ($detalle_orses AS $detalle_orse) {
            if( $detalle_orse['espe_id']==$detalle['espe_id'] && $detalle_orse['zona_id']==$detalle['zona_id'] ){
                $detalle['total_os_indisponibilidad'] = $detalle_orse['total_os_indisponibilidad'];
                break;
            }
        }

        $detalle['tasa_falla'] = ($detalle['total_equipos']>0)?round($detalle['total_os_indisponibilidad']/$detalle['total_equipos']*100,2):0;
     }  
     
     
     if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad'] = $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad'] + $out[$i]['total_os_indisponibilidad'];
                $out_filtered[count($out_filtered) - 1]['total_equipos'] = $out_filtered[count($out_filtered) - 1]['total_equipos'] + $out[$i]['total_equipos'];
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            $detalle['tasa_falla'] = ($detalle['total_equipos']>0)?round($detalle['total_os_indisponibilidad']/$detalle['total_equipos']*100,2):0;
        }
    }
     
    return $out;

});

Flight::map('ObtenerSLAFallasReiteradas', function($cont_id,$filtros_ini){
    $out = array();   
    $usua_id = $_SESSION['user_id'];
    $filtros = " ";
    $filtrosZonaConsultada = " ";
    $todasEspecialidades = false;
    $filtrosEspecialidad = "";
    $filtrosFecha = "";   
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtrosEspecialidad .= " AND sos.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtrosEspecialidad .= " AND sos.espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }      
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
        
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtrosZonaConsultada .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND regi.regi_id = ".$filtros_ini['regi_id'];
        $filtrosZonaConsultada .= " AND regi.regi_id = ".$filtros_ini['regi_id'];
    }
    
    if (isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio'] != "") {
        $filtrosFecha .= " AND orse_fecha_validacion >= '" . $filtros_ini['orse_fecha_validacion_inicio'] . " 00:00:00' ";
    }

    if (isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino'] != "") {
        $filtrosFecha .= " AND orse_fecha_validacion <= '" . $filtros_ini['orse_fecha_validacion_termino'] . " 23:59:59' ";
    }

    if( $isFiltroPorZona ){
        $query = "SELECT  zona.zona_id
                          , zona.zona_nombre
                          , espe_id
                          , espe_nombre  
                          , IFNULL(SUM(os_fallas),0) total_os_fallas
                          , IFNULL(SUM(os_fallas_reiteradas),0) total_os_fallas_reiteradas
                          , IFNULL(SUM(os_indisponibilidad),0) total_os_indisponibilidad
                          , IFNULL(SUM(os_indisponibilidad_reiteradas),0) total_os_indisponibilidad_reiteradas
                    FROM zona
                    LEFT JOIN (
                                SELECT  
                                    zona_consultada.zona_id
                                    ,zona_consultada.zona_nombre
                                    ,sos.espe_id
                                    ,sos.espe_nombre  
                                    ,COUNT(1) AS os_fallas
                                    ,IF( COUNT(1) > 0, COUNT(1) - 1, 0 ) AS os_fallas_reiteradas
                                    ,SUM( IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ) ) AS os_indisponibilidad
                                    ,IF(SUM(IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ))>0,SUM( IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ))-1,0) AS os_indisponibilidad_reiteradas
                                FROM sla_orden_servicio sos 
                                INNER JOIN emplazamiento e on  (sos.empl_id = e.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = sos.cont_id)
                                INNER JOIN  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                                INNER JOIN zona zona_consultada on (zona_consultada.zona_id = rze.zona_id 
                                    AND zona_consultada.cont_id =sos.cont_id
                                    AND zona_consultada.zona_estado = 'ACTIVO' 
                                    $filtrosZonaConsultada )
                                WHERE sos.cont_id = $cont_id
                                    AND  sos.sla_tasa_fallas_rei_exclusion = 0 
                                    $filtrosEspecialidad  
                                    $filtrosFecha
                                GROUP BY sos.empl_id, zona_consultada.zona_id, sos.espe_id
                                        ,zona_consultada.zona_nombre
                                         
                    ) x ON x.zona_id = zona.zona_id
                    WHERE zona.cont_id = $cont_id  
                        AND zona.zona_estado = 'ACTIVO' 
                        $filtros
                    GROUP BY zona.zona_id
                          , zona.zona_nombre
        "; 

        $query_tolerancias = "SELECT 
                                spf.espe_id
                                ,spf.zona_id
                                ,spf.slpf_tolerancia_fallas_reiteradas as tolerancia_fallas_reiteradas
                        FROM 
                                sla_parametros_fallas spf
                        WHERE
                                cont_id = $cont_id
                                AND zona_id IS NOT NULL
                        ;";
    } else {

        $query = "SELECT      
                            region.regi_id AS zona_id
                            , region.regi_nombre AS zona_nombre
                            , espe_id
                            , espe_nombre  
                            , IFNULL(SUM(os_fallas),0) total_os_fallas
                            , IFNULL(SUM(os_fallas_reiteradas),0) total_os_fallas_reiteradas
                            , IFNULL(SUM(os_indisponibilidad),0) total_os_indisponibilidad
                            , IFNULL(SUM(os_indisponibilidad_reiteradas),0) total_os_indisponibilidad_reiteradas
                        FROM region
                        INNER JOIN pais ON pais.pais_id = region.pais_id
                        INNER JOIN contrato cont ON cont.pais_id =  pais.pais_id
                        LEFT JOIN (
                                    SELECT
                                                    r.regi_id
                                                    ,r.regi_nombre
                                                    ,sos.espe_id
                                                    ,sos.espe_nombre  
                                                    ,COUNT(1) AS os_fallas
                                                    ,IF( COUNT(1) > 0, COUNT(1) - 1, 0 ) AS os_fallas_reiteradas
                                                    ,SUM( IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ) ) AS os_indisponibilidad
                                                    ,IF(SUM(IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ))>0,SUM( IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ))-1,0) AS os_indisponibilidad_reiteradas
                                    FROM sla_orden_servicio sos 
                                    INNER JOIN emplazamiento e on  (sos.empl_id = e.empl_id)
                                    INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = sos.cont_id)
                                    INNER JOIN comuna c on (c.comu_id = e.comu_id)
                                    INNER JOIN provincia p on (p.prov_id = c.prov_id)
                                    INNER JOIN region r on (r.regi_id = p.regi_id)
                                    WHERE sos.cont_id = $cont_id 
                                        AND  sos.sla_tasa_fallas_rei_exclusion = 0 
                                        $filtrosEspecialidad  
                                        $filtrosFecha
                                    group by sos.empl_id,r.regi_id, sos.espe_id,r.regi_nombre
                        ) x ON (x.regi_id = region.regi_id)
                        WHERE cont.cont_id = $cont_id
        ";
/*
        $query = "SELECT 
                            espe_id
                            ,espe_nombre
                            ,regi_id AS zona_id
                            ,regi_nombre AS zona_nombre
                            ,SUM(os_fallas) AS total_os_fallas
                            ,SUM(os_fallas_reiteradas) AS total_os_fallas_reiteradas
                            ,SUM(os_indisponibilidad) AS total_os_indisponibilidad
                            ,SUM(os_indisponibilidad_reiteradas) AS total_os_indisponibilidad_reiteradas
                    FROM (
                            SELECT 
                                    sos.empl_id
                                    ,sos.empl_nombre
                                    ,sos.espe_id
                                    ,sos.espe_nombre  
                                    ,sos.regi_id
                                    ,sos.regi_nombre
                                    ,regi.regi_orden
                                    ,COUNT(1) AS os_fallas
                                    ,IF( COUNT(1) > 0, COUNT(1) - 1, 0 ) AS os_fallas_reiteradas
                                    ,SUM( IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ) ) AS os_indisponibilidad
                                    ,IF(SUM(IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ))>0,SUM( IF( sos.orse_indisponibilidad IN ('SI','PARCIAL'),1,0 ))-1,0) AS os_indisponibilidad_reiteradas
                            FROM
                                    sla_orden_servicio sos
                                    INNER JOIN (  
                                        SELECT empl.empl_id
                                        FROM 
                                            rel_contrato_usuario rcu 
                                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                        WHERE 
                                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                        GROUP BY empl_id
                                    ) alcance ON (alcance.empl_id = sos.empl_id)
                                    
                                    INNER JOIN region regi ON sos.regi_id = regi.regi_id                        
                            WHERE
                                    sos.cont_id = $cont_id
                                    AND sos.sla_tasa_fallas_rei_exclusion = 0
                                    AND $filtros
                            GROUP BY  sos.empl_id, regi.regi_id, sos.espe_id
                            #ORDER BY sos.empl_id ASC, regi.regi_orden ASC, sos.espe_id ASC
                    ) indisponibilidad
                    GROUP BY regi_id, espe_id
                    ORDER BY regi_orden ASC, espe_id ASC
        "; 
        */
        $query_tolerancias = "SELECT 
                                spf.espe_id
                                ,spf.regi_id AS zona_id
                                ,spf.slpf_tolerancia_fallas_reiteradas as tolerancia_fallas_reiteradas
                        FROM 
                                sla_parametros_fallas spf
                                INNER JOIN region r ON spf.regi_id = r.regi_id
                        WHERE
                                cont_id = $cont_id
                                AND spf.regi_id IS NOT NULL
        ";
    }
        
    $dbo = new MySQL_Database();    
    //Flight::json(array("status" => 0, "error" => $query));
        
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];  
    
    $res = $dbo->ExecuteQuery($query_tolerancias);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $detalle_tolerancias = $res['data'];

    foreach ($out AS &$detalle) {
        foreach ($detalle_tolerancias AS $detalle_tolerancia) {
            if( $detalle_tolerancia['espe_id']==$detalle['espe_id'] && $detalle_tolerancia['zona_id']==$detalle['zona_id'] ){
                $detalle['tolerancia_fallas_reiteradas'] = $detalle_tolerancia['tolerancia_fallas_reiteradas'];
                break;
            }
        }   
        
        //$detalle['tasa_falla_reiteradas'] = ($detalle['total_os_indisponibilidad']>0)?round($detalle['total_os_indisponibilidad_reiteradas']/$detalle['total_os_indisponibilidad']*100,2):0;
        $detalle['tasa_falla_reiteradas'] = ($detalle['total_os_fallas']>0)?round($detalle['total_os_fallas_reiteradas']/$detalle['total_os_fallas']*100,2):0;
    }
    
    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';
        unset($out_filtered[0]['tolerancia_fallas_reiteradas']);

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_os_fallas'] = $out_filtered[count($out_filtered) - 1]['total_os_fallas'] + $out[$i]['total_os_fallas'];
                $out_filtered[count($out_filtered) - 1]['total_os_fallas_reiteradas'] = $out_filtered[count($out_filtered) - 1]['total_os_fallas_reiteradas'] + $out[$i]['total_os_fallas_reiteradas'];
                $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad'] = $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad'] + $out[$i]['total_os_indisponibilidad'];
                $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad_reiteradas'] = $out_filtered[count($out_filtered) - 1]['total_os_indisponibilidad_reiteradas'] + $out[$i]['total_os_indisponibilidad_reiteradas'];
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
                unset($out_filtered[count($out_filtered) - 1]['tolerancia_fallas_reiteradas']);
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            //$detalle['tasa_falla_reiteradas'] = ($detalle['total_os_indisponibilidad']>0)?round($detalle['total_os_indisponibilidad_reiteradas']/$detalle['total_os_indisponibilidad']*100,2):0;
            $detalle['tasa_falla_reiteradas'] = ($detalle['total_os_fallas']>0)?round($detalle['total_os_fallas_reiteradas']/$detalle['total_os_fallas']*100,2):0;
        }
    }

    return $out;

});

Flight::map('ObtenerSLAEjecucion', function($cont_id,$filtros_ini){
    $out = array();   
    $usua_id = $_SESSION['user_id'];
    $filtros = " ";
    $todasEspecialidades = false;
    $filtrosEspecialidad = "";
    $filtrosFecha = "";
    $filtrosTipoZona = "";
    $filtrosZonaConsultada = "";

    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtrosEspecialidad .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";            
        }
        else{
            $filtrosEspecialidad .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }      
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
        
    
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtrosTipoZona .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }   
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND sla.regi_id = ".$filtros_ini['regi_id'];
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtrosFecha .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtrosFecha .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }  
         
    if  (isset($filtros_ini['clas_nombre']) && $filtros_ini['clas_nombre']!="" ){
        $filtros .= " AND clas_nombre REGEXP BINARY '".$filtros_ini['clas_nombre']."' ";
    }   
    
    if( $isFiltroPorZona ){

        $query = "SELECT  
						zona.zona_id
                      , zona.zona_nombre
					  , espe_id
					  ,espe_nombre
                      , ifnull(total_mant_aprobados,0) total_mant_aprobados
                      , ifnull(total_mant,0) total_mant
                    FROM zona
                    left join (
                    SELECT  
                            zona_consultada.zona_id
                            ,zona_consultada.zona_nombre
                            , SUM(IF(m.mant_estado='APROBADA',1,0)) AS total_mant_aprobados
							, m.espe_id
							, m.espe_nombre
                            , COUNT(1) AS  total_mant
                    FROM sla_mantenimiento m 
                    inner join emplazamiento e on  (m.empl_id = e.empl_id)
                    inner join rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id AND rce.cont_id = m.cont_id)
                    inner join  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                    inner join zona zona_consultada on (zona_consultada.zona_id = rze.zona_id 
                                                        AND zona_consultada.cont_id =m.cont_id
                                                        AND zona_consultada.zona_estado = 'ACTIVO' 
                                                        $filtrosZonaConsultada $filtros) 
                    WHERE m.cont_id = $cont_id 
                    AND  m.sla_ejecucion_exclusion = 0 
                    $filtrosEspecialidad
                    $filtrosFecha
                    group by zona_consultada.zona_id
                            ,zona_consultada.zona_nombre
                             
        ) x on x.zona_id = zona.zona_id
        where zona.cont_id = $cont_id  
        AND zona.zona_estado = 'ACTIVO'
        $filtrosTipoZona";
        /*and zona.zona_tipo = 'MOVISTAR' #### valor que entra por parametro*/
        //$filtros";
        /*
        $query =    "SELECT 
                                zona.zona_id
                                ,zona.zona_nombre
                                ,sla.espe_id
                                ,sla.espe_nombre
                                , SUM(IF(mant_estado='APROBADA',1,0)) AS total_mant_aprobados
                                , COUNT(1) AS  total_mant	
                    FROM 
                                sla_mantenimiento sla
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)

                    INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
            WHERE 
                    sla.cont_id = $cont_id
                    AND sla_ejecucion_exclusion = 0
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;"; 
          */      
                
    } else {       
        

        $query = "SELECT  region.regi_id as zona_id				
					, espe_id
					, espe_nombre
					, region.regi_nombre as zona_nombre
					, ifnull(total_mant_aprobados,0) total_mant_aprobados
					, ifnull(total_mant,0) total_mant
            from region
            inner join pais on pais.pais_id = region.pais_id
            inner join contrato cont on cont.pais_id =  pais.pais_id
            left join (
                        select  
                                r.regi_id
                                ,r.regi_nombre
								,m.espe_id
								,m.espe_nombre
                                , SUM(IF(m.mant_estado='APROBADA',1,0)) AS total_mant_aprobados
                                , COUNT(1) AS  total_mant
                        from sla_mantenimiento m 
                        inner join emplazamiento e on  (m.empl_id = e.empl_id)
                        inner join rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = m.cont_id)
                        inner join comuna c on (c.comu_id = e.comu_id)
                        inner join provincia p on (p.prov_id = c.prov_id)
                        inner join region r on (r.regi_id = p.regi_id)
                        where m.cont_id = $cont_id 
                        and  m.sla_ejecucion_exclusion = 0 
                        #and  m.espe_id = 1 
                        $filtrosEspecialidad
                        $filtrosFecha
						$filtros
                        ####and  m.mant_fecha_validacion >= '2017-07-01 00:00:00'   #### valor que entra por parametro
                        ###and  m.mant_fecha_validacion <= '2017-07-30 23:59:59' #### valor que entra por parametro
                        group by r.regi_id
                                ,r.regi_nombre
                                 
            ) x on (x.regi_id = region.regi_id)
            where cont.cont_id = $cont_id  #### valor que entra por parametro
			   #ORDER BY region.regi_orden asc
        ";
        
        /*$query = "SELECT 
                    regi.regi_id AS zona_id
                    , regi.regi_nombre AS zona_nombre
                    , sla.espe_id
                    , sla.espe_nombre
                    , SUM(IF(mant_estado='APROBADA',1,0)) AS total_mant_aprobados
                    , COUNT(1) AS  total_mant	
            FROM 
                    sla_mantenimiento sla 
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)
                    
                    LEFT JOIN region regi ON sla.regi_id = regi.regi_id 
            WHERE 
                    sla.cont_id = $cont_id
                    AND regi.pais_id = 1
                    AND sla_ejecucion_exclusion = 0
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY regi_orden ASC, espe_id ASC
            ;";          */
    }
        
    $dbo = new MySQL_Database();   
        
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];
        
    foreach ($out AS &$detalle) {
        $detalle['tasa_ejecucion'] = ($detalle['total_mant']>0)?round($detalle['total_mant_aprobados']/$detalle['total_mant']*100,2):0;
    }
     
     
     
    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';
        
        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_mant'] = $out_filtered[count($out_filtered) - 1]['total_mant'] + $out[$i]['total_mant'];
                $out_filtered[count($out_filtered) - 1]['total_mant_aprobados'] = $out_filtered[count($out_filtered) - 1]['total_mant_aprobados'] + $out[$i]['total_mant_aprobados'];
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';                
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            $detalle['tasa_ejecucion'] = ($detalle['total_mant']>0)?round($detalle['total_mant_aprobados']/$detalle['total_mant']*100,2):0;
        }
    }
     
    return $out;

});

Flight::map('ObtenerSLACronograma', function($cont_id,$filtros_ini){
    $out = array();
    $usua_id = $_SESSION['user_id'];
    $filtros = "TRUE ";
    $todasEspecialidades = false;
    $filtrosFecha = " ";
    $filtrosZonaConsultada = "";
    $filtrosTipoZona = "";
    $filtrosEspecialidad = "";
     
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtrosEspecialidad .= " AND m.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";            
        }
        else{
            $filtrosEspecialidad .= " AND m.espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }   
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
    
        
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosTipoZona .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtrosTipoZona .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtrosZonaConsultada .= " AND zona_consultada.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND sla.regi_id = ".$filtros_ini['regi_id'];
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtrosFecha .= " AND m.mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtrosFecha .= " AND m.mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }  
    
    if( $isFiltroPorZona ){
        $query= "SELECT  zona.zona_id
                          , zona.zona_nombre
                          , espe_id
                          , espe_nombre
                          , ifnull(total_mant,0) total_mant
                          , ifnull(total_mant_cronograma,0) total_mant_cronograma
                FROM zona
                LEFT JOIN (
                                SELECT  
                                        zona_consultada.zona_id
                                        , zona_consultada.zona_nombre
                                        , m.espe_id
                                        , m.espe_nombre
                                        ,COUNT(1) AS total_mant
                                        ,SUM(IF(m.mant_fecha_programada >= DATE_SUB(COALESCE(m.mant_fecha_ejecucion, m.mant_fecha_validacion),INTERVAL m.sla_cronograma_ajuste HOUR ), 1, 0) ) AS total_mant_cronograma
                                FROM sla_mantenimiento m 
                                INNER JOIN emplazamiento e on  (m.empl_id = e.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id AND rce.cont_id = m.cont_id)
                                INNER JOIN  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                                INNER JOIN zona zona_consultada on (zona_consultada.zona_id = rze.zona_id 
                                                                    AND zona_consultada.cont_id = m.cont_id
                                                                    AND zona_consultada.zona_estado = 'ACTIVO' 
                                                                    $filtrosZonaConsultada
                                ) #### valor que entra por parametro
                                WHERE m.cont_id = $cont_id #### valor que entra por parametro
                                AND  m.sla_ejecucion_exclusion = 0 AND m.mant_estado  in ('APROBADA','RECHAZADA','NO REALIZADO')
                                $filtrosEspecialidad 
                                $filtrosFecha
                                group by zona_consultada.zona_id
                                        ,zona_consultada.zona_nombre
                                         
                    ) x on x.zona_id = zona.zona_id
                WHERE zona.cont_id = $cont_id  #### valor que entra por parametro
                and zona.zona_estado = 'ACTIVO' 
                $filtrosTipoZona #### valor que entra por parametro
    ";    
        /*$query = "SELECT 
                    zona.zona_id
                    ,zona.zona_nombre
                    ,sla.espe_id
                    ,sla.espe_nombre
                    ,COUNT(1) AS total_mant
                    ,SUM(IF( sla.mant_fecha_programada >= DATE_SUB(COALESCE(sla.mant_fecha_ejecucion, sla.mant_fecha_validacion),INTERVAL sla.sla_cronograma_ajuste HOUR ), 1, 0) ) AS total_mant_cronograma
            FROM 
                    sla_mantenimiento sla
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)
                    
                    INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
            WHERE 
                    sla.cont_id = $cont_id
                    AND sla.sla_cronograma_exclusion = 0
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;";     
            */
    } else { 
        $query = "SELECT  region.regi_id as zona_id
                      , region.regi_nombre as zona_nombre
                      , espe_id
                      , espe_nombre
                      , ifnull(total_mant,0) total_mant
                      , ifnull(total_mant_cronograma,0) total_mant_cronograma
                FROM region
                INNER JOIN pais on pais.pais_id = region.pais_id
                INNER JOIN contrato cont on cont.pais_id =  pais.pais_id
                LEFT JOIN (
                            SELECT  
                                    r.regi_id
                                    ,r.regi_nombre
                                    , m.espe_id
                                    , m.espe_nombre
                                    ,COUNT(1) AS total_mant
                                    ,SUM(IF(m.mant_fecha_programada >= DATE_SUB(COALESCE(m.mant_fecha_ejecucion, m.mant_fecha_validacion),INTERVAL m.sla_cronograma_ajuste HOUR ), 1, 0) ) AS total_mant_cronograma
                            FROM sla_mantenimiento m 
                            INNER JOIN emplazamiento e on  (m.empl_id = e.empl_id)
                            INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = m.cont_id)
                            INNER JOIN comuna c on (c.comu_id = e.comu_id)
                            INNER JOIN provincia p on (p.prov_id = c.prov_id)
                            INNER JOIN region r on (r.regi_id = p.regi_id)
                            WHERE m.cont_id = $cont_id 
                                AND  m.sla_ejecucion_exclusion = 0 AND m.mant_estado in ('APROBADA','RECHAZADA','NO REALIZADO')
                                $filtrosEspecialidad 
                                $filtrosFecha
                            GROUP BY r.regi_id
                                    ,r.regi_nombre
                         
                ) x ON (x.regi_id = region.regi_id)
                WHERE cont.cont_id = $cont_id  
                ";
        /*$query = "SELECT 
                    regi.regi_id AS zona_id
                    ,regi.regi_nombre AS zona_nombre
                    ,sla.espe_id
                    ,sla.espe_nombre
                    ,COUNT(1) AS total_mant
                    ,SUM(IF( sla.mant_fecha_programada >= DATE_SUB(COALESCE(sla.mant_fecha_ejecucion, sla.mant_fecha_validacion),INTERVAL sla.sla_cronograma_ajuste HOUR ), 1, 0) ) AS total_mant_cronograma
            FROM 
                    sla_mantenimiento sla
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)
                    
                    LEFT JOIN region regi ON sla.regi_id = regi.regi_id 
            WHERE 
                    sla.cont_id = $cont_id
                    AND regi.pais_id = 1
                    AND sla.sla_cronograma_exclusion = 0
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY regi_orden ASC, espe_id ASC
            ;";   
        */
    }
        
    $dbo = new MySQL_Database();   
        
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];
        
    foreach ($out AS &$detalle) {
        $detalle['tasa_cronograma'] = ($detalle['total_mant']>0)?round($detalle['total_mant_cronograma']/$detalle['total_mant']*100,2):0;
    }
    
    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_mant'] = $out_filtered[count($out_filtered) - 1]['total_mant'] + $out[$i]['total_mant'];
                $out_filtered[count($out_filtered) - 1]['total_mant_cronograma'] = $out_filtered[count($out_filtered) - 1]['total_mant_cronograma'] + $out[$i]['total_mant_cronograma'];
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            $detalle['tasa_cronograma'] = ($detalle['total_mant']>0)?round($detalle['total_mant_cronograma']/$detalle['total_mant']*100,2):0;
        }
    }
     
    return $out;

});

Flight::map('ObtenerSLACalidad', function($cont_id,$filtros_ini){
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    $filtros = "TRUE ";
    $filtros_no_realizados = "TRUE ";
    $todasEspecialidades = false;
    $filtrosFecha = "";
    $filtrosEspecialidad = "";
    $filtrosZonaConsultada = "";
    $filtrosTipoZona = "";


	
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtrosEspecialidad .= " AND m.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";  
        } else {
            $filtrosEspecialidad .= " AND m.espe_id = '".$filtros_ini['espe_id']."' ";
        }
        
    }        
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
        
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosTipoZona .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtrosZonaConsultada .= " AND zona_consultada.zona_id = ".$filtros_ini['zona_id'];
        $filtrosTipoZona .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND regi_id = ".$filtros_ini['regi_id'];
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' ";
        $filtrosFecha .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "; 
        $filtrosFecha .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "; 
    }  
    
    if( $isFiltroPorZona ){
		
	
		
            $query = "SELECT  zona.zona_id
                          , zona.zona_nombre
                          , espe_id
                          , espe_nombre
                          , ifnull(total_insp_rechazadas,0) total_insp_rechazadas
                          , ifnull(total_mant,0) total_mant
                          , ifnull(total_mant_aprobados,0) total_mant_aprobados
                          , ifnull(total_mant_rechazados,0) total_mant_rechazados
                    FROM zona
                    LEFT JOIN (
                                SELECT  
                                        zona_consultada.zona_id
                                        , zona_consultada.zona_nombre
                                        , m.espe_id
                                        , m.espe_nombre
                                        , SUM(IF((insp_estado='RECHAZADA' AND sla_calidad_exclusion = 0),1,0)) AS total_insp_rechazadas
                                        , COUNT(1) AS  total_mant
                                        , SUM(IF( m.mant_estado = 'APROBADA',1,0 )) AS  total_mant_aprobados
                                        , SUM(IF( m.mant_estado = 'RECHAZADA',1,0 )) AS  total_mant_rechazados
                                FROM sla_mantenimiento m 
                                    INNER JOIN emplazamiento e on  (m.empl_id = e.empl_id)
                                    INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = m.cont_id)
                                    INNER JOIN  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                                    INNER JOIN zona zona_consultada on  (zona_consultada.zona_id = rze.zona_id 
                                                                            AND zona_consultada.cont_id = m.cont_id
                                                                            AND zona_consultada.zona_estado = 'ACTIVO' 
                                                                            $filtrosZonaConsultada
                                                                        )
                                LEFT JOIN sla_inspeccion slai ON (m.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
                                WHERE m.cont_id = $cont_id
                                    AND m.mant_estado <> 'NO REALIZADO' 
                                    $filtrosEspecialidad #### valor que entra por parametro
                                    $filtrosFecha
                                GROUP BY zona_consultada.zona_id
                                        ,zona_consultada.zona_nombre
                                        ,m.espe_id
                                        ,m.espe_nombre
                    ) x on x.zona_id = zona.zona_id
                    WHERE zona.cont_id = $cont_id  #### valor que entra por parametro
                    AND zona.zona_estado = 'ACTIVO' 
                    $filtrosTipoZona #### valor que entra por parametro
             ";


			 
/*
        $query = "
            SELECT 
                    zona.zona_id
                    ,zona.zona_nombre
                    ,sla.espe_id
                    ,sla.espe_nombre
                    , SUM(IF((insp_estado='RECHAZADA' AND sla_calidad_exclusion = 0),1,0)) AS total_insp_rechazadas
                    , COUNT(1) AS  total_mant
                    , SUM(IF( sla.mant_estado = 'APROBADA',1,0 )) AS  total_mant_aprobados
                    , SUM(IF( sla.mant_estado = 'RECHAZADA',1,0 )) AS  total_mant_rechazados
            FROM 
                    sla_mantenimiento sla      
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)

                    INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                    LEFT JOIN sla_inspeccion slai ON (sla.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
            WHERE 
                    sla.cont_id = $cont_id
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;"; */
        
        $filtros = str_replace("mant_fecha_validacion","mape_fecha_cierre",$filtros);
        $filtros = str_replace("sla.espe_id","espe.espe_id",$filtros);
        
        $query_no_realizados = "
            SELECT 
                    zona.zona_id
                    ,zona.zona_nombre
                    ,espe.espe_id
                    ,espe.espe_nombre
                    , COUNT(1) AS  total_mant_no_realizados
            FROM 
                    mantenimiento mant     
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = mant.empl_id)
                    
                    INNER JOIN especialidad espe ON mant.espe_id = espe.espe_id
                    INNER JOIN mantenimiento_periodos mape ON mant.mape_id = mape.mape_id               
                    INNER JOIN rel_zona_emplazamiento rze ON mant.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')

            WHERE 
                    mant.cont_id = $cont_id
                    AND mant_estado = 'NO REALIZADO'
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;";     
                
                
    } else {      
        $query = "SELECT  region.regi_id as zona_id
                          , region.regi_nombre as zona_nombre
                          , espe_id
                          , espe_nombre
                          , ifnull(total_mant_aprobados,0) total_mant_aprobados
                          , ifnull(total_mant,0) total_mant
                          , ifnull(total_insp_rechazadas,0) total_insp_rechazadas
                          , ifnull(total_mant,0) total_mant
                          , ifnull(total_mant_aprobados,0) total_mant_aprobados
                          , ifnull(total_mant_rechazados,0) total_mant_rechazados
                    FROM region
                    INNER JOIN pais on pais.pais_id = region.pais_id
                    INNER JOIN contrato cont on cont.pais_id =  pais.pais_id
                    LEFT JOIN (
                                SELECT  
                                        r.regi_id
                                        ,r.regi_nombre
                                        , m.espe_id
                                        , m.espe_nombre
                                        , SUM(IF((insp_estado='RECHAZADA' AND sla_calidad_exclusion = 0),1,0)) AS total_insp_rechazadas
                                        , COUNT(1) AS  total_mant
                                        , SUM(IF( m.mant_estado = 'APROBADA',1,0 )) AS  total_mant_aprobados
                                        , SUM(IF( m.mant_estado = 'RECHAZADA',1,0 )) AS  total_mant_rechazados
                                FROM sla_mantenimiento m 
                                INNER JOIN emplazamiento e on  (m.empl_id = e.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = m.cont_id)
                                INNER JOIN comuna c on (c.comu_id = e.comu_id)
                                INNER JOIN provincia p on (p.prov_id = c.prov_id)
                                INNER JOIN region r on (r.regi_id = p.regi_id)
                                LEFT JOIN sla_inspeccion slai ON (m.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
                                WHERE m.cont_id = $cont_id #### valor que entra por parametro
                                    AND m.mant_estado <> 'NO REALIZADO' 
                                    $filtrosEspecialidad
                                    $filtrosFecha
                                GROUP BY r.regi_id
                                        ,m.espe_id
                                        ,m.espe_nombre
                                        ,r.regi_nombre    
                    ) x ON (x.regi_id = region.regi_id)
                    WHERE cont.cont_id = $cont_id  #### valor que entra por parametro
        ";
/*
        $query = "SELECT 
                    sla.regi_id AS zona_id
                    , sla.regi_nombre AS zona_nombre
                    , sla.espe_id
                    , sla.espe_nombre
                    , SUM(IF( insp_estado='RECHAZADA' AND sla_calidad_exclusion = 0,1,0)) AS total_insp_rechazadas
                    , COUNT(1) AS  total_mant
                    , SUM(IF( sla.mant_estado = 'APROBADA',1,0 )) AS  total_mant_aprobados
                    , SUM(IF( sla.mant_estado = 'RECHAZADA',1,0 )) AS  total_mant_rechazados
            FROM 
                    sla_mantenimiento sla 
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)

                    INNER JOIN region regi ON sla.regi_id = regi.regi_id
                    LEFT JOIN sla_inspeccion slai ON (sla.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
            WHERE 
                    sla.cont_id = $cont_id
                    AND regi.pais_id = 1
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY regi_orden ASC, espe_id ASC
            ;";     
  */      
        $filtros = str_replace("mant_fecha_validacion","mape_fecha_cierre",$filtros);
        $filtros = str_replace("sla.espe_id","espe.espe_id",$filtros);
        $filtros = str_replace("regi_id","regi.regi_id",$filtros);
        
        $query_no_realizados="
            SELECT 
                    regi.regi_id AS zona_id
                    ,regi.regi_nombre AS zona_nombre
                    ,espe.espe_id
                    ,espe.espe_nombre
                    ,COUNT(1) AS  total_mant_no_realizados
            FROM 
                    mantenimiento mant  
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = mant.empl_id)
                    
                    INNER JOIN especialidad espe ON mant.espe_id = espe.espe_id
                    INNER JOIN mantenimiento_periodos mape ON mant.mape_id = mape.mape_id               
                    INNER JOIN emplazamiento empl ON mant.empl_id = empl. empl_id
                    INNER JOIN comuna comu ON empl.comu_id = comu.comu_id
                    INNER JOIN provincia prov ON comu.prov_id = prov.prov_id
                    INNER JOIN region regi ON (prov.regi_id = regi.regi_id AND regi.pais_id = 1)
            WHERE 
                    mant.cont_id = $cont_id
                    AND mant_estado = 'NO REALIZADO'
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;";  
    }
    echo 'ANTES DBO';
    $dbo = new MySQL_Database();   
    //Flight::json(array("status" => 0, "error" => $query."    ".$query_no_realizados));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];
    
    $res = $dbo->ExecuteQuery($query_no_realizados);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $detalle_no_realizados = $res['data'];
    
    echo 'ANTES ENTRA FOREACH';
    foreach ($out AS &$detalle) {
        echo 'ENTRA FOREACH';
        $detalle['total_mant_no_realizados'] = 0;
        
        echo 'ANTES ENTRA FOREACH2';
        foreach ($detalle_no_realizados AS $detalle_no_realizado) {
            echo 'ENTRA FOREACH2';
            if( $detalle_no_realizado['espe_id']==$detalle['espe_id'] && $detalle_no_realizado['zona_id']==$detalle['zona_id'] ){
                $detalle['total_mant_no_realizados'] = $detalle_no_realizado['total_mant_no_realizados'];
                break;
            }
        }

        /*$detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];*/
        $detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados'];
        /*$detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;*/
        $detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;
    }
    echo 'FIN FOREACH';
    
    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_mant_aprobados'] = $out_filtered[count($out_filtered) - 1]['total_mant_aprobados'] + $out[$i]['total_mant_aprobados'];
                $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] = $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] + $out[$i]['total_mant_rechazados'];
                $out_filtered[count($out_filtered) - 1]['total_mant_no_realizados'] = $out_filtered[count($out_filtered) - 1]['total_mant_no_realizados'] + $out[$i]['total_mant_no_realizados'];
                $out_filtered[count($out_filtered) - 1]['total_insp_rechazadas'] = $out_filtered[count($out_filtered) - 1]['total_insp_rechazadas'] + $out[$i]['total_insp_rechazadas'];
                
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            /*$detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];*/
            $detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];
            /*$detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;*/
            $detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;
        }
    }
     
    return $out;

});



Flight::map('ObtenerSLACalidadyCronograma', function($cont_id,$filtros_ini){
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    $filtros = "TRUE ";
    $filtros_no_realizados = "TRUE ";
    $todasEspecialidades = false;
    $filtrosZonaConsultada = "";
    $filtrosTipoZona = "";
    $filtrosFecha = "";
    $filtrosEspecialidad = "";

    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtrosEspecialidad .= " AND m.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";  
        }
        else{
            $filtrosEspecialidad .= " AND m.espe_id = '".$filtros_ini['espe_id']."' ";
        }

    }        
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
        
    $isFiltroPorZona = false;
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosTipoZona .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona = true;
    }
    /*
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
        $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtrosTipoZona .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $filtrosZonaConsultada .= " AND zona.zona_id = ".$filtros_ini['zona_id'];
        $isFiltroPorZona = true;
    }
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!=""){
        $filtros .= " AND regi_id = ".$filtros_ini['regi_id'];
    }
    */
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' ";
        $filtrosFecha .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' ";
        $filtrosFecha .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "; 
    }  
    
    if( $isFiltroPorZona ){
        $query = "SELECT  zona.zona_id
                          , zona.zona_nombre
                          , espe_id
                          , espe_nombre
                          , ifnull(total_mant,0) total_mant
                          , ifnull(total_mant_rechazados,0) total_mant_rechazados
                          , ifnull(total_mant_no_cumple,0) total_mant_no_cumple
                    FROM zona
                    LEFT JOIN (
                                SELECT  
                                        zona_consultada.zona_id
                                        , zona_consultada.zona_nombre
                                        , m.espe_id
                                        , m.espe_nombre
                                        , SUM(sla_ejecucion_exclusion = 0) AS  total_mant
                                        , SUM(IF(((sla_ejecucion_exclusion = 0) AND (m.mant_estado = 'RECHAZADA' OR  m.mant_estado = 'NO REALIZADO')),1,0 ))  AS total_mant_rechazados
                                , SUM(IF( (sla_cronograma_exclusion=0) AND (( mant_fecha_ejecucion - INTERVAL sla_cronograma_ajuste HOUR ) > mant_fecha_programada) AND (m.mant_estado NOT IN ('RECHAZADA','NO REALIZADO') ), 1, 0 ))  AS total_mant_no_cumple
                                FROM sla_mantenimiento m 
                                    INNER JOIN emplazamiento e on  (m.empl_id = e.empl_id)
                                    INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id and rce.cont_id = m.cont_id)
                                    INNER JOIN  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                                    INNER JOIN zona zona_consultada on (zona_consultada.zona_id = rze.zona_id 
                                                                        AND zona_consultada.cont_id = m.cont_id
                                                                        AND zona_consultada.zona_estado = 'ACTIVO' 
                                                                        $filtrosZonaConsultada
                                    )
                                LEFT JOIN sla_inspeccion slai ON (m.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
                                WHERE m.cont_id = $cont_id
                                  
                                    $filtrosEspecialidad #### valor que entra por parametro
                                    $filtrosFecha
                                GROUP BY zona_consultada.zona_id
                                        ,zona_consultada.zona_nombre
                                        ,m.espe_id
                                        ,m.espe_nombre
                    ) x on x.zona_id = zona.zona_id
                    WHERE zona.cont_id = $cont_id  #### valor que entra por parametro
                    AND zona.zona_estado = 'ACTIVO' 
                    $filtrosTipoZona #### valor que entra por parametro
    ";
    /*
        $queryOLD = "SELECT 
                    zona.zona_id
                    ,zona.zona_nombre
                    ,sla.espe_id
                    ,sla.espe_nombre
                    , COUNT(1) AS  total_mant
                    , SUM(IF( sla_calidad_exclusion = 0 AND sla.mant_estado = 'RECHAZADA',1,0 )) AS total_mant_rechazados
                    , SUM(IF(mant_fecha_ejecucion>mant_fecha_programada, 1, 0 )) AS total_mant_no_cumple
            FROM 
                    sla_mantenimiento sla      
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)

                    INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                    LEFT JOIN sla_inspeccion slai ON (sla.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
            WHERE 
                    sla.cont_id = $cont_id
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;"; 
        */
        $filtros = str_replace("mant_fecha_validacion","mape_fecha_cierre",$filtros);
        $filtros = str_replace("m.espe_id","espe.espe_id",$filtros);
        
        $query_no_realizados = "
            SELECT 
                    zona.zona_id
                    ,zona.zona_nombre
                    ,espe.espe_id
                    ,espe.espe_nombre
                    , COUNT(1) AS  total_mant_no_realizados
            FROM 
                    mantenimiento mant     
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = mant.empl_id)
                    
                    INNER JOIN especialidad espe ON mant.espe_id = espe.espe_id
                    INNER JOIN mantenimiento_periodos mape ON mant.mape_id = mape.mape_id               
                    INNER JOIN rel_zona_emplazamiento rze ON mant.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
            WHERE 
                    mant.cont_id = $cont_id
                   #AND mant_estado = 'NO REALIZADO'
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;";     
                
                
    } else {       
        /*$queryOLD = "
            SELECT 
                    sla.regi_id AS zona_id
                    , sla.regi_nombre AS zona_nombre
                    , sla.espe_id
                    , sla.espe_nombre
                    , SUM(IF( insp_estado='RECHAZADA' AND sla_calidad_exclusion = 0,1,0)) AS total_insp_rechazadas
                    , COUNT(1) AS  total_mant
                    , SUM(IF( sla.mant_estado = 'APROBADA',1,0 )) AS  total_mant_aprobados
                    , SUM(IF( sla.mant_estado = 'RECHAZADA',1,0 )) AS  total_mant_rechazados
            FROM 
                    sla_mantenimiento sla 
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)

                    INNER JOIN region regi ON sla.regi_id = regi.regi_id
                    LEFT JOIN sla_inspeccion slai ON (sla.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
            WHERE 
                    sla.cont_id = $cont_id
                    AND regi.pais_id = 1
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY regi_orden ASC, espe_id ASC
            ;";     
        */
        $query= "SELECT  region.regi_id as zona_id
                          , region.regi_nombre as zona_nombre
                          , espe_id
                          , espe_nombre
                          , ifnull(total_mant,0) total_mant
                          , ifnull(total_mant_rechazados,0) total_mant_rechazados
                          , ifnull(total_mant_no_cumple,0) total_mant_no_cumple
            FROM region
            INNER JOIN pais on pais.pais_id = region.pais_id
            INNER JOIN contrato cont on cont.pais_id =  pais.pais_id
            LEFT JOIN (
                        SELECT  
                                r.regi_id
                                ,r.regi_nombre
                                , m.espe_id
                                , m.espe_nombre
                                , SUM(sla_ejecucion_exclusion = 0) AS  total_mant
                                , SUM(IF(((sla_ejecucion_exclusion = 0) AND (m.mant_estado = 'RECHAZADA' OR  m.mant_estado = 'NO REALIZADO')),1,0 ))  AS total_mant_rechazados
                                , SUM(IF( (sla_cronograma_exclusion=0) AND (( mant_fecha_ejecucion - INTERVAL sla_cronograma_ajuste HOUR ) > mant_fecha_programada) AND (m.mant_estado NOT IN ('RECHAZADA','NO REALIZADO') ), 1, 0 ))  AS total_mant_no_cumple
                        from sla_mantenimiento m 
                        INNER JOIN emplazamiento e ON  (m.empl_id = e.empl_id)
                        INNER JOIN rel_contrato_emplazamiento rce ON (rce.empl_id = e.empl_id and rce.cont_id = m.cont_id)
                        INNER JOIN comuna c on (c.comu_id = e.comu_id)
                        INNER JOIN provincia p on (p.prov_id = c.prov_id)
                        INNER JOIN region r on (r.regi_id = p.regi_id)
                        LEFT JOIN sla_inspeccion slai ON (m.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
                        WHERE m.cont_id = $cont_id #### valor que entra por parametro
                            AND  m.sla_ejecucion_exclusion = 0 
                            $filtrosEspecialidad #### valor que entra por parametro
                            $filtrosFecha
                        group by r.regi_id
                                ,r.regi_nombre
                                 
            ) x on (x.regi_id = region.regi_id)
            where cont.cont_id = $cont_id
        ";
        $filtros = str_replace("mant_fecha_validacion","mape_fecha_cierre",$filtros);
        $filtros = str_replace("sla.espe_id","espe.espe_id",$filtros);
        $filtros = str_replace("regi_id","regi.regi_id",$filtros);
        
        $query_no_realizados = "
            SELECT 
                    regi.regi_id AS zona_id
                    ,regi.regi_nombre AS zona_nombre
                    ,espe.espe_id
                    ,espe.espe_nombre
                    ,COUNT(1) AS  total_mant_no_realizados
            FROM 
                    mantenimiento mant  
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = mant.empl_id)
                    
                    INNER JOIN especialidad espe ON mant.espe_id = espe.espe_id
                    INNER JOIN mantenimiento_periodos mape ON mant.mape_id = mape.mape_id               
                    INNER JOIN emplazamiento empl ON mant.empl_id = empl. empl_id
                    INNER JOIN comuna comu ON empl.comu_id = comu.comu_id
                    INNER JOIN provincia prov ON comu.prov_id = prov.prov_id
                    INNER JOIN region regi ON (prov.regi_id = regi.regi_id AND regi.pais_id = 1)
            WHERE 
                    mant.cont_id = $cont_id
                    #AND mant_estado = 'NO REALIZADO'
                    AND $filtros
            GROUP BY zona_id, espe_id
            ORDER BY zona_id ASC, espe_id ASC
            ;";  
    }
    echo 'ANTES DBO';
    $dbo = new MySQL_Database();   
    //Flight::json(array("status" => 0, "error" => $query."    ".$query_no_realizados));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0){ 
        Flight::json(array("status" => 0, "error" => $res['error']));
    }

    $out = $res['data'];
    
    $res_query_norealizados = $dbo->ExecuteQuery($query_no_realizados);
    if ($res_query_norealizados['status'] == 0){   
        Flight::json(array("status" => 0, "error" => $res_query_norealizados['error']));
    }

    $detalle_no_realizados = $res_query_norealizados['data'];
    

    echo 'ANTES ENTRA FOREACH';
    foreach ($out AS &$detalle) {
        echo 'ENTRA FOREACH';
        $detalle['total_mant_no_realizados'] = 0;
       
        echo 'ANTES ENTRA FOREACH2';


/*  Este cambio es de ALAN debiera regurarizarse en paso a prod de incidencia sla para el 
    Viernes 04-01
        foreach ($detalle_no_realizados AS $detalle_no_realizado) {
            echo 'ENTRA FOREACH2';
			 // $detalle['total_mant_no_cumple'] = $detalle['total_mant_no_cumple'];
              //  $detalle['total_mant_defectuoso'] = $detalle['total_mant_rechazados']; 
			
            if( $detalle['espe_id']==$detalle['espe_id'] && $detalle['zona_id']==$detalle['zona_id'] ){
              
                //break;
            }
        }
*/
        $detalle['total_mant'] = $detalle['total_mant'];
        //$detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados'];
        //$detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;
        //$detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;
        $detalle['tasa_calidad'] = round(($detalle['total_mant_rechazados']+$detalle['total_mant_no_cumple'])/$detalle['total_mant'],4)*100;
    }
    echo 'FIN FOREACH';

    /*
    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                #$out_filtered[count($out_filtered) - 1]['total_mant'] = $out_filtered[count($out_filtered) - 1]['total_mant'] + $out[$i]['total_mant'];
                $out_filtered[count($out_filtered) - 1]['total_mant_defectuoso'] = $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] + $out[$i]['total_mant_no_realizados'];
                $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] = $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] + $out[$i]['total_mant_rechazados'];
//                $out_filtered[count($out_filtered) - 1]['total_mant_no_realizados'] = $out_filtered[count($out_filtered) - 1]['total_mant_no_realizados'] + $out[$i]['total_mant_no_realizados'];
                
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
            }
        }
        
       
        $out = $out_filtered;

        foreach ($out AS &$detalle) {
            $detalle['total_mant'] = $detalle['total_mant']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];
            $detalle['tasa_calidad'] = round( (isset($detalle['total_mant_rechazados'])?$detalle['total_mant_rechazados']:0+isset($detalle['total_mant_no_realizados'])?$detalle['total_mant_no_realizados']:0+isset($detalle['total_mant_no_cumple'])?$detalle['total_mant_no_cumple']:0)/$detalle['total_mant'],4)*100;
     //       $out_filtered[0]['tasa_calidad']= round(round((isset($detalle['total_mant_rechazados'])?$detalle['total_mant_rechazados']:0+isset($detalle['total_mant_no_realizados'])?$detalle['total_mant_no_realizados']:0+isset($detalle['total_mant_no_cumple'])?$detalle['total_mant_no_cumple']:0)/$detalle['total_mant'],4),4);
    //        $detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];
   //         $detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];
     //   }

        }
    }*/

    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_mant_no_cumple'] = $out_filtered[count($out_filtered) - 1]['total_mant_no_cumple'] + $out[$i]['total_mant_no_cumple'];
                #$out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] = $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] + $out[$i]['total_mant_rechazados'];
                #$out_filtered[count($out_filtered) - 1]['total_mant_no_realizados'] = $out_filtered[count($out_filtered) - 1]['total_mant_no_realizados'] + $out[$i]['total_mant_no_realizados'];
                #$out_filtered[count($out_filtered) - 1]['total_insp_rechazadas'] = $out_filtered[count($out_filtered) - 1]['total_insp_rechazadas'] + $out[$i]['total_insp_rechazadas'];
                #$out_filtered[count($out_filtered) - 1]['total_mant_defectuoso'] = $out_filtered[count($out_filtered) - 1]['total_mant_defectuoso'] + $out[$i]['total_mant_defectuoso'];
                #$out_filtered[count($out_filtered) - 1]['total_mant_defectuoso'] = $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] + $out[$i]['total_mant_no_realizados'];
                $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] = $out_filtered[count($out_filtered) - 1]['total_mant_rechazados'] + $out[$i]['total_mant_rechazados'];
                $out_filtered[count($out_filtered) - 1]['total_mant'] = $out_filtered[count($out_filtered) - 1]['total_mant'] + $out[$i]['total_mant'];

            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            /*$detalle['total_mant'] = $detalle['total_mant_aprobados']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];*/
            #$detalle['total_mant'] = $detalle['total_mant']+$detalle['total_mant_rechazados']+$detalle['total_mant_no_realizados'];
            /*$detalle['tasa_calidad'] = ($detalle['total_mant']>0)?round(($detalle['total_mant_aprobados']-$detalle['total_insp_rechazadas'])/($detalle['total_mant'])*100,2):0;*/
            $detalle['tasa_calidad'] = round(($detalle['total_mant_rechazados']+$detalle['total_mant_no_cumple'])/$detalle['total_mant'],4)*100;
        }
    }
    return $out;

});

Flight::map('ObtenerSLATiempos', function($cont_id,$filtros_ini){
    if( !isset(Flight::get('SLA_PARAMETROS')[$cont_id]) ) Flight::json(array("status" => 0, "error" => "No se han definido los parametros para calcular este SLA en este contrato")) ;
    
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    $filtros = "TRUE ";
    $todasEspecialidades = false;
    
    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
        if( is_array($filtros_ini['orse_tipo']) ){
            $filtros .= " AND orse_tipo IN ('".implode("','",$filtros_ini['orse_tipo'])."') ";
        } else {
            $filtros .= " AND orse_tipo = '".$filtros_ini['orse_tipo']."' ";
        }
    } else {
        $filtros .= " AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  ";
    }
    
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 
    else if( !isset($filtros_ini['zona_id']) || $filtros_ini['zona_id']=="" ){
        $todasEspecialidades = true;
        //Flight::json(array("status" => 0, "error" => "todasEspecialidades"));
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    
    
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona_tipo = '".$filtros_ini['zona_tipo']."'";
    }
       
    //$filtros = Flight::filtersToWhereString( array("orden_servicio", "zona"), $filtros_ini);
    
    $query = "SELECT 
                    
                    zona_id 
                    ,zona_nombre                    
                    ,regi_id
                    ,regi_nombre
                    ,espe_id
                    ,espe_nombre
                    ,SUM( orse_tipo = 'OSGN') as total_osgn
                    ,SUM( orse_tipo = 'OSGU') as total_osgu
                    ,SUM( orse_tipo = 'OSEN') as total_osen
                    ,SUM( orse_tipo = 'OSEU') as total_oseu
                    ,SUM( orse_tipo = 'OSGN' OR orse_tipo = 'OSEN'  ) as total_osgn_osen
                    ,SUM( orse_tipo = 'OSGU' OR orse_tipo = 'OSEU'  ) as total_osgu_oseu
                    ,SUM( (orse_tipo = 'OSGN' OR orse_tipo = 'OSEN') AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osgn_osen_sla
                    ,SUM( (orse_tipo = 'OSGU' OR orse_tipo = 'OSEU') AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_osgu_oseu_sla
                    
            FROM (

                    SELECT 
                            sos.orse_id
                            ,sos.orse_tipo
                            ,sos.orse_estado
                            ,sos.orse_fecha_creacion
                            ,sos.orse_fecha_solicitud
                            ,sos.subida_servicio
                            ,sos.sla_tiempo_respuesta - sos.sla_tiempo_respuesta_ajuste*3600 AS sla_tiempo_respuesta
                            ,sos.regi_id
                            ,sos.regi_nombre
                            ,sos.espe_id
                            ,sos.espe_nombre
                            ,regi.regi_orden
                            ,zona.zona_id
                            ,zona.zona_tipo
                            ,zona.zona_nombre
                    FROM 
                            sla_orden_servicio sos
                            INNER JOIN (  
                                SELECT empl.empl_id
                                FROM 
                                    rel_contrato_usuario rcu 
                                    INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                    INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                    INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                WHERE 
                                    rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                GROUP BY empl_id
                            ) alcance ON (alcance.empl_id = sos.empl_id)                            

                            #para filtros
                            INNER JOIN region regi ON sos.regi_id = regi.regi_id
                            INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                            INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                    WHERE
                            sos.cont_id = $cont_id
                            AND sos.sla_tiempo_respuesta_exclusion = 0                        
                            AND $filtros
                    GROUP BY orse_id
            ) t
            ".((isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES")?
                " GROUP BY zona_id ".((isset($filtros_ini['espe_id']))?",espe_id":"")." ORDER BY zona_id,espe_id"
                :" GROUP BY regi_id".((isset($filtros_ini['espe_id']))?",espe_id":"")." ORDER BY regi_orden,espe_id ")."
            ;";
    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out = $res['data'];
    foreach ($out AS &$row) {
        $row['porcentaje_osgn_osen_sla'] = ($row['total_osgn_osen']>0)?round($row['total_osgn_osen_sla']/$row['total_osgn_osen']*100,2):100;
        $row['porcentaje_osgu_oseu_sla'] = ($row['total_osgu_oseu']>0)?round($row['total_osgu_oseu_sla']/$row['total_osgu_oseu']*100,2):100;
        $row['zona_id'] = (isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']=="REGIONES" )?$row['regi_id']:$row['zona_id'];
        $row['zona_nombre'] = (isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']=="REGIONES" )?$row['regi_nombre']:$row['zona_nombre'];
    }
    
    
    if ($todasEspecialidades && count($out > 0)) {
        $out_filtered = array();
        $out_filtered[] = $out[0];
        $out_filtered[0]['espe_id'] = 0;
        $out_filtered[0]['espe_nombre'] = 'TODAS';

        for ($i = 1; $i < count($out); $i++) {
            if ($out_filtered[count($out_filtered) - 1]['zona_id'] == $out[$i]['zona_id']) {
                $out_filtered[count($out_filtered) - 1]['total_osgn'] = $out_filtered[count($out_filtered) - 1]['total_osgn'] + $out[$i]['total_osgn'];
                $out_filtered[count($out_filtered) - 1]['total_osgu'] = $out_filtered[count($out_filtered) - 1]['total_osgu'] + $out[$i]['total_osgu'];
                $out_filtered[count($out_filtered) - 1]['total_osen'] = $out_filtered[count($out_filtered) - 1]['total_osen'] + $out[$i]['total_osen'];
                $out_filtered[count($out_filtered) - 1]['total_oseu'] = $out_filtered[count($out_filtered) - 1]['total_oseu'] + $out[$i]['total_oseu'];
                $out_filtered[count($out_filtered) - 1]['total_osgn_osen'] = $out_filtered[count($out_filtered) - 1]['total_osgn_osen'] + $out[$i]['total_osgn_osen'];
                $out_filtered[count($out_filtered) - 1]['total_osgn_osen_sla'] = $out_filtered[count($out_filtered) - 1]['total_osgn_osen_sla'] + $out[$i]['total_osgn_osen_sla'];
                $out_filtered[count($out_filtered) - 1]['total_osgu_oseu'] = $out_filtered[count($out_filtered) - 1]['total_osgu_oseu'] + $out[$i]['total_osgu_oseu'];
                $out_filtered[count($out_filtered) - 1]['total_osgu_oseu_sla'] = $out_filtered[count($out_filtered) - 1]['total_osgu_oseu_sla'] + $out[$i]['total_osgu_oseu_sla'];
            } else {
                $out_filtered[] = $out[$i];
                $out_filtered[count($out_filtered) - 1]['espe_id'] = 0;
                $out_filtered[count($out_filtered) - 1]['espe_nombre'] = 'TODAS';
            }
        }
        
        $out = $out_filtered;
        foreach ($out AS &$detalle) {      
            $detalle['porcentaje_osgn_osen_sla'] = ($detalle['total_osgn_osen']>0)?round($detalle['total_osgn_osen_sla']/$detalle['total_osgn_osen']*100,2):100;
            $detalle['porcentaje_osgu_oseu_sla'] = ($detalle['total_osgu_oseu']>0)?round($detalle['total_osgu_oseu_sla']/$detalle['total_osgu_oseu']*100,2):100;
        }
    }

    return $out;

});


//SLA___________________________________________________________________________

Flight::route('GET /contrato/@cont_id:[0-9]+/sla/filtros',function($cont_id){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO' AND cont_id=$cont_id");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['especialidades'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO' AND cont_id=$cont_id AND flag_grafico=1");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['especialidades_mnt'] = $res['data'];

    $res = $dbo->ExecuteQuery("SHOW COLUMNS FROM orden_servicio WHERE Field = 'orse_tipo'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['tipos'] = explode("','", $matches[1]);
    //Borramos la OSI del SLA
    $out['tipos'] = array_diff($out['tipos'], array('OSI'));
   
    $out['zonas_tipos'] = array( 'CONTRATO', 'MOVISTAR', 'REGIONES' );
    
    //TODO: cuando se consulta un SLA historico no se debe filtrar por zonas en estado 'ACTIVO'
    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_tipo='CONTRATO' AND zona_estado='ACTIVO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zonas_contrato'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $cont_id . " AND zona_estado = 'ACTIVO' AND zona_tipo='MOVISTAR'");

    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zonas_movistar'] = $res['data'];
    
    $res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=1 ORDER BY regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];


    $out['sla_cumple'] = array('NO', 'SI');

//    $res = $dbo->ExecuteQuery("SELECT p.peri_id, p.peri_nombre FROM periodicidad p, rel_contrato_periodicidad rcp WHERE rcp.cont_id=" . $cont_id . " AND rcp.peri_id=p.peri_id");
//    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
//    $out['periodos'] = $res['data'];
    
    Flight::json($out);
});


#Tiempos de Respuesta___________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/tiempos/*', function($cont_id){
    if( !isset(Flight::get('SLA_PARAMETROS')[$cont_id]) ){
        Flight::json(array("status" => 0, "error" => "No se han definido los parametros para calcular este SLA en este contrato")) ;
    }
    return true;
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/tiempos/resumen', function($cont_id) {     
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    $filtros_ini = array_merge($_GET,$_POST);
    $dbo = new MySQL_Database();
    /*if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);*/
    /*if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);*/


    $filtros = "TRUE ";

   
    /* if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
       $filtros .= " AND orse_id IN (".implode(",",$filtros_ini['orse_tipo']).") ";
   }
   else{
       $filtros .= " AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  ";
   }*/
   
   

    /* 
    $zona_id= $filtros_ini['zona_id'];

    $query = "SELECT $zona_id  FROM DUAL";
    $res = $dbo->ExecuteQuery($query);
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!="" ){
        if( is_array($filtros_ini['regi_id']) )
            $filtros .= " AND regi_id IN (".implode(",",$filtros_ini['regi_id']).") ";
        else
            $filtros .= " AND regi_id = ".$filtros_ini['regi_id']." ";
    }*/

    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }
    
    if( isset($filtros_ini['zona_id']) && ""!=$filtros_ini['zona_id']){
        $array_zona = explode("-", $filtros_ini['zona_id']); 
        $zona_nombre = $array_zona[0];
        $zona_id = $array_zona[1];

        if('MOVISTAR'==$zona_nombre){     
            $filtros .= " AND zmov.zona_id= ".$zona_id." ";
        } elseif ('CONTRATO'==$zona_nombre) {        
            $filtros .= " AND zcon.zona_id = ".$zona_id." ";
        }
    }

    if( isset($filtros_ini['regi_id']) && ""!=$filtros_ini['regi_id']){
        $array_zona = explode("-", $filtros_ini['regi_id']); 
        $zona_nombre = $array_zona[0];
        $zona_id = $array_zona[1];

        if ('REGIONES'==$zona_nombre) {
            $filtros .= " AND regi_id = ".$zona_id." ";
           
        }
    }

    $query = "SELECT 

                     SUM( orse_tipo = 'OSGN' ) AS total_osgn
                    ,SUM( orse_tipo = 'OSGU' ) AS total_osgu
                    ,SUM( orse_tipo = 'OSEN' ) AS total_osen
                    ,SUM( orse_tipo = 'OSEU' ) AS total_oseu
                    
                    ,SUM( orse_tipo = 'OSGN' AND sla_tiempo_respuesta IS NOT NULL ) AS total_osgn_tiempo_respuesta
                    ,SUM( orse_tipo = 'OSGU' AND sla_tiempo_respuesta IS NOT NULL ) AS total_osgu_tiempo_respuesta
                    ,SUM( orse_tipo = 'OSEN' AND sla_tiempo_respuesta IS NOT NULL ) AS total_osen_tiempo_respuesta	
                    ,SUM( orse_tipo = 'OSEU' AND sla_tiempo_respuesta IS NOT NULL ) AS total_oseu_tiempo_respuesta
                    
                    ,SUM( orse_tipo = 'OSGN' AND sla_tiempo_respuesta < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])."*3600 ) AS total_osgn_sla_tiempo_respuesta
                    ,SUM( orse_tipo = 'OSGU' AND sla_tiempo_respuesta < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])."*3600 ) AS total_osgu_sla_tiempo_respuesta
                    ,SUM( orse_tipo = 'OSEN' AND sla_tiempo_respuesta < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])."*3600 ) AS total_osen_sla_tiempo_respuesta	
                    ,SUM( orse_tipo = 'OSEU' AND sla_tiempo_respuesta < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])."*3600 ) AS total_oseu_sla_tiempo_respuesta

                    ,SUM( orse_tipo = 'OSGN' AND sla_entrega_presupuesto IS NOT NULL ) AS total_osgn_presupuesto
                    ,SUM( orse_tipo = 'OSGN' AND sla_entrega_presupuesto IS NOT NULL AND sla_entrega_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_ENTREGA'])."*3600 ) AS total_osgn_sla_entrega_presupuesto
                    ,SUM( orse_tipo = 'OSGN' AND sla_entrega_presupuesto IS NOT NULL AND sla_validacion_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_VALIDACION'])."*3600 ) AS total_osgn_sla_validacion_presupuesto

                    ,SUM( orse_tipo = 'OSGU' AND sla_entrega_presupuesto IS NOT NULL ) AS total_osgu_presupuesto
                    ,SUM( orse_tipo = 'OSGU' AND sla_entrega_presupuesto IS NOT NULL AND sla_entrega_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_ENTREGA'])."*3600 ) AS total_osgu_sla_entrega_presupuesto
                    ,SUM( orse_tipo = 'OSGU' AND sla_entrega_presupuesto IS NOT NULL AND sla_validacion_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_VALIDACION'])."*3600 ) AS total_osgu_sla_validacion_presupuesto

                    ,SUM( orse_tipo = 'OSEN' AND sla_entrega_presupuesto IS NOT NULL ) AS total_osen_presupuesto
                    ,SUM( orse_tipo = 'OSEN' AND sla_entrega_presupuesto IS NOT NULL AND sla_entrega_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_ENTREGA'])."*3600 ) AS total_osen_sla_entrega_presupuesto
                    ,SUM( orse_tipo = 'OSEN' AND sla_entrega_presupuesto IS NOT NULL AND sla_validacion_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_VALIDACION'])."*3600 ) AS total_osen_sla_validacion_presupuesto

                    ,SUM( orse_tipo = 'OSEU' AND sla_entrega_presupuesto IS NOT NULL ) AS total_oseu_presupuesto
                    ,SUM( orse_tipo = 'OSEU' AND sla_entrega_presupuesto IS NOT NULL AND sla_entrega_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_ENTREGA'])."*3600 ) AS total_oseu_sla_entrega_presupuesto
                    ,SUM( orse_tipo = 'OSEU' AND sla_entrega_presupuesto IS NOT NULL AND sla_validacion_presupuesto < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_PPTO_VALIDACION'])."*3600 ) AS total_oseu_sla_validacion_presupuesto

                    ,SUM( orse_tipo = 'OSGN' AND sla_entrega_informe IS NOT NULL ) AS total_osgn_informe
                    ,SUM( orse_tipo = 'OSGN' AND sla_entrega_informe IS NOT NULL AND sla_entrega_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_ENTREGA'])."*3600 ) AS total_osgn_sla_entrega_informe
                    ,SUM( orse_tipo = 'OSGN' AND sla_entrega_informe IS NOT NULL AND sla_validacion_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_VALIDACION'])."*3600 ) AS total_osgn_sla_validacion_informe

                    ,SUM( orse_tipo = 'OSGU' AND sla_entrega_informe IS NOT NULL ) AS total_osgu_informe
                    ,SUM( orse_tipo = 'OSGU' AND sla_entrega_informe IS NOT NULL AND sla_entrega_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_ENTREGA'])."*3600 ) AS total_osgu_sla_entrega_informe
                    ,SUM( orse_tipo = 'OSGU' AND sla_entrega_informe IS NOT NULL AND sla_validacion_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_VALIDACION'])."*3600 ) AS total_osgu_sla_validacion_informe

                    ,SUM( orse_tipo = 'OSEN' AND sla_entrega_informe IS NOT NULL ) AS total_osen_informe
                    ,SUM( orse_tipo = 'OSEN' AND sla_entrega_informe IS NOT NULL AND sla_entrega_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_ENTREGA'])."*3600 ) AS total_osen_sla_entrega_informe
                    ,SUM( orse_tipo = 'OSEN' AND sla_entrega_informe IS NOT NULL AND sla_validacion_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_VALIDACION'])."*3600 ) AS total_osen_sla_validacion_informe

                    ,SUM( orse_tipo = 'OSEU' AND sla_entrega_informe IS NOT NULL ) AS total_oseu_informe
                    ,SUM( orse_tipo = 'OSEU' AND sla_entrega_informe IS NOT NULL AND sla_entrega_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_ENTREGA'])."*3600 ) AS total_oseu_sla_entrega_informe
                    ,SUM( orse_tipo = 'OSEU' AND sla_entrega_informe IS NOT NULL AND sla_validacion_informe < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_INFORME_VALIDACION'])."*3600 ) AS total_oseu_sla_validacion_informe


                    ,SUM( orse_tipo = 'OSGN' AND sla_solucion_final IS NOT NULL ) AS total_osgn_solucion_final
                    ,SUM( orse_tipo = 'OSGN' AND sla_solucion_final IS NOT NULL AND sla_solucion_final < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_SOLUCION_FINAL'])."*3600 ) AS total_osgn_sla_solucion_final

                    ,SUM( orse_tipo = 'OSGU' AND sla_solucion_final IS NOT NULL ) AS total_osgu_solucion_final
                    ,SUM( orse_tipo = 'OSGU' AND sla_solucion_final IS NOT NULL AND sla_solucion_final < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_SOLUCION_FINAL'])."*3600 ) AS total_osgu_sla_solucion_final

                    ,SUM( orse_tipo = 'OSEN' AND sla_solucion_final IS NOT NULL ) AS total_osen_solucion_final
                    ,SUM( orse_tipo = 'OSEN' AND sla_solucion_final IS NOT NULL AND sla_solucion_final < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_SOLUCION_FINAL'])."*3600 ) AS total_osen_sla_solucion_final

                    ,SUM( orse_tipo = 'OSEU' AND sla_solucion_final IS NOT NULL ) AS total_oseu_solucion_final
                    ,SUM( orse_tipo = 'OSEU' AND sla_solucion_final IS NOT NULL AND sla_solucion_final < ".(Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_SOLUCION_FINAL'])."*3600 ) AS total_oseu_sla_solucion_final

                    ,AVG( IF( orse_tipo = 'OSGN', sla_tiempo_orse, null) ) AS prom_osgn_tiempo
                    ,AVG( IF( orse_tipo = 'OSGU', sla_tiempo_orse, null) ) AS prom_osgu_tiempo
                    ,AVG( IF( orse_tipo = 'OSEN', sla_tiempo_orse, null) ) AS prom_osen_tiempo
                    ,AVG( IF( orse_tipo = 'OSEU', sla_tiempo_orse, null) ) AS prom_oseu_tiempo	

                     ,zona_movistar
                    ,zona_contrato
                    ,regi_id
            FROM(
                    SELECT #sos.*
                            sos.orse_id
                            ,sos.orse_tipo
                            ,sos.sla_tiempo_orse
                            ,sos.sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600 AS sla_tiempo_respuesta
                            ,sos.sla_entrega_presupuesto
                            ,sos.sla_validacion_presupuesto
                            ,sos.sla_entrega_informe
                            ,sos.sla_validacion_informe
                            ,sos.sla_solucion_final
                            ,zmov.zona_id zona_movistar
                            ,zcon.zona_id zona_contrato
                            ,regi_id
                    FROM
                            sla_orden_servicio sos
                            INNER JOIN rel_zona_emplazamiento rze ON rze.empl_id = sos.empl_id
                            INNER JOIN zona zmov ON zmov.zona_id = rze.zona_id
                                                    AND zmov.zona_tipo ='MOVISTAR'
                                                    AND zmov.zona_estado = 'ACTIVO'
                                                    AND zmov.cont_id =  $cont_id
                            INNER JOIN rel_zona_emplazamiento rco ON rco.empl_id = sos.empl_id
                            INNER JOIN zona zcon ON zcon.zona_id = rco.zona_id
                                                    AND zcon.zona_tipo ='CONTRATO'
                                                    AND zcon.zona_estado = 'ACTIVO'
                                                    AND zcon.cont_id = $cont_id
                            INNER JOIN (   ## Verifificar los emplazamientos que puede ver el usuario                               
                                SELECT  distinct empl.empl_id 
                                FROM
                                    rel_contrato_usuario rcu
                                    INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id AND recu_estado ='ACTIVO')
                                    INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                    INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id AND empl.empl_estado = 'ACTIVO')
                                    INNER JOIN rel_contrato_emplazamiento e ON  e.cont_id =$cont_id and e.empl_id = rze.empl_id
                                WHERE rcu.usua_id = $usua_id
                                AND rcu.cont_id =$cont_id
                            ) alcance ON (alcance.empl_id = sos.empl_id)
                    WHERE                       
                            sos.cont_id =$cont_id
                            AND sos.sla_tiempo_respuesta_exclusion = 0 
                            AND $filtros
            ) t";
    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
	
    $tipos_os = array('OSGN','OSGU','OSEN','OSEU');
    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
        $tipos_os = (is_array($filtros_ini['orse_tipo']))? $filtros_ini['orse_tipo']:array($filtros_ini['orse_tipo']);
    }
  
    if ( count($res['data']) == 0 ) {
        $out['resumen'] = array(
            'total_osgn' => '0',
            'total_osgu' => '0',
            'total_osen' => '0',
            'total_oseu' => '0',
            'prom_osgn_tiempo' => '0',
            'prom_osgu_tiempo' => '0',
            'prom_osen_tiempo' => '0',
            'prom_oseu_tiempo' => '0',
            'sla_tiempo_respuesta' => '0',
            'sla_entrega' => '0',
            'sla_validacion_ppto' => '0',
            'sla_solucion_final' => '0',
        );          
    }
    else{
        $out['resumen'] = array(
            'total_osgn' => $res['data'][0]['total_osgn'],
            'total_osgu' => $res['data'][0]['total_osgu'],
            'total_osen' => $res['data'][0]['total_osen'],
            'total_oseu' => $res['data'][0]['total_oseu'],
            
            'prom_osgn_tiempo' => elapsedSeconds2string($res['data'][0]['prom_osgn_tiempo']),
            'prom_osgu_tiempo' => elapsedSeconds2string($res['data'][0]['prom_osgu_tiempo']),
            'prom_osen_tiempo' => elapsedSeconds2string($res['data'][0]['prom_osen_tiempo']),
            'prom_oseu_tiempo' => elapsedSeconds2string($res['data'][0]['prom_oseu_tiempo']),
            
            'sla_tiempo_respuesta'       => totalSLASelected($tipos_os, $res['data'][0],'_tiempo_respuesta','_sla_tiempo_respuesta'),
            'sla_entrega_presupuesto'    => totalSLASelected($tipos_os, $res['data'][0],'_presupuesto','_sla_entrega_presupuesto'),
            'sla_validacion_presupuesto' => totalSLASelected($tipos_os, $res['data'][0],'_presupuesto','_sla_validacion_presupuesto'),
            'sla_entrega_informe'        => totalSLASelected($tipos_os, $res['data'][0],'_informe','_sla_entrega_informe'),
            'sla_validacion_informe'     => totalSLASelected($tipos_os, $res['data'][0],'_informe','_sla_validacion_informe'),
            'sla_solucion_final'         => totalSLASelected($tipos_os, $res['data'][0],'_solucion_final','_sla_solucion_final'),
        ); 
		
        $out['datos'] = $res['data'][0];        
    }
    
    
    
    $out['status'] = 1;    
    
    Flight::json($out);
});

function totalSLASelected( $tipos_os, $row, $base_total, $base_sla ){   
    
    $total = 0;
    $total += (in_array('OSGN',$tipos_os))?$row["total_osgn$base_total"]:0;
    $total += (in_array('OSGU',$tipos_os))?$row["total_osgu$base_total"]:0;
    $total += (in_array('OSEN',$tipos_os))?$row["total_osen$base_total"]:0;
    $total += (in_array('OSEU',$tipos_os))?$row["total_oseu$base_total"]:0;
        
    $total_sla = 0;
    $total_sla += (in_array('OSGN',$tipos_os))?$row["total_osgn$base_sla"]:0;
    $total_sla += (in_array('OSGU',$tipos_os))?$row["total_osgu$base_sla"]:0;
    $total_sla += (in_array('OSEN',$tipos_os))?$row["total_osen$base_sla"]:0;
    $total_sla += (in_array('OSEU',$tipos_os))?$row["total_oseu$base_sla"]:0;
    
    return ($total!=0)? round(100.0*$total_sla/$total,2) : 0;
}

function elapsedSeconds2string($time) 
{
    $time = (float)$time;
    $points = array(
        'd'   => 86400,
        'h'   => 3600,
        'm'   => 60,
        's'   => 1
    );
    
    $timestamp = "";

    foreach ($points as $point => $value) {
        $elapsed = floor($time / $value);                    
        if ( $elapsed > 0) {
            $timestamp = $timestamp." $elapsed$point";
            $time = $time - $elapsed*$value;
        }
    }
    
    return $timestamp;
}

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/tiempos/detalle', function($cont_id) {
    $filtros_ini = array_merge($_GET,$_POST);
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLATiempos($cont_id,$filtros_ini);    
    Flight::json($out);
});

//Contiene la logica detras de los graficos de sla tiempo respuesta
Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/tiempos/graficos', function($cont_id) {
    $out = array();
    $usua_id = $_SESSION['user_id'];
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $filtros .= " AND orse_tipo IN ('OSGN', 'OSEN')  ";
    $filtrosTipoZona = "";
    $filtrosZonaConsultada = "";
    $filtrosFecha = "";
    $isFiltroPorZona = false;

    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' ";
        $filtrosFecha .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ;
        $filtrosFecha .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    

    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosTipoZona .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona=true;
    }
    
    //$filtros = Flight::filtersToWhereString( array("orden_servicio", "zona"), $filtros_ini);
    /*
    $query = "SELECT 
                    SUM( orse_tipo = 'OSGN'  AND sla_tiempo_respuesta IS NOT NULL) as total_osgn
                    ,SUM( orse_tipo = 'OSGU' AND sla_tiempo_respuesta IS NOT NULL) as total_osgu
                    ,SUM( orse_tipo = 'OSEN' AND sla_tiempo_respuesta IS NOT NULL) as total_osen
                    ,SUM( orse_tipo = 'OSEU' AND sla_tiempo_respuesta IS NOT NULL) as total_oseu
                    ,SUM( orse_tipo = 'OSGN' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osgn_sla
                    ,SUM( orse_tipo = 'OSEN' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osen_sla
                    ,SUM( orse_tipo = 'OSGU' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_osgu_sla
                    ,SUM( orse_tipo = 'OSEU' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_oseu_sla
                    ,zona_nombre
                    ,regi_nombre
            FROM (

                    SELECT 
                            sos.orse_id
                            ,sos.orse_tipo
                            ,sos.orse_estado
                            ,sos.orse_fecha_creacion
                            ,sos.orse_fecha_solicitud
                            ,sos.subida_servicio
                            ,sos.sla_tiempo_respuesta - sos.sla_tiempo_respuesta_ajuste*3600 AS sla_tiempo_respuesta
                            ,sos.regi_id
                            ,sos.regi_nombre
                            ,regi.regi_orden
                            ,zona.zona_id
                            ,zona.zona_tipo
                            ,zona.zona_nombre
                    FROM 
                            sla_orden_servicio sos
                            INNER JOIN (  
                                SELECT empl.empl_id
                                FROM 
                                    rel_contrato_usuario rcu 
                                    INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                    INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                    INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                WHERE 
                                    rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                GROUP BY empl_id
                            ) alcance ON (alcance.empl_id = sos.empl_id)
                                
                            #para filtros
                            INNER JOIN region regi ON sos.regi_id = regi.regi_id
                            INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                            INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                    WHERE
                            sos.cont_id = $cont_id
                            AND sos.sla_tiempo_respuesta_exclusion = 0                        
                            AND $filtros
                    GROUP BY orse_id
            ) t
            ".((isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES")?" GROUP BY zona_id ":" GROUP BY regi_id ORDER BY regi_orden ")."
            ";
    */
    if($isFiltroPorZona){
        $query = "SELECT    zona.zona_id
                            , zona.zona_nombre
                            , total_osgn
                            , total_osgu
                            , total_osen
                            , total_oseu
                            , total_osgn_sla
                            , total_osen_sla
                            , total_osgu_sla
                            , total_oseu_sla
                            FROM zona
                    LEFT JOIN (
                                SELECT 
                                        zona_consultada.zona_id
                                        ,zona_consultada.zona_nombre
                                        ,SUM( orse_tipo = 'OSGN') as total_osgn
                                        ,SUM( orse_tipo = 'OSGU') as total_osgu
                                        ,SUM( orse_tipo = 'OSEN') as total_osen
                                        ,SUM( orse_tipo = 'OSEU') as total_oseu
                                        ,SUM( orse_tipo = 'OSGN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osgn_sla
                                        ,SUM( orse_tipo = 'OSEN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osen_sla
                                        ,SUM( orse_tipo = 'OSGU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_osgu_sla
                                        ,SUM( orse_tipo = 'OSEU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_oseu_sla
                                FROM sla_orden_servicio sos
                                INNER JOIN emplazamiento e on  (sos.empl_id = e.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id AND rce.cont_id = sos.cont_id)
                                INNER JOIN  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                                INNER JOIN zona zona_consultada on (zona_consultada.zona_id = rze.zona_id
                                                                    AND zona_consultada.cont_id = sos.cont_id
                                                                    AND zona_consultada.zona_estado = 'ACTIVO'
                                                                    $filtrosZonaConsultada)
                                                                    
                                WHERE sos.cont_id = $cont_id
                                    AND  sos.sla_tiempo_respuesta_exclusion = 0
                                    $filtrosFecha
                                    AND sos.orse_tipo IN ('OSGN', 'OSEN')
                                GROUP BY zona_consultada.zona_id, zona_consultada.zona_nombre

                    ) x ON x.zona_id = zona.zona_id
                    WHERE zona.cont_id = $cont_id  #### valor que entra por parametro
                        AND zona.zona_estado = 'ACTIVO' 
                    $filtrosTipoZona";
    }else{        

        $query = "SELECT  region.regi_id AS regi_id
                            , region.regi_nombre as regi_nombre
                            , total_osgn
                            , total_osgu
                            , total_osen
                            , total_oseu
                            , total_osgn_sla
                            , total_osen_sla
                            , total_osgu_sla
                            , total_oseu_sla
            FROM region
            INNER JOIN pais ON pais.pais_id = region.pais_id
            INNER JOIN contrato cont ON cont.pais_id =  pais.pais_id
            LEFT JOIN (
                        SELECT  
                                r.regi_id
                                ,r.regi_nombre
                                ,SUM( orse_tipo = 'OSGN') as total_osgn
                                ,SUM( orse_tipo = 'OSGU') as total_osgu
                                ,SUM( orse_tipo = 'OSEN') as total_osen
                                ,SUM( orse_tipo = 'OSEU') as total_oseu
                                ,SUM( orse_tipo = 'OSGN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osgn_sla
                                ,SUM( orse_tipo = 'OSEN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osen_sla
                                ,SUM( orse_tipo = 'OSGU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_osgu_sla
                                ,SUM( orse_tipo = 'OSEU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_oseu_sla
                        FROM sla_orden_servicio sos
                            INNER JOIN emplazamiento e on  (sos.empl_id = e.empl_id)
                            INNER JOIN rel_contrato_emplazamiento rce ON (rce.empl_id = e.empl_id AND rce.cont_id = sos.cont_id)
                            INNER JOIN comuna c ON (c.comu_id = e.comu_id)
                            INNER JOIN provincia p ON (p.prov_id = c.prov_id)
                            INNER JOIN region r ON (r.regi_id = p.regi_id)
                        WHERE sos.cont_id = $cont_id 
                            AND  sos.sla_tiempo_respuesta_exclusion = 0
                            $filtrosFecha
                            #AND sos.orse_tipo IN ('OSGU', 'OSEU')
                        GROUP BY r.regi_id
                                ,r.regi_nombre
                                 
            ) x on (x.regi_id = region.regi_id)
            where cont.cont_id = $cont_id
			ORDER BY region.regi_orden
        ";
    }

    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['grafico_zonas'] = array();
    
    $tipos_os = array('OSGN','OSEN');
    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
        $tipos_os = (is_array($filtros_ini['orse_tipo']))? $filtros_ini['orse_tipo']:array($filtros_ini['orse_tipo']);
    }
    
    $total_general = 0;
    $total_sla_general = 0;
    
    foreach ($res['data'] AS $row) {
        $aux = array();        
        $total = 0;
        $total_sla = 0;
        //Corresponde al total de OS en el periodo y con los filtros datos
        $total += (in_array('OSGN',$tipos_os))?$row['total_osgn']:0;
        $total += (in_array('OSGU',$tipos_os))?$row['total_osgu']:0;
        $total += (in_array('OSEN',$tipos_os))?$row['total_osen']:0;
        $total += (in_array('OSEU',$tipos_os))?$row['total_oseu']:0;
        //Corresponde a las OS que cumplen con los requisitos de tiempo
        $total_sla += (in_array('OSGN',$tipos_os))?$row['total_osgn_sla']:0;
        $total_sla += (in_array('OSGU',$tipos_os))?$row['total_osgu_sla']:0;
        $total_sla += (in_array('OSEN',$tipos_os))?$row['total_osen_sla']:0;
        $total_sla += (in_array('OSEU',$tipos_os))?$row['total_oseu_sla']:0;
        
        //$aux['valor'] = round(100.0*$total_sla/$total,2);
        
        $aux['aprobadas'] = $total_sla;
        $aux['textoAprobadas'] = 'Cumple';
        $aux['rechazadas'] = $total-$total_sla;
        $aux['textoRechazadas'] = 'No Cumple';
        $aux['umbral'] = 90;
        $aux['total'] = $total;
        $aux['nombre'] = (isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']=="REGIONES" )?$row['regi_nombre']:$row['zona_nombre'];
        
        /* Variables usadas para el gráfico de torta
        $total_general += $total;
        $total_sla_general += $total_sla;
        */
        $out['grafico_zonas'][] = $aux;
    }
    $out['grafico']['aprobadas']='Total OS Cumple';
    $out['grafico']['rechazadas']='Total OS No Cumple';
    $out['grafico']['sindatos']='Sin OS';
    $out['grafico']['textoUmbral'] = ' 10% ';
    $out['status'] = 1;
    Flight::json($out);
});


Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/tiempos/graficos_oseu_osgu', function($cont_id) {
    $out = array();
    $usua_id = $_SESSION['user_id'];
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $filtrosFecha = "";
    $filtrosEspecialidad = "";
    $filtrosZonaConsultada = "";
    $filtrosTipoZona = "";
    $filtros .= " AND orse_tipo IN ('OSGU', 'OSEU')  ";
    $isFiltroPorZona=false;
        
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ;
        $filtrosFecha .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ;
        $filtrosFecha .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    
    
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosTipoZona .= " AND zona.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $filtrosZonaConsultada .= " AND zona_consultada.zona_tipo = '".$filtros_ini['zona_tipo']."'";
        $isFiltroPorZona=true;
    }
    
    //$filtros = Flight::filtersToWhereString( array("orden_servicio", "zona"), $filtros_ini);
    if($isFiltroPorZona){
        $query = "SELECT    zona.zona_id
                            , zona.zona_nombre
                            , total_osgn
                            , total_osgu
                            , total_osen
                            , total_oseu
                            , total_osgn_sla
                            , total_osen_sla
                            , total_osgu_sla
                            , total_oseu_sla
                            FROM zona
                LEFT JOIN (
                            SELECT 
                                    zona_consultada.zona_id
                                    ,zona_consultada.zona_nombre
                                    ,SUM( orse_tipo = 'OSGN') as total_osgn
                                    ,SUM( orse_tipo = 'OSGU') as total_osgu
                                    ,SUM( orse_tipo = 'OSEN') as total_osen
                                    ,SUM( orse_tipo = 'OSEU') as total_oseu
                                    ,IFNULL(SUM( orse_tipo = 'OSGN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ),0) as total_osgn_sla
                                    ,IFNULL(SUM( orse_tipo = 'OSEN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ),0) as total_osen_sla
                                    ,IFNULL(SUM( orse_tipo = 'OSGU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ),0) as total_osgu_sla
                                    ,IFNULL(SUM( orse_tipo = 'OSEU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ),0) as total_oseu_sla
                            FROM sla_orden_servicio sos
                            INNER JOIN emplazamiento e on  (sos.empl_id = e.empl_id)
                            INNER JOIN rel_contrato_emplazamiento rce on (rce.empl_id = e.empl_id AND rce.cont_id = sos.cont_id)
                            INNER JOIN  rel_zona_emplazamiento rze on (rze.empl_id = e.empl_id)
                            INNER JOIN zona zona_consultada on (zona_consultada.zona_id = rze.zona_id
                                                                AND zona_consultada.cont_id = sos.cont_id
                                                                AND zona_consultada.zona_estado = 'ACTIVO'
                                                                $filtrosZonaConsultada)
                                                                
                            WHERE sos.cont_id = $cont_id
                                AND  sos.sla_tiempo_respuesta_exclusion = 0
                                $filtrosFecha
                                AND sos.orse_tipo IN ('OSGU', 'OSEU')
                            GROUP BY zona_consultada.zona_id, zona_consultada.zona_nombre

                ) x ON x.zona_id = zona.zona_id
                WHERE zona.cont_id = $cont_id  #### valor que entra por parametro
                    AND zona.zona_estado = 'ACTIVO' 
                $filtrosTipoZona"
    ;/*
    $query = "SELECT 
                    SUM( orse_tipo = 'OSGN'  AND sla_tiempo_respuesta IS NOT NULL) as total_osgn
                    ,SUM( orse_tipo = 'OSGU' AND sla_tiempo_respuesta IS NOT NULL) as total_osgu
                    ,SUM( orse_tipo = 'OSEN' AND sla_tiempo_respuesta IS NOT NULL) as total_osen
                    ,SUM( orse_tipo = 'OSEU' AND sla_tiempo_respuesta IS NOT NULL) as total_oseu
                    ,SUM( orse_tipo = 'OSGN' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osgn_sla
                    ,SUM( orse_tipo = 'OSEN' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ) as total_osen_sla
                    ,SUM( orse_tipo = 'OSGU' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_osgu_sla
                    ,SUM( orse_tipo = 'OSEU' AND sla_tiempo_respuesta < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ) as total_oseu_sla
                    ,zona_nombre
                    ,regi_nombre
            FROM (

                    SELECT 
                            sos.orse_id
                            ,sos.orse_tipo
                            ,sos.orse_estado
                            ,sos.orse_fecha_creacion
                            ,sos.orse_fecha_solicitud
                            ,sos.subida_servicio
                            ,sos.sla_tiempo_respuesta - sos.sla_tiempo_respuesta_ajuste*3600 AS sla_tiempo_respuesta
                            ,sos.regi_id
                            ,sos.regi_nombre
                            ,regi.regi_orden
                            ,zona.zona_id
                            ,zona.zona_tipo
                            ,zona.zona_nombre
                    FROM 
                            sla_orden_servicio sos
                            INNER JOIN (  
                                SELECT empl.empl_id
                                FROM 
                                    rel_contrato_usuario rcu 
                                    INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                    INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                    INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                WHERE 
                                    rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                GROUP BY empl_id
                            ) alcance ON (alcance.empl_id = sos.empl_id)
                                
                            #para filtros
                            INNER JOIN region regi ON sos.regi_id = regi.regi_id
                            INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                            INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                    WHERE
                            sos.cont_id = $cont_id
                            AND sos.sla_tiempo_respuesta_exclusion = 0                        
                            AND $filtros
                    GROUP BY orse_id
            ) t
            ".((isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES")?" GROUP BY zona_id ":" GROUP BY regi_id ORDER BY regi_orden ")."
            ";
    */

   } else {       
        
        $query = "SELECT  x.regi_id AS zona_id
                            , x.regi_nombre
                            , total_osgn
                            , total_osgu
                            , total_osen
                            , total_oseu
                            , total_osgn_sla
                            , total_osen_sla
                            , total_osgu_sla
                            , total_oseu_sla
            FROM region
            INNER JOIN pais ON pais.pais_id = region.pais_id
            INNER JOIN contrato cont ON cont.pais_id =  pais.pais_id
            LEFT JOIN (
                        SELECT  
                                r.regi_id
                                ,r.regi_nombre
                                    ,SUM( orse_tipo = 'OSGN') as total_osgn
                                    ,SUM( orse_tipo = 'OSGU') as total_osgu
                                    ,SUM( orse_tipo = 'OSEN') as total_osen
                                    ,SUM( orse_tipo = 'OSEU') as total_oseu
                                    ,IFNULL(SUM( orse_tipo = 'OSGN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ),0) as total_osgn_sla
                                    ,IFNULL(SUM( orse_tipo = 'OSEN' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_NORMAL'])*3600)." ),0) as total_osen_sla
                                    ,IFNULL(SUM( orse_tipo = 'OSGU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ),0) as total_osgu_sla
                                    ,IFNULL(SUM( orse_tipo = 'OSEU' AND (sla_tiempo_respuesta - sla_tiempo_respuesta_ajuste*3600) < ".((Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_TPO_RESP_UMBRAL_URGENTE'])*3600)." ),0) as total_oseu_sla
                        FROM sla_orden_servicio sos
                            INNER JOIN emplazamiento e on  (sos.empl_id = e.empl_id)
                            INNER JOIN rel_contrato_emplazamiento rce ON (rce.empl_id = e.empl_id AND rce.cont_id = sos.cont_id)
                            INNER JOIN comuna c ON (c.comu_id = e.comu_id)
                            INNER JOIN provincia p ON (p.prov_id = c.prov_id)
                            INNER JOIN region r ON (r.regi_id = p.regi_id)
                        WHERE sos.cont_id = $cont_id 
                            AND  sos.sla_tiempo_respuesta_exclusion = 0
                            $filtrosFecha
                            #AND sos.orse_tipo IN ('OSGU', 'OSEU')
                        GROUP BY r.regi_id
                                ,r.regi_nombre
                                 
            ) x on (x.regi_id = region.regi_id)
            where cont.cont_id = $cont_id
			ORDER BY region.regi_orden
        ";
    } 
    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['grafico_zonas'] = array();
    
    $tipos_os = array('OSGU','OSEU');
    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
        $tipos_os = (is_array($filtros_ini['orse_tipo']))? $filtros_ini['orse_tipo']:array($filtros_ini['orse_tipo']);
    }
    
    $total_general = 0;
    $total_sla_general = 0;
    
    foreach ($res['data'] AS $row) {
        $aux = array();        
        $total = 0;
        $total_sla = 0;
        //Corresponde al total de OS en el periodo y con los filtros datos
        $total += (in_array('OSGN',$tipos_os))?$row['total_osgn']:0;
        $total += (in_array('OSGU',$tipos_os))?$row['total_osgu']:0;
        $total += (in_array('OSEN',$tipos_os))?$row['total_osen']:0;
        $total += (in_array('OSEU',$tipos_os))?$row['total_oseu']:0;
        //Corresponde a las OS que cumplen con los requisitos de tiempo
        $total_sla += (in_array('OSGN',$tipos_os))?$row['total_osgn_sla']:0;
        $total_sla += (in_array('OSGU',$tipos_os))?$row['total_osgu_sla']:0;
        $total_sla += (in_array('OSEN',$tipos_os))?$row['total_osen_sla']:0;
        $total_sla += (in_array('OSEU',$tipos_os))?$row['total_oseu_sla']:0;
        //$aux['valor'] = round(100.0*$total_sla/$total,2);
        
        $aux['aprobadas'] = $total_sla;
        $aux['textoAprobadas'] = 'Cumple';
        $aux['rechazadas'] = $total-$total_sla;
        $aux['textoRechazadas'] = 'No Cumple';
        $aux['umbral'] = 95;
        $aux['total'] = $total;
        $aux['nombre'] = (isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']=="REGIONES" )?$row['regi_nombre']:$row['zona_nombre'];
        
        /* Variables usadas para el gráfico de torta
        $total_general += $total;
        $total_sla_general += $total_sla;
        */
        $out['grafico_zonas'][] = $aux;
    }     
    $out['grafico']['titulo'] = 'Tiempos Respuesta';
    $out['grafico']['textoUmbral'] = ' 5% ';
    $out['grafico']['aprobadas']='Total OS Cumple';
    $out['grafico']['rechazadas']='Total OS No Cumple';
    $out['grafico']['sindatos']='Sin OS';
    $out['status'] = 1;
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/tiempos/validacion(/@page:[0-9]+)', function($cont_id, $page) {
    $results_by_page = 8;//Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $having = "";
    
    $out = array();  
    $usua_id = $_SESSION['user_id'];
    
    
    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
        if( is_array($filtros_ini['orse_tipo']) ){
            $filtros .= " AND orse_tipo IN ('".implode("','",$filtros_ini['orse_tipo'])."') ";
        }
        else{
            $filtros .= " AND orse_tipo = '".$filtros_ini['orse_tipo']."' ";
        }
    }
    else{
        $filtros .= " AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  ";
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    
   
   /* if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }*/

     if( isset($filtros_ini['zona_id']) && ""!=$filtros_ini['zona_id']){
        $array_zona = explode("-", $filtros_ini['zona_id']); 
        $zona_nombre = $array_zona[0];
        $zona_id = $array_zona[1];
     
            $filtros .= " AND zona.zona_id= ".$zona_id." ";
    }



    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    } 

    if( isset($filtros_ini['orse_id']) && $filtros_ini['orse_id']!="" ){
        $filtros .= " AND orse_id like '%".$filtros_ini['orse_id']."%'"; 
    }

    if( isset($filtros_ini['sla_cumple']) && $filtros_ini['sla_cumple']!="" ){
        $having .= " having sla_cumple like '".$filtros_ini['sla_cumple']."'"; 
    }

    $query= "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        sos.orse_id
                        ,sos.orse_tipo
                        ,sos.empl_nombre
                        ,sos.espe_nombre
                        ,sos.orse_descripcion
                        ,sos.info_id
                        ,sos.orse_fecha_solicitud
                        ,sos.subida_servicio
                         ,round(sos.sla_tiempo_respuesta/3600, 2) AS sla_tiempo_respuesta
                        ,sos.sla_tiempo_respuesta_ajuste
                        ,ifnull((SELECT sru.sla_respuesta_valor_umbral FROM sla_respuesta_umbral sru
                                    WHERE sru.cont_id = sos.cont_id 
                                    AND sru.orse_tipo = sos.orse_tipo
                                    Limit 1
                         ) , 0 )as sla
                        ,CASE WHEN (round(sos.sla_tiempo_respuesta/3600, 2)-sos.sla_tiempo_respuesta_ajuste) <= (
                                ifnull((SELECT sru.sla_respuesta_valor_umbral FROM sla_respuesta_umbral sru
                                            WHERE sru.cont_id = sos.cont_id 
                                            AND sru.orse_tipo = sos.orse_tipo
                                            Limit 1
                                 ) , 0 ) ) THEN 'SI'
                         ELSE 'NO' END AS sla_cumple
                        ,sos.sla_tiempo_respuesta_exclusion
                        ,TRIM(sos.sla_tiempo_respuesta_observacion) AS sla_tiempo_respuesta_observacion
                        ,sos.regi_nombre
                        ,zona.zona_nombre
                FROM 
                        sla_orden_servicio sos
                        INNER JOIN (  
                            SELECT empl.empl_id
                            FROM 
                                rel_contrato_usuario rcu 
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                            WHERE 
                                rcu.usua_id = $usua_id
                                AND rcu.cont_id = $cont_id
                            GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sos.empl_id)                        

                        INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                WHERE
                        sos.cont_id = $cont_id
                        AND $filtros
                GROUP BY orse_id
                $having 
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));  
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    
    $out['status'] = 1;
    Flight::json($out);
});


#Tasa de Fallas_________________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/fallas/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLAFallas($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/fallas/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);


    $data = Flight::ObtenerSLAFallas($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Fallas por Zona';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total Equipos sin fallas';  
    $out['grafico']['rechazadas'] = 'Total Fallas';
    $out['grafico']['sindatos']='Sin Datos';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $sla_especialidad_aprobadas=$row['total_equipos']-$row['total_os_indisponibilidad'];
         /*-$row['total_os_fallas']*/;
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $sla_especialidad_aprobadas
                                            , 'textoAprobadas' => 'Total Equipos sin fallas'
                                            , 'rechazadas' => $row['total_os_indisponibilidad']
                                            , 'textoRechazadas' => 'Total Fallas'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_equipos']);    
    }  

    $out['status'] = 1;  
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/fallas/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLAFallas($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Fallas por Especialidad';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total Equipos sin fallas';
    $out['grafico']['rechazadas'] = 'Total Fallas';
    $out['grafico']['sindatos']='Sin Datos';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $sla_especialidad_aprobadas=$row['total_os_indisponibilidad']-$row['total_os_fallas'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $sla_especialidad_aprobadas
                                            , 'textoAprobadas' => 'Total Equipos sin fallas'
                                            , 'rechazadas' => $row['total_os_fallas']
                                            , 'textoRechazadas' => 'Total Fallas'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_os_indisponibilidad']);
    }  
    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/fallas/validacion(/@page:[0-9]+)', function($cont_id, $page) {

    $results_by_page = 8;//Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $usua_id = $_SESSION['user_id'];
    
    $out = array();  
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 
    
    
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }    
    if( isset($filtros_ini['orse_id']) && $filtros_ini['orse_id']!="" ){
        $filtros .= " AND orse_id like '%".$filtros_ini['orse_id']."%'"; 
    } 
    $query = "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        sos.orse_id
                        ,sos.orse_tipo
                        ,sos.empl_nombre
                        ,sos.espe_nombre
                        ,sos.orse_descripcion
                        ,sos.info_id
                        ,sos.orse_indisponibilidad
                        ,sos.orse_fecha_creacion
                        ,sos.subida_servicio
                        ,sos.sla_tasa_fallas_exclusion
                        ,TRIM(sos.sla_tasa_fallas_observacion) AS sla_tasa_fallas_observacion
                        #para filtros
                        ,sos.regi_nombre
                        ,zona.zona_nombre
                FROM 
                        sla_orden_servicio sos
                        INNER JOIN (  
                            SELECT empl.empl_id
                            FROM 
                                rel_contrato_usuario rcu 
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                            WHERE 
                                rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                            GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sos.empl_id)               

                        INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                WHERE
                        sos.cont_id = $cont_id
                        AND $filtros
                GROUP BY orse_id                
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ";    
    //TODO: Ver especialidades activas para filtro histórico
    
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    
//    foreach ($out['validacion'] AS &$row) {
//        if( $row['sla_tiempo_respuesta']-$row['sla_tiempo_respuesta_ajuste'] <= $row['sla'] ){
//            $row['sla_cumple'] = 'SI';
//        }
//    }


    $out['status'] = 1;
    Flight::json($out);
});


#Tasa de Fallas Reiteradas______________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/reiteradas/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLAFallasReiteradas($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/reiteradas/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    //if( isset($filtros_ini['zona_tipo'])) unset($filtros_ini['zona_tipo']); 
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 

    $data = Flight::ObtenerSLAFallasReiteradas($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Fallas Reiteradas por Zona';
    $out['grafico']['textoUmbral'] = ' 5% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total OS';
    $out['grafico']['rechazadas'] = 'Total Fallas Reiteradas';
    $out['grafico']['sindatos']='Sin Datos';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $aprobadas_reiteradas=$row['total_os_fallas']-$row['total_os_fallas_reiteradas'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $aprobadas_reiteradas
                                            , 'textoAprobadas' => 'Total OS'
                                            , 'rechazadas' => $row['total_os_fallas_reiteradas']
                                            , 'textoRechazadas' => 'Total Fallas Reiteradas'
                                            , 'umbral' => 95
                                            , 'total'=>$row['total_os_fallas']);
    }
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/reiteradas/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 

    $data = Flight::ObtenerSLAFallasReiteradas($cont_id,$filtros_ini);

    $out = array();   
    $out['grafico'] = array();   
    $out['grafico']['titulo'] = 'Porcentaje de Fallas Reiteradas por Especialidad';
    $out['grafico']['textoUmbral'] = ' 5% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total OS';
    $out['grafico']['rechazadas'] = 'Total Fallas Reiteradas';
    $out['grafico']['sindatos']='Sin OS';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $aprobadas_reiteradas=$row['total_os_fallas']-$row['total_os_fallas_reiteradas'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $row['total_os_fallas_reiteradas']
                                            , 'textoAprobadas' => 'Total OS'
                                            , 'rechazadas' => $row['total_os_fallas_reiteradas']
                                            , 'textoRechazadas' => 'Total Fallas Reiteradas'
                                            , 'umbral' => 95
                                            , 'total'=>$row['total_os_fallas']);
    }    
    $out['status'] = 1;  

    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/reiteradas/validacion(/@page:[0-9]+)', function($cont_id, $page) {

    $results_by_page = 8;//Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $usua_id = $_SESSION['user_id'];
    
    $out = array();  
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 
    
    
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }    
    if( isset($filtros_ini['orse_id']) && $filtros_ini['orse_id']!="" ){
        $filtros .= " AND orse_id like '%".$filtros_ini['orse_id']."%'"; 
    }
    $query = "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        sos.orse_id
                        ,sos.orse_tipo
                        ,sos.empl_nombre
                        ,sos.espe_nombre
                        ,sos.orse_descripcion
                        ,sos.info_id
                        ,sos.orse_indisponibilidad
                        ,sos.orse_fecha_creacion
                        ,sos.subida_servicio
                        ,sos.sla_tasa_fallas_rei_exclusion
                        ,TRIM(sos.sla_tasa_fallas_rei_observacion) AS sla_tasa_fallas_rei_observacion
                        #para filtros
                        ,sos.regi_nombre
                        ,zona.zona_nombre
                FROM 
                        sla_orden_servicio sos
                        INNER JOIN (  
                            SELECT empl.empl_id
                            FROM 
                                rel_contrato_usuario rcu 
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                            WHERE 
                                rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                            GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sos.empl_id)

                        INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                WHERE
                        sos.cont_id = $cont_id
                        AND $filtros
                GROUP BY orse_id                
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ";    
    //TODO: Ver especialidades activas para filtro histórico
    
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    
//    foreach ($out['validacion'] AS &$row) {
//        if( $row['sla_tiempo_respuesta']-$row['sla_tiempo_respuesta_ajuste'] <= $row['sla'] ){
//            $row['sla_cumple'] = 'SI';
//        }
//    }


    $out['status'] = 1;
    Flight::json($out);
});


#Disponibilidad_________________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/disponibilidad/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLADisponibilidad($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/disponibilidad/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    //if( isset($filtros_ini['zona_tipo'])) unset($filtros_ini['zona_tipo']); 
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);

    if( isset($filtros_ini['espe_id'])) unset($filtros_ini['espe_id']);
    $query = "SELECT espe_id from especialidad where espe_nombre='CLIMA' AND cont_id=$cont_id";
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $filtros_ini['espe_id'] = $res['data'][0]['espe_id'];

/*
    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);

*/

    $data = Flight::ObtenerSLADisponibilidad($cont_id,$filtros_ini);

    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Disponibilidad por Zona';
    $out['grafico']['textoUmbral'] = " 99.95% de tolerancia para Energia, 99.88% para Clima y 10% para el resto de las especialidades";
    $out['grafico']['aprobadas'] = 'Horas Disponible';
    $out['grafico']['rechazadas'] = 'Horas Indisponible';
    $out['grafico']['sindatos']='Sin Datos';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        //$out['grafico']['datos'][] = array( 'nombre' => $row['espe_nombre'], 'valor' => $row['tasa_disponibilidad']);
        $umbral=90;
        if($row['espe_nombre']=='ENERGIA'){
            $umbral=99.95;
        }else if($row['espe_nombre']=='CLIMA'){
            $umbral=99.88;
        }
        $total_horas_disponible=$row['total_horas_periodo']-$row['total_os_indisponibilidad_horas'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            ,'aprobadas' => $total_horas_disponible
                                            ,'textoAprobadas' => 'Horas Disponible'
                                            ,'rechazadas'=> $row['total_os_indisponibilidad_horas']
                                            ,'textoRechazadas'=> 'Horas Indisponible'
                                            ,'umbral' => $umbral
                                            ,'total' => $row['total_horas_periodo']);
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});


Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/disponibilidad/grafico_disponiblidad_energia', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    //if( isset($filtros_ini['zona_tipo'])) unset($filtros_ini['zona_tipo']); 
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);

    if( isset($filtros_ini['espe_id'])) unset($filtros_ini['espe_id']);
    $query = "SELECT espe_id from especialidad where espe_nombre='ENERGIA' AND cont_id=$cont_id";
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $filtros_ini['espe_id'] = $res['data'][0]['espe_id'];

/*
    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);

*/

    $data = Flight::ObtenerSLADisponibilidad($cont_id,$filtros_ini);

    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Disponibilidad por Zona';
    $out['grafico']['textoUmbral'] = " 99.95% de tolerancia para Energia, 99.88% para Clima y 10% para el resto de las especialidades";
    $out['grafico']['aprobadas'] = 'Horas Disponible';
    $out['grafico']['rechazadas'] = 'Horas Indisponible';
    $out['grafico']['sindatos']='Sin Datos';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        //$out['grafico']['datos'][] = array( 'nombre' => $row['espe_nombre'], 'valor' => $row['tasa_disponibilidad']);
        $umbral=90;
        if($row['espe_nombre']=='ENERGIA'){
            $umbral=99.95;
        }else if($row['espe_nombre']=='CLIMA'){
            $umbral=99.88;
        }
        $total_horas_disponible=$row['total_horas_periodo']-$row['total_os_indisponibilidad_horas'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            ,'aprobadas' => $total_horas_disponible
                                            ,'textoAprobadas' => 'Horas Disponible'
                                            ,'rechazadas'=> $row['total_os_indisponibilidad_horas']
                                            ,'textoRechazadas'=> 'Horas Indisponible'
                                            ,'umbral' => $umbral
                                            ,'total' => $row['total_horas_periodo']);
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/disponibilidad/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLADisponibilidad($cont_id,$filtros_ini);
        
    $out = array();   
    $out['grafico'] = array();   
    $out['grafico']['titulo'] = 'Porcentaje de Disponibilidad por Especialidad';
    $out['grafico']['textoUmbral'] = " 10% de tolerancia";
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        /*$out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                            , 'valor' => $row['tasa_disponibilidad'] );
                            */
        $out['grafico']['datos'][] = array( 'nombre' => $row['espe_nombre']
                                            ,'aprobadas' => $total_horas_disponible 
                                            ,'textoAprobadas' => 'Horas Disponible'
                                            ,'rechazadas'=> $row['total_os_indisponibilidad_horas']
                                            ,'textoRechazadas'=> 'Horas Indisponible'
                                            , 'umbral' => 10
                                            ,'total' => $row['total_horas_periodo']);
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/disponibilidad/validacion(/@page:[0-9]+)', function($cont_id, $page) {

    $results_by_page = 8;
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $usua_id = $_SESSION['user_id'];
    
    $out = array();  
        
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 
    
    
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
        unset($filtros_ini['orse_fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
        unset($filtros_ini['orse_fecha_validacion_termino']);
    }    
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }    
    if( isset($filtros_ini['orse_id']) && $filtros_ini['orse_id']!="" ){
        $filtros .= " AND orse_id like '%".$filtros_ini['orse_id']."%'"; 
    }
    
    //$filtros = Flight::filtersToWhereString( array("orden_servicio", "zona"), $filtros_ini);
    
    $query = "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        sos.orse_id
                        ,sos.orse_tipo
                        ,sos.empl_nombre
                        ,sos.espe_nombre
                        ,sos.orse_descripcion
                        ,sos.info_id
                        ,sos.orse_fecha_creacion
                        ,sos.subida_servicio
                        ,IF(FLOOR(sos.sla_disponibilidad/3600)>0,FLOOR(sos.sla_disponibilidad/3600),0) AS sla_disponibilidad
                        ,sos.sla_disponibilidad_ajuste
                        ,IF(FLOOR(sos.sla_disponibilidad/3600) - sos.sla_disponibilidad_ajuste > 0,FLOOR(sos.sla_disponibilidad/3600) - sos.sla_disponibilidad_ajuste,0) AS sla
                        ,sos.sla_disponibilidad_exclusion
                        ,TRIM(sos.sla_disponibilidad_observacion) AS sla_disponibilidad_observacion
                        #para filtros
                        ,sos.regi_nombre
                        ,zona.zona_nombre
                        ,IF( ubicacion_subida.fore_ubicacion LIKE 'web','WEB', IF( ubicacion_subida.fore_ubicacion = '', 'DESCONOCIDA' ,'MOVIL'  ) ) as ubicacion

                FROM 
                        sla_orden_servicio sos
                        INNER JOIN (  
                            SELECT empl.empl_id
                            FROM 
                                rel_contrato_usuario rcu 
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                            WHERE 
                                rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                            GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sos.empl_id)                        

                        INNER JOIN rel_zona_emplazamiento rze ON sos.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)

                        LEFT JOIN (
                            SELECT * FROM (
                                    SELECT 
                                            tare_id_relacionado
                                            ,fore_ubicacion
                                    FROM
                                            tarea tare
                                            INNER JOIN rel_tarea_formulario_respuesta rtfr ON (tare.tare_id = rtfr.tare_id)
                                            INNER JOIN formulario_respuesta fr ON (fr.fore_id = rtfr.fore_id)
                                    WHERE
                                            tare_modulo = 'OS'
                                            AND tare_tipo = 'VISITAR_SITIO'
                                            AND tare_estado = 'REALIZADA'
                                            AND rtfr_fecha IS NOT NULL
                                            AND rtfr.form_id = 4
                                    ORDER BY
                                            tare_id_relacionado ASC
                                            ,rtfr.rtfr_fecha DESC
                                ) tareas
                                GROUP BY tare_id_relacionado
                        ) ubicacion_subida ON sos.orse_id=ubicacion_subida.tare_id_relacionado
                WHERE
                        sos.cont_id = $cont_id
                        AND sos.orse_indisponibilidad IN ('SI','PARCIAL')
                        AND $filtros
                GROUP BY orse_id                
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ";
    //TODO: Ver especialidades activas para filtro histórico
    
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    
//    foreach ($out['validacion'] AS &$row) {
//        if( $row['sla_tiempo_respuesta']-$row['sla_tiempo_respuesta_ajuste'] <= $row['sla'] ){
//            $row['sla_cumple'] = 'SI';
//        }
//    }


    $out['status'] = 1;
    Flight::json($out);
});


#Ejecucion______________________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/ejecucion/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLAEjecucion($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/ejecucion/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    //if( isset($filtros_ini['zona_tipo'])) unset($filtros_ini['zona_tipo']); 
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);

    $data = Flight::ObtenerSLAEjecucion($cont_id,$filtros_ini);

    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Cumplimiento del Mantenimiento por Zona';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Cumple';
    $out['grafico']['rechazadas'] = 'No Cumple';
    $out['grafico']['sindatos']='Sin MNT';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $total_mant_rechazados_ejecucion=$row['total_mant']-$row['total_mant_aprobados'];
        $out['grafico']['datos'][] = array(   'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $row['total_mant_aprobados']
                                            , 'textoAprobadas' => 'Cumple'
                                            , 'rechazadas' => $total_mant_rechazados_ejecucion
                                            , 'textoRechazadas' => 'No Cumple'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_mant']
        );
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/ejecucion/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLAEjecucion($cont_id,$filtros_ini);
        
    $out = array();   
    $out['grafico'] = array();   
    $out['grafico']['titulo'] = 'Cumplimiento del Mantenimiento por Especialidad';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Cumple';
    $out['grafico']['rechazadas'] = 'No Cumple';
    $out['grafico']['sindatos']='Sin MNT';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $total_mant_rechazados_ejecucion=$row['total_mant']-$row['total_mant_aprobados'];
        $out['grafico']['datos'][] = array(   'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $row['total_mant_aprobados']
                                            , 'rechazadas' => $total_mant_rechazados_ejecucion
                                            , 'umbral' => 10
                                            , 'total' => $row['total_mant'] 
        );
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/ejecucion/validacion(/@page:[0-9]+)', function($cont_id, $page) {

    $results_by_page = 8;//Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $usua_id = $_SESSION['user_id'];
    
    $out = array();  
    
    
//    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
//        if( is_array($filtros_ini['orse_tipo']) ){
//            $filtros .= " AND orse_tipo IN ('".implode("','",$filtros_ini['orse_tipo'])."') ";
//        }
//        else{
//            $filtros .= " AND orse_tipo = '".$filtros_ini['orse_tipo']."' ";
//        }
//    }
//    else{
//        $filtros .= " AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  ";
//    }
    
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 

    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }    
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }    
    
    if( isset($filtros_ini['mant_id']) && $filtros_ini['mant_id']!="" ){
        $filtros .= " AND mant_id like '%".$filtros_ini['mant_id']."%'"; 
    }
    //$filtros = Flight::filtersToWhereString( array("orden_servicio", "zona"), $filtros_ini);
    
    $query = "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        sla.mant_id
                        ,sla.peri_nombre
                        ,sla.espe_nombre
                        ,sla.clas_nombre
                        ,sla.mant_estado
                        ,sla.empl_nombre
                        ,sla.mant_descripcion
                        ,null AS info_id
                        ,sla.mant_fecha_validacion
                        ,sla.sla_ejecucion_exclusion
                        ,sla.sla_ejecucion_observacion
                FROM
                        sla_mantenimiento sla
                        INNER JOIN (  
                            SELECT empl.empl_id
                            FROM 
                                rel_contrato_usuario rcu 
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                            WHERE 
                                rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                            GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sla.empl_id)               
                        INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                WHERE
                        sla.cont_id = $cont_id
                        AND $filtros
                GROUP BY mant_id             
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ";
    //TODO: Ver especialidades activas para filtro histórico
    
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    
//    foreach ($out['validacion'] AS &$row) {
//        if( $row['sla_tiempo_respuesta']-$row['sla_tiempo_respuesta_ajuste'] <= $row['sla'] ){
//            $row['sla_cumple'] = 'SI';
//        }
//    }


    $out['status'] = 1;
    Flight::json($out);
});


#Cronograma_____________________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/cronograma/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLACronograma($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/cronograma/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);  
    
    $data = Flight::ObtenerSLACronograma($cont_id,$filtros_ini);

    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Cumplimiento del Conograma de mantenimientos por Zona';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total Mantenimiento Cumple';
    $out['grafico']['rechazadas'] = 'Total Mantenimiento No Cumple';
    $out['grafico']['sindatos']='Sin MNT';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $rechazadas_cronograma= $row['total_mant']-$row['total_mant_cronograma'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $row['total_mant_cronograma']
                                            , 'textoAprobadas' => 'Total Mantenimiento Cumple'
                                            , 'rechazadas' => $rechazadas_cronograma
                                            , 'textoRechazadas' => 'Total Mantenimiento No Cumple'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_mant']);
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/cronograma/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLACronograma($cont_id,$filtros_ini);
        
    $out = array();   
    $out['grafico'] = array();   
    $out['grafico']['titulo'] = 'Cumplimiento del Conograma de mantenimientos por Especialidad';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $rechazadas_cronograma= $row['total_mant']-$row['total_mant_cronograma'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['espe_nombre']
                                            , 'aprobadas' => $row['total_mant_cronograma']
                                            , 'textoAprobadas' => 'Total Mantenimiento Cumple'
                                            , 'rechazadas' => $rechazadas_cronograma
                                            , 'textoRechazadas' => 'Total Mantenimiento No Cumple'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_mant']);
    }    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/cronograma/validacion(/@page:[0-9]+)', function($cont_id, $page) {

    $results_by_page = 8;//Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $usua_id = $_SESSION['user_id'];
    $having = "";
    $out = array();   
   
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 

    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }    
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }    
    if( isset($filtros_ini['mant_id']) && $filtros_ini['mant_id']!="" ){
        $filtros .= " AND mant_id like '%".$filtros_ini['mant_id']."%'"; 
    }

    if( isset($filtros_ini['sla_cumple']) && $filtros_ini['sla_cumple']!="" ){
        $having .= " having sla_cumple like '".$filtros_ini['sla_cumple']."'"; 
    }

    $query = "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        sla.mant_id
                        ,sla.peri_nombre
                        ,sla.espe_nombre
                        ,sla.clas_nombre
                        ,sla.mant_estado
                        ,sla.empl_nombre
                        ,sla.mant_descripcion
                        ,null AS info_id
                        ,sla.mant_fecha_programada
                        ,COALESCE(sla.mant_fecha_ejecucion, sla.mant_fecha_validacion) AS mant_fecha_ejecucion
                        ,ROUND(TIMESTAMPDIFF(SECOND,COALESCE(sla.mant_fecha_ejecucion, sla.mant_fecha_validacion),sla.mant_fecha_programada)/3600,1) AS sla_cronograma_margen   
                        ,sla.sla_cronograma_ajuste
                        ,ROUND(TIMESTAMPDIFF(SECOND,COALESCE(sla.mant_fecha_ejecucion, sla.mant_fecha_validacion),sla.mant_fecha_programada)/3600,1) + sla.sla_cronograma_ajuste AS sla_cronograma_margen_ajuste
                        ,IF( sla.mant_fecha_programada >= DATE_SUB(COALESCE(sla.mant_fecha_ejecucion, sla.mant_fecha_validacion),INTERVAL sla.sla_cronograma_ajuste HOUR ), 'SI', 'NO') AS sla_cumple
                        ,sla.sla_cronograma_exclusion
                        ,sla.sla_cronograma_observacion
                FROM
                        sla_mantenimiento sla
                        INNER JOIN (  
                            SELECT empl.empl_id
                            FROM 
                                rel_contrato_usuario rcu 
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                            WHERE 
                                rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                            GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sla.empl_id)
                        INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                WHERE
                        sla.cont_id = $cont_id
                        AND $filtros
                GROUP BY mant_id             
                $having
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ";
    //TODO: Ver especialidades activas para filtro histórico
    
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }
    
//    foreach ($out['validacion'] AS &$row) {
//        if( $row['sla_tiempo_respuesta']-$row['sla_tiempo_respuesta_ajuste'] <= $row['sla'] ){
//            $row['sla_cumple'] = 'SI';
//        }
//    }


    $out['status'] = 1;
    Flight::json($out);
});


#Calidad________________________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidad/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLACalidad($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidad/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    //if( isset($filtros_ini['zona_tipo'])) unset($filtros_ini['zona_tipo']); 
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLACalidad($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Calidad Mantenimientos Zona';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total Cumple';
    $out['grafico']['rechazadas'] = 'Total No Cumple';
    $out['grafico']['sindatos']='Sin MNT';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        //$calidad_mant_aprobados=$row['total_mant_aprobados']-$row['total_mant_rechazados']-$row['total_insp_rechazadas'];
        $calidad_mant_aprobados=$row['total_mant_aprobados']-$row['total_insp_rechazadas'];
        //$calidad_mant_rechazados=$row['total_mant']-$row['total_mant_aprobados'];
        $calidad_mant_rechazados=$row['total_mant']-$row['total_mant_aprobados'];
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => $calidad_mant_aprobados
                                            , 'textoAprobadas' => 'Total Cumple'
                                            , 'rechazadas' => $calidad_mant_rechazados
                                            , 'textoRechazadas' => 'Total No Cumple'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_mant']
        );
    }  

    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidad/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLACalidad($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Calidad Mantenimientos por Especialidad';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Total Mantenimiento Cumple';
    $out['grafico']['rechazadas'] = 'Total Mantenimiento No Cumple';
    $out['grafico']['sindatos']='Sin MNT';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $out['grafico']['datos'][] = array( 'nombre' => $row['espe_nombre']
                                            , 'aprobados' => $calidad_mant_aprobados
                                            , 'textoAprobados' => 'Total Cumple'
                                            , 'rechazados' => $row['total_mant_rechazados']
                                            , 'textoRechazados' => 'Total No Cumple'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_mant']
        );
    }  
    
    $out['status'] = 1;  
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidad/validacion(/@page:[0-9]+)', function($cont_id, $page) {

    $results_by_page = 8;//Flight::get('results_by_page'); 
    $filtros_ini = array_merge($_GET,$_POST);    
    $filtros = "TRUE ";
    $usua_id = $_SESSION['user_id'];
    
    $out = array(); 
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND sla.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND sla.espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 

    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    }    
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }   
    if( isset($filtros_ini['insp_id']) && $filtros_ini['insp_id']!="" ){
        $filtros .= " AND insp_id like '%".$filtros_ini['insp_id']."%'"; 
    }
    $query = "
                SELECT 
                        SQL_CALC_FOUND_ROWS
                        slai.insp_id
                        ,slai.espe_nombre
                        ,slai.insp_estado
                        ,slai.empl_nombre
                        ,slai.empr_nombre
                        ,slai.insp_descripcion
                        ,null AS info_id
                        ,slai.insp_fecha_validacion
                        ,slai.sla_calidad_exclusion
                        ,slai.sla_calidad_observacion
                FROM
                        sla_mantenimiento sla
                        INNER JOIN (  
                           SELECT empl.empl_id
                           FROM 
                               rel_contrato_usuario rcu 
                               INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                               INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                               INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                           WHERE 
                               rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                           GROUP BY empl_id
                        ) alcance ON (alcance.empl_id = sla.empl_id)

                        INNER JOIN sla_inspeccion slai ON sla.mant_id = slai.mant_id
                        INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                        INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                WHERE
                        sla.cont_id = $cont_id
                        AND $filtros
                GROUP BY insp_id             
                ". ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ";
    //TODO: Ver especialidades activas para filtro histórico
    
    $dbo = new MySQL_Database();  
    //Flight::json(array("status" => 0, "error" => $query));
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $out['validacion'] = $res['data'];
    $out['filtros'] = $filtros_ini;
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $out['pagina'] = intval($page);
        $out['paginas'] = ceil($out['total']/$results_by_page);
    }

    $out['status'] = 1;
    Flight::json($out);
});


#Exportacion de datos___________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/os/exportar', function($cont_id) {
    $usua_id = $_SESSION['user_id'];
    $filtros_ini = array_merge($_GET, $_POST);
    $filtros = "TRUE ";
    
    if( isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!="" ){
        if( is_array($filtros_ini['orse_tipo']) ){
            $filtros .= " AND orse_tipo IN ('".implode("','",$filtros_ini['orse_tipo'])."') ";
        } else {
            $filtros .= " AND orse_tipo = '".$filtros_ini['orse_tipo']."' ";
        }
    } else {
        $filtros .= " AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  ";
    }
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        } else {
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_inicio']) && $filtros_ini['orse_fecha_validacion_inicio']!="" ){
        $filtros .= " AND orse_fecha_validacion >= '".$filtros_ini['orse_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['orse_fecha_validacion_termino']) && $filtros_ini['orse_fecha_validacion_termino']!="" ){
        $filtros .= " AND orse_fecha_validacion <= '".$filtros_ini['orse_fecha_validacion_termino']." 23:59:59' "    ; 
    }    
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) ){
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        } else {
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
        }
    } 
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!="" ){
        if( is_array($filtros_ini['regi_id']) ){
            $filtros .= " AND regi_id IN (".implode(",",$filtros_ini['regi_id']).") ";
        } else {
            $filtros .= " AND regi_id = ".$filtros_ini['regi_id']." ";
        }
    }
    
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona_tipo = '".$filtros_ini['zona_tipo']."'";
    }
    
    
    $db = new MySQL_Database();
    $res = $db->ExecuteQuery("SELECT 
                                sla.orse_id
                                ,sla.cont_id
                                ,sla.orse_tipo
                                ,sla.orse_estado
                                ,sla.orse_indisponibilidad
                                ,sla.orse_descripcion
                                ,sla.orse_fecha_creacion
                                ,sla.orse_fecha_solicitud
                                ,sla.subida_servicio
                                ,sla.pres_fecha_creacion
                                ,sla.pres_fecha_validacion
                                ,sla.info_fecha_creacion
                                ,sla.info_fecha_validacion
                                ,sla.orse_fecha_validacion
                                ,sla.info_id
                                ,sla.regi_id
                                ,sla.regi_nombre
                                ,sla.empl_id
                                ,sla.empl_nombre
                                ,sla.espe_id
                                ,sla.espe_nombre
                                ,sla.sla_tiempo_orse
                                ,sla.sla_tiempo_respuesta
                                ,sla.sla_entrega_presupuesto
                                ,sla.sla_validacion_presupuesto
                                ,sla.sla_entrega_informe
                                ,sla.sla_validacion_informe
                                ,sla.sla_solucion_final
                                ,sla.sla_tiempo_respuesta_ajuste
                                ,sla.sla_tiempo_respuesta_exclusion
                                ,sla.sla_tiempo_respuesta_observacion
                                ,sla.sla_tasa_fallas_exclusion
                                ,sla.sla_tasa_fallas_observacion
                                ,sla.sla_tasa_fallas_rei_exclusion
                                ,sla.sla_tasa_fallas_rei_observacion
                                ,sla.sla_disponibilidad
                                ,sla.sla_disponibilidad_ajuste
                                ,sla.sla_disponibilidad_exclusion
                                ,sla.sla_disponibilidad_observacion
                                ,sla.usua_creador
                                ,sla.usua_creador_nombre
                                ,sla.ultimo_jefe_cuadrilla
                                ,sla.ultimo_jefe_cuadrilla_nombre
                                ,GROUP_CONCAT(rze.zona_id) as zona_id
                                #,GROUP_CONCAT(zona.zona_nombre) as zona_nombre
                                ,IF( ubicacion_subida.fore_ubicacion LIKE 'web','WEB', IF( ubicacion_subida.fore_ubicacion = '', 'DESCONOCIDA' ,'MOVIL'  ) ) as ubicacion
                            FROM 
				sla_orden_servicio sla
                                INNER JOIN (  
                                    SELECT empl.empl_id
                                    FROM 
                                        rel_contrato_usuario rcu 
                                        INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                        INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                        INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                    WHERE 
                                        rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                    GROUP BY empl_id
                                ) alcance ON (alcance.empl_id = sla.empl_id)   
                                
				#para filtros
                                INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                                INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                                #movil o web
                                LEFT JOIN (
                                    SELECT * FROM (
                                            SELECT 
                                                    tare_id_relacionado
                                                    ,fore_ubicacion
                                            FROM
                                                    tarea tare
                                                    INNER JOIN rel_tarea_formulario_respuesta rtfr ON (tare.tare_id = rtfr.tare_id)
                                                    INNER JOIN formulario_respuesta fr ON (fr.fore_id = rtfr.fore_id)
                                            WHERE
                                                    tare_modulo = 'OS'
                                                    AND tare_tipo = 'VISITAR_SITIO'
                                                    AND tare_estado = 'REALIZADA'
                                                    AND rtfr_fecha IS NOT NULL
                                                    AND rtfr.form_id = 4
                                            ORDER BY
                                                    tare_id_relacionado ASC
                                                    ,rtfr.rtfr_fecha DESC
                                        ) tareas
                                        GROUP BY tare_id_relacionado
                            ) ubicacion_subida ON sla.orse_id=ubicacion_subida.tare_id_relacionado

                            WHERE
                                sla.cont_id = $cont_id
                                AND $filtros
                            GROUP BY sla.orse_id
                            ");

    //Flight::json($res);
    if ($res['status'] == 0) {
        echo $res['error'];
        return;
    }
    if ($res['rows'] == 0) {
        echo "No hay datos disponibles";
        return;
    }

    //Exporta a CSV
    $delimiter = ";";
    $filename = "SLA_" . date("Ymd") . ".csv";
    header('Content-Type: application/csv; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');          // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate');           // HTTP/1.1
    header('Pragma: public');

    $f = fopen('php://output', 'w');
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/mnt/exportar', function($cont_id) {
    $usua_id = $_SESSION['user_id'];
    $filtros_ini = array_merge($_GET, $_POST);
    $filtros = "TRUE ";
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }    
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    } 
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!="" ){
        if( is_array($filtros_ini['regi_id']) )
            $filtros .= " AND regi_id IN (".implode(",",$filtros_ini['regi_id']).") ";
        else
            $filtros .= " AND regi_id = ".$filtros_ini['regi_id']." ";
    }
    
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona_tipo = '".$filtros_ini['zona_tipo']."'";
    }
    
    
    $db = new MySQL_Database();
    $res = $db->ExecuteQuery("SELECT 
                                sla.mant_id
                                ,sla.cont_id
                                ,sla.peri_id
                                ,sla.peri_nombre
                                ,sla.mant_estado
                                ,sla.mant_responsable
                                ,sla.mant_descripcion
                                ,sla.mant_fecha_programada
                                ,sla.mant_fecha_ejecucion
                                ,sla.mant_fecha_validacion
                                ,sla.info_id
                                ,sla.regi_id
                                ,sla.regi_nombre
                                ,sla.empl_id
                                ,sla.empl_nombre
                                ,sla.espe_id
                                ,sla.espe_nombre
                                ,sla.clas_id
                                ,sla.clas_nombre
                                ,sla.empr_id
                                ,sla.empr_nombre
                                ,sla.usua_creador
                                ,sla.usua_creador_nombre
                                ,sla.usua_validador
                                ,sla.usua_validador_nombre
                                ,sla.sla_ejecucion_exclusion
                                ,sla.sla_ejecucion_observacion
                                ,sla.sla_cronograma_ajuste
                                ,sla.sla_cronograma_exclusion
                                ,sla.sla_cronograma_observacion
                                ,GROUP_CONCAT(rze.zona_id) as zona_id
                                #,GROUP_CONCAT(zona.zona_nombre) as zona_nombre
                            FROM 
				            sla_mantenimiento sla
                                INNER JOIN (  
                                    SELECT empl.empl_id
                                    FROM 
                                        rel_contrato_usuario rcu 
                                        INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                                        INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                                        INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                                    WHERE 
                                        rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                                    GROUP BY empl_id
                                ) alcance ON (alcance.empl_id = sla.empl_id)                                

				#para filtros
                                INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                                INNER JOIN zona ON (rze.zona_id = zona.zona_id AND zona.cont_id = $cont_id)
                            WHERE
                                sla.cont_id = $cont_id
                                AND $filtros
                            GROUP BY sla.mant_id
                            ");

    //Flight::json($res);
    if ($res['status'] == 0) {
        echo $res['error'];
        return;
    }
    if ($res['rows'] == 0) {
        echo "No hay datos disponibles";
        return;
    }

    //Exporta a CSV
    $delimiter = ";";
    $filename = "SLA_" . date("Ymd") . ".csv";
    header('Content-Type: application/csv; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');          // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate');           // HTTP/1.1
    header('Pragma: public');

    $f = fopen('php://output', 'w');
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/insp/exportar', function($cont_id) {
    $usua_id = $_SESSION['user_id'];
    $filtros_ini = array_merge($_GET, $_POST);
    $filtros = "TRUE ";
    
    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND espe_id = '".$filtros_ini['espe_id']."' ";
        }
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_inicio']) && $filtros_ini['mant_fecha_validacion_inicio']!="" ){
        $filtros .= " AND mant_fecha_validacion >= '".$filtros_ini['mant_fecha_validacion_inicio']." 00:00:00' "    ; 
    }
    
    if( isset($filtros_ini['mant_fecha_validacion_termino']) && $filtros_ini['mant_fecha_validacion_termino']!="" ){
        $filtros .= " AND mant_fecha_validacion <= '".$filtros_ini['mant_fecha_validacion_termino']." 23:59:59' "    ; 
    }    
    
    if( isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="" ){
        if( is_array($filtros_ini['zona_id']) )
            $filtros .= " AND zona.zona_id IN (".implode(",",$filtros_ini['zona_id']).") ";
        else
            $filtros .= " AND zona.zona_id = ".$filtros_ini['zona_id']." ";
    } 
    
    if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!="" ){
        if( is_array($filtros_ini['regi_id']) )
            $filtros .= " AND regi_id IN (".implode(",",$filtros_ini['regi_id']).") ";
        else
            $filtros .= " AND regi_id = ".$filtros_ini['regi_id']." ";
    }
    
    if( isset($filtros_ini['zona_tipo']) && $filtros_ini['zona_tipo']!="" && $filtros_ini['zona_tipo']!="REGIONES"){
        $filtros .= " AND zona_tipo = '".$filtros_ini['zona_tipo']."'";
    }
    
    
    $db = new MySQL_Database();
    $query = "SELECT 
                    sla.mant_id
                    ,sla.cont_id
                    ,sla.peri_id
                    ,sla.peri_nombre
                    ,sla.mant_estado
                    ,sla.mant_responsable
                    ,sla.mant_descripcion
                    ,sla.mant_fecha_programada
                    ,sla.mant_fecha_ejecucion
                    ,sla.mant_fecha_validacion
                    ,sla.info_id AS mant_info_id
                    ,sla.regi_id
                    ,sla.regi_nombre
                    ,sla.empl_id
                    ,sla.empl_nombre
                    ,sla.espe_id
                    ,sla.espe_nombre
                    ,sla.clas_id
                    ,sla.clas_nombre
                    ,sla.empr_id
                    ,sla.empr_nombre
                    ,sla.usua_creador AS mant_usua_creador
                    ,sla.usua_creador_nombre AS mant_usua_creador_nombre
                    ,sla.usua_validador AS mant_usua_validador
                    ,sla.usua_validador_nombre AS mant_usua_validador_nombre
                    ,slai.insp_id
                    ,slai.insp_estado
                    ,slai.insp_responsable
                    ,slai.insp_descripcion
                    ,slai.insp_fecha_creacion
                    ,slai.insp_fecha_solicitud
                    ,slai.insp_fecha_validacion
                    ,slai.info_id AS insp_info_id
                    ,slai.usua_creador AS insp_usua_creador
                    ,slai.usua_creador_nombre AS insp_usua_creador_nombre
                    ,slai.usua_validador AS insp_usua_validador
                    ,slai.usua_validador_nombre AS insp_usua_validador_nombre
                    ,slai.sla_calidad_exclusion
                    ,slai.sla_calidad_observacion
                    ,GROUP_CONCAT(rze.zona_id) as zona_id
                FROM 
                    sla_mantenimiento sla      
                    INNER JOIN (  
                        SELECT empl.empl_id
                        FROM 
                            rel_contrato_usuario rcu 
                            INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id) 
                            INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rcua.zona_id)
                            INNER JOIN emplazamiento empl ON (rze.empl_id = empl.empl_id)
                        WHERE 
                            rcu.usua_id = $usua_id AND rcu.cont_id = $cont_id
                        GROUP BY empl_id
                    ) alcance ON (alcance.empl_id = sla.empl_id)

                    LEFT JOIN sla_inspeccion slai ON (sla.mant_id = slai.mant_id AND slai.sla_calidad_exclusion = 0)
                    INNER JOIN rel_zona_emplazamiento rze ON sla.empl_id = rze.empl_id
                    INNER JOIN zona ON (zona.cont_id = $cont_id AND rze.zona_id = zona.zona_id AND zona.zona_estado = 'ACTIVO')
                WHERE
                    sla.cont_id = $cont_id
                    AND $filtros
                GROUP BY sla.mant_id, slai.insp_id, zona.zona_id
                ";

    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        echo $res['error'];
        return;
    }
    if ($res['rows'] == 0) {
        echo "No hay datos disponibles";
        return;
    }

    //Exporta a CSV
    $delimiter = ";";
    $filename = "SLA_" . date("Ymd") . ".csv";
    header('Content-Type: application/csv; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');          // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate');           // HTTP/1.1
    header('Pragma: public');

    $f = fopen('php://output', 'w');
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
});


#Evaluacion_____________________________________________________________________

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/evaluacion/*', function($cont_id){
    if( !isset(Flight::get('SLA_PARAMETROS')[$cont_id]) ){
        Flight::json(array("status" => 0, "error" => "No se han definido los parametros para calcular este SLA en este contrato")) ;
    }
    return true;
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/evaluacion/detalle', function($cont_id) {

    //Parseo de filtros
    $filtros_ini = array_merge($_GET, $_POST);
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    if( isset($filtros_ini['fecha_validacion_inicio']) && $filtros_ini['fecha_validacion_inicio']!="" ){
        $filtros_ini['orse_fecha_validacion_inicio'] = $filtros_ini['fecha_validacion_inicio'];
        $filtros_ini['mant_fecha_validacion_inicio'] = $filtros_ini['fecha_validacion_inicio'];
        $filtros_ini['insp_fecha_validacion_inicio'] = $filtros_ini['fecha_validacion_inicio'];
        unset($filtros_ini['fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['fecha_validacion_termino']) && $filtros_ini['fecha_validacion_termino']!="" ){
        $filtros_ini['orse_fecha_validacion_termino'] = $filtros_ini['fecha_validacion_termino'];
        $filtros_ini['mant_fecha_validacion_termino'] = $filtros_ini['fecha_validacion_termino'];
        $filtros_ini['insp_fecha_validacion_termino'] = $filtros_ini['fecha_validacion_termino'];
        unset($filtros_ini['fecha_validacion_termino']);
    } 
        
    if( !isset($filtros_ini['espe_id']) ){
        $filtros_ini['espe_id'] = "";
    }

    if (isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);
    if (isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);

    
    $data = array();
    $data['cronograma']  = Flight::ObtenerSLACronograma($cont_id, $filtros_ini);
    $data['calidad_mpp'] = Flight::ObtenerSLACalidad($cont_id, $filtros_ini);
	$filtros_ini['clas_nombre'] = ".*[A][ABCD]+";	
    $data['ejecucion_aa_ab_ac_ad'] = Flight::ObtenerSLAEjecucion($cont_id, $filtros_ini);
    $filtros_ini['clas_nombre'] = ".*[B][ABCD]+";
	$data['ejecucion_ba_bb_bc_bd'] = Flight::ObtenerSLAEjecucion($cont_id, $filtros_ini);
    $data['tiempos_osu'] = Flight::ObtenerSLATiempos($cont_id,$filtros_ini);
    $data['tiempos_osn'] = $data['tiempos_osu'];
                
    $slas= array( 
                array( "nombre" => "cronograma", "tasa" => "tasa_cronograma", "seccion" => "ejecucion_mpp", "subseccion" => "cronograma_mpp"  )
                ,array( "nombre" => "calidad_mpp", "tasa" => "tasa_calidad", "seccion" => "calidad", "subseccion" => "auditoria_mpp"  )
                ,array( "nombre" => "ejecucion_aa_ab_ac_ad", "tasa" => "tasa_ejecucion", "seccion" => "ejecucion_mpp", "subseccion" => "aa_ab_ac_ad"  )
                ,array( "nombre" => "ejecucion_ba_bb_bc_bd", "tasa" => "tasa_ejecucion", "seccion" => "ejecucion_mpp", "subseccion" => "ba_bb_bc_bd"  )
                ,array( "nombre" => "tiempos_osu", "tasa" => "porcentaje_osgu_oseu_sla", "seccion" => "sla", "subseccion" => "osu"  )
                ,array( "nombre" => "tiempos_osn", "tasa" => "porcentaje_osgn_osen_sla", "seccion" => "sla", "subseccion" => "osn"  )
                
            );
			
			
			
			//$res = $dbo->ExecuteQuery($query);

    $escala_notas = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_NOTAS'];
    $pesos_notas  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_PESOS_NOTAS'];
    $calificaciones  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_CALIFICACION'];
    
    //Hacemos una unión de las {zona,especialidad}   
    //$zona_espe = array();    
    foreach( $slas as $sla  ){
        foreach( $data[$sla['nombre']] as $row ){
            $zona_espe[] =  array('zona_id' => $row['zona_id'], 'espe_id' => $row['espe_id']);       
        } 
    }       
    //Ordenamos las {zona,especialidad}  
    $zona_espe = array_unique($zona_espe, SORT_REGULAR);
    $zonas = array();
    $espes = array();
    foreach ($zona_espe as $key => $row) {
        $zonas[$key]  = $row['zona_id'];
        $espes[$key] = $row['espe_id'];
    }
    array_multisort($zonas, SORT_ASC, $espes, SORT_ASC, $zona_espe);
        
    //Calculamos las notas
    $notas = array();
    foreach( $zona_espe as $ids ){
        $dato = array(
            'zona_id' => $ids['zona_id']
            ,'espe_id' => $ids['espe_id']
            ,'zona_nombre' => ''
            ,'espe_nombre' => ''
            ,'tasa_cronograma' => ''
            ,'tasa_calidad_mpp' => ''
            ,'tasa_ejecucion_aa_ab_ac_ad' => ''
            ,'tasa_ejecucion_ba_bb_bc_bd' => ''
            ,'tasa_tiempos_osu' => ''
            ,'tasa_tiempos_osn' => ''
            ,'tasa_calidad_os' => ''
            ,'tasa_calidad_pprr' => ''
            ,'tasa_personal' => ''  
            ,'tasa_presentacion' => ''
            
            ,'nota_cronograma' => '(10)'
            ,'nota_calidad_mpp' => '(10)'
            ,'nota_ejecucion_aa_ab_ac_ad' => '(10)'
            ,'nota_ejecucion_ba_bb_bc_bd' => '(10)'
            ,'nota_tiempos_osu' => '(10)'
            ,'nota_tiempos_osn' => '(10)'
            ,'nota_calidad_os' => '(10)'
            ,'nota_calidad_pprr' => '(10)'
            ,'nota_personal' => '(10)'  
            ,'nota_presentacion' => '(10)'
        );
        
        foreach( $slas as $sla  ){
            foreach( $data[$sla['nombre']] as $row ) {
                if ($ids['zona_id'] == $row['zona_id'] && $ids['espe_id'] == $row['espe_id']) {
                    $dato['tasa_'.$sla['nombre']] = $row[$sla['tasa']];
                    $dato['nota_'.$sla['nombre']] = Flight::CalcularNotaSeccion($sla['seccion'],$sla['subseccion'],$escala_notas,$row[$sla['tasa']]);
                    $dato['zona_nombre'] = $row['zona_nombre'];
                    $dato['espe_nombre'] = $row['espe_nombre'];
                } 
            }
        }
        
        $dato['nota_final'] = Flight::CalcularNotaFinal($pesos_notas,$dato);
        $dato['calificacion'] = Flight::ObtenerCalificacion($calificaciones,$dato['nota_final']);
        $notas[] = $dato;
    }
    
    $out = array();
    $out['status'] = array("status" => 1);  
    
    $out['cronograma']             = $data['cronograma'];
    $out['calidad_mpp']            = $data['calidad_mpp'];
    $out['ejecucion_aa_ab_ac_ad']  = $data['ejecucion_aa_ab_ac_ad'];
    $out['ejecucion_ba_bb_bc_bd']  = $data['ejecucion_ba_bb_bc_bd'];
    $out['tiempos']                = $data['tiempos_osu'];
        
    $out['escala_notas']    = $escala_notas;  
    $out['pesos_notas']     = $pesos_notas;
    $out['calificaciones']  = $calificaciones;
    $out['notas']           = $notas;
    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/evaluacion/escalas', function($cont_id) {

    $out = array();
    $out['status'] = array("status" => 1);
    $out['escala_notas'] = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_NOTAS'];
    $out['pesos_notas']  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_PESOS_NOTAS'];
    $out['calificaciones']  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_CALIFICACION'];
    Flight::json($out);  
});



Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/evaluacion/grafico_zonas', function($cont_id) {
//Parseo de filtros
    $filtros_ini = array_merge($_GET, $_POST);
    
    if( isset($filtros_ini['fecha_validacion_inicio']) && $filtros_ini['fecha_validacion_inicio']!="" ){
        $filtros_ini['orse_fecha_validacion_inicio'] = $filtros_ini['fecha_validacion_inicio'];
        $filtros_ini['mant_fecha_validacion_inicio'] = $filtros_ini['fecha_validacion_inicio'];
        $filtros_ini['insp_fecha_validacion_inicio'] = $filtros_ini['fecha_validacion_inicio'];
        unset($filtros_ini['fecha_validacion_inicio']);
    }
    
    if( isset($filtros_ini['fecha_validacion_termino']) && $filtros_ini['fecha_validacion_termino']!="" ){
        $filtros_ini['orse_fecha_validacion_termino'] = $filtros_ini['fecha_validacion_termino'];
        $filtros_ini['mant_fecha_validacion_termino'] = $filtros_ini['fecha_validacion_termino'];
        $filtros_ini['insp_fecha_validacion_termino'] = $filtros_ini['fecha_validacion_termino'];
        unset($filtros_ini['fecha_validacion_termino']);
    } 
        
    if( !isset($filtros_ini['espe_id']) ){
        $filtros_ini['espe_id'] = "";
    }

    if (isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']);
    if (isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);

    
    $data = array();
    $data['cronograma']  = Flight::ObtenerSLACronograma($cont_id, $filtros_ini);
    $data['calidad_mpp'] = Flight::ObtenerSLACalidad($cont_id, $filtros_ini);   
    /*$data['ejecucion_aa_ab_ac_ad'] = Flight::ObtenerSLAEjecucion($cont_id, array_merge($filtros_ini, array('clas_nombre' => '.*[A][ABCD]+')));
    $data['ejecucion_ba_bb_bc_bd'] = Flight::ObtenerSLAEjecucion($cont_id, array_merge($filtros_ini, array('clas_nombre' => '.*[B][ABCD]+')));
    */
    $filtros_ini['clas_nombre'] = ".*[A][ABCD]+";   
    $data['ejecucion_aa_ab_ac_ad'] = Flight::ObtenerSLAEjecucion($cont_id, $filtros_ini);
    $filtros_ini['clas_nombre'] = ".*[B][ABCD]+";
    $data['ejecucion_ba_bb_bc_bd'] = Flight::ObtenerSLAEjecucion($cont_id, $filtros_ini);
    $data['tiempos_osu'] = Flight::ObtenerSLATiempos($cont_id,$filtros_ini);
    $data['tiempos_osn'] = $data['tiempos_osu'];
                
    $slas= array( 
                array( "nombre" => "cronograma", "tasa" => "tasa_cronograma", "seccion" => "ejecucion_mpp", "subseccion" => "cronograma_mpp"  )
                ,array( "nombre" => "calidad_mpp", "tasa" => "tasa_calidad", "seccion" => "calidad", "subseccion" => "auditoria_mpp"  )
                ,array( "nombre" => "ejecucion_aa_ab_ac_ad", "tasa" => "tasa_ejecucion", "seccion" => "ejecucion_mpp", "subseccion" => "aa_ab_ac_ad"  )
                ,array( "nombre" => "ejecucion_ba_bb_bc_bd", "tasa" => "tasa_ejecucion", "seccion" => "ejecucion_mpp", "subseccion" => "ba_bb_bc_bd"  )
                ,array( "nombre" => "tiempos_osu", "tasa" => "porcentaje_osgu_oseu_sla", "seccion" => "sla", "subseccion" => "osu"  )
                ,array( "nombre" => "tiempos_osn", "tasa" => "porcentaje_osgn_osen_sla", "seccion" => "sla", "subseccion" => "osn"  )
                
            );
    
    $escala_notas = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_NOTAS'];
    $pesos_notas  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_PESOS_NOTAS'];
    $calificaciones  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_CALIFICACION'];
    
    //Hacemos una unión de las {zona,especialidad}   
    $zona_espe = array();    
    foreach( $slas as $sla  ){
        foreach( $data[$sla['nombre']] as $row ){
            $zona_espe[] =  array('zona_id' => $row['zona_id'], 'espe_id' => $row['espe_id']);       
        } 
    }       
    //Ordenamos las {zona,especialidad}  
    $zona_espe = array_unique($zona_espe, SORT_REGULAR);
    $zonas = array();
    $espes = array();
    foreach ($zona_espe as $key => $row) {
        $zonas[$key]  = $row['zona_id'];
        $espes[$key] = $row['espe_id'];
    }
    array_multisort($zonas, SORT_ASC, $espes, SORT_ASC, $zona_espe);
        
    //Calculamos las notas
    $notas = array();
    foreach( $zona_espe as $ids ){
        $dato = array(
            'zona_id' => $ids['zona_id']
            ,'espe_id' => $ids['espe_id']
            ,'zona_nombre' => ''
            ,'espe_nombre' => ''
            ,'tasa_cronograma' => ''
            ,'tasa_calidad_mpp' => ''
            ,'tasa_ejecucion_aa_ab_ac_ad' => ''
            ,'tasa_ejecucion_ba_bb_bc_bd' => ''
            ,'tasa_tiempos_osu' => ''
            ,'tasa_tiempos_osn' => ''
            ,'tasa_calidad_os' => ''
            ,'tasa_calidad_pprr' => ''
            ,'tasa_personal' => ''  
            ,'tasa_presentacion' => ''
            
            ,'nota_cronograma' => '(10)'
            ,'nota_calidad_mpp' => '(10)'
            ,'nota_ejecucion_aa_ab_ac_ad' => '(10)'
            ,'nota_ejecucion_ba_bb_bc_bd' => '(10)'
            ,'nota_tiempos_osu' => '(10)'
            ,'nota_tiempos_osn' => '(10)'
            ,'nota_calidad_os' => '(10)'
            ,'nota_calidad_pprr' => '(10)'
            ,'nota_personal' => '(10)'  
            ,'nota_presentacion' => '(10)'
        );
        
        foreach( $slas as $sla  ){
            foreach( $data[$sla['nombre']] as $row ) {
                if ($ids['zona_id'] == $row['zona_id'] && $ids['espe_id'] == $row['espe_id']) {
                    $dato['tasa_'.$sla['nombre']] = $row[$sla['tasa']];
                    $dato['nota_'.$sla['nombre']] = Flight::CalcularNotaSeccion($sla['seccion'],$sla['subseccion'],$escala_notas,$row[$sla['tasa']]);
                    $dato['zona_nombre'] = $row['zona_nombre'];
                    $dato['espe_nombre'] = $row['espe_nombre'];
                } 
            }
        }
        
        $dato['nota_final'] = Flight::CalcularNotaFinal($pesos_notas,$dato);
        $dato['calificacion'] = Flight::ObtenerCalificacion($calificaciones,$dato['nota_final']);
        $notas[] = $dato;
    }
    
    $out = array();
    $out['status'] = array("status" => 1);  
    
    $out['cronograma']             = $data['cronograma'];
    $out['calidad_mpp']            = $data['calidad_mpp'];
    $out['ejecucion_aa_ab_ac_ad']  = $data['ejecucion_aa_ab_ac_ad'];
    $out['ejecucion_ba_bb_bc_bd']  = $data['ejecucion_ba_bb_bc_bd'];
    $out['tiempos']                = $data['tiempos_osu'];
        
    $out['escala_notas']    = $escala_notas;  
    $out['pesos_notas']     = $pesos_notas;
    $out['calificaciones']  = $calificaciones;
    $out['notas']           = $notas;
/*
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Fallas por Zona';
    $out['grafico']['aprobadas'] = 'Total Equipos sin fallas';
    $out['grafico']['rechazadas'] = 'Total Fallas';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $out['grafico']['datos'][] = array( 'nombre' => $out['zona_nombre']
                                            , 'aprobadas' => null
                                            , 'rechazadas' => null
                                            , 'total' => $row['nota_final']);    
    }  
*/
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Evaluación por Zona';
    $out['grafico']['aprobadas'] = 'Total Equipos sin fallas';
    $out['grafico']['rechazadas'] = 'Total Fallas';
    $out['grafico']['sindatos']='Sin Datos';
    //$out['grafico']['datos'] = $notas;
    foreach ($notas AS $row) {
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => null
                                            , 'rechazadas' => null
                                            , 'total' => $row['nota_final']);    
    }
    $out['status'] = 1;
    Flight::json($out);
});






Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidadycronograma/detalle', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $out = array();
    $out['status'] = 1;  
    $out['detalle'] = Flight::ObtenerSLACalidadyCronograma($cont_id,$filtros_ini);    
    Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidadycronograma/grafico_zonas', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    //if( isset($filtros_ini['zona_tipo'])) unset($filtros_ini['zona_tipo']); 
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLACalidadyCronograma($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Cumplimiento del Conograma de mantenimientos por Zona';
    $out['grafico']['textoUmbral'] = ' 10% de tolerancia';
    $out['grafico']['aprobadas'] = 'Cumple';
    $out['grafico']['rechazadas'] = 'No Cumple';
    $out['grafico']['sindatos']='Sin MNT';
    $out['grafico']['total'] = 'Cumplimiento';
    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $total= round(round(($row['total_mant_rechazados']+$row['total_mant_no_cumple'])/$row['total_mant'],4),4);
        $out['grafico']['datos'][] = array( 'nombre' => $row['zona_nombre']
                                            , 'aprobadas' => null
                                            , 'rechazadas' => null
                                            , 'nRechazadas' => $row['total_mant_rechazados']
                                            , 'umbral' => 10
                                            , 'total' => $total
                                            , 'nTotal' => $row['total_mant']
                                            );
    }      

    $out['status'] = 1;      
    Flight::json($out);
});
/*
Flight::route('GET|POST /contrato/@cont_id:[0-9]+/sla/calidadycronograma/grafico_especialidades', function($cont_id) {
    
    $filtros_ini = array_merge($_GET,$_POST);  
    if( isset($filtros_ini['regi_id'])) unset($filtros_ini['regi_id']);
    if( isset($filtros_ini['zona_id'])) unset($filtros_ini['zona_id']); 
    
    $data = Flight::ObtenerSLACalidadyCronograma($cont_id,$filtros_ini);
    
    $out = array();   
    $out['grafico'] = array();  
    $out['grafico']['titulo'] = 'Porcentaje de Calidad Mantenimientos por Especialidad';
    $out['grafico']['aprobadas'] = 'Total Mantenimiento Cumple';
    $out['grafico']['rechazadas'] = 'Total Mantenimiento No Cumple';
    $out['grafico']['sindatos']='Sin MNT';

    $out['grafico']['datos'] = array();
    foreach ($data AS $row) {
        $out['grafico']['datos'][] = array( 'nombre' => $row['espe_nombre']
                                            , 'aprobados' => $calidad_mant_aprobados
                                            , 'textoAprobados' => 'Total Mantenimiento Cumple'
                                            , 'rechazados' => $row['total_mant_rechazados']
                                            , 'textoRechazados' => 'Total Mantenimiento No Cumple'
                                            , 'umbral' => 90
                                            , 'total' => $row['total_mant']
        );
    }  
    
    $out['status'] = 1;  
    
    Flight::json($out);
});

*/
Flight::map('SlaCalcularNotas', function($cont_id, $data) {
               
    $escala_notas = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_ESCALA_NOTAS'];
    $pesos_notas  = Flight::get('SLA_PARAMETROS')[$cont_id]['SLA_EVALUACION_PESOS_NOTAS'];
    
    $slas= array( 
                array( "nombre" => "cronograma", "tasa" => "tasa_cronograma", "seccion" => "ejecucion_mpp", "subseccion" => "cronograma_mpp"  )
                ,array( "nombre" => "calidad_mpp", "tasa" => "tasa_calidad", "seccion" => "calidad", "subseccion" => "auditoria_mpp"  )
                ,array( "nombre" => "ejecucion_aa_ab_ac_ad", "tasa" => "tasa_ejecucion", "seccion" => "ejecucion_mpp", "subseccion" => "aa_ab_ac_ad"  )
                ,array( "nombre" => "ejecucion_ba_bb_bc_bd", "tasa" => "tasa_ejecucion", "seccion" => "ejecucion_mpp", "subseccion" => "ba_bb_bc_bd"  )
                ,array( "nombre" => "tiempos_osu", "tasa" => "porcentaje_osgu_oseu_sla", "seccion" => "sla", "subseccion" => "osu"  )
                ,array( "nombre" => "tiempos_osn", "tasa" => "porcentaje_osgn_osen_sla", "seccion" => "sla", "subseccion" => "osn"  )
                
            );
        
    //Hacemos una unión de las {zona,especialidad}   
    $zona_espe = array();    
    foreach( $slas as $sla  ){
        foreach( $data[$sla['nombre']] as $row ){
            $zona_espe[] =  array('zona_id' => $row['zona_id'], 'espe_id' => $row['espe_id']);       
        } 
    }        
    
    //Ordenamos las {zona,especialidad}  
    $zona_espe = array_unique($zona_espe, SORT_REGULAR);
    $zonas = array();
    $espes = array();
    foreach ($zona_espe as $key => $row) {
        $zonas[$key]  = $row['zona_id'];
        $espes[$key] = $row['espe_id'];
    }
    array_multisort($zonas, SORT_ASC, $espes, SORT_ASC, $zona_espe);
    
    
    $datos = array();
    foreach( $zona_espe as $ids ){
        $dato = array(
            'zona_id' => $ids['zona_id']
            ,'espe_id' => $ids['espe_id']
            ,'zona_nombre' => ''
            ,'espe_nombre' => ''
            ,'tasa_cronograma' => ''
            ,'tasa_calidad_mpp' => ''
            ,'tasa_ejecucion_aa_ab_ac_ad' => ''
            ,'tasa_ejecucion_ba_bb_bc_bd' => ''
            ,'tasa_tiempos_osu' => ''
            ,'tasa_tiempos_osn' => ''
            ,'tasa_calidad_os' => ''
            ,'tasa_calidad_pprr' => ''
            ,'tasa_personal' => ''  
            ,'tasa_presentacion' => ''
            
            ,'nota_cronograma' => '(10)'
            ,'nota_calidad_mpp' => '(10)'
            ,'nota_ejecucion_aa_ab_ac_ad' => '(10)'
            ,'nota_ejecucion_ba_bb_bc_bd' => '(10)'
            ,'nota_tiempos_osu' => '(10)'
            ,'nota_tiempos_osn' => '(10)'
            ,'nota_calidad_os' => '(10)'
            ,'nota_calidad_pprr' => '(10)'
            ,'nota_personal' => '(10)'  
            ,'nota_presentacion' => '(10)'
        );
        
        foreach( $slas as $sla  ){
            foreach( $data[$sla['nombre']] as $row ) {
                if ($ids['zona_id'] == $row['zona_id'] && $ids['espe_id'] == $row['espe_id']) {
                    $dato['tasa_'.$sla['nombre']] = $row[$sla['tasa']];
                    $dato['nota_'.$sla['nombre']] = Flight::CalcularNotaSeccion($sla['seccion'],$sla['subseccion'],$escala_notas,$row[$sla['tasa']]);
                    $dato['zona_nombre'] = $row['zona_nombre'];
                    $dato['espe_nombre'] = $row['espe_nombre'];
                } 
            }
        }
        
        $dato['nota_final'] = Flight::CalcularNotaFinal($pesos_notas,$dato);
        /*
        foreach( $data['cronograma'] as $row ) {
            if ($ids['zona_id'] == $row['zona_id'] && $ids['espe_id'] == $row['espe_id']) {
                $dato['tasa_cronograma'] = $row['tasa_cronograma'];
                $dato['nota_cronograma'] = Flight::CalcularNotaSeccion("ejecucion_mpp","cronograma_mpp",$escala_notas,$row['tasa_cronograma']);
                $dato['zona_nombre'] = $row['zona_nombre'];
                $dato['espe_nombre'] = $row['espe_nombre'];
            } 
        }
        foreach( $data['calidad'] as $row ) {
            if ($ids['zona_id'] == $row['zona_id'] && $ids['espe_id'] == $row['espe_id']) {
                $dato['tasa_calidad'] = $row['tasa_calidad'];
                $dato['nota_calidad'] = Flight::CalcularNotaSeccion("calidad","auditoria_mpp",$escala_notas,$row['tasa_calidad']);
                $dato['zona_nombre'] = $row['zona_nombre'];
                $dato['espe_nombre'] = $row['espe_nombre'];
            } 
        }*/    
                
        $datos[] = $dato;
    }

    return $datos;
});


Flight::map('CalcularNotaSeccion',function( $seccion, $subseccion, $escala_notas, $tasa ){
    foreach( $escala_notas as $row ){
        foreach( $row as $item ){
            if( $item['nombre'] == $seccion ){
                foreach( $item['item'] as $subitem ){
                    if( $subitem['nombre'] == $subseccion 
                        && $tasa <= $subitem['tasa_max'] 
                        && $tasa >= $subitem['tasa_min']  ){
                        return $item['nota'];
                    }
                }           
            }        
        }
    }
    return 0;
});


Flight::map('CalcularNotaFinal',function( $pesos_notas, $notas ){
    
    $nota_ejecucion_mpp = parseIfString($notas['nota_ejecucion_aa_ab_ac_ad'])*Flight::ObtenerPonderacion($pesos_notas,'ejecucion_mpp','aa_ab_ac_ad');
    $nota_ejecucion_mpp += parseIfString($notas['nota_ejecucion_ba_bb_bc_bd'])*Flight::ObtenerPonderacion($pesos_notas,'ejecucion_mpp','ba_bb_bc_bd');
    $nota_ejecucion_mpp += parseIfString($notas['nota_cronograma'])*Flight::ObtenerPonderacion($pesos_notas,'ejecucion_mpp','cronograma_mpp');
    $nota_ejecucion_mpp *= Flight::ObtenerPonderacion($pesos_notas,'ejecucion_mpp');
    
    $nota_cumplimiento_sla = parseIfString($notas['nota_tiempos_osu'])*Flight::ObtenerPonderacion($pesos_notas,'sla','osu');
    $nota_cumplimiento_sla += parseIfString($notas['nota_tiempos_osn'])*Flight::ObtenerPonderacion($pesos_notas,'sla','osn');
    $nota_cumplimiento_sla *= Flight::ObtenerPonderacion($pesos_notas,'sla');
    
    $nota_calidad = parseIfString($notas['nota_calidad_mpp'])*Flight::ObtenerPonderacion($pesos_notas,'calidad','auditoria_mpp');
    $nota_calidad += 10*Flight::ObtenerPonderacion($pesos_notas,'calidad','auditoria_os');
    $nota_calidad *= Flight::ObtenerPonderacion($pesos_notas,'calidad');
    
    $nota_pprr_imagen = 10*Flight::ObtenerPonderacion($pesos_notas,'pprr_imagen');
    
    return $nota_ejecucion_mpp + $nota_cumplimiento_sla + $nota_calidad + $nota_pprr_imagen;
    
});

function parseIfString($nota){
    return( is_string($nota) )?(float)str_replace(')','',str_replace('(','',$nota)):$nota;
}

Flight::map('ObtenerPonderacion',function( $pesos_notas, $seccion, $subseccion = null ){
    foreach( $pesos_notas as $item ){
        if( $item['nombre'] == $seccion ){
            if( $subseccion==null ){
                return $item['peso_espe']/100.0;          
            }
            foreach( $item['item'] as $subitem ){
                if( $subitem['nombre'] == $subseccion ){
                    return $subitem['peso_espe']/100.0;
                }
            }           
        }        
    }
    return 0;
});

Flight::map('ObtenerCalificacion',function( $calificaciones, $nota ){
    foreach( $calificaciones as $calificacion ){
        if( $calificacion['nota_min'] <= $nota && $nota <= $calificacion['nota_max'] ){
            return $calificacion['calificacion'];          
        }        
    }
    return '???';
});

?>
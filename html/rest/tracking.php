<?php

Flight::route('GET /contrato/@id:[0-9]+/tracking/filtros', function($cont_id){ 

    $out = array();
    $dbo = new MySQL_Database();
    $filtro_empresa = "";   
    
    if(!in_array("SYS_ADMIN", $_SESSION['usua_cargo'])) {
        $filtro_empresa = "WHERE usuario.empr_id=".$_SESSION['empr_id'];
    }
    $res = $dbo->ExecuteQuery("SELECT DISTINCT 
                                usuario.usua_id, 
                                usuario.usua_nombre 
                                FROM 
                                usuario 
                                INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.usua_id=usuario.usua_id AND rel_contrato_usuario.cont_id=$cont_id) 
                                INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id) 
                                INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))

                                ".$filtro_empresa. " ORDER BY usuario.usua_nombre"
    ); 
    
    if ($res['status'] == 0){ 
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros")); 
    } 

    $out['usuarios'] = $res['data']; 
    $out['status'] = 1; 
    Flight::json($out); 
}); 

/*
Flight::route('GET /contrato/@id:[0-9]+/tracking/filtros', function($cont_id)
{    
    $out = array();
    $dbo = new MySQL_Database();

    $filtro_empresa = "";
    if(!in_array("SYS_ADMIN", $_SESSION['usua_cargo'])) {
        $filtro_empresa = "WHERE usuario.empr_id=".$_SESSION['empr_id'];
    }

    $res = $dbo->ExecuteQuery("SELECT
                                 usuario.usua_id,
                                 usuario.usua_nombre
                              FROM 
                                usuario
                              INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.usua_id=usuario.usua_id AND rel_contrato_usuario.cont_id=$cont_id)
                              INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                              INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('JEFE_CUADRILLA'))
                              ".$filtro_empresa);
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }
    $out['usuarios'] = $res['data'];


    $out['status'] = 1;
    Flight::json($out);
});
*/
/*
Flight::route('GET /contrato/@id:[0-9]+/tracking/data', function($cont_id)
{ 
    $db = new MySQL_Database();

    $usua_id        = mysql_real_escape_string($_GET['ustr_usuario']);
    $fecha_inicio   = mysql_real_escape_string($_GET['ustr_fecha_inicio']);
    $fecha_fin      = mysql_real_escape_string($_GET['ustr_fecha_fin']);

    $res = $db->ExecuteQuery("SELECT 
                                    usuario.usua_id AS user_id,
                                    usua_nombre AS user_name,
                                    ustr_latitud AS usta_latitude,
                                    ustr_longitud AS usta_longitude,
                                    ustr_fecha AS usta_datetime
                                FROM
                                usuario
                                INNER JOIN usuario_trackings ON (usuario.usua_id=usuario_trackings.usua_id)
                                WHERE 
                                usuario.usua_id LIKE '$usua_id' AND
                                ustr_fecha BETWEEN '$fecha_inicio 00:00:00' AND '$fecha_fin 23:59:59'
                                ORDER BY usuario.usua_id,ustr_fecha ASC");
    Flight::json($res);
});
*/
Flight::route('GET /contrato/@id:[0-9]+/tracking/data', function($cont_id) { 
    $db = new MySQL_Database(); 

    $usua_id = mysql_real_escape_string($_GET['ustr_usuario']); 
    $fecha_inicio = mysql_real_escape_string($_GET['ustr_fecha_inicio']); 
    $fecha_fin = mysql_real_escape_string($_GET['ustr_fecha_fin']); 

    if ($usua_id==0) { 
        Flight::json(array("status"=>1, "error"=>"El usuario seleccionado no posee datos entre fechas seleccionadas."));
        //return false; 
    } 

    $res = $db->ExecuteQuery("  SELECT 
                                    u.usua_id AS user_id, 
                                    u.usua_nombre AS user_name, 
                                    t.ustr_latitud AS usta_latitude, 
                                    t.ustr_longitud AS usta_longitude, 
                                    t.ustr_fecha AS usta_datetime 
                                FROM 
                                    usuario u 
                                INNER JOIN rel_contrato_usuario r ON r.cont_id= $cont_id AND r.usua_id = u.usua_id 
                                INNER JOIN usuario_trackings t ON (u.usua_id=t.usua_id) 
                                WHERE u.usua_id = $usua_id 
                                    AND t.ustr_fecha BETWEEN '$fecha_inicio 00:00:00' AND '$fecha_fin 23:59:59'
                                ORDER BY u.usua_id, t.ustr_fecha ASC 
    ");
    Flight::json($res); 
}); 


?>

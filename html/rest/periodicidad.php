<?php
Flight::route('GET|POST /core/periodicidad/contrato/@cont_id:[0-9]+/list(/@page:[0-9]+)', function($cont_id,$page){    
    
    $out = array();
    $dbo = new MySQL_Database();
    $results_by_page = Flight::get('results_by_page');

    $res=$dbo->ExecuteQuery("SELECT peri.peri_id
                                    ,peri.peri_nombre
                                    ,peri.peri_meses
                                    ,peri.peri_orden
                                    ,rcp.rcpe_dias_apertura 
                                    ,rcp.rcpe_dias_post_cierre
                                    ,rcp.rcpe_participa_mpp 
                            FROM periodicidad peri
                                ,rel_contrato_periodicidad rcp
                            WHERE rcp.peri_id=peri.peri_id
                            AND rcp.cont_id=$cont_id "

                            .((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page)));
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));

    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }
    Flight::json($res);
});

Flight::route('GET|POST /core/periodicidad/contrato', function(){    
    
    $out = array();
    $dbo = new MySQL_Database();
    
    $res=$dbo->ExecuteQuery("SELECT peri.peri_id
                                    ,peri.peri_nombre
                                    ,peri.peri_meses
                                    ,peri.peri_orden 
                            FROM periodicidad peri
    ");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    //$out['periodicidad']=$res['data'];

    $res['status'] = 1;
    Flight::json($res);
});

Flight::route('GET|POST /core/periodicidad/filtros', function(){    
    
    $out = array();
    $dbo = new MySQL_Database();
    
 
    $out['status'] = 1;
    Flight::json($out);
});

//rest/core/periodicidad/add'


Flight::route('GET|POST /core/periodicidad/add', function(){    
    /*EMB esta logica se comenta ya que esta mala por completo debiera apuntar a 
	tablas de periodicidad con datos de periodicidad*/
    $out = array();
    $dbo = new MySQL_Database();
    $inve_elemento=$data;
    // $query = "INSERT INTO periodicidad ".Flight::dataToInsertString($inve_elemento);
     Flight::json(array("status" => 0, "error" => $query));return;
    if($res['status'] == 0) {$dbo->Rollback();Flight::json(array("status" => 0, "error" => $res['error']));return;}
 
    $out['status'] = 1;
    Flight::json($out);
});
?>
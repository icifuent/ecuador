<?php


//____________________________________________________________
//Bandeja
Flight::set('flight.log_errors', true);

Flight::route('GET /contrato/@cont_id:[0-9]+/dashboard/os/filtros', function($cont_id){
    $out = array();
    $out['status'] = 1;    
	$out['cont_id'] = $cont_id; 
    Flight::json($out);
});


Flight::route('GET|POST /contrato/@id:[0-9]+/dashboard/os', function($cont_id){

    $usua_id = $_SESSION['user_id']; 
    $out     = array();

    if( !isset($_POST['orse_fecha_inicio']) || !isset($_POST['orse_fecha_termino']) ){
        Flight::json(array("status" => 0, "error" => "Debe seleccionar una fecha o mes de analisis"));
        return;
    }
    
    $dbo = new MySQL_Database();
    
    $orse_fecha_inicio  = "'".mysql_real_escape_string($_POST['orse_fecha_inicio'])."'";
    $orse_fecha_termino = "'".mysql_real_escape_string($_POST['orse_fecha_termino'])."'";

    /*Obtener zona de alcance de usuario*/
    $res = $dbo->ExecuteQuery("SELECT 
                                    z.zona_id
                                    , z.zona_nombre 
                                FROM rel_contrato_usuario rcu
                                INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id)
                                INNER JOIN zona z ON (z.zona_tipo= 'ALCANCE' AND z.zona_estado='ACTIVO' AND z.zona_id = rcua.zona_id 
                                AND z.cont_id =rcu.cont_id)
                                WHERE rcu.usua_id=$usua_id AND rcu.cont_id=$cont_id
                                order by z.zona_id");
    if ($res['status'] == 0){
    	Flight::json(array("status" => 0, "error" => $res['error']));
    	return;
    }
    if ($res['rows']==0){
    	Flight::json(array("status" => 0, "error" => "Usuario no tiene alcance definido"));
    	return;
    }

    $zona_id 			 = $res['data'][0]["zona_id"];
    $out['zona_alcance'] = $res['data'][0]["zona_nombre"];

    //Obtener OS por especialidad
    $res = $dbo->ExecuteQuery("SELECT
                                    espe_nombre AS nombre,
                                    count(*) AS cantidad
                               FROM 
                                    orden_servicio
                                    INNER JOIN emplazamiento emp ON (emp.empl_id=orden_servicio.empl_id)
                                    INNER JOIN rel_contrato_emplazamiento rce ON (rce.cont_id=orden_servicio.cont_id AND  rce.empl_id=emp.empl_id)
                                    INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = emp.empl_id)
                                    INNER JOIN subespecialidad ON (subespecialidad.sube_id=orden_servicio.sube_id)
                                    INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)
                                WHERE 
                                    orden_servicio.cont_id = $cont_id
                                    AND ( 
                                        (orden_servicio.orse_estado IN ('RECHAZADA','APROBADA') AND orden_servicio.orse_fecha_validacion >= $orse_fecha_inicio AND orden_servicio.orse_fecha_validacion <= $orse_fecha_termino )
                                        OR
                                        (orden_servicio.orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO'/*,'NOACTIVO','ANULADA'*/) AND orden_servicio.orse_fecha_solicitud <= $orse_fecha_termino )
                                    )
                                GROUP BY especialidad.espe_id");    
    
    if ($res['status'] == 0){
    	Flight::json(array("status" => 0, "error" => $res['error']));
    	return;
    }
    $out['os_espe'] = $res['data'];
    
    //Obtener OS por zona
    $res = $dbo->ExecuteQuery("SELECT
                                    zona_nombre,
                                    /*SUM(IF(orse_estado IN ('RECHAZADA','APROBADA'), 1, 0)) AS ejecutadas,
                                    SUM(IF(orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO'/*,'NOACTIVO','ANULADA'*/ /*), 1, 0)) AS noejecutadas*/

                                    SUM(IF(orse_estado IN ('APROBADA'), 1, 0)) AS aprobadas,
                                    SUM(IF(orse_estado IN ('RECHAZADA'), 1, 0)) AS rechazadas,
                                    /*SUM(IF(orse_estado IN ('VALIDANDO'), 1, 0)) AS movistar,
                                    SUM(IF(orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO'), 1, 0)) as contratista,*/
                                    SUM(IF(orse_responsable='MOVISTAR' AND orse_estado in ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO','VALIDANDO'), 1, 0)) AS movistar,
                                    SUM(IF(orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO','VALIDANDO') AND orse_responsable='CONTRATISTA', 1, 0)) as contratista,
                                    SUM(IF(orse_estado IN ('ANULADA'), 1, 0)) AS anuladas
                                FROM 
                                    orden_servicio
                                    INNER JOIN emplazamiento emp ON (emp.empl_id=orden_servicio.empl_id)
                                    INNER JOIN rel_contrato_emplazamiento rce ON (rce.cont_id=orden_servicio.cont_id AND  rce.empl_id=emp.empl_id)
                                    INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = emp.empl_id)
                                    INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = emp.empl_id)
                                    INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' AND zona.cont_id=$cont_id)
                                WHERE 
                                    orden_servicio.cont_id = $cont_id
                                    AND( 
                                        (orden_servicio.orse_estado IN ('RECHAZADA','APROBADA') AND orden_servicio.orse_fecha_validacion >= $orse_fecha_inicio AND orden_servicio.orse_fecha_validacion <= $orse_fecha_termino )
                                        OR
                                        (orden_servicio.orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO'/*,'NOACTIVO','ANULADA'*/) AND orden_servicio.orse_fecha_solicitud <= $orse_fecha_termino )
                                    )
                                GROUP BY zona.zona_nombre
                                ORDER BY zona.zona_id;");
    if ($res['status'] == 0){
    	Flight::json(array("status" => 0, "error" => $res['error']));
    	return;
    }
    $out['os_zona'] = $res['data'];

    //Obtener OS gasto
    $res = $dbo->ExecuteQuery("SELECT
                                zona_nombre AS nombre,
                                SUM((SELECT
                                    ROUND(SUM(prit_cantidad * lpu_item_precio.lpip_precio),2) AS pres_valor
                                    FROM
                                    presupuesto
                                    INNER JOIN presupuesto_item ON(presupuesto.pres_id=presupuesto_item.pres_id)
                                    INNER JOIN lpu_item_precio ON(presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
                                    WHERE presupuesto.orse_id=orden_servicio.orse_id AND pres_estado = 'APROBADO')
                                ) AS cantidad
                                FROM 
                                orden_servicio
                                INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = orden_servicio.empl_id)
                                INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = orden_servicio.empl_id)
                                INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' AND zona.cont_id=$cont_id)
                                WHERE 
                                    orden_servicio.cont_id = $cont_id AND 
                                    orse_estado='APROBADA' AND 
                                    orse_tipo IN ('OSEU','OSEN') AND
                                    orden_servicio.orse_fecha_validacion >= $orse_fecha_inicio AND orden_servicio.orse_fecha_validacion <= $orse_fecha_termino
                                #GROUP BY zona_nombre
                                GROUP BY zona.zona_nombre
                                ORDER by zona.zona_id");
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    $out['os_gasto'] = $res['data'];

    //Obtener OS por especialidad y zona
    $res = $dbo->ExecuteQuery("SELECT
                                zona.zona_nombre,
                                espe_nombre AS nombre,
                                count(*) AS cantidad
                               FROM 
                                orden_servicio
                                INNER JOIN emplazamiento emp ON (emp.empl_id=orden_servicio.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce ON (rce.cont_id=orden_servicio.cont_id AND  rce.empl_id=emp.empl_id)
                                INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = emp.empl_id)
                                INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = emp.empl_id)
                                INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' and zona.cont_id=$cont_id)
                                INNER JOIN subespecialidad ON (subespecialidad.sube_id=orden_servicio.sube_id)
                                INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)
                                WHERE orden_servicio.cont_id = $cont_id
                                    AND ( 
                                        (orden_servicio.orse_estado IN ('RECHAZADA','APROBADA') AND orden_servicio.orse_fecha_validacion >= $orse_fecha_inicio AND orden_servicio.orse_fecha_validacion <= $orse_fecha_termino )
                                        OR
                                        (orden_servicio.orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO'/*,'NOACTIVO','ANULADA'*/) AND orden_servicio.orse_fecha_solicitud <= $orse_fecha_termino )
                                    )
                                GROUP BY zona.zona_nombre,especialidad.espe_id
                                ORDER by zona.zona_id");
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    $out['os_espe_zona'] = array();
    foreach($res['data'] as $row){
        $out['os_espe_zona'][$row['zona_nombre']][] = array("nombre"=>$row['nombre'],"cantidad"=>$row['cantidad']);
    }

    //Obtener OSs por gestor y zona
    $res = $dbo->ExecuteQuery("SELECT
                                zona_nombre,
                                usua_nombre AS nombre,
                                /*SUM(IF(orse_estado IN ('RECHAZADA','APROBADA'), 1, 0)) AS ejecutadas,*/
                                SUM(IF(orse_estado IN ('APROBADA'), 1, 0)) AS aprobadas,
                                SUM(IF(orse_estado IN ('RECHAZADA'), 1, 0)) AS rechazadas,
                                /*SUM(IF(orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO'/*,'NOACTIVO','ANULADA'*/ /*), 1, 0)) AS noejecutadas,*/
                                /*SUM(IF(orse_estado IN ('VALIDANDO), 1, 0)) AS movistar,*/
                                SUM(IF(orse_responsable='MOVISTAR' AND orse_estado in ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO','VALIDANDO'), 1, 0)) AS movistar,
                                SUM(IF(orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO','VALIDANDO') AND orse_responsable='CONTRATISTA', 1, 0)) as contratista,
                                SUM(IF(orse_estado IN ('ANULADA'/*,'NOACTIVO','ANULADA'*/), 1, 0)) AS anuladas
                               FROM 
                                orden_servicio
                                INNER JOIN emplazamiento emp ON (emp.empl_id=orden_servicio.empl_id)
                                INNER JOIN rel_contrato_emplazamiento rce ON (rce.cont_id=orden_servicio.cont_id AND  rce.empl_id=emp.empl_id)
                                INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = emp.empl_id)
                                INNER JOIN usuario ON (usuario.usua_id=orden_servicio.usua_creador)
                                INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = emp.empl_id)
                                INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' AND zona.cont_id=$cont_id)
                                WHERE orden_servicio.cont_id = $cont_id
                                    AND ( 
                                        (orden_servicio.orse_estado IN ('RECHAZADA','APROBADA') AND orden_servicio.orse_fecha_validacion >= $orse_fecha_inicio AND orden_servicio.orse_fecha_validacion <= $orse_fecha_termino  )
                                        OR
                                        (orden_servicio.orse_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO', 'ANULADA' /*,'NOACTIVO','ANULADA'*/) AND orden_servicio.orse_fecha_solicitud <= $orse_fecha_termino )
                                    )
                                GROUP BY zona.zona_nombre,usua_nombre
                                ORDER BY  zona.zona_id");
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    $out['os_gestor_zona'] = array();
    foreach($res['data'] as $row){
        $out['os_gestor_zona'][$row['zona_nombre']][] = array("nombre"=>$row['nombre'],"aprobadas"=>$row['aprobadas'],"rechazadas"=>$row['rechazadas'],"movistar"=>$row['movistar'], "contratista"=>$row['contratista']);
    }


    //Obtener OS gasto por gestor y zona
    $res = $dbo->ExecuteQuery("SELECT
                                zona_nombre,
                                usua_nombre AS nombre,
                                SUM((SELECT
                                    ROUND(SUM(prit_cantidad * lpu_item_precio.lpip_precio),2) AS pres_valor
                                    FROM
                                    presupuesto
                                    INNER JOIN presupuesto_item ON(presupuesto.pres_id=presupuesto_item.pres_id)
                                    INNER JOIN lpu_item_precio ON(presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
                                    WHERE presupuesto.orse_id=orden_servicio.orse_id AND pres_estado = 'APROBADO')
                                ) AS cantidad
                                FROM 
                                orden_servicio
                                INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = orden_servicio.empl_id)
                                INNER JOIN usuario ON (usuario.usua_id=orden_servicio.usua_creador)
                                INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = orden_servicio.empl_id)
                                INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' and zona.cont_id=$cont_id)
                                WHERE 
                                    orden_servicio.cont_id = $cont_id AND 
                                    orse_estado='APROBADA' AND 
                                    orse_tipo IN ('OSEU','OSEN') AND
                                    orden_servicio.orse_fecha_validacion >= $orse_fecha_inicio AND orden_servicio.orse_fecha_validacion <= $orse_fecha_termino 
                                GROUP BY zona.zona_nombre,usua_nombre
                                ORDER BY  zona.zona_id");
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    $out['os_gasto_zona'] = array();
    foreach($res['data'] as $row){
        $out['os_gasto_zona'][$row['zona_nombre']][] = array("nombre"=>$row['nombre'],"cantidad"=>$row['cantidad']);
    }

    $out['os_zonas'] = array_unique(array_merge(array_keys($out['os_espe_zona']),
                       array_keys($out['os_gestor_zona']),
                       array_keys($out['os_gasto_zona'])));

    $out['status'] = 1;
    
    //Flight::json(array("status" => 0, "error" => json_encode($out)));
	
	
	
    
    Flight::json($out);
});

Flight::route('GET /contrato/@cont_id:[0-9]+/dashboard/mnt/filtros', function($cont_id){
    $out = array();
    $dbo = new MySQL_Database();
    
    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO' AND cont_id = $cont_id ");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['especialidades'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO' AND cont_id = $cont_id AND flag_grafico=1");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['especialidades_mnt'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT p.peri_id, p.peri_nombre FROM periodicidad p, rel_contrato_periodicidad rcp WHERE rcp.cont_id= $cont_id AND rcp.peri_id=p.peri_id");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['periodos'] = $res['data'];
    
    $out['status'] = 1;    
	$out['cont_id'] = $cont_id;   
	
    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/dashboard/mnt', function($cont_id){
    //session_start();
    $filtros_ini = array_merge($_GET,$_POST); 
    $usua_id = $_SESSION['user_id']; 
    $out     = array();
    $filtros = "TRUE ";

    $dbo = new MySQL_Database();

    if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
        if( is_array($filtros_ini['espe_id']) ){
            $filtros .= " AND mantenimiento.espe_id IN ('".implode("','",$filtros_ini['espe_id'])."') ";
        }
        else{
            $filtros .= " AND mantenimiento.espe_id = '".$filtros_ini['espe_id']."' ";
        }
    } 
    
    if( isset($filtros_ini['peri_id']) && $filtros_ini['peri_id']!="" ){
        if( is_array($filtros_ini['peri_id']) ){
            $filtros .= " AND periodicidad.peri_id IN ('".implode("','",$filtros_ini['peri_id'])."') ";
        }
        else{
            $filtros .= " AND periodicidad.peri_id = '".$filtros_ini['peri_id']."' ";
        }
    }     
    
    $fecha_referencia = 'NOW()';
    if( isset($filtros_ini['mant_fecha_referencia']) && $filtros_ini['mant_fecha_referencia']!="" ){
        $fecha_referencia = "DATE('".$filtros_ini['mant_fecha_referencia']."')";
    } 
    
    /*Obtener zona de alcance de usuario*/
    $res = $dbo->ExecuteQuery("SELECT 
									z.zona_id
								    , z.zona_nombre 
								FROM rel_contrato_usuario rcu
								INNER JOIN rel_contrato_usuario_alcance rcua ON (rcua.recu_id = rcu.recu_id)
								INNER JOIN zona z ON (z.zona_tipo= 'ALCANCE' AND z.zona_estado='ACTIVO'  AND z.zona_id = rcua.zona_id 
                                AND z.cont_id =rcu.cont_id)
								WHERE rcu.usua_id=$usua_id AND rcu.cont_id=$cont_id
								order by z.zona_id
							  ");
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    if ($res['rows']==0){
        Flight::json(array("status" => 0, "error" => "Usuario no tiene alcance definido"));
        return;
    }

    $zona_id             = $res['data'][0]["zona_id"];
    $out['zona_alcance'] = $res['data'][0]["zona_nombre"];


    //Obtener MNT por especialidad,periodo,zons 
    $res = $dbo->ExecuteQuery("
	
	SELECT 							espe_nombre,
                                    peri_nombre,
                                    zona_nombre,
									ejecutadas,
                                    aprobadas,
									rechazadas,
                                    finalizadas,
                                    ejecutadas_noaprobadas,
                                    movistar,
                                    contratista,
                                    anuladas,
                                    programadas,
                                    norealizadas,
                                    mape_fecha_inicio,
                                    mape_fecha_termino,
                                    peri_meses  
                                    from
									
                               (SELECT
                                    espe_nombre,
                                    peri_nombre,
                                    zona_nombre,
                                    SUM(IF(mant_estado IN ('APROBADA','RECHAZADA','FINALIZADA'), 1, 0)) AS ejecutadas,
                                    SUM(IF(mant_estado IN ('APROBADA'), 1, 0)) AS aprobadas,
                                    SUM(IF(mant_estado IN ('RECHAZADA'), 1, 0)) AS rechazadas,
                                    SUM(IF(mant_estado IN ('FINALIZADA'), 1, 0)) AS finalizadas,
                                    SUM(IF(mant_estado IN ('RECHAZADA','FINALIZADA','NO REALIZADO'), 1, 0)) AS ejecutadas_noaprobadas,
                                    SUM(IF(mant_estado IN ('VALIDANDO') OR (mant_responsable='MOVISTAR' AND mant_estado in ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO')), 1, 0)) AS movistar,
                                    SUM(IF(mant_estado IN ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO') AND mant_responsable='CONTRATISTA', 1, 0)) as contratista,
                                    SUM(IF(mant_estado IN ('ANULADA'), 1, 0)) AS anuladas,
                                    SUM(IF(mant_fecha_programada BETWEEN mape_fecha_inicio AND $fecha_referencia,1,0)) as programadas,
                                    SUM(IF(mant_estado IN ('NO REALIZADO'), 1, 0)) as norealizadas,
                                    DATE(mape_fecha_inicio) AS mape_fecha_inicio,
                                    DATE( DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY )) AS mape_fecha_termino,
                                    peri_meses
                                FROM 
                                    mantenimiento
                                    INNER JOIN especialidad ON (mantenimiento.espe_id = especialidad.espe_id)
                                    INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
                                    INNER JOIN rel_contrato_periodicidad ON (mantenimiento_periodos.rcpe_id = rel_contrato_periodicidad.rcpe_id)
                                    INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)

                                    INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = mantenimiento.empl_id)
                                    INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = mantenimiento.empl_id)
                                    INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' and zona.cont_id=$cont_id)
                                WHERE 
                                    mantenimiento.cont_id = $cont_id 
                                    AND mant_estado NOT IN ('ANULADA','NOACTIVO')
                                    AND (
                                        ($fecha_referencia BETWEEN mape_fecha_inicio AND DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY ) )
                                        OR
                                        (peri_nombre = 'A SOLICITUD' AND YEAR(mant_fecha_programada) = YEAR($fecha_referencia) AND MONTH(mant_fecha_programada) =  MONTH($fecha_referencia) )
                                    )
									 and peri_nombre <> 'A SOLICITUD'
                                    AND $filtros
									GROUP BY espe_nombre,peri_nombre,zona.zona_nombre,especialidad.espe_ord
                                    ORDER BY  zona.zona_id,periodicidad.peri_id,especialidad.espe_ord) a
									
									union all
									
									(SELECT
                                    espe_nombre,
                                    peri_nombre,
                                    zona_nombre,
                                    SUM(IF(mant_estado IN ('APROBADA','RECHAZADA','FINALIZADA'), 1, 0)) AS ejecutadas,
                                    SUM(IF(mant_estado IN ('APROBADA'), 1, 0)) AS aprobadas,
                                    SUM(IF(mant_estado IN ('RECHAZADA'), 1, 0)) AS rechazadas,
                                    SUM(IF(mant_estado IN ('FINALIZADA'), 1, 0)) AS finalizadas,
                                    SUM(IF(mant_estado IN ('RECHAZADA','FINALIZADA','NO REALIZADO'), 1, 0)) AS ejecutadas_noaprobadas,
                                    SUM(IF(mant_estado IN ('VALIDANDO') OR (mant_responsable='MOVISTAR' AND mant_estado in ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO')), 1, 0)) AS movistar,
                                    SUM(IF(mant_estado IN ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO') AND mant_responsable='CONTRATISTA', 1, 0)) as contratista,
                                    SUM(IF(mant_estado IN ('ANULADA'), 1, 0)) AS anuladas,
                                    SUM(IF(mant_fecha_programada BETWEEN mape_fecha_inicio AND $fecha_referencia,1,0)) as programadas,
                                    SUM(IF(mant_estado IN ('NO REALIZADO'), 1, 0)) as norealizadas,
                                    DATE(mape_fecha_inicio) AS mape_fecha_inicio,
                                    DATE( DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY )) AS mape_fecha_termino,
                                    peri_meses
                                FROM 
                                    mantenimiento
                                    INNER JOIN especialidad ON (mantenimiento.espe_id = especialidad.espe_id)
                                    INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
                                    INNER JOIN rel_contrato_periodicidad ON (mantenimiento_periodos.rcpe_id = rel_contrato_periodicidad.rcpe_id)
                                    INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)

                                    INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = mantenimiento.empl_id)
                                    INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = mantenimiento.empl_id)
                                    INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' and zona.cont_id=$cont_id)
                                WHERE 
                                    mantenimiento.cont_id = $cont_id 
                                    AND mant_estado NOT IN ('ANULADA','NOACTIVO')
                                    AND (
                                        ($fecha_referencia BETWEEN mape_fecha_inicio AND DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY ) )
                                        OR
                                        (peri_nombre = 'A SOLICITUD' AND YEAR(mant_fecha_programada) = YEAR($fecha_referencia) AND MONTH(mant_fecha_programada) =  MONTH($fecha_referencia) )
                                    )
									 AND peri_nombre = 'A SOLICITUD'
                                    AND $filtros
									GROUP BY espe_nombre,peri_nombre,zona.zona_nombre,especialidad.espe_ord)
                                   
									
									
									
									"); 
    

/**	$res = $dbo->ExcecuteQuery("
				SELECT
                                    espe_nombre,
                                    peri_nombre,
                                    zona_nombre,
                                    SUM(IF(mant_estado IN ('APROBADA','RECHAZADA','FINALIZADA','NO REALIZADO'), 1, 0)) AS ejecutadas,
                                    SUM(IF(mant_estado IN ('APROBADA'), 1, 0)) AS ejecutadas_aprobadas,
                                    SUM(IF(mant_estado IN ('RECHAZADA','FINALIZADA','NO REALIZADO'), 1, 0)) AS ejecutadas_noaprobadas,
                                    SUM(IF(mant_estado IN ('CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO'), 1, 0)) AS noejecutadas,
                                    SUM(IF(mant_fecha_programada BETWEEN mape_fecha_inicio AND $fecha_referencia,1,0)) as programadas,
                                    DATE(mape_fecha_inicio) AS mape_fecha_inicio,
                                    DATE( DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY )) AS mape_fecha_termino,
                                    peri_meses
                                FROM 
                                    mantenimiento
                                    INNER JOIN especialidad ON (mantenimiento.espe_id = especialidad.espe_id)
                                    INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
                                    INNER JOIN rel_contrato_periodicidad ON (mantenimiento_periodos.rcpe_id = rel_contrato_periodicidad.rcpe_id)
                                    INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)

                                    INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = mantenimiento.empl_id)
                                    INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = mantenimiento.empl_id)
                                    INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR')
                                WHERE 
                                    mantenimiento.cont_id = $cont_id 
                                    AND mant_estado NOT IN ('ANULADA','NOACTIVO')
                                    AND (
                                        ($fecha_referencia BETWEEN mape_fecha_inicio AND DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY ) )
                                        OR
                                        (peri_nombre = 'A SOLICITUD' AND YEAR(mant_fecha_programada) = YEAR($fecha_referencia) AND MONTH(mant_fecha_programada) =  MONTH($fecha_referencia) )
                                    )
                                    AND $filtros
                                GROUP BY espe_nombre,peri_nombre,zona_nombre, peri_meses, DATE(mape_fecha_inicio), DATE( DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY ))
                                ;"); 
*/
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    $out['mnt_zona'] = array();
    foreach($res['data'] as $row){
        $key = $row['espe_nombre']." ".$row['peri_nombre'].(($row['peri_meses']!=NULL)?" (".$row['mape_fecha_inicio']." a ".$row['mape_fecha_termino'].")":" (mes actual)");
        $out['mnt_zona'][$key][] = array("nombre"=>$row['zona_nombre'],"aprobadas"=>$row['aprobadas'],"rechazadas"=>$row['rechazadas'],"programadas"=>$row['programadas'],"movistar"=>$row['movistar'],"contratista"=>$row['contratista'],"norealizadas"=>$row['norealizadas']);
    }


    //Obtener MNT por zona 
    $res = $dbo->ExecuteQuery("
                                SELECT
                                    zona_nombre,
                                    CONCAT(espe_nombre,' ',peri_nombre) AS espe_peri,
                                    SUM(IF(mant_estado IN ('APROBADA','RECHAZADA','FINALIZADA'), 1, 0)) AS ejecutadas,
                                    SUM(IF(mant_estado IN ('APROBADA'), 1, 0)) AS aprobadas,
                                    SUM(IF(mant_estado IN ('RECHAZADA'), 1, 0)) AS rechazadas,
                                    SUM(IF(mant_estado IN ('FINALIZADA'), 1, 0)) AS finalizadas,
                                    SUM(IF(mant_estado IN ('ANULADA'), 1, 0)) AS anuladas,
                                    SUM(IF(mant_estado IN ('RECHAZADA','FINALIZADA','NO REALIZADO'), 1, 0)) AS ejecutadas_noaprobadas,
                                    SUM(IF(mant_estado IN ('VALIDANDO'/*,'NOACTIVO','ANULADA'*/), 1, 0)) AS movistar,
                                    SUM(IF(mant_estado IN ('CREADA','ASIGNANDO','ASIGNADA', 'EJECUTANDO'), 1, 0)) as contratista,
                                    SUM(IF(mant_estado IN ('NO REALIZADO'), 1, 0)) as norealizadas,
                                    SUM(IF(mant_fecha_programada BETWEEN mape_fecha_inicio AND $fecha_referencia,1,0)) as programadas 
                               FROM 
                                    mantenimiento
                                    INNER JOIN especialidad ON (mantenimiento.espe_id = especialidad.espe_id)
                                    INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
                                    INNER JOIN rel_contrato_periodicidad ON (mantenimiento_periodos.rcpe_id = rel_contrato_periodicidad.rcpe_id)
                                    INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)

                                    INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.zona_id=$zona_id AND rel_zona_emplazamiento.empl_id = mantenimiento.empl_id)
                                    INNER JOIN rel_zona_emplazamiento AS reze ON (reze.empl_id = mantenimiento.empl_id)
                                    INNER JOIN zona ON(reze.zona_id = zona.zona_id AND zona.zona_tipo='MOVISTAR' and zona.cont_id=$cont_id)
                                WHERE 
                                    mantenimiento.cont_id = $cont_id 
                                    AND mant_estado NOT IN ('ANULADA','NOACTIVO')
                                    AND (
                                        ($fecha_referencia BETWEEN mape_fecha_inicio AND DATE_SUB( DATE_ADD(mape_fecha_inicio,INTERVAL peri_meses MONTH), INTERVAL 1 DAY ) )
                                        OR
                                        (peri_nombre = 'A SOLICITUD'   AND YEAR(mant_fecha_programada) = YEAR($fecha_referencia) AND MONTH(mant_fecha_programada) =  MONTH($fecha_referencia) )
                                    )
                                    AND $filtros
									 
									AND peri_nombre <> 'A SOLICITUD'
									GROUP BY  espe_nombre,peri_nombre,zona.zona_nombre,especialidad.espe_ord
									ORDER BY  zona.zona_id,periodicidad.peri_id,especialidad.espe_ord");
    if ($res['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    $out['mnt_espe_peri'] = array();
    foreach($res['data'] as $row){
        $out['mnt_espe_peri'][$row['zona_nombre']][] = array("nombre"=>$row['espe_peri'],"aprobadas"=>$row['aprobadas'],"rechazadas"=>$row['rechazadas'],"programadas"=>$row['programadas'],"movistar"=>$row['movistar'],"contratista"=>$row['contratista'],"norealizadas"=>$row['norealizadas']);
    }

    $out['status'] = 1;
    Flight::json($out);
});

?>

<?php


Flight::route('GET /core/formulario/filtros', function(){    
    
    $out = array();
    $out['status'] = 1;

    $db  = new MySQL_Database();
    $res = $db->ExecuteQuery("SELECT
                                cont_id,
                                cont_nombre
                              FROM 
                                contrato
                              WHERE cont_estado='ACTIVO'");
    if (0 == $res['status'] ){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    $out['contratos'] = $res['data'];
    Flight::json($out);
});


Flight::route('GET|POST /core/contrato/@contract:[0-9]+/formulario/list/@page:[0-9]+', function($contract,$page){
    
    $out = array();
    $out['status'] = 1;

    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    #$filtros = Flight::filtersToWhereString( array($tabla), $filtros_ini);
    $filtros = Flight::filtersToWhereString( array("formulario"), $filtros_ini);

    $db  = new MySQL_Database();
    $res = $db->ExecuteQuery("SELECT  SQL_CALC_FOUND_ROWS
                                        f.form_id as form_id,
                                        f.cont_id as cont_id,
                                        f.espe_id as espe_id,
                                        espe.espe_nombre as espe_nombre,
                                        f.form_nombre as form_nombre,
                                        f.form_tipo as form_tipo,
                                        f.form_alias as form_alias,
                                        f.form_descripcion as form_descripcion,
                                        f.form_fecha_creacion as form_fecha_creacion,
                                        f.usua_creador as usua_creador,
                                        f.form_opciones as form_opciones,
                                        f.form_estado as form_estado
                                FROM formulario f
                                join especialidad espe on f.espe_id=espe.espe_id and f.cont_id=espe.cont_id 
                                WHERE f.cont_id=$contract AND " .$filtros." ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page)));
    if (0 == $res['status'] ){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    $res_count = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;  
    } 
    $res['total'] = intval($res_count['data'][0]['total']);

    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }
    Flight::json($res); 

});


Flight::route('GET /core/formulario/get/@form_id:[0-9]+', function($form_id){   
    $out = array();
    $out['status'] = 1;
    $out['data'] = array();

    $db  = new MySQL_Database();
    $res = $db->ExecuteQuery("SELECT
                                cont_id,
                                form_id ,
                                form_nombre,
                                form_alias,
                                form_descripcion,
                                form_tipo,
                                form_opciones,
								form_orden
                              FROM 
                                formulario
                              WHERE form_id = $form_id");
    if (0 == $res['status'] ){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
           
    $out['data']['form'] = $res['data'][0];

    $res = $db->ExecuteQuery("SELECT
                                fogr_id,
                                fogr_nombre,
                                fogr_opciones,
                                COALESCE(NULLIF(fogr_orden, 1), 1) as fogr_orden,
                                fogr_estado
                              FROM 
                                formulario_grupo
                              WHERE form_id = $form_id
                              ORDER BY fogr_orden,fogr_id");
    if (0 == $res['status'] ){
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    $out['data']['pages'] = $res['data'];

    foreach($out['data']['pages'] as &$page){
        $fogr_id = $page['fogr_id'];

        $res = $db->ExecuteQuery("SELECT
                                    foit.foit_id,
                                    foit_tipo,
                                    foit_nombre,
                                    foit_requerido,
                                    foit_orden,
                                    foit_opciones,
                                    foit_estado,
                                    indf_genero,
                                    indf_especie,
                                    indf_campo,
                                    indf_codigo
                                  FROM 
                                    formulario_item foit
                                  LEFT JOIN inventario_definicion_formulario indf ON (indf.foit_id=foit.foit_id)
                                  WHERE foit.fogr_id = $fogr_id
                                  ORDER BY foit_orden,foit_id");
        if($res['status']==0){
            return array("status"=>false, "error"=>$res['error']);
        }
        $page['controls'] = $res['data'];

    }

    Flight::json($out);
});


Flight::route('POST /core/formulario/add', function(){
    $form = json_decode($_POST['form'],true);
 

    if($form!=null){
        $usua_creador  = $_SESSION['user_id'];
        $foit_id_status = array();
        $inventariables = array();

        $db  = new MySQL_Database();
        $db->startTransaction();

        $cont_id = mysql_real_escape_string($form['cont_id']);
        $form_nombre = mysql_real_escape_string($form['form_nombre']);
        $form_alias = mysql_real_escape_string($form['form_alias']);
        $form_descripcion = mysql_real_escape_string($form['form_descripcion']);
        if(empty($form['form_opciones'])){
            $form_opciones = "";
        }
        else{
            $form_opciones = mysql_real_escape_string(json_encode($form['form_opciones']));
        }

        $res = $db->ExecuteQuery("INSERT INTO formulario SET
                                    cont_id = $cont_id,
                                    form_nombre = '$form_nombre',
                                    form_tipo = 'form',
                                    form_alias = '$form_alias',
                                    form_descripcion = '$form_descripcion',
                                    form_fecha_creacion = NOW(),
                                    usua_creador = '$usua_creador',
                                    form_opciones = '$form_opciones',
                                    form_estado = 'ACTIVO'");

        if (0 == $res['status'] ){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        $form_id = $res['data'][0]['id'];

        foreach($form['pages'] as $page_order => $page){
            $fogr_nombre   = mysql_real_escape_string($page['fogr_nombre']);
            $fogr_estado   = mysql_real_escape_string($page['fogr_estado']);

            if(empty($page['fogr_opciones'])){
                $fogr_opciones = "";
            }
            else{
                $fogr_opciones = mysql_real_escape_string(json_encode($page['fogr_opciones']));
            }

            $res = $db->ExecuteQuery("INSERT INTO formulario_grupo SET
                                    form_id = $form_id,
                                    fogr_nombre = '$fogr_nombre',
                                    fogr_orden = '$page_order',
                                    fogr_opciones = '$fogr_opciones',
                                    fogr_estado = '$fogr_estado'");
            if (0 == $res['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
            $fogr_id = $res['data'][0]['id'];

            $inAgregador = false;
            $labels = [];
            $controls= [];
            $foit_id_agregador=0;
            foreach($page['controls'] as $control_order => $control){
                $foit_tipo     = mysql_real_escape_string($control['foit_tipo']);
                $foit_nombre   = mysql_real_escape_string($control['foit_nombre']);
                $foit_requerido = mysql_real_escape_string($control['foit_requerido']);
                $foit_estado   = mysql_real_escape_string($control['foit_estado']);
                $aggregated     = mysql_real_escape_string($control['aggregated']);
                $with_status = false;
                
                if($aggregated){
                    $control['foit_opciones']['aggregated'] = true;
                }

                if(empty($control['foit_opciones'])){
                    $foit_opciones = "";
                }
                else{
                    if($foit_tipo=="SAVE"){
                        if(isset($control['foit_opciones']['save_action']) && $control['foit_opciones']['save_action']=="save"){
                            $with_status = true;
                        }  
                    }

                    $foit_opciones = mysql_real_escape_string(json_encode($control['foit_opciones']));
                }
                
                

                $res = $db->ExecuteQuery("INSERT INTO formulario_item SET
                                        fogr_id = $fogr_id,
                                        foit_tipo = '$foit_tipo',
                                        foit_nombre = '$foit_nombre',
                                        foit_orden = '$control_order',
                                        foit_requerido = '$foit_requerido',
                                        foit_opciones = '$foit_opciones',
                                        foit_estado = '$foit_estado'");
                if (0 == $res['status'] ){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $foit_id = $res['data'][0]['id'];

                if($with_status){
                    array_push($foit_id_status,$foit_id);
                }

                //Inventariable
                if($control['indf_inventariable']){
                    array_push($inventariables,array('fogr_id' => $fogr_id,
                                                     'foit_id' => $foit_id,
                                                     'indf_genero' => $control['indf_genero'],
                                                     'indf_especie' => $control['indf_especie'],
                                                     'indf_campo' => $control['indf_campo'],
                                                     'indf_codigo' => $control['indf_codigo']));
                }

                if($foit_tipo == "AGGREGATOR"){
                    $inAgregador = true;
                    $labels = [];
                    $controls= [];
                    $foit_id_agregador=$foit_id;
                    continue;
                }
                if($inAgregador){
                    if( $aggregated ){
                        array_push($labels, $foit_nombre);
                        array_push($controls, $foit_id);
                    }
                    else{
                        $inAgregador = false;
                        $string = json_encode(  array( "labels"=>$labels,"controls"=>$controls) );
                        $res = $db->ExecuteQuery("UPDATE formulario_item SET
                                    foit_opciones = '$string'
                                    WHERE foit_id=$foit_id_agregador");

                        if (0 == $res['status'] ){
                            $db->Rollback();
                            Flight::json(array("status" => 0, "error" => $res['error']));
                            return;
                        }
                    }                   

                }

            }
            if($inAgregador){
                $inAgregador = false;
                $string = json_encode(  array( "labels"=>$labels,"controls"=>$controls) );
                $res = $db->ExecuteQuery("UPDATE formulario_item SET
                            foit_opciones = '$string'
                            WHERE foit_id = $foit_id_agregador");

                if (0 == $res['status'] ){
                        $db->Rollback();
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                }
            }
                    

                
        }


        //Agregar opciones de cambio de estado en guardar con action:save
        if(0<count($foit_id_status)){
            if($form_opciones==""){
                $status = array();
                $conditions = array();
                foreach ($foit_id_status as $foit_id) {
                    array_push($status,'"DONE"');
                    array_push($conditions,'['.$foit_id.',"=","save"]');
                }
                $status = implode(",",$status);
                $conditions = implode(",",$conditions);

                $form_opciones = '{"location":true, "task_item_status_conditions":{"status":['.$status.'],"conditions":['.$conditions.']}}';
            }
            else{
                //TODO
            }

            $res = $db->ExecuteQuery("UPDATE formulario SET
                                    form_opciones = '$form_opciones'
                                    WHERE form_id=$form_id");
            if (0 == $res['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }

        //Agregar inventariables
        if(0<count($inventariables)){
            foreach($inventariables as $i){
                $fogr_id = $i['fogr_id'];
                $foit_id = $i['foit_id'];
                $indf_genero= $i['indf_genero'];
                $indf_especie= $i['indf_especie'];
                $indf_campo= $i['indf_campo'];
                $indf_codigo= $i['indf_codigo'];

                $res = $db->ExecuteQuery("INSERT INTO inventario_definicion_formulario SET
                                        form_id = $form_id,
                                        fogr_id = $fogr_id,
                                        foit_id = $foit_id,
                                        indf_genero = '$indf_genero',
                                        indf_especie = '$indf_especie',
                                        indf_campo = '$indf_campo',
                                        indf_codigo = '$indf_codigo'");
                if (0 == $res['status'] ){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
            }
        }



        $db->Commit();
        Flight::json($res);
    }
    else{
        Flight::json(array("status" => 0, "error" => "Formato de formulario no válido"));
    }
});

Flight::route('POST /core/formulario/update/@form_id:[0-9]+', function($form_id){  
    $form = json_decode($_POST['form'],true);

    //Flight::json(array("status" => 0, "error" => json_encode($form)));
    if($form!=null){
        $usua_creador  = $_SESSION['user_id'];
        $foit_id_status = array();
        $inventariables = array();

        $db  = new MySQL_Database();
        $db->startTransaction();

        $cont_id = mysql_real_escape_string($form['cont_id']);
        $form_nombre = mysql_real_escape_string($form['form_nombre']);
		$form_orden = mysql_real_escape_string($form['form_orden']);
        $form_alias = mysql_real_escape_string($form['form_alias']);
        $form_descripcion = mysql_real_escape_string($form['form_descripcion']);
        if(empty($form['form_opciones'])){
            $form_opciones = "";
        }
        else{
            $form_opciones = mysql_real_escape_string(json_encode($form['form_opciones']));
        }

        $res = $db->ExecuteQuery("UPDATE formulario SET
                                    form_nombre = '$form_nombre',
                                    form_tipo = 'form',
                                    form_alias = '$form_alias',
									form_orden = '$form_orden',
                                    form_descripcion = '$form_descripcion'
                                    WHERE form_id = $form_id
                                    AND cont_id=$cont_id");
        if (0 == $res['status'] ){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        foreach($form['pages'] as $page_order => $page){
            $fogr_id       = mysql_real_escape_string($page['fogr_id']);
            $fogr_nombre   = mysql_real_escape_string($page['fogr_nombre']);
			$fogr_orden    = mysql_real_escape_string($page['fogr_orden']);
            $fogr_estado   = mysql_real_escape_string($page['fogr_estado']);

            if(empty($page['fogr_opciones'])){
                $fogr_opciones = "";
            }
            else{
                $fogr_opciones = mysql_real_escape_string(json_encode($page['fogr_opciones']));
            }

            if($fogr_id==""){
                $res = $db->ExecuteQuery("INSERT INTO formulario_grupo SET
                                    form_id = $form_id,
                                    fogr_nombre = '$fogr_nombre',
                                    fogr_orden = '$fogr_orden',
                                    fogr_opciones = '$fogr_opciones',
                                    fogr_estado = '$fogr_estado'");
                if (0 == $res['status'] ){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
                $fogr_id = $res['data'][0]['id'];
            }
            else{
                $res = $db->ExecuteQuery("UPDATE formulario_grupo SET
                                    fogr_nombre = '$fogr_nombre',
                                    fogr_orden = '$fogr_orden',
                                    fogr_opciones = '$fogr_opciones',
                                    fogr_estado = '$fogr_estado'
                                    WHERE fogr_id=$fogr_id");
                if (0 == $res['status'] ){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
            }
            
            $inAgregador = false;
            $labels = [];
            $controls= [];
            $foit_id_agregador=0;
            
            foreach($page['controls'] as $control_order => $control){
                $foit_id     = mysql_real_escape_string($control['foit_id']);
                $foit_tipo     = mysql_real_escape_string($control['foit_tipo']);
                $foit_nombre   = mysql_real_escape_string($control['foit_nombre']);
                $foit_requerido = mysql_real_escape_string($control['foit_requerido']);
                $foit_estado   = mysql_real_escape_string($control['foit_estado']);
                $aggregated     = mysql_real_escape_string($control['aggregated']);
                $with_status = false;
                
                if($aggregated && !isset($control['foit_opciones']['aggregated'])  ){
                    $control['foit_opciones']['aggregated'] = true;
                }

                if(empty($control['foit_opciones'])){
                    $foit_opciones = "";
                }
                else{
                    if($foit_tipo=="SAVE"){
                        if(isset($control['foit_opciones']['save_action']) && $control['foit_opciones']['save_action']=="save"){
                            $with_status = true;
                        }  
                    }
                    $foit_opciones = mysql_real_escape_string(json_encode($control['foit_opciones']));
                }

                if($foit_id==""){
                    $res = $db->ExecuteQuery("INSERT INTO formulario_item SET
                                            fogr_id = $fogr_id,
                                            foit_tipo = '$foit_tipo',
                                            foit_nombre = '$foit_nombre',
                                            foit_orden = '$control_order',
                                            foit_requerido = '$foit_requerido',
                                            foit_opciones = '$foit_opciones',
                                            foit_estado = '$foit_estado'");
                    if (0 == $res['status'] ){
                        $db->Rollback();
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                    $foit_id = $res['data'][0]['id'];
                }
                else{
                    $res = $db->ExecuteQuery("UPDATE formulario_item SET
                                            foit_nombre = '$foit_nombre',
                                            foit_orden = '$control_order',
                                            foit_requerido = '$foit_requerido',
                                            foit_opciones = '$foit_opciones',
                                            foit_estado = '$foit_estado'
                                            WHERE foit_id=$foit_id");
                    if (0 == $res['status'] ){
                        $db->Rollback();
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                    }
                }

                if($with_status){
                    array_push($foit_id_status,$foit_id);
                }

                //Inventariable
                if($control['indf_inventariable']){
                    array_push($inventariables,array('fogr_id' => $fogr_id,
                                                     'foit_id' => $foit_id,
                                                     'indf_genero' => $control['indf_genero'],
                                                     'indf_especie' => $control['indf_especie'],
                                                     'indf_campo' => $control['indf_campo'],
                                                     'indf_codigo' => $control['indf_codigo']));
                }
                
                if($foit_tipo == "AGGREGATOR"){
                    $inAgregador = true;
                    $labels = [];
                    $controls= [];
                    $foit_id_agregador=$foit_id;
                    continue;
                }
                if($inAgregador){
                    if( $aggregated ){
                        array_push($labels, $foit_nombre);
                        array_push($controls, $foit_id);
                    }
                    else{
                        $inAgregador = false;
                        $string = json_encode(  array( "labels"=>$labels,"controls"=>$controls) );
                        $res = $db->ExecuteQuery("UPDATE formulario_item SET
                                    foit_opciones = '$string'
                                    WHERE foit_id=$foit_id_agregador");

                        if (0 == $res['status'] ){
                            $db->Rollback();
                            Flight::json(array("status" => 0, "error" => $res['error']));
                            return;
                        }
                    }                   

                }
            }
            if($inAgregador){
                $inAgregador = false;
                $string = json_encode(  array( "labels"=>$labels,"controls"=>$controls) );
                $res = $db->ExecuteQuery("UPDATE formulario_item SET
                            foit_opciones = '$string'
                            WHERE foit_id = $foit_id_agregador");

                if (0 == $res['status'] ){
                        $db->Rollback();
                        Flight::json(array("status" => 0, "error" => $res['error']));
                        return;
                }
            }
        }

        //Agregar opciones de cambio de estado en guardar con action:save
        if(0<count($foit_id_status)){
            if($form_opciones==""){
                $status = array();
                $conditions = array();
                foreach ($foit_id_status as $foit_id) {
                    array_push($status,'"DONE"');
                    array_push($conditions,'['.$foit_id.',"=","save"]');
                }
                $status = implode(",",$status);
                $conditions = implode(",",$conditions);

                $form_opciones = '{"location":true, "task_item_status_conditions":{"status":['.$status.'],"conditions":['.$conditions.']}}';
            }
            else{
                //TODO
            }

            $res = $db->ExecuteQuery("UPDATE formulario SET
                                    form_opciones = '$form_opciones'
                                    WHERE form_id=$form_id");
            if (0 == $res['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $res['error']));
                return;
            }
        }


        //borro definiciones anteriores
        $res = $db->ExecuteQuery("DELETE FROM inventario_definicion_formulario WHERE form_id=$form_id");
        if (0 == $res['status'] ){
            $db->Rollback();
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }

        //Actualizar inventariables
        if(0<count($inventariables)){
            foreach($inventariables as $i){
                $fogr_id = $i['fogr_id'];
                $foit_id = $i['foit_id'];
                $indf_genero= $i['indf_genero'];
                $indf_especie= $i['indf_especie'];
                $indf_campo= $i['indf_campo'];
                $indf_codigo= $i['indf_codigo'];

                $res = $db->ExecuteQuery("INSERT INTO inventario_definicion_formulario SET
                                        form_id = $form_id,
                                        fogr_id = $fogr_id,
                                        foit_id = $foit_id,
                                        indf_genero = '$indf_genero',
                                        indf_especie = '$indf_especie',
                                        indf_campo = '$indf_campo',
                                        indf_codigo = '$indf_codigo'");
                if (0 == $res['status'] ){
                    $db->Rollback();
                    Flight::json(array("status" => 0, "error" => $res['error']));
                    return;
                }
            }
        }



        $db->Commit();
        Flight::json($res);
    }
    else{
        Flight::json(array("status" => 0, "error" => "Formato de formulario no válido"));
    }
});


?>
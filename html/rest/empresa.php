<?php

/*FILTROS*/
Flight::route('GET /core/empresa/filtros', function(){
    $dbo = new MySQL_Database();
    $out = array("status" => 1);
    
    $res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre 
                                FROM region 
                                WHERE pais_id=1 
                                ORDER BY regi_orden ASC");
    if (0 == $res['status'] ){ 
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }
    $out['regiones'] = $res['data'];
    
    $res = $dbo->ExecuteQuery(" SELECT prov_id, regi_id, prov_nombre, prov_orden 
                                FROM provincia");
    if (0 == $res['status'] ){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));  
    } 
    $out['provincias'] = $res['data'];

    $res = $dbo->ExecuteQuery(" SELECT comu_id, prov_id, comu_nombre 
                                FROM comuna 
                                ORDER BY comu_nombre");
    if (0 == $res['status'] ) {
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }
    $out['comunas'] = $res['data'];
    
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM empresa WHERE Field = 'empr_estado'" );
    if (0 == $res['status'] ) {
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['estados'] = explode("','", $matches[1]);  

    Flight::json($out);	
});

/*Lista la informacion completa de la empresa*/
Flight::route('GET|POST /core/empresa/@id:[0-9]+/contrato/list(/@page:[0-9]+)', function($empr_id, $page){
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("empresa", "contrato"), $filtros_ini);
    
    $dbo = new MySQL_Database();	
    $query = "SELECT  c.cont_id
                    , c.cont_nombre
                    , c.cont_alias
                    , c.cont_fecha_inicio
                    , c.cont_fecha_termino
                    , c.cont_fecha_creacion
                    , c.cont_observacion
                    , c.cont_descripcion
                    , c.cont_estado
                    , c.usua_creador
                    , c.pais_id
                    , rce.coem_tipo
					, rce.coem_id
                FROM empresa e
                    , contrato c
                    , rel_contrato_empresa rce
                WHERE 
                    e.empr_id = ".$empr_id."  
                    AND rce.empr_id = e.empr_id 
                    AND rce.cont_id = c.cont_id 
                    AND $filtros ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))
            ;
    $res = $dbo->ExecuteQuery($query);
    if ( 0 == $res['status']) {
        Flight::json(array("status" => 0, "error" => $res['error']));
    }
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ( 0 == $res_count['status']) {
        Flight::json(array("status" => 0, "error" => $res_count['error']));
    }
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if (!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total'] / $results_by_page);
    }
    Flight::json($res);
});

/*Listado de contratos por empresa Felipe Azabache */
Flight::route('GET /core/empresa/@id:[0-9]+/contrato/lista(/@page:[0-9]+)', function($empr_id, $page){

    $out = array();
    $out['status'] = 1;
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("empresa", "contrato"), $filtros_ini);
    
    $dbo = new MySQL_Database();    
    $query = "
                SELECT  c.cont_id
                    ,c.cont_nombre
                    ,GROUP_CONCAT( rce.coem_tipo) as coem_tipo
                FROM empresa e
                , contrato c
                , rel_contrato_empresa rce
                WHERE
                    e.empr_id = $empr_id
                    AND rce.empr_id = e.empr_id
                    AND rce.cont_id = c.cont_id
                    AND c.cont_estado= 'ACTIVO'
                    AND rce.coem_estado = 'ACTIVO'
                    group by c.cont_id
                   "
    ;
    
    $res = $dbo->ExecuteQuery($query);
    if (0 == $res['status']) {
        Flight::json(array("status" => 0, "error" => $res['error']));
    } 
    $cantidad = sizeof($res['data']);

    $out=$res;

    for($i=0; $i<$cantidad; $i++) {
        $out ['data'][$i]['coem_tipo'] = explode(',',$res['data'][$i]['coem_tipo']);     
    }    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if (0 == $res_count['status'] ) {
        Flight::json(array("status" => 0, "error" => $res_count['error']));
    }
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if (!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total'] / $results_by_page);
    }
    Flight::json($out);
});

/*LIST*/
Flight::route('GET|POST /core/empresa/contrato/@cont_id:[0-9]+/list(/@page:[0-9]+)', function($cont_id, $page){
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("empresa", "contrato"), $filtros_ini);
    $dbo = new MySQL_Database();    
    $query = "  SELECT DISTINCT e.empr_id
                        ,e.comu_id
                        ,e.empr_alias
                        ,e.empr_nombre
                        ,e.empr_rut
                        ,e.empr_giro
                        ,e.empr_direccion
                        ,e.empr_fecha_creacion
                        ,e.empr_observacion
                        ,e.empr_contacto_nombre
                        ,e.empr_contacto_email
                        ,e.empr_contacto_telefono_fijo
                        ,e.empr_contacto_telefono_movil
                        ,e.empr_contacto_direccion
                        ,e.empr_estado
                        ,e.usua_creador
                FROM empresa e,  rel_contrato_empresa rce
                WHERE 
                    e.empr_id = rce.empr_id  
                    AND rce.cont_id = $cont_id
                    AND $filtros 
    ";
    $res = $dbo->ExecuteQuery($query);
    if (0 == $res['status'] ) {
        Flight::json(array("status" => 0, "error" => $res['error']));
    }
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if (0 == $res_count['status'] ) {
        Flight::json(array("status" => 0, "error" => $res_count['error']));  
    } 
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if (!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total'] / $results_by_page);
    }
    Flight::json($res);
});

/*Boton eliminar empresa, des habilita todas las relaciones Felipe Azabache*/
Flight::route('POST /core/empresa/@empr_id/contrato/@cont_id/eliminar', function($empr_id, $cont_id){

    $out = array("status" => 1);
    $dbo = new MySQL_Database();
    
    $query = "  UPDATE  rel_contrato_empresa
                SET  coem_estado = 'NOACTIVO'
                WHERE cont_id    = $cont_id 
                and  empr_id     = $empr_id";
    $res = $dbo->ExecuteQuery($query);
    if (0 == $res['status']) {
        Flight::json(array("status" => 0, "error" => $res['error']));
    } 

    Flight::json($out);
});

/*Actualiza   los tipos de contratos tales como contrato tipo cliente ,contratista,subcontratista,terceros' Felipe Azabache*/
Flight::route('POST /core/empresa/@empr_id/contrato/@cont_id/editar/@coem_tipo/tipo', 
function($empr_id, $cont_id,$coem_tipo){
    $out = array("status" => 1);
    $dbo = new MySQL_Database();    
    $tipo = explode(",", $coem_tipo);

    $query = "  UPDATE rel_contrato_empresa 
                SET  coem_estado = 'NOACTIVO'
                WHERE cont_id    = $cont_id 
                and  empr_id     = $empr_id";
    $res = $dbo->ExecuteQuery($query);
    if (0 == $res['status']) {
            Flight::json(array("status" => 0, "error" => $res['error']));
        }

    foreach($tipo AS $tipos){

        $query = " SELECT coem_tipo
                    FROM  rel_contrato_empresa
                    WHERE cont_id   = $cont_id
                    AND empr_id     = $empr_id
                    AND coem_tipo   = '$tipos'

        ";
        $res = $dbo->ExecuteQuery($query);
        if (0 == $res['status']) {
                Flight::json(array("status" => 0, "error" => $res['error']));
            }
        if(0 == $res['rows']){
            
            $query = " INSERT INTO rel_contrato_empresa
                                    (cont_id
                                    ,empr_id
                                    ,coem_tipo
                                    ,coem_estado)
                        VALUES
                                    (
                                    $cont_id
                                    ,$empr_id
                                    ,'$tipos'
                                    ,'ACTIVO'
                                    )

            ";
            $res = $dbo->ExecuteQuery($query);

            if (0 == $res['status']) {
                Flight::json(array("status" => 0, "error" => $res['error']));
            }
        }else{ 
            $query = "  UPDATE  rel_contrato_empresa
                        SET  coem_estado= 'ACTIVO'
                        WHERE cont_id   = cont_id 
                        AND  empr_id    = empr_id
                        AND  coem_tipo  = '$tipos'";

            $res = $dbo->ExecuteQuery($query);

            if (0 == $res['status']) {
                Flight::json(array("status" => 0, "error" => $res['error']));
            }
        }
    }
    Flight::json($out);
});
?>

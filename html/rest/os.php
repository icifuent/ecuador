<?php

Flight::set('OS_MODU_ID', 1);
Flight::set('flight.log_errors', true);
/* HELPERS */
Flight::map('ObtenerDetalleOS', function($db,$cont_id,$orse_id){
	$query = "SELECT 
    os.orse_id,
    os.cont_id,
    os.empr_id,
    os.empl_id,
    os.sube_id,
    os.orse_folio,
    os.orse_tipo,
    os.orse_descripcion,
    os.orse_indisponibilidad,
    os.orse_fecha_creacion,
    os.orse_fecha_solicitud,
    os.orse_pagado,
    os.orse_estado,
    os.usua_creador,
    os.orse_responsable,
    os.orse_solicitud_cambio,
    os.orse_solicitud_informe_web,
    os.usua_validador,
    os.orse_fecha_validacion,
    orse_comentario,
    os.orse_tag,
    os.orse_sla_incluida,
    os.orse_pago_relacionado,
    os.caaf_aper_id,
    os.caaf_cier_id,
    emp.empl_id,
    emp.comu_id,
    emp.clas_id,
    emp.cate_id,
    emp.duto_id,
    emp.empl_nemonico,
    emp.empl_nombre,
    emp.empl_direccion,
    emp.empl_referencia,
    emp.empl_tipo,
    emp.empl_tipo_acceso,
    emp.empl_nivel_criticidad,
    emp.empl_espacio,
    emp.empl_macrositio,
    emp.empl_subtel,
    emp.empl_distancia,
    emp.empl_latitud,
    emp.empl_longitud,
    emp.empl_observacion,
    emp.empl_id_emplazamiento_atix,
    emp.empl_observacion_ingreso,
    emp.empl_fecha_creacion,
    emp.empl_estado,
    emp.empl_subestado,
    emp.usua_creador,
    emp.riva_id,
    empcl.clas_nombre,
    e.empr_nombre,
    sesp.sube_nombre,
    esp.espe_nombre,
    c.comu_nombre,
    z.zona_id,
    z.zona_nombre,
    r.regi_id,
    r.regi_nombre,
    uc.usua_nombre AS usua_creador_nombre,
    IFNULL((SELECT 
				rod.defa_id
			FROM
				rel_orden_denuncia rod
			WHERE
				rod.orse_id = os.orse_id),
		'') AS defa_id,
    IFNULL((SELECT 
				usua_nombre
			FROM
				usuario
			WHERE
				usuario.usua_id = os.usua_validador),
		'') AS usua_validador_nombre,
    ( SELECT GROUP_CONCAT(t.tecn_nombre)
			FROM	tecnologia t, 
				rel_emplazamiento_tecnologia ret
			WHERE ret.empl_id=emp.empl_id
			AND ret.tecn_id=t.tecn_id
			GROUP BY ret.empl_id )  AS tecn_nombre
FROM
    orden_servicio os,
    empresa e,
    subespecialidad sesp,
    especialidad esp,
    emplazamiento emp,
    emplazamiento_clasificacion empcl,
    comuna c,
    region r,
    provincia p,
    zona z,
    rel_zona_emplazamiento rze,
    usuario uc
WHERE
    os.cont_id = $cont_id 
	AND os.orse_id = $orse_id
	AND os.empr_id = e.empr_id
	AND os.empl_id = emp.empl_id
	AND os.sube_id = sesp.sube_id
	AND sesp.espe_id = esp.espe_id
	AND emp.clas_id = empcl.clas_id
	AND emp.comu_id = c.comu_id
	AND p.prov_id = c.prov_id
	AND r.regi_id = p.regi_id
	AND rze.empl_id = emp.empl_id
	AND rze.zona_id = z.zona_id
	AND z.cont_id = os.cont_id
	AND z.zona_tipo = 'CONTRATO'
	AND uc.usua_id = os.usua_creador";
	
	$res = $db->ExecuteQuery($query);

	if (0 == $res['status']){
        return $res;
    }
	
	if(0 < $res['rows']){
		$res_repo = $db->ExecuteQuery("SELECT * FROM repositorio r WHERE r.repo_tabla_id = $orse_id AND r.repo_tabla = 'orden_servicio';");
		if ($res_repo['status'] == 0){
			return $res_repo;
		}
		$res['data'][0]['archivos'] = $res_repo['data'];
		$res_resp = $db->ExecuteQuery("SELECT DISTINCT
											   usuario.usua_id,
											   usuario.usua_nombre,
											   perf_nombre
									   FROM 
										 orden_servicio
									   INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=orden_servicio.empl_id)
									   INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
									   INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
									   INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id  AND usuario.usua_estado='ACTIVO')
									   INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
									   INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND (perfil.perf_nombre='GESTOR' OR (perfil.perf_nombre='DESPACHADOR' AND usuario.empr_id=orden_servicio.empr_id)))
									   WHERE
											orse_id=$orse_id
		");

		if ($res_resp['status'] == 0){
			return $res_resp;
		}

		$res['data'][0]['gestor_responsable'] = array();
		$res['data'][0]['despachador_responsable'] = array();
		foreach($res_resp['data'] AS $row){
			if($row['perf_nombre'] == "GESTOR"){
				array_push($res['data'][0]['gestor_responsable'],$row['usua_nombre']);
			}
			else{
				array_push($res['data'][0]['despachador_responsable'],$row['usua_nombre']);
			}
		}
	}
/* Encargado emplazamiento  os*/

	if(0 < $res['rows']){
		$res_repo = $db->ExecuteQuery("SELECT * FROM repositorio r WHERE r.repo_tabla_id = $orse_id AND r.repo_tabla = 'orden_servicio';");
		if ($res_repo['status'] == 0){
			return $res_repo;
		}
		$res['data'][0]['archivos'] = $res_repo['data'];
		$res_resp= array();
		$res_resp = $db->ExecuteQuery("SELECT 
											  (select u.usua_nombre
											  from rel_zona_usuario rezu,
												usuario u,
												zona zo
												where rezu.zona_id = z.zona_id
												AND rezu.zona_id = zo.zona_id
												AND zo.zona_tipo = 'CLUSTER'
												AND zo.cont_id = orse.cont_id
												AND rezu.usua_id = u.usua_id
												AND u.usua_estado = 'ACTIVO'
												group by zo.zona_id 
												order by u.usua_nombre DESC 
											) AS usuario

											FROM orden_servicio orse 
											,rel_zona_emplazamiento  rempl 
											,zona z
											WHERE orse.orse_id = $orse_id
											AND orse.empl_id = rempl.empl_id 
											AND rempl.zona_id = z.zona_id
											AND z.zona_tipo = 'CLUSTER'
											AND orse.cont_id = z.cont_id;
		");

		if ($res_resp['status'] == 0){
			return $res_resp;
		}

		/* $res['encargado_emplazamiento'] = $res_resp['data'];*/

		if($res_resp['rows']>0){
			$res['data'][0]['encargado_emplazamiento'] = $res_resp['data'][0]['usuario'];
		}else{
			$res['data'][0]['encargado_emplazamiento'] = "";
		}
	}

	return $res;
});

/*Obtiene las ordenes de servicio de la OS bandeja  Felipe Azabache */
Flight::map('ObtenerListaOS', function($db,$cont_id,$filtros,$pagina=null){
  	$results_by_page = Flight::get('results_by_page');

  	$usua_id     = $_SESSION['user_id']; 
  	$empr_id     = $_SESSION['empr_id'];
  	$filtros_ini = $filtros;
  	$filtros     = "";
  	$pre_filtro  = "";
  	$post_filtro = "";
  	$filtros_tec = "";

	/*    $res = $dbo->ExecuteQuery("SELECT e.empr_id
				FROM rel_contrato_usuario rcu, 
				empresa e, 
				rel_contrato_empresa rel
				WHERE rcu.cont_id =$id
				AND rcu.usua_id=$usua_id
				AND rel.cont_id= rcu.cont_id
				AND rel.empr_id = e.empr_id
				AND e.empr_estado = 'ACTIVO'");
				
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$empr_id = $res['data'][0]['empr_id']; */


	if(isset($filtros_ini['orse_id']) && "" != $filtros_ini['orse_id']){
		$filtros .= "AND orden_servicio.orse_id = '".$filtros_ini['orse_id']."' ";
	} else {
		if(isset($filtros_ini['orse_tipo']) && "" != $filtros_ini['orse_tipo']){
			$filtros .= "AND orden_servicio.orse_tipo = '".$filtros_ini['orse_tipo']."' ";
		}

		if( isset($filtros_ini['orse_fecha_solicitud_inicio']) &&  "" != $filtros_ini['orse_fecha_solicitud_inicio']){
			$filtros .= " AND orden_servicio.orse_fecha_solicitud >= '".$filtros_ini['orse_fecha_solicitud_inicio']." 00:00:00' "    ; 
		}
	
		if( isset($filtros_ini['orse_fecha_solicitud_termino']) &&  "" != $filtros_ini['orse_fecha_solicitud_termino']){
			$filtros .= " AND orden_servicio.orse_fecha_solicitud <= '".$filtros_ini['orse_fecha_solicitud_termino']." 23:59:59' "    ; 
		}

		if( isset($filtros_ini['orse_estado']) && "" != $filtros_ini['orse_estado']){
		  	if(is_array($filtros_ini['orse_estado'])){ 
			  	$estados = implode("','",$filtros_ini['orse_estado']);
		  	}else{
			  	$estados = $filtros_ini['orse_estado'];
		  	}
		  
		  	$filtros .= " AND orden_servicio.orse_estado IN ('".$estados."') ";
		}

		if( isset($filtros_ini['orse_responsable']) && "" != $filtros_ini['orse_responsable'] ){
		  	$filtros .= "AND orden_servicio.orse_responsable = '".$filtros_ini['orse_responsable']."' ";
		}

		if( isset($filtros_ini['empl_nemonico']) && "" != $filtros_ini['empl_nemonico'] ){
		  	$filtros .= "AND emplazamiento.empl_nemonico LIKE '%".$filtros_ini['empl_nemonico']."%' ";
		}

		if( isset($filtros_ini['empl_nombre']) && "" != $filtros_ini['empl_nombre']){
		  	$filtros .= "AND emplazamiento.empl_nombre LIKE '%".$filtros_ini['empl_nombre']."%' ";
		}

		if( isset($filtros_ini['tecn_id']) && "" != $filtros_ini['tecn_id'] ){
		  /*
		  if(is_array($filtros_ini['tecn_id'])){
			  $filtros .= "AND (";
			  $filtros .= $filtros_ini['tecn_id'][0]."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
			  for($i=1;$i<count($filtros_ini['tecn_id']);$i++){
				$filtros .= " OR ".$filtros_ini['tecn_id'][$i]."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
			  }
			  $filtros .= ") ";
		  }
		  else{
		  */

			/* USAR
			  #AND emplazamiento.empl_id IN (
						  
					#   SELECT empl_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.tecn_id in (4,3) 
						   #       AND rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id
			  #)
			*/

			if(is_array($filtros_ini['tecn_id'])){ 
			  	$filtros_tec="AND emplazamiento.empl_id IN (
									SELECT empl_id 
									FROM rel_emplazamiento_tecnologia 
									WHERE rel_emplazamiento_tecnologia.tecn_id in (" .implode(",",$filtros_ini['tecn_id']) .") 
									AND rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id)
			  ";
			 /* $filtros_tec .= "AND (".implode("','",$filtros_ini['tecn_id']).") IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";*/
			}else{
			  	$filtros_tec="AND emplazamiento.empl_id IN (
									SELECT empl_id 
									FROM rel_emplazamiento_tecnologia 
									WHERE rel_emplazamiento_tecnologia.tecn_id = " .$filtros_ini['tecn_id'] ." 
									AND rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id)
			  ";

			 /* $filtros_tec .= "AND '".$filtros_ini['tecn_id']."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";*/
			}

			/*$filtros .= "AND '".$filtros_ini['tecn_id']."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
		  }*/
		}

		if( isset($filtros_ini['empl_direccion']) &&  "" != $filtros_ini['empl_direccion']  ){
		  	$filtros .= "AND emplazamiento.empl_direccion LIKE '%".$filtros_ini['empl_direccion']."%' ";
		}

		if(isset($filtros_ini['zona_id']) && "" != $filtros_ini['zona_id']){
			$filtros .= "AND '".$filtros_ini['zona_id']."' IN (SELECT
																	zona.zona_id
															   	FROM 
																	rel_zona_emplazamiento
															   	INNER JOIN zona ON (zona.zona_id=rel_zona_emplazamiento.zona_id AND zona.zona_tipo='CONTRATO')
															   	WHERE rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)";
		}

		if(isset($filtros_ini['clus_id']) && "" != $filtros_ini['clus_id']){
			$filtros .= "AND '".$filtros_ini['clus_id']."' IN (SELECT
																	zona.zona_id
															   	FROM 
																	rel_zona_emplazamiento
															   	INNER JOIN zona ON (zona.zona_id=rel_zona_emplazamiento.zona_id AND zona.zona_tipo='CLUSTER')
															   	WHERE rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)";
		}

		if( isset($filtros_ini['regi_id']) && "" != $filtros_ini['regi_id'] ){
		  	$filtros .= "AND '".$filtros_ini['regi_id']."' IN 	(SELECT
																	region.regi_id
															 	FROM 
																	comuna
															 	INNER JOIN provincia ON (provincia.prov_id=comuna.prov_id)
															 	INNER JOIN region ON (region.regi_id=provincia.regi_id) 
															 	WHERE comuna.comu_id=emplazamiento.comu_id)";
		}

		if( isset($filtros_ini['usua_creador']) && "" != $filtros_ini['usua_creador'] ){
		  	$filtros .= "AND usuario_creador.usua_id = '".$filtros_ini['usua_creador']."'";
		}

		if( isset($filtros_ini['zona_movistar_id']) && "" != $filtros_ini['zona_movistar_id'] ){
		  	$filtros .= "AND '".$filtros_ini['zona_movistar_id']."'IN (SELECT
																		zona.zona_id
															   		FROM 
																	rel_zona_emplazamiento
															   		INNER JOIN zona ON (zona.zona_id=rel_zona_emplazamiento.zona_id AND zona.zona_tipo='MOVISTAR')
															   		WHERE rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)";
		}

  	}

	$SQL_CALC_FOUND_ROWS = "";
	if(null  != $pagina){
		if( "" != $pre_filtro){
			str_replace("SELECT","SELECT SQL_CALC_FOUND_ROWS",$pre_filtro);
		}else{
			$SQL_CALC_FOUND_ROWS = "SQL_CALC_FOUND_ROWS";
		}
		$post_filtro .= " LIMIT ".$results_by_page." OFFSET ".(($pagina-1)*$results_by_page);
	}

	$query = $pre_filtro;

	$usuario_tipo = $db->ExecuteQuery("
									SELECT rcem.coem_tipo
									FROM rel_contrato_empresa rcem 
										, usuario u
									WHERE 
										rcem.coem_tipo='CLIENTE'
									AND rcem.cont_id =$cont_id
									AND rcem.coem_estado = 'ACTIVO' 
									AND rcem.empr_id =u.empr_id
									AND u.usua_id = $usua_id ;
	");
	if (0 == $usuario_tipo['status'] ){ 
		Flight::json(array("status" => 0, "error" => $usuario_tipo['error']));
	}
	if(0 == $usuario_tipo['rows']){

		$filtros .= " AND orden_servicio.empr_id = $empr_id ";
	}
	/*$query.= "SELECT $SQL_CALC_FOUND_ROWS 
				orden_servicio.orse_id
				,orden_servicio.cont_id
				,orden_servicio.empr_id
				,orden_servicio.empl_id
				,orden_servicio.sube_id
				,orden_servicio.orse_folio
				,orden_servicio.orse_tipo
				,orden_servicio.orse_descripcion
				,orden_servicio.orse_indisponibilidad
				,orden_servicio.orse_fecha_creacion
				,orden_servicio.orse_fecha_solicitud
				,orden_servicio.orse_pagado
				,orden_servicio.orse_estado
				,orden_servicio.usua_creador
				,orden_servicio.orse_responsable
				,orden_servicio.orse_solicitud_cambio
				,orden_servicio.orse_solicitud_informe_web
				,orden_servicio.usua_validador
				,orden_servicio.orse_fecha_validacion
				,orden_servicio.orse_comentario
				,orden_servicio.orse_tag
				,orden_servicio.orse_sla_incluida 
				,subespecialidad.sube_nombre 
				,emplazamiento.empl_nombre 
				,emplazamiento.empl_nemonico 
				,emplazamiento.empl_direccion                       
			FROM 
			contrato
			INNER JOIN orden_servicio ON (orden_servicio.cont_id = contrato.cont_id)
			INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
			INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
			INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)              
			INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
			INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
			INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)
			INNER JOIN zona AS zona_alcance ON (rel_zona_emplazamiento.zona_id = zona_alcance.zona_id AND zona_alcance.zona_tipo='ALCANCE')
			INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.cont_id=contrato.cont_id AND rel_contrato_usuario.usua_id=$usua_id)
			INNER JOIN rel_contrato_usuario_alcance ON (rel_contrato_usuario_alcance.recu_id = rel_contrato_usuario.recu_id AND rel_contrato_usuario_alcance.zona_id=zona_alcance.zona_id)   
								  
			WHERE contrato.cont_id=$cont_id
				  AND ( orden_servicio.orse_tipo <> 'OSI' OR usuario_creador.empr_id = $empr_id )
			$filtros
			ORDER BY orden_servicio.orse_fecha_solicitud DESC";*/
	$query.= " SELECT SQL_CALC_FOUND_ROWS 
									  orden_servicio.orse_id
									  ,orden_servicio.cont_id
									  ,orden_servicio.empr_id
									  ,orden_servicio.empl_id
									  ,orden_servicio.sube_id
									  ,orden_servicio.orse_folio
									  ,orden_servicio.orse_tipo
									  ,orden_servicio.orse_descripcion
									  ,orden_servicio.orse_indisponibilidad
									  ,orden_servicio.orse_fecha_creacion
									  ,orden_servicio.orse_fecha_solicitud
									  ,orden_servicio.orse_pagado
									  ,orden_servicio.orse_estado
									  ,orden_servicio.usua_creador
									  ,orden_servicio.orse_responsable
									  ,orden_servicio.orse_solicitud_cambio
									  ,orden_servicio.orse_solicitud_informe_web
									  ,orden_servicio.usua_validador
									  ,orden_servicio.orse_fecha_validacion
									  ,orden_servicio.orse_comentario
									  ,orden_servicio.orse_tag
									  ,orden_servicio.orse_sla_incluida
									  ,subespecialidad.sube_nombre
									  ,emplazamiento.empl_nombre
									  ,emplazamiento.empl_nemonico
									  ,emplazamiento.empl_direccion
									  ,especialidad.espe_id
									  
			FROM
				  contrato
			INNER JOIN orden_servicio ON (orden_servicio.cont_id = contrato.cont_id)
			INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
			INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)
			INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)
			INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
			INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)
			AND emplazamiento.empl_id IN (  SELECT DISTINCT  r.empl_id
											FROM  rel_contrato_emplazamiento r
												  , rel_zona_emplazamiento z
												  , zona x
												  , rel_contrato_usuario
												  , rel_contrato_usuario_alcance
											WHERE r.cont_id = $cont_id     
											  AND z.empl_id = r.empl_id
											  AND x.zona_id =  z.zona_id 
											  AND x.zona_tipo='ALCANCE'  
											  AND x.cont_id = $cont_id
											  AND rel_contrato_usuario.cont_id= x.cont_id 
											  AND rel_contrato_usuario.usua_id= $usua_id
											  AND rel_contrato_usuario_alcance.recu_id = rel_contrato_usuario.recu_id 
											  AND rel_contrato_usuario_alcance.zona_id=x.zona_id 
			)
			$filtros_tec
			WHERE contrato.cont_id=$cont_id
				  AND ( orden_servicio.orse_tipo <> 'OSI' OR usuario_creador.empr_id = $empr_id )
				  $filtros
			ORDER BY orden_servicio.orse_fecha_solicitud DESC

	";
	$query.= $post_filtro;

	$res = $db->ExecuteQuery($query);
	if(!$res['status']){
		return $res;
	}
	$out['status']  = true;
	$out['ordenes'] = $res['data'];

	if(null != $pagina){
		$res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
		if(!$res['status']){
			return $res;
		}
		$out['pagina'] = intval($pagina);
		$out['total'] = intval($res['data'][0]['total']);
		$out['paginas'] = ceil($out['total']/$results_by_page);
	}

	return $out;
});



Flight::map('ObtenerListaOSExport', function($db,$cont_id,$filtros,$pagina=null){
  	$results_by_page = Flight::get('results_by_page');

  	$usua_id     = $_SESSION['user_id']; 
  	$empr_id     = $_SESSION['empr_id'];
  	$filtros_ini = $filtros;
  	$filtros     = "";
  	$pre_filtro  = "";
  	$post_filtro = "";


  	if(isset($filtros_ini['orse_id']) && $filtros_ini['orse_id']!=""){
		$filtros .= "AND orse_id = '".$filtros_ini['orse_id']."' ";
  	}else{
		if(isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!=""){
		  	$filtros .= "AND orse_tipo = '".$filtros_ini['orse_tipo']."' ";
		}
		if( isset($filtros_ini['orse_fecha_solicitud_inicio']) && $filtros_ini['orse_fecha_solicitud_inicio']!= ""){
			$filtros .= " AND orse_fecha_solicitud >= '".$filtros_ini['orse_fecha_solicitud_inicio']." 00:00:00' "    ; 
		}
		if( isset($filtros_ini['orse_fecha_solicitud_termino']) && $filtros_ini['orse_fecha_solicitud_termino']!= ""){
			$filtros .= " AND orse_fecha_solicitud <= '".$filtros_ini['orse_fecha_solicitud_termino']." 23:59:59' "    ; 
		}




		if( isset($filtros_ini['orse_estado']) && $filtros_ini['orse_estado']!=""){
		  	if(is_array($filtros_ini['orse_estado'])){ 
		  		$estados = implode("','",$filtros_ini['orse_estado']);
		  	}else{
			  	$estados = $filtros_ini['orse_estado'];
		  	}
		  
		  	$filtros .= " AND orse_estado IN ('".$estados."') ";
		}

		if( isset($filtros_ini['orse_responsable']) && $filtros_ini['orse_responsable']!="" ){
		  	$filtros .= "AND orse_responsable = '".$filtros_ini['orse_responsable']."' ";
		}

		if( isset($filtros_ini['empl_nemonico']) && $filtros_ini['empl_nemonico']!="" ){
		  	$filtros .= "AND empl_nemonico LIKE '%".$filtros_ini['empl_nemonico']."%' ";
		}

		if( isset($filtros_ini['empl_nombre']) && $filtros_ini['empl_nombre']!=""){
		  	$filtros .= "AND empl_nombre LIKE '%".$filtros_ini['empl_nombre']."%' ";
		}

		if( isset($filtros_ini['tecn_id']) && $filtros_ini['tecn_id']!="" ){
		  /*
		  if(is_array($filtros_ini['tecn_id'])){
			  $filtros .= "AND (";
			  $filtros .= $filtros_ini['tecn_id'][0]."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
			  for($i=1;$i<count($filtros_ini['tecn_id']);$i++){
				$filtros .= " OR ".$filtros_ini['tecn_id'][$i]."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
			  }
			  $filtros .= ") ";
		  }
		  else{
		  */
			$filtros .= "AND '".$filtros_ini['tecn_id']."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
		  /*}*/
		}

		if( isset($filtros_ini['empl_direccion']) && $filtros_ini['empl_direccion']!="" ){
		  	$filtros .= "AND empl_direccion LIKE '%".$filtros_ini['empl_direccion']."%' ";
		}

		if(isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
			$filtros .= "AND '".$filtros_ini['zona_id']."' IN (SELECT
																zona.zona_id
															   FROM 
																rel_zona_emplazamiento
															   INNER JOIN zona ON (zona.zona_id=rel_zona_emplazamiento.zona_id AND zona.zona_tipo='CONTRATO')
															   WHERE rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)";
		}

		if(isset($filtros_ini['clus_id']) && $filtros_ini['clus_id']!=""){
			$filtros .= "AND '".$filtros_ini['clus_id']."' IN (SELECT
																zona.zona_id
															   FROM 
																rel_zona_emplazamiento
															   INNER JOIN zona ON (zona.zona_id=rel_zona_emplazamiento.zona_id AND zona.zona_tipo='CLUSTER')
															   WHERE rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)";
		}

		if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!="" ){
		  	$filtros .= "AND '".$filtros_ini['regi_id']."' IN (SELECT
																region.regi_id
															 FROM 
																comuna
															 INNER JOIN provincia ON (provincia.prov_id=comuna.prov_id)
															 INNER JOIN region ON (region.regi_id=provincia.regi_id) 
															 WHERE comuna.comu_id=emplazamiento.comu_id)";
		}

		if( isset($filtros_ini['usua_creador']) && $filtros_ini['usua_creador']!="" ){
		  	$filtros .= "AND usua_creador = '".$filtros_ini['usua_creador']."'";
		}
  	}

  	$SQL_CALC_FOUND_ROWS = "";
  	if($pagina!=null){
		if($pre_filtro!=""){
			str_replace("SELECT","SELECT SQL_CALC_FOUND_ROWS",$pre_filtro);
		}else{
			$SQL_CALC_FOUND_ROWS = "SQL_CALC_FOUND_ROWS";
	}

	$post_filtro .= " LIMIT ".$results_by_page." OFFSET ".(($pagina-1)*$results_by_page);
  }

  $query = $pre_filtro;
  $query.= "SELECT $SQL_CALC_FOUND_ROWS 
					os_consolidado.*       
			FROM os_consolidado
			WHERE cont_id=$cont_id
			$filtros
			ORDER BY orse_fecha_solicitud DESC";
	$query.= $post_filtro;

	$res = $db->ExecuteQuery($query);
	if(!$res['status']){
		return $res;
	}
	$out['status']  = true;
	$out['ordenes'] = $res['data'];

	if($pagina!=null){
		$res = $db->ExecuteQuery("SELECT FOUND_ROWS() as total");
		if(!$res['status']){
			return $res;
		}
		$out['pagina'] = intval($pagina);
		$out['total'] = intval($res['data'][0]['total']);
		$out['paginas'] = ceil($out['total']/$results_by_page);
	}

	return $out;
});

/*Bandeja  */
Flight::route('GET /contrato/@id:[0-9]+/os/bandeja/filtros', function($id){
	$out = array();
	$out['status'] = 1;
	$dbo = new MySQL_Database();

	$res = $dbo->ExecuteQuery("SELECT 
									zona_id, zona_nombre 
								FROM zona 
								WHERE cont_id=" . $id . " 
								AND zona_estado = 'ACTIVO' 
								AND zona_tipo='CONTRATO'"
	);
	if ($res['status'] == 0) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	$out['zonas'] = $res['data'];

	$res = $dbo->ExecuteQuery("SELECT 
									zona_id
									,zona_nombre 
								FROM zona WHERE cont_id=" . $id . " 
								AND zona_estado = 'ACTIVO' 
								AND zona_tipo='CLUSTER'
	");
	if ($res['status'] == 0) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	$out['clusters'] = $res['data'];

	$res = $dbo->ExecuteQuery("SELECT pais_id from contrato WHERE cont_id=$id");
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$pais_id = $res['data'][0]['pais_id']; 


	$res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=$pais_id ORDER BY regi_orden ASC");
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$out['regiones'] = $res['data'];

	$res = $dbo->ExecuteQuery("SELECT tecn_id, tecn_nombre FROM tecnologia");
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$out['tecnologias'] = $res['data'];
	
	$res = $dbo->ExecuteQuery("SELECT sube.sube_id, espe.espe_nombre 
								FROM especialidad espe
									, subespecialidad sube
								WHERE espe.espe_id = sube.espe_id 
								AND espe.cont_id=$id");
	if ($res['status'] == 0) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	$out['especialidades'] = $res['data'];
	
	$res = $dbo->ExecuteQuery( "SHOW COLUMNS 
								FROM orden_servicio 
								WHERE Field = 'orse_tipo'" 
	);
	if ($res['status'] == 0) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
	$out['tipos'] = explode("','", $matches[1]); 

	$res = $dbo->ExecuteQuery("SELECT zona_id AS zona_movistar_id
									, zona_nombre AS zona_movistar_nombre 
								FROM zona 
								WHERE cont_id=" . $id . "
								AND zona_estado = 'ACTIVO' 
								AND zona_tipo='MOVISTAR'"
	);
	if ($res['status'] == 0){
	 Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	$out['zona_movistar'] = $res['data'];

	/*Si la empresa del usuario no es CLIENTE en el contrato actual, se elimina de los tipos la OSI*/
	$empr_id     = $_SESSION['empr_id'];  

	/*    $res = $dbo->ExecuteQuery("SELECT e.empr_id
				FROM rel_contrato_usuario rcu, 
				empresa e, 
				rel_contrato_empresa rel
				WHERE rcu.cont_id =$id
				AND rcu.usua_id=$usua_id
				AND rel.cont_id= rcu.cont_id
				AND rel.empr_id = e.empr_id
				AND e.empr_estado = 'ACTIVO'");
				
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$empr_id = $res['data'][0]['empr_id'];  */

	$res = $dbo->ExecuteQuery("	SELECT coem_tipo 
							  	FROM rel_contrato_empresa 
							  	WHERE cont_id=$id
							   	AND coem_estado = 'ACTIVO'
							   	AND empr_id=$empr_id"
	);
	if ($res['status'] == 0) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	if( count($res['data'])==0 ) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	if( $res['data'][0]['coem_tipo']!='CLIENTE' ){
		$out['tipos'] = array_diff($out['tipos'], array('OSI'));
	}
	
	$res = $dbo->ExecuteQuery("SELECT usuario.usua_id, UPPER(usua_nombre)  AS usua_nombre
							  FROM orden_servicio
							  INNER JOIN usuario ON (orden_servicio.usua_creador = usuario.usua_id)
							  WHERE cont_id=$id
							  GROUP BY usuario.usua_id
							  ORDER BY usua_nombre");
	if ($res['status'] == 0) {
		Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	}
	$out['usuarios_creador'] = $res['data'];

	Flight::json($out);
});

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/os/list(/@page:[0-9]+)', function($cont_id,$page){

	$usua_id = $_SESSION['user_id'];
	$db = new MySQL_Database();
	$filtros = array_merge($_GET,$_POST);
	$res = Flight::ObtenerListaOS($db,$cont_id,$filtros,$page);
	if ($res['status'] == 0){
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}
	$out = $res;

	foreach($out['ordenes'] as &$orden) {
		$orse_id = $orden['orse_id'];

		/* TAREAS */
		$res = $db->ExecuteQuery("SELECT usua_id,perf_nombre 
								  FROM perfil, rel_usuario_perfil 
								  WHERE perfil.perf_id= rel_usuario_perfil.perf_id 
									AND usua_id=$usua_id
									AND perf_nombre='SUPERVISOR'");
		if ($res['status'] == 0){
			Flight::json(array("status" => 0, "error" => $res['error']));
			return;
		}

		$query="";
		if($res['rows']>0){
			$query = ("SELECT
											tare_id
											,usua_id
											,tare_modulo
											,tare_tipo
											,tare_id_relacionado
											,tare_fecha_despacho
											,tare_fecha_descarga
											,tare_data
											,tare_estado
									  FROM
										  tarea
									  WHERE 
											tare_modulo='OS' 
											AND tare_estado IN ('CREADA','DESPACHADA') 
											AND tare_id_relacionado = $orse_id
											AND tare_tipo in ('VALIDAR_PRESUPUESTO', 'VALIDAR_SOLICITUD_CAMBIO', 'VALIDAR_SOLICITUD_INFORME', 'VALIDAR_OS', 'VALIDAR_INFORME')
									  GROUP BY tare_tipo
									  ORDER BY tare_fecha_despacho DESC
			");
		} else {
		  $query = ("SELECT
										  tare_id
										  ,usua_id
										  ,tare_modulo
										  ,tare_tipo
										  ,tare_id_relacionado
										  ,tare_fecha_despacho
										  ,tare_fecha_descarga
										  ,tare_data
										  ,tare_estado
									FROM
										tarea
									WHERE 
										  tare_modulo='OS' 
										  AND tare_estado IN ('CREADA','DESPACHADA') 
										  AND tare_id_relacionado = $orse_id
										  AND usua_id=$usua_id
									GROUP BY tare_tipo
									ORDER BY tare_fecha_despacho DESC
		");

		}

		$res = $db->ExecuteQuery($query);
		if ($res['status'] == 0){
			Flight::json(array("status" => 0, "error" => $res['error']));
			return;
		}
		if(0<$res['rows']){
			$orden['tarea'] = $res['data'];
		}

		//Agregar presupuesto asociado
		$res = $db->ExecuteQuery("SELECT
										pres_id
									FROM 
									presupuesto
									WHERE orse_id=$orse_id 
									AND pres_estado!='RECHAZADO'
									ORDER BY pres_id DESC
									LIMIT 1
		");
		if ($res['status'] == 0){
			Flight::json(array("status" => 0, "error" => $res['error']));
			return;
		}
		if(0<$res['rows']){
			$orden['pres_id'] = $res['data'][0]['pres_id'];
		}
	}
	
	$out['filtros'] = $filtros;
	Flight::json($out);
});

/*Crear*/
Flight::route('GET /contrato/@id:[0-9]+/os/add/filtros', function($id){
	try{
		$modu_id = Flight::get('OS_MODU_ID');

		$out = array();
		$out['status'] = 1;
		$dbo = new MySQL_Database();
		$dbo->startTransaction();

		$query = "SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CONTRATO'";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$out['zonas'] = $res['data'];

		$query = "SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CLUSTER'";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$out['clusters'] = $res['data'];

		$query = "SELECT pais_id from contrato WHERE cont_id=$id";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$pais_id = $res['data'][0]['pais_id'];

		$query = "SELECT regi_id, regi_nombre FROM region WHERE pais_id=$pais_id ORDER BY regi_orden ASC";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$out['regiones'] = $res['data'];

		$query = "SELECT tecn_id, tecn_nombre FROM tecnologia";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$out['tecnologias'] = $res['data'];

		$query = "SELECT * FROM orden_servicio_definicion_formulario osf,formulario f WHERE f.form_id=osf.form_id AND osf.cont_id=$id AND f.form_estado='ACTIVO'";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$out['def_formulario'] = $res['data'];

		$query = "
	        SELECT
	            f.form_id, f.form_nombre, refe.espe_id, refe.refe_preseleccion
	        FROM rel_modulo_especialidad reme
		        INNER JOIN rel_formulario_especialidad refe ON(refe.espe_id = reme.espe_id)
		        INNER JOIN formulario f ON(refe.form_id=f.form_id AND f.cont_id=$id AND f.form_estado='ACTIVO' )
		        INNER JOIN modulo M on M.modu_id = reme.modu_id
	        WHERE
	        	M.modu_alias = 'OS'"
	     	;
		$res=$dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		$out['formularios'] = $res['data'];

		$query = "SELECT e.empr_id, e.empr_nombre, rce.coem_tipo "
								. "FROM empresa e, rel_contrato_empresa rce "
								. "WHERE "
									. "rce.cont_id=" . $id . " "
									. "AND rce.coem_tipo='CONTRATISTA' "
									. "AND e.empr_estado='ACTIVO' "
									. "AND e.empr_id=rce.empr_id
									  ORDER BY e.empr_id DESC";
		$res = $dbo->ExecuteQuery($query);
		if( 0==$res['status'] ) throw new Exception($res['error'].". Query => ".$query);
		$out['empresa'] = $res['data'];

		$query = "
			SELECT 
			  especialidad.espe_id, espe_nombre 
			FROM especialidad 
			INNER JOIN rel_modulo_especialidad ON (rel_modulo_especialidad.espe_id=especialidad.espe_id AND 
												   rel_modulo_especialidad.modu_id=$modu_id)
			WHERE espe_estado='ACTIVO'
						and cont_id=$id";
		$res = $dbo->ExecuteQuery($query);
		if( 0==$res['status'] ) throw new Exception($res['error'].". Query => ".$query);
		$out['especialidad'] = $res['data'];

		$query = "
			SELECT 
				A.sube_id
				,A.sube_nombre
				,B.espe_id 
			FROM 
				subespecialidad A
				INNER JOIN especialidad B ON (B.espe_id=A.espe_id)
				INNER JOIN rel_modulo_especialidad C ON (C.espe_id=B.espe_id)
				INNER JOIN modulo D ON D.modu_id = C.modu_id
			WHERE
				B.cont_id = $id
				AND D.modu_alias = 'OS'
				AND B.espe_estado='ACTIVO' 
				AND A.sube_estado='ACTIVO'"
			;
		$res = $dbo->ExecuteQuery($query);

		if( 0==$res['status'] ) throw new Exception($res['error'].". Query => ".$query);
		$out['subespecialidad'] = $res['data'];
		
		$query =  "SHOW COLUMNS FROM orden_servicio WHERE Field = 'orse_tipo'" ;
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
		$out['tipos'] = explode("','", $matches[1]); 
		
		  $usua_id = $_SESSION['user_id'];
		
		$query = "
			SELECT e.empr_id
			FROM rel_contrato_usuario rcu, 
			empresa e, 
			rel_contrato_empresa rel
			WHERE rcu.cont_id =$id
			AND rcu.usua_id=$usua_id
			AND rel.cont_id= rcu.cont_id
			AND rel.empr_id = e.empr_id
			AND e.empr_estado = 'ACTIVO'";
		  $res = $dbo->ExecuteQuery($query);
									
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
			$empr_id = $res['data'][0]['empr_id'];					

		$usua_id           = $_SESSION['user_id'];				

		/*Si la empresa del usuario no es CLIENTE en el contrato actual, se elimina de los tipos la OSI
		$empr_id     = $_SESSION['empr_id'];    */
		$query = "SELECT coem_tipo FROM rel_contrato_empresa WHERE cont_id=$id AND empr_id=$empr_id";
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) throw new Exception($res['error'].". Query => ".$query);
		if( count($res['data'])==0 ) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
		if( $res['data'][0]['coem_tipo']!='CLIENTE' ){
			$out['tipos'] = array_diff($out['tipos'], array('OSI'));
		}

		$out['status'] = 1;
		Flight::json($out);
	} catch ( Exception $e ) {
		GLOBAL $BASEDIR;
        $dbo->Rollback();
        $time = time();
        $output = "\n[".$time."] ".$e."\n";
        error_log($output, 3, $BASEDIR."logs/error_log.log");
        Flight::json(array("status"=>0, "error"=>$time));

	}
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/add', function($cont_id){
	$db = new MySQL_Database();
	$db->startTransaction();
	
	if (isset($_POST['form_id'])) {
	  $form_id                = json_decode($_POST['form_id'],true);
	} else {
	  $form_id=NULL;
	}

	$usua_creador           = $_SESSION['user_id'];
	$empr_id                = mysql_real_escape_string($_POST['empr_id']);
	$empl_id                = mysql_real_escape_string($_POST['empl_id']);
	$sube_id                = mysql_real_escape_string($_POST['sube_id']);
	$orse_tipo              = mysql_real_escape_string($_POST['orse_tipo']);
	$orse_descripcion       = mysql_real_escape_string($_POST['orse_descripcion']);
	$orse_indisponibilidad  = mysql_real_escape_string($_POST['orse_indisponibilidad']);
	$orse_fecha_solicitud   = mysql_real_escape_string($_POST['orse_fecha_solicitud']);
   
	$orse_tag = "";
	if(isset($_POST['orse_tag'])){
		$orse_tag = mysql_real_escape_string($_POST['orse_tag']);
	}

	$query = "INSERT INTO orden_servicio SET 
				cont_id='$cont_id',
				empr_id='$empr_id',
				empl_id='$empl_id',
				sube_id='$sube_id',
				orse_tipo='$orse_tipo',
				orse_descripcion='$orse_descripcion',
				orse_indisponibilidad='$orse_indisponibilidad',
				orse_fecha_solicitud='$orse_fecha_solicitud',
				orse_fecha_creacion=NOW(),
				usua_creador='$usua_creador',
				orse_tag='$orse_tag'";
	$res = $db->ExecuteQuery($query);
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$orse_id = $res['data'][0]['id'];
	if( is_array($form_id)){
	  if(!(is_null($form_id))){
		for($i=0;$i<count($form_id);$i++){
		  $res = $db->ExecuteQuery("INSERT INTO rel_orden_servicio_formulario (orse_id,form_id) VALUES (".$orse_id.",".$form_id[$i].")");
		  if( 0==$res['status'] ){
			$db->Rollback();
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return;
		  }
		}
	  }
	}else{
	  $res = $db->ExecuteQuery("INSERT INTO rel_orden_servicio_formulario (orse_id,form_id) VALUES (".$orse_id.",".$form_id.")");
		  if( 0==$res['status'] ){
			$db->Rollback();
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return;
		  }
	}
	/*
 	Flight::json(array("status"=>0, "error"=>count($form_id)));

	Subida de archivos*/
	if (isset($_FILES["archivos"])) { 
		$files = array(); 
		foreach($_FILES["archivos"] as $key1 => $value1) {
			foreach($value1 as $key2 => $value2) {
				$files[$key2][$key1] = $value2; 
			}
		}
		$descriptions = json_decode($_POST['archivos_descripciones'],true);

		for($i=0; $i<count($files); $i++){
			$nombre      = $files[$i]['name'];
			$descripcion = $descriptions[$i];

			$filename = date('ymdHis')."_os_".$orse_id."_".str_replace(" ","_",$nombre);
			$resFile = Upload::UploadFile($files[$i],$filename); 
			if( !$resFile['status'] ){
				$db->Rollback();
				Flight::json(array("status" => 0, "error" => $resFile['error']));
				return;
			}
			
			$query = "INSERT INTO repositorio (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
						VALUES(
							'DOCUMENTO',
							'".$nombre."',
							'".$descripcion."',
							'orden_servicio',
							$orse_id,
							'".$resFile['data']['filename']."',
							NOW(),
							NULL,
							$usua_creador
					)";
			$resUpload = $db->ExecuteQuery($query);
			if( $resUpload['status']==0 ) {
				$db->Rollback();
				Flight::json(array("status" => 0, "error" => $resUpload['error']));
				return;
			}            
		}
	}

	/*Agregar eventos*/
	$resEvent = Flight::AgregarEvento($db,"OS","CREADA",$orse_id,null);
	if( $resEvent['status']==0 ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$db->Commit();
	
	Flight::json(array("status"=>true,"orse_id"=>$orse_id));
});
/*
Flight::route('GET /contrato/@id:[0-9]+/os/get/@ido:[0-9]+', function($id, $ido){
	$dbo = new MySQL_Database();
	$res['os'] = Flight::ObtenerDetalleOS($dbo,$id,$ido);   
	Flight::json($res);
});
*/

Flight::route('GET /contrato/@id:[0-9]+/os/get/@ido:[0-9]+', function($id, $ido){
	$dbo = new MySQL_Database();
	$modu_id = Flight::get('OS_MODU_ID');
/*select * from orden_servicio os,empresa em,especialidad es,subespecialidad su 
	where os.orse_id=10001 and em.empr_id=os.empr_id and 
	su.sube_id=os.sube_id and su.espe_id=es.espe_id*/

	 $query="
			SELECT os.orse_id
			, os.cont_id
			, os.empr_id
			, os.empl_id
			, os.sube_id 
			, es.espe_id
			, os.orse_folio
			, os.orse_tipo
			, os.orse_descripcion
			, os.orse_indisponibilidad
			, os.orse_fecha_creacion
			, os.orse_fecha_solicitud
			, os.orse_pagado
			, os.orse_estado
			, os.usua_creador
			, os.orse_responsable
			, os.orse_solicitud_cambio
			, os.orse_solicitud_informe_web
			, os.usua_validador
			, os.orse_fecha_validacion
			, os.orse_comentario 
			, os.orse_tag
			, os.orse_sla_incluida
			, em.comu_id
			, em.empr_alias
			,CASE WHEN (orse_estado in('APROBADA','RECHAZADA','ANULADA','NOACTIVO','ASIGNANDO','ASIGNADA') )THEN 'disabled'
			ELSE '' END AS estado_disable
			, em.empr_nombre
			, em.empr_rut
			, em.empr_giro
			, em.empr_direccion
			, em.empr_fecha_creacion
			, em.empr_observacion
			, em.empr_contacto_nombre
			, em.empr_contacto_email
			, em.empr_contacto_telefono_fijo
			, em.empr_contacto_telefono_movil
			, em.empr_contacto_direccion
			, em.empr_estado
			, em.usua_creador
			, es.espe_nombre
			, es.espe_estado
			, es.flag_grafico
			, es.espe_ord
			, su.sube_nombre
			, su.sube_estado 
			FROM orden_servicio os
						,empresa em
						,especialidad es
						,subespecialidad su 
						WHERE os.orse_id=".$ido."
						AND em.empr_id=os.empr_id 
						AND su.sube_id=os.sube_id 
						AND su.espe_id=es.espe_id

	 ";
	 $res=$dbo->ExecuteQuery($query);
	 $out['os']=$res['data'][0];
	 $empl_id=$res['data'][0]['empl_id'];

	 $out['orse_sube_id']= $res['data'][0]['sube_id'];
	 $out['orse_nombre_sube_id']=$res['data'][0]['sube_nombre'];

	 $res= Flight::ObtenerDetalleEmplazamiento($dbo,$id,$empl_id);

	 /* CORREGIR EN EL SIGUIENTE PASO*/
	 $res_resp = $dbo->ExecuteQuery("SELECT 
											  (select u.usua_nombre
											  from rel_zona_usuario rezu,
												usuario u,
												zona zo
												where rezu.zona_id = z.zona_id
												AND rezu.zona_id = zo.zona_id
												AND zo.zona_tipo = 'CLUSTER'
												AND zo.cont_id = orse.cont_id
												AND rezu.usua_id = u.usua_id
												group by zo.zona_id 
												order by u.usua_nombre DESC 
											) AS usuario

											FROM orden_servicio orse 
											,rel_zona_emplazamiento  rempl 
											,zona z
											WHERE orse.orse_id = $ido
											AND orse.empl_id = rempl.empl_id 
											AND rempl.zona_id = z.zona_id
											AND z.zona_tipo = 'CLUSTER'
											AND orse.cont_id = z.cont_id;
		");

		if ($res_resp['status'] == 0){
			return $res_resp;
		}

		/*$res['encargado_emplazamiento'] = $res_resp['data'];*/
		
		$out['emplazamiento']=$res['data'][0];

		if($res_resp['rows']>0){
		  $out['emplazamiento']['encargado_emplazamiento'] = $res_resp['data'][0]['usuario'];
		} else {
		  $out['emplazamiento']['encargado_emplazamiento'] ="";
		}

	/*$res = $dbo->ExecuteQuery("SELECT e.empr_id, e.empr_nombre, rce.coem_tipo "
							. "FROM empresa e, rel_contrato_empresa rce "
							. "WHERE "
								. "rce.cont_id=" . $id . " "
								//. "AND rce.coem_tipo='CONTRATISTA' "
								. "AND e.empr_estado='ACTIVO' "
								. "AND e.empr_id=rce.empr_id"
	);*/

	$res = $dbo->ExecuteQuery("SELECT DISTINCT empr.empr_id AS empr_id, empr.empr_nombre AS empr_nombre 
										FROM empresa empr, rel_contrato_empresa rcem 
										WHERE empr.empr_id=rcem.empr_id 
										AND empr.empr_estado='ACTIVO'
										AND rcem.cont_id=$id
										ORDER BY empr_nombre ASC"
	);

	if( 0==$res['status'] ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
	$out['os']['empresa'] = $res['data'];


	$res = $dbo->ExecuteQuery("SELECT 
								  especialidad.espe_id, espe_nombre 
								FROM especialidad 
								INNER JOIN rel_modulo_especialidad ON (rel_modulo_especialidad.espe_id=especialidad.espe_id AND 
																	   rel_modulo_especialidad.modu_id=$modu_id)
								WHERE espe_estado='ACTIVO'
								AND cont_id=$id");
	if( 0==$res['status'] ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
	$out['os']['especialidad'] = $res['data'];

	$res = $dbo->ExecuteQuery("SELECT 
								  subespecialidad.sube_id, sube_nombre,especialidad.espe_id 
								FROM subespecialidad
								INNER JOIN especialidad ON (especialidad.espe_id=subespecialidad.espe_id)
								INNER JOIN rel_modulo_especialidad ON (rel_modulo_especialidad.espe_id=especialidad.espe_id AND 
																	   rel_modulo_especialidad.modu_id=$modu_id)
								WHERE espe_estado='ACTIVO' AND sube_estado='ACTIVO'
								AND cont_id=$id");
	if( 0==$res['status'] ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
	$out['os']['subespecialidad'] = $res['data'];
	
	$res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM orden_servicio WHERE Field = 'orse_tipo'" );
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
	$out['os']['tipos'] = explode("','", $matches[1]); 

	//Si la empresa del usuario no es CLIENTE en el contrato actual, se elimina de los tipos la OSI
	$empr_id     = $_SESSION['empr_id'];    
	$res = $dbo->ExecuteQuery("SELECT coem_tipo FROM rel_contrato_empresa WHERE cont_id=$id AND empr_id=$empr_id");
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	if( count($res['data'])==0 ) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	if( $res['data'][0]['coem_tipo']!='CLIENTE' ){
		$out['os']['tipos'] = array_diff($out['tipos'], array('OSI'));
	}
   // $res = $dbo->ExecuteQuery("SELECT * FROM orden_servicio_definicion_formulario osf,formulario f WHERE f.form_id=osf.form_id AND osf.cont_id=".$id);
	//select f.form_nombre from rel_orden_servicio_formulario rosf,formulario f where rosf.orse_id=10461 and rosf.form_id=f.form_id 
	  $res=$dbo->ExecuteQuery("SELECT f.form_nombre FROM rel_orden_servicio_formulario rosf,formulario f WHERE rosf.orse_id=".$ido." AND rosf.form_id=f.form_id AND f.form_estado='ACTIVO'");
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$out['os']['def_formulario'] = $res['data'];

	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/upd/@orse_id:[0-9]+', function($cont_id, $orse_id){
	$usua_creador = $_SESSION['user_id'];
	$data = array_merge($_GET,$_POST);
	$orse_especialidad = $data['espe_id'];
	$orse_subespecialidad = $data['sube_id'];
	$orse_empresa = $data['empr_id'];
	$orse_delta = $data['orse_delta_tiempo'];
	
	$orse_id = $data['orse_id'];
	
   
	// $orse_especialidad = $data[0]['espe_id'];

	//$orse_especialidad=$data['espe_id'];

	$dbo = new MySQL_Database();
	/*$asd = "SELECT $orse_especialidad,$orse_subespecialidad,$orse_empresa,$orse_delta,$orse_estado,$orse_id,$orse_fecha,$orse_hora,$orse_fecha_hora from $orse_especialidad";
	  $res = $dbo->ExecuteQuery($asd);*/

	if(isset($data['orse_fecha_programada_fecha']))
	{
		$orse_fecha = $data['orse_fecha_programada_fecha'];
		$orse_hora = $data['orse_fecha_programada_hora'];
		$orse_hora = str_replace(' ', '', $orse_hora);

		$fechaformato = date_create($orse_fecha);
		$orse_fecha = date_format($fechaformato, 'Y-m-d');

		$orse_fecha_hora = $orse_fecha." ".$orse_hora;
	}else
	{
		$orse_fecha_hora= NULL;
	}

	if( isset($data['orse_estado']) ){
		$orse_estado = $data['orse_estado'];

		if ($orse_estado == "0") {

			if( NULL== $orse_fecha_hora)
			{
				$query = "UPDATE orden_servicio SET sube_id=".$orse_subespecialidad.",".
				"empr_id=".$orse_empresa." WHERE orse_id=".$orse_id.";";
			}else{
				$query = "UPDATE orden_servicio SET sube_id=".$orse_subespecialidad.",".
				"empr_id=".$orse_empresa.",".
				"orse_fecha_solicitud='".$orse_fecha_hora."' WHERE orse_id=".$orse_id.";";
			}

		}else {
			$query = "UPDATE orden_servicio SET sube_id=".$orse_subespecialidad.",".
			"empr_id=".$orse_empresa.",orse_estado='".$orse_estado."',".
			"orse_fecha_solicitud='".$orse_fecha_hora."' WHERE orse_id=".$orse_id.";";
		}

	}else{

		 if( NULL== $orse_fecha_hora)
			{
				$query = "UPDATE orden_servicio SET sube_id=".$orse_subespecialidad.",".
				"empr_id=".$orse_empresa." WHERE orse_id=".$orse_id.";";
			}else{
				 $query = "UPDATE orden_servicio SET sube_id=".$orse_subespecialidad.",".
				"empr_id=".$orse_empresa.",".
				"orse_fecha_solicitud='".$orse_fecha_hora."' WHERE orse_id=".$orse_id.";";
			}
		
	}
	  $res = $dbo->ExecuteQuery($query);
		
		if( $res['status']==0 ){
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return;
		}
		Flight::json($res);
	
	/*
	if($orse_estado == "VALIDANDO"){
		$query = "UPDATE orden_servicio SET orse_responsable = 'MOVISTAR' where orse_id = ".$orse_id.";";
	}else{
		$query = "UPDATE orden_servicio SET orse_responsable = 'CONTRATISTA' where orse_id = ".$orse_id.";";
	}

	*/

	//$query = "UPDATE orden_servicio SET ".Flight::dataToUpdateString($data)." WHERE cont_id=".$cont_id." AND orse_id =".$orse_id;
   // Flight::json(array("status" => 0, "error" => $query));
 
	
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/del/@orse_id:[0-9]+', function($cont_id, $orse_id){
	$db = new MySQL_Database();

	$db->startTransaction();
	$res = $db->ExecuteQuery("UPDATE orden_servicio 
								SET orse_estado='NOACTIVO' 
								WHERE cont_id= $cont_id AND orse_id = $orse_id");
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	//tarea
	$query = "UPDATE tarea SET tare_estado='ANULADA' 
			  WHERE tare_modulo='OS' AND tare_id_relacionado = $orse_id";
	$res = $db->ExecuteQuery($query);
	if ($res['status'] == 0) {
		$db->Rollback();
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}

	//notificacion
	$query = "UPDATE notificacion SET noti_estado='ENTREGADA' 
			  WHERE 
					noti_modulo='OS'
					AND noti_estado='DESPACHADA'
					AND noti_id_relacionado = $orse_id";
	$res = $db->ExecuteQuery($query);
	if ($res['status'] == 0) {
		$db->Rollback();
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}

	//evento
	$usua_id = 1;
	if (isset($_SESSION['user_id'])) {
		$usua_id = $_SESSION['user_id'];
	}

	$query = "UPDATE evento SET even_estado='ATENDIDO' 
			  WHERE 
					even_modulo='OS'
					AND even_estado='DESPACHADO'
					AND even_id_relacionado = $orse_id";
	$res = $db->ExecuteQuery($query);
	if ($res['status'] == 0) {
		$db->Rollback();
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}

	//insertamos los eventos de anulacion
	$res = Flight::AgregarEvento($db,"OS","ANULADA",$orse_id);
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$db->Commit();

	Flight::json($res);
});


//____________________________________________________________
//Adjuntar
Flight::route('GET /contrato/@cont_id:[0-9]+/os/adjuntar/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();

	//detalle
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if( count($res['data'])==0 ){
		Flight::json(array("status"=>0, "error"=>"Orden de servicio $orse_id no existe"));
		return;
	}
	
	$out['os'] = $res['data'][0];


	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/adjuntar/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$db = new MySQL_Database();
	$db->startTransaction();

	$usua_creador           = $_SESSION['user_id'];
	if (isset($_FILES["archivos"])) { 
		$files = array(); 
		foreach($_FILES["archivos"] as $key1 => $value1) {
			foreach($value1 as $key2 => $value2) {
				$files[$key2][$key1] = $value2; 
			}
		}
		$descriptions = json_decode($_POST['archivos_descripciones'],true);

		for($i=0; $i<count($files); $i++){
			$nombre      = $files[$i]['name'];
			$descripcion = $descriptions[$i];

			$filename = date('ymdHis')."_os_".$orse_id."_".str_replace(" ","_",$nombre);
			$resFile = Upload::UploadFile($files[$i],$filename); 
			if( !$resFile['status'] ){
				$db->Rollback();
				Flight::json(array("status" => 0, "error" => $resFile['error']));
				return;
			}
			
			$query = "INSERT INTO repositorio 
						(repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
						VALUES(
							'DOCUMENTO',
							'".$nombre."',
							'".$descripcion."',
							'orden_servicio',
							$orse_id,
							'".$resFile['data']['filename']."',
							NOW(),
							NULL,
							$usua_creador
					)";
			$resUpload = $db->ExecuteQuery($query);
			if( $resUpload['status']==0 ) {
				$db->Rollback();
				Flight::json(array("status" => 0, "error" => $resUpload['error']));
				return;
			}            
		}
	}

	$db->Commit();
	
	Flight::json(array("status" => 1));
});

//____________________________________________________________
//Detalle
Flight::route('GET /contrato/@cont_id:[0-9]+/os/detalle/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();

	//detalle
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if( count($res['data'])==0 ){
		Flight::json(array("status"=>0, "error"=>"Orden de servicio $orse_id no existe"));
		return;
	}
	
	$out['os'] = $res['data'][0];


	//asignacion
	$res = $dbo->ExecuteQuery("SELECT 
								oras_id,
								DATE_FORMAT(oras_fecha_asignacion,'%d-%m-%Y %T') AS oras_fecha_asignacion,
								usua_nombre,
								oras_estado
							   FROM 
								orden_servicio_asignacion
							   INNER JOIN usuario ON (orden_servicio_asignacion.usua_creador=usuario.usua_id)
							   WHERE
								orse_id=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$out['asignacion'] = $res['data'];
	    if(0<$res['rows']){
        for($i=0;$i<count($out['asignacion']);$i++){
            $oras_id = $out['asignacion'][$i]['oras_id'];

            $res = $dbo->ExecuteQuery("SELECT 
                                usua_nombre,
                                roau_tipo
                               FROM 
                                rel_orden_servicio_asignacion_usuario
                               INNER JOIN usuario ON (rel_orden_servicio_asignacion_usuario.usua_id=usuario.usua_id)
                               WHERE
                                oras_id=$oras_id");
            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
            //$detalle = $res['data'];
            $detalle = array("jefe"=>"","acompanantes"=>array());
            if(0<$res['rows']){
                for($j=0;$j<$res['rows'];$j++){
                    if($res['data'][$j]['roau_tipo']=="JEFECUADRILLA"){
                        $detalle['jefe'] = $res['data'][$j]['usua_nombre'];
                    }
                    else if($res['data'][$j]['roau_tipo']=="ACOMPANANTE"){
                        array_push($detalle['acompanantes'],$res['data'][$j]['usua_nombre']);
                    }
                }
            }

            $out['asignacion'][$i]['detalle'] = $detalle;
        }
    }


	//presupuesto
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id,
								pres_nombre,
								DATE_FORMAT(pres_fecha_creacion,'%d-%m-%Y %T') AS pres_fecha_creacion,
								uc.usua_nombre AS usua_creador,
								pres_estado,
								uv.usua_nombre AS usua_validador,
								DATE_FORMAT(pres_fecha_validacion,'%d-%m-%Y %T') AS pres_fecha_validacion
							   FROM 
								presupuesto
							   INNER JOIN usuario uc ON (presupuesto.usua_creador=uc.usua_id)
							   LEFT JOIN usuario uv ON (presupuesto.usua_validador=uv.usua_id)
							   WHERE
								orse_id=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$out['presupuesto'] = $res['data'];

	//visitas
	$res = $dbo->ExecuteQuery("SELECT
								tare_id,
								usua_nombre,
								tare_fecha_despacho,
								tare_fecha_descarga,
								tare_estado
							  FROM tarea
							  INNER JOIN usuario ON (usuario.usua_id=tarea.usua_id)
							  WHERE tare_modulo='OS' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['visitas'] = $res['data'];

	 /*visitas detalle    para cargar duplicado de formularios*/
    for($i=0;$i<count($out['visitas']);$i++){
        $tare_id = $out['visitas'][$i]['tare_id'];
        $res = $dbo->ExecuteQuery(" SELECT
                                       rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                    FROM rel_tarea_formulario_respuesta
                                    INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                    INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id) 
                                    WHERE tare_id=$tare_id
                                    GROUP BY fore_id");
        if( $res['status']==0 ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['visitas'][$i]['detalle'] = $res['data'];
    }

	/*informe*/
	$res = $dbo->ExecuteQuery("SELECT 
								info_id,
								DATE_FORMAT(info_fecha_creacion,'%d-%m-%Y %T') AS info_fecha_creacion,
								uc.usua_nombre AS usua_creador,
								info_estado,
								uv.usua_nombre AS usua_validador,
								DATE_FORMAT(info_fecha_validacion,'%d-%m-%Y %T') AS info_fecha_validacion,
								info_observacion
							   FROM 
								informe
							   INNER JOIN usuario uc ON (informe.usua_creador=uc.usua_id)
							   LEFT JOIN usuario uv ON (informe.usua_validador=uv.usua_id)
							   WHERE info_modulo='OS' AND info_id_relacionado=$orse_id");
	if( $res['status']==0 ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['informes'] = $res['data'];


	//eventos
	$res = $dbo->ExecuteQuery("SELECT 
								even_evento,
								even_fecha,
								even_estado,
								even_comentario
							   FROM 
								evento
							   WHERE
								even_modulo='OS' AND even_id_relacionado=$orse_id
							   ORDER BY even_fecha DESC");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$out['eventos'] = $res['data'];

   

	Flight::json($out);
});


//____________________________________________________________
//Asignacion
Flight::route('GET /contrato/@cont_id:[0-9]+/os/asignacion/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();

	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	//Obtener asignación previa (si la hay)
	$res = $dbo->ExecuteQuery("SELECT 
								orden_servicio_asignacion.oras_id,
								usuario.usua_nombre,
								oras_fecha_asignacion,
								emplazamiento_visita.emvi_fecha_ingreso,
								emplazamiento_visita.emvi_estado
							   FROM 
								orden_servicio_asignacion
							   INNER JOIN rel_orden_servicio_asignacion_usuario ON (orden_servicio_asignacion.oras_id=rel_orden_servicio_asignacion_usuario.oras_id AND roau_tipo='JEFECUADRILLA')
							   INNER JOIN usuario ON (rel_orden_servicio_asignacion_usuario.usua_id=usuario.usua_id)
							   LEFT JOIN emplazamiento_visita ON (emplazamiento_visita.emvi_modulo='OS' AND emplazamiento_visita.emvi_id_relacionado=$orse_id AND emplazamiento_visita.emvi_estado='ACTIVO')
							   WHERE
								orse_id=$orse_id AND oras_estado='ACTIVO'
							   ORDER BY oras_fecha_asignacion DESC
							   LIMIT 1");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['asignacion'] = null;
	if(0<$res['rows']){
		$out['asignacion'] = $res['data'][0];
	}

	//Obtener listado de usuarios
	$res = $dbo->ExecuteQuery("SELECT
								 usuario.usua_id,
								 usuario.usua_nombre,
								 perfil.perf_nombre
							  FROM 
								 orden_servicio
							  INNER JOIN usuario ON (usuario.empr_id=orden_servicio.empr_id AND usuario.usua_estado ='ACTIVO')
							  INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.usua_id=usuario.usua_id 
																  AND rel_contrato_usuario.cont_id=orden_servicio.cont_id 
																  AND rel_contrato_usuario.recu_estado ='ACTIVO')
							  INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
							  INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id 
													  AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))
							  WHERE
							   orse_id=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$out['jefes'] = array();
	$out['acompanantes'] = array();
	foreach ($res['data'] as $usuario) {
		if ($usuario['perf_nombre'] == 'TECNICO') {
			$out['acompanantes'][] = $usuario;
		} else {
			$out['jefes'][] = $usuario;
		}
	}	
	
	/*NOMBRE FORMULARIOS*/
	 $res = $dbo->ExecuteQuery("SELECT 
										 fo.form_id,
										 fo.form_nombre
										 FROM formulario fo
										 INNER JOIN rel_orden_servicio_formulario rof 
										 ON (fo.form_id = rof.form_id) AND fo.form_estado='ACTIVO'
										 WHERE orse_id=$orse_id");
								
	if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
	$out['nomForm'] = $res['data'];

	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/asignacion/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$usua_creador = $_SESSION['user_id'];

	$db = new MySQL_Database();   
	$db->startTransaction();

	$res = $db->ExecuteQuery("INSERT INTO orden_servicio_asignacion SET
								orse_id=$orse_id,
								oras_fecha_asignacion=NOW(),
								usua_creador='$usua_creador',
								oras_estado='ACTIVO'");
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$oras_id = $res['data'][0]['id'];

	$jefe = $_POST['jefecuadrilla'];
	$res = $db->ExecuteQuery("INSERT INTO rel_orden_servicio_asignacion_usuario SET
								oras_id=$oras_id,
								usua_id=$jefe,
								roau_tipo='JEFECUADRILLA'");
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}


	$acompanantes = array();
	if(is_array($_POST['acompanantes'])){
		foreach($_POST['acompanantes'] as $a){
			array_push($acompanantes,"(".$oras_id.",".$a.",'ACOMPANANTE')");
		}
		$acompanantes = implode(",",$acompanantes);
	} else {
		$acompanantes = "(".$oras_id.",".$_POST['acompanantes'].",'ACOMPANANTE')";
	}
	
	$res = $db->ExecuteQuery("INSERT INTO rel_orden_servicio_asignacion_usuario 
								(oras_id,usua_id,roau_tipo)
								VALUES
								$acompanantes");
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$resEvent = Flight::FinalizarTareaRelacionada($db,"OS","ASIGNAR",$orse_id);
	if( $resEvent['status']==0 ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$resEvent = Flight::AgregarEvento($db,"OS","ASIGNADA",$orse_id,array("oras_id"=>$oras_id));
	if( $resEvent['status']==0 ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$db->Commit();

	Flight::json($res);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/asignacion/@orse_id:[0-9]+/cancelar/@oras_id:[0-9]+', function($cont_id,$orse_id,$oras_id){
	$db = new MySQL_Database();   
	$db->startTransaction();

	$res = $db->ExecuteQuery("UPDATE orden_servicio_asignacion SET
								oras_estado='NOACTIVO' WHERE oras_id=$oras_id");
	if( 0==$res['status'] ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	
	$resEvent = Flight::AgregarEvento($db,"OS","ASIGNACION_CANCELADA",$orse_id,array("oras_id"=>$oras_id));
	if( $resEvent['status']==0 ){
		$db->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$db->Commit();
	Flight::json($res);

});


//____________________________________________________________
//Visita
Flight::route('GET /contrato/@cont_id:[0-9]+/os/visita/@orse_id:[0-9]+', function($cont_id,$orse_id){
	global $DEF_CONFIG;
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();

	//Revisar si se tiene aprobación
	$res = $dbo->ExecuteQuery("SELECT 
									orse_solicitud_informe_web
							   FROM 
									orden_servicio
							   WHERE
									orse_id=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if( 0==$res['rows'] || $res['data'][0]['orse_solicitud_informe_web'] != 'APROBADA'){
		Flight::json(array("status"=>0, "error"=>"Se requiere de aprobación para ingresar visita via web"));
		return;
	}

	//Revisar si se ha asignado
	$res = $dbo->ExecuteQuery("SELECT 
								oras_id                               
								FROM 
								orden_servicio_asignacion
							   WHERE
								orse_id=$orse_id AND oras_estado='ACTIVO'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if( 0==$res['rows'] ){
		Flight::json(array("status"=>0, "error"=>"Se requiere haber asignado OS antes de ingresar visita via web"));
		return;
	}


	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	$form_ids = array_keys($DEF_CONFIG["os"]["formularios"]);
	$form_ids_str = implode("','",$form_ids);
	$forms = array();
	//Cargar formularios
	$res = $dbo->ExecuteQuery("SELECT
								form_id     AS form_id,
								form_nombre AS form_name,
								form_opciones AS form_options

							  FROM 
								formulario
							  WHERE (form_id IN ('$form_ids_str') AND form_estado='ACTIVO' and cont_id=$cont_id)
							  OR (form_id IN ('1','2','3','4') AND form_estado='ACTIVO' and cont_id=1)");
	if(0==$res['status']){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	if(0<$res['rows']){
		$forms = $res['data'];

		foreach($forms as &$form){
			$form_id = $form['form_id'];
			if($form['form_options']!=""){
			  $form['form_options'] = json_decode($form['form_options'],true);
			}

			$res = $dbo->ExecuteQuery("SELECT
								fogr_id         AS fopa_id,
								fogr_nombre     AS fopa_name,
								fogr_opciones   AS fopa_options
							  FROM 
								formulario_grupo
							  WHERE form_id = '$form_id'  AND fogr_estado='ACTIVO'");
			if(0==$res['status']){
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}
			$form['pages'] = $res['data'];

			foreach($form['pages'] as &$page){
				$fopa_id = $page['fopa_id'];

				$res = $dbo->ExecuteQuery("SELECT
								foit_id         AS foco_id,
								foit_tipo       AS foco_type,
								foit_nombre     AS foco_label,
								foit_requerido  AS foco_required,
								foit_opciones   AS foco_options
							  FROM 
								formulario_item
							  WHERE fogr_id = '$fopa_id'  AND foit_estado='ACTIVO'
							  ORDER BY foit_orden,foit_id");
				if(0==$res['status']){
					Flight::json(array("status"=>0, "error"=>$res['error']));
					return;
				}
				$page['controls'] = $res['data'];
				
				foreach($page['controls'] as &$control){
					if($control['foco_options']!=""){
						$control['foco_options'] = json_decode($control['foco_options'],true);
					}
				}
				
				
			}
			
		}
	}
	$out['forms'] = $forms;
	/*Ruta para guardar el formulario (para hacerlo funcionar en distintos modulos)*/
	$out['route'] = "/contrato/".$cont_id."/os/visita/".$orse_id;

	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/visita/@orse_id:[0-9]+', function($cont_id,$orse_id){
  $db = new MySQL_Database();
  
  $form_id        = mysql_real_escape_string($_POST['form_id']);
  $form_name      = mysql_real_escape_string($_POST['form_name']);
  $user_id        = $_SESSION['user_id']; 
  $foan_datetime  = mysql_real_escape_string($_POST['foan_datetime_saved']);
  $status         = mysql_real_escape_string($_POST['status']);
  $last           = mysql_real_escape_string($_POST['last']);
  
  $db->startTransaction();
	  
  $res = $db->ExecuteQuery("INSERT INTO formulario_respuesta SET 
							  form_id=$form_id,
							  usua_creador=$user_id,
							  fore_fecha_creacion='$foan_datetime',
							  fore_fecha_recepcion=NOW(),
							  fore_ubicacion='web'");
  if($res['status']){
	$foan_id = $res['data'][0]['id'];

	//actualizar datos    
	$data    = json_decode(utf8_encode($_POST['form_data']),true);           
	if($data){
		//Revisar data para subir imagenes
		$values = array();
		foreach($data as $key => &$value){
			if(is_array($value)){
			  
			  foreach($value as &$v){
				if(isset($v['image'])){
				  if (isset($_FILES[$key])) {
					foreach ($_FILES[$key]['name'] as $k => $n) {
					  if($v['image']==$n){
						$v['image'] = "web_".date("Ymd_His").".".microtime(true)."_photo.jpg";

						$file_path = "../uploads/".$v['image'];
						if(!move_uploaded_file($_FILES[$key]['tmp_name'][$k], $file_path)) {
						   $db->Rollback();
						   Flight::json(array("status"=>false,"error"=>"Error al cargar archivo '$n'"));
						} 
						break;
					  }
					}

				  }
				}

			  }

			  $value = json_encode($value);
			}

			array_push($values,"(0,$foan_id,$key,'$value')");
		}
	
		//Agregar valores a formulario
		$query = "INSERT INTO formulario_valor VALUES ".implode(",",$values);
		$res = $db->ExecuteQuery($query);
		if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
		}

		//Obtener tarea relacionada
		$res = $db->ExecuteQuery("SELECT
									tare_id
								  FROM tarea
								  WHERE tare_modulo='OS' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado=$orse_id
								  ORDER BY tare_fecha_despacho DESC
								  LIMIT 1");
		if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
		}
		$tare_id = "";
		if(0<$res['rows']){
		  $tare_id = $res['data'][0]['tare_id'];
		}

		if($tare_id!=""){
		  $res = $db->ExecuteQuery("SELECT
									  rtfr_id
									FROM
									  rel_tarea_formulario_respuesta
									WHERE
									  tare_id=$tare_id AND form_id=$form_id");
		  if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
		  }
		  $rtfr_id = "";
		  if(0<$res['rows']){
			$rtfr_id = $res['data'][0]['rtfr_id'];
		  }

		  if($rtfr_id==""){
			$res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta SET
										tare_id=$tare_id,
										form_id=$form_id,
										fore_id=$foan_id,
										rtfr_accion='$form_name',
										rtfr_fecha='$foan_datetime',
										rtfr_estado='$status'");
			if(!$res['status']){
			  $db->Rollback();
			  Flight::json(array("status"=>false,"error"=>$res['error']));
			}
			$rtfr_id = $res['data'][0]['id'];
		  }
		  else{
			$res = $db->ExecuteQuery("UPDATE rel_tarea_formulario_respuesta SET
										fore_id=$foan_id,
										rtfr_fecha='$foan_datetime',
										rtfr_estado='$status' 
										WHERE rtfr_id='$rtfr_id'");
			if(!$res['status']){
			  $db->Rollback();
			  Flight::json(array("status"=>false,"error"=>$res['error']));
			}
		  }


		  $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta_revisiones SET
								rtfr_id=$rtfr_id,
								fore_id=$foan_id,
								rfrr_fecha='$foan_datetime',
								rfrr_estado='$status'");
		  if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
			return;
		  }
		}

		if($last=="1"){ //crear informe, enviar evento, finalizar tarea
		  $res = $db->ExecuteQuery("INSERT INTO informe SET 
									  info_modulo='OS',
									  info_id_relacionado='$orse_id',
									  usua_creador='$user_id',
									  info_fecha_creacion=NOW(),
									  info_estado='SINVALIDAR',
									  info_data='{\"tare_id\":$tare_id}'");
		  if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
			return;
		  }
		  $info_id = $res['data'][0]['id'];

		  $res = Flight::AgregarEvento($db,"OS","INFORME_AGREGADO",$orse_id,array("info_id"=>$info_id));
		  if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
			return;
		  }

		  $res = Flight::FinalizarTareaRelacionada($db,"OS","VISITAR_SITIO",$orse_id);
		  if(!$res['status']){
			$db->Rollback();
			Flight::json(array("status"=>false,"error"=>$res['error']));
			return;
		  }
	  }

	  $db->Commit();
	  Flight::json(array("status"=>true,"data"=>$foan_id));
	}
	else{
		$db->Rollback();
		Flight::json(array("status"=>false,"data"=>"Sin datos de formulario"));
		return;
	}
  }
  else{
	  $db->Rollback();
	  Flight::json(array("status"=>false,"error"=>$res['error']));
	  return;
  }

});


//____________________________________________________________
//Presupuesto
Flight::route('GET /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$out = array();
	$out['status'] = 1;
	$dbo = new MySQL_Database();

	//Validar si hay presupuestos previos
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id
							   FROM 
								presupuesto
							   WHERE orse_id=$orse_id AND pres_estado!='RECHAZADO'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if(0<$res['rows']){
		Flight::json(array("status"=>0, "error"=>"Debe tener rechazados los presupuestos anteriores antes de ingresar uno nuevo."));
		return;
	}


	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];
/*
	$query = "SELECT lgi.lpit_id,lgi.lpit_nombre,lgi.lpit_unidad,lgi.lpit_comentario,lgip.lpip_id,lgc.lpgc_nombre,lgip.lpip_precio,lgip.zona_id,lg.lpgr_id,lg.lpgr_nombre "
			. "FROM orden_servicio os, lpu l, lpu_grupo lg, lpu_item lgi, lpu_item_precio lgip, lpu_grupo_clase lgc, emplazamiento e, zona z, rel_zona_emplazamiento rze "
			. "WHERE "
				. "os.cont_id=" . $cont_id . " "
				. "AND os.orse_id=" . $orse_id . " "
				. "AND os.cont_id=l.cont_id "
				. "AND os.empl_id=e.empl_id "
				. "AND rze.empl_id=e.empl_id "
				. "AND rze.zona_id=z.zona_id "
				. "AND lgip.zona_id=z.zona_id "
				. "AND lg.lpu_id=l.lpu_id "
				. "AND lgi.lpgr_id=lg.lpgr_id "
				. "AND lgip.lpit_id=lgi.lpit_id "
				. "AND lgip.lpgc_id=lgc.lpgc_id "
			. "ORDER BY "
				. "lgi.lpit_id ASC, lgip.lpip_id ASC"
			;
  */  
	
	$query = "SELECT 
					lg.lpgr_padre as lpgr_id_padre
					,lg_padre.lpgr_nombre as lpgr_nombre_padre
					,lg.lpgr_id 
					,lg.lpgr_nombre
					,lgc.lpgc_id
					,lgc.lpgc_nombre 
					,lgi.lpit_id
					,lgi.lpit_nombre
					,lgi.lpit_unidad
					,lgi.lpit_comentario
					,lgip.lpip_id
					,lgip.lpip_precio
					,lgip.lpip_sap_capex
					,lgip.lpip_sap_opex
					,lgip.lpip_code1
					,lgip.lpip_code2
					,lgip.lpip_code3
					,lgip.lpip_code4
					,lgip.lpip_code5
					
			FROM 
					orden_servicio os
					, lpu l
					, lpu_grupo lg
					, lpu_item lgi
					, lpu_item_precio lgip
					, lpu_grupo_clase lgc
					, lpu_grupo lg_padre
					, emplazamiento e
					, rel_zona_emplazamiento rze
					, zona z
			WHERE
					os.cont_id = $cont_id
					AND os.orse_id = $orse_id
					AND os.cont_id = l.cont_id
					AND lg.lpu_id=l.lpu_id
					AND l.lpu_estado ='ACTIVO' 
					AND lgi.lpgr_id=lg.lpgr_id
					AND lgip.lpit_id = lgi.lpit_id
					AND lgip.lpgc_id = lgc.lpgc_id
					AND lgip.lpip_estado = 'ACTIVO'
					AND lg.lpgr_padre = lg_padre.lpgr_id
					AND os.empl_id = e.empl_id
					AND rze.empl_id = e.empl_id
					AND rze.zona_id = z.zona_id
					AND z.zona_tipo = 'CONTRATO'
					AND z.zona_estado = 'ACTIVO'
					AND lgip.zona_id = z.zona_id
	
			ORDER BY
					lg.lpgr_padre ASC 
					, lg.lpgr_id ASC
					, lgc.lpgc_id ASC
					,lgi.lpit_id ASC
					, lgip.lpip_id ASC
			";
	
	$res = $dbo->ExecuteQuery($query);
	if( 0==$res['status'] ) Flight::json(array("status"=>0, "error"=>$res['error']));

	$out['lpu_grupo']    = array();
	$out['lpu_subgrupo'] = array();
	$out['lpu']          = array();
	$out['total']        = $res['rows'];
	
	foreach ($res['data'] as $row) {
		if(!isset($out['lpu_grupo'][$row['lpgr_id_padre']])){
			$out['lpu_grupo'][$row['lpgr_id_padre']] = array("lpgr_id"=>$row['lpgr_id_padre'],
															 "lpgr_nombre"=>$row['lpgr_nombre_padre']);
		}
		if(!isset($out['lpu_subgrupo'][$row['lpgr_id']])){
			$out['lpu_subgrupo'][$row['lpgr_id']]    = array("lpgr_id"=>$row['lpgr_id'],
															 "lpgr_nombre"=>$row['lpgr_nombre'],
															 "lpgr_id_padre"=>$row['lpgr_id_padre']);
		}

		if(!isset($out['lpu'][$row['lpit_id']])){
			$out['lpu'][$row['lpit_id']] = array("lpit_id"=>$row['lpit_id'],
												 "lpit_nombre"=>$row['lpit_nombre'],
												 "lpit_unidad"=>$row['lpit_unidad'],
												 "lpit_comentario"=>$row['lpit_comentario'],
												 "lpgr_id_padre"=>$row['lpgr_id_padre'],
												 "lpgr_nombre_padre"=>$row['lpgr_nombre_padre'],
												 "lpgr_id"=>$row['lpgr_id'],
												 "lpgr_nombre"=>$row['lpgr_nombre'],
												 "precios"=>array());
		}
		array_push($out['lpu'][$row['lpit_id']]['precios'],array("lpip_id"=>$row['lpip_id'],
																 "lpgc_nombre"=>$row['lpgc_nombre'],
																 "lpip_precio"=>$row['lpip_precio'],
																 "lpip_sap_capex"=>$row['lpip_sap_capex'],
																 "lpip_sap_opex"=>$row['lpip_sap_opex'],
																 "lpip_code1"=>$row['lpip_code1'],
																 "lpip_code2"=>$row['lpip_code2'],
																 "lpip_code3"=>$row['lpip_code3'],
																 "lpip_code4"=>$row['lpip_code4'],
																 "lpip_code5"=>$row['lpip_code5'] ));
	}
	$out['lpu_grupo'] = array_values($out['lpu_grupo']);
	$out['lpu_subgrupo'] = array_values($out['lpu_subgrupo']);
	$out['lpu'] = array_values($out['lpu']);

	/*
	$total = $res['rows'];
	$j = 0;

	for ($i = 0; $i < $total; $i++) {

		$out['lpu'][$j] = array();
		$out['lpu'][$j]['lpit_id'] = $res['data'][$i]['lpit_id'];
		$out['lpu'][$j]['lpit_nombre'] = $res['data'][$i]['lpit_nombre'];
		$out['lpu'][$j]['lpit_unidad'] = $res['data'][$i]['lpit_unidad'];
		$out['lpu'][$j]['lpit_comentario'] = $res['data'][$i]['lpit_comentario'];
		$out['lpu'][$j]['lpgr_id'] = $res['data'][$i]['lpgr_id'];
		$out['lpu'][$j]['lpgr_nombre'] = $res['data'][$i]['lpgr_nombre'];
		$out['lpu'][$j]['lpgr_id_padre'] = $res['data'][$i]['lpgr_id_padre'];
		$out['lpu'][$j]['lpgr_nombre_padre'] = $res['data'][$i]['lpgr_nombre_padre'];

		$out['lpu'][$j]['lpip_id'] = $res['data'][$i]['lpip_id']; //<= para que funcione el filtro

		$k = 0;
		$out['lpu'][$j]['precios'] = array();
		$out['lpu'][$j]['precios'][$k] = array();
		$out['lpu'][$j]['precios'][$k]['lpip_id'] = $res['data'][$i]['lpip_id'];
		$out['lpu'][$j]['precios'][$k]['lpgc_nombre'] = $res['data'][$i]['lpgc_nombre'];
		$out['lpu'][$j]['precios'][$k]['lpip_precio'] = $res['data'][$i]['lpip_precio'];

		while ($i + 1 < $total && $res['data'][$i]['lpit_id'] == $res['data'][$i + 1]['lpit_id']) {
			$k++;
			$out['lpu'][$j]['precios'][$k] = array();
			$out['lpu'][$j]['precios'][$k]['lpip_id'] = $res['data'][$i + 1]['lpip_id'];
			$out['lpu'][$j]['precios'][$k]['lpgc_nombre'] = $res['data'][$i + 1]['lpgc_nombre'];
			$out['lpu'][$j]['precios'][$k]['lpip_precio'] = $res['data'][$i + 1]['lpip_precio'];
			$i++;
		}
		$j++;
	}
	
	for ($i = 0; $i < $total; $i++) {
		$grupo = array();
		$grupo['lpgr_id'] = $res['data'][$i]['lpgr_id_padre']; 
		$grupo['lpgr_nombre'] = $res['data'][$i]['lpgr_nombre_padre'];
		$out['lpu_grupo'][] = $grupo;

		$subgrupo = array();        
		$subgrupo['lpgr_id'] = $res['data'][$i]['lpgr_id'];
		$subgrupo['lpgr_nombre'] = $res['data'][$i]['lpgr_nombre'];
		$subgrupo['lpgr_id_padre'] = $grupo['lpgr_id'];
		$out['lpu_subgrupo'][] = $subgrupo;
		
		while ($i + 1 < $total && $res['data'][$i]['lpgr_id_padre'] == $res['data'][$i + 1]['lpgr_id_padre']) {
			if( $res['data'][$i]['lpgr_id'] != $res['data'][$i + 1]['lpgr_id'] ){
				$subgrupo = array();        
				$subgrupo['lpgr_id'] = $res['data'][$i + 1]['lpgr_id'];
				$subgrupo['lpgr_nombre'] = $res['data'][$i + 1]['lpgr_nombre'];
				$subgrupo['lpgr_id_padre'] = $grupo['lpgr_id'];
				$out['lpu_subgrupo'][] = $subgrupo;
			}
			$i++;
		}
	}
	*/

	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$dbo = new MySQL_Database();

	//Validar si hay presupuestos previos
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id
							   FROM 
								presupuesto
							   WHERE orse_id=$orse_id 
							   AND (pres_estado!='RECHAZADO'
							   AND pres_estado!='SINVALIDAR')");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if(0<$res['rows']){
		Flight::json(array("status"=>0, "error"=>"Debe tener rechazados los presupuestos anteriores antes de ingresar uno nuevo."));
		return;
	}
	
	$res = $dbo->ExecuteQuery("SELECT count(pres_id) AS count
							   FROM presupuesto 
							   WHERE orse_id='$orse_id'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$count = 1;
	if(0<$res['rows']){
		$count = intval($res['data'][0]['count']) + 1;   
	}

	//session_start();
	$pres_nombre  = "OS-$orse_id-PRES-".sprintf("%03d",$count);
	$usua_creador = $_SESSION['user_id'];

	$dbo->startTransaction();

	$res = $dbo->ExecuteQuery("INSERT INTO presupuesto SET
								orse_id='$orse_id',
								pres_nombre = '$pres_nombre',
								pres_fecha_creacion=NOW(),
								usua_creador='$usua_creador',
								pres_estado='SINVALIDAR'");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$pres_id = $res['data'][0]['id'];

	$lpip_ids        = $_POST['lpip_id'];
	$prit_cantidades = $_POST['prit_cantidad'];
	if(!is_array($lpip_ids)){
	  $lpip_ids = array($lpip_ids);
	  $prit_cantidades = array($prit_cantidades);
	}

	$lpipPrecio='null';
	if(isset($_POST['lpipPrecio']) && "" != $_POST['lpipPrecio']){
	  $lpipPrecio = $_POST['lpipPrecio'];

	}

	$lpitNombre='';
	if(isset($_POST['lpitNombre'])){
	  $lpitNombre = $_POST['lpitNombre'];

	}

	$lpgcNombre='';
	if(isset($_POST['lpgcNombre'])){
	  $lpgcNombre = $_POST['lpgcNombre'];
	}

	for($i=0;$i<count($lpip_ids);$i++){
		$lpip_id = $lpip_ids[$i];
		$prit_cantidad = $prit_cantidades[$i];

		$res = $dbo->ExecuteQuery("INSERT INTO presupuesto_item SET
								pres_id='$pres_id',
								lpip_id='$lpip_id',
								prit_cantidad='$prit_cantidad',
								prit_valor=$lpipPrecio,
								prit_nombre='$lpitNombre',
								prit_categoria='$lpgcNombre',
								prit_estado='SINVALIDAR'");
		if( 0==$res['status'] ){
			$dbo->Rollback();
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return;
		}
	}
	
	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","INGRESAR_PRESUPUESTO",$orse_id);
	if( 0 == $resEvent['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_AGREGADO",$orse_id,array("pres_id"=>$pres_id));
	if( 0 ==  $resEvent['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$dbo->Commit();
	Flight::json($res);
});

Flight::route('GET /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+/ver/@pres_id:[0-9]+', function($cont_id,$orse_id,$pres_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	$res = $dbo->ExecuteQuery("SELECT prit_id,
									prit_cantidad,
									prit_estado,
									prit_comentario,
									prit_valor,
									CONCAT('(',IFNULL(lpip_code1, ''),') ', lpit_nombre) as lpit_nombre,
									lpit_unidad,
									lpit_comentario,
									lpgr_nombre,
									lpip_precio,
									lpip_sap_capex,
									lpip_sap_opex,
									lpip_code1,
									lpip_code2,
									lpip_code3,
									lpip_code4,
									lpip_code5,
									lpgc_nombre,
									lpip_id
									FROM (

									SELECT
											prit_id,
											prit_cantidad,
											prit_estado,
											prit_comentario,
											prit_valor,
											prit_nombre as lpit_nombre,
											'' as lpit_unidad,
											'' as lpit_comentario,
											'' as lpgr_nombre,
											prit_valor AS lpip_precio,
											'' as lpip_sap_capex,
											NULL as lpip_sap_opex,
											NULL as lpip_code1,
											NULL as lpip_code2,
											NULL as lpip_code3,
											NULL as lpip_code4,
											NULL as lpip_code5,
											prit_categoria as lpgc_nombre,
											'0' as lpip_id
									FROM
										presupuesto
									INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
									#INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
									#INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
									#INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
									#INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
									WHERE
										presupuesto.pres_id=$pres_id 
										AND presupuesto.orse_id=$orse_id
										AND prit_valor is not null
										AND lpip_id is null
									UNION ALL

									SELECT
										prit_id,
										prit_cantidad,
										prit_estado,
										prit_comentario,
										prit_valor,
										lpit_nombre,
										lpit_unidad,
										lpit_comentario,
										lpgr_nombre,
										lpip_precio,
										lpip_sap_capex,
										lpip_sap_opex,
										lpip_code1,
										lpip_code2,
										lpip_code3,
										lpip_code4,
										lpip_code5,
										lpgc_nombre,
										presupuesto_item.lpip_id
									FROM
										presupuesto
									INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
									INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
									INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
									INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
									INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
									WHERE
										presupuesto.pres_id=$pres_id AND presupuesto.orse_id=$orse_id

								  ) a
	");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['presupuesto'] = $res['data'];
	Flight::json($out);
});

Flight::route('GET /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+/validar/@pres_id:[0-9]+', function($cont_id,$orse_id,$pres_id){
	$out = array();
	$out['status'] = 1;
	$dbo = new MySQL_Database();

	//Chequear si ya fue validado
	$res = $dbo->ExecuteQuery("SELECT 
								pres_estado
							   FROM 
								presupuesto
							   WHERE pres_id=$pres_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$pres_estado = $res['data'][0]['pres_estado'];
	if($pres_estado=="RECHAZADO"){
		Flight::json(array("status"=>0, "error"=>"Presupuesto ya fue RECHAZADO y no se puede volver a validar"));
		return;
	}
	else if($pres_estado=="APROBADO"){
		Flight::json(array("status"=>0, "error"=>"Presupuesto ya fue APROBADO y no se puede volver a validar"));
		return;
	}

/*
	$query = "UPDATE orden_servicio SET orse_estado='VALIDANDO',orse_responsable = 'MOVISTAR' where orse_id=".$orse_id;
	$res = $dbo->ExecuteQuery($query);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
*/

	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	//informe
	$res = $dbo->ExecuteQuery("SELECT 
								info_id,
								DATE_FORMAT(info_fecha_creacion,'%d-%m-%Y %T') AS info_fecha_creacion,
								uc.usua_nombre AS usua_creador,
								info_estado,
								uv.usua_nombre AS usua_validador,
								DATE_FORMAT(info_fecha_validacion,'%d-%m-%Y %T') AS info_fecha_validacion
							   FROM 
								informe
							   INNER JOIN usuario uc ON (informe.usua_creador=uc.usua_id)
							   LEFT JOIN usuario uv ON (informe.usua_validador=uv.usua_id)
							   WHERE info_modulo='OS' AND info_id_relacionado=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['informes'] = $res['data'];

	$res = $dbo->ExecuteQuery("SELECT 
									prit_id,
									prit_cantidad,
									CONCAT('(',IFNULL(lpip_code1,''),') ',lpit_nombre) as lpit_nombre,
									lpit_unidad,
									lpit_comentario,
									lpgr_nombre,
									lpip_precio,
									lpip_sap_capex,
									lpip_sap_opex,
									lpip_code1,
									lpip_code2,
									lpip_code3,
									lpip_code4,
									lpip_code5,
									lpgc_nombre,
									prit_estado,
									prit_comentario
								FROM 
									presupuesto
								INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
								INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
								INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
								INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
								INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
								WHERE
									presupuesto.pres_id=$pres_id AND presupuesto.orse_id=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['presupuesto'] = $res['data'];
	Flight::json($out);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+/validar/@pres_id:[0-9]+', function($cont_id,$orse_id,$pres_id){
	//session_start();

	$dbo = new MySQL_Database();

	$dbo->startTransaction();

	$usua_validador = $_SESSION['user_id'];
	$pres_estado = "PREAPROBADO"; 

	$cont_rechazados = 0;

	if(count($_POST['prit_id'])==1){
		$prit_id         = $_POST['prit_id'];
		$prit_estado     = $_POST['prit_estado'];
		$prit_comentario = $_POST['prit_comentario'];

		$res = $dbo->ExecuteQuery("UPDATE presupuesto_item SET
								prit_estado='$prit_estado',
								prit_comentario='$prit_comentario'
								WHERE
								prit_id='$prit_id'");
		if( 0==$res['status'] ){
			$dbo->Rollback();
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return;
		}

		if("RECHAZADO"==$prit_estado){
			$pres_estado = "RECHAZADO";
			$cont_rechazados=1;
		}
	} else{
		for($i=0;$i<count($_POST['prit_id']);$i++){
			$prit_id         = $_POST['prit_id'][$i];
			$prit_estado     = $_POST['prit_estado'][$i];
			$prit_comentario = $_POST['prit_comentario'][$i];

			$res = $dbo->ExecuteQuery("UPDATE presupuesto_item SET
									prit_estado='$prit_estado',
									prit_comentario='$prit_comentario'
									WHERE
									prit_id='$prit_id'");
			if( 0==$res['status'] ){
				$dbo->Rollback();
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}

			if("RECHAZADO"==$prit_estado){
			  $cont_rechazados=$cont_rechazados+1;
			  $pres_estado='PREVALIDADO';
				/*$pres_estado = "RECHAZADO";*/
			}
		}

		if(0<$cont_rechazados){
		  $pres_estado='PREVALIDADO';
		}

		if(count($_POST['prit_id'])==$cont_rechazados){
		  $pres_estado='RECHAZADO';
		}
	}

	$res = $dbo->ExecuteQuery("UPDATE presupuesto SET
								pres_estado='$pres_estado',
								usua_validador='$usua_validador',
								pres_fecha_validacion=NOW()
								WHERE
								pres_id='$pres_id'");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	if(count($_POST['prit_id'])>$cont_rechazados){
	  $resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_OBSERVADO",$orse_id,array("pres_id"=>$pres_id,"pres_estado"=>$pres_estado));
	  if( $resEvent['status']==0 ){
		  $dbo->Rollback();
		  Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		  return;
	  }

	} else {
	  $resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_VALIDADO",$orse_id,array("pres_id"=>$pres_id,"pres_estado"=>$pres_estado));
	  if( $resEvent['status']==0 ){
		  $dbo->Rollback();
		  Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		  return;
	  }
	}

	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","VALIDAR_PRESUPUESTO",$orse_id,'"pres_id":'.$pres_id);
	  if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$dbo->Commit();
	Flight::json($res);
});

//Modificar Presupuesto
Flight::route('GET /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+/modificar', function($cont_id,$orse_id){
	#Ver detalle
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	
	//presupuesto
	/*
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id,
								pres_nombre,
								DATE_FORMAT(pres_fecha_creacion,'%d-%m-%Y %T') AS pres_fecha_creacion,
								uc.usua_nombre AS usua_creador,
								pres_estado,
								uv.usua_nombre AS usua_validador,
								DATE_FORMAT(pres_fecha_validacion,'%d-%m-%Y %T') AS pres_fecha_validacion
							   FROM 
								presupuesto
							   INNER JOIN usuario uc ON (presupuesto.usua_creador=uc.usua_id)
							   LEFT JOIN usuario uv ON (presupuesto.usua_validador=uv.usua_id)
							   WHERE
								orse_id=$orse_id");*/
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id
							   FROM 
								presupuesto
							   WHERE orse_id=$orse_id AND pres_estado!='APROBADO'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	if(0==$res['rows']){
		Flight::json(array("status"=>0, "error"=>"Debe tener un presupuesto asignado para modificarlo."));
		return;
	}

	$pres_id = $res['data'][0]['pres_id'];
	$out['pres_id'] = $res['data'][0]['pres_id'];;

	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	/*
  
	SELECT
									prit_id,
									prit_cantidad as pritCantidad,
									prit_estado,
									prit_comentario,
									prit_valor AS lpipPrecio,
									'',
									'',
									'',
									'',
									prit_nombre as lpitNombre,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									NULL,
									prit_categoria as lpgcNombre,
									'0' as lpip_id
								FROM
									presupuesto
								INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
								#INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
								#INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
								#INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
								#INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
								WHERE
									presupuesto.pres_id=$pres_id 
									AND presupuesto.orse_id=$orse_id
									AND presupuesto_item.prit_valor is not null
								UNION ALL

								SELECT
									prit_id,
									prit_cantidad as pritCantidad,
									prit_estado,
									prit_comentario,
									lpip_precio as lpipPrecio,
									prit_valor,
									lpit_nombre as lpitNombre,
									lpit_unidad,
									lpit_comentario,
									lpgr_nombre,
									lpip_sap_capex,
									lpip_sap_opex,
									lpip_code1,
									lpip_code2,
									lpip_code3,
									lpip_code4,
									lpip_code5,
									lpgc_nombre as lpgcNombre,
									presupuesto_item.lpip_id
								FROM
									presupuesto
								INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
								INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
								INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
								INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
								INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
								WHERE
									presupuesto.pres_id=$pres_id 
									AND presupuesto.orse_id=$orse_id
									AND presupuesto_item.prit_valor is null

	*/

	$res = $dbo->ExecuteQuery("SELECT prit_id,
									prit_cantidad AS pritCantidad,
									prit_estado,
									prit_comentario,
									prit_valor ,
									CONCAT('(',IFNULL(lpip_code1, ''),') ', lpit_nombre) as lpitNombre,
									lpit_unidad,
									lpit_comentario,
									lpgr_nombre,
									lpip_precio as lpipPrecio,
									lpip_sap_capex,
									lpip_sap_opex,
									lpip_code1,
									lpip_code2,
									lpip_code3,
									lpip_code4,
									lpip_code5,
									lpgc_nombre AS lpgcNombre,
									lpip_id
								FROM (

									SELECT
											prit_id,
											prit_cantidad,
											prit_estado,
											prit_comentario,
											prit_valor,
											prit_nombre as lpit_nombre,
											'' as lpit_unidad,
											'' as lpit_comentario,
											'' as lpgr_nombre,
											prit_valor AS lpip_precio,
											'' as lpip_sap_capex,
											NULL as lpip_sap_opex,
											NULL as lpip_code1,
											NULL as lpip_code2,
											NULL as lpip_code3,
											NULL as lpip_code4,
											NULL as lpip_code5,
											prit_categoria as lpgc_nombre,
											'0' as lpip_id
									FROM
										presupuesto
									INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
									#INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
									#INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
									#INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
									#INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
									WHERE
										presupuesto.pres_id=$pres_id 
										AND presupuesto.orse_id=$orse_id
										AND prit_valor is not null
										AND lpip_id is null
									UNION ALL

									SELECT
										prit_id,
										prit_cantidad,
										prit_estado,
										prit_comentario,
										prit_valor,
										lpit_nombre,
										lpit_unidad,
										lpit_comentario,
										lpgr_nombre,
										lpip_precio,
										lpip_sap_capex,
										lpip_sap_opex,
										lpip_code1,
										lpip_code2,
										lpip_code3,
										lpip_code4,
										lpip_code5,
										lpgc_nombre,
										presupuesto_item.lpip_id
									FROM
										presupuesto
									INNER JOIN presupuesto_item ON (presupuesto_item.pres_id=presupuesto.pres_id)
									INNER JOIN lpu_item_precio ON (lpu_item_precio.lpip_id=presupuesto_item.lpip_id)
									INNER JOIN lpu_item ON (lpu_item.lpit_id=lpu_item_precio.lpit_id)
									INNER JOIN lpu_grupo_clase ON (lpu_grupo_clase.lpgc_id=lpu_item_precio.lpgc_id)
									INNER JOIN lpu_grupo ON (lpu_grupo.lpgr_id=lpu_item.lpgr_id)
									WHERE
										presupuesto.pres_id=$pres_id AND presupuesto.orse_id=$orse_id

								  ) a



	");
	

	/*
	$query = "SELECT lgi.lpit_id,lgi.lpit_nombre,lgi.lpit_unidad,lgi.lpit_comentario,lgip.lpip_id,lgc.lpgc_nombre,lgip.lpip_precio,lgip.zona_id,lg.lpgr_id,lg.lpgr_nombre "
			. "FROM orden_servicio os, lpu l, lpu_grupo lg, lpu_item lgi, lpu_item_precio lgip, lpu_grupo_clase lgc, emplazamiento e, zona z, rel_zona_emplazamiento rze "
			. "WHERE "
				. "os.cont_id=" . $cont_id . " "
				. "AND os.orse_id=" . $orse_id . " "
				. "AND os.cont_id=l.cont_id "
				. "AND os.empl_id=e.empl_id "
				. "AND rze.empl_id=e.empl_id "
				. "AND rze.zona_id=z.zona_id "
				. "AND lgip.zona_id=z.zona_id "
				. "AND lg.lpu_id=l.lpu_id "
				. "AND lgi.lpgr_id=lg.lpgr_id "
				. "AND lgip.lpit_id=lgi.lpit_id "
				. "AND lgip.lpgc_id=lgc.lpgc_id "
			. "ORDER BY "
				. "lgi.lpit_id ASC, lgip.lpip_id ASC";
	$res = $dbo->ExecuteQuery($query);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
*/
	$out['presupuesto'] = $res['data'];

	$query = "SELECT 
					lg.lpgr_padre as lpgr_id_padre
					,lg_padre.lpgr_nombre as lpgr_nombre_padre
					,lg.lpgr_id 
					,lg.lpgr_nombre
					,lgc.lpgc_id
					,lgc.lpgc_nombre 
					,lgi.lpit_id
					,lgi.lpit_nombre
					,lgi.lpit_unidad
					,lgi.lpit_comentario
					,lgip.lpip_id
					,lgip.lpip_precio
					,lgip.lpip_sap_capex
					,lgip.lpip_sap_opex
					,lgip.lpip_code1
					,lgip.lpip_code2
					,lgip.lpip_code3
					,lgip.lpip_code4
					,lgip.lpip_code5
			FROM 
					orden_servicio os
					, lpu l
					, lpu_grupo lg
					, lpu_item lgi
					, lpu_item_precio lgip
					, lpu_grupo_clase lgc
					, lpu_grupo lg_padre
					, emplazamiento e
					, rel_zona_emplazamiento rze
					, zona z
			WHERE
					os.cont_id = $cont_id
					AND os.orse_id = $orse_id
					AND os.cont_id = l.cont_id
					AND lg.lpu_id=l.lpu_id
					AND l.lpu_estado ='ACTIVO' 
					AND lgi.lpgr_id=lg.lpgr_id
					AND lgip.lpit_id = lgi.lpit_id
					AND lgip.lpgc_id = lgc.lpgc_id
					AND lgip.lpip_estado = 'ACTIVO'
					AND lg.lpgr_padre = lg_padre.lpgr_id
					AND os.empl_id = e.empl_id
					AND rze.empl_id = e.empl_id
					AND rze.zona_id = z.zona_id
					AND z.zona_tipo = 'CONTRATO'
					AND z.zona_estado = 'ACTIVO'
					AND lgip.zona_id = z.zona_id
			  ORDER BY
					lg.lpgr_padre ASC 
					, lg.lpgr_id ASC
					, lgc.lpgc_id ASC
					, lgi.lpit_id ASC
					, lgip.lpip_id ASC
			";
	
	$res = $dbo->ExecuteQuery($query);
	if( 0==$res['status'] ) Flight::json(array("status"=>0, "error"=>$res['error']));

	$out['lpu_grupo']    = array();
	$out['lpu_subgrupo'] = array();
	$out['lpu']          = array();
	$out['total']        = $res['rows'];
	
	foreach ($res['data'] as $row) {
		if(!isset($out['lpu_grupo'][$row['lpgr_id_padre']])){
			$out['lpu_grupo'][$row['lpgr_id_padre']] = array("lpgr_id"=>$row['lpgr_id_padre'],
															 "lpgr_nombre"=>$row['lpgr_nombre_padre']);
		}
		if(!isset($out['lpu_subgrupo'][$row['lpgr_id']])){
			$out['lpu_subgrupo'][$row['lpgr_id']]    = array("lpgr_id"=>$row['lpgr_id'],
															 "lpgr_nombre"=>$row['lpgr_nombre'],
															 "lpgr_id_padre"=>$row['lpgr_id_padre']);
		}

		if(!isset($out['lpu'][$row['lpit_id']])){
			$out['lpu'][$row['lpit_id']] = array("lpit_id"=>$row['lpit_id'],
												 "lpit_nombre"=>$row['lpit_nombre'],
												 "lpit_unidad"=>$row['lpit_unidad'],
												 "lpit_comentario"=>$row['lpit_comentario'],
												 "lpgr_id_padre"=>$row['lpgr_id_padre'],
												 "lpgr_nombre_padre"=>$row['lpgr_nombre_padre'],
												 "lpgr_id"=>$row['lpgr_id'],
												 "lpgr_nombre"=>$row['lpgr_nombre'],
												 "precios"=>array());
		}
		array_push($out['lpu'][$row['lpit_id']]['precios'],array("lpip_id"=>$row['lpip_id'],
																 "lpgc_nombre"=>$row['lpgc_nombre'],
																 "lpip_precio"=>$row['lpip_precio'],
																 "lpip_sap_capex"=>$row['lpip_sap_capex'],
																 "lpip_sap_opex"=>$row['lpip_sap_opex'],
																 "lpip_code1"=>$row['lpip_code1'],
																 "lpip_code2"=>$row['lpip_code2'],
																 "lpip_code3"=>$row['lpip_code3'],
																 "lpip_code4"=>$row['lpip_code4'],
																 "lpip_code5"=>$row['lpip_code5'] ));
	}
	$out['lpu_grupo'] = array_values($out['lpu_grupo']);
	$out['lpu_subgrupo'] = array_values($out['lpu_subgrupo']);
	$out['lpu'] = array_values($out['lpu']);
	
	Flight::json($out);

});

/*
Flight::route('POST /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+/modificar/@pres_id:[0-9]+/cerrar', function($cont_id,$orse_id,$pres_id){

	$dbo = new MySQL_Database();
	//Validar si hay presupuestos previos
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id
							   FROM 
								presupuesto
							   WHERE orse_id=$orse_id 
				 AND (pres_estado!='APROBADO'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$prit_ids        = $_POST['prit_id'];
	$lpip_ids        = $_POST['lpip_id'];
	$prit_cantidades = $_POST['prit_cantidad'];
	if(!is_array($lpip_ids)){
	  $lpip_ids = array($lpip_ids);
	  //$prit_cantidades = array($prit_cantidades);
	  //$prit_ids = array($prit_ids);
	}

	$lpipPrecios='0';
	if(isset($_POST['lpipPrecio'])){
	  $lpipPrecios = $_POST['lpipPrecio'];

	}

	$lpitNombres='';
	if(isset($_POST['lpitNombre'])){
	  $lpitNombres = $_POST['lpitNombre'];

	}

	$lpipValor='0';
	if(isset($_POST['lpipValor'])){
	  $lpipValores = $_POST['lpipValor'];

	}

	$lpgcNombres='';
	if(isset($_POST['lpgcNombre'])){
	  $lpgcNombres = $_POST['lpgcNombre'];

	}

	for($i=0;$i<count($lpip_ids);$i++){
		
		$lpip_id = $lpip_ids[$i];
		$prit_cantidad = $prit_cantidades[$i];
		
		$prit_id="";
		if(isset($prit_ids[$i]) && $prit_ids[$i] != ""){
		  $prit_id = $prit_ids[$i];
		}

		$lpitNombre='';
		if(isset($lpitNombres[$i]) && $lpitNombres[$i] != ''){
		  $lpitNombre = $lpitNombres[$i];
		}

		$lpipPrecio=0;
		if(isset($lpipPrecios[$i]) && $lpipPrecios[$i] != ''){
		  $lpipPrecio=$lpipPrecios[$i];
		}

		$lpgcNombre="";
		if(isset($lpgcNombres[$i]) && $lpgcNombres[$i] != ''){
		  $lpgcNombre=$lpgcNombres[$i];
		}
		
		//$prit_id_implode= implode(",",$prit_ids);
		if(isset($prit_id) && $prit_id!=""){
		  $res = $dbo->ExecuteQuery("UPDATE presupuesto_item SET
											  prit_cantidad='$prit_cantidad'
									  WHERE
											prit_id='$prit_id'
									  AND   pres_id='$pres_id' ");
		  if( 0==$res['status'] ){
			$dbo->Rollback();
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return; 
		  }

		}else{
		  $res = $dbo->ExecuteQuery("INSERT INTO presupuesto_item SET
												pres_id='$pres_id',
												lpip_id='$lpip_id',
												prit_cantidad='$prit_cantidad',
												prit_valor=IFNULL('$lpipPrecio',0),
												prit_nombre='$lpitNombre',
												prit_categoria='$lpgcNombre',
												prit_estado='SINVALIDAR'
		  ");
		  if( 0==$res['status'] ){
			  $dbo->Rollback();
			  Flight::json(array("status"=>0, "error"=>$res['error']));
			  return;
		  }
		}
		$dbo->Commit();
	}

	$res = $dbo->ExecuteQuery("UPDATE presupuesto SET
											  pres_estado='SINVALIDAR'
									  WHERE pres_id='$pres_id' ");
	if( 0==$res['status'] ){
	  $dbo->Rollback();
	  Flight::json(array("status"=>0, "error"=>$res['error']));
	  return; 
	}

	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","VALIDAR_INFORME",$orse_id,'"info_id":'.$info_id);
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_ACTUALIZADO",$orse_id,array("pres_id"=>$pres_id));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_AGREGADO",$orse_id,array("pres_id"=>$pres_id));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$dbo->Commit();
	Flight::json($res);

});
*/

Flight::route('POST /contrato/@cont_id:[0-9]+/os/presupuesto/@orse_id:[0-9]+/modificar/@pres_id:[0-9]+', function($cont_id,$orse_id,$pres_id){

  $dbo = new MySQL_Database();
/*
	//Validar si hay presupuestos previos
	$res = $dbo->ExecuteQuery("SELECT 
								pres_id
							   FROM 
								presupuesto
							   WHERE orse_id=$orse_id 
				 AND (pres_estado!='RECHAZADO'
				 AND pres_estado!='SINVALIDAR')");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	
	if(0<$res['rows']){
		Flight::json(array("status"=>0, "error"=>"Debe tener rechazados los presupuestos anteriores antes de ingresar uno nuevo."));
		return;
	}

	/*
	$res = $dbo->ExecuteQuery("SELECT count(pres_id) AS count
							   FROM presupuesto 
							   WHERE orse_id='$orse_id'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$count = 1;
	if(0<$res['rows']){
		$count = intval($res['data'][0]['count']) + 1;   
	}

	//session_start();
	$pres_nombre  = "OS-$orse_id-PRES-".sprintf("%03d",$count);
	$usua_creador = $_SESSION['user_id'];

	$dbo->startTransaction();

	$res = $dbo->ExecuteQuery("INSERT INTO presupuesto SET
								orse_id='$orse_id',
								pres_nombre = '$pres_nombre',
								pres_fecha_creacion=NOW(),
								usua_creador='$usua_creador',
								pres_estado='SINVALIDAR'");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	
	$pres_id = $res['data'][0]['id'];
  
	*/

	$prit_ids        = $_POST['prit_id'];
	$lpip_ids        = $_POST['lpip_id'];
	$prit_cantidades = $_POST['prit_cantidad'];
	
	if(!is_array($lpip_ids)){
	  $lpip_ids = array($lpip_ids);
	}
	
	if(!is_array($prit_cantidades)){
	  $$prit_cantidades = array($prit_cantidades);
	}

	if(!is_array($prit_ids)){
	  $prit_ids = array($prit_ids);
	}

	$lpipPrecios='0';
	if(isset($_POST['lpipPrecio'])){
	  $lpipPrecios = $_POST['lpipPrecio'];

	}

	$lpitNombres='';
	if(isset($_POST['lpitNombre'])){
	  $lpitNombres = $_POST['lpitNombre'];

	}

	$lpipValor='0';
	if(isset($_POST['lpipValor'])){
	  $lpipValores = $_POST['lpipValor'];

	}

	$lpgcNombres='';
	if(isset($_POST['lpgcNombre'])){
	  $lpgcNombres = $_POST['lpgcNombre'];

	}

	for($i=0;$i<count($lpip_ids);$i++){
		
		$lpip_id = $lpip_ids[$i];
		$prit_cantidad = $prit_cantidades[$i];
		
		$prit_id="";
		if(isset($prit_ids[$i]) && $prit_ids[$i] != ""){
		  $prit_id = $prit_ids[$i];
		}

		$lpitNombre='';
		if(isset($lpitNombres[$i]) && $lpitNombres[$i] != ''){
		  $lpitNombre = $lpitNombres[$i];
		}

		$lpipPrecio=0;
		if(isset($lpipPrecios[$i]) && $lpipPrecios[$i] != ''){
		  $lpipPrecio=$lpipPrecios[$i];
		}

		$lpgcNombre="";
		if(isset($lpgcNombres[$i]) && $lpgcNombres[$i] != ''){
		  $lpgcNombre=$lpgcNombres[$i];
		}
		
		if(isset($prit_id) && $prit_id!=""){
		  $res = $dbo->ExecuteQuery(" UPDATE presupuesto_item SET
											  prit_cantidad='$prit_cantidad'
									  WHERE
											prit_id='$prit_id'
									  AND   pres_id='$pres_id' ");
		  if( 0==$res['status'] ){
			$dbo->Rollback();
			Flight::json(array("status"=>0, "error"=>$res['error']));
			return; 
		  }

		} else {
		  $res = $dbo->ExecuteQuery("INSERT INTO presupuesto_item SET
												pres_id='$pres_id',
												lpip_id=$lpip_id,
												prit_cantidad='$prit_cantidad',
												prit_valor=IFNULL('$lpipPrecio',0),
												prit_nombre='$lpitNombre',
												prit_categoria='$lpgcNombre',
												prit_estado='SINVALIDAR'
		  ");

		  if( 0==$res['status'] ){
			  $dbo->Rollback();
			  Flight::json(array("status"=>0, "error"=>$res['error']));
			  return;
		  }
		}
		$dbo->Commit();
	}

	$res = $dbo->ExecuteQuery("UPDATE presupuesto SET
											  pres_estado='SINVALIDAR'
									  WHERE pres_id='$pres_id' ");
	if( 0==$res['status'] ){
	  $dbo->Rollback();
	  Flight::json(array("status"=>0, "error"=>$res['error']));
	  return; 
	}

	/*FINALIZAR TAREA */

	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","MODIFICA_PRESUPUESTO",$orse_id,'"pres_id":'.$pres_id);
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$accion      = $_POST['accion'];
	if($accion=="CERRAR"){
	  $resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_ACTUALIZADO",$orse_id,'{"pres_id":'.$pres_id.'}');
	  if( $resEvent['status']==0 ){
		  $dbo->Rollback();
		  Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		  return;
	  }

	  $resEvent = Flight::AgregarEvento($dbo,"OS","PRESUPUESTO_AGREGADO",$orse_id,array("pres_id"=>$pres_id));
	  if( $resEvent['status']==0 ){
		  $dbo->Rollback();
		  Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		  return;
	  }
	}

	$dbo->Commit();
	Flight::json($res);

});

Flight::route('GET|POST /contrato/os/presupuesto/modificar/del/@prit_id:[0-9]+', function($prit_id){
	$out = array();
	$out['status'] = 1;
	$dbo = new MySQL_Database();
	$res = $dbo->ExecuteQuery("DELETE FROM presupuesto_item WHERE prit_id=$prit_id");
	if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
	Flight::json($out);
});

//____________________________________________________________
//Informe
Flight::route('GET /contrato/@cont_id:[0-9]+/os/informe/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$dbo = new MySQL_Database();
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	Flight::json($out);

});

Flight::route('GET /contrato/@cont_id:[0-9]+/os/informe/@orse_id:[0-9]+/ver/@info_id:[0-9]+', function($cont_id,$orse_id,$info_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];


	$res = $dbo->ExecuteQuery("SELECT
									uc.usua_nombre AS usua_creador,
									info_id,
									info_fecha_creacion,
									info_data,
									uv.usua_nombre AS usua_validador,
									info_fecha_validacion,
									info_estado
								FROM
									informe
								INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
								LEFT JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
								WHERE
									info_modulo='OS' AND info_id=$info_id AND info_id_relacionado=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$out['informe'] = array();
	if(0<$res['rows']){
        $out['informe'] = $res['data'][0];

        $out['formularios'] = array();

        //Obtener visitas
        $info_data = json_decode($out['informe']['info_data'],true);
        if($info_data!=null){
            $tare_id = $info_data['tare_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                            rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                        FROM
                                            rel_tarea_formulario_respuesta
                                        INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                        INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
                                        WHERE
                                            tare_id=$tare_id
                                        GROUP BY fore_id");
            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }

            $out['formularios'] = $res['data'];

            for($i=0;$i<count($out['formularios']);$i++){
                $fore_id = $out['formularios'][$i]['fore_id'];

                if($out['formularios'][$i]['fore_ubicacion']!="" && $out['formularios'][$i]['fore_ubicacion']!="web"){
                    $out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
                }

                $res = $dbo->ExecuteQuery("SELECT
                                                fogr_nombre,
                                                formulario_item.foit_id,
                                                foit_nombre,
                                                foit_tipo,
                                                fova_valor,
                                                foit_opciones
                                            FROM
                                                formulario_respuesta
                                            INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
                                            INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
                                            LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
                                            WHERE
                                                formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
                                            ORDER BY fogr_orden,foit_orden;");
                if( $res['status']==0 ){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }

               $data = array();

                foreach ($res['data'] as &$row) {
                    $row['fova_valor'] = $row['fova_valor'];//utf8_encode($row['fova_valor']);
                    
                    if(!isset($data[$row['fogr_nombre']])){
                        $data[$row['fogr_nombre']] = array();
                    }
                    if($row['foit_tipo']=="CAMERA"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }
                    if($row['foit_tipo']=="AGGREGATOR"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_opciones']!=""){
                        $row['foit_opciones'] = json_decode($row['foit_opciones'],true);
                    }

                    array_push($data[$row['fogr_nombre']],$row);   
                }
                $out['formularios'][$i]['data'] = $data;
            }

        }
    }

	
	Flight::json($out);
});

Flight::route('GET /contrato/@cont_id:[0-9]+/os/informe/@orse_id:[0-9]+/validar/@info_id:[0-9]+', function($cont_id,$orse_id,$info_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	
	//Chequear si ya fue validado
	$res = $dbo->ExecuteQuery("SELECT 
								info_estado
							   FROM 
								informe
							   WHERE info_id=$info_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$info_estado = $res['data'][0]['info_estado'];
	if($info_estado!="SINVALIDAR"){
		Flight::json(array("status"=>0, "error"=>"Informe ya fue validado"));
		return;
	}

/*    $query = "UPDATE orden_servicio SET orse_estado='VALIDANDO',orse_responsable = 'MOVISTAR' where orse_id=".$orse_id;
	$res = $dbo->ExecuteQuery($query);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
*/
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	$res = $dbo->ExecuteQuery("SELECT
									uc.usua_nombre AS usua_creador,
									info_id,
									info_fecha_creacion,
									info_data,
									uv.usua_nombre AS usua_validador,
									info_fecha_validacion,
									info_estado
								FROM
									informe
								INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
								LEFT JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
								WHERE
									info_modulo='OS' AND info_id=$info_id AND info_id_relacionado=$orse_id");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$out['informe'] = array();
	if(0<$res['rows']){
		$out['informe'] = $res['data'][0];

		$out['formularios'] = array();

		//Obtener visitas
		$info_data = json_decode($out['informe']['info_data'],true);
		if($info_data!=null){
			$tare_id = $info_data['tare_id'];

			$res = $dbo->ExecuteQuery("SELECT
											rel_tarea_formulario_respuesta_revisiones.fore_id,
											fore_ubicacion,
											rtfr_accion,
											rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
											rel_tarea_formulario_respuesta_revisiones.rfrr_estado
										FROM
											rel_tarea_formulario_respuesta
										INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
										INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
										WHERE
											tare_id=$tare_id");
			if( 0==$res['status'] ){
				Flight::json(array("status"=>0, "error"=>$res['error']));
				return;
			}

			$out['formularios'] = $res['data'];

			for($i=0;$i<count($out['formularios']);$i++){
				$fore_id = $out['formularios'][$i]['fore_id'];

				if($out['formularios'][$i]['fore_ubicacion']!="" && $out['formularios'][$i]['fore_ubicacion']!="web"){
					$out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
				}

				$res = $dbo->ExecuteQuery("SELECT
												fogr_nombre,
												formulario_item.foit_id,
												foit_nombre,
												foit_tipo,
												fova_valor,
												foit_opciones
											FROM
												formulario_respuesta
											INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
											INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
											LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
											WHERE
												formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
											ORDER BY fogr_orden,foit_orden;");
				if( 0==$res['status'] ){
					Flight::json(array("status"=>0, "error"=>$res['error']));
					return;
				}

				$data = array();

				foreach ($res['data'] as &$row) {
					$row['fova_valor'] = $row['fova_valor'];//utf8_encode($row['fova_valor']);

					if(!isset($data[$row['fogr_nombre']])){
						$data[$row['fogr_nombre']] = array();
					}
					if($row['foit_tipo']=="CAMERA"){
						$row['fova_valor'] = json_decode($row['fova_valor'],true);
					}
					if($row['foit_tipo']=="AGGREGATOR"){
						$row['fova_valor'] = json_decode($row['fova_valor'],true);
					}

					if($row['foit_opciones']!=""){
						$row['foit_opciones'] = json_decode($row['foit_opciones'],true);
					}

					array_push($data[$row['fogr_nombre']],$row);   
				}
				$out['formularios'][$i]['data'] = $data;
			}

		}
	}
	
	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/informe/@orse_id:[0-9]+/validar/@info_id:[0-9]+', function($cont_id,$orse_id,$info_id){
	//session_start();

	$info_estado      = $_POST['info_estado'];
	$info_observacion = $_POST['info_observacion'];
	$usua_validador   = $_SESSION['user_id'];

	$dbo = new MySQL_Database();

	$dbo->startTransaction();
	$res = $dbo->ExecuteQuery("UPDATE informe SET
								info_estado='$info_estado',
								info_observacion='$info_observacion',
								usua_validador='$usua_validador',
								info_fecha_validacion=NOW()
								WHERE
								info_id='$info_id'");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	//TODO: Filtrar que se marque como realizada sólo la tarea relativa al informe en cuestion.
	//Pues pueden haber informes no validados pendientes....
	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","VALIDAR_INFORME",$orse_id,'"info_id":'.$info_id);
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}
	

	$resEvent = Flight::AgregarEvento($dbo,"OS","INFORME_VALIDADO",$orse_id,array("info_id"=>$info_id,"info_estado"=>$info_estado));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$dbo->Commit();
	Flight::json($res);
});


//____________________________________________________________
//Cerrar
Flight::route('GET /contrato/@cont_id:[0-9]+/os/cerrar/@orse_id:[0-9]+', function($cont_id,$orse_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];

	$res = $dbo->ExecuteQuery("SELECT
								pres_id,
								uc.usua_nombre AS usua_creador,
								pres_fecha_creacion,
								uv.usua_nombre AS usua_validador,
								pres_fecha_validacion,
								pres_estado
							FROM
								presupuesto
							INNER JOIN usuario uc ON (uc.usua_id = presupuesto.usua_creador)
							LEFT JOIN usuario uv ON (uv.usua_id = presupuesto.usua_validador)
							WHERE orse_id=$orse_id AND pres_estado='PREAPROBADO'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['presupuesto'] = $res['data'];


	$res = $dbo->ExecuteQuery("SELECT
									info_id,
									uc.usua_nombre AS usua_creador,
									info_fecha_creacion,
									uv.usua_nombre AS usua_validador,
									info_fecha_validacion,
									info_estado,
									info_observacion
								FROM
									informe
								INNER JOIN usuario uc ON (uc.usua_id = informe.usua_creador)
								LEFT JOIN usuario uv ON (uv.usua_id = informe.usua_validador)
								WHERE info_modulo='OS' AND info_id_relacionado=$orse_id AND info_estado='PREAPROBADO'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['informe'] = $res['data'];

	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/cerrar/@orse_id:[0-9]+', function($cont_id,$orse_id){
	//session_start();
	try{
		$orse_estado      = $_POST['orse_estado'];
		$orse_comentario  = $_POST['orse_comentario'];
		$usua_validador   = $_SESSION['user_id'];

		$dbo = new MySQL_Database();
		$dbo->startTransaction();

		if($orse_estado=="CANCELADA"){
			$resEvent = Flight::AgregarEvento($dbo,"OS","OS_FINALIZADA",$orse_id);
			if( $resEvent['status']==0 ){
				throw new Exception($resEvent['error']);
			}
			Flight::json($resEvent);
		}
		else{

			$query = "
	            SELECT
	                MAX(info_id) AS info_id
	            FROM
	                informe
	            WHERE
	                info_modulo = 'OS'
	                AND info_id_relacionado = ".$orse_id.";";
	        $res = $dbo->ExecuteQuery($query);
	        if( 0 == $res['status'] ){
	            throw new Exception($res['error'].". Query => ".$query);
	        }

	        if( $res['rows'] > 0 ){
	            $info_id = $res['data'][0]['info_id'];
	        } else{
	            throw new Exception("No hay datos. Query => ".$query);
	        }

			if( $orse_estado == "RECHAZADA" ){
				$orse_estado = "VALIDANDO";
				$pres_estado = "RECHAZADO";
				$info_estado = "SINVALIDAR";
			} else{
				$pres_estado = "APROBADO";
				$info_estado = "APROBADO";
			}

			$query = "
				UPDATE
					orden_servicio
				SET
					orse_estado='$orse_estado',
					usua_validador='$usua_validador',
					orse_fecha_validacion=NOW(),
					orse_comentario='$orse_comentario'
				WHERE
					orse_id='$orse_id'";
			$res = $dbo->ExecuteQuery($query);
			if( 0==$res['status'] ){
				throw new Exception($res['error'].". Query => ".$query);
			}

			

			$query = "
				UPDATE 
					presupuesto
				SET
					pres_estado='$pres_estado'
				WHERE
					orse_id='$orse_id' 
					AND pres_estado='PREAPROBADO'";
			$res = $dbo->ExecuteQuery($query);
			if( 0==$res['status'] ){
				throw new Exception($res['error'].". Query => ".$query);
			}

			$query = "
				UPDATE
					informe
				SET
					info_estado='$info_estado'
				WHERE
					info_modulo='OS' 
					AND info_id_relacionado='$orse_id' 
					AND info_estado='PREAPROBADO'";
			$res = $dbo->ExecuteQuery($query);
			if( 0==$res['status'] ){
				throw new Exception($res['error'].". Query => ".$query);
			}

			//Enviar evento
			$resEvent = Flight::AgregarEvento($dbo,"OS","OS_VALIDADA",$orse_id,array("orse_estado"=>$orse_estado, "info_id" => $info_id));
			if( $resEvent['status']==0 ){
				throw new Exception($resEvent['error']);
			}

			//Finalizar tarea
			$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","VALIDAR_OS",$orse_id);
			if( $resEvent['status']==0 ){
				throw new Exception($resEvent['error']);
			}
			
			
			//Eliminar tareas y notificaciones pendientes
			$query = "UPDATE tarea SET tare_estado='CANCELADA' 
					  WHERE tare_modulo='OS' AND tare_estado IN ('CREADA','DESPACHADA','EJECUTANDO') AND tare_id_relacionado = $orse_id";
			
			$res = $dbo->ExecuteQuery($query);
			if ($res['status'] == 0) {
					throw new Exception($res['error'].". Query => ".$query);
			}

			//notificacion
			$query = "UPDATE notificacion SET noti_estado='ENTREGADA' 
							  WHERE 
											noti_modulo='OS'
											AND noti_estado='DESPACHADA'
											AND noti_id_relacionado = $orse_id";
			$res = $dbo->ExecuteQuery($query);
			if ($res['status'] == 0) {
					throw new Exception($res['error'].". Query => ".$query);
			}
		
		
		//Eliminar tareas y notificaciones pendientes
		$query = "UPDATE tarea SET tare_estado='CANCELADA' 
				  WHERE tare_modulo='OS' AND tare_estado IN ('CREADA','DESPACHADA','EJECUTANDO') AND tare_id_relacionado = $orse_id";
		
		$res = $dbo->ExecuteQuery($query);
		if ($res['status'] == 0) {
				$dbo->Rollback();
				Flight::json(array("status" => 0, "error" => $res['error']));
				return;
		}

			$dbo->Commit();
			Flight::json($res);
		}
	} catch( Exception $e ){
        GLOBAL $BASEDIR;
        $dbo->Rollback();
        $time = time();
        $output = "\n[".$time."] ".$e."\n";
        error_log($output, 3, $BASEDIR."logs/error_log.log");
        Flight::json(array("status"=>0, "error"=>$time));
    }
});

//____________________________________________________________
//Solicitudes
Flight::route('POST /contrato/@cont_id:[0-9]+/os/solicitud/cambio/@orse_id:[0-9]+', function($cont_id,$orse_id){
	
	$dbo = new MySQL_Database();
	$dbo->startTransaction();

	$razon = $_POST['razon'];

	$res = $dbo->ExecuteQuery("UPDATE orden_servicio SET
								orse_solicitud_cambio='SOLICITANDO'
								WHERE
								orse_id='$orse_id' AND cont_id=$cont_id");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
   
	$resEvent = Flight::AgregarEvento($dbo,"OS","SOLICITUD_CAMBIO",$orse_id,array("razon"=>$razon));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}
   
	$dbo->Commit();
	Flight::json($res);
});

Flight::route('GET /contrato/@cont_id:[0-9]+/os/solicitud/cambio/@orse_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$orse_id,$tare_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];


	$res = $dbo->ExecuteQuery("SELECT tare_id,tare_data
							   FROM tarea 
							   WHERE  tare_id='$tare_id'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['tarea'] = $res['data'][0];
	if($out['tarea']['tare_data']!=""){
	   $out['tarea']['tare_data'] = json_decode($out['tarea']['tare_data'],true); 
	}

	Flight::json($out);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/os/solicitud/cambio/@orse_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$orse_id,$tare_id){
	$orse_solicitud_cambio  = $_POST['orse_solicitud_cambio'];
	$orse_tipo_anterior     = $_POST['orse_tipo_anterior'];
	$orse_tipo              = $_POST['orse_tipo'];

	$dbo = new MySQL_Database();

	$dbo->startTransaction();

	$res = $dbo->ExecuteQuery("UPDATE orden_servicio SET
								orse_tipo='$orse_tipo',
								orse_solicitud_cambio='$orse_solicitud_cambio'
								WHERE
								orse_id='$orse_id' AND cont_id=$cont_id");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","VALIDAR_SOLICITUD_CAMBIO",$orse_id);
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}
	

	$resEvent = Flight::AgregarEvento($dbo,"OS","SOLICITUD_CAMBIO_VALIDADA",$orse_id,array("orse_tipo_anterior"=>$orse_tipo_anterior,"orse_tipo_actual"=>$orse_tipo,"orse_solicitud_cambio"=>$orse_solicitud_cambio));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$dbo->Commit();
	Flight::json($res);
});


//solicitud de informe
Flight::route('POST /contrato/@cont_id:[0-9]+/os/solicitud/informe/@orse_id:[0-9]+', function($cont_id,$orse_id){
	
	$dbo = new MySQL_Database();
	$dbo->startTransaction();

	$razon = $_POST['razon'];

	$res = $dbo->ExecuteQuery("UPDATE orden_servicio SET
								orse_solicitud_informe_web='SOLICITANDO'
								WHERE
								orse_id='$orse_id' AND cont_id=$cont_id");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
   
	$resEvent = Flight::AgregarEvento($dbo,"OS","SOLICITUD_INFORME",$orse_id,array("razon"=>$razon));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}
   
	$dbo->Commit();
	Flight::json($res);
});


Flight::route('GET /contrato/@cont_id:[0-9]+/os/solicitud/informe/@orse_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$orse_id,$tare_id){
	$out = array();
	$out['status'] = 1;

	$dbo = new MySQL_Database();
	$res = Flight::ObtenerDetalleOS($dbo,$cont_id,$orse_id);
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['os'] = $res['data'][0];


	$res = $dbo->ExecuteQuery("SELECT tare_id,tare_data
							   FROM tarea 
							   WHERE  tare_id='$tare_id'");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['tarea'] = $res['data'][0];
	if($out['tarea']['tare_data']!=""){
	   $out['tarea']['tare_data'] = json_decode($out['tarea']['tare_data'],true); 
	}

	Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/solicitud/informe/@orse_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$orse_id,$tare_id){
	$orse_solicitud_informe_web  = $_POST['orse_solicitud_informe_web'];
	
	$dbo = new MySQL_Database();

	$dbo->startTransaction();

	$res = $dbo->ExecuteQuery("UPDATE orden_servicio SET
								orse_solicitud_informe_web='$orse_solicitud_informe_web'
								WHERE
								orse_id='$orse_id' AND cont_id=$cont_id");
	if( 0==$res['status'] ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}

	$resEvent = Flight::FinalizarTareaRelacionada($dbo,"OS","VALIDAR_SOLICITUD_INFORME",$orse_id);
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}
	

	$resEvent = Flight::AgregarEvento($dbo,"OS","SOLICITUD_INFORME_VALIDADA",$orse_id,array("orse_solicitud_informe_web"=>$orse_solicitud_informe_web));
	if( $resEvent['status']==0 ){
		$dbo->Rollback();
		Flight::json(array("status"=>0, "error"=>$resEvent['error']));
		return;
	}

	$dbo->Commit();
	Flight::json($res);
});

//obtener lista de formulario por el tipo de os

Flight::route('GET|POST /contrato/@cont_id:[0-9]+/os/tipo/@orse_tipo+/formularios/list',function($cont_id,$orse_tipo){
	$dbo=new MySQL_Database();
	$out['status']=1;
	$query="SELECT * FROM orden_servicio_formulario osf,formulario f WHERE f.form_id=osf.form_id AND osf.cont_id=".$cont_id." AND osf.orse_tipo='".$orse_tipo."'"; 
	$res=$dbo->ExecuteQuery($query);

	if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
	$out['data']=$res['data'];
	Flight::json($out);

});

//REPORTES_OS
Flight::route('GET|POST /reportes_os', function(){    
	$out = array();
	$out['status'] = 1;
	$dbo = new MySQL_Database();

	$res = $dbo->ExecuteQuery("SELECT
								  repo_id
								  , repo_tipo_doc
								  , repo_nombre
								  , repo_descripcion
								  , repo_tabla
								  , repo_tabla_id
								  , repo_ruta
								  , repo_fecha_creacion
								  , repo_data
								  , usua_creador
								FROM 
								repositorio 
								WHERE repo_tipo_doc='reporte_os'
								ORDER BY repo_fecha_creacion desc
								LIMIT 5;");
	if( 0==$res['status'] ){
		Flight::json(array("status"=>0, "error"=>$res['error']));
		return;
	}
	$out['archivos'] = array();
	foreach($res['data'] AS $row){
	  if(!isset($out['archivos'][$row['repo_tipo_doc']])){
		$out['archivos'][$row['repo_tipo_doc']] = array();
	  }
	  array_push($out['archivos'][$row['repo_tipo_doc']],$row);
	}
	
	Flight::json($out);
});












































































































































































































/* FUNCION CREAR OS SOLO EN CASO DE CREACION DESDE DENUNCIA FALLA */
Flight::route('POST /contrato/@cont_id:[0-9]+/os/crear/@defa_id:[0-9]+', function($cont_id,$defa_id){
    $db = new MySQL_Database();
    $db->startTransaction();
    
    if(isset($_POST['form_id'])){
        $form_id = json_decode($_POST['form_id'],true);
    }else{
        $form_id=NULL;
    }

    $usua_creador = $_SESSION['user_id'];
    $empr_id = mysql_real_escape_string($_POST['empr_id']);
    $empl_id = mysql_real_escape_string($_POST['empl_id']);
    $sube_id = mysql_real_escape_string($_POST['sube_id']);
    $orse_tipo = mysql_real_escape_string($_POST['orse_tipo']);
    $orse_descripcion = mysql_real_escape_string($_POST['orse_descripcion']);
    $orse_indisponibilidad = mysql_real_escape_string($_POST['orse_indisponibilidad']);
    $orse_fecha_solicitud = mysql_real_escape_string($_POST['orse_fecha_solicitud']);
   
    $orse_tag = "";
    if(isset($_POST['orse_tag'])){
        $orse_tag = mysql_real_escape_string($_POST['orse_tag']);
    }

	$caaf_id = 0;

    if(isset($_POST['caaf_id']))  {
	    $caaf_id = mysql_real_escape_string($_POST['caaf_id']);
    }

    $query = "INSERT INTO orden_servicio SET
                cont_id='$cont_id',
                empr_id='$empr_id',
                empl_id='$empl_id',
                sube_id='$sube_id',
                orse_tipo='$orse_tipo',
                orse_descripcion='$orse_descripcion',
                orse_indisponibilidad='$orse_indisponibilidad',
                orse_fecha_solicitud='$orse_fecha_solicitud',
                orse_fecha_creacion=NOW(),
                usua_creador='$usua_creador',
                orse_tag='$orse_tag',
				caaf_aper_id='$caaf_id'";

    $res = $db->ExecuteQuery($query);

    if(0 == $res['status']){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $orse_id = $res['data'][0]['id'];

    /*QUERY QUE CREA RELACION ENTRE LA OS Y DENUNCIA FALLA CORRESPONDIENTE*/
    $query_rel_os_defa = "INSERT INTO rel_orden_denuncia SET
                            defa_id = $defa_id,
                            orse_id = $orse_id";

    $res = $db->ExecuteQuery($query_rel_os_defa);
    /*FIN QUERY RELACION*/
  
    /*QUERY CAMBIAR ESTADO DENUNCIA FALLA*/
    $res2 = $db->ExecuteQuery("UPDATE denuncia_falla SET defa_estado ='ATENDIDA' WHERE defa_id = $defa_id");

    if(0 == $res2['status'])  {
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res2['error']));
        return;
    }
  
    $defa_estado = 'ATENDIDA';

    if(is_array($form_id)){
        if(!(is_null($form_id))){
            for($i = 0;$i < count($form_id);$i++){
                $res = $db->ExecuteQuery("INSERT INTO rel_orden_servicio_formulario (orse_id,form_id) VALUES (".$orse_id.",".$form_id[$i].")");
                if(0 == $res['status']){
                    $db->Rollback();
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }
            }
        }
    }else{
        $res = $db->ExecuteQuery("INSERT INTO rel_orden_servicio_formulario (orse_id,form_id) VALUES (".$orse_id.",".$form_id.")");
        if(0 == $res['status']){
            $db->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
    }
    /* Flight::json(array("status"=>0, "error"=>count($form_id))); */
    /*SUBIDA DE ARCHIVOS*/
    if(isset($_FILES["archivos"])){
        $files = array();
        foreach($_FILES["archivos"] as $key1 => $value1){
            foreach($value1 as $key2 => $value2){
                $files[$key2][$key1] = $value2;
            }
        }

        $descriptions = json_decode($_POST['archivos_descripciones'],true);

        for($i=0; $i<count($files); $i++){
            $nombre = $files[$i]['name'];
            $descripcion = $descriptions[$i];

            $filename = date('ymdHis')."_os_".$orse_id."_".str_replace(" ","_",$nombre);
            $resFile = Upload::UploadFile($files[$i],$filename); 
            if(!$resFile['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resFile['error']));
                return;
            }
            
            $query = "INSERT INTO repositorio (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
                        VALUES(
                            'DOCUMENTO',
                            '".$nombre."',
                            '".$descripcion."',
                            'orden_servicio',
                            $orse_id,
                            '".$resFile['data']['filename']."',
                            NOW(),
                            NULL,
                            $usua_creador
                    )";

            $resUpload = $db->ExecuteQuery($query);

            if(0 == $resUpload['status']){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resUpload['error']));
                return;
            }
        }
    }
  
    /*CAUSA AFECTACIÓN*/
    /*AGREGAR EVENTOS*/
    $resEvent = Flight::AgregarEvento($db,"OS","CREADA",$orse_id,null);
    if(0 == $resEvent['status']){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $db->Commit();
    
    Flight::json(array("status"=>true,"orse_id"=>$orse_id,"defa_estado"=>$defa_estado));
});

?>

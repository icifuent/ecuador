<?php
    
  /*BANDEJA*/
  Flight::route('GET /defaMovil/bandeja/filtros', function($usuario){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    /*BUSCAR POR N° FALLA*/
    $res = $dbo ->ExecuteQuery("SELECT
                                    defa_id,
                                    defa_fecha_creacion
                                FROM
                                    denuncia_falla");

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['numeroDefa'] = $res['data'];

    /*USUARIO*/
    $res = $dbo->ExecuteQuery(" SELECT DISTINCT
                                    denuncia_falla.usua_id,
                                    usuario.usua_nombre
                                FROM
                                    usuario
                                inner join denuncia_falla on usuario.usua_id = denuncia_falla.usua_id
                                inner JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                                #inner JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))
                                ORDER BY usuario.usua_nombre");

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['defaUser'] = $res['data'];

    /*EMPRESA*/
    $res = $dbo->ExecuteQuery(" SELECT DISTINCT
                                    denuncia_falla.usua_id,
                                    empresa.empr_nombre,
                                    usuario.usua_nombre
                                FROM
                                    usuario
                                INNER JOIN denuncia_falla ON usuario.usua_id = denuncia_falla.usua_id
                                INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                                #inner JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))
                                INNER JOIN empresa ON (empresa.empr_id=usuario.empr_id)
                                ORDER BY usuario.usua_nombre");

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['defaEmpr'] = $res['data'];

    Flight::json($out);
});
/*FIN FLIGHT FUNCTION*/
/* LISTAR LAS DENUNCIAS FALLA EXISTENTES EN LA BBDD */
Flight::route('POST /contrato/@cont_id:[0-9]+/defaMovil/list(/@page:[0-9]+)', function($cont_id,$page){

    $filtro_usuario = array_merge($_GET,$_POST);
    $filtros = "TRUE ";

    if(isset($filtro_usuario['defa_id']) && "" != $filtro_usuario['defa_id']){
        $filtros .= " AND def.defa_id=".$filtro_usuario['defa_id']." ";
        unset( $filtro_usuario['defa_id'] );
    }

    if(isset($filtro_usuario['defaUser']) && "" != $filtro_usuario['defaUser'] && 0 != $filtro_usuario['defaUser']){
        $filtros .= " AND def.usua_id=".$filtro_usuario['defaUser']." ";
        unset( $filtro_usuario['defaUser'] );
    }

    if(isset($filtro_usuario['defaEmpr']) && "" != $filtro_usuario['defaEmpr'] && 0 != $filtro_usuario['defaEmpr']){
        $filtros .= " AND def.defa_empresa=".$filtro_usuario['defaEmpr']." ";
        unset( $filtro_usuario['defaEmpr'] );
    }

    $results_by_page = Flight::get('results_by_page');

    $out['status'] = 1;
    $out['pagina'] = $page;
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery(" SELECT
                                    SQL_CALC_FOUND_ROWS
                                    def.defa_id,
                                    def.usua_id,
                                    def.defa_latitud,
                                    def.defa_longitud,
                                    def.defa_fecha_creacion,
                                    def.defa_estado,
                                    def.defa_fecha_creacion,
                                    def.defa_contrato,
                                    usr.usua_nombre,
                                    emp.empr_nombre,
                                    (select rod.orse_id from rel_orden_denuncia rod where rod.defa_id = def.defa_id LIMIT 1) as orse_id
                                FROM
                                    denuncia_falla def
                                INNER JOIN usuario usr ON usr.usua_id = def.usua_id
                                INNER JOIN empresa emp ON emp.empr_id=usr.empr_id
                                WHERE def.defa_contrato = (SELECT c.cont_nombre FROM contrato c WHERE c.cont_id = $cont_id) AND $filtros" . ((is_null($page))?"":" ORDER BY def.defa_id DESC LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page)) );

    //$query = sprintf( $superquery, str_replace("SQL_CALC_FOUND_ROWS","",$query) );
    // $query.= $post_filtro;
    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['defaM'] = $res['data'];

    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() AS total");

    if(0 == $res_count['status']){
        Flight::json(array("status" => 0, "error" => $res_count['error']));
    }

    $out['total'] = intval($res_count['data'][0]['total']);
    $out['paginas'] = ceil($out['total']/$results_by_page);

    Flight::json($out);
});
/*FIN FLIGHT FUNCTION*/
/* OBTENER DETALLE DE ALGUNA DENUNCIA FALLA EN ESPECIFICA Y LISTADO DE EMPLAZAMIENTO CERCANOS */
Flight::route('GET /contrato/@cont_id:[0-9]+/defaMovil/detalle/@defa_id:[0-9]+', function($cont_id,$defa_id/*,$fecha*/){

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleFalla($dbo,$defa_id,$cont_id);

    if(0 == $res['status']){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $res = Flight::ObtenerDetalleEm($dbo,$latitud,$longitud);

    if(0 == $res['status']){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($out);
});
/*FIN FLIGHT FUNCTION*/
/* OBTENER DETALLE EN ESPECIFICO DE UNA DENUNCIA FALLA */
Flight::map('ObtenerDetalleFalla', function($db,$defa_id,$cont_id){
    $out = array();
    $out['status'] = 1;
    //$dbo = new MySQL_Database();
    $query ="SELECT
                defa_id
                ,defa_descripcion
                ,defa_especialidad
                ,defa_subespecialidad
                ,defa_estado
            FROM
                denuncia_falla
            WHERE defa_id = '$defa_id'";

    $res = $db->ExecuteQuery($query);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle de los código"));
    }

	$out['detFalla'] = $res['data'];
	
	$query = "SELECT 
  			    cont_id,
  			    cont_nombre
		      FROM contrato
		      WHERE cont_estado = 'ACTIVO'
		      AND cont_id <> $cont_id";

	$res = $db->ExecuteQuery($query);
	  
	if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle de los código"));
    }

  	$out['contratos'] = $res['data'];

    /*CABECERA*/
    $query1 =(" SELECT
                    def.defa_id,
                    def.usua_id,
                    def.defa_latitud,
                    def.defa_longitud,
                    def.defa_fecha_creacion,
                    usr.usua_nombre
                FROM denuncia_falla def
                INNER JOIN usuario usr ON usr.usua_id = def.usua_id
                WHERE def.defa_id = '$defa_id'
                LIMIT 1");

    $res = $db->ExecuteQuery($query1);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle cabecera qr"));
    }

    $out['cabFalla'] = $res['data'][0];

    /*QUERY
    $query3 = ("SELECT
                    defa_latitud,
                    defa_longitud
                FROM
                    denuncia_falla
                WHERE defa_id='$defa_id'");

    $res = $db->ExecuteQuery($query3);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle 3"));
    }

    $latitud = $res["data"][0]['defa_latitud'];
    $longitud = $res["data"][0]['defa_longitud'];*/

    $res = $db->ExecuteQuery("  SELECT paco_valor 
                                FROM parametros_contrato
                                WHERE paco_modu_nombre = 'CANT_EMPLAZAMIENTO_REPORTE_FALLA'
                                AND cont_id = $cont_id");

    $cant_registros = 5;
    if(0 < $res['status'] && 0 < $res['rows'] ){
        $cant_registros = $res['data'][0]['paco_valor'];
    }

    /* LISTADO EMPLAZAMIENTOS */
    $query2 ="  SELECT 
                    con.cont_nombre,
                    con.cont_id,
                    emp.empl_id,
                    emp.empl_nombre,
                    emp.empl_direccion,
                    emp.empl_observacion,
                    def.defa_id,
                    emp.empl_latitud,
                    def.defa_especialidad,
                    def.defa_subespecialidad,
                    def.defa_estado,
                    emp.empl_longitud,
                    def.defa_latitud,
                    def.defa_longitud,
                    (6371 * ACOS(SIN(RADIANS(empl_latitud)) * SIN(RADIANS(defa_latitud)) + COS(RADIANS(empl_longitud - defa_longitud)) * COS(RADIANS(empl_latitud)) * COS(RADIANS(defa_latitud)))) AS distance
                FROM
                    denuncia_falla def
                        INNER JOIN
                    contrato con ON (con.cont_nombre = def.defa_contrato)
                        INNER JOIN
                    rel_contrato_emplazamiento rel ON (rel.cont_id = con.cont_id)
                        INNER JOIN
                    emplazamiento emp ON (emp.empl_id = rel.empl_id
                        AND emp.empl_estado = 'ACTIVO')
                WHERE
                    defa_id = $defa_id
                ORDER BY distance ASC
                LIMIT $cant_registros";

    $res = $db->ExecuteQuery($query2);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle emplazamiento"));
    }

    $out['listEmp'] = $res['data'];

        /* DETALLES EMPLAZAMIENTO SELECCIONADO */


        // Datos Emplazamiento
        /* $query2 ="SELECT con.cont_nombre,
                         con.cont_id,
                         emp.empl_nombre,
                         emp.empl_direccion,
                         emp.empl_observacion,
                         def.defa_id,
                         emp.empl_latitud,
                         def.defa_especialidad,
                         def.defa_alarma,
                         def.defa_indisponibilidad,
                         def.defa_estado,
                         def.defa_empresa,
                         emp.empl_longitud,
                         def.defa_latitud,

                         def.defa_longitud,(6371 * ACOS(
                                    SIN(RADIANS(empl_latitud)) * SIN(RADIANS($latitud))
                                     + COS(RADIANS(empl_longitud - $longitud)) * COS(RADIANS(empl_latitud))
                                     * COS(RADIANS($latitud))
                                    )
                                           ) AS distance
                        FROM emplazamiento emp
                        INNER JOIN denuncia_falla def
                        INNER JOIN rel_contrato_emplazamiento rel on (rel.empl_id = emp.empl_id)
                        INNER JOIN contrato con on (con.cont_id = rel.cont_id)

                        where defa_id=$defa_id
                        HAVING distance < 0.350
                        ORDER BY distance ASC
                        limit 1";



        $res = $db->ExecuteQuery($query2);
        if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle emplazamiento"));
        $out['detEmp'] = $res['data'];*/

        /*$query9 ="SELECT DISTINCT
                                    denuncia_falla.usua_id,
                                    empresa.empr_nombre,
                                    denuncia_falla.defa_empresa,
                                    usuario.usua_nombre
                                    FROM
                                    usuario
                                    inner join denuncia_falla on usuario.usua_id = denuncia_falla.usua_id
                                    inner JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                                   #inner JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id  AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))
                                    inner join empresa on (empresa.empr_id=usuario.empr_id)
                                    ORDER BY usuario.usua_nombre";

        $res = $db->ExecuteQuery($query9);
        if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle emplazamiento"));
        $out['detEmp'] = $res['data'];*/

    /*EVIDENCIAS - IMAGENES*/
    $query4 = ("SELECT 
                    def.defa_id,
                    def.usua_id,
                    rep.repo_id,
                    rep.repo_tipo_doc,
                    rep.repo_ruta,
                    rep.repo_estado
                FROM
                    denuncia_falla def
                INNER JOIN
                    repositorio rep ON rep.repo_tabla_id = def.defa_id
                AND rep.repo_tabla = 'denuncia_falla'
                AND rep.repo_tipo_doc = 'IMAGE'
                AND rep.repo_estado = 'ACTIVO'
                WHERE
                    defa_id = $defa_id");

    $res = $db->ExecuteQuery($query4);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle "));
    }

    $out['eviFalla'] = $res['data'];

    /*EVIDENCIAS VIDEO*/
    $query8 = ("SELECT 
                    def.defa_id,
                    def.usua_id,
                    rep.repo_id,
                    rep.repo_tipo_doc,
                    rep.repo_ruta
                FROM
                    denuncia_falla def
                INNER JOIN
                    repositorio rep ON rep.repo_tabla_id = def.defa_id
                AND rep.repo_tabla = 'denuncia_falla'
                AND rep.repo_tipo_doc = 'VIDEO'
                AND rep.repo_estado = 'ACTIVO'
                WHERE
                    defa_id = $defa_id");

    $res = $db->ExecuteQuery($query8);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle "));
    }

    $out['eviVideo'] = $res['data'];

    /*EVIDENCIAS AUDIO*/
    $query9 = ("SELECT 
                    def.defa_id,
                    def.usua_id,
                    rep.repo_id,
                    rep.repo_tipo_doc,
                    rep.repo_ruta
                FROM
                    denuncia_falla def
                INNER JOIN
                    repositorio rep ON rep.repo_tabla_id = def.defa_id
                AND rep.repo_tabla = 'denuncia_falla'
                AND rep.repo_tipo_doc = 'AUDIO'
                AND rep.repo_estado = 'ACTIVO'
                WHERE
                    defa_id = $defa_id");

    $res = $db->ExecuteQuery($query9);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle "));
    }

    $out['eviAudio'] = $res['data'];

    Flight::json($out);
});
/*FIN FLIGHT FUNCTION*/

/*CODIGO AÑADIDO PARA LOGICA DENUNCIA FALLA -> ORDEN SERVICIO*/
Flight::map('datosCrearOS', function($db,$defa_id,$empl_id){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    /*VALIDAR DATOS EMPL_ID Y DEFTFALA_ID*/
    $query ="SELECT
                defa_id
                ,defa_descripcion
                ,defa_especialidad
                ,defa_subespecialidad
                ,defa_estado
            FROM
                denuncia_falla
            WHERE defa_id = '$defa_id'";

    $res = $db->ExecuteQuery($query);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle de los código"));
    }

    $out['detFalla'] = $res['data'];

});
/*FIN FLIGHT MAP*/
/* DETALLE DE LA DENUNCIA FALLA EN ESPECIFICO, JUNTO A LAS EVIDENCIAS Y EMPLAZAMIENTO SELECCIONADO */
Flight::route('GET /contrato/@cont_id:[0-9]+/defaMovil/detallefalla/datosOS/@defa_id:[0-9]+/emplazamiento/@empl_id:[0-9]+', function($cont_id,$defa_id,$empl_id){

    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $modu_id = Flight::get('OS_MODU_ID');

    /*QUERY PARA TRAER DETALLE DE DENUNCIA FALLA*/
    $query = (" SELECT
                    def.defa_id,
                    def.usua_id,
                    def.defa_latitud,
                    def.defa_longitud,
                    def.defa_fecha_creacion,
                    usr.usua_nombre,
                    def.defa_descripcion
                FROM denuncia_falla def
                INNER JOIN usuario usr ON usr.usua_id = def.usua_id
                WHERE def.defa_id = '$defa_id'
                LIMIT 1");

    $res = $dbo->ExecuteQuery($query);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle cabecera qr"));
    }

    $out['cabFalla'] = $res['data'];

    /*QUERY PARA TRAER DETALLE DE EMPLAZAMIENTO SELECCIONADO EN VISTA ANTERIOR DENUNCIA FALLA*/
    $query = (" SELECT
                    emp.empl_id,
                    emp.empl_nombre,
                    emp.empl_nemonico,
                    empcl.clas_nombre,
                    (SELECT
                        group_concat(tec.tecn_nombre) tecn_nombre
                    FROM tecnologia tec
                    INNER JOIN rel_emplazamiento_tecnologia relemtec ON relemtec.tecn_id = tec.tecn_id
                    WHERE relemtec.empl_id = emp.empl_id
                    GROUP BY relemtec.empl_id
                    ) AS tecn_nombre,
                    emp.empl_direccion,
                    com.comu_nombre,
                    reg.regi_nombre,
                    (SELECT
                        group_concat(zo.zona_nombre) zona_nombre
                    FROM zona zo
                    INNER JOIN rel_zona_emplazamiento relzonem ON relzonem.zona_id = zo.zona_id
                    AND zo.zona_tipo = 'CONTRATO'
                    AND zo.zona_estado = 'ACTIVO'
                    AND zo.cont_id = 1
                    WHERE relzonem.empl_id = emp.empl_id
                    GROUP BY relzonem.empl_id
                    ) AS zona_nombre,
                    emp.empl_macrositio,
                    emp.empl_subtel,
                    emp.empl_espacio,
                    emp.empl_tipo_acceso,
                    emp.empl_observacion,
                    emp.usua_creador
                FROM emplazamiento emp
                INNER JOIN emplazamiento_clasificacion empcl ON empcl.clas_id = emp.clas_id
                INNER JOIN comuna com ON com.comu_id = emp.comu_id
                INNER JOIN provincia pro ON pro.prov_id = com.prov_id
                INNER JOIN region reg ON reg.regi_id = pro.regi_id
                WHERE emp.empl_id = $empl_id");

    $res = $dbo->ExecuteQuery($query);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle cabecera qr"));
    }

    $out['emplazamiento'] = $res['data'];

    /*CÓDIGO DE INFORMACIÓN CAJAS DE TEXTO CREAR ORDEN SERVICIO*/
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM orden_servicio WHERE Field = 'orse_tipo'" );

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
  
    $out['tipos'] = explode("','", $matches[1]);

    $res = $dbo->ExecuteQuery(" SELECT 
                                    e.empr_id, e.empr_nombre, rce.coem_tipo
                                FROM
                                    empresa e,
                                    rel_contrato_empresa rce
                                WHERE
                                    rce.cont_id = $cont_id
                                AND e.empr_estado = 'ACTIVO'
                                AND rce.coem_estado = 'ACTIVO'
                                AND rce.coem_tipo != 'CLIENTE'
                                AND e.empr_id = rce.empr_id
                                AND e.empr_id = rce.empr_id
                                ORDER BY e.empr_nombre DESC");

    if(0 == $res['status']){
        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    }

    $out['empresa'] = $res['data'];

    $res = $dbo->ExecuteQuery(" SELECT
                                    e.espe_id,
                                    e.espe_nombre,
                                    df.defa_id
                                FROM
                                    especialidad e,
                                    denuncia_falla df
                                WHERE
                                    e.espe_nombre = df.defa_especialidad
                                    and df.defa_id = $defa_id
                                    and e.cont_id = $cont_id");
  
    if(0 == $res['status']){
        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    }

    $out['especialidad'] = $res['data'][0];

    $res = $dbo->ExecuteQuery("SELECT
                                especialidad.espe_id, espe_nombre
                             FROM especialidad
                             INNER JOIN rel_modulo_especialidad ON (rel_modulo_especialidad.espe_id=especialidad.espe_id AND
                                                                       rel_modulo_especialidad.modu_id=$modu_id)
                             WHERE espe_estado='ACTIVO' AND cont_id=$cont_id");

    if(0 == $res['status'] )  {
        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    }

    $out['especialidades'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT
                                  subespecialidad.sube_id,
                                  sube_nombre,
                                  especialidad.espe_id
                             FROM subespecialidad
                             INNER JOIN especialidad ON (especialidad.espe_id=subespecialidad.espe_id)
                             INNER JOIN rel_modulo_especialidad ON (rel_modulo_especialidad.espe_id=especialidad.espe_id AND
                                                                       rel_modulo_especialidad.modu_id=$modu_id)
                             WHERE espe_estado='ACTIVO' AND sube_estado='ACTIVO'");

    if(0 == $res['status'] )  {
        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    }

    $out['subespecialidades'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT
                                    se.sube_id,
                                    se.sube_nombre,
                                    df.defa_id
                                FROM
                                    subespecialidad se,
                                    especialidad e,
                                    denuncia_falla df
                                WHERE
                                    se.sube_nombre = df.defa_subespecialidad
                                and se.espe_id = e.espe_id
                                and df.defa_id = $defa_id
                                and e.cont_id = $cont_id");
  
    if(0 == $res['status']){
        Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    }

    $out['subespecialidad'] = $res['data'][0];

    /*CAUSA AFECTACION*/
    $res = $dbo->ExecuteQuery(" SELECT caaf_id,
                                    caaf_nombre
                                FROM causa_afectacion_cierre
                                WHERE caaf_estado ='ACTIVO'");

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['causa_afectacion'] = $res['data'];

    $res = $dbo->ExecuteQuery(" SELECT *
                                    FROM orden_servicio_definicion_formulario osf,formulario f
                                WHERE f.form_id=osf.form_id AND osf.cont_id=$cont_id AND f.form_estado='ACTIVO'");

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['def_formulario'] = $res['data'];

    $res = $dbo->ExecuteQuery(" SELECT
                                    form_id,
                                    form_nombre
                                FROM formulario
                                WHERE cont_id = $cont_id AND espe_id is NULL
                                UNION ALL
                                SELECT
                                    form_id,
                                    form_nombre
                                FROM formulario
                                WHERE cont_id = $cont_id AND form_estado='ACTIVO' AND form_id NOT IN (1,2,3,4)");

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    }

    $out['formularios'] = $res['data'];
    
    /*EVIDENCIAS - IMAGENES*/
    $query4 = ("SELECT 
                    def.defa_id,
                    def.usua_id,
                    rep.repo_id,
                    rep.repo_tipo_doc,
                    rep.repo_ruta,
                    rep.repo_estado
                FROM
                    denuncia_falla def
                INNER JOIN
                    repositorio rep ON rep.repo_tabla_id = def.defa_id
                AND rep.repo_tabla = 'denuncia_falla'
                AND rep.repo_tipo_doc = 'IMAGE'
                AND rep.repo_estado = 'ACTIVO'
                WHERE
                    defa_id = $defa_id");

    $res = $dbo->ExecuteQuery($query4);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle "));
    }

    $out['eviFalla'] = $res['data'];

    /*EVIDENCIAS VIDEO*/
    $query8 = ("SELECT 
                    def.defa_id,
                    def.usua_id,
                    rep.repo_id,
                    rep.repo_tipo_doc,
                    rep.repo_ruta
                FROM
                    denuncia_falla def
                INNER JOIN
                    repositorio rep ON rep.repo_tabla_id = def.defa_id
                AND rep.repo_tabla = 'denuncia_falla'
                AND rep.repo_tipo_doc = 'VIDEO'
                AND rep.repo_estado = 'ACTIVO'
                WHERE
                    defa_id = $defa_id");

    $res = $dbo->ExecuteQuery($query8);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle "));
    }

    $out['eviVideo'] = $res['data'];

    /*EVIDENCIAS AUDIO*/
    $query9 = ("SELECT 
                    def.defa_id,
                    def.usua_id,
                    rep.repo_id,
                    rep.repo_tipo_doc,
                    rep.repo_ruta
                FROM
                    denuncia_falla def
                INNER JOIN
                    repositorio rep ON rep.repo_tabla_id = def.defa_id
                AND rep.repo_tabla = 'denuncia_falla'
                AND rep.repo_tipo_doc = 'AUDIO'
                AND rep.repo_estado = 'ACTIVO'
                WHERE
                    defa_id = $defa_id");

    $res = $dbo->ExecuteQuery($query9);

    if(0 == $res['status']){
        Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle "));
    }

    $out['eviAudio'] = $res['data'];

    /*VARIABLE $OUT SE CONVIERTE A JSON PARA SU UTILIZACION EN VISTA HB*/
    Flight::json($out);
});
/* FUNCION PARA CAMBIAR DE ESTADO A 'CANCELADA' UNA DENUNCIA FALLA EN ESPECIFICA */
Flight::route('POST /defaMovil/cancelar/@defa_id:[0-9]+', function($defa_id){
    $out = array();
    $dbo = new MySQL_Database();
    $even_evento = 'CANCELADO';
    $even_data = $defa_id;
    $even_modulo = 'DEFA';

    $dbo->startTransaction();

    $res=$dbo->ExecuteQuery("UPDATE denuncia_falla SET defa_estado = 'CANCELADA' WHERE defa_id=$defa_id");

  	if(0 == $res['status']){
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => "No se pudo cancelar denuncia falla N° ",$defa_id));
    }else if(1 == $res['status']){
        $dbo->Commit();
        Flight::json(array("status" => 1, "success" => "Denuncia Falla N° " + $defa_id + " cambió a estado CANCELADA", "defa_id" => $defa_id)); 
    }
});
/* FUNCION PARA DERIVAR A OTRO CONTRATO UNA DENUNCIA FALLA EN ESPECIFICA */
Flight::route('POST /defaMovil/derivar/@defa_id:[0-9]+/contrato/@cont_nombre', function($defa_id,$cont_nombre){
    $out = array();
    $dbo = new MySQL_Database();
    $even_evento = 'DERIVADO';
    $even_data = $defa_id;
    $even_modulo = 'DEFA';
    //$even_comentario = data.comentario;
	$dbo->startTransaction();
	
	$res = $dbo->ExecuteQuery("UPDATE denuncia_falla SET defa_contrato='$cont_nombre' WHERE defa_id=$defa_id");

    if(0 == $res['status']){
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => "No se pudo derivar denuncia falla N° ",$defa_id));
    }else if(1 == $res['status']){
        $dbo->Commit();
        $resEvent = Flight::AgregarEvento($dbo,$even_modulo,$even_evento,$defa_id,$even_data);
        if(0 == $resEvent['status']){
            Flight::Log("[SIOM] AgregarEvento: ".$resEvent['error']);
        }
        Flight::json(array("status" => 1, "success" => "Denuncia Falla N° " + $defa_id + " cambió a contrato " + $cont_nombre, "defa_id" => $defa_id));
    }
});
/* FUNCION PARA NO VISUALIZAR IMAGENES EN LA CREACION DE OS DESDE REPORTE FALLA */
Flight::route('POST /defaMovil/ocultarEvidencia/repositorio/@repo_id:[0-9]+', function($repo_id){
    $out = array();
    $dbo = new MySQL_Database();
    $dbo->startTransaction();
	
    $res = $dbo->ExecuteQuery("UPDATE repositorio SET repo_estado ='NOACTIVO' WHERE repo_id ='$repo_id'");
    
    if(0 == $res['status']){
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => "No se logro efectuar ocultar imagen"));
    } else if(1 == $res['status']){
        $dbo->Commit();
        Flight::json(array("status" => 1, "success" => "Imagen ocultada"));
    }
});

?>

<?php

Flight::map('ValidateAppPruebasBeta', function($imei){
    //ValidateAppPruebasBeta

    return true;
});


Flight::map('ObtenerOSAppPruebasBeta', function($db,$orse_id){
    $query = "SELECT
                os.*,
                emp.*,
                e.empr_nombre,
                sesp.sube_nombre,
                esp.espe_nombre,
                c.comu_nombre,
                GROUP_CONCAT(t.tecn_nombre) as tecn_nombre,
                z.zona_id, z.zona_nombre,
                r.regi_id, r.regi_nombre "
            . "FROM orden_servicio os, empresa e, subespecialidad sesp, especialidad esp, emplazamiento emp, comuna c, region r, provincia p, tecnologia t, rel_emplazamiento_tecnologia ret, zona z, rel_zona_emplazamiento rze "
            . "WHERE "
                . "os.orse_id=" . $orse_id . " "
                . "AND os.orse_estado NOT IN ('RECHAZADA','APROBADA','NOACTIVO','ANULADA') "
                . "AND os.empr_id=e.empr_id "
                . "AND os.empl_id=emp.empl_id "
                . "AND os.sube_id=sesp.sube_id "
                . "AND sesp.espe_id=esp.espe_id "
                . "AND e.comu_id=c.comu_id "
                . "AND p.prov_id = c.prov_id "
                . "AND r.regi_id = p.regi_id "
                . "AND ret.empl_id=emp.empl_id "
                . "AND ret.tecn_id=t.tecn_id "
                . "AND rze.empl_id = emp.empl_id "
                . "AND rze.zona_id = z.zona_id "
                . "AND z.cont_id = os.cont_id "
                . "AND z.zona_tipo = 'CONTRATO' ";
    return $db->ExecuteQuery($query);
});

Flight::map('ObtenerMNTAppPruebasBeta', function($db,$mant_id){
    $query = "  SELECT
                    mnt.*,
                    emp.*,
                    mp.*,
                    pe.peri_id,
                    pe.peri_nombre,
                    e.empr_nombre,
                    esp.espe_nombre,
                    co.comu_nombre,
                    GROUP_CONCAT(t.tecn_nombre) as tecn_nombre,
                    z.zona_id, z.zona_nombre,
                    r.regi_id, r.regi_nombre
                FROM
                    mantenimiento mnt,
                    empresa e,
                    rel_contrato_empresa rce,
                    especialidad esp,
                    emplazamiento emp,
                    comuna co,
                    region r,
                    provincia p,
                    tecnologia t,
                    rel_emplazamiento_tecnologia ret,
                    zona z,
                    rel_zona_emplazamiento rze,
                    periodicidad pe,
                    rel_contrato_periodicidad rcp,
                    mantenimiento_periodos mp
                WHERE
                    mnt.mant_id= $mant_id
                    AND mnt.mant_estado NOT IN ('APROBADA','RECHAZADA','FINALIZADA','ANULADA','NO REALIZADO')
                    AND rce.cont_id=mnt.cont_id
                    AND rce.empr_id=e.empr_id
                    AND mnt.empr_id=e.empr_id

                    AND mnt.empl_id=emp.empl_id
                    AND mnt.espe_id=esp.espe_id

                    AND emp.comu_id=co.comu_id
                    AND p.prov_id = co.prov_id
                    AND r.regi_id = p.regi_id
                    AND ret.empl_id=emp.empl_id
                    AND ret.tecn_id=t.tecn_id
                    AND rze.empl_id = emp.empl_id
                    AND rze.zona_id = z.zona_id
                    AND z.cont_id = mnt.cont_id
                    AND z.zona_tipo = 'CONTRATO'

                    AND mnt.mape_id = mp.mape_id
                    AND rcp.peri_id = pe.peri_id
                    AND rcp.cont_id = mnt.cont_id
                    AND mp.rcpe_id = rcp.rcpe_id
                ;";
    return $db->ExecuteQuery($query);
});

Flight::map('ObtenerINSPAppPruebasBeta', function($db,$insp_id){
    $query = "  SELECT
                    inspeccion.*,
                    emplazamiento.*,
                    especialidad.espe_nombre
                FROM
                    inspeccion
                INNER JOIN emplazamiento ON (inspeccion.empl_id=emplazamiento.empl_id)
                INNER JOIN especialidad ON (inspeccion.espe_id=especialidad.espe_id)
                WHERE
                    inspeccion.insp_id= $insp_id
                    AND inspeccion.insp_estado NOT IN ('APROBADA','RECHAZADA','FINALIZADA','ANULADA','NO REALIZADO')
                ;";
    return $db->ExecuteQuery($query);
});


Flight::map('ObtenerFormulariosMFAppPruebasBeta', function($db,$form_ids){
    $forms          = array();
    $forms_pages    = array();
    $forms_controls = array();

    $form_ids_str = implode("','",array_unique($form_ids));


    //Cargar formularios
    $res = $db->ExecuteQuery("SELECT
                                form_id             AS form_id,
                                IFNULL(cont_id,0)    AS clie_id,
                                form_nombre         AS form_name,
                                form_descripcion    AS form_description,
                                '1'                 AS form_version,
                                form_tipo           AS form_type,
                                ''                  AS form_emails,
                                ''                  AS form_webservice,
                                form_opciones       AS form_options,
                                '1'                 AS form_active
                              FROM
                                formulario
                              WHERE form_id IN ('$form_ids_str') 
							  AND form_estado='ACTIVO'");
    if($res['status']==0){
        return array("status"=>false, "error"=>$res['error']);
    }

    if(0<$res['rows']){
        array_push($forms,array_keys($res['data'][0]));
        foreach($res['data'] as $row){
            array_push($forms,array_values($row));
        }
    }
    else{
        return array("status"=>true,"forms"=>$forms,"forms_pages"=>$forms_pages,"forms_controls"=>$forms_controls);
    }

    //Cargar páginas
    $res = $db->ExecuteQuery("SELECT
                                fogr_id         AS fopa_id,
                                form_id         AS form_id,
                                fogr_nombre     AS fopa_name,
    				IFNULL(fogr_orden,0) AS fopa_orden,
                                fogr_opciones   AS fopa_options
                              FROM
                                formulario_grupo
                              WHERE form_id IN ('$form_ids_str')
			      AND fogr_estado='ACTIVO'
                              ORDER BY fopa_orden,fogr_id");
    if($res['status']==0){
        return array("status"=>false, "error"=>$res['error']);
    }

    $fopa_ids = array();
    if(0<$res['rows']){
        array_push($forms_pages,array_keys($res['data'][0]));
        foreach($res['data'] as $row){
            array_push($fopa_ids,$row['fopa_id']);
            array_push($forms_pages,array_values($row));
        }
    }
    else{
        return array("status"=>true,"forms"=>$forms,"forms_pages"=>$forms_pages,"forms_controls"=>$forms_controls);
    }

    //Cargar controles
    $fopa_ids_str = implode("','",array_unique($fopa_ids));

    $res = $db->ExecuteQuery("SELECT
                                foit_id         AS foco_id,
                                fogr_id         AS fopa_id,
                                foit_tipo       AS foco_type,
                                ''              AS foco_tag,
                                foit_nombre     AS foco_label,
                                foit_requerido  AS foco_required,
                                foit_orden      AS foco_order,
                                foit_opciones   AS foco_options,
                                '1'             AS foco_active
                              FROM
                                formulario_item
                              WHERE fogr_id IN ('$fopa_ids_str')
			      AND foit_estado='ACTIVO'");
    if($res['status']==0){
        return array("status"=>false, "error"=>$res['error']);
    }

    if(0<$res['rows']){
        array_push($forms_controls,array_keys($res['data'][0]));
        foreach($res['data'] as $row){
            array_push($forms_controls,array_values($row));
        }
    }

    return array("status"=>true,"forms"=>$forms,"forms_pages"=>$forms_pages,"forms_controls"=>$forms_controls);
});


Flight::map('ObtenerTareasMFAppPruebasBeta', function($db,$tareas){
    global $DEF_CONFIG;
    $assignments        = array();
    $assignments_status = array();
    $contracts          = array();
    $services           = array();
    $forms              = array();
    $forms_pages        = array();
    $forms_controls     = array();

    $cont_lock = $DEF_CONFIG["geofence_enabled"]?("1"):("0");

    if(0<count($tareas)){
        array_push($assignments,array("assi_id","assi_name"));
        array_push($assignments_status,array("asst_id","assi_id","cont_id","serv_id","asst_goods","asst_time","asst_comment","asst_status"));
        array_push($contracts,array("cont_id","cust_name","cont_code","cont_name","cont_address","cont_latitude","cont_longitude","cont_geofence","cont_contact_name","cont_contact_phone","cont_contact_email","cont_lock","cont_descripcion"));
        //array_push($contracts,array("cont_id","cust_name","cont_code","cont_name","cont_address","cont_latitude","cont_longitude","cont_geofence","cont_contact_name","cont_contact_phone","cont_contact_email","cont_lock"));
        array_push($services,array("serv_id","serv_name","serv_description","form_id","form_orden"));

        $form_ids = array();
        $service_ids = array();

        $oss  = 0;
        $mnts = 0;
        $insps = 0;

        //$cont_id = 1;
        foreach($tareas as $t){
            $tare_id = $t['tare_id'];
            $cont_id = $tare_id;

            switch($t['tare_modulo']){
                case "OS":{
                    $assi_id = 1;
                    switch($t['tare_tipo']){
                        case "VISITAR_SITIO":{
                            $res = Flight::ObtenerOSAppPruebasBeta($db,$t['tare_id_relacionado']);
                            if( $res['status']==0 ){
                                return array("status"=>false, "error"=>$res2['error']);
                            }
                            if($res['rows']==0 ){
                                continue;
                            }
                            $os = $res['data'][0];
                            $orse_id = $os['orse_id'];
                            if($orse_id==null || $orse_id==""){
                                continue;
                            }

                            $res = $db->ExecuteQuery("SELECT
			    				distinct
                                                        formulario.form_id,
                                                        formulario.form_nombre
								 FROM rel_orden_servicio_formulario rel
								 INNER JOIN orden_servicio ON (orden_servicio.orse_id = rel.orse_id)
								 INNER JOIN formulario ON (formulario.form_id=rel.form_id AND formulario.form_estado='ACTIVO'
								                      /*AND formulario.cont_id = orden_servicio.cont_id*/)
								 WHERE rel.orse_id=$orse_id
												 
                                                     ORDER BY rosf_id");
                            if( $res['status']==0 ){
                                return array("status"=>false, "error"=>$res['error']);
                            }

                            $forms = array();
                            if($res['rows']==0){ //Versión anterior
                                foreach($DEF_CONFIG['os']['formularios'] as $form_id => $form_nombre){
                                    array_push($forms,array("form_id"=>$form_id,"form_nombre"=>$form_nombre));
                                }
                            }
                            else{
                                $forms = $res['data'];
                                $form_ingreso = NULL;
                                $form_salida  = NULL;

                                foreach ($forms as $idx => $form){
                                    if( $form['form_id'] == $DEF_CONFIG['os']['formulario_ingreso_id'] ) {
                                        $form_ingreso = $form;
                                        unset($forms[$idx]);
                                        break;
                                    }
                                }
                                foreach ($forms as $idx => $form){
                                    if( $form['form_id'] == $DEF_CONFIG['os']['formulario_salida_id'] ) {
                                        $form_salida = $form;
                                        unset($forms[$idx]);
                                        break;
                                    }
                                }

                                if( $form_ingreso!=NULL ) array_unshift($forms,$form_ingreso);
                                if( $form_salida!=NULL ) array_push($forms,$form_salida);
                            }

                            foreach ($forms as $form)
                            {
                                $form_id     = $form["form_id"];
                                $form_nombre = $form["form_nombre"];

                                $db->startTransaction();
                                $res = $db->ExecuteQuery("SELECT
                                                            rtfr_id,
                                                            rtfr_estado
                                                          FROM
                                                            rel_tarea_formulario_respuesta
                                                          WHERE
                                                            tare_id=$tare_id AND form_id=$form_id");
                                if($res['status']==0){
                                    $db->Rollback();
                                    return array("status"=>false, "error"=>$res['error']);
                                }
                                if(0<$res['rows']){
                                    $estado = $res['data'][0]['rtfr_estado'];
                                    if($estado==""){
                                        $estado = "TODO";
                                    }

                                    array_push($assignments_status,array($res['data'][0]['rtfr_id'],$assi_id,$cont_id,$form_id,0,"",$os['orse_descripcion'],$estado));
                                }
                                else{
                                    $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta SET
                                                              tare_id=$tare_id,form_id=$form_id,rtfr_accion='$form_nombre'");
                                    if($res['status']==0){
                                        $db->Rollback();
                                        return array("status"=>false, "error"=>$res['error']);
                                    }
                                    array_push($assignments_status,array($res['data'][0]['id'],$assi_id,$cont_id,$form_id,0,"",$os['orse_descripcion'],"TODO"));
                                }
                                $db->Commit();

                                if(!in_array(strval($form_id),$service_ids)){

                                    $res = $db->ExecuteQuery("SELECT
                                                            IFNULL(form_orden,1) as fogr_orden
                                                          FROM
                                                            formulario
                                                          WHERE form_id=$form_id
														  AND form_estado='ACTIVO'
                                                          ORDER BY IFNULL(form_orden,1),form_id");
                                    array_push($services,array(strval($form_id),$form_nombre,"",strval($form_id), $res['data'][0]['fogr_orden']));
                                    array_push($service_ids,strval($form_id));
                                }


                                array_push($form_ids,$form_id);
                            }

                            //array_push($contracts,array($cont_id,$os["orse_tipo"]." - ".$os["espe_nombre"],"OS Nº".$os['orse_id'],$os['empl_nombre'],$os['empl_direccion'],$os['empl_latitud'],$os['empl_longitud'],$os['empl_distancia'],$os['empl_observacion'],"","",$cont_lock));
                            array_push($contracts,array($cont_id,$os["orse_tipo"]." - ".$os["espe_nombre"],"OS Nº".$os['orse_id'],$os['empl_nombre'],$os['empl_direccion'],$os['empl_latitud'],$os['empl_longitud'],$os['empl_distancia'],$os['empl_observacion'],"","",$cont_lock, $os['orse_descripcion']));
                            $oss++;
                            break;
                        }
                    }
                    break;
                }
                case "MNT":{
                    $assi_id = 2;

                    switch($t['tare_tipo']){
                        case "VISITAR_SITIO":{
                            $res = Flight::ObtenerMNTAppPruebasBeta($db,$t['tare_id_relacionado']);
                            if( $res['status']==0 ){
                                return array("status"=>false, "error"=>$res['error']);
                            }
                            if($res['rows']==0 ){
                                continue;
                            }
                            $mnt = $res['data'][0];
                            $mant_id = $mnt['mant_id'];
                            if($mant_id==null || $mant_id==""){
                                continue;
                            }


                            $res = $db->ExecuteQuery("SELECT
			    				distinct
                                                        formulario.form_id,
                                                        formulario.form_nombre
                                                     FROM rel_mantenimiento_formulario rel
						     INNER JOIN mantenimiento ON (mantenimiento.mant_id = rel.mant_id)
                                                     INNER JOIN formulario ON(formulario.form_id=rel.form_id 
													 AND formulario.form_estado='ACTIVO'
						     /*AND formulario.cont_id=mantenimiento.cont_id*/)
                                                     WHERE rel.mant_id=$mant_id
                                                     ORDER BY rmaf_id");
                            if( $res['status']==0 ){
                                return array("status"=>false, "error"=>$res['error']);
                            }

                            $forms = $res['data'];
                            foreach ($forms as $form)
                            {
                                $form_id     = $form["form_id"];
                                $form_nombre = $form["form_nombre"];

                                $db->startTransaction();
                                $res = $db->ExecuteQuery("SELECT
                                                            rtfr_id,
                                                            rtfr_estado
                                                          FROM
                                                            rel_tarea_formulario_respuesta
                                                          WHERE
                                                            tare_id=$tare_id AND form_id=$form_id");
                                if($res['status']==0){
                                    $db->Rollback();
                                    return array("status"=>false, "error"=>$res['error']);
                                }
                                if(0<$res['rows']){
                                    $estado = $res['data'][0]['rtfr_estado'];
                                    if($estado==""){
                                        $estado = "TODO";
                                    }

                                    array_push($assignments_status,array($res['data'][0]['rtfr_id'],$assi_id,$cont_id,$form_id,0,"","",$estado));
                                }
                                else{
                                    $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta SET
                                                              tare_id=$tare_id,form_id=$form_id,rtfr_accion='$form_nombre'");
                                    if($res['status']==0){
                                         $db->Rollback();
                                        return array("status"=>false, "error"=>$res['error']);
                                    }
                                    array_push($assignments_status,array($res['data'][0]['id'],$assi_id,$cont_id,$form_id,0,"","","TODO"));
                                }
                                $db->Commit();


                                if(!in_array(strval($form_id),$service_ids)){

                                    $res = $db->ExecuteQuery("SELECT
                                                            IFNULL(form_orden,1) as fogr_orden
                                                          FROM
                                                            formulario
                                                          WHERE form_id='$form_id'
							  AND form_estado='ACTIVO'
                                                          ORDER BY IFNULL(form_orden,1),form_id");

                                    array_push($services,array(strval($form_id),$form_nombre,"",strval($form_id),$res['data'][0]['fogr_orden']));
                                    array_push($service_ids,strval($form_id));
                                }

                                array_push($form_ids,$form_id);
                            }
                            //array_push($contracts,array($cont_id,$mnt["peri_nombre"]." - ".$mnt["espe_nombre"],"MNT Nº".$mnt['mant_id'],$mnt['empl_nombre'],$mnt['empl_direccion'],$mnt['empl_latitud'],$mnt['empl_longitud'],$mnt['empl_distancia'],$mnt['empl_observacion'],"","",$cont_lock));
                            array_push($contracts,array($cont_id,$mnt["peri_nombre"]." - ".$mnt["espe_nombre"],"MNT Nº".$mnt['mant_id'],$mnt['empl_nombre'],$mnt['empl_direccion'],$mnt['empl_latitud'],$mnt['empl_longitud'],$mnt['empl_distancia'],$mnt['empl_observacion'],"","",$cont_lock,$mnt['mant_descripcion']));

                            $mnts++;
                            break;
                        }
                    }

                    break;
                }
                case "INSP":{
                    $assi_id = 3;

                    switch($t['tare_tipo']){
                        case "VISITAR_SITIO":{
                            $res = Flight::ObtenerINSPAppPruebasBeta($db,$t['tare_id_relacionado']);
                            if( $res['status']==0 ){
                                return array("status"=>false, "error"=>$res2['error']);
                            }
                            if($res['rows']==0 ){
                                continue;
                            }
                            $insp = $res['data'][0];
                            $insp_id = $insp['insp_id'];
                            if($insp_id==null || $insp_id==""){
                                continue;
                            }

                            $forms = array();
                            array_push($forms,array("form_id"=>$DEF_CONFIG['insp']['formIngreso'],"form_nombre"=>"Ingreso a sitio"));

                            $res = $db->ExecuteQuery("SELECT
                                                        inspeccion_definicion_formulario.form_id,
                                                        form_nombre
                                                     FROM inspeccion
                                                     INNER JOIN inspeccion_definicion_formulario ON (inspeccion_definicion_formulario.cont_id = inspeccion.cont_id AND
                                                                                                     inspeccion_definicion_formulario.espe_id = inspeccion.espe_id AND
                                                                                                     inspeccion_definicion_formulario.clas_id = (SELECT clas_id FROM emplazamiento WHERE empl_id=inspeccion.empl_id))
                                                     INNER JOIN formulario ON (formulario.form_id=inspeccion_definicion_formulario.form_id)
                                                     WHERE
                                                        inspeccion.insp_id=$insp_id");
                            if( $res['status']==0 ){
                                return array("status"=>false, "error"=>$res['error']);
                            }
                            foreach($res['data'] as $row){
                                array_push($forms,$row);
                            }

                            array_push($forms,array("form_id"=>$DEF_CONFIG['insp']['formSalida'],"form_nombre"=>"Salida de sitio"));

                            foreach ($forms as $form)
                            {
                                $form_id     = $form["form_id"];
                                $form_nombre = $form["form_nombre"];

                                $db->startTransaction();
                                $res = $db->ExecuteQuery("SELECT
                                                            rtfr_id,
                                                            rtfr_estado
                                                          FROM
                                                            rel_tarea_formulario_respuesta
                                                          WHERE
                                                            tare_id=$tare_id AND form_id=$form_id");
                                if($res['status']==0){
                                    $db->Rollback();
                                    return array("status"=>false, "error"=>$res['error']);
                                }
                                if(0<$res['rows']){
                                    $estado = $res['data'][0]['rtfr_estado'];
                                    if($estado==""){
                                        $estado = "TODO";
                                    }

                                    array_push($assignments_status,array($res['data'][0]['rtfr_id'],$assi_id,$cont_id,$form_id,0,"","",$estado));
                                }
                                else{
                                    $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta SET
                                                              tare_id=$tare_id,form_id=$form_id,rtfr_accion='$form_nombre'");
                                    if($res['status']==0){
                                        $db->Rollback();
                                        return array("status"=>false, "error"=>$res['error']);
                                    }
                                    array_push($assignments_status,array($res['data'][0]['id'],$assi_id,$cont_id,$form_id,0,"","","TODO"));
                                }
                                $db->Commit();

                                if(!in_array(strval($form_id),$service_ids)){

                                    $res = $db->ExecuteQuery("SELECT
                                                            IFNULL(form_orden,1) as fogr_orden
                                                          FROM
                                                            formulario
                                                          WHERE form_id='$form_id'
                                                          AND form_estado='ACTIVO'
                                                          ORDER BY IFNULL(form_orden,1),form_id");

                                    
                                    array_push($services,array(strval($form_id),$form_nombre,"",strval($form_id),$res['data'][0]['fogr_orden']));
                                    array_push($service_ids,strval($form_id));
                                }

                                array_push($form_ids,$form_id);
                            }
                            //array_push($contracts,array($cont_id,$insp["espe_nombre"],"INSP Nº".$insp['insp_id'],$insp['empl_nombre'],$insp['empl_direccion'],$insp['empl_latitud'],$insp['empl_longitud'],$insp['empl_distancia'],$insp['empl_observacion'],"","",$cont_lock));
                            array_push($contracts,array($cont_id,$insp["espe_nombre"],"INSP Nº".$insp['insp_id'],$insp['empl_nombre'],$insp['empl_direccion'],$insp['empl_latitud'],$insp['empl_longitud'],$insp['empl_distancia'],$insp['empl_observacion'],"","",$cont_lock, $insp['insp_descripcion']));

                            $insps++;
                            break;
                        }
                    }
                    break;
                }
            }
        }


        if(0<$oss){
            array_push($assignments,array(1,"Orden de servicio (".$oss.")"));
        }

        if(0<$mnts){
            array_push($assignments,array(2,"Mantención (".$mnts.")"));
        }

        if(0<$insps){
            array_push($assignments,array(3,"Inspección (".$insps.")"));
        }


        $services = array_values(array_intersect_key($services, array_unique(array_map('serialize', $services))));

        if(0<count($form_ids)){
            $res = Flight::ObtenerFormulariosMFAppPruebasBeta($db,$form_ids);
            if(!$res['status']){
                Flight::json(array("status"=>false, "error"=>$res['error']));
                return;
            }
            $forms          = $res['forms'];
            $forms_pages    = $res['forms_pages'];
            $forms_controls = $res['forms_controls'];
        }
    }

    $out['status'] = true;
    $out['data']   = array("assignments"        =>$assignments,
                           "assignments_status" =>$assignments_status,
                           "contracts"          =>$contracts,
                           "services"           =>$services,
                           "users_launchers"    =>array(),
                           "users_messages"     =>array(),
                           "users_forms"        =>array(),
                           "forms"              =>$forms,
                           "forms_pages"        =>$forms_pages,
                           "forms_controls"     =>$forms_controls);
    return $out;
});


Flight::map('ActualizarTareaAppPruebasBeta', function($db,$rtfr_id,$fore_id,$rtfr_estado,$rtfr_fecha,$user){
    global $DEF_CONFIG;
    $timezone = $DEF_CONFIG["app_timezone"];

    $res = $db->ExecuteQuery("SELECT rfrr_id FROM rel_tarea_formulario_respuesta_revisiones
                               WHERE rtfr_id=$rtfr_id AND fore_id=$fore_id");
    if(!$res['status']){
        Flight::Log($res['error']);
        return array("status"=>false,"error"=>$res['error']);
    }
    else if(0<$res['rows']){ //revision ya ingresada
        return array("status"=>true);
    }


    $db->startTransaction();
    $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta_revisiones SET
                                rtfr_id=$rtfr_id,
                                fore_id=$fore_id,
                                rfrr_fecha=CONVERT_TZ('$rtfr_fecha','GMT','$timezone'),
                                rfrr_estado='$rtfr_estado'");
    if(!$res['status']){
        $db->Rollback();
        Flight::Log($res['error']);
        return array("status"=>false,"error"=>$res['error']);
    }

    $res = $db->ExecuteQuery("UPDATE rel_tarea_formulario_respuesta SET
                                fore_id=$fore_id,
                                rtfr_fecha=CONVERT_TZ('$rtfr_fecha','GMT','$timezone'),
                                rtfr_estado='$rtfr_estado'
                              WHERE rtfr_id=$rtfr_id");
    if(!$res['status']){
        $db->Rollback();
        Flight::Log($res['error']);
        return array("status"=>false,"error"=>$res['error']);
    }

    $res = $db->ExecuteQuery("SELECT
                                tarea.tare_id,
                                tare_modulo,
                                tare_tipo,
                                tare_estado,
                                tare_id_relacionado,
                                count(fore_id) AS subidos,
                                count(*) AS total
                              FROM
                                rel_tarea_formulario_respuesta
                              INNER JOIN tarea ON (tarea.tare_id=rel_tarea_formulario_respuesta.tare_id)
                              WHERE
                                rel_tarea_formulario_respuesta.tare_id = (SELECT tare_id FROM rel_tarea_formulario_respuesta WHERE rtfr_id=$rtfr_id)");
    if($res['status']){
        $tare_id      = $res['data'][0]['tare_id'];
        $tare_estado  = $res['data'][0]['tare_estado'];
        $subidos = $res['data'][0]['subidos'];
        $total   = $res['data'][0]['total'];

        if(0<$total){
            $nuevo_estado = "";
            if($subidos==$total){
                $nuevo_estado = "REALIZADA";
            }
            else if($subidos!=0 && $tare_estado!='EJECUTANDO'){
                $nuevo_estado = "EJECUTANDO";
            }

            if($nuevo_estado!=""){
                $res2 = $db->ExecuteQuery("UPDATE tarea SET tare_estado='$nuevo_estado' WHERE tare_id='$tare_id'");
                if(!$res2['status']){
                    $db->Rollback();
                    Flight::Log($res2['error']);
                    return array("status"=>false,"error"=>$res['error']);
                }

                $tare_modulo = $res['data'][0]['tare_modulo'];
                $tare_tipo   = $res['data'][0]['tare_tipo'];
                $even_modulo = "";
                $even_evento = "";
                $even_id_relacionado = "";
                $even_data="";

                switch($tare_modulo){
                    case 'OS':{
                        $even_modulo         = "OS";
                        $even_id_relacionado = $res['data'][0]['tare_id_relacionado'];
                        switch($tare_tipo){
                            case 'VISITAR_SITIO':{
                                if($nuevo_estado == "EJECUTANDO"){
                                    $even_evento = "VISITA_EJECUTANDO";
                                }
                                else if($nuevo_estado == "REALIZADA"){
                                    $res2 = $db->ExecuteQuery("INSERT INTO informe SET
                                                                info_modulo='OS',
                                                                info_id_relacionado='$even_id_relacionado',
                                                                usua_creador='$user',
                                                                info_fecha_creacion=NOW(),
                                                                info_estado='SINVALIDAR',
                                                                info_data='{\"tare_id\":$tare_id}'");
                                    if(!$res2['status']){
                                        $db->Rollback();
                                        Flight::Log($res2['error']);
                                        return array("status"=>false,"error"=>$res['error']);
                                    }
                                    $info_id = $res2['data'][0]['id'];

                                    $res2 = $db->ExecuteQuery("INSERT INTO rel_informe_formulario_respuesta (info_id,fore_id)
                                                                SELECT
                                                                    $info_id AS info_id,
                                                                    rel_tarea_formulario_respuesta_revisiones.fore_id
                                                                FROM rel_tarea_formulario_respuesta
                                                                INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id=rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                                                WHERE
                                                                tare_id=$tare_id
                                                                ORDER BY rel_tarea_formulario_respuesta.rtfr_id,rfrr_fecha ASC");
                                    if(!$res2['status']){
                                        $db->Rollback();
                                        Flight::Log($res2['error']);
                                        return array("status"=>false,"error"=>$res['error']);
                                    }


                                    $even_evento = "INFORME_AGREGADO";
                                    $even_data   = array("info_id"=>$info_id);
                                }
                                break;
                            }
                        }
                        break;
                    }
                    case 'MNT':{
                        $even_modulo         = "MNT";
                        $even_id_relacionado = $res['data'][0]['tare_id_relacionado'];
                        switch($tare_tipo){
                            case 'VISITAR_SITIO':{
                                if($nuevo_estado == "EJECUTANDO"){
                                    $even_evento = "VISITA_EJECUTANDO";
                                }
                                else if($nuevo_estado == "REALIZADA"){
                                    $res2 = $db->ExecuteQuery("INSERT INTO informe SET
                                                                info_modulo='MNT',
                                                                info_id_relacionado='$even_id_relacionado',
                                                                usua_creador='$user',
                                                                info_fecha_creacion=NOW(),
                                                                info_estado='SINVALIDAR',
                                                                info_data='{\"tare_id\":$tare_id}'");
                                    if(!$res2['status']){
                                        $db->Rollback();
                                        Flight::Log($res2['error']);
                                        return array("status"=>false,"error"=>$res['error']);
                                    }
                                    $info_id = $res2['data'][0]['id'];

                                    $res2 = $db->ExecuteQuery("INSERT INTO rel_informe_formulario_respuesta (info_id,fore_id)
                                                                SELECT
                                                                    $info_id AS info_id,
                                                                    rel_tarea_formulario_respuesta_revisiones.fore_id
                                                                FROM rel_tarea_formulario_respuesta
                                                                INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id=rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                                                WHERE
                                                                tare_id=$tare_id
                                                                ORDER BY rel_tarea_formulario_respuesta.rtfr_id,rfrr_fecha ASC");
                                    if(!$res2['status']){
                                        $db->Rollback();
                                        Flight::Log($res2['error']);
                                        return array("status"=>false,"error"=>$res['error']);
                                    }


                                    $even_evento = "INFORME_AGREGADO";
                                    $even_data   = array("info_id"=>$info_id);
                                }
                                break;
                            }
                        }
                        break;
                    }
                    case 'INSP':{
                        $even_modulo         = "INSP";
                        $even_id_relacionado = $res['data'][0]['tare_id_relacionado'];
                        switch($tare_tipo){
                            case 'VISITAR_SITIO':{
                                if($nuevo_estado == "EJECUTANDO"){
                                    $even_evento = "VISITA_EJECUTANDO";
                                }
                                else if($nuevo_estado == "REALIZADA"){
                                    $res2 = $db->ExecuteQuery("INSERT INTO informe SET
                                                                info_modulo='INSP',
                                                                info_id_relacionado='$even_id_relacionado',
                                                                usua_creador='$user',
                                                                info_fecha_creacion=NOW(),
                                                                info_estado='SINVALIDAR',
                                                                info_data='{\"tare_id\":$tare_id}'");
                                    if(!$res2['status']){
                                        $db->Rollback();
                                        Flight::Log($res2['error']);
                                        return array("status"=>false,"error"=>$res['error']);
                                    }
                                    $info_id = $res2['data'][0]['id'];

                                    $res2 = $db->ExecuteQuery("INSERT INTO rel_informe_formulario_respuesta (info_id,fore_id)
                                                                SELECT
                                                                    $info_id AS info_id,
                                                                    rel_tarea_formulario_respuesta_revisiones.fore_id
                                                                FROM rel_tarea_formulario_respuesta
                                                                INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id=rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                                                WHERE
                                                                tare_id=$tare_id
                                                                ORDER BY rel_tarea_formulario_respuesta.rtfr_id,rfrr_fecha ASC");
                                    if(!$res2['status']){
                                        $db->Rollback();
                                        Flight::Log($res2['error']);
                                        return array("status"=>false,"error"=>$res['error']);
                                    }

                                    $even_evento = "INFORME_AGREGADO";
                                    $even_data   = array("info_id"=>$info_id);
                                }
                                break;
                            }
                        }
                        break;
                    }
                }

                if($even_modulo!="" && $even_evento!=""){
                    $resEvent = Flight::AgregarEvento($db,$even_modulo,$even_evento,$even_id_relacionado,$even_data);
                    if( $resEvent['status']==0 ){
                        Flight::Log("[SIOM] AgregarEvento: ".$resEvent['error']);
                    }
                }
            }
        }
        $db->Commit();
        return array("status"=>true);
    }
    else{
        $db->Rollback();
        Flight::Log($res['error']);
        return array("status"=>false,"error"=>$res['error']);
    }
});


//______________________________________________________________________
//______________________________________________________________________
Flight::route('POST /app_pruebasBeta/login', function(){
    $db = new MySQL_Database();
    $out = array("status"=>false);

    $user    = mysql_real_escape_string($_POST['user']);
    $pass    = mysql_real_escape_string($_POST['pass']);
    $imei    = mysql_real_escape_string($_POST['imei']);
    $version = mysql_real_escape_string($_POST['version']);

  
    if(''== $user || '' == $pass ){
        $out['error']   = "Usuario inválido";
        Flight::json($out);
    }

    $res = $db->ExecuteQuery("SELECT
                                    usuario.usua_id,
                                    usua_nombre,
                                    usua_cargo,
                                    usua_opciones
                                 FROM usuario
                                 WHERE usua_login='$user' AND usua_password='$pass' AND usua_acceso_movil='1' AND usua_estado='ACTIVO'");
    if($res['status']){
        if(0<$res['rows']){
            $usuario  = $res['data'][0];
            $usua_id  = $usuario['usua_id'];

            $res = $db->ExecuteQuery("SELECT
                                        reum_id
                                    FROM rel_usuario_movil
                                    WHERE usua_id='$usua_id' AND reum_imei='$imei' AND reum_estado='ACTIVO'");
            if($res['status']){
                if(0<$res['rows']){
                    $out['status'] = true;
                    $out['user']['id']      = $usuario['usua_id'];
                    $out['user']['name']    = $usuario['usua_nombre'];
                    $out['user']['level']   = $usuario['usua_cargo'];
                    $out['user']['options'] = $usuario['usua_opciones'];
                }
                else{
                    $out['error']   = "Usuario no tiene móvil asignado";
                }
            }
            else{
                $out['error'] = $res['error'];
            }
        }
        else{
            $out['error']   = "Usuario inválido";
        }
    }
    else{
        $out['error'] = $res['error'];
    }

    Flight::json($out);
});


Flight::route('POST /app_pruebasBeta/register', function(){
    $db = new MySQL_Database();
    $out = array("status"=>false);

    $user = mysql_real_escape_string($_POST['user']);
    $code = mysql_real_escape_string($_POST['code']);
    $imei = mysql_real_escape_string($_POST['imei']);
    $version = mysql_real_escape_string($_POST['version']);

    $res = $db->ExecuteQuery("UPDATE rel_usuario_movil SET
                                reum_codigo_registro='$code',
                                reum_fecha_registro=NOW()
                              WHERE
                                  usua_id='$user' AND reum_imei='$imei'");
    if($res['status']){
        $out['status'] = true;
    }
    else{
        $out['error'] = $res['error'];
    }
    Flight::json($out);
});

Flight::route('POST /app_pruebasBeta/sync', function(){
    $db = new MySQL_Database();
    $out = array("status"=>false);

    $user = mysql_real_escape_string($_POST['user']);
    $imei = mysql_real_escape_string($_POST['imei']);
    $version = mysql_real_escape_string($_POST['version']);

  /*  $res = $db->ExecuteQuery("SELECT
                                tare_id,
                                tare_modulo,
                                tare_tipo,
                                tare_id_relacionado,
                                tare_estado
                              FROM tarea
                              INNER JOIN rel_usuario_movil ON (rel_usuario_movil.usua_id=tarea.usua_id AND rel_usuario_movil.reum_imei='$imei')
                              WHERE tarea.usua_id=$user AND tare_estado IN ('CREADA','DESPACHADA','EJECUTANDO')"); */
							  
		$res = $db->ExecuteQuery("SELECT  
                                     t.tare_id, 
                                     t.tare_modulo, 
                                     t.tare_tipo, 
                                     t.tare_id_relacionado, 
                                     t.tare_estado  

                                FROM( 
                                    (SELECT MAX(tare_id) AS max_tare_id 
                                                    , tare_id_relacionado AS max_tare_id_relacionado 
                                    FROM tarea 
                                    INNER JOIN rel_usuario_movil ON (rel_usuario_movil.usua_id=tarea.usua_id 
                                    #AND rel_usuario_movil.reum_imei=''
                                    ) 
                                WHERE  tarea.usua_id=$user 
								AND tarea.tare_tipo='VISITAR_SITIO'
                                AND tarea.tare_estado IN ('CREADA','DESPACHADA','EJECUTANDO') 
                                                    GROUP BY tare_id_relacionado) 
                                
                                )AS  maximo 
                    JOIN  tarea t ON t.tare_id = maximo.max_tare_id
					ORDER BY tare_fecha_despacho
                    LIMIT 20");
		
    if($res['status']){
        $out = Flight::ObtenerTareasMFAppPruebasBeta($db,$res['data']);

        if($out['status']){
            foreach($res['data'] as $row){
                if($row['tare_estado']=='CREADA'){
                    $tare_id = $row['tare_id'];
                    $res = $db->ExecuteQuery("UPDATE tarea SET
                                                tare_estado='DESPACHADA',tare_fecha_descarga=NOW()
                                              WHERE tare_id=$tare_id");
                    if(!$res['status']){
                        Flight::Log($res['error']);
                    }
                }
            }
        }
    }
    else{
        $out['error'] = $res['error'];
    }

    Flight::json($out);
});


Flight::route('POST /app_pruebasBeta/formulario/add', function(){
    include_once("../server/config.php");

    global $DEF_CONFIG;

    $db = new MySQL_Database();
    $out = array("status"=>false);

    $form_id        = mysql_real_escape_string($_POST['form_id']);
    $user_id        = mysql_real_escape_string($_POST['user_id']);
    $foan_datetime  = mysql_real_escape_string($_POST['foan_datetime_saved']);
    $foan_email     = mysql_real_escape_string($_POST['foan_emails']);
    $foan_location  = "";
    $timezone       = $DEF_CONFIG["app_timezone"];

    if(isset($_POST['foan_location'])){
        $foan_location = mysql_real_escape_string($_POST['foan_location']);
    }
    $imei = mysql_real_escape_string($_POST['imei']);
    $version = mysql_real_escape_string($_POST['version']);


    if(!Flight::ValidateAppPruebasBeta($db,$imei)){
        Flight::Log("Aceso denegado a /app_pruebasBeta/formulario/add (USERID: ".$user_id." IMEI: ".$imei.")");
        Flight::json(array("status"=>false,"error"=>"Aceso denegado"));
        return;
    }

    $db->startTransaction();

    $res = $db->ExecuteQuery("INSERT INTO formulario_respuesta SET
                                form_id=$form_id,
                                usua_creador=$user_id,
                                fore_fecha_creacion=CONVERT_TZ('$foan_datetime','GMT','$timezone'),
                                fore_fecha_recepcion=NOW(),
                                fore_ubicacion='$foan_location'");
    if($res['status']){
        $foan_id = $res['data'][0]['id'];

        //actualizar datos
        $data    = json_decode($_POST['form_data'],true);
        if($data){
            $values = array();
            foreach($data as $key => $value){
                $val = mysql_real_escape_string($value);
                array_push($values,"(0,$foan_id,$key,'$val')");
            }

            $query = "INSERT INTO formulario_valor VALUES ".implode(",",$values);
            $res = $db->ExecuteQuery($query);
            if($res['status']){
                $db->Commit();

                //actualizar estado
                if(isset($_POST['asst_id']) && $_POST['asst_id']!='' && isset($_POST['asfa_status']) && $_POST['asfa_status']!=''){
                    $rtfr_id   = mysql_real_escape_string($_POST['asst_id']);
                    $rtfr_estado   = mysql_real_escape_string($_POST['asfa_status']);

                    $res = Flight::ActualizarTareaAppPruebasBeta($db,$rtfr_id,$foan_id,$rtfr_estado,$foan_datetime,$user_id);
                    if($res['status']==false){
                        Flight::Log("Error al actualizar tarea $rtfr_id,$foan_id,$rtfr_estado,$foan_datetime,$user_id en formulario/add (version $version)");
                    }

                    Flight::json(array("status"=>true,"data"=>$foan_id));
                }
                else{
                    Flight::json(array("status"=>true,"data"=>$foan_id));
                }
            }
            else{
                $db->Rollback();
                Flight::Log($res['error']);
                Flight::json(array("status"=>false,"error"=>$res['error']));
            }
        }
        else{
            $db->Rollback();
            Flight::Log("Datos con valores de formularios no encontrados:\n "+$_POST['form_data']);
            Flight::json(array("status"=>true,"data"=>0));
            return;
        }
    }
    else{
        $db->Rollback();
        Flight::Log($res['error']);
        Flight::json(array("status"=>false,"error"=>$res['error']));
        return;
    }
});

Flight::route('POST /app_pruebasBeta/formulario/imagen/add', function(){
    /*
    $imei = strval(mysql_real_escape_string($_POST['imei']));
    if(!Flight::ValidateAppPruebasBeta($db,$imei)){
        Flight::Log("Aceso denegado a /app_pruebasBeta/formulario/imagen/add (IMEI: ".$imei.")");
        Flight::json(array("status"=>false,"error"=>"Aceso denegado"));
        return;
    }
    */

    if($_FILES['uploadedfile']['error']==0){
        $file_path = "../uploads/".basename($_FILES['uploadedfile']['name']);

        if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $file_path)) {
            Flight::json(array("status"=>true,"data"=>""));
        }
        else{
            Flight::Log("Error al mover archivo a directorio de uploads (".$_FILES['uploadedfile']['name'].")");
            Flight::json(array("status"=>false,
                               "error"=>"Error al mover archivo a directorio de uploads (".$_FILES['uploadedfile']['name'].")"));
        }
    }
    else{
        //logIt("Error al subir archivo (".$_FILES['uploadedfile']['error'].")");
        Flight::json(array("status"=>false,
                           "error"=>"Error al subir archivo (".$_FILES['uploadedfile']['error'].")"));
    }
});

Flight::route('POST /app_pruebasBeta/formulario/tarea/add', function(){
    include_once("../server/config.php");

    global $DEF_CONFIG;
    $timezone  = $DEF_CONFIG["app_timezone"];

    $db = new MySQL_Database();

    $user = strval(mysql_real_escape_string($_POST['user']));
    $imei = strval(mysql_real_escape_string($_POST['imei']));
    $version = mysql_real_escape_string($_POST['version']);

    if(!Flight::ValidateAppPruebasBeta($db,$imei)){
        Flight::Log("Aceso denegado a /app_pruebasBeta/formulario/tarea/add (IMEI: ".$imei.")");
        Flight::json(array("status"=>false,"error"=>"Aceso denegado"));
        return;
    }

    $rtfr_id   = mysql_real_escape_string($_POST['asst_id']);
    $fore_id   = mysql_real_escape_string($_POST['foan_server_id']);
    $rtfr_estado   = mysql_real_escape_string($_POST['asfa_status']);
    $rtfr_fecha    = mysql_real_escape_string($_POST['asfa_datetime']);

    $res = Flight::ActualizarTareaAppPruebasBeta($db,$rtfr_id,$fore_id,$rtfr_estado,$rtfr_fecha,$user);
    if($res['status']==false){
        Flight::Log("Error al actualizar tarea $rtfr_id,$fore_id,$rtfr_estado,$rtfr_fecha,$user en tarea/add (version $version)");
    }
    Flight::json($res);
    return;
});



Flight::route('POST /app_pruebasBeta/tracking/add', function(){
    include_once("../server/config.php");

    global $DEF_CONFIG;
    $timezone  = $DEF_CONFIG["app_timezone"];

    $db = new MySQL_Database();

    $imei = mysql_real_escape_string($_POST['imei']);
    $version = mysql_real_escape_string($_POST['version']);


    if(!Flight::ValidateAppPruebasBeta($db,$imei)){
        Flight::Log("Aceso denegado a /app_pruebasBeta/tracking/add (USERID: ".$user_id." IMEI: ".$imei.")");
        Flight::json(array("status"=>false,"error"=>"Aceso denegado"));
        return;
    }

    $user_id        = mysql_real_escape_string($_POST['user_id']);
    $trac_position  = json_decode($_POST['trac_position'],true);
    $trac_datetime  = mysql_real_escape_string($_POST['trac_datetime']);

    $res = $db->ExecuteQuery("INSERT INTO usuario_trackings
                                (usua_id,ustr_latitud,ustr_longitud,ustr_fecha)
                                VALUES
                                ($user_id,
                                '".$trac_position['lat']."',
                                '".$trac_position['lng']."',
                                CONVERT_TZ('".$trac_datetime."','GMT','$timezone'))");

    if(!$res['status']){
        Flight::Log($res['error']);
        Flight::json(array("status"=>false,"error"=>$res['error']));
        return;
    }

    Flight::json(array("status"=>true));
});


Flight::route('POST /app_pruebasBeta/error', function(){
    Flight::Log(print_r($_POST,true));
});


Flight::route('POST /app_pruebasBeta/logout', function(){
    $db = new MySQL_Database();
    $out = array("status"=>false);

    $user = mysql_real_escape_string($_POST['user']);
    $imei = mysql_real_escape_string($_POST['imei']);
    $version = mysql_real_escape_string($_POST['version']);

    $res = $db->ExecuteQuery("UPDATE rel_usuario_movil SET
                                reum_codigo_registro=''
                              WHERE
                                usua_id='$user' AND reum_imei='$imei'");
    if($res['status']){
        $out['status'] = true;
    }
    else{
        $out['error'] = $res['error'];
    }
    Flight::json($out);
});

?>

<?php

Flight::route('GET /contrato/@cont_id:[0-9]+/tareas/get/@usua_id:[0-9]+', function($cont_id,$usua_id){
    $out = array();
    $out['status'] = 1;

    $db = new MySQL_Database();
# INDICES POR TARE_MODULO, USARIO, ESTADO

    /*
;
    */
/*    $res = $db->ExecuteQuery("SELECT
                                tarea.*,
                                IFNULL(eo.empl_nombre,em.empl_nombre) AS empl_nombre,
                                IFNULL(eo.empl_direccion,em.empl_direccion) AS empl_direccion
                              FROM tarea 
                              LEFT JOIN orden_servicio ON (tare_modulo='OS' AND tarea.tare_id_relacionado=orden_servicio.orse_id AND orden_servicio.cont_id = $cont_id )
                              LEFT JOIN emplazamiento AS eo ON (eo.empl_id = orden_servicio.empl_id)
                              LEFT JOIN mantenimiento ON (tare_modulo='MNT' AND tarea.tare_id_relacionado=mantenimiento.mant_id AND mantenimiento.cont_id = $cont_id )
                              LEFT JOIN emplazamiento AS em ON (em.empl_id = mantenimiento.empl_id)
							                LEFT JOIN inspeccion ON (tare_modulo='INSP' AND tarea.tare_id_relacionado=inspeccion.insp_id AND inspeccion.cont_id = $cont_id )
                              LEFT JOIN emplazamiento AS ei ON (ei.empl_id = inspeccion.empl_id)
                              WHERE 
								usua_id=$usua_id 
								AND tare_estado NOT IN ('REALIZADA','CANCELADA','ANULADA')",false); //no debug
                */
$res = $db->ExecuteQuery("
      SELECT
          tarea.tare_id
          ,tarea.usua_id
          ,tarea.tare_modulo
          ,tarea.tare_tipo
          ,tarea.tare_id_relacionado
          ,tarea.tare_fecha_despacho
          ,tarea.tare_fecha_descarga
          ,tarea.tare_data
          ,tarea.tare_estado
          ,IFNULL(eo.empl_nombre,'') AS empl_nombre
          ,IFNULL(eo.empl_direccion,'') AS empl_direccion
        FROM tarea 
        JOIN orden_servicio  ON (tare_modulo='OS'
                    AND usua_id=$usua_id
                    AND tare_estado NOT IN ('REALIZADA','CANCELADA','ANULADA')
                    AND tarea.tare_id_relacionado=orden_servicio.orse_id 
                    AND orden_servicio.cont_id = $cont_id) 
        JOIN emplazamiento AS eo ON (eo.empl_id = orden_servicio.empl_id)
        UNION
        SELECT
           tarea.tare_id
          ,tarea.usua_id
          ,tarea.tare_modulo
          ,tarea.tare_tipo
          ,tarea.tare_id_relacionado
          ,tarea.tare_fecha_despacho
          ,tarea.tare_fecha_descarga
          ,tarea.tare_data
          ,tarea.tare_estado
          ,IFNULL(em.empl_nombre,'') AS empl_nombre
          ,IFNULL(em.empl_direccion,'') AS empl_direccion
        FROM tarea              
        JOIN mantenimiento ON (tare_modulo='MNT'
                    AND usua_id=$usua_id
                    AND tare_estado NOT IN ('REALIZADA','CANCELADA','ANULADA')
                    AND tarea.tare_id_relacionado=mantenimiento.mant_id 
                    AND mantenimiento.cont_id = $cont_id)
        JOIN emplazamiento AS em ON (em.empl_id = mantenimiento.empl_id)
        UNION
        SELECT
           tarea.tare_id
          ,tarea.usua_id
          ,tarea.tare_modulo
          ,tarea.tare_tipo
          ,tarea.tare_id_relacionado
          ,tarea.tare_fecha_despacho
          ,tarea.tare_fecha_descarga
          ,tarea.tare_data
          ,tarea.tare_estado
          ,IFNULL(ei.empl_nombre,'') AS empl_nombre
          ,IFNULL(ei.empl_direccion,'') AS empl_direccion
        FROM tarea  
        JOIN inspeccion ON (tare_modulo='INSP' 
                    AND usua_id=$usua_id
                    AND tare_estado NOT IN ('REALIZADA','CANCELADA','ANULADA')
                    AND tarea.tare_id_relacionado=inspeccion.insp_id 
                    AND inspeccion.cont_id = $cont_id)
        JOIN emplazamiento AS ei ON (ei.empl_id = inspeccion.empl_id)
  ");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['tareas'] = $res['data'];

  /*  $res = $db->ExecuteQuery("SELECT 
                                notificacion.*,
                                IFNULL(eo.empl_nombre,em.empl_nombre) AS empl_nombre,
                                IFNULL(eo.empl_direccion,em.empl_direccion) AS empl_direccion 
                              FROM notificacion 
                              LEFT JOIN orden_servicio ON (noti_modulo='OS' AND notificacion.noti_id_relacionado=orden_servicio.orse_id AND orden_servicio.cont_id = $cont_id )
                              LEFT JOIN emplazamiento AS eo ON (eo.empl_id = orden_servicio.empl_id)
                              LEFT JOIN mantenimiento ON (noti_modulo='MNT' AND notificacion.noti_id_relacionado=mantenimiento.mant_id AND mantenimiento.cont_id = $cont_id )
                              LEFT JOIN emplazamiento AS em ON (em.empl_id = mantenimiento.empl_id)
							                LEFT JOIN inspeccion ON (noti_modulo='INSP' AND notificacion.noti_id_relacionado=inspeccion.insp_id AND inspeccion.cont_id = $cont_id )
                              LEFT JOIN emplazamiento AS ei ON (ei.empl_id = inspeccion.empl_id)
                              WHERE 
								usua_id=$usua_id 
								AND noti_estado = 'DESPACHADA'",false); //no debug
                */

    $res = $db->ExecuteQuery("
      SELECT 
        notificacion.noti_id
        ,notificacion.usua_id
        ,notificacion.noti_modulo
        ,notificacion.noti_tipo
        ,notificacion.noti_id_relacionado
        ,notificacion.noti_fecha_despacho
        ,notificacion.noti_fecha_descarga
        ,notificacion.noti_data
        ,notificacion.noti_estado
        ,IFNULL(eo.empl_nombre,'') AS empl_nombre
        ,IFNULL(eo.empl_direccion,'') AS empl_direccion 
  FROM notificacion 
  JOIN orden_servicio ON (noti_modulo='OS'
        AND usua_id=$usua_id 
        AND noti_estado = 'DESPACHADA'  
        AND notificacion.noti_id_relacionado=orden_servicio.orse_id 
        AND orden_servicio.cont_id = $cont_id )
  JOIN emplazamiento AS eo ON (eo.empl_id = orden_servicio.empl_id)           
  UNION
  SELECT 
        notificacion.noti_id
        ,notificacion.usua_id
        ,notificacion.noti_modulo
        ,notificacion.noti_tipo
        ,notificacion.noti_id_relacionado
        ,notificacion.noti_fecha_despacho
        ,notificacion.noti_fecha_descarga
        ,notificacion.noti_data
        ,notificacion.noti_estado
        ,IFNULL(em.empl_nombre,'') AS empl_nombre
        ,IFNULL(em.empl_direccion,'') AS empl_direccion 
  FROM notificacion             
  JOIN mantenimiento ON (noti_modulo='MNT' 
        AND usua_id=$usua_id 
        AND noti_estado = 'DESPACHADA'
        AND notificacion.noti_id_relacionado=mantenimiento.mant_id 
        AND mantenimiento.cont_id = $cont_id )
  JOIN emplazamiento AS em ON (em.empl_id = mantenimiento.empl_id)
  UNION
  SELECT 
        notificacion.noti_id
        ,notificacion.usua_id
        ,notificacion.noti_modulo
        ,notificacion.noti_tipo
        ,notificacion.noti_id_relacionado
        ,notificacion.noti_fecha_despacho
        ,notificacion.noti_fecha_descarga
        ,notificacion.noti_data
        ,notificacion.noti_estado
        ,IFNULL(ei.empl_nombre,'') AS empl_nombre
        ,IFNULL(ei.empl_direccion,'') AS empl_direccion 
  FROM notificacion             
  JOIN inspeccion ON (noti_modulo='INSP' 
        AND usua_id=$usua_id 
        AND noti_estado = 'DESPACHADA'
        AND notificacion.noti_id_relacionado=inspeccion.insp_id 
        AND inspeccion.cont_id = $cont_id )
  JOIN emplazamiento AS ei ON (ei.empl_id = inspeccion.empl_id)
      ");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['notificaciones'] = $res['data'];

    Flight::json($out);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/notificacion/del/@noti_id:[0-9]+', function($cont_id,$noti_id){
     $db  = new MySQL_Database();
     $res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' WHERE noti_id=$noti_id");
     Flight::json($res);
});



?>
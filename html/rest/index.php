<?php 
ini_set('memory_limit', '-1');
set_time_limit(0);
error_reporting(E_ALL ^ E_WARNING ^ E_DEPRECATED);

require('../../libs/flight/Flight.php');
include("../server/config.php");
require("db.php");
include("../api/upload.php");

/*#################################################################################
Variables globales                                                                        
#################################################################################*/
Flight::set('core_tables', array( 'usuario'       => array( 'prefijo'=>'usua', 'historial'=>TRUE, 'filtros' => array( 'usua_nombre' => "REGEXP '%s'" ) )
                                 ,'contrato'      => array( 'prefijo'=>'cont', 'historial'=>TRUE, 'filtros' => array( 'cont_nombre' => "REGEXP '%s'" ) )
                                 ,'empresa'       => array( 'prefijo'=>'empr', 'historial'=>TRUE, 'filtros' => array( ) )
                                 ,'emplazamiento' => array( 'prefijo'=>'empl', 'historial'=>TRUE, 'filtros' => array( 'empl_nemonico' => "REGEXP '%s'", 'empl_nombre' => "REGEXP '%s'", 'empl_direccion' => "REGEXP '%s'") )
                                 ,'curso'         => array( 'prefijo'=>'curs', 'historial'=>TRUE, 'filtros' => array( ) )
                                 ,'formulario'    => array( 'prefijo'=>'form', 'historial'=>TRUE, 'filtros' => array( 'form_nombre' => "REGEXP '%s'" ) )
                                 ,'repositorio'   => array( 'prefijo'=>'repo', 'historial'=>TRUE, 'filtros' => array( 'repo_nombre' => "REGEXP '%s'" ) )
                                 ,'inspeccion'    => array( 'prefijo'=>'insp', 'historial'=>TRUE, 'filtros' => array( ) )
                                 ,'orden_servicio'=> array( 'prefijo'=>'orse', 'historial'=>TRUE, 'filtros' => array( ) )
                                 
                                 ,'sla_orden_servicio' => array( 'prefijo'=>'orse', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'sla_mantenimiento'  => array( 'prefijo'=>'mant', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'sla_inspeccion'     => array( 'prefijo'=>'insp', 'historial'=>FALSE, 'filtros' => array( ) )
    
                                 ,'perfil'        => array( 'prefijo'=>'perf', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'zona'          => array( 'prefijo'=>'zona', 'historial'=>FALSE, 'filtros' => array( 'zona_nombre' => "REGEXP '%s'" , 'zona_alias' => "REGEXP '%s'" , 'zona_descripcion' => "REGEXP '%s'" ) )
                                 ,'especialidad'  => array( 'prefijo'=>'espe', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'formulario'    => array( 'prefijo'=>'form', 'historial'=>TRUE,  'filtros' => array( ) )
                                 ,'tecnologia'    => array( 'prefijo'=>'tecn', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'periodicidad'  => array( 'prefijo'=>'peri', 'historial'=>FALSE, 'filtros' => array('peri_nombre'=>"REGEXP '%s'" ) )
                                 ,'modulo'        => array( 'prefijo'=>'modu', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'emplazamiento_dueno_torre' 
                                                  => array( 'prefijo'=>'duto', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'rel_contrato_usuario_notificacion'  => array( 'prefijo'=>'rcun', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'rel_usuario_movil'  => array( 'prefijo'=>'reum', 'historial'=>FALSE, 'filtros' => array( ) )
                                 ,'rel_usuario_curso'  => array( 'prefijo'=>'uscu', 'historial'=>TRUE, 'filtros' => array( ) )
                                 ,'rel_formulario_especialidad'  => array( 'prefijo'=>'rfe', 'historial'=>TRUE, 'filtros' => array( ) )
                                 ,'emplazamiento_clasificacion'  => array( 'prefijo'=>'clas', 'historial'=>TRUE, 'filtros' => array( ) )
                                 ,'lpu_grupo'          => array( 'prefijo'=>'lpgr', 'historial'=>FALSE, 'filtros' => array( 'lpgr_nombre' => "REGEXP '%s'" ) )
                                 ,'lpu_item'           => array( 'prefijo'=>'lpit', 'historial'=>FALSE, 'filtros' => array( 'lpit_nombre' => "REGEXP '%s'" ) )
                                 ,'lpu_item_precio'    => array( 'prefijo'=>'lpip', 'historial'=>FALSE, 'filtros' => array( 'lpip_nombre' => "REGEXP '%s'" ) )
                                
                                 ,'inventario_elemento'    => array( 'prefijo'=>'inel', 'historial'=>TRUE, 'filtros' => array( 'inel_nombre' => "REGEXP '%s'",'inel_descripcion' => "REGEXP '%s'",'inel_ubicacion' => "REGEXP '%s'" ) )

 ));

Flight::set('results_by_page', 20);
Flight::set('query', null);

/*#################################################################################
Funciones globales                                                                        
#################################################################################*/
Flight::map('Log', function($message,$priority=LOG_ERR){
    global $ENV;
    syslog($priority,"[SIOM-".strtoupper($ENV)."] ".$message);
});

/* No se utiliza
Flight::map('filtersProcess',function( $tabla,$filtros ){
    if( is_null($filtros) || count($filtros) == 0 ){
        return array();        
    }
    
    $tablas = Flight::get('core_tables');
    $filtros_str = array();
       
    foreach ($filtros as $key => $value) {
        if( array_key_exists ( $key , $tablas[$tabla]['filtros'] ) ){
            $filtros_str[ $key ] = sprintf( $tablas[$tabla]['filtros'][$key], $value );
        }
        else{
            $filtros_str[ $key ] = (is_null($value)) ? "IS NULL" : ((is_numeric($value)) ? "= ".$value : "= '" . $value . "'" );
        }
    }
    return $filtros_str;    
});
*/

Flight::map('filtersToWhereString', function( $tablas, $filtros ){
    
    if( is_null($filtros) || count($filtros) == 0 ){
        return "TRUE";        
    }
    
    foreach ($filtros as $key => $value) {
        if( is_array($value) ){
            unset($filtros[$key]);
        }
        if( is_null($value) || strlen(trim($value.""))==0 ){
            unset($filtros[$key]);
        }
    } 
    
    if( is_null($filtros) || count($filtros) == 0 ){
        return "TRUE";        
    }
    
    $tablas_core = Flight::get('core_tables');
    $filtros_arr = array();
    
    foreach ($tablas as $tabla) {
        foreach ($filtros as $key => $value) {
            if (array_key_exists($key, $tablas_core[$tabla]['filtros'])) {
                $filtros_arr[] = $key . " " . sprintf($tablas_core[$tabla]['filtros'][$key], $value);
                unset($filtros[$key]);
            } 
        }
    }    
    foreach ($filtros as $key => $value) {
        $filtros_arr[] = (is_null($value)) ? $key." IS NULL":((is_numeric($value))?$key." = ".$value:$key." = '".$value."'" );
    } 
    return implode(' AND ', $filtros_arr);   
});

Flight::map('dataToInsertString', function( $data ){    
    if ( !isset($data) || is_null($data)) {
        return "";
    }
    
    /*Borramos las keys vacias*/
    foreach (array_keys($data, "", true) as $key) {
        unset($data [$key]);
    }

    /*Construimos el string para el insert*/
    $data_str_keys = implode(",", array_keys($data));
    $data_str_vals = "";
    foreach ($data as $key => $val) {
        $data_str_vals = $data_str_vals . ( (is_null($val)) ? "NULL" : ((is_numeric($val) || $val=="NOW()") ? $val : "'" . $val . "'" )) . ",";
    }
    $data_str_vals = substr($data_str_vals, 0, -1);
    return "(" . $data_str_keys . ") VALUES (" . $data_str_vals . ")";
});

Flight::map('dataToUpdateString', function( $data ){    
    if ( !isset($data) || is_null($data)) {
        return "";
    }
    
    /*Borramos las keys vacias*/
    foreach (array_keys($data, "", true) as $key) {
        unset($data [$key]);
    }

    /*Construimos el string para el update*/
    $data_str = "";
    foreach ($data as $key => $val) {
        $data_str = $data_str . $key . "=" . ( (is_numeric($val)) ? $val : "'" . $val . "'" ) . ",";
    }
    $data_str = substr($data_str, 0, -1);

    return $data_str;
});

Flight::map('AgregarEvento', function($db,$modulo,$evento,$id,$datos=''){
    $datos = json_encode($datos, JSON_UNESCAPED_UNICODE);

    $usua_id = 1;
    if(isset($_SESSION['user_id'])){
        $usua_id = $_SESSION['user_id'];
    }   
    $res = $db->ExecuteQuery("INSERT INTO evento SET 
                              even_modulo='$modulo',
                              even_evento='$evento',
                              even_id_relacionado='$id',
                              even_fecha=NOW(),
                              even_estado='DESPACHADO',
                              even_datos='$datos',
                              usua_creador='$usua_id'");
    return $res;
});

Flight::map('FinalizarTareaRelacionada', function($db,$modulo,$tipo,$id_rel,$datos=''){
    $query = "UPDATE tarea SET tare_estado = 'REALIZADA' 
                             WHERE tare_modulo = '$modulo' AND tare_tipo = '$tipo' AND tare_id_relacionado = $id_rel";
    if($datos!=''){
       $query .= " AND tare_data LIKE '%$datos%'";
    }
    return $db->ExecuteQuery($query);
});

/*#################################################################################
    Validación de acceso a REST                                                    #
    Se llama antes de cada request al REST                                         #
#################################################################################*/
Flight::before('start', function(&$params, &$output){ 
    $request = Flight::request();
    
    session_start();
	/*Valida la ultima interacción del usuario */
	if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] >= 1800)) {
		/* Tiempo  de inactividad 1 hora;*/
        $db     = new MySQL_Database();
        $db->startTransaction();
        $tiempo     = $_SESSION['LAST_ACTIVITY'];
        $usua_id    = $_SESSION['user_id'];
        $estado     = "DESCONECTADO"; 
        $vall       = $db->ExecuteQuery(" UPDATE usuario 
                                            SET usua_estado_coneccion= '".$estado."'
                                            WHERE usua_id = $usua_id;                        
        "); 
        if(0 == $vall['status']){
            $db->rollback();
            $db->Disconnect(); 
            Flight::Log($vall['error']);
            Flight::json(array("status"=>false,"error"=>$vall['error']));
            return;
        }  
        $db->commit();
        $db->Disconnect();
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 60,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_unset();
        session_destroy();

		Flight::json(array("status"=>false,"error"=>"Su sesión ha expirado","isSessionExpired"=>true));
		Flight::redirect('#/logout');
	
    if(!isset($_SESSION["LAST_ACTIVITY"])){
       $_SESSION["LAST_ACTIVITY"] =100;
    }
	} else if (time() - $_SESSION["LAST_ACTIVITY"] > 400) {
        $_SESSION["LAST_ACTIVITY"] = time();  /*Actualiza el tiempo de ultima actividad*/
    }
	
    if($request->url == "/login" || (isset($_SESSION) && isset($_SESSION['user_id'])) ){
        return true;
    }
    else if(isset($_SERVER['HTTP_AUTHORIZATION'])) {
      if (strpos(strtolower($_SERVER['HTTP_AUTHORIZATION']),'basic')===0){
        list($username,$password) = explode(':',base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
         
        $db = new MySQL_Database();
        $user = mysql_real_escape_string($username);
        $pass = mysql_real_escape_string($password);
        $res = $db->ExecuteQuery("SELECT 
                                    usua_id
                                 FROM usuario
                                 WHERE usua_login='$user' AND usua_password='$pass' AND usua_acceso_api='1' AND usua_estado='ACTIVO'");
        if($res['status']){
            if(0<$res['rows']){
                return true;
            }
        }else{
            Log(LOG_ERR,$res['error']);
            header("HTTP/1.1 500 Internal Server Error");
            die($res['error']);
            return false;
        }
      }
    }
    else if ($_SERVER['HTTP_USER_AGENT']=="SIOM User Agent V/1.0"){
        //TODO: Validar por imei
        return true;
    }
    
    /*header("HTTP/1.1 401 Unauthorized");
    /die("Acceso denegado");*/

    Flight::json(array("status"=>false,"error"=>"Su sesión ha expirado","isSessionExpired"=>true));
	Flight::redirect('/logout');
    return false;
});
/*##################################################################################
Rutas 
TODO: Cargar solo el/los archivos necesarios en funcion de la ruta
##################################################################################*/
include("utiles.php");

include("contrato.php");

include("login.php");

include("zona.php");

include("lpu.php");

include("usuario.php");

include("empresa.php");

include("emplazamiento.php");

include("dashboard.php");

include("os.php");

include("mnt.php");

include("insp.php");

include ("inve.php");

include("app.php");

include("app_pruebas.php");

include("tarea.php");

include("repo.php"); /*requiere os.php, mnt.php, emplazamiento.php insp.php*/

include("curso.php");

include("formulario.php");

include("sla.php");

include("periodicidad.php");

include("tracking.php");

include("generales.php");

include("defaMovil.php");

include("app_pruebasBeta.php");


/*======================NO ENCONTRADOS=====================*/
Flight::map('notFound', function(){
    Flight::json(array("status"=>0,"error"=>"Ruta no encontrada [".Flight::request()->url."]"));
});
Flight::route('GET /*', function(){
	Flight::notFound();
});
Flight::route('POST /*', function(){
	Flight::notFound();
});
Flight::set('flight.log_errors', true);
Flight::start();
?>

<?php

	Flight::route('GET /core/zona/filtros', function(){    
	    $out = array();
	    $dbo = new MySQL_Database();
	    
	    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM zona WHERE Field = 'zona_tipo'" );
	    if (0 == $res['status']) {
			Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
		}
	    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
	    $tipos = explode("','", $matches[1]);   
	    sort($tipos);
	    $out['tipos'] = $tipos;
	    
	    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM zona WHERE Field = 'zona_estado'" );
	    if (0 == $res['status']) {
			Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
		}
	    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
	    $out['estados'] = explode("','", $matches[1]);   
	   
	    $out['status'] = 1;
	    Flight::json($out);	
	});
		
	/*ZONA*/
	Flight::route('GET /core/contrato/@cont_id:[0-9]+/zona/list(/@page:[0-9]+)', function($cont_id, $page){          
	    $filtros_ini = array_merge($_GET,$_POST);
	    $filtros = Flight::filtersToWhereString( array("zona"), $filtros_ini);    
	    $results_by_page = Flight::get('results_by_page');
	    $query = "SELECT    zona_id
                            ,cont_id
                            ,zona_nombre
                            ,zona_alias
                            ,zona_descripcion
                            ,zona_estado
                            ,zona_tipo
                            ,zona_padre 
                    FROM zona 
                    WHERE cont_id=$cont_id
					AND $filtros 
					ORDER BY zona_tipo ASC ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET /core/contrato/@cont_id:[0-9]+/zona/get/@idz:[0-9]+', function($cont_id, $idz){
	    $query = "SELECT * FROM zona WHERE cont_id=$cont_id AND zona_id=".$idz;
	    $dbo = new MySQL_Database();
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/zona/add', function($cont_id){
	    $data = array_merge($_GET,$_POST);
	    //$data['cont_id'] = $id;

	    $zona_nombre=$data['zona_nombre'];
	    $zona_alias=$data['zona_alias'];
	    $zona_descripcion=$data['zona_descripcion'];
	    $zona_estado='ACTIVO';
	    $zona_tipo=$data['zona_tipo'];
	    $query = "INSERT INTO zona (cont_id
	                                ,zona_nombre
	                                ,zona_alias
	                                ,zona_descripcion
	                                ,zona_estado
	                                ,zona_tipo) 
	                VALUES($cont_id, '$zona_nombre', '$zona_alias', '$zona_descripcion', '$zona_estado', '$zona_tipo')";
	    $dbo = new MySQL_Database();    
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/zona/upd/@idz:[0-9]+', function($cont_id, $idz){
	    $data = array_merge($_GET,$_POST);
	    $query = "UPDATE zona SET ".Flight::dataToUpdateString($data)." WHERE cont_id=$cont_id AND zona_id =".$idz;
	    $dbo = new MySQL_Database();    
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET /core/contrato/@cont_id:[0-9]+/zona/del/@idz:[0-9]+', function($cont_id, $idz){
	    $query = "UPDATE zona SET zona_estado='NOACTIVO' WHERE cont_id=$cont_id AND zona_id =".$idz;
	    $dbo = new MySQL_Database();    
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET /core/contrato/@cont_id:[0-9]+/zona/@idz:[0-9]+/emplazamiento/list', function($cont_id, $idz){
	    $query = "SELECT e.* 
	              FROM zona z, emplazamiento e, rel_zona_emplazamiento rze
	              WHERE 
	                z.cont_id=$cont_id
	                AND z.zona_id=$idz
	                AND rze.zona_id=z.zona_id 
	                AND rze.empl_id=e.empl_id";
	    $dbo = new MySQL_Database();    
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/zona/@idz:[0-9]+/emplazamiento/add/@ide:[0-9]+', function($cont_id, $idz, $ide){
	    $query = "SELECT zona_id FROM zona WHERE cont_id=$cont_id AND zona_id=".$idz;
	    $dbo = new MySQL_Database();    
	    $res = $dbo->ExecuteQuery($query);
	    if( 0 == $res['status'] || 0 == $res['rows'] ){
	        Flight::json(array("status"=>0, "error"=>"La zona no pertenece al contrato"));
	    }    
	    $query = "INSERT INTO rel_zona_emplazamiento (zona_id,empl_id) VALUES (".$idz.",".$ide.")";
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});
	
	Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/zona/@idz:[0-9]+/emplazamiento/del/@ide:[0-9]+', function($cont_id, $idz, $ide){
	    $query = "SELECT zona_id FROM zona WHERE cont_id=$cont_id AND zona_id=".$idz;
	    $dbo = new MySQL_Database();    
	    $res = $dbo->ExecuteQuery($query);
	    if( 0 == $res['status'] || 0 == $res['rows'] ){
	        Flight::json(array("status"=>0, "error"=>"La zona no pertenece al contrato"));
	    }     
	    $query = "DELETE FROM rel_zona_emplazamiento WHERE zona_id=".$idz." AND empl_id =".$ide; 
	    $res = $dbo->ExecuteQuery($query);
	    Flight::json($res);
	});

?>
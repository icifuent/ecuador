<?php

Flight::set('flight.log_errors', true);

/*Agrega archivos desde core > repo */
Flight::route('GET|POST /core/repo/add', function() {
    if ( empty($_FILES) || 0 == count($_FILES) ){
      Flight::json(array("status" => 0, "No se proporciono un archivo para agregar al repositorio"));
      return;
    }
    
    $data = array_merge($_GET,$_POST);
    $data['usua_creador'] = $_SESSION['user_id'];
    $data['repo_fecha_creacion'] = "NOW()";
    $nombre   = $_FILES['archivo']['name'];
    $filename = date('ymdHis')."_".$data['repo_tabla']."_".$data['repo_tabla_id']."_".str_replace(" ","_",$nombre);
    $resFile = Upload::UploadFile($_FILES['archivo'],$filename); 
    if( !$resFile['status'] ){
        Flight::json(array("status" => 0, "error" => $resFile['error']));
        return;
    }

    $data['repo_ruta'] = $resFile['data']['filename'];
    
    $query = "INSERT INTO repositorio ".Flight::dataToInsertString($data);
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

/*Descarga los archivos*/
Flight::route('GET|POST /core/repo/@repo_id:[0-9]+', function($repo_id)
{
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery("SELECT
                                repo_nombre,
                                repo_ruta
                               FROM
                                repositorio
                               WHERE 
                                repo_id = '$repo_id'
                               ORDER BY repo_fecha_creacion DESC
                               LIMIT 1
    ");
    if($res['status']){
        if(0<$res['rows']){
            $ruta     =  $res['data'][0]['repo_ruta'];
            $ext      =  pathinfo($ruta, PATHINFO_EXTENSION);
            $filename =  $res['data'][0]['repo_nombre'].".".$ext;

            if (file_exists("../".$ruta)) {
                switch($ext){
                    case "xls":{
                        $contentType = "application/vnd.ms-excel";
                    break;
                    }
                    default:{
                        $contentType = "application/octet-stream";
                    }
                }

                header('Content-Type: '.$contentType);
                header('Content-Disposition: attachment; filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                header('Cache-Control: max-age=1');
                header('Set-Cookie: fileDownload=true; path=/');
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
                header ('Pragma: public');                // HTTP/1.0
                readfile("../".$ruta);
                exit;
            }else{
                echo "Archivo no existe ($filename)";
            }
        }else{
        echo "No hay archivos asociados a $tabla (ID: $tabla_id)";
        }
    }else{
        echo $res['error'];
    }
});

/*Elimina un archivo*/
Flight::route('GET|POST /core/repo/del/@repo_id:[0-9]+', function($repo_id)
{
    $dbo = new MySQL_Database();  
    $res = $dbo->ExecuteQuery(" SELECT
                                    repo_ruta
                                FROM
                                    repositorio
                                WHERE 
                                    repo_id = '$repo_id';"
    );
    if($res['status']){
        if(0<$res['rows']){
            $ruta = $res['data'][0]['repo_ruta'];
            if (file_exists("../".$ruta)) {
            unlink("../".$ruta);  
            }
        }
    }

    $res = $dbo->ExecuteQuery("DELETE FROM repositorio WHERE repo_id = '$repo_id'");
    Flight::json($res);
});

Flight::route('GET|POST /core/repo/upd/@repo_id:[0-9]+', function($repo_id)
{
    $data = array_merge($_GET,$_POST);
    $query = "UPDATE repositorio SET ".Flight::dataToUpdateString($data)." WHERE repo_id=".$repo_id;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/repo/@tabla/@tabla_id:[0-9]+', function($tabla,$tabla_id)
{
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery("SELECT
                                repo_nombre,
                                repo_ruta
                               FROM
                                repositorio
                               WHERE 
                                repo_tabla = '$tabla' AND repo_tabla_id = '$tabla_id'
                               ORDER BY repo_fecha_creacion DESC
                               LIMIT 1
                              ");
    if($res['status']){
            if(0<$res['rows']){
                    $ruta     =  $res['data'][0]['repo_ruta'];
                    $ext      =  pathinfo($ruta, PATHINFO_EXTENSION);
                    $filename =  $res['data'][0]['repo_nombre'].".".$ext;

                    if (file_exists("../".$ruta)) {
                        switch($ext){
                            case "xls":{
                                $contentType = "application/vnd.ms-excel";
                                break;
                            }
                            default:{
                               $contentType = "application/octet-stream";
                            }
                        }
                        header('Content-Type: '.$contentType);
                        header('Content-Disposition: attachment; filename="'.$filename.'"');
                        header('Cache-Control: max-age=0');
                        header('Cache-Control: max-age=1');
                        header('Set-Cookie: fileDownload=true; path=/');
                        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      
                        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); 
                        header ('Cache-Control: cache, must-revalidate');       
                        header ('Pragma: public');                
                        readfile("../".$ruta);
                        exit;
                    }else{
                        echo "Archivo no existe ($filename)";
                    }
            }else{
              echo "No hay archivos asociados a $tabla (ID: $tabla_id)";
            }
    }else{
        echo $res['error'];
    }
});

/*Descarga la data de la bandeja de Orden de servicio  Felipe Azabache */
Flight::route('GET|POST /core/repo/contrato/@cont_id:[0-9]+/os/bandeja', function($cont_id){

    $db = new MySQL_Database();  
    $usua_id     = $_SESSION['user_id'];
    $empr_id     = $_SESSION['empr_id'];
    $filtros     = "";
    /*Obtener OS filtradas*/
    $res = Flight::ObtenerListaOS($db,$cont_id,array_merge($_GET,$_POST));
    if ( 0 == $res['status'] ){
        echo $res['error'];
        return;
    }
    $usuario_tipo = $db->ExecuteQuery("
                                    SELECT rcem.coem_tipo
                                    FROM rel_contrato_empresa rcem 
                                        ,usuario u
                                    WHERE 
                                        rcem.coem_tipo='CLIENTE'
                                    AND rcem.cont_id =$cont_id
                                    AND rcem.coem_estado = 'ACTIVO' 
                                    AND rcem.empr_id =u.empr_id
                                    AND u.usua_id = $usua_id ;
    ");
    if (0 == $usuario_tipo['status'] ){ 
        Flight::json(array("status" => 0, "error" => $usuario_tipo['error']));
    }
    if(0 == $usuario_tipo['rows']){
        $filtros .= " AND orden_servicio.empr_id =$empr_id ";
    }
    $OSs = implode("','",array_map(function($item){ return $item['orse_id'];},$res['ordenes']));

    $res = $db->ExecuteQuery("SELECT
            orden_servicio.orse_id AS 'Nº OS',
            orse_tipo AS 'Tipo OS',
            espe_nombre AS 'Especialidad OS',
            sube_nombre AS 'Alarma OS',
            empr_nombre AS 'EECC OS',
            usuario_creador.usua_nombre AS 'Usuario creador OS',
            orse_fecha_creacion AS 'Fecha creación OS',
            orse_fecha_solicitud AS 'Fecha programada OS',
            emplazamiento_visita.emvi_fecha_ingreso AS 'Fecha ingreso sitio',
            emplazamiento_visita.emvi_fecha_salida AS 'Fecha salida sitio',
            orse_fecha_validacion AS 'Fecha finalización OS',
            jefe.usua_nombre AS 'Jefe cuadrilla asignado OS',
            orse_descripcion AS 'Descripcion OS',
            (SELECT 
                GROUP_CONCAT(DISTINCT tecnicos.usua_nombre)
                FROM rel_orden_servicio_asignacion_usuario AS tecnicos_asignado 
                INNER JOIN usuario AS tecnicos ON (tecnicos.usua_id=tecnicos_asignado.usua_id)
                WHERE
                 tecnicos_asignado.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) 
                 AND tecnicos_asignado.roau_tipo='ACOMPANANTE'
                ) AS 'Técnicos asignado OS',
            (SELECT
               ROUND(SUM(prit_cantidad * lpu_item_precio.lpip_precio),2) AS pres_valor
               FROM
               presupuesto
               INNER JOIN presupuesto_item ON(presupuesto.pres_id=presupuesto_item.pres_id)
               INNER JOIN lpu_item_precio ON(presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
               WHERE orse_id=orden_servicio.orse_id
               GROUP BY presupuesto.pres_id
               ORDER BY CASE WHEN pres_estado = 'APROBADO' THEN 1 ELSE CASE WHEN pres_estado = 'PREAPROBADO' THEN 2 ELSE 3 END END, pres_fecha_creacion DESC
               LIMIT 1) AS 'Valor total Ultimo Ppto.',
            orse_estado AS 'Estado proceso OS',        
            IF(orse_estado IN ('NOACTIVO','ANULADA'),'ANULADO', IF(orse_estado IN ('RECHAZADA','APROBADA'),'FINALIZADO','ACTIVO'))AS 'Estado OS',
             
            empl_nombre AS 'Nombre EMP.',
            empl_direccion AS 'Dirección EMP.',
            #clas_nombre AS 'Clasificación EMP.',
			#clpr_nombre AS 'Clasificación Programación EMP.',
			(SELECT c.clas_nombre FROM emplazamiento_clasificacion c where c.clas_id = emplazamiento.clas_id) AS'Clasificación EMP.',
			( SELECT cl.clpr_nombre 
							FROM clasificacion_programacion cl, rel_contrato_emplazamiento rce  
							where cl.clpr_id = rce.clpr_id 
                            and cl.clpr_activo ='ACTIVO'
                            and rce.cont_id = orden_servicio.cont_id 
                            and rce.empl_id = orden_servicio.empl_id
                            
                            )AS 'Clasificación Programación EMP.',
            duto_nombre AS 'Dueño de Torre EMP.',
            IF(empl_observacion_ingreso=NULL,'NO','SI') AS 'Requiere Acceso EMP.',
            TRIM(REPLACE(REPLACE(REPLACE(empl_observacion_ingreso, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'Permisos de acceso EMP.',
            regi_nombre AS 'Región EMP.',
            (SELECT 
                zona_nombre
                FROM rel_zona_emplazamiento 
                INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' AND zona.cont_id = $cont_id)
                WHERE 
                rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                LIMIT 1) AS 'Zona MOVISTAR',
                (SELECT 
                zona_nombre
                FROM rel_zona_emplazamiento 
                INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' AND zona.cont_id = $cont_id)
                WHERE 
                    rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                LIMIT 1) AS 'ZONA DE CONTRATO CIM',
            (SELECT 
                zona_nombre
                FROM rel_zona_emplazamiento 
                INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' AND zona.cont_id = $cont_id)
                WHERE 
                    rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                LIMIT 1) AS 'Cluster',
                (SELECT DISTINCT
                    GROUP_CONCAT(DISTINCT usuario.usua_nombre)
                FROM 
                    rel_zona_emplazamiento
                INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id  AND usuario.usua_estado='ACTIVO')
                INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre='GESTOR')
                WHERE
                    rel_zona_emplazamiento.empl_id=orden_servicio.empl_id) AS 'Responsable EMP.',
                orse_tag AS 'Codigo Incidencia'                        
            FROM 
                orden_servicio
            INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
            INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
            INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)              
            INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
            LEFT JOIN rel_orden_servicio_asignacion_usuario AS jefe_asignado ON (jefe_asignado.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) AND jefe_asignado.roau_tipo='JEFECUADRILLA') 
            LEFT JOIN usuario AS jefe ON (jefe.usua_id=jefe_asignado.usua_id)
            
            INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
            #INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = emplazamiento.clas_id)
            INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
			
			#INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id = orden_servicio.empl_id AND rel_contrato_emplazamiento.cont_id = orden_servicio.cont_id )
			#LEFT JOIN clasificacion_programacion ON (clasificacion_programacion.clpr_id = rel_contrato_emplazamiento.clpr_id AND clpr_activo = 'ACTIVO')
			
            INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
            INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
            INNER JOIN region ON (region.regi_id = provincia.regi_id)

            LEFT JOIN emplazamiento_visita ON (emplazamiento_visita.emvi_id = (SELECT emvi_id  FROM emplazamiento_visita WHERE emvi_modulo='OS' AND emvi_id_relacionado=orden_servicio.orse_id AND emplazamiento_visita.empl_id=orden_servicio.empl_id ORDER BY emvi_id DESC LIMIT 1))
           
            WHERE orden_servicio.orse_id IN ('$OSs')
            $filtros
            ORDER BY orden_servicio.orse_fecha_creacion,emplazamiento_visita.emvi_fecha_ingreso ASC");
	    /*Codigo comentado posible uso*/
			/*$res = $db->ExecuteQuery("SELECT
              orse_id AS 'Num. OS',
              orse_tipo AS 'Tipo OS',
              espe_nombre AS 'Especialidad OS',
              sube_nombre AS 'Alarma OS',
              empr_nombre AS 'EECC OS',
              usua_creador AS 'Usuario creador OS',
              orse_fecha_creacion AS 'Fecha creacion OS',
              orse_fecha_solicitud AS 'Fecha programada OS',
              emvi_fecha_ingreso AS 'Fecha ingreso sitio',
              emvi_fecha_salida AS 'Fecha salida sitio',
              orse_fecha_validacion AS 'Fecha finalizacion OS',
              usua_jefe_cuadrilla AS 'Jefe cuadrilla asignado OS',
			  orse_descripcion AS 'Descripcion OS',
              orse_tecnico_asignado AS 'Tecnicos asignado OS',
              valor_ult_presupuesto AS 'Valor total Ultimo Ppto.',
              orse_estado_proceso AS 'Estado proceso OS',        
              orse_estado AS 'Estado OS',
              empl_nombre AS 'Nombre EMP.',
              empl_direccion AS 'Direccion EMP.',
              clas_nombre AS 'Clasificacion EMP.',
			  clpr_nombre AS 'Clasificacion Programacion EMP.',
              duto_nombre AS 'Dueño de Torre EMP.',
              requiere_acceso_emp AS 'Requiere Acceso EMP.',
              permisos_acceso_emp AS 'Permisos de acceso EMP.',
              regi_nombre AS 'Region EMP.',
              zona_movistar AS 'Zona MOVISTAR',
              zona_contrato AS 'ZONA DE CONTRATO CIM',
              cluster AS 'Cluster',
              responsable_emp AS 'Responsable EMP.',
              orse_tag as 'Codigo Incidencia'

                                    
            FROM 
            os_consolidado            
            WHERE orse_id IN ('$OSs')
            ORDER BY orse_fecha_creacion,emvi_fecha_ingreso ASC");*/

    if(0 == $res['status']){
        echo $res['error'];
        return;
    }

    if(0 == $res['rows']){
        echo "No hay datos disponibles";
        return;
    }
  
      /*
        require_once "../libs/PHPExcel/Classes/PHPExcel.php";
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("SIOM")
                     ->setLastModifiedBy("SIOM")
                     ->setTitle("OS Bandeja")
                     ->setDescription("");
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        $EXCEL_STYLES = array(
          'header' => array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Verdana'
            )
          ),
          'value' => array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Verdana'
            )
          )
        );
      
    //header
    $col = "A";
      $row =  1;
      foreach(array_keys($res['data'][0]) as $field){
        $sheet->setCellValue($col.$row,$field);
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

        $sheet->getColumnDimension($col)->setAutoSize( true );
        $col++; 
    }

      //datos
      $row++;
      foreach($res['data'] as $data){
        $col = "A";
        foreach($data as $key => $value){
          $sheet->setCellValue($col.$row,trim($value));
          $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
          $col++; 
        }
        $row++;
    }


    $filename = "OS_".date("dmY").".xls";
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
    header ('Pragma: public'); 

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save('php://output');
    exit;
    */
    
    $delimiter = ";";
    $filename = "OS_".date("dmY").".csv";
    header('Content-Type: application; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); 
    header ('Cache-Control: cache, must-revalidate');       
    header ('Pragma: public');
    $f = fopen('php://output', 'w');
    fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
  
    exit;

});

Flight::route('GET|POST /core/repo/contrato/@cont_id:[0-9]+/inve/emplazamiento/item', function($cont_id){

    $db = new MySQL_Database();  

    /*
      //Obtener OS filtradas
      $res = Flight::ObtenerListaOS($db,$cont_id,array_merge($_GET,$_POST));
      if ($res['status'] == 0){
          echo $res['error'];
          return;
      }
      $OSs = implode("','",array_map(function($item){ return $item['orse_id'];},$res['ordenes']));
    */
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";

    /*Obtenemos el resto de filtros*/
    $filtros = Flight::filtersToWhereString( array("inventario_elemento"), $filtros_ini).$filtros;

    $query="SELECT  inel_codigo as Codigo,
                    inel_nombre as Nombre, 
                    inel_ubicacion as Ubicación,
                    inel_descripcion as Descripción,
                    inel_fecha as Fecha,
                    inel_estado as Estado,
                    e.empl_id as emplazamiento_id,
                    e.empl_nombre as emplazamiento_nombre,
                    e.empl_direccion as emplazamiento_dirección,
                    e.empl_estado as emplazamiento_estado,
                    e.empl_nemonico as nemonico

            FROM (SELECT * FROM inventario_elemento WHERE $filtros ORDER BY inel_codigo,inel_fecha DESC) AS w INNER JOIN emplazamiento e ON w.empl_id=e.empl_id GROUP BY inel_codigo";
  
    $res = $db->ExecuteQuery( $query);

    if(0 == $res['status']){
        echo $res['error'];
        return;
    }

    if(0 == $res['rows']){
        echo "No hay datos disponibles";
        return;
    }
  
    require_once "../libs/PHPExcel/Classes/PHPExcel.php";
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("SIOM")
                 ->setLastModifiedBy("SIOM")
                 ->setTitle("OS Bandeja")
                 ->setDescription("");
    $sheet = $objPHPExcel->setActiveSheetIndex(0);
    $EXCEL_STYLES = array(
        'header' => array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Verdana'
            )
        ),
        'value' => array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Verdana'
            )
        )
    );
  
    /*Header*/
    $col = "A";
    $row =  1;
    foreach(array_keys($res['data'][0]) as $field){
        $sheet->setCellValue($col.$row,$field);
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

        $sheet->getColumnDimension($col)->setAutoSize( true );
        $col++; 
    }

    /*datos*/
    $row++;
    foreach($res['data'] as $data){
        $col = "A";
        foreach($data as $key => $value){
            $sheet->setCellValue($col.$row,trim($value));
            $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
            $col++; 
        }
        $row++;
    }

    $delimiter = ";";
    $filename = "INVE_".date("dmY").".csv";
    header('Content-Type: application; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); 
    header ('Cache-Control: cache, must-revalidate');       
    header ('Pragma: public');

    $f = fopen('php://output', 'w');
    fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
});

/*Descarga la data de la bandeja de mantenimiento Felipe Azabache*/
Flight::route('GET|POST /core/repo/contrato/@cont_id:[0-9]+/mnt/bandeja', function($cont_id){
  
    $dbo = new MySQL_Database();
    $usua_id = $_SESSION['user_id'];  
    $empr_id     = $_SESSION['empr_id'];
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";
    $filtros_excel = "";
    $pre_filtro = "";
    $post_filtro = "";

    if(isset($filtros_ini['mant_id']) && "" != $filtros_ini['mant_id']){
        $filtros .= "AND mantenimiento.mant_id = '".$filtros_ini['mant_id']."' ";
    }else{
        if(isset($filtros_ini['peri_id']) && "" != $filtros_ini['peri_id']){
            $filtros .= "AND periodicidad.peri_id = '".$filtros_ini['peri_id']."' ";
        }
        if( isset($filtros_ini['mant_fecha_programada_inicio']) && "" != $filtros_ini['mant_fecha_programada_inicio']){
            $filtros .= " AND mantenimiento.mant_fecha_programada >= '".$filtros_ini['mant_fecha_programada_inicio']." 00:00:00'"; 
        }
    	if(isset($filtros_ini['mant_fecha_programada_termino']) && "" != $filtros_ini['mant_fecha_programada_termino']){
    		$filtros .= " AND mantenimiento.mant_fecha_programada <= '".$filtros_ini['mant_fecha_programada_termino']." 23:59:59'";
    	}

        if( isset($filtros_ini['mant_estado']) &&  "" != $filtros_ini['mant_estado']){
            if(is_array($filtros_ini['mant_estado'])){ 
              $estados = implode("','",$filtros_ini['mant_estado']);
            }else{
              $estados = $filtros_ini['mant_estado'];
            }
            $filtros .= " AND mantenimiento.mant_estado IN ('".$estados."') ";
        }

    	if(isset($filtros_ini['mant_responsable']) && "" != $filtros_ini['mant_responsable']){
    		$filtros .= " AND mantenimiento.mant_responsable = '".$filtros_ini['mant_responsable']."' ";
    	}

        if( isset($filtros_ini['empl_nombre']) && "" != $filtros_ini['empl_nombre']){
            $filtros .= "AND emplazamiento.empl_nombre LIKE '%".$filtros_ini['empl_nombre']."%' ";
        }

        if( isset($filtros_ini['clas_id']) && "" != $filtros_ini['clas_id'] ){
            $filtros .= "AND mantenimiento.clas_id = '".$filtros_ini['clas_id']."' ";
        }

        if( isset($filtros_ini['empl_macrositio']) && "" != $filtros_ini['empl_macrositio'] ){
            $filtros .= "AND emplazamiento.empl_macrositio = '".$filtros_ini['empl_macrositio']."' ";
        }

        if( isset($filtros_ini['empl_subtel']) && "" != $filtros_ini['empl_subtel'] ){
            $filtros .= "AND emplazamiento.empl_subtel = '".$filtros_ini['empl_subtel']."' ";
        }

        if( isset($filtros_ini['espe_id']) && $filtros_ini['espe_id']!="" ){
            $filtros .= "AND especialidad.espe_id = '".$filtros_ini['espe_id']."' ";
        }

        if( isset($filtros_ini['regi_id']) && "" != $filtros_ini['regi_id']){
            $filtros .= "AND region.regi_id = '".$filtros_ini['regi_id']."' ";
        }

        if( (isset($filtros_ini['usua_id']) && "" != $filtros_ini['usua_id']) ||
            (isset($filtros_ini['subg_id']) && "" != $filtros_ini['subg_id']) ||
            (isset($filtros_ini['zona_id']) && "" != $filtros_ini['zona_id']) ||  
            (isset($filtros_ini['clus_id']) && "" != $filtros_ini['clus_id']) ){
            $pre_filtro = "SELECT * FROM(";
            $post_filtro = ") AS T WHERE ";

            $con_filtro = false;
            if(isset($filtros_ini['usua_id']) && "" != $filtros_ini['usua_id']){
                $post_filtro .= " UsuarioAsignadoMNT = (SELECT usua_nombre FROM usuario WHERE usua_id='".$filtros_ini['usua_id']."') ";
                $con_filtro = true;
            }
          
            if(isset($filtros_ini['subg_id']) && "" != $filtros_ini['subg_id']){
                if($con_filtro){
                    $post_filtro .= "AND"; 
                }
                $post_filtro .= " `Zona MOVISTAR` = (SELECT zona_nombre FROM zona WHERE zona_id='".$filtros_ini['subg_id']."') ";
                $con_filtro = true;
            }

            if(isset($filtros_ini['zona_id']) && "" != $filtros_ini['zona_id']){
                if($con_filtro){
                    $post_filtro .= "AND"; 
                }
                $post_filtro .= " `ZONA DE CONTRATO CIM` = (SELECT zona_nombre FROM zona WHERE zona_id='".$filtros_ini['zona_id']."') ";
                $con_filtro = true;
            }

            if(isset($filtros_ini['clus_id']) && "" != $filtros_ini['clus_id']){
                if($con_filtro){
                    $post_filtro .= "AND"; 
                }
                $post_filtro .= " `Cluster` = (SELECT zona_nombre FROM zona WHERE zona_id='".$filtros_ini['clus_id']."') ";
            }  
        }
            if( isset($filtros_ini['usua_creador']) && "" != $filtros_ini['usua_creador']){
                $filtros .= " AND mantenimiento.usua_creador='".$filtros_ini['usua_creador']."' ";
                unset( $filtros_ini['usua_creador'] );            
            }
    }
    $usuario_tipo = $dbo->ExecuteQuery("
                                    SELECT rcem.coem_tipo
                                    FROM rel_contrato_empresa rcem 
                                        , usuario u
                                    WHERE 
                                        rcem.coem_tipo='CLIENTE'
                                    AND rcem.cont_id =$cont_id
                                    AND rcem.coem_estado = 'ACTIVO' 
                                    AND rcem.empr_id =u.empr_id
                                    AND u.usua_id = $usua_id ;
    ");
    if (0 == $usuario_tipo['status'] ){ 
        Flight::json(array("status" => 0, "error" => $usuario_tipo['error']));
    }
    if(0 == $usuario_tipo['rows']){
        $filtros .= " AND mantenimiento.empr_id = $empr_id ";
    }

    $query = $pre_filtro;
    $query.= "SELECT DISTINCT
                    mantenimiento.mant_id AS 'Nº MNT',
                    peri_nombre AS 'Período MNT',
                    espe_nombre AS 'Especialidad MNT',
                    empr_nombre AS 'EECC MNT',
                    mant_fecha_programada AS 'Fecha programada MNT',
                    emplazamiento_visita.emvi_fecha_ingreso AS 'Fecha ingreso sitio',
                    emplazamiento_visita.emvi_fecha_salida AS 'Fecha salida sitio',
                    mant_fecha_ejecucion AS 'Fecha ejecuci�n MNT',
					mant_fecha_validacion AS 'fecha de validaci�n',
                    usuario_asignado.usua_nombre AS 'UsuarioAsignadoMNT',
                    mant_estado AS 'Estado proceso MNT',
                     (SELECT MAX(tare_fecha_despacho) FROM tarea WHERE tare_modulo='MNT' AND tare_id_relacionado=mantenimiento.mant_id) AS 'Fecha proceso MNT',
                    IF(mant_estado IN ('NOACTIVO', 'ANULADA'),'ANULADO', IF(mant_estado IN ('RECHAZADA','APROBADA','NO REALIZADO'),'FINALIZADO','ACTIVO'))AS 'Estado MNT',
                    
                    empl_nombre AS 'Nombre EMP.',
                    empl_direccion AS 'Dirección EMP.',
                    clas_nombre AS 'Clasificación EMP.',
                    #clpr_nombre AS 'Clasificación Programación EMP.',
					(SELECT clpr_nombre FROM clasificacion_programacion WHERE clpr_id = mantenimiento.clpr_id) AS 'Clasificación Programación EMP.',
                    empl_macrositio AS 'Macrositio EMP.',
                    empl_subtel AS 'Subtel EMP.',
                    duto_nombre AS 'Dueño de Torre EMP.',
                    IF(empl_observacion_ingreso=NULL,'NO','SI') AS 'Requiere Acceso EMP.',
                    TRIM(REPLACE(REPLACE(REPLACE(empl_observacion_ingreso, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'Permisos de acceso EMP.',
                    regi_nombre AS 'Región EMP.',
                    (SELECT 
                      zona_nombre
                     FROM rel_zona_emplazamiento 
                     INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' AND zona.cont_id = $cont_id)
                     WHERE 
                      rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                     LIMIT 1) AS 'Zona MOVISTAR',
                    (SELECT 
                      zona_nombre
                     FROM rel_zona_emplazamiento 
                     INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' AND zona.cont_id = $cont_id)
                     WHERE 
                      rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                     LIMIT 1) AS 'ZONA DE CONTRATO CIM',
                    (SELECT 
                      zona_nombre
                     FROM rel_zona_emplazamiento 
                     INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' AND zona.cont_id = $cont_id)
                     WHERE 
                      rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                     LIMIT 1) AS 'Cluster',
                     (SELECT 
                      usua_nombre
          						FROM usuario u 
          						INNER JOIN rel_zona_usuario rzu ON (rzu.usua_id = u.usua_id)
          						INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rzu.zona_id)
                    WHERE 
                                rze.empl_id = emplazamiento.empl_id
          					  LIMIT 1) AS 'Responsable EMP.',
          					  mantenimiento.mant_observacion AS 'Observacion anulacion', 
                      (SELECT i.info_estado 
                            FROM informe i
                            WHERE i.info_id_relacionado = mantenimiento.mant_id 
                            AND i.info_modulo = 'MNT' order by i.info_id desc
                      LIMIT 1) AS 'Estado informe', 
                      (SELECT TRIM(REPLACE(REPLACE(REPLACE(i.info_observacion, '\n', ' '), '\r', ' '), '\t', ' ')) 
                            FROM informe i 
                            WHERE i.info_id_relacionado = mantenimiento.mant_id 
                            AND i.info_modulo = 'MNT' order by i.info_id desc
                      LIMIT 1) AS 'Observacion Est. Ult. Informe'
                  FROM 
                  contrato
                  INNER JOIN mantenimiento ON (mantenimiento.cont_id = contrato.cont_id)
                  INNER JOIN empresa ON (empresa.empr_id = mantenimiento.empr_id) 
                  INNER JOIN especialidad ON (especialidad.espe_id = mantenimiento.espe_id)   
                  INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
                  INNER JOIN rel_contrato_periodicidad ON (rel_contrato_periodicidad.rcpe_id = mantenimiento_periodos.rcpe_id) 
                  INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)
                      
                  LEFT JOIN rel_mantenimiento_asignacion_usuario ON (rel_mantenimiento_asignacion_usuario.maas_id = (SELECT maas_id FROM mantenimiento_asignacion WHERE mantenimiento_asignacion.mant_id = mantenimiento.mant_id ORDER BY maas_id DESC LIMIT 1) AND rmau_tipo='JEFECUADRILLA')  
                  LEFT JOIN usuario AS usuario_asignado ON (usuario_asignado.usua_id=rel_mantenimiento_asignacion_usuario.usua_id)
                              
                  INNER JOIN emplazamiento ON (emplazamiento.empl_id = mantenimiento.empl_id)         
                  INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = mantenimiento.clas_id)
                  INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
                  INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
                  INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
                  INNER JOIN region ON (region.regi_id = provincia.regi_id)
          
          #INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id = mantenimiento.empl_id AND rel_contrato_emplazamiento.cont_id = mantenimiento.cont_id )
          #LEFT JOIN clasificacion_programacion ON (clasificacion_programacion.clpr_id = rel_contrato_emplazamiento.clpr_id AND clpr_activo = 'ACTIVO')
      
                  INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)
                  INNER JOIN zona AS zona_alcance ON (rel_zona_emplazamiento.zona_id = zona_alcance.zona_id AND zona_alcance.zona_tipo='ALCANCE' 
                                                        AND zona_alcance.cont_id = mantenimiento.cont_id)  
                  INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.cont_id=contrato.cont_id AND rel_contrato_usuario.usua_id=$usua_id)
                  INNER JOIN rel_contrato_usuario_alcance ON (rel_contrato_usuario_alcance.recu_id = rel_contrato_usuario.recu_id AND rel_contrato_usuario_alcance.zona_id=zona_alcance.zona_id)
          
          LEFT JOIN emplazamiento_visita ON (emplazamiento_visita.emvi_id = (SELECT emvi_id  FROM emplazamiento_visita WHERE emvi_modulo='MNT' AND emvi_id_relacionado=mantenimiento.mant_id AND emplazamiento_visita.empl_id=mantenimiento.empl_id ORDER BY emvi_id DESC LIMIT 1))
                  WHERE contrato.cont_id=$cont_id
                  $filtros
                  ORDER BY mantenimiento.mant_fecha_programada ASC";
    $query.= $post_filtro;

    $res = $dbo->ExecuteQuery($query);
    if(0 == $res['status']){
        echo $res['error'];
        return;
    }

    if(0 == $res['rows']){
        echo "No hay datos disponibles";
        return;
    }
    /*
    require_once "../libs/PHPExcel/Classes/PHPExcel.php";
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("SIOM")
                   ->setLastModifiedBy("SIOM")
                   ->setTitle("OS Bandeja")
                   ->setDescription("");
    $sheet = $objPHPExcel->setActiveSheetIndex(0);
    $EXCEL_STYLES = array(
        'header' => array(
          'font'  => array(
              'bold'  => true,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        ),
        'value' => array(
          'font'  => array(
              'bold'  => false,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        )
    );
    
    //header
    $col = "A";
    $row =  1;
    foreach(array_keys($res['data'][0]) as $field){
      $sheet->setCellValue($col.$row,$field);
      $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

      $sheet->getColumnDimension($col)->setAutoSize( true );
      $col++; 
    }

    //datos
    $row++;
    foreach($res['data'] as $data){
      $col = "A";
      foreach($data as $key => $value){
        $sheet->setCellValue($col.$row,trim($value));
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
        $col++; 
      }
      $row++;
    }

    $filename = "filtro_mnt.xls";
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
    header ('Pragma: public'); 

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save('php://output');
    exit;
    */
    /*csv*/

    $delimiter = ";";
    $filename = "MNT_".date("dmY").".csv";
    header('Content-Type: application/csv; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');     
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); 
    header ('Cache-Control: cache, must-revalidate');      
    header ('Pragma: public');
    $f = fopen('php://output', 'w');
	fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
});

Flight::route('GET|POST /core/repo/contrato/@cont_id:[0-9]+/insp/bandeja', function($cont_id){
  /*session_start();*/
  $usua_id = $_SESSION['user_id'];  

  $filtros_ini = array_merge($_GET,$_POST);
  $filtros = "";
  
  //Eliminamos las llaves vacias
  foreach ($filtros_ini as $key => $value) {
      if( is_null($value) ){
          unset($filtros_ini[$key]);
      }
      if( !is_array($value) && strlen(trim($value.""))==0 ){
          unset($filtros_ini[$key]);
      }
  }

  //Sacamos los filtros especiales del arreglo de filtros
  
  if( isset($filtros_ini['insp_fecha_solicitud_inicio']) ){
      $filtros .= " AND i.insp_fecha_solicitud >= '".$filtros_ini['insp_fecha_solicitud_inicio']." 00:00:00' "    ; 
      unset($filtros_ini['insp_fecha_solicitud_inicio']);
  }
  
  if( isset($filtros_ini['insp_fecha_solicitud_termino']) ){
      $filtros .= " AND i.insp_fecha_solicitud <= '".$filtros_ini['insp_fecha_solicitud_termino']." 23:59:59' "    ; 
      unset($filtros_ini['insp_fecha_solicitud_termino']);
  }
  
  if( isset($filtros_ini['usuario_inspeccion']) ){
      $filtros .= " AND riau.usua_id=".$filtros_ini['usuario_inspeccion']." ";
      unset( $filtros_ini['usuario_inspeccion'] );            
  }    
  
  if( isset($filtros_ini['tecnico_mantenimiento']) ){
      $filtros .= " AND rmau.usua_id=".$filtros_ini['tecnico_mantenimiento']." ";
      unset( $filtros_ini['tecnico_mantenimiento'] );            
  }   
  
  if( isset($filtros_ini['insp_id']) ){
      $filtros .= " AND i.insp_id=".$filtros_ini['insp_id']." ";
      unset( $filtros_ini['insp_id'] );            
  }
  
  if( isset($filtros_ini['mant_id']) ){
      $filtros .= " AND m.mant_id=".$filtros_ini['mant_id']." ";
      unset( $filtros_ini['mant_id'] );            
  }
  
  if( isset($filtros_ini['orse_id']) ){
      $filtros .= " AND o.orse_id=".$filtros_ini['orse_id']." ";
      unset( $filtros_ini['orse_id'] );            
  }

  //Obtenemos el resto de filtros
  $filtros = Flight::filtersToWhereString( array("inspeccion", "orden_servicio", "emplazamiento"), $filtros_ini).$filtros;
  
  $query = "SELECT SQL_CALC_FOUND_ROWS * FROM
              (
                      SELECT 
                              i.*
                              ,e.empl_nombre
                              ,e.empl_nemonico
                              ,m.mant_fecha_ejecucion
                              ,empr.empr_nombre                         
                      FROM 
                              inspeccion i
                      LEFT JOIN mantenimiento m ON i.mant_id = m.mant_id
                      LEFT JOIN orden_servicio o ON i.orse_id = o.orse_id 

                      LEFT JOIN inspeccion_asignacion ia ON i.insp_id = ia.insp_id
                      LEFT JOIN rel_inspeccion_asignacion_usuario riau ON (ia.inas_id = riau.inas_id AND riau.rinu_tipo = 'JEFECUADRILLA') 

                      LEFT JOIN mantenimiento_asignacion ma ON m.mant_id = ma.mant_id
                      LEFT JOIN rel_mantenimiento_asignacion_usuario rmau ON (ma.maas_id = rmau.maas_id AND rmau.rmau_tipo = 'JEFECUADRILLA')

                      INNER JOIN emplazamiento e ON i.empl_id = e.empl_id

                      INNER JOIN empresa empr ON (empr.empr_id = i.empr_id)
                      WHERE
                      i.cont_id = $cont_id
                      AND ".$filtros."
              ) t
          GROUP BY insp_id;";

    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    if($res['status']==0){
        echo $res['error'];
        return;
    }

    if(0==$res['rows']){
      echo "No hay datos disponibles";
      return;
    }


    require_once "../libs/PHPExcel/Classes/PHPExcel.php";
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("SIOM")
                   ->setLastModifiedBy("SIOM")
                   ->setTitle("OS Bandeja")
                   ->setDescription("");
    $sheet = $objPHPExcel->setActiveSheetIndex(0);
    $EXCEL_STYLES = array(
        'header' => array(
          'font'  => array(
              'bold'  => true,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        ),
        'value' => array(
          'font'  => array(
              'bold'  => false,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        )
    );
    
    //header
    $col = "A";
    $row =  1;
    foreach(array_keys($res['data'][0]) as $field){
      $sheet->setCellValue($col.$row,$field);
      $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

      $sheet->getColumnDimension($col)->setAutoSize( true );
      $col++; 
    }

    //datos
    $row++;
    foreach($res['data'] as $data){
      $col = "A";
      foreach($data as $key => $value){
        $sheet->setCellValue($col.$row,trim($value));
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
        $col++; 
      }
      $row++;
    }


//    $filename = "filtro_insp.xls";
//    header('Content-Type: application/vnd.ms-excel');
//    header('Content-Disposition: attachment; filename="'.$filename.'"');
//    header('Cache-Control: max-age=0');
//    header('Cache-Control: max-age=1');
//    header('Set-Cookie: fileDownload=true; path=/');
//    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
//    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
//    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
//    header ('Pragma: public'); 

//    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
//    $objWriter->save('php://output');
//    exit;

  $delimiter = ";";
  $filename = "filtro_insp.csv";
  header('Content-Type: application; charset=ISO-8859-1');
  header('Content-Disposition: attachment; filename="'.$filename.'";');
  header('Cache-Control: max-age=0');
  header('Cache-Control: max-age=1');
  header('Set-Cookie: fileDownload=true; path=/');
  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
  header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
  header ('Pragma: public');

  $f = fopen('php://output', 'w');
  fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
  fputcsv($f, array_keys($res['data'][0]), $delimiter);

  foreach ($res['data'] as $data) {
    fputcsv($f, $data, $delimiter);
  }
  
  exit;
});



Flight::route('GET|POST /core/repo/contrato/@cont_id:[0-9]+/emplazamientos', function($cont_id){
    //session_start();
    $usua_id = $_SESSION['user_id'];   
    
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";
    $filtros_zonas_contrato_flag = FALSE;
    $filtros_zonas_cluster_flag = FALSE; 
        
    //Sacamos los filtros especiales del arreglo de filtros
    if( isset($filtros_ini['zona_id']) ){
        if( strlen(trim($filtros_ini['zona_id']))>0 ){
            $filtros .= " AND rzeco.zona_id=".$filtros_ini['zona_id']." AND rzeco.empl_id=em.empl_id AND rzeco.zona_id=zco.zona_id AND zco.zona_tipo='CONTRATO' ";
            $filtros_zonas_contrato_flag = TRUE;
        }
        unset( $filtros_ini['zona_id'] );
    }
    
    if( isset($filtros_ini['clus_id']) ){
        if( strlen(trim($filtros_ini['clus_id']))>0 ){
            $filtros .= " AND rzecl.zona_id=".$filtros_ini['clus_id']."  AND rzecl.empl_id=em.empl_id AND rzecl.zona_id=zcl.zona_id AND  zcl.zona_tipo='CLUSTER' ";
            $filtros_zonas_cluster_flag = TRUE;
        }
        unset( $filtros_ini['clus_id'] );
    }   
    
    if( isset($filtros_ini['tecn_id']) ){
        if(is_array($filtros_ini['tecn_id'])){
          $filtros .= " AND t.tecn_id IN ('".implode("','",$filtros_ini['tecn_id'])."')";
        }
        else{
          $filtros .= " AND t.tecn_id=".$filtros_ini['tecn_id']; 
        }
        unset( $filtros_ini['tecn_id'] );
    } 
    
    if( isset($filtros_ini['regi_id']) ){
        if( strlen(trim($filtros_ini['regi_id']))>0 ){
            $filtros .= " AND r.regi_id=".$filtros_ini['regi_id']." ";
        }
        unset( $filtros_ini['regi_id'] );            
    }


    if( isset($filtros_ini['cate_id']) ){
        if(is_array($filtros_ini['cate_id'])){
            $filtros .= " AND emca.cate_id in (".implode(",",$filtros_ini['cate_id']).")";
        } else {
            $filtros .= " AND emca.cate_id=".$filtros_ini['cate_id'];    
        }
        unset( $filtros_ini['cate_id'] );            
    }

    $orse_indisponibilidad = "";
    if( isset($filtros_ini['orse_indisponibilidad'])){
      $orse_indisponibilidad = $filtros_ini['orse_indisponibilidad'];
      unset( $filtros_ini['orse_indisponibilidad']);  
    }
    
    /*Obtenemos el resto de filtros*/
    $filtros = Flight::filtersToWhereString( array("emplazamiento"), $filtros_ini).$filtros;
    
    
    $query = "SELECT 
                em.empl_id AS id
                ,em.empl_nemonico AS nemonico
                ,em.empl_nombre AS nombre
                ,em.empl_direccion AS dirección
                ,co.comu_nombre AS comuna
                ,r.regi_nombre AS región
                ,(
                    SELECT 
                      zona_nombre
                    FROM rel_zona_emplazamiento 
					INNER JOIN rel_contrato_emplazamiento recem on (recem.empl_id = rel_zona_emplazamiento.empl_id)
                    INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' and zona.cont_id = recem.cont_id)
                    WHERE 
                      rel_zona_emplazamiento.empl_id = em.empl_id
                    LIMIT 1
                ) AS 'zona MOVISTAR'
                ,(
                    SELECT 
                      zona_nombre
                    FROM rel_zona_emplazamiento 
					INNER JOIN rel_contrato_emplazamiento recem on (recem.empl_id = rel_zona_emplazamiento.empl_id)
                    INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' and zona.cont_id = recem.cont_id)
                    WHERE 
                      rel_zona_emplazamiento.empl_id = em.empl_id
                    LIMIT 1
                ) AS 'ZONA DE CONTRATO CIM'
                ,emcl.clas_nombre AS clasificación
				        ,clpr.clpr_nombre AS 'clasificación programación'
                ,(
                    SELECT 
                      usua_nombre
                    FROM usuario u 
                        INNER JOIN rel_zona_usuario rzu ON (rzu.usua_id = u.usua_id)
					              INNER JOIN rel_zona_emplazamiento rze ON (rze.zona_id = rzu.zona_id)
                    WHERE 
                        rze.empl_id = em.empl_id
                    LIMIT 1
				        ) AS 'encargado'
                ,em.empl_referencia AS referencia
                ,em.empl_tipo AS tipo
                ,em.empl_tipo_acceso  AS tipo_acceso
                ,em.empl_espacio  AS espacio
                ,em.empl_macrositio AS macrositio
                ,em.empl_subtel AS subtel
                ,em.empl_distancia  AS distancia
                ,em.empl_observacion AS observacion
                ,em.empl_id_emplazamiento_atix  AS id_emplazamiento_atix
                ,TRIM(REPLACE(REPLACE(REPLACE(em.empl_observacion_ingreso, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'observación ingreso'
                ,em.empl_fecha_creacion AS 'fecha creación'
                ,em.empl_estado AS 'estado'
                ,em.empl_subestado AS 'subestado'
                ,uc.usua_nombre AS creador
                ,'NO' as indisponibilidad
                , emca.cate_nombre as 'categoría'
                , emdt.duto_nombre as 'Dueno Torre'"
            . "FROM "
                . "contrato c, "
                . "usuario uc, "
                . "rel_contrato_emplazamiento rce, "
                . "emplazamiento em, "
				        . "clasificacion_programacion clpr, "
                . "comuna co, "
                . "region r,"
                . "provincia p, "
                . "emplazamiento_clasificacion emcl, "
                . "emplazamiento_categoria emca, "
                . "emplazamiento_dueno_torre emdt, "
                . "tecnologia t, "
                . "rel_emplazamiento_tecnologia ret, "
                . "usuario u, rel_contrato_usuario rcu, "
                . "rel_contrato_usuario_alcance rcua , rel_zona_emplazamiento rzeal, zona zal "
                .(($filtros_zonas_contrato_flag)?", rel_zona_emplazamiento rzeco, zona zco ":"" )
                .(($filtros_zonas_cluster_flag)?", rel_zona_emplazamiento rzecl, zona zcl ":"" )
            . "WHERE "
                . "c.cont_id=".$cont_id." "
                . "AND rce.cont_id=c.cont_id "
                . "AND rce.empl_id=em.empl_id "
                . "AND em.comu_id=co.comu_id "
                . "AND p.prov_id = co.prov_id "
                . "AND r.regi_id = p.regi_id "
                . "AND em.clas_id=emcl.clas_id "
                . "AND em.cate_id=emca.cate_id "                              
                . "AND em.duto_id=emdt.duto_id "
                . "AND em.usua_creador=uc.usua_id "
                . "AND ret.empl_id=em.empl_id "
                . "AND ret.tecn_id=t.tecn_id "
                . "AND rzeal.empl_id=em.empl_id "
                . "AND rzeal.zona_id=zal.zona_id "
                . "AND zal.zona_tipo='ALCANCE' "
                . "AND u.usua_id = $usua_id "
                . "AND rcu.usua_id = u.usua_id "
                . "AND rcu.cont_id = c.cont_id "
                . "AND rcua.recu_id = rcu.recu_id "
                . "AND rcua.zona_id = rzeal.zona_id "
				        . "AND clpr.clpr_id = rce.clpr_id "
				        . "AND clpr.clpr_activo = 'ACTIVO' "                           
                . "AND ".$filtros." "
            . "GROUP BY em.empl_id " 
            ;
    
    $dbo = new MySQL_Database();
    //Flight::json($query);
    $res = $dbo->ExecuteQuery($query);    
    if ($res['status'] == 0){
      Flight::json(array("status" => 0, "error" => $res['error']));
    }
     
    //Agregamos la indisponibilidad de los emplazamientos
    $resOS = $dbo->ExecuteQuery("SELECT 
                                        orse_id
                                        ,empl_id
                                        ,orse_indisponibilidad
                                FROM 
                                        orden_servicio 
                                WHERE
                                        cont_id = 1
                                        AND orse_estado NOT IN ('NOACTIVO', 'APROBADA', 'RECHAZADA')  
                                        AND orse_indisponibilidad != 'NO' 
                                ORDER BY 
                                        empl_id ASC");    
    if ($resOS['status'] == 0) Flight::json(array("status" => 0, "error" => $resOS['error']));
        
    foreach($res['data'] as $i => &$emplazamiento) {
        
        foreach($resOS['data'] as $key => &$os) {
            if( $emplazamiento['id'] == $os['empl_id'] ) {
                if( $emplazamiento['indisponibilidad'] != 'SI' ){
                    $emplazamiento['indisponibilidad'] = $os['orse_indisponibilidad'];
                }
                unset( $resOS['data'][$key] );
            }
        }   

        if($orse_indisponibilidad!=""){
            if($emplazamiento['indisponibilidad']!=$orse_indisponibilidad){
              unset($res['data'][$i]);
            }
        }
    }

    $out['data'] = array_values($res['data']);


    if(0==count($out['data'])){
      echo "No hay datos disponibles";
      return;
    }
    
    require_once "../libs/PHPExcel/Classes/PHPExcel.php";
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("SIOM")
                   ->setLastModifiedBy("SIOM")
                   ->setTitle("Emplazamientos")
                   ->setDescription("");
    $sheet = $objPHPExcel->setActiveSheetIndex(0);
    $EXCEL_STYLES = array(
        'header' => array(
          'font'  => array(
              'bold'  => true,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        ),
        'value' => array(
          'font'  => array(
              'bold'  => false,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        )
    );
    
    //header
    $col = "A";
    $row =  1;
    foreach(array_keys($out['data'][0]) as $field){
      $sheet->setCellValue($col.$row,$field);
      $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

      $sheet->getColumnDimension($col)->setAutoSize( true );
      $col++; 
    }

    //datos
    $row++;
    foreach($out['data'] as $data){
      $col = "A";
      foreach($data as $key => $value){
        $sheet->setCellValue($col.$row,trim($value));
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
        $col++; 
      }
      $row++;
    }


//    $filename = "filtro_emplazamientos.xls";
//    header('Content-Type: application/vnd.ms-excel');
  //  header('Content-Disposition: attachment; filename="'.$filename.'"');
  //  header('Cache-Control: max-age=0');
  //  header('Cache-Control: max-age=1');
  //  header('Set-Cookie: fileDownload=true; path=/');
  //  header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
  //  header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
  //  header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
  //  header ('Pragma: public'); 

  //  $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
  //  $objWriter->save('php://output');
  //  exit;
  
    $delimiter = ";";
    $filename = "filtro_emplazamientos.csv";
    header('Content-Type: application/csv; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
    header ('Pragma: public');

    $f = fopen('php://output', 'w');
	fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
});


Flight::route('GET|POST /core/repo/contrato/@cont_id:[0-9]+/os/reporte/sap', function($cont_id){
  //$_POST
  $filtros_ini = array_merge($_GET,$_POST);
  $filtros = "";
  $filtros_excel = "";
  $pre_filtro = "";
  $post_filtro = "";

  try{
    if(isset($filtros_ini['orse_id']) && $filtros_ini['orse_id']!=""){
      $filtros .= "AND orden_servicio.orse_id = '".$filtros_ini['orse_id']."' ";
    }
    else{
      if(isset($filtros_ini['orse_tipo']) && $filtros_ini['orse_tipo']!=""){
        $filtros .= "AND orden_servicio.orse_tipo = '".$filtros_ini['orse_tipo']."' ";
      }
	  
	  if( isset($filtros_ini['orse_fecha_solicitud_inicio']) && $filtros_ini['orse_fecha_solicitud_inicio']!=""){
        $filtros .= " AND orden_servicio.orse_fecha_solicitud >= '".$filtros_ini['orse_fecha_solicitud_inicio']." 00:00:00' "    ; 
	  }
	  
	  if( isset($filtros_ini['orse_fecha_solicitud_termino']) && $filtros_ini['orse_fecha_solicitud_termino']!=""){
        $filtros .= " AND orden_servicio.orse_fecha_solicitud <= '".$filtros_ini['orse_fecha_solicitud_termino']." 23:59:59' "    ; 
	  }	  

      if( isset($filtros_ini['orse_responsable']) && $filtros_ini['orse_responsable']!="" ){
        $filtros .= "AND orden_servicio.orse_responsable = '".$filtros_ini['orse_responsable']."' ";
      }

      if( isset($filtros_ini['empl_nemonico']) && $filtros_ini['empl_nemonico']!="" ){
        $filtros .= "AND emplazamiento.empl_nemonico LIKE '%".$filtros_ini['empl_nemonico']."%' ";
      }

      if( isset($filtros_ini['empl_nombre']) && $filtros_ini['empl_nombre']!=""){
        $filtros .= "AND emplazamiento.empl_nombre LIKE '%".$filtros_ini['empl_nombre']."%' ";
      }

      if( isset($filtros_ini['tecn_id']) && $filtros_ini['tecn_id']!="" ){
        /*
        if(is_array($filtros_ini['tecn_id'])){
            $filtros .= "AND (";
            $filtros .= $filtros_ini['tecn_id'][0]."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
            for($i=1;$i<count($filtros_ini['tecn_id']);$i++){
              $filtros .= " OR ".$filtros_ini['tecn_id'][$i]."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
            }
            $filtros .= ") ";
        }
        else{
        */
          $filtros .= "AND '".$filtros_ini['tecn_id']."' IN (SELECT tecn_id FROM rel_emplazamiento_tecnologia WHERE rel_emplazamiento_tecnologia.empl_id=emplazamiento.empl_id) ";
        //}
      }

      if( isset($filtros_ini['empl_direccion']) && $filtros_ini['empl_direccion']!="" ){
        $filtros .= "AND emplazamiento.empl_direccion LIKE '%".$filtros_ini['empl_direccion']."%' ";
      }

      if( (isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!="") ||  
          (isset($filtros_ini['clus_id']) && $filtros_ini['clus_id']!="") ){
          $pre_filtro = "SELECT * FROM(";
          $post_filtro = ") AS T WHERE ";
          if(isset($filtros_ini['zona_id']) && $filtros_ini['zona_id']!=""){
              $post_filtro .= " ZonaContratoEMP = (SELECT zona_nombre FROM zona WHERE zona_id='".$filtros_ini['zona_id']."') ";
              if(isset($filtros_ini['clus_id'])){
                $post_filtro .= "AND"; 
              }
          }
          if(isset($filtros_ini['clus_id']) && $filtros_ini['clus_id']!=""){
              $post_filtro .= " ZonaClusterEMP = (SELECT zona_nombre FROM zona WHERE zona_id='".$filtros_ini['clus_id']."') ";
          }  
      }

      if( isset($filtros_ini['regi_id']) && $filtros_ini['regi_id']!="" ){
        $filtros .= "AND region.regi_id = '".$filtros_ini['regi_id']."' ";
      }
    }

    //filtrar sólo a aprobadas      
    $filtros .= " AND orden_servicio.orse_estado = 'APROBADA' ";


    $query = $pre_filtro;
    $query.= "SELECT
                orden_servicio.orse_id                   
              FROM 
              contrato
              INNER JOIN orden_servicio ON (orden_servicio.cont_id = contrato.cont_id)
              INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
              INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
              INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)              
              INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
              LEFT JOIN rel_orden_servicio_asignacion_usuario ON (rel_orden_servicio_asignacion_usuario.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) AND roau_tipo='JEFECUADRILLA') 
              LEFT JOIN usuario AS usuario_asignado ON (usuario_asignado.usua_id=rel_orden_servicio_asignacion_usuario.usua_id)
                          
              INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
              INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = emplazamiento.clas_id)
              INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
              INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
              INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
              INNER JOIN region ON (region.regi_id = provincia.regi_id)
                                    
              WHERE contrato.cont_id=$cont_id
              $filtros
              ORDER BY orden_servicio.orse_fecha_creacion ASC";
    $query.= $post_filtro;

    $db = new MySQL_Database();
    $res = $db->ExecuteQuery($query);
    if($res['status']==0){
        echo $res['error'];
        return;
    }

    if(0==$res['rows']){
      echo "No hay datos ordenes de servicios APROBADAS que cumplan los filtros";
      return;
    }

    $data         = $res['data'];
    $presupuestos = array();
    foreach($data as $row){
      $orse_id = $row['orse_id'];
      $res = $db->ExecuteQuery("SELECT 
                                pres_id
                               FROM
                                presupuesto
                                WHERE
                                pres_estado='APROBADO' AND orse_id=$orse_id
                                ORDER BY pres_fecha_validacion DESC
                                LIMIT 1");
      if($res['status']==0){
        echo $res['error'];
        return;
      }

      if(0<$res['rows']){
        array_push($presupuestos,$res['data'][0]['pres_id']);
      }
    }


    if(0==count($presupuestos)){
      echo "No hay presupuestos PREAPROBADOS para las ordenes de servicios encontradas";
      return;
    }

    $presupuestos = implode("','",$presupuestos);

    $res = $db->ExecuteQuery("SELECT
                    orden_servicio.orse_id AS 'Nº OS',
                    orse_tipo AS 'Tipo OS',
                    espe_nombre AS 'Especialidad OS',
                    sube_nombre AS 'Alarma OS',
                    empr_nombre AS 'EECC OS',
                    usuario_creador.usua_nombre AS 'Usuario creador OS',
                    orse_fecha_creacion AS 'Fecha creación OS',
                    orse_fecha_solicitud AS 'Fecha solicitud OS',
                    usuario_asignado.usua_nombre AS 'Usuario asignado OS',
                    orse_descripcion AS 'Descripcion OS',
                    orse_estado AS 'Estado proceso OS',
                     (SELECT MAX(tare_fecha_despacho) FROM tarea WHERE tare_modulo='OS' AND tare_id_relacionado=orden_servicio.orse_id) AS 'Fecha ejecución OS',
                    IF(orse_estado='NOACTIVO','ANULADO', IF(orse_estado='RECHAZADA' OR orse_estado='APROBADA','FINALIZADO','ACTIVO'))AS 'Estado OS',
                    
                    empl_nombre AS 'Nombre EMP.',
                    empl_direccion AS 'Dirección EMP.',
                    clas_nombre AS 'Clasificación EMP.',
                    duto_nombre AS 'Dueño de Torre EMP.',
                    IF(empl_observacion_ingreso=NULL,'NO','SI') AS 'Requiere Acceso EMP.',
                    empl_observacion_ingreso AS 'Permisos de acceso EMP.',
                    regi_nombre AS 'Región EMP.',
                    (SELECT 
                      zona_nombre
                     FROM rel_zona_emplazamiento 
                     INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' AND zona.cont_id = $cont_id)
                     WHERE 
                      rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                     LIMIT 1) AS 'Zona Movistar',
                    (SELECT 
                      zona_nombre
                     FROM rel_zona_emplazamiento 
                     INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' AND zona.cont_id = $cont_id)
                     WHERE 
                      rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                     LIMIT 1) AS 'ZONA DE CONTRATO CIM',
                    (SELECT 
                      zona_nombre
                     FROM rel_zona_emplazamiento 
                     INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' AND zona.cont_id = $cont_id)
                     WHERE 
                      rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
                     LIMIT 1) AS 'Cluster',
                     (SELECT 
                      usua_nombre
                      FROM 
                      rel_zona_emplazamiento
                      INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                      INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                      INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_cargo='GESTOR')
                      WHERE
                      rel_zona_emplazamiento.empl_id=emplazamiento.empl_id) AS 'Responsable EMP.',
                      
                    lpu_item_precio.lpip_id AS 'Id LPU',
                    lpgr_nombre AS 'Grupo LPU', 
                      lpit_nombre AS 'Item LPU',
                      lpip_sap_opex AS 'SAP OPEX LPU',
                    lpip_sap_capex AS 'SAP CAPEX LPU',
                      lpip_precio AS 'Valor LPU',
                      prit_cantidad AS 'Cantidad LPU',
                    lpip_precio*prit_cantidad AS 'Subtotal LPU'                     
                                          
                  FROM 
                  presupuesto
                  INNER JOIN presupuesto_item ON (presupuesto_item.pres_id = presupuesto.pres_id)
                  INNER JOIN lpu_item_precio ON (presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
                  INNER JOIN lpu_item ON (lpu_item_precio.lpit_id=lpu_item.lpit_id)
                  INNER JOIN lpu_grupo ON (lpu_item.lpgr_id=lpu_grupo.lpgr_id)

                  INNER JOIN orden_servicio ON (orden_servicio.orse_id = presupuesto.orse_id)
                  INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
                  INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
                  INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)              
                  INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
                  LEFT JOIN rel_orden_servicio_asignacion_usuario ON (rel_orden_servicio_asignacion_usuario.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) AND roau_tipo='JEFECUADRILLA') 
                  LEFT JOIN usuario AS usuario_asignado ON (usuario_asignado.usua_id=rel_orden_servicio_asignacion_usuario.usua_id)
                              
                  INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
                  INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = emplazamiento.clas_id)
                  INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
                  INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
                  INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
                  INNER JOIN region ON (region.regi_id = provincia.regi_id)
                                        
                  WHERE presupuesto.pres_id IN ('$presupuestos')
                  ORDER BY orden_servicio.orse_id,presupuesto_item.prit_id");


/*
    $res = $db->ExecuteQuery("SELECT
                    os_consolidado.orse_id AS 'Num. OS',
                    orse_tipo AS 'Tipo OS',
                    espe_nombre AS 'Especialidad OS',
                    sube_nombre AS 'Alarma OS',
                    empr_nombre AS 'EECC OS',
                    usua_creador AS 'Usuario creador OS',
                    orse_fecha_creacion AS 'Fecha creacion OS',
                    orse_fecha_solicitud AS 'Fecha solicitud OS',
                    usua_jefe_cuadrilla AS 'Jefe Cuadrilla',
                    REPLACE(REPLACE(orse_descripcion, '\r', ''), '\n', '') AS 'Descripcion OS',
                    orse_estado AS 'Estado proceso OS',
                    tare_fecha_despacho AS 'Fecha ejecucion OS',
                    orse_estado AS 'Estado OS',
                    empl_nombre AS 'Nombre EMP.',
                    empl_direccion AS 'Direccion EMP.',
                    clas_nombre AS 'Clasificacion EMP.',
                    duto_nombre AS 'Dueño de Torre EMP.',
                    requiere_acceso_emp AS 'Requiere Acceso EMP.',
                    permisos_acceso_emp AS 'Permisos de acceso EMP.',
                    regi_nombre AS 'Region EMP.',
                    zona_movistar AS 'Zona Movistar',
                    zona_contrato AS 'ZONA DE CONTRATO CIM',
                    cluster AS 'Cluster',
                    responsable_emp AS 'Responsable EMP.',
                    os_consolidado_sap.lpip_id AS 'Id LPU',
                    os_consolidado_sap.lpgr_nombre AS 'Grupo LPU', 
                    os_consolidado_sap.lpit_nombre AS 'Item LPU',
                    os_consolidado_sap.lpip_sap_opex AS 'SAP OPEX LPU',
                    os_consolidado_sap.lpip_sap_capex AS 'SAP CAPEX LPU',
                    os_consolidado_sap.lpip_precio AS 'Valor LPU',
                    os_consolidado_sap.prit_cantidad AS 'Cantidad LPU',
                    os_consolidado_sap.lpip_precio*os_consolidado_sap.prit_cantidad AS 'Subtotal LPU',
                    os_consolidado.orse_tag AS 'Codigo Incidencia'                     
                                          
                  FROM os_consolidado
                  INNER JOIN os_consolidado_sap ON (os_consolidado.orse_id = os_consolidado_sap.orse_id)                                        
                  #WHERE pres_id IN ('$presupuestos')
                  ORDER BY os_consolidado.orse_id");
*/
    if(!$res['status']){
      throw new Exception($res['error']);
    }
    if(0==$res['rows']){
      throw new Exception("No hay datos para id de presupuesto $pres_id");
    }
    
    /*
    require_once "../libs/PHPExcel/Classes/PHPExcel.php";
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("SIOM")
                   ->setLastModifiedBy("SIOM")
                   ->setTitle("Reporte SAP OS")
                   ->setDescription("Reporte SAP OS");
    $sheet = $objPHPExcel->setActiveSheetIndex(0);

    $EXCEL_STYLES = array(
        'header' => array(
          'font'  => array(
              'bold'  => true,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        ),
        'value' => array(
          'font'  => array(
              'bold'  => false,
              'color' => array('rgb' => '000000'),
              'size'  => 10,
              'name'  => 'Verdana'
          )
        )
    );
    
    //header
    $col = "A";
    $row =  1;
    foreach(array_keys($res['data'][0]) as $field){
      $sheet->setCellValue($col.$row,$field);
      $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

      $sheet->getColumnDimension($col)->setAutoSize( true );
      $col++; 
    }

    //datos
    $row++;
    $total = 0;
    foreach($res['data'] as $data){
      $col = "A";
      foreach($data as $key => $value){
        $sheet->setCellValue($col.$row,$value);
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
        
        if($key=="subtotal"){
          $total += $value;
        }
        $col++; 
      }
      $row++;
    }

    $filename = "reporte_sap_os.xls";
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
    header ('Pragma: public'); 

    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    $objWriter->save('php://output');
    exit;
    */
    //csv
    $delimiter = ";";
    $filename = "reporte_sap_".date("dmY").".csv";
    header('Content-Type: application/csv; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
    header ('Pragma: public');

    $f = fopen('php://output', 'w');
	fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
        fputcsv($f, $data, $delimiter);
    }
    exit;
  }
  catch (Exception $e) {
    echo "Error al generar excel: ".$e->getMessage();
  }
});


Flight::route('POST /core/repo/contrato/@cont_id:[0-9]+/insp/resumen/resumen', function($cont_id){
    try{  
      
      $db = new MySQL_Database();
      $res = Flight::ObtenerResumenInspeccionesResumen($db,$cont_id,$_POST);
      if(!$res['status']){
        throw new Exception($res['error']);
      }
      if(0==$res['rows']){
        throw new Exception("No hay datos para resumen");
      }
    
      require_once "../libs/PHPExcel/Classes/PHPExcel.php";
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getProperties()->setCreator("SIOM")
                     ->setLastModifiedBy("SIOM")
                     ->setTitle("Reporte resumen de Inspecciones")
                     ->setDescription("Resumen de Inspecciones");
      $sheet = $objPHPExcel->setActiveSheetIndex(0);

      $EXCEL_STYLES = array(
          'header' => array(
            'font'  => array(
                'bold'  => true,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Verdana'
            )
          ),
          'value' => array(
            'font'  => array(
                'bold'  => false,
                'color' => array('rgb' => '000000'),
                'size'  => 10,
                'name'  => 'Verdana'
            )
          )
      );
      
      //header
      $col = "A";
      $row =  1;
      foreach(array_keys($res['data'][0]) as $field){
        $sheet->setCellValue($col.$row,$field);
        $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

        $sheet->getColumnDimension($col)->setAutoSize( true );
        $col++; 
      }

      //datos
      $row++;
      foreach($res['data'] as $data){
        $col = "A";
        foreach($data as $key => $value){
          $sheet->setCellValue($col.$row,$value);
          $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
          $col++; 
        }
        $row++;
      }
    /*    $filename = "resumen_inspecciones.xls";
         header('Content-Type: application/vnd.ms-excel');
         header('Content-Disposition: attachment; filename="'.$filename.'"');
         header('Cache-Control: max-age=0');
         header('Cache-Control: max-age=1');
         header('Set-Cookie: fileDownload=true; path=/');
         header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
         header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
         header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
         header ('Pragma: public'); 

         $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
         $objWriter->save('php://output');
         exit;
       }
       catch (Exception $e) {
         echo "Error al generar excel: ".$e->getMessage();
    }*/

    $delimiter = ";";
    $filename = "resumen_inspecciones.csv";
    header('Content-Type: application; charset=ISO-8859-1');
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Set-Cookie: fileDownload=true; path=/');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
    header ('Pragma: public');

    $f = fopen('php://output', 'w');
    fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
    fputcsv($f, array_keys($res['data'][0]), $delimiter);

    foreach ($res['data'] as $data) {
      fputcsv($f, $data, $delimiter);
    }
    
    exit;
  } catch (Exception $e) {
    echo "Error al generar excel: ".$e->getMessage();
  }
});

Flight::route('POST /core/repo/contrato/@cont_id:[0-9]+/insp/resumen/detalle', function($cont_id){
  try{  
		$db = new MySQL_Database();
		$res = Flight::ObtenerResumenInspeccionesDetalle($db,$cont_id,$_POST);
		if(!$res['status']){
		  throw new Exception($res['error']);
		}
		if(0==$res['rows']){
		  throw new Exception("No hay datos para resumen");
		}
	  
		require_once "../libs/PHPExcel/Classes/PHPExcel.php";
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("SIOM")
					   ->setLastModifiedBy("SIOM")
					   ->setTitle("Reporte resumen de Inspecciones")
					   ->setDescription("Resumen de Inspecciones");
		$sheet = $objPHPExcel->setActiveSheetIndex(0);

		$EXCEL_STYLES = array(
			'header' => array(
			  'font'  => array(
				  'bold'  => true,
				  'color' => array('rgb' => '000000'),
				  'size'  => 10,
				  'name'  => 'Verdana'
			  )
			),
			'value' => array(
			  'font'  => array(
				  'bold'  => false,
				  'color' => array('rgb' => '000000'),
				  'size'  => 10,
				  'name'  => 'Verdana'
			  )
			)
		);
		
		//header
		$col = "A";
		$row =  1;
		foreach(array_keys($res['data'][0]) as $field){
		  $sheet->setCellValue($col.$row,$field);
		  $sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

		  $sheet->getColumnDimension($col)->setAutoSize( true );
		  $col++; 
		}

		//datos
		$row++;
		foreach($res['data'] as $data){
		  $col = "A";
		  foreach($data as $key => $value){
			$sheet->setCellValue($col.$row,$value);
			$sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
			$col++; 
		  }
		  $row++;
		}

    /*
       $filename = "detalle_inspecciones.xls";
       header('Content-Type: application/vnd.ms-excel');
       header('Content-Disposition: attachment; filename="'.$filename.'"');
       header('Cache-Control: max-age=0');
       header('Cache-Control: max-age=1');
       header('Set-Cookie: fileDownload=true; path=/');
       header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
       header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
       header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
       header ('Pragma: public'); 

       $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
       $objWriter->save('php://output');
       exit;
     }
     catch (Exception $e) {
       echo "Error al generar excel: ".$e->getMessage();
     }*/

		$delimiter = ";";
		$filename = "detalle_inspecciones.csv";
		header('Content-Type: application; charset=ISO-8859-1');
		header('Content-Disposition: attachment; filename="'.$filename.'";');
		header('Cache-Control: max-age=0');
		header('Cache-Control: max-age=1');
		header('Set-Cookie: fileDownload=true; path=/');
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');      // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate');       // HTTP/1.1
		header ('Pragma: public');

		$f = fopen('php://output', 'w');
		fprintf ($f, chr (0xEF) .chr (0xBB) .chr (0xBF)); 
		fputcsv($f, array_keys($res['data'][0]), $delimiter);

		foreach ($res['data'] as $data) {
		fputcsv($f, $data, $delimiter);
		}

		exit;
	}
	catch (Exception $e) {
		echo "Error al generar excel: ".$e->getMessage();
	}
});

?>

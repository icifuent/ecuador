<?php

    Flight::route('GET|POST /indisponibilidad/bandeja/filtros', function(){
        $out = array();
        $out['status'] = 1;
        $dbo = new MySQL_Database();

        //CLUSTER
        /*
         *  Se debe de recuperar el contrato correspondiente
        */
        $res = $dbo->ExecuteQuery(" SELECT zona_id, zona_nombre 
                                    FROM zona 
                                    WHERE cont_id=1 
                                        AND zona_estado = 'ACTIVO' 
                                        AND zona_tipo='CLUSTER'");
        if (0 == $res['status'] ) {
            Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
        }
        $out['clusters'] = $res['data'];

        /*PRIORIDAD*/
        $res = $dbo->ExecuteQuery("SELECT indi_prioridad FROM indisponibilidad");
        if (0 == $res['status'] ) {
            Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
        }
        $out['prioridad'] = $res['data'];

        Flight::json($out);
    });


    Flight::route('GET|POST /indisponibilidad/list(/@page:[0-9]+)', function($page){
        $results_by_page = Flight::get('results_by_page');

        $out['status'] = 1;
        $dbo = new MySQL_Database();
        $res = $dbo->ExecuteQuery("SELECT   indi_id
                                            ,indi_cluster_id
                                            ,indi_cluster_nombre
                                            ,indi_incidencia
                                            ,indi_resumen
                                            ,indi_estado
                                            ,indi_impacto
                                            ,indi_urgencia
                                            ,indi_prioridad
                                            ,indi_importancia
                                            ,indi_motivo_estado
                                            ,indi_fuente_reportada
                                            ,indi_fecha_notificacion
                                            ,indi_fecha_inicio_real
                                            ,indi_fecha_estado_resolucion
                                            ,indi_organizacion_soporte
                                            ,indi_grupo_asignado
                                FROM indisponibilidad
        " . ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page)) );
        if (0 == $res['status'] ) {
            Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
        }
        $out['indisponibilidades'] = $res['data'];

        $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
        if ($res_count['status'] == 0) {
            Flight::json(array("status" => 0, "error" => $res_count['error']));
        }
        $out['total'] = intval($res_count['data'][0]['total']);
        $out['paginas'] = ceil($out['total']/$results_by_page);
        $out['status'] = 1;

        Flight::json($out);
    });


    Flight::route('GET|POST /indisponibilidad/detalle/@indi_id:[0-9]+', function($indi_id){

        $dbo = new MySQL_Database();
        $res = Flight::ObtenerDetalleIndisponibilidad($dbo,$indi_id);
        if( 0 == $res['status'] ){
            Flight::json(array("status"=>0, "No se pudo obtener el detalle de indisponibilidad"=>$res['error']));
            return;
        }
        $out['indisponibilidad'] = $res['data'][0];

        $res = Flight::ObtenerDetalleBitacora($dbo,$indi_id);
        if( 0 == $res['status'] ){
            Flight::json(array("status"=>0, "No se pudo obtener el detalle de la bitacora "=>$res['error']));
            return;
        }
        $out['bitacora'] = $res['data'];
        $res = Flight::ObtenerDetalleRelacion($dbo,$indi_id);
        if( 0 == $res['status'] ){
            Flight::json(array("status"=>0, "No se pudo obtener el detalle de las relaciones de  indisponibilidad"=>$res['error']));
            return;
        }
        $out['relacion'] = $res['data'];
        $out['status'] = 1;
        Flight::json($out);
    });

    Flight::map('ObtenerDetalleIndisponibilidad', function($db,$indi_id){
        $query = "  SELECT      indi_id
                                ,indi_cluster_id
                                ,indi_cluster_nombre
                                ,indi_incidencia
                                ,indi_resumen
                                ,indi_estado
                                ,indi_impacto
                                ,indi_urgencia
                                ,indi_prioridad
                                ,indi_importancia
                                ,indi_motivo_estado
                                ,indi_fuente_reportada
                                ,indi_fecha_notificacion
                                ,indi_fecha_inicio_real
                                ,indi_fecha_estado_resolucion
                                ,indi_organizacion_soporte
                                ,indi_grupo_asignado
                                ,indi_fono_cliente
                                ,indi_fecha_creacion_indisponibilidad
                                ,espe_id
                                ,sube_id
                                ,indi_id_indisponibilidad
                                ,cont_id
                                ,indi_tipo_servicio
                    FROM indisponibilidad
                    WHERE indi_id=$indi_id ";
        $res = $db->ExecuteQuery($query);
        if (0 == $res['status'] ){
            return $res;
        }

        return $res;
    });

    Flight::map('ObtenerDetalleBitacora', function($db,$indi_id){
    $query = "  SELECT      inbi_tipo
                            ,inbi_resumen
                            ,inbi_a
                            ,inbi_fecha
                            ,inbi_remitente
                            ,inbi_remitente_sotie
                    FROM indisponibilidad_bitacora
                    WHERE inbi_id_relacionado=$indi_id ";
        $res = $db->ExecuteQuery($query);
        if (0 == $res['status'] ){
            return $res;
        }
        return $res;
    });


    Flight::map('ObtenerDetalleRelacion', function($db,$indi_id){
    $query = "  SELECT      inre_resumen
                            ,inre_prioridad
                            ,inre_descripcion
                            ,inre_item
                            ,inre_direccion_emplazamiento
                            ,inre_fecha_inicio
                            ,inre_fecha_fin
                            ,inre_localidad
                            ,inre_tipo_relacion
                            ,inre_tipo_peticion
                            ,inre_estado
                    FROM indisponibilidad_relacion
                    WHERE inre_id_relacionado=$indi_id ";
        $res = $db->ExecuteQuery($query);
        if (0 == $res['status'] ){
            return $res;
        }
        return $res;
    });

    Flight::map('ObtenerDetalleIndisponibilidadSOAP', function(){
        /*

        <soapenv:Header>
          <urn:AuthenticationInfo>
             <urn:userName>SIOMDES</urn:userName>
             <urn:password>SIOMDES</urn:password>
          </urn:AuthenticationInfo>
       </soapenv:Header>
       <soapenv:Body>
          <urn:HelpDesk_QueryList_Service>
             <urn:Qualification>'Incident Number' != $NULL$ AND 'Status' = "Pending"</urn:Qualification>
             <urn:startRecord>1</urn:startRecord>
             <urn:maxLimit>20</urn:maxLimit>
          </urn:HelpDesk_QueryList_Service>
       </soapenv:Body>


        */
        $wsdl= "";
        $soapClient = new SoapClient("http://service.mydomain.com/Services.asmx?wsdl",array( "trace" => 1 ));
        $AuthenticationInfo= array (
          'userName' => 'SIOMDES',
          'password' => 'SIOMDES'
        );
        $header = array(
            'AuthenticationInfo' => $AuthenticationInfo
        );
        $HelpDesk_QueryList_Service = array (
          'Qualification' => '',
          'startRecord' => '',
          'maxLimit' => 'something'
        );
        $body = array (
            'HelpDesk_QueryList_Service' => $HelpDesk_QueryList_Service
        );
        $envelope = array (
            'Header' => $header,
            'Body' => $body);
        $info = $soapClient->__call("Envelopment", array($envelope));
        echo "Request :\n".htmlspecialchars($soapClient->__getLastRequest()) ."\n";
    /*-
        $wsdl   = "https://<your_web_service_url>?wsdl";
        $client = new SoapClient($wsdl, array('trace'=>1));  // The trace param will show you errors stack

        // web service input params
        $request_param = array(
            "param1" => $value1,
            "param2" => $value2,
            "param3" => $value3,
            "param4" => $value4
        );

        try
        {
            $responce_param = $client->webservice_methode_name($request_param);
           //$responce_param =  $client->call("webservice_methode_name", $request_param); // Alternative way to call soap method
        } catch (Exception $e) { 
            echo "<h2>Exception Error!</h2>"; 
            echo $e->getMessage(); 
        }
    */
    });

?>
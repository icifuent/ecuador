<?php

    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.
    /*CACHE*/
    clearstatcache();
    clearstatcache("siom.js");

    /*
    foreach ($_POST as $key => $value) {
        echo '<p><strong>' . $key.':</strong> '.$value.'</p>';
    }*/
    function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) //INTERNET
        {
          $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  //PROXY
        {
          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
          $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /*CONFIGURACION RECAPTCHA DE GOOGLE*/
    if (isset($_POST['btnSubmitLogin'])) {
        $secret     = '6LfjymsUAAAAAPA4f8WIq1265csVkikMxA2O8XvO';
        $response = $_POST['g-recaptcha-response'];
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $url = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");
        $result = json_decode($url, TRUE);

    }
    /*Chequea login y devuelve  secciones disponibles*/
    Flight::route('POST /login', function(){
        global $DEF_CONFIG;
        global $PERFILES;
        global $BASEDIR;

        /*GMT*/
        $gmt_date   = gmdate('Y-m-d h:i:s');
        $ip         = getRealIpAddr();
        $db = new MySQL_Database();
        $res        = $db->ExecuteQuery("SELECT 1 FROM dual"); 

        require $BASEDIR."../libs/password.php";

        $user = mysql_real_escape_string($_POST['user']);
        $pass = mysql_real_escape_string($_POST['pass']);

       /* error_log("USUARIO: ".$user);
        error_log("FECHA: ".$gmt_date);
        error_log("IP: ".$ip);*/
        /*$db = NULL;*/

        if('' == $user || '' == $pass ){
            $out['error']   =   "Usuario inválido";
            Flight::json($out);
        }

        $db->startTransaction();
        /* $hashedPass= password_hash($pass, PASSWORD_DEFAULT);
        VALIDACION DE LOGIN CON ENCRIPTACION HASH */
        $res = $db->ExecuteQuery("SELECT
                                        u.usua_id
                                        ,u.usua_nombre
                                        ,empresa.empr_id
                                        ,empresa.empr_nombre 
                                        ,u.usua_crypt_pass 
                                        ,u.usua_fecha_bloqueo
                                        ,u.usua_bloqueado
                                        ,rcu.cont_id
                                        ,u.usua_estado_coneccion
                                        ,NOW() AS fecha_actual
                                        FROM usuario u
                                        INNER JOIN empresa
                                        ON (empresa.empr_id = u.empr_id) 
                                        INNER JOIN rel_contrato_usuario  rcu 
                                        ON (rcu.usua_id = u.usua_id)
                                        WHERE usua_login='$user'
                                        AND usua_acceso_web='1'
                                        AND usua_estado='ACTIVO'
                                        #AND usua_estado_coneccion = 'DESCONECTADO'
                                        ");
                                        #AND usua_crypt_pass='$hashedPass'
                                        /*AND usua_crypt_pass='$hashedPass'*/ 
        if(0 == $res['status']){
            $db->rollback();
            Flight::Log($res['error']);
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
        }  
        $db->commit();
        if( 0 == $res['rows']){

            $db->startTransaction();
        
            $res_ins = $db->ExecuteQuery("INSERT INTO login 
                                            SET logi_usr = '$user',
                                                logi_fecha_gmt = '$gmt_date',
                                                logi_ip = '$ip'
                                        ");
            if(0 == $res_ins['status']){
                Flight::Log($res_ins['error']);
                Flight::json(array("status"=>false,"error"=>$res_ins['error']));
                return;
            }  
            $db->commit();
           
            $res = array("status"=>0,"error"=>"Usuario inválido");
            Flight::json($res);

            return;
        }else{
          
            $db->startTransaction();
        
            $res_ins = $db->ExecuteQuery("INSERT INTO login 
                                            SET logi_usr = '$user',
                                                logi_fecha_gmt = '$gmt_date',
                                                logi_ip = '$ip',
                                                logi_status = 'OK'");
             if(0 == $res_ins['status']){
                Flight::Log($res_ins['error']);
                Flight::json(array("status"=>false,"error"=>$res_ins['error']));
                return;
            }  

            $db->commit();
      
        }

       /* $usua_estado_coneccion  =   $res['data'][0]['usua_estado_coneccion'];
        if('CONECTADO' == $usua_estado_coneccion)
        {
            $res = array("status"=>0,"error"=>"Usuario se encuentra conectado");
            Flight::json($res);
            return;
        }*/

        $usua_fecha_bloqueo =$res['data'][0]['usua_fecha_bloqueo'];
        $usua_bloqueado     =$res['data'][0]['usua_bloqueado'];
        $usua_id    =$res['data'][0]['usua_id'];
        $cont_id    =$res['data'][0]['cont_id'];
        $crypt      =$res['data'][0]['usua_crypt_pass'];

        $query = $db->ExecuteQuery("    SELECT paco_valor 
                                        FROM parametros_contrato 
                                        WHERE cont_id = $cont_id
                                        AND paco_modu_nombre = 'BLOQUEO_SEGURIDAD_MEDIO';
        ");
        if(0 == $query['status']){
            Flight::Log($query['error']);
            Flight::json(array("status"=>false,"error"=>$query['error']));
            return;
        }  

         if(isset($query['data'][0]['paco_valor']))
        {
            $BLOQUEO_SEGURIDAD_MEDIO = (int)$query['data'][0]['paco_valor'];
        }else 
        {
            $BLOQUEO_SEGURIDAD_MEDIO = 60;
        }
      

        /*  if(NULL == $BLOQUEO_SEGURIDAD_MEDIO &&  0 == $BLOQUEO_SEGURIDAD_MEDIO){
            $BLOQUEO_SEGURIDAD_MEDIO = 60;
        }*/

        $query = $db->ExecuteQuery("    SELECT paco_valor 
                                        FROM parametros_contrato 
                                        WHERE cont_id = $cont_id
                                        AND paco_modu_nombre = 'BLOQUEO_SEGURIDAD_ALTO';
        ");
        if(0 == $query['status']){
            Flight::Log($query['error']);
            Flight::json(array("status"=>false,"error"=>$query['error']));
            return;
        }  
        
        if(isset($query['data'][0]['paco_valor']))
        {
            $BLOQUEO_SEGURIDAD_ALTO = (int)$query['data'][0]['paco_valor'];
        }else 
        {
            $BLOQUEO_SEGURIDAD_ALTO = 1800;
        }
        /* if( NULL == $BLOQUEO_SEGURIDAD_ALTO &&  0 == $BLOQUEO_SEGURIDAD_ALTO){
            $BLOQUEO_SEGURIDAD_ALTO = 1800;
        }*/

        $fecha_actual           =   $res['data'][0]['fecha_actual'];
        $fecha_desbloqueo_medio =   strtotime($fecha_actual) + $BLOQUEO_SEGURIDAD_MEDIO;
        $fecha_desbloqueo_medio =   date("y-m-d H:i:s",$fecha_desbloqueo_medio);
        
        if (NULL != $usua_fecha_bloqueo && "" != $usua_fecha_bloqueo)
        {
              
            if($fecha_actual < $usua_fecha_bloqueo )
            {
                $segundos_a_esperar= strtotime($usua_fecha_bloqueo) -  strtotime($fecha_actual) ; 
                $res = array("status"=>0,"error"=>"Usuario bloqueado debe esperar ".$segundos_a_esperar." segundos");
                Flight::json($res);

            }
            
        }
        if(false == password_verify($pass,$crypt) ){

            if(2 >= $usua_bloqueado)
            {
                $db->startTransaction();
                $res = $db->ExecuteQuery(" UPDATE usuario
                                            SET usua_bloqueado = $usua_bloqueado + 1 
                                            ,usua_fecha_bloqueo = '$fecha_desbloqueo_medio'
                                            WHERE usua_id =  $usua_id
                ");

                if(0 == $res['status']){
                    $db->rollback();
                    $db->Disconnect();
                    Flight::Log($res['error']);
                    Flight::json(array("status"=>false,"error"=>$res['error']));
                    return;
                }  
                $db->commit();
            
                $res = array("status"=>0,"error"=>"Usuario bloqueado tiempo de espera ".$BLOQUEO_SEGURIDAD_MEDIO." segundos ");
            }elseif (3 == $usua_bloqueado) {

                $fecha_desbloqueo_alto = strtotime($fecha_actual) + $BLOQUEO_SEGURIDAD_ALTO;
                $fecha_desbloqueo_alto = date("y-m-d H:i:s",$fecha_desbloqueo_alto);
                $db->startTransaction();
                $res = $db->ExecuteQuery(" UPDATE usuario
                                            SET usua_bloqueado = 0
                                            ,usua_fecha_bloqueo = '$fecha_desbloqueo_alto'
                                            WHERE usua_id =  $usua_id
                ");
                if(0 == $res['status']){
                    $db->rollback();
                    Flight::Log($res['error']);
                    Flight::json(array("status"=>false,"error"=>$res['error']));
                    return;
                }  
                $db->commit();
              
                $res = array("status"=>0,"error"=>"Usuario bloqueado tiempo de espera ".$BLOQUEO_SEGURIDAD_ALTO." segundos ");
            }
        }else{
            $db->startTransaction();
            $query = $db->ExecuteQuery(" UPDATE usuario
                                        SET usua_bloqueado = 0
                                        ,usua_fecha_bloqueo = null
                                        WHERE usua_id =  $usua_id
                               
            ");
            if(0 == $query['status']){
                $db->rollback();
                $db->Disconnect();
                Flight::Log($query['error']);
                Flight::json(array("status"=>false,"error"=>$query['error']));
                return;
            }  
            $db->commit();
        
        }

            if($res['status']) {
                if(0<$res['rows']) {
                $usuario = $res['data'][0];
                #$usua_id = $usuario['usua_id'];
                #$crypt = $usuario['usua_crypt_pass'];
                if(password_verify($pass, $crypt)) {
                    global $res, $vall;

                    $res = $db->ExecuteQuery("SELECT
                                        c.cont_id,
                                        c.cont_nombre
                                        FROM contrato c
                                        INNER JOIN rel_contrato_usuario rel ON (rel.cont_id=c.cont_id AND rel.usua_id=$usua_id
                                        AND rel.recu_estado='ACTIVO')");

                    if(0 == $res['status']){
                        Flight::Log($res['error']);
                        Flight::json(array("status"=>false,"error"=>$res['error']));
                        return;
                    }
                    $contratos = $res['data'];


                    $res = $db->ExecuteQuery("SELECT
                                                perf_nombre
                                            FROM
                                                rel_usuario_perfil
                                            INNER JOIN perfil ON (rel_usuario_perfil.perf_id=perfil.perf_id)
                                            WHERE usua_id=$usua_id");
                    if(0 == $res['status']){
                        Flight::Log($res['error']);
                        Flight::json(array("status"=>false,"error"=>$res['error']));
                        return;
                    }
                    $usuario['usua_cargo'] = array();
                    $perfil = array();
                    $estado = "CONECTADO";
                    $db->startTransaction();
                    $query = $db->ExecuteQuery("    UPDATE usuario 
                                                    SET usua_estado_coneccion = '".$estado."'
                                                    WHERE usua_id = $usua_id
                                       
                    ");
                    if(0 == $query['status']){
                        $db->rollback();
                        $db->Disconnect();
                        Flight::Log($query['error']);
                        Flight::json(array("status"=>false,"error"=>$query['error']));
                        return;
                    }  
                    $db->commit();    

                    foreach($res['data'] as $row){
                        if(isset($PERFILES[$row['perf_nombre']])){
                            $perfil = array_merge($perfil,$PERFILES[$row['perf_nombre']]);
                        }
                        array_push($usuario['usua_cargo'],$row['perf_nombre']);
                    }
                    $perfil = array_values(array_unique($perfil));
                    sort($perfil);
                    /*session_start();*/
                    $_SESSION['user_id']    = $usuario['usua_id'];
                    $_SESSION['user_name']  = $usuario['usua_nombre'];
                    $_SESSION['usua_cargo'] = $usuario['usua_cargo'];
                    $_SESSION['empr_id']    = $usuario['empr_id'];
                    $_SESSION['empr_nombre']  = $usuario['empr_nombre'];
                    $_SESSION['perfil']     = $perfil;
                    $_SESSION['logged']     = true;
                    $_SESSION['config']     = $DEF_CONFIG;
                    $_SESSION['contracts']  = $contratos;
                    $_SESSION['sections']   = array();
                    $_SESSION['cont_id']    = $contratos[0]['cont_id']; //TODO
                    $_SESSION['LAST_ACTIVITY'] = time();

                    if(1==$contratos[0]['cont_id']){
                        array_push($_SESSION['sections'],array("link"=>"#/dashboard","section"=>"Dashboard","sections"=>array(
                        array('link' =>"#/dashboard/os" ,"section"=>"Orden de servicio" ),
                        array("link"=>"#/dashboard/mnt" ,"section"=>"Mantenimiento")
                        )));
                    }else{
                        array_push($_SESSION['sections'],array("link"=>"#/dashboard","section"=>"Dashboard","sections"=>array(
                                array("link"=>"#/dashboard/mnt" ,"section"=>"Mantenimiento"),
                                array('link' =>"#/dashboard/os" ,"section"=>"Orden de servicio" )
                        )));
                    }

                    array_push($_SESSION['sections'],array("link"=>"#/tracking","section"=>"Tracking"));

                    array_push($_SESSION['sections'],array("link"=>"#/emplazamientos","section"=>"Emplazamientos"));

                    array_push($_SESSION['sections'],array("link"=>"#/mnt","section"=>"Mantenimiento"));

                    array_push($_SESSION['sections'],array("link"=>"#/os","section"=>"Orden de servicio"));

                    array_push($_SESSION['sections'],array("link"=>"#/insp","section"=>"Inspecciones"));

                    array_push($_SESSION['sections'],array("link"=>"#/indisponibilidad","section"=>"Indisponibilidad"));

                    array_push($_SESSION['sections'],array("link"=>"#/defaMovil","section"=>"Denuncia de falla"));

                    array_push($_SESSION['sections'],array("link"=>"#/qr","section"=>"Códigos QR"));

                    array_push($_SESSION['sections'],array("link"=>"#/inve","section"=>"Inventario"));

                    array_push($_SESSION['sections'],array("link"=>"#/docs","section"=>"Documentación"));

                    array_push($_SESSION['sections'],array("link"=>"#/sla","section"=>"SLA","sections"=>array(
                        array('link' =>"#/sla/tiempos" ,"section"=>"Tiempos de Respuesta")
                        ,array('link' =>"#/sla/fallas","section"=>"Tasa de Fallas")
                        ,array('link' =>"#/sla/reiteradas","section"=>"Tasa de Fallas Reiteradas")
                        ,array('link' =>"#/sla/disponibilidad" ,"section"=>"Disponibilidad")
                        ,array('link' =>"#/sla/ejecucion" ,"section"=>"Cumplimiento Mantenimientos")
                        ,array('link' =>"#/sla/cronograma" ,"section"=>"Cumplimiento Planificacion")
                        ,array('link' =>"#/sla/calidad" ,"section"=>"Calidad Mantenimientos")
                        ,array('link' =>"#/sla/calidadycronograma" ,"section"=>"Calidad y Cronograma")
                        ,array('link' =>"#/sla/evaluacion" ,"section"=>"Evaluacion")
                    )));

                    array_push($_SESSION['sections'],array("link"=>"#/core","section"=>"Core","sections"=>array(
                        array('link' =>"#/core/contrato" ,"section"=>"Contrato" ),
                        array("link"=>"#/core/emplazamiento_core","section"=>"Emplazamientos"),
                        array('link' =>"#/core/empresa" ,"section"=>"Empresas" ),
                        array('link' =>"#/core/curso" ,"section"=>"Cursos" ),
                        array("link"=>"#/core/usuario","section"=>"Usuarios"),
                        array("link"=>"#/core/formulario","section"=>"Formulario"),
                        array("link"=>"#/core/periodicidad","section"=>"Periodicidad")
                    )));


                    $res = array("status"=>true,
                    "user_id"=>$_SESSION['user_id'],
                    "user_name"=>$_SESSION['user_name'],
                    "user_level"=>$_SESSION['usua_cargo'],
                    "profile"=>$_SESSION['perfil'],
                    "client"=>$_SESSION['empr_nombre'],
                    "config"=>$_SESSION['config'],
                    "sections"=>$_SESSION['sections'],
                    "contracts"=>$_SESSION['contracts'],
                    "contrato"=>$_SESSION['cont_id']);

                /* ----*/
                } else {
                    $res = array("status"=>0,"error"=>"Usuario inv�lido 1");
                }

            } else {
                $res = array("status"=>0,"error"=>"Usuario inv�lido");
               
            }
        }
        Flight::json($res);
    });

    /*======================Login=====================*/
    Flight::route('GET /islogged', function(){
        $user_id=$_SESSION['user_id'];
        session_id($user_id);
        if(isset($user_id)){
            Flight::json(array("status"=>true,
                                    "logged"=>true,
                                    "user_id"=>$_SESSION['user_id'],
                                    "user_name"=>$_SESSION['user_name'],
                                    "user_level"=>$_SESSION['usua_cargo'],
                                    "profile"=>$_SESSION['perfil'],
                                    "client"=>$_SESSION['empr_nombre'],
                                    "config"=>$_SESSION['config'],
                                    "sections"=>$_SESSION['sections'],
                                    "contracts"=>$_SESSION['contracts'],
                                    "contrato"=>$_SESSION['cont_id']));
        } else {
            Flight::json(array("status"=>true,"logged"=>0));
        }

    });

    Flight::route('POST /logout', function(){
        /*session_start();*/
        $db     = new MySQL_Database();
        $db->startTransaction();
        $usua_id= $_SESSION['user_id'];
        $estado = "DESCONECTADO"; 
        if(NULL==$usua_id || 0 == $usua_id){
            $res = array("status"=>0,"error"=>"Favor limpiar temporales, si el problema persiste contactar con mesa de ayuda");
            Flight::json($res);
            return;
        }

        $vall   = $db->ExecuteQuery(" UPDATE usuario 
                                        SET usua_estado_coneccion= '".$estado."'
                                        WHERE usua_id = $usua_id;                        
        "); 
        if(0 == $vall['status']){
            $db->rollback();
            $db->Disconnect(); 
            Flight::Log($vall['error']);
            Flight::json(array("status"=>false,"error"=>$vall['error']));
            return;
        }  
        $db->commit();
        $db->Disconnect();
        $_SESSION = array();
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 1800,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }
        session_unset();
        session_destroy();

        Flight::json(array("status"=>1));
    });

?>

<?php
	class Cache{	
		private $memcache = null;
				 
		function Cache(){
			$HOST="localhost";
			$PORT=11211;
	
			$this->memcache = new Memcache;
			if(!$this->memcache->pconnect($HOST,$PORT)){
				echo "Error al conectar a memcache";
			}
		}
		
		function get($key){
			if($this->memcache){
				return $this->memcache->get($key);
			}
			return FALSE;
		}
		
		function set($key,$value){
			if($this->memcache){
				return $this->memcache->set($key,$value);
			}
			return FALSE;
		}
	}
?>
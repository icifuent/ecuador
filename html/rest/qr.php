<?php
/*
Flight::set('PAIS_ID', 1);
Flight::set('QR_MODU_ID', 2);
Flight::set('PERI_ID_A_SOLICITUD',14);
Flight::set('FORM_INGRESO',1);
Flight::set('FORM_SALIDA',4);*/

//____________________________________________________________
//Helpers
/*
Flight::map('ObtenerDetalleCodigoQR', function($db,$leco_id){
    $query = "  SELECT
					leco.usua_id,
					leco.leco_latitud,
					leco.leco_longitud,
					leco.leco_fecha,
					leco.leco_imei,
					leco.leco_tipo,
                    leco1.leco_id,
					leco1.rlco_codigo_descifrado,
					leco1.rlco_orden                   
                FROM
                
				lectura_codigo leco  inner join
				registro_lectura_codigo leco1 on leco1.leco_id = leco.leco_id
               WHERE
				leco1.leco_id = ".$leco_id."
                ;";
				
    $res =  $db->ExecuteQuery($query);
    if ($res['status'] == 0){
        return $res;
    }
    Flight::json($out);
});
	*/

//____________________________________________________________
//Bandeja

Flight::route('GET|POST /qr/bandeja/filtros', function($usuario){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();


/*    
	$filtros = 
	Flight::filtersToWhereString(array("",""))*/
/*	
	$usua_id = mysql_real_escape_string($_GET['ustr_usuario']); 
    $fecha_inicio = mysql_real_escape_string($_GET['leco_fecha_inicio']); 
    $fecha_fin = mysql_real_escape_string($_GET['leco_fecha_fin']); */
	
	/*$results_by_page = Flight::get('results_by_page');
    $filtros_qr = array_merge($_GET,$_POST);
    $filtros = "";
	
	$out = array();
    $out['pagina'] = $page;
    $out['filtros'] = $filtros_qr;
	
	 foreach ($filtros_qr as $key => $value) {
        if( is_null($value) ){
            unset($filtros_qr[$key]);
        }
        if( !is_array($value) && strlen(trim($value.""))==0 ){
            unset($filtros_qr[$key]);
        }
    }
	
	 if( isset($filtros_qr['usua_nombre']) ){
        $filtros .= " AND usr.usua_nombre=".$filtros_qr['usua_nombre']." ";
        unset( $filtros_qr['usua_nombre'] );
    }*/

    //BUSCAR POR N° QR
    $res = $dbo->ExecuteQuery(" SELECT 
								        leco_id,
								        leco_fecha
								FROM lectura_codigo 
                                ");
	
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['numeroQr'] = $res['data'];

	
	
   
	
    //USUARIO	
    $res = $dbo->ExecuteQuery("SELECT DISTINCT 
                                lectura_codigo.usua_id, 
                                usuario.usua_nombre 
                                FROM 
                                usuario 
								inner join lectura_codigo on usuario.usua_id = lectura_codigo.usua_id
                                inner JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id) 
                                inner JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))
								ORDER BY usuario.usua_nombre");
								
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['usua_leco'] = $res['data'];
								
    Flight::json($out);
});



Flight::route('GET|POST /qr/list(/@page:[0-9]+)', function($page){
    
    $filtro_usuario = array_merge($_GET,$_POST);   
    $filtros = "TRUE ";

    if( isset($filtro_usuario['leco_id']) && $filtro_usuario['leco_id'] !=""){
        $filtros .= " AND lec.leco_id=".$filtro_usuario['leco_id']." ";
        unset( $filtro_usuario['leco_id'] );
    }

	if( isset($filtro_usuario['usua_leco']) && $filtro_usuario['usua_leco'] !="" && $filtro_usuario['usua_leco'] !=0){
        $filtros .= " AND lec.usua_id=".$filtro_usuario['usua_leco']." ";
        unset( $filtro_usuario['usua_leco'] );
    }
	
	
	$results_by_page = Flight::get('results_by_page');
	
    echo "HERE";
    $out['status'] = 1;
    $dbo = new MySQL_Database();
	
	
    $res = $dbo->ExecuteQuery("SELECT 
									    SQL_CALC_FOUND_ROWS
										lec.leco_id
										,lec.usua_id
										,lec.leco_latitud
										,lec.leco_longitud
										,lec.leco_fecha
										,usr.usua_nombre
									FROM lectura_codigo lec
									inner join usuario usr on usr.usua_id = lec.usua_id
									WHERE $filtros
	" . ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page)) );
	
	
	   
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['qrs'] = $res['data'];

	$res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    $out['paginas'] = ceil($out['total']/$results_by_page);
    $out['status'] = 1;

    Flight::json($out);
});

Flight::route('GET|POST /qr/detalle/@leco_id:[0-9]+', function($leco_id/*,$fecha*/){

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleQr($dbo,$leco_id/*,$fecha*/);
	
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
		 return;
    }  
	
	$res = Flight::ObtenerDetalleEm($dbo,$leco_latitud,$leco_longitud);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
		 return;
    } 

    Flight::json($out);
});
		
	Flight::map('ObtenerDetalleQr', function($db,$leco_id/*,$fecha*/){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();
     $query ="SELECT         rlc.leco_id,
							 lec.leco_fecha,
							 lec.usua_id,
							 rlc.rlco_orden + 1 as rlco_orden,
							 lec.leco_latitud,
							 lec.leco_longitud,
							 usr.usua_nombre,
							 rlc.rlco_codigo_descifrado from registro_lectura_codigo rlc
							 inner join lectura_codigo lec on (lec.leco_id=rlc.leco_id)
							 inner join usuario usr on usr.usua_id = lec.usua_id
							 where lec.leco_id='$leco_id'
";

    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle de los código"));
    $out['detQr'] = $res['data'];
	
	$servicio="http://200.29.32.119/SIOMWS/services/ConsultaParametros?wsdl";
	$parametros=array();
	$parametros['cont_id']=1;
	$client = new SoapClient($servicio,$parametros);
	$result = $client->obtenerEmplazamientoPorContrato($parametros);
	
	$result = obj2array($result);
	
	Flight::json(array("status" => 0, "error" => $result));
	
	$Emplazamientos=$result['resultado']['Emplazamientos'];
	$n=count($Emplazamientos);

	//procesamos el resultado como con cualquier otro array
	for($i=0; $i<$n; $i++){
		$Emplazamiento=$Emplazamientos[$i];
		$id=$Emplazamiento['id'];
	}

	function obj2array($obj) {
	  $out = array();
	  foreach ($obj as $key => $val) {
		switch(true) {
			case is_object($val):
			 $out[$key] = obj2array($val);
			 break;
		  case is_array($val):
			 $out[$key] = obj2array($val);
			 break;
		  default:
			$out[$key] = $val;
		}
	  }
	  return $out;
	}
	
	//$query ="SELECT $result from DUAL";

    //$res = $db->ExecuteQuery($query);
	Flight::json(array("status" => 0, "error" => $result));	
		
	
	//Cabecera QR
   $query1 =("SELECT                         
   
											 lec.leco_id,
											 lec.leco_fecha,
											 lec.usua_id,
											 lec.leco_latitud,
											 lec.leco_longitud,
											 usr.usua_nombre
											 from lectura_codigo lec
											 inner join usuario usr on usr.usua_id = lec.usua_id
											 where lec.leco_id='$leco_id'
											 limit 1
											
							   ");  
		
    $res = $db->ExecuteQuery($query1);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle cabecera qr"));
    $out['cabQr'] = $res['data'];
	
	//query
	 $query3=("SELECT                         
            leco_latitud,
			leco_longitud
			FROM
			lectura_codigo
			WHERE leco_id='$leco_id'
			");  
	
	$res = $db->ExecuteQuery($query3);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle 3"));
	$latitud = $res["data"][0]['leco_latitud'];
	$longitud = $res["data"][0]['leco_longitud'];
    
	
	//Emplazamiento
    $query2 ="SELECT con.cont_nombre,
				     con.cont_id,
	                 emp.empl_nombre,
	                 emp.empl_direccion,
					 emp.empl_observacion,
					 lec.leco_id,
					 emp.empl_latitud,
					 emp.empl_longitud,
					 lec.leco_latitud,
					 lec.leco_longitud,(6371 * ACOS( 
                                SIN(RADIANS(empl_latitud)) * SIN(RADIANS($latitud)) 
                                 + COS(RADIANS(empl_longitud - $longitud)) * COS(RADIANS(empl_latitud)) 
                                 * COS(RADIANS($latitud))
                                )
									   ) AS distance
					FROM emplazamiento emp
					INNER JOIN lectura_codigo lec
					INNER JOIN rel_contrato_emplazamiento rel on (rel.empl_id = emp.empl_id)
					INNER JOIN contrato con on (con.cont_id = rel.cont_id)
					where leco_id=$leco_id
					HAVING distance < 0.350
					ORDER BY distance ASC
					limit 1";
		
    $res = $db->ExecuteQuery($query2);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle emplazamiento"));
    $out['detEmp'] = $res['data'];
	

    Flight::json($out);	
});

 //Emplazamiento del Qr
 /* Flight::map('ObtenerDetalleEm', function($db,$leco_id,$leco_latitud,$leco_longitud){
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();
	$query2 ="SELECT empl_nombre,leco_id,empl_latitud,empl_longitud,leco_latitud,leco_longitud,(6371 * ACOS( 
                                SIN(RADIANS(empl_latitud)) * SIN(RADIANS('$leco_latitud')) 
                                 + COS(RADIANS(empl_longitud - '$leco_longitud')) * COS(RADIANS(empl_latitud)) 
                                 * COS(RADIANS('$leco_latitud'))
                                )
									   ) AS distance
					FROM emplazamiento, lectura_codigo
					where leco_id='$leco_id'
					HAVING distance < 1
					ORDER BY distance ASC
					limit 1";
		
    $res = $db->ExecuteQuery($query2);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener detalle de los códigos"));
    $out['detEmp'] = $res['data'];

	Flight::json($out);
});
	*/


?>
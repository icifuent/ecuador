<?php
include_once("DBObject.php");
include_once("Usuario.php");
include_once("Empresa.php");
include_once("Emplazamiento.php");

echo("Inicio<br/>");

$dbo = new DBObject();
/*
echo("<pre>");
print_r($dbo->read("emplazamiento_categoria"));
echo("</pre><br/><br/><br/>");
*/

echo("<pre>");
print_r($dbo->read("curso", null, array("curs"."_id"=>"="."1")));
echo("</pre><br/><br/><br/>");



echo("ZONAS________________________________________________<br/>");
$obj = new DBObject();
$tabla   = "empresa";
$prefijo = "empr";
$data = array(
	"zona_nombre"=>"ZONA TEST",
	"zona_alias"=>"ZT",
	"zona_descripcion"=>"Zona de pruebas",
	"zona_estado"=>"ACTIVO",
	"zona_tipo"=>"ALCANCE",
	"zona_padre"=>"1",
	"cont_id"=>"1"
);

/*
echo("<pre>"); 
print_r($obj->create($tabla, $data));
echo("</pre><br/><br/><br/>");
*/

echo("EMPRESA_______________________________________________<br/>");
$obj     = new Empresa();
$tabla   = "empresa";
$prefijo = "empr";
$data = array( 
			"comu_id"=>"318",
			"empr_alias"=>"MTEL",
			"empr_nombre"=>"MOVIL TELEFONICA",
			"empr_rut"=>"88888888-8",
			"empr_giro"=>"TELECOMUNICACIONES",
			"empr_direccion"=>"RODO 1799",
			"empr_fecha_creacion"=>"2015-02-09 14:35:00",
			"empr_observacion"=>null,
			"empr_contacto_nombre"=>null,
			"empr_contacto_email"=>null,
			"empr_contacto_telefono_fijo"=>null,
			"empr_contacto_telefono_movil"=>null,
			"empr_contacto_direccion"=>null,
			"empr_estado"=>"ACTIVO",
			"usua_creador" => 1
);

echo("<pre>");
print_r($obj->read($tabla, null, null, null, 5, 0));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($obj->get(1));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($obj->update($tabla, array($prefijo."_estado"=>"ACTIVO"), array($prefijo."_id"=>"=1")));
echo("</pre><br/><br/><br/>");
/*
echo("<pre>");
print_r($obj->create($tabla, $data));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($obj->read($tabla));
echo("</pre><br/><br/><br/>");
*/

echo("EMPLAZAMIENTO_________________________________________<br/>");
$obj     = new Emplazamiento();
$tabla   = "emplazamiento";
$prefijo = "empl";
$data = array( 
			"comu_id"=>"1",
			"clas_id"=>"3",
			"cate_id"=>"4",
			"duto_id"=>"1",
			"empl_nemonico"=>"PRUEBA",
			"empl_nombre"=>"EMPLAZAMIENTO DE PRUEBA",
			"empl_direccion"=>"Dirección de prueba",
			"empl_referencia"=>"Rederencia de prueba",
			"empl_tipo"=>"OUTDOOR",
			"empl_tipo_acceso"=>"4X4",
			"empl_nivel_criticidad"=>"BAJO",
			"empl_espacio"=>"URBANO",
			"empl_macrositio"=>"0",
			"empl_subtel"=>"1",
			"empl_distancia"=>"99999",
			"empl_latitud"=>"-18.48930550",
			"empl_longitud"=>"-70.31116660",
			"empl_observacion"=>"Sin observación",
			"empl_id_emplazamiento_atix"=>"88888",
			"empl_observacion_ingreso"=>"Si observación de ingreso",
			"empl_fecha_creacion"=>"2015-02-10 10:00:00",
			"empl_estado"=>"ACTIVO",
			"empl_subestado"=>"EN MNT PREVENTIVO PERIODICO",
			"usua_creador"=>"1"
);


echo("<pre>");
print_r($obj->read($tabla, null, null, null, 5, 0));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($obj->get(1));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($obj->update($tabla, array($prefijo."_estado"=>"ACTIVO"), array($prefijo."_id"=>"=1")));
echo("</pre><br/><br/><br/>");
/*
echo("<pre>");
print_r($obj->create($tabla, $data));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($obj->read($tabla));
echo("</pre><br/><br/><br/>");
*/

echo("USUARIO_______________________________________________<br/>");
$usuario = new Usuario();
echo("<pre>");
print_r($usuario->read("usuario", null, array("usua_id"=>"=1")));
echo("</pre><br/><br/><br/>");

echo("<pre>");
print_r($usuario->get(1));
echo("</pre><br/><br/><br/>");


echo("DIRECCIONES____________________________________________<br/>");
echo("<pre>");
$provincia = 15;
print_r($dbo->read("comuna", null, (is_null($provincia))?NULL:array("prov_id"=>"=".$provincia) , array("comu_nombre"=>"ASC")));
echo("</pre><br/><br/><br/>");


echo("<pre>");
print_r($dbo->read("pais", null,null, array("pais_nombre"=>"ASC")));
echo("</pre><br/><br/><br/>");



/*
echo("<pre>");
print_r($dbo->get("categoria", null, null, null, 3));
echo("</pre>");

echo("<br/><br/><br/>");

echo("<pre>");
print_r($dbo->get("categoria", null, null, array("cate_id"=>"DESC", "cate_nombre"=>"ASC"), 3, 1));
echo("</pre>");

echo("<br/><br/><br/>");

echo("<pre>");
print_r($dbo->get("categoria", array("cate_id", "cate_nombre", "cate_descripcion"), null, array("cate_id"=>"DESC", "cate_nombre"=>"ASC"), 3, 1));
echo("</pre>");


echo("<br/><br/><br/>");

echo("<pre>");
print_r($dbo->get("categoria", array("cate_id", "cate_nombre", "cate_descripcion"), array("cate_nombre"=>"LIKE '%UNICO%'"), array("cate_id"=>"DESC", "cate_nombre"=>"ASC"), 3, 0));
echo("</pre>");

echo("<br/><br/><br/>");

echo("<pre>");
print_r($dbo->set("categoria", array("cate_estado"=>1), array("cate_id"=>"=15")));
echo("</pre>");


echo("<br/><br/><br/>");

echo("<pre>");
print_r($dbo->create("categoria", array("cate_nombre"=>"'PRUEBAS'", "cate_descripcion"=>"'CATEGORIA DE PRUEBAS'", "cate_estado"=>1)));
echo("</pre>");

*/




echo("Fin<br/>");


function testAPI($obj, $tabla, $prefijo, $data, $insert=false){
	echo("<pre>");
	print_r($obj->read($tabla, null, null, null, 5, 0));
	echo("</pre><br/><br/><br/>");

	echo("<pre>");
	print_r($obj->get(1));
	echo("</pre><br/><br/><br/>");

	echo("<pre>");
	print_r($obj->update($tabla, array($prefijo."_estado"=>"ACTIVO"), array($prefijo."_id"=>"=1")));
	echo("</pre><br/><br/><br/>");
	if( $insert ){
		echo("<pre>");
		print_r($obj->create($tabla, $data));
		echo("</pre><br/><br/><br/>");

		echo("<pre>");
		print_r($obj->read($tabla));
		echo("</pre><br/><br/><br/>");
	}
}

?>
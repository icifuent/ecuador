<?php

Flight::set('flight.log_errors', true);

//LPU______________________________

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/filtros', function($id){
    $out = array("status" => 1, "estados" => array('ACTIVO', 'NOACTIVO'));
    Flight::json($out);	
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/list(/@page:[0-9]+)', function($id, $page){
    $results_by_page = Flight::get('results_by_page');
    $query = "SELECT SQL_CALC_FOUND_ROWS * FROM lpu WHERE cont_id=".$id.((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }   
    
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/add', function($id){
    $data = array_merge($_GET,$_POST);
    $data['cont_id'] = $id;
    $query = "INSERT INTO lpu ".Flight::dataToInsertString($data);
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/upd/@idl:[0-9]+', function($id, $idl){
    $data = array_merge($_GET,$_POST);
    $query = "UPDATE lpu SET ".Flight::dataToUpdateString($data)." WHERE cont_id=".$id." AND lpu_id =".$idl;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/del/@idl:[0-9]+', function($id, $idl){
    $query = "UPDATE lpu SET lpu_estado='NOACTIVO' WHERE cont_id=".$id." AND lpu_id =".$idl;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

//LPU_GRUPO________________________

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/list(/@page:[0-9]+)', function($id, $idl, $page)
{    
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("lpu_grupo"), $filtros_ini);
    
    $query = "SELECT lpu_id FROM lpu WHERE cont_id=".$id." AND lpu_id=".$idl;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"La LPU no pertenece al contrato"));
    }   
    $query = "SELECT SQL_CALC_FOUND_ROWS lg.* "
            . "FROM lpu l, lpu_grupo lg "
            . "WHERE "
                . "l.cont_id=".$id." "
                . "AND l.lpu_id=".$idl." "
                . "AND lg.lpu_id=l.lpu_id "
                . "AND $filtros "
                .((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
    $res = $dbo->ExecuteQuery($query);
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }   
    
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/add', function($id, $idl)
{
    $query = "SELECT lpu_id FROM lpu WHERE cont_id=".$id." AND lpu_id=".$idl;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"La LPU no pertenece al contrato"));
    }  
    
    $data = array_merge($_GET,$_POST);
    $data['lpu_id'] = $idl;
    $query = "INSERT INTO lpu_grupo ".Flight::dataToInsertString($data);
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);    
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo(/@action)/@idg:[0-9]+(/*)', function($id, $idl, $action, $idg)
{    
    if( strlen($action) != 0 && $action!="upd" && $action!="del" ){
        Flight::notFound();
    }    
    $query = "SELECT lg.lpgr_id "
            . "FROM lpu l, lpu_grupo lg "
            . "WHERE "
                . "l.cont_id=".$id." "
                . "AND l.lpu_id=".$idl." "
                . "AND lg.lpgr_id=".$idg." "
                . "AND lg.lpu_id=l.lpu_id ";
       
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"El grupo no pertenece a la LPU en el contrato actual"));
    }  
    return true;
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/upd/@idg:[0-9]+', function($id, $idl, $idg)
{    
    $data = array_merge($_GET,$_POST);
    $query = "UPDATE lpu_grupo SET ".Flight::dataToUpdateString($data)." WHERE lpgr_id=".$idg;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/del/@idg:[0-9]+', function($id, $idl, $idg)
{
    $query = "DELETE FROM lpu_grupo WHERE lpgr_id=".$idg; 
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/clase/list(/@page:[0-9]+)', function($id, $idl, $idg, $page)
{
    $results_by_page = Flight::get('results_by_page');
    $query = "SELECT SQL_CALC_FOUND_ROWS * FROM lpu_grupo_clase WHERE lpgr_id=".$idg.((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }   
    
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/clase/add', function($id, $idl, $idg)
{   
    $data = array_merge($_GET,$_POST);
    $data['lpgr_id'] = $idg;
    $query = "INSERT INTO lpu_grupo_clase ".Flight::dataToInsertString($data);    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});


Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/clase/upd/@idc:[0-9]+', function($id, $idl, $idg, $idc){
    $query = "SELECT lgc.lpgc_id "
            . "FROM lpu l, lpu_grupo lg, lpu_grupo_clase lgc "
            . "WHERE "
                . "l.cont_id=".$id." "
                . "AND l.lpu_id=".$idl." "
                . "AND lg.lpgr_id=".$idg." "
                . "AND lg.lpu_id=l.lpu_id "
                . "AND lgc.lpgc_id=".$idc." "
                . "AND lgc.lpgr_id=lg.lpgr_id "
            ;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"La clase no pertenece al grupo o la LPU en el contrato actual"));
    } 
    
    $data = array_merge($_GET,$_POST);
    $query = "UPDATE lpu_grupo_clase SET ".Flight::dataToUpdateString($data)." WHERE lpgc_id=".$idc;
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/clase/del/@idc:[0-9]+', function($id, $idl, $idg, $idc){
    $query = "SELECT lgc.lpgc_id "
            . "FROM lpu l, lpu_grupo lg, lpu_grupo_clase lgc "
            . "WHERE "
                . "l.cont_id=".$id." "
                . "AND l.lpu_id=".$idl." "
                . "AND lg.lpgr_id=".$idg." "
                . "AND lg.lpu_id=l.lpu_id "
                . "AND lgc.lpgc_id=".$idc." "
                . "AND lgc.lpgr_id=lg.lpgr_id "
            ;
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"La clase no pertenece al grupo o la LPU en el contrato actual"));
    } 
    
    $query = "DELETE FROM lpu_grupo_clase WHERE lpgc_id=".$idc; 
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});


//LPU_ITEM_________________________

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/list(/@page:[0-9]+)', function($id, $idl, $idg, $page)
{ 
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("lpu_item"), $filtros_ini);
    
    $query = "SELECT SQL_CALC_FOUND_ROWS lgi.* "
            . "FROM lpu l, lpu_grupo lg, lpu_item lgi "
            . "WHERE "
                . "l.cont_id=".$id." "
                . "AND l.lpu_id=".$idl." "
                . "AND lg.lpu_id=l.lpu_id "
                . "AND lg.lpgr_id=".$idg." "
                . "AND lgi.lpgr_id=lg.lpgr_id "
                . "AND $filtros "
                .((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }    
    
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/add', function($id, $idl, $idg)
{
    $data = array_merge($_GET,$_POST);
    $data['lpgr_id'] = $idg;
    $query = "INSERT INTO lpu_item ".Flight::dataToInsertString($data);    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item(/@action)/@idi:[0-9]+(/*)', function($id, $idl, $idg, $action, $idi)
{    
    if( strlen($action) != 0 && $action!="upd" && $action!="del" ){
        Flight::notFound();
    }    
    
    $query = "SELECT lgi.lpit_id "
            . "FROM lpu l, lpu_grupo lg, lpu_item lgi "
            . "WHERE "
                . "l.cont_id=".$id." "
                . "AND l.lpu_id=".$idl." "
                . "AND lg.lpgr_id=".$idg." "
                . "AND lg.lpu_id=l.lpu_id "
                . "AND lgi.lpit_id=".$idi." "
                . "AND lgi.lpgr_id=lg.lpgr_id "            
            ;
    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"El item no pertenece al grupo o la LPU en el contrato actual"));
    }   
    
    return true;
});


Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/upd/@idi:[0-9]+', function($id, $idl, $idg, $idi)
{ 
    $data = array_merge($_GET,$_POST);
    $query = "UPDATE lpu_item SET ".Flight::dataToUpdateString($data)." WHERE lpit_id=".$idi;
    $dbo = new MySQL_Database();  
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/del/@idi:[0-9]+', function($id, $idl, $idg, $idi)
{
    $query = "DELETE FROM lpu_item WHERE lpit_id=".$idi; 
    $dbo = new MySQL_Database();  
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/@idi:[0-9]+/precio/list(/@page:[0-9]+)', function($id, $idl, $idg, $idi, $page)
{    
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array("lpu_item_precio"), $filtros_ini);
    
    $query = "SELECT SQL_CALC_FOUND_ROWS lgip.* 
                        FROM lpu l, lpu_grupo lg, lpu_grupo_clase lgc, lpu_item lgi, lpu_item_precio lgip 
                WHERE 
                        l.cont_id=$id
                        AND l.lpu_id=$idl
                        AND lg.lpgr_id=$idg
                        AND lg.lpu_id=l.lpu_id 
                        AND lgi.lpit_id=$idi
                        AND lgi.lpgr_id=lg.lpgr_id 
                        AND lgc.lpgr_id=lgc.lpgr_id
                        AND lgip.lpgc_id=lgc.lpgc_id
                        AND lgip.lpit_id=lgi.lpit_id
                        AND $filtros 
                        ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))."
                ;";
    $dbo = new MySQL_Database();  
    $res = $dbo->ExecuteQuery($query);
        
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }    
    
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/@idi:[0-9]+/precio/add', function($id, $idl, $idg, $idi)
{    
    $data = array_merge($_GET,$_POST);
    $data['lpit_id'] = $idi;
    $query = "INSERT INTO lpu_item_precio ".Flight::dataToInsertString($data);  
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);    
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/@idi:[0-9]+/precio/upd/@idp:[0-9]+', function($id, $idl, $idg, $idi, $idp)
{    
    $query = "SELECT lgip.lpip_id
                        FROM lpu l, lpu_grupo lg, lpu_grupo_clase lgc, lpu_item lgi, lpu_item_precio lgip 
                WHERE 
                        l.cont_id=$id 
                        AND l.lpu_id=$idl
                        AND lg.lpgr_id=$idg
                        AND lg.lpu_id=l.lpu_id 
                        AND lgi.lpit_id=$idi
                        AND lgi.lpgr_id=lg.lpgr_id 
                        AND lgc.lpgr_id=lgc.lpgr_id
                        AND lgip.lpip_id=$idp
                        AND lgip.lpgc_id=lgc.lpgc_id
                        AND lgip.lpit_id=lgi.lpit_id
                ;";    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"El precio no pertenece al item, al grupo o la LPU en el contrato actual"));
    }   

    $data = array_merge($_GET,$_POST);
    $query = "UPDATE lpu_item_precio SET ".Flight::dataToUpdateString($data)." WHERE lpip_id=".$idp;   
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);    
});

Flight::route('GET|POST /core/contrato/@id:[0-9]+/lpu/@idl:[0-9]+/grupo/@idg:[0-9]+/item/@idi:[0-9]+/precio/del/@idp:[0-9]+', function($id, $idl, $idg, $idi, $idp)
{    
    $query = "SELECT lgip.lpip_id
                        FROM lpu l, lpu_grupo lg, lpu_grupo_clase lgc, lpu_item lgi, lpu_item_precio lgip 
                WHERE 
                        l.cont_id=$id 
                        AND l.lpu_id=$idl
                        AND lg.lpgr_id=$idg
                        AND lg.lpu_id=l.lpu_id 
                        AND lgi.lpit_id=$idi
                        AND lgi.lpgr_id=lg.lpgr_id 
                        AND lgc.lpgr_id=lgc.lpgr_id
                        AND lgip.lpip_id=$idp
                        AND lgip.lpgc_id=lgc.lpgc_id
                        AND lgip.lpit_id=lgi.lpit_id
                ;";    
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 || $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"El precio no pertenece al item, al grupo o la LPU en el contrato actual"));
    } 

    $data = array_merge($_GET,$_POST);
    $query = "DELETE FROM lpu_item_precio WHERE lpip_id=".$idp;  
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});


?>
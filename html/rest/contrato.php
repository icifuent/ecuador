<?php

Flight::set('flight.log_errors', true);

Flight::route('GET /contrato/@cont_id:[0-9]+/docs', function($cont_id){    
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();
    
    $res = $dbo->ExecuteQuery("SELECT   cont_id, 
                                        cont_nombre, 
                                        cont_alias, 
                                        DATE_FORMAT(cont_fecha_inicio, '%d-%m-%Y'), 
                                        DATE_FORMAT(cont_fecha_termino, '%d-%m-%Y'), 
                                        cont_fecha_creacion, 
                                        cont_observacion,
                                        cont_descripcion,
                                        cont_estado,
                                        usua_creador FROM contrato WHERE cont_id=$cont_id");
    if( 0==$res['status'] ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['contrato'] = $res['data'][0];


    $res = $dbo->ExecuteQuery("SELECT
                                  repo_id
                                  , repo_tipo_doc
                                  , repo_nombre
                                  , repo_descripcion
                                  , repo_tabla
                                  , repo_tabla_id
                                  , repo_ruta
                                  , repo_fecha_creacion
                                  , repo_data
                                  , usua_nombre AS usua_creador
                                FROM 
                                repositorio 
                                LEFT JOIN usuario ON (usuario.usua_id=repositorio.usua_creador)
                                WHERE repo_tabla='contrato' AND repo_tabla_id=$cont_id
                                ORDER BY repo_tipo_doc,repo_nombre");
    if( 0==$res['status'] ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['archivos'] = array();
    foreach($res['data'] AS $row){
		if(!isset($out['archivos'][$row['repo_tipo_doc']])){
			$out['archivos'][$row['repo_tipo_doc']] = array();
		}
		array_push($out['archivos'][$row['repo_tipo_doc']],$row);
    }

  
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/especialidad/@espe_id:[0-9]+/upd',function($cont_id,$espe_id){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();
    $data = array_merge($_GET,$_POST); 
    $query="UPDATE especialidad SET ".Flight::dataToUpdateString($data)." WHERE cont_id=".$cont_id." AND espe_id=".$espe_id;    
    $res=$dbo->ExecuteQuery($query);
    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
   
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/especialidad/formulario/@form_id:[0-9]+/@espe_id:[0-9]+/del',function($cont_id,$form_id,$espe_id){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();
    $query='DELETE FROM rel_formulario_especialidad WHERE form_id='.$form_id.' AND espe_id='.$espe_id;
    $out['query']=$query;
     $res=$dbo->ExecuteQuery($query);

    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
   
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/especialidad/formulario/add',function(){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();
    $data = array_merge($_GET,$_POST); 

    $query='DELETE FROM rel_formulario_especialidad WHERE espe_id='.$data['form'][0]['espe_id'];
    $res=$dbo->ExecuteQuery($query);
    if(0==$res['status']) {
		Flight::json(array("status" => 0, "error" => $res['error']));
		return;
	}

    //Flight::json(array("status" => 0, "error" => count($data['form'])));return;
    for($i=0;$i<count($data['form']);$i++){
        $query="INSERT INTO rel_formulario_especialidad ".Flight::dataToInsertString($data['form'][$i]);
        $res=$dbo->ExecuteQuery($query);
        if(0==$res['status']) {
			Flight::json(array("status" => 0, "error" => $res['error']));
			return;
		}
    }
   
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/periodicidad/add',function($cont_id,$peri_id,$rcpe_dias_apertura,$rcpe_dias_post_cierre,$rcpe_participa_mpp){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();
    $data=array_merge($_GET,$_POST);
    $query="INSERT INTO rel_contrato_periodicidad ".Flight::dataToInsertString($data);
    $res=$dbo->ExecuteQuery($query);
    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/mant_def_form/add',function($cont_id,$clpr_id,$espe_id,$form_id,$peri_id,$peri_mes){
    
	$dbo=new MySQL_Database();
    $data=array_merge($_GET,$_POST);
   
	$query="INSERT INTO mantenimiento_definicion_formulario ".Flight::dataToInsertString($data);
    $res=$dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/mantenimiento_definicion_formulario/@madf_id:[0-9]+/del',function($cont_id,$madf_id){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();

    $query="DELETE FROM mantenimiento_definicion_formulario WHERE madf_id=".$madf_id." AND cont_id=".$cont_id;
    $res=$dbo->ExecuteQuery($query);
    $out=$res;
    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/mant_def_form/list',function($cont_id){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();

    $query="SELECT 
			madf_id,peri_nombre,clpr_nombre,espe_nombre,form_nombre,peri_mes 
			FROM mantenimiento_definicion_formulario mdf,especialidad e,formulario f, clasificacion_programacion cp,periodicidad p  
			WHERE  cp.clpr_id=mdf.clpr_id AND 
				   f.form_id=mdf.form_id AND 
				   e.espe_id=mdf.espe_id AND 
				   p.peri_id=mdf.peri_id AND 
				   mdf.cont_id=$cont_id 
				   ORDER BY peri_mes";
    $res=$dbo->ExecuteQuery($query);
    $out=$res;

    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/emplazamiento_clasificacion/list',function($cont_id){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();

    $query="SELECT * FROM emplazamiento_clasificacion ec WHERE  cont_id=".$cont_id;

    $res = $dbo->ExecuteQuery($query);
        $out['data']=$res['data'];

    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/clasificacion_programacion/list',function($cont_id){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();

    $query="SELECT * FROM clasificacion_programacion WHERE  cont_id=$cont_id AND clpr_activo='ACTIVO'";

    $res = $dbo->ExecuteQuery($query);
        $out['data']=$res['data'];

    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});

/*
Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/periodicidad/list',function(){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();

    $query="SELECT * FROM rel_contrato_periodicidad recp,periodicidad peri WHERE recp.peri_id=peri.peri_id";

    $res = $dbo->ExecuteQuery($query);
        $out['data']=$res['data'];

    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});
*/

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/especialidad/add',function($cont_id,$espe_id,$espe_estado){
    $out=array();
    $out['status']=1;
    $dbo=new MySQL_Database();
    $data = array_merge($_GET,$_POST); 
    //obtiene el id con mayor valor se le suma 1 y se ingresa ese
    $query="SELECT espe_id FROM especialidad ORDER BY espe_id DESC";
    $res = $dbo->ExecuteQuery($query);
    $out['espe_id']=$res['data'];
    $data['espe_id']=$out['espe_id'][0]['espe_id']+1;
   
    $query= "INSERT INTO especialidad ".Flight::dataToInsertString($data);
    $res=$dbo->ExecuteQuery($query);
    
    if($res['status'] == 0) {Flight::json(array("status" => 0, "error" => $res['error']));return;}
    Flight::json($out);
});

Flight::route('GET|POST /core/contrato/@cont_id:[0-9]+/especialidad/@espe_id:[0-9]+/formulario/list',function($cont_id,$espe_id){

    $dbo=new MySQL_Database();

    $res=$dbo->ExecuteQuery("SELECT f.*,rfe.* FROM formulario f,rel_formulario_especialidad rfe WHERE f.cont_id=$cont_id AND rfe.espe_id=$espe_id AND rfe.form_id=f.form_id");
    Flight::json($res);
});

Flight::route('GET /core/contrato/filtros', function(){    
    
    $out = array();
    $dbo = new MySQL_Database();
  
    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM contrato WHERE Field = 'cont_estado'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);    
    $out['estados'] = explode("','", $matches[1]); 
    
    $out['status'] = 1;
    Flight::json($out);
});

Flight::route('GET /core/contrato/list', function(){
    $dbo = new MySQL_Database();
    $out['status']=1;
    $res = $dbo->ExecuteQuery("SELECT   cont_id
                                        ,cont_nombre
                                        ,cont_alias
                                        ,cont_fecha_inicio
                                        ,cont_fecha_termino
                                        ,cont_fecha_creacion
                                        ,cont_observacion
                                        ,cont_descripcion
                                        ,cont_estado
                                        ,usua_creador
                                        ,pais_id
                                FROM contrato 
                                WHERE cont_estado='ACTIVO'
    ");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo recuperar la lista de contratos"));
    Flight::json($res);
});

Flight::route('GET /core/contrato/@cont_id:[0-9]+/lista/excepciones', function($cont_id){
    $dbo = new MySQL_Database();
    $out['status'] = 1;

    $res = $dbo->ExecuteQuery(" SELECT  exlo_id
                                        ,exlo_palabra_clave 
                                FROM excepciones_login 
                                WHERE cont_id =$cont_id
                                AND exlo_estado = 'ACTIVO'
    ");

    if (0 == $res['status'] ) {
        Flight::json(array("status" => 0, "error" => "No se pudo recuperar la lista de contratos"));
    }
    $out['excepciones'] = $res['data']; 

    Flight::json($out);
});

Flight::route('POST /core/contrato/@cont_id:[0-9]+/add/excepcion', function($cont_id){
    $dbo = new MySQL_Database();
    $out['status'] = 1;
    $excepcion = array_merge($_GET,$_POST);
    $cont_excepcion= $excepcion['cont_excepcion'];

    /*Validador de password / si existe en las excepciones no permite el ingreso */
    $query = ("     SELECT exlo_palabra_clave
                    FROM excepciones_login
                    WHERE cont_id = $cont_id
                    AND  exlo_estado = 'ACTIVO'
                    AND exlo_palabra_clave = UPPER ( '$cont_excepcion')");

    $res = $dbo->ExecuteQuery($query);
    if( 0 == $res['status'] ) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
     /*Si existe el registro arrojara un error */
    if( 0 <$res['rows']){
       Flight::json(array("status"=>2,"rows"=>1,"data"=>array(array("status"=>2,"id"=>0)), "error"=>"error" ));
        return;
        
    }

    $query = "INSERT INTO   excepciones_login(  cont_id
                                        ,exlo_palabra_clave  
                                    )
                            VALUES  (
                                      $cont_id
                                      ,UPPER ( '$cont_excepcion')
                                    )";

    $res = $dbo->ExecuteQuery($query);
    if (0 == $res['status']) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    Flight::json($out);
});
/*Elimina una excepcion de la lista por contrato*/
Flight::route('POST /core/contrato/@cont_id:[0-9]+/eliminar/excepcion', function($cont_id){
    $dbo = new MySQL_Database();
    $out['status'] = 1;
    $excepcion = array_merge($_GET,$_POST);
    $exlo_id= $excepcion['exlo_id'];
    /*Cambia a estado NOACTIVO para remover logicamente la excepción */
    $query = ("     UPDATE excepciones_login 
                    SET exlo_estado = 'NOACTIVO'
                    where cont_id = $cont_id
                    AND exlo_id = $exlo_id");

    $res = $dbo->ExecuteQuery($query);
    if( 0 == $res['status'] ) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }
    Flight::json($out);
});

    
?>
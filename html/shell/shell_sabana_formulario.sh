CARPETA='/var/lib/mysql'
RUTA='/var/www/siom.movistar.cl/html/reportes/'
RETCODE=0

set `date +%m" "%Y`
CURMTH=$1
CURYR=$2

if [ $CURMTH -eq 1 ]
then PRVMTH=12
     PRVYR=`expr $CURYR - 1`
else PRVMTH=`expr $CURMTH - 1`
     PRVYR=$CURYR
fi

if [ $PRVMTH -lt 10 ]
	then PRVMTH="0"$PRVMTH
fi

LASTDY=`cal $PRVMTH $PRVYR | egrep "28|29|30|31" |tail -1 |awk '{print $NF}'`

fecha_kpi_inicio=$PRVYR-$PRVMTH-01
fecha_kpi_fin=$PRVYR-$PRVMTH-$LASTDY

# INICIO REPORTES SABANA FORMULARIO
#php ../server/sabana_formulario_reportes.php $fecha_kpi_inicio $fecha_kpi_fin $PRVMTH $PRVYR
php ../server/sabana_formulario_reportes.php
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi
# FIN REPORTES SABANA FORMULARIO

exit 0;

#FIN SHELL REPORTES SABANA FORMULARIO
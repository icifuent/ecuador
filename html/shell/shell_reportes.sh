# SHELL CREACION REPORTES

RETCODE=0

fecha=`date +'%Y%m%d'`
archivo_os=reporte_os_$fecha.csv
archivo_mnt=reporte_mnt_$fecha.csv

meses_os='6';
meses_mnt='6';

php ../rest/generar_reportes.php $archivo_os $archivo_mnt $meses_os $meses_mnt
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi

#CONTAR Y DESCARTAR EL MAS ANTIGUO SI ES IFUAL A 5

#MOVER
mv $archivo_os /var/www/siom.movistar.cl/html/reportes/$archivo_os
mv $archivo_mnt /var/www/siom.movistar.cl/html/reportes/$archivo_mnt
exit 0;

#FIN SHELL CREACION REPORTES
##CARPETA='/var/lib/mysql-files'
RUTA='/var/www/siom.movistar.cl/html/reportes/'
RETCODE=0

#set `date +%m" "%Y`
CURMTH=`date +'%m'`
CURYR=`date +'%Y'`
echo $CURMTH
echo $CURYR
if [ $CURMTH -eq 1 ] ; then 
	 PRVMTH=12
     PRVYR=`expr $CURYR - 1`
	 echo $PRVYR
else 
	PRVMTH=`expr $CURMTH - 1`
    PRVYR=$CURYR
fi

if [ $PRVMTH -lt 10 ]
	then PRVMTH="0"$PRVMTH
fi

LASTDY=`cal $PRVMTH $PRVYR | egrep "28|29|30|31" |tail -1 |awk '{print $NF}'`

fecha_kpi_inicio=$PRVYR-$PRVMTH-01
fecha_kpi_fin=$PRVYR-$PRVMTH-$LASTDY

# INICIO REPORTES KPI

rm -f "${RUTA}CHI_FLM_SITIOS_ACTIVOS_${PRVYR}${PRVMTH}.csv"
RETCODE=$?
echo $RETCODE
echo "${RUTA}CHI_FLM_SITIOS_ACTIVOS_${PRVYR}${PRVMTH}.csv"
rm -f "${RUTA}CHI_FLM_MNTO_PREVENT_${PRVYR}${PRVMTH}.csv"
RETCODE=$?
echo $RETCODE
echo "${RUTA}CHI_FLM_MNTO_PREVENT_${PRVYR}${PRVMTH}.csv"
rm -f "${RUTA}CHI_FLM_SERVICIOS_BAJO_DEMANDA_${PRVYR}${PRVMTH}.csv"
RETCODE=$?
echo $RETCODE
echo "${RUTA}CHI_FLM_SERVICIOS_BAJO_DEMANDA_${PRVYR}${PRVMTH}.csv"
rm -f "${RUTA}CHI_FLM_CORRECTIVO_INCIDENCIAS_${PRVYR}${PRVMTH}.csv"
RETCODE=$?
echo $RETCODE
echo "${RUTA}CHI_FLM_CORRECTIVO_INCIDENCIAS_${PRVYR}${PRVMTH}.csv"

# php ../server/kpi_reportes.php $CARPETA $fecha_kpi_inicio $fecha_kpi_fin $PRVMTH $PRVYR
# php ../server/kpi_reportes.php $RUTA $fecha_kpi_inicio $fecha_kpi_fin $PRVMTH $PRVYR
php ../server/kpi_reportes.php $RUTA $fecha_kpi_inicio $fecha_kpi_fin $PRVMTH $PRVYR

RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi
# FIN REPORTES KPI

exit 0;

#FIN SHELL REPORTES KPI
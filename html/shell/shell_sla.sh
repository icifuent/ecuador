#CARPETA='/var/lib/mysql-files'
#RUTA='/var/www/siom.movistar.cl/html/reportes/'
RETCODE=0


#set `date +%m" "%Y`
CURDATE=`date +'%Y%m%d'`

echo "INICIO SLA ACTUALIZAR ${CURDATE}"
php ../server/sla_actualizar.php
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR DURANTE LA ACTUALIZACION'
	exit 1;
fi

CURDATE=`date +'%Y%m%d'`
echo "FIN SLA ACTUALIZAR $CURDATE"
# FIN REPORTES KPI

exit 0;

#FIN SHELL REPORTES KPI
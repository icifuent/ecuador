#INICIO SHELL REPORTES OS
#CARPETA='/var/www/siom.movistar.cl/html/reportes'
CARPETA='/var/lib/mysql-files'
RETCODE=0

# INICIO CONSOLIDADO
php ../server/os_consolidado.php
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi
# FIN CONSOLIDADO

# INICIO GENERAR REPORTES
fecha=`date +'%Y%m%d'`
fecha_inicio=$(date --date "-6 month" +'%Y-%m-%d')
fecha_fin=`date +'%Y-%m-%d'`

set `date +%m" "%Y`
CURMTH=$1
CURYR=$2

if [ $CURMTH -eq 1 ]
then PRVMTH=12
     PRVYR=`expr $CURYR - 1`
else PRVMTH=`expr $CURMTH - 1`
     PRVYR=$CURYR
fi

if [ $PRVMTH -lt 10 ]
	then PRVMTH="0"$PRVMTH
fi


LASTDY=`cal $PRVMTH $PRVYR | egrep "28|29|30|31" |tail -1 |awk '{print $NF}'`

fecha_sap_inicio=$PRVYR-$PRVMTH-01
fecha_sap_fin=$PRVYR-$PRVMTH-$LASTDY

echo $PRVMTH
echo $PRVYR

echo $fecha
echo $fecha_inicio
echo $fecha_fin
echo $fecha_sap_inicio
echo $fecha_sap_fin

nombre_os=os_$fecha.csv
nombre_sap_os=os_sap_$fecha.csv
archivo_os=${CARPETA}/$nombre_os
archivo_sap_os=${CARPETA}/$nombre_sap_os

echo $archivo_os
echo $archivo_sap_os

#Elimina si existe el archivo 
rm -f $archivo_os
rm -f $archivo_sap_os

php ../server/os_reportes.php $archivo_os $archivo_sap_os $fecha_inicio $fecha_fin $fecha_sap_inicio $fecha_sap_fin
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi

# FIN GENERAR REPORTES

#SINCRONIZACION ARCHIVO
rutaarchivo_os=/reportes/$nombre_os
rutaarchivo_os_sap=/reportes/$nombre_sap_os

#iconv -f UTF-8 -t ISO-8859-1 $archivo_os > /srv/siom.movistar.cl/reportes/$nombre_os
#iconv -f UTF-8 -t ISO-8859-1 $archivo_sap_os > /srv/siom.movistar.cl/reportes/$nombre_sap_os

#mv $archivo_os /srv/siom.movistar.cl/reportes/$nombre_os
#mv $archivo_sap_os /srv/siom.movistar.cl/reportes/$nombre_sap_os
#FIN SINCRONIZACION ARCHIVO
#INICIO REGISTRAR ARCHIVO BBDD
#$tipo_doc               = $argv[1];
#$nombre                 = $argv[2];
#$descripcion            = $argv[3];
#$reporte                = $argv[4];
#$reporte_id             = $argv[5];
#$ruta                   = $argv[6];
#$usua_creador           = $argv[7];

php ../server/insert_repositorio.php "reporte_os" "os_$fecha.csv" "Reporte os_sap generado automaticamente por el sistema" "os" '1' $rutaarchivo_os "sys_admin"
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi
php ../server/insert_repositorio.php "reporte_os" "os_sap_$fecha.csv" "Reporte os_sap generado automaticamente por el sistema" "os_sap" '1' $rutaarchivo_os_sap "sys_admin"
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi
#FIN REGISTRAR ARCHIVO BBDD



exit 0;

#FIN SHELL REPORTES OS

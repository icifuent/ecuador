#INICIO SHELL REPORTES MNT

#FICHERO INICIAL QA
#CARPETA='/var/lib/mysql-files'
#FICHERO INICIAL PROD
CARPETA='/var/lib/mysql-files'
RUTA='/var/www/siom.movistar.cl/html/reportes/'
RETCODE=0

# INICIO CONSOLIDADO
php ../server/mnt_consolidado.php
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi
# FIN CONSOLIDADO

# INICIO GENERAR REPORTES
fecha=`date +'%Y%m%d'`
fecha_inicio=$(date --date "-6 month" +'%Y-%m-%d')
fecha_fin=`date +'%Y-%m-%d'`


echo $fecha
echo $fecha_inicio
echo $fecha_fin

nombre_mnt=mnt_$fecha.csv
nombre_sap_mnt=mnt_sap_$fecha.csv
archivo_mnt=${CARPETA}/$nombre_mnt

echo $archivo_mnt


#Elimina si existe el archivo 
rm -f $archivo_mnt

php ../server/mnt_reportes.php $archivo_mnt $fecha_inicio $fecha_fin 
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi

# FIN GENERAR REPORTES

#SINCRONIZACION ARCHIVO
rutaarchivo_mnt=/reportes/$nombre_mnt


#mv $archivo_mnt /srv/siom.movistar.cl/reportes/$nombre_mnt
#mv $archivo_sap_mnt /srv/siom.movistar.cl/reportes/$nombre_sap_mnt
#FIN SINCRONIZACION ARCHIVO
#INICIO REGISTRAR ARCHIVO BBDD
#$tipo_doc               = $argv[1];
#$nombre                 = $argv[2];
#$descripcion            = $argv[3];
#$reporte                = $argv[4];
#$reporte_id             = $argv[5];
#$ruta                   = $argv[6];
#$usua_creador           = $argv[7];
echo $rutaarchivo_mnt
php ../server/insert_repositorio.php "reporte_mnt" "mnt_$fecha.csv" "Reporte mnt generado automaticamente por el sistema" "mnt" '1' $rutaarchivo_mnt "sys_admin"
RETCODE=$?
if [ ${RETCODE} -ne 0 ]; then
	echo 'ERROR'
fi

#FIN REGISTRAR ARCHIVO BBDD



exit 0;

#FIN SHELL REPORTES MNT

<?php

$dir 	= $argv[1];
$count 	= 0;
$sqlfile = "db-update_".time().".sql";

foreach (glob($dir."/siom*.log") as $filename) {
    echo "Procesando ".basename($filename)."\n";

    $handle = fopen($filename, "r");
	if ($handle) {
	    while (($line = fgets($handle)) !== false) {
	       	if(strpos($line,"INSERT INTO rel_tarea_formulario_respuesta_revisiones SET")!==FALSE){
	       		$fore_id = 0;
	       		$date    = "";

	       		$line = fgets($handle);

	       		$line = fgets($handle);
	       		if($line!=NULL){
	       			echo $line;
	       			//fore_id=1386,
	       			$line = trim($line);
	       			preg_match('#fore_id=(.+),#s',$line,$matches);
					$fore_id = $matches[1];
	       		}

	       		$line = fgets($handle);
	       		if($line!=NULL){
	       			if(strpos($line,"CONVERT_TZ")===FALSE){
	       				continue;
	       			}

	       			echo $line;
	       			//rfrr_fecha=CONVERT_TZ('2015-06-23 05:11:55','GMT','-03:00'),
	       			$line = trim($line);
	       			preg_match('#rfrr_fecha=(.+),#s',$line,$matches);
	       			$date = $matches[1];
	       		}
	       		if($fore_id!=0 && $date!=""){
	       			echo $fore_id." => ".$date."\n";
	       			$data = "UPDATE formulario_respuesta SET fore_fecha_creacion=$date WHERE fore_id=$fore_id AND fore_fecha_creacion IS NULL;\n";
	       			$data.= "UPDATE rel_tarea_formulario_respuesta SET rtfr_fecha=$date WHERE fore_id=$fore_id AND rtfr_fecha IS NULL;\n";
	       			$data.= "UPDATE rel_tarea_formulario_respuesta_revisiones SET rfrr_fecha=$date WHERE fore_id=$fore_id AND rfrr_fecha IS NULL;\n";

	       			file_put_contents($sqlfile,$data,FILE_APPEND);
	       			$count++;
	       		}

	       	}
	    }

	    fclose($handle);
	} else {
	    echo "Error al abrir archivo\n";
	} 
}

echo "Total de actualizaciones: ".$count."\n";



?>
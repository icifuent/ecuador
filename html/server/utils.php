<?php

function Loggear($message,$priority=LOG_INFO){
    global $ENV;
    echo("[SIOM-".strtoupper($ENV)."] ".$message."\n");
    syslog($priority,"[SIOM-".strtoupper($ENV)."] ".$message);
}

function ParsearDatos($datos){
    if($datos!="" && $datos!=null){
        if(is_array($datos)){
            return $datos;
        }
        return json_decode(str_replace("\'","\"",$datos),true);
    }

    return null;
}

function Juntar(){
    $array = array();
    foreach (func_get_args() as $p) {
        if($p){
            if(is_array($p)){
                $array = array_merge($array,$p);
            }
            else{
                $array[] = $p;
            }
        }
    }
    return array_unique($array);
}

function EnviarTarea($db,$usua_id,$tare_modulo,$tare_tipo,$tare_id_relacionado,$tare_data=null){
    $res = array("status"=>true);

    if($tare_data){
        $data=",tare_data='$tare_data'";
    }
    else{
        $data="";
    }

    if(!is_array($usua_id)){
        $usua_id = array($usua_id);
    }

    foreach ($usua_id as $u) {
        $res = $db->ExecuteQuery("INSERT INTO tarea SET
                                    usua_id = $u,
                                    tare_modulo = '$tare_modulo',
                                    tare_tipo = '$tare_tipo',
                                    tare_id_relacionado = $tare_id_relacionado,
                                    tare_fecha_despacho = NOW(),
                                    tare_estado = 'CREADA'
                                    $data");
        if($res['status']==0){
            break;
        }
        Loggear("Tarea ".$tare_modulo.":".$tare_tipo." enviada a usuario ".$u." (id: ".$tare_id_relacionado.")");
		/* SE COMENTA EN DESARROLLO PARA NO ENVIAR CORREOS AL CLIENTE NI A LA CONTRATA
        EnviarEmail($db,$u,$tare_modulo,$tare_tipo,$tare_id_relacionado,$tare_data);
		*/
    }
    return $res;
}

function EnviarNotificacion($db,$usua_id,$noti_modulo,$noti_tipo,$noti_id_relacionado,$noti_data=null){
    if($noti_data){
        $data=",noti_data='$noti_data'";
    }
    else{
        $data="";
    }

    if(!is_array($usua_id)){
        $usua_id = array($usua_id);
    }

    //Obtener contrato
    $cont_id = 0;
    switch($noti_modulo){
        case 'OS':{
            $orse_id = $noti_id_relacionado;

            $res = $db->ExecuteQuery("SELECT cont_id FROM orden_servicio WHERE orse_id=$orse_id");
            if($res['status']==0 || $res['rows']==0){
                return $res;
            }
            $cont_id = $res['data'][0]['cont_id'];
            break;
        }
        case 'MNT':{
            $mant_id = $noti_id_relacionado;

            $res = $db->ExecuteQuery("SELECT cont_id FROM mantenimiento WHERE mant_id=$mant_id");
            if($res['status']==0 || $res['rows']==0){
                return $res;
            }
            $cont_id = $res['data'][0]['cont_id'];
            break;
        }
        case 'INSP':{
            $insp_id = $noti_id_relacionado;

            $res = $db->ExecuteQuery("SELECT cont_id FROM inspeccion WHERE insp_id=$insp_id");
            if($res['status']==0 || $res['rows']==0){
                return $res;
            }
            $cont_id = $res['data'][0]['cont_id'];
            break;
        }
    }

    if($cont_id==0){
        $res = array("status"=>false,"error"=>"No se pudo obtener contrato para verificar envío de notificacion");
        Loggear($res["error"]);
        return $res;
    }

    foreach ($usua_id as $u) {
        $res = $db->ExecuteQuery("SELECT
                                    rcun_id
                                 FROM rel_contrato_usuario_notificacion
                                 INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_notificacion.recu_id)
                                 WHERE
                                    rel_contrato_usuario.cont_id=$cont_id AND
                                    rel_contrato_usuario.usua_id=$u AND
                                    rel_contrato_usuario_notificacion.modu_alias  = '$noti_modulo' AND
                                    rel_contrato_usuario_notificacion.rcun_evento = '$noti_tipo' AND
                                    rel_contrato_usuario_notificacion.rcun_accion = 'ENVIAR_NOTIF_WEB'");
        if($res['status']==0){
            break;
        }
        if($res['rows']==0){
            continue;
        }

        $res = $db->ExecuteQuery("INSERT INTO notificacion SET
                                    usua_id = $u,
                                    noti_modulo = '$noti_modulo',
                                    noti_tipo = '$noti_tipo',
                                    noti_id_relacionado = $noti_id_relacionado,
                                    noti_fecha_despacho = NOW(),
                                    noti_estado = 'DESPACHADA'
                                    $data");
        if($res['status']==0){
            break;
        }

        Loggear("Notificación ".$noti_modulo.":".$noti_tipo." enviada a usuario ".$u." (id: ".$noti_id_relacionado.")");
    }
    return $res;
}


function EnviarPush($db,$usua_id,$type,$message){
    Loggear("usua_id: ".$usua_id);
    $res = $db->ExecuteQuery("SELECT
                                reum_id,
                                reum_imei,
                                reum_codigo_registro
                               FROM rel_usuario_movil
                               WHERE
                                   usua_id=$usua_id AND
                                   reum_codigo_registro IS NOT NULL AND
                                   reum_codigo_registro != '' AND
                                   reum_estado='ACTIVO'");
    if($res['status']==0){
           return $res;
    }
    if(0<$res['rows']){
        $data = $res['data'];
        foreach($data as $row){
            Loggear("CODIGO REGISTRO:".$row['reum_codigo_registro']);
            Loggear("TYPE: ".$type);
            Loggear("MESSAGE: ".$message);
            Loggear("Inicio PUSH");
            $res = SendPush(array($row['reum_codigo_registro']),array("type"=>$type,"message"=>$message));
            Loggear("FIN PUSH");
            if($res['status']==1){
                Loggear("Se envio push ".$type." a usuario ".$usua_id." (imei: ".$row['reum_imei'].")");
            } else {
                Loggear("ERROR:".$res['error']);
                if($res['error']=="MismatchSenderId" || $res['error']=="NotRegistered"){
                    $resDel = $db->ExecuteQuery("UPDATE rel_usuario_movil SET reum_codigo_registro = NULL WHERE reum_id='".$row['reum_id']."'");
                    if(!$resDel['status']){
                        Loggear($resDel['error'],LOG_ERR);
                    }
                }
                $res['error'] = "Error enviando push ".$type." a usuario ".$usua_id." (imei: ".$row['reum_imei']."): ".$res['error'];
                Loggear($res['error'],LOG_ERR);
            }
        }
        return $res;
    } else {
       Loggear("Usuario ".$usua_id." no registrado para envío de push");
       return array("status"=>true);//,"error"=>"Usuario no registrado");
   }
}

/* SE COMENTA PARA NO ENVIAR CORREOS AL CLIENTE NI A LA CONTRATA*/
function EnviarEmail($db,$usua_id,$modulo,$tipo,$id_relacionado,$data=null){
    global $EMAIL_FROM;

    if(!is_array($usua_id)){
        $usua_id = array($usua_id);
    }

    foreach ($usua_id as $u) {
        Loggear($u);
        $data = ParsearDatos($data);
        switch($modulo){
            case 'OS':{
                $orse_id = $id_relacionado;
                Loggear($u);
                Loggear($orse_id);
                Loggear($modulo);
                Loggear($tipo);
                $sql = "SELECT
                                                usua_correo_electronico
                                            FROM
                                                rel_contrato_usuario_notificacion,
                                                rel_contrato_usuario,
                                                orden_servicio,
                                                usuario
                                            WHERE
                                            orden_servicio.orse_id = $orse_id AND
                                            usuario.usua_id = $u AND
                                            rel_contrato_usuario.cont_id = orden_servicio.cont_id AND
                                            rel_contrato_usuario.usua_id =  usuario.usua_id AND
                                            rel_contrato_usuario_notificacion.recu_id = rel_contrato_usuario.recu_id AND
                                            rel_contrato_usuario_notificacion.modu_alias = '$modulo' AND
                                            '$tipo' LIKE rel_contrato_usuario_notificacion.rcun_evento AND
                                            rel_contrato_usuario_notificacion.rcun_accion = 'ENVIAR_EMAIL'";
                Loggear($sql);
                $res = $db->ExecuteQuery($sql);
                if($res['status']==0 || $res['rows']==0){
                   Loggear("exit sendEmail");
                   return $res;
                }
                Loggear($res['data'][0]['usua_correo_electronico']);
                $to          = $res['data'][0]['usua_correo_electronico'];
                $title       = "OS Nº ".$orse_id.": ".str_replace("_"," ",$tipo);
                $message     = ObtenerInformacionOS($db,$orse_id);
                $attachments = null;

                switch($tipo){
                    case 'PRESUPUESTO_AGREGADO':{
                        $pres_id = $data['pres_id'];

                        $res = $db->ExecuteQuery("SELECT
                                                    repo_ruta
                                                    FROM
                                                    repositorio
                                                    WHERE
                                                    repo_nombre LIKE 'OS-$orse_id%' AND repo_tipo_doc='PRESUPUESTO' AND repo_tabla='presupuesto' AND repo_tabla_id=$pres_id
                                                    ORDER BY repo_fecha_creacion DESC
                                                    LIMIT 1");
                        if($res['status']==0 || $res['rows']==0){
                            return $res;
                        }

                        global $BASEDIR;
                        $attachments = array();
                        foreach($res['data'] as $row){
                            array_push($attachments,$BASEDIR.$row['repo_ruta']);
                        }
                        break;
                    }

                    case 'INFORME_AGREGADO':{
                        $info_id = $data['info_id'];

                        $res = $db->ExecuteQuery("SELECT
                                                    repo_ruta
                                                    FROM
                                                    repositorio
                                                    WHERE
                                                    repo_nombre LIKE 'OS-$orse_id%' AND repo_tipo_doc='INFORME' AND repo_tabla='informe' AND repo_tabla_id=$info_id
                                                    ORDER BY repo_fecha_creacion DESC
                                                    LIMIT 1");
                        if($res['status']==0 || $res['rows']==0){
                            return $res;
                        }

                        global $BASEDIR;
                        $attachments = array();
                        foreach($res['data'] as $row){
                            array_push($attachments,$BASEDIR.$row['repo_ruta']);
                        }
                        break;
                    }

                }

                    $res = SendEmail($EMAIL_FROM,$to,$title,$message,$attachments);
                    if($res['status']==1){
                        Loggear("Se envio email a ".$to." para notificar ".$modulo.":".$tipo);
                    }
                else{
                    Loggear("Error enviando email: ".$res['error'],LOG_ERR);
                }
                break;
            }
            case 'MNT':{
                $mant_id = $id_relacionado;

                $res = $db->ExecuteQuery("SELECT
                                                usua_correo_electronico
                                            FROM
                                                rel_contrato_usuario_notificacion,
                                                rel_contrato_usuario,
                                                mantenimiento,
                                                usuario
                                            WHERE
                                            mantenimiento.mant_id = $mant_id AND
                                            usuario.usua_id = $u AND
                                            rel_contrato_usuario.cont_id = mantenimiento.cont_id AND
                                            rel_contrato_usuario.usua_id =  usuario.usua_id AND
                                            rel_contrato_usuario_notificacion.recu_id = rel_contrato_usuario.recu_id AND
                                            rel_contrato_usuario_notificacion.modu_alias = '$modulo' AND
                                            '$tipo' LIKE rel_contrato_usuario_notificacion.rcun_evento AND
                                            rel_contrato_usuario_notificacion.rcun_accion = 'ENVIAR_EMAIL'");
                if($res['status']==0 || $res['rows']==0){
                       return $res;
                   }

                   $to      = $res['data'][0]['usua_correo_electronico'];
                $title   = "MNT Nº ".$mant_id.": ".$tipo;
                $message = ObtenerInformacionMNT($db,$mant_id);
                $attachments = null;

                switch($tipo){
                    case 'INFORME_AGREGADO':{
                        $info_id = $data['info_id'];

                        $res = $db->ExecuteQuery("SELECT
                                                    repo_ruta
                                                    FROM
                                                    repositorio
                                                    WHERE
                                                    repo_nombre LIKE 'MNT-$mant_id%' AND repo_tipo_doc='INFORME' AND repo_tabla='informe' AND repo_tabla_id=$info_id
                                                    ORDER BY repo_fecha_creacion DESC
                                                    LIMIT 1");
                        if($res['status']==0 || $res['rows']==0){
                            return $res;
                        }

                        global $BASEDIR;
                        $attachments = array();
                        foreach($res['data'] as $row){
                            array_push($attachments,$BASEDIR.$row['repo_ruta']);
                        }
                        break;
                    }

                }

                   $res = SendEmail($EMAIL_FROM,$to,$title,$message,$attachments);
                   if($res['status']==1){
                    Loggear("Se envio email a ".$to." para notificar ".$modulo.":".$tipo);
                }
                else{
                    Loggear("Error enviando email: ".$res['error'],LOG_ERR);
                }
                break;
            }

            case 'INSP':{
                $insp_id = $id_relacionado;

                $res = $db->ExecuteQuery("SELECT
                                                usua_correo_electronico
                                            FROM
                                                rel_contrato_usuario_notificacion,
                                                rel_contrato_usuario,
                                                inspeccion,
                                                usuario
                                            WHERE
                                            inspeccion.insp_id = $insp_id AND
                                            usuario.usua_id = $u AND
                                            rel_contrato_usuario.cont_id = inspeccion.cont_id AND
                                            rel_contrato_usuario.usua_id =  usuario.usua_id AND
                                            rel_contrato_usuario_notificacion.recu_id = rel_contrato_usuario.recu_id AND
                                            rel_contrato_usuario_notificacion.modu_alias = '$modulo' AND
                                            '$tipo' LIKE rel_contrato_usuario_notificacion.rcun_evento AND
                                            rel_contrato_usuario_notificacion.rcun_accion = 'ENVIAR_EMAIL'");
                if($res['status']==0 || $res['rows']==0){
                    return $res;
                }

                $to      = $res['data'][0]['usua_correo_electronico'];
                $title   = "Inspeccion Nº ".$insp_id.": ".$tipo;
                $message = ObtenerInformacionINSP($db,$insp_id);
                $attachments = null;

                switch($tipo){
                    case 'INFORME_AGREGADO':{
                        $info_id = $data['info_id'];

                        $res = $db->ExecuteQuery("SELECT
                                                    repo_ruta
                                                    FROM
                                                    repositorio
                                                    WHERE
                                                    repo_nombre LIKE 'INSP-$insp_id%' AND repo_tipo_doc='INFORME' AND repo_tabla='informe' AND repo_tabla_id=$info_id
                                                    ORDER BY repo_fecha_creacion DESC
                                                    LIMIT 1");
                        if($res['status']==0 || $res['rows']==0){
                            return $res;
                        }

                        global $BASEDIR;
                        $attachments = array();
                        foreach($res['data'] as $row){
                            array_push($attachments,$BASEDIR.$row['repo_ruta']);
                        }
                        break;
                    }

                }

                $res = SendEmail($EMAIL_FROM,$to,$title,$message,$attachments);
                if($res['status']==1){
                    Loggear("Se envio email a ".$to." para notificar ".$modulo.":".$tipo);
                } else {
                    Loggear("Error enviando email: ".$res['error'],LOG_ERR);
                }
                break;
            }
        }
    }
}


function AgregarInventario($db,$info_id){

    //se obtienen los datos de relacion entre informe y formulario respuesta
    $res = $db->ExecuteQuery("SELECT
                                rifr.fore_id,
                                fr.form_id
                                FROM rel_informe_formulario_respuesta rifr,formulario_respuesta fr
                                WHERE rifr.info_id = $info_id AND rifr.fore_id=fr.fore_id");
    if($res['status']==0 || $res['rows']==0){
        return $res;
    }
    $forms = $res['data'];

    //print_r($forms);

    //se obtienen los datos de inventario definicion formulario y se ve si es inventariable
    $inventariable = false;

    foreach ($forms as $key => &$form) {
        $form_id = $form['form_id'];
        $foits   = array();

        $res = $db->ExecuteQuery("SELECT
                                    fogr_id,
                                    foit_id,
                                    indf_genero,
                                    indf_especie,
                                    indf_campo,
                                    indf_codigo
                                    FROM inventario_definicion_formulario
                                    WHERE form_id=$form_id");
        if($res['status']==0){
            return $res;
        }
        if(0<$res['rows']){
            if(!isset($form['inventario'])){
                $form['inventario_elemento'] = array();
                $form['inventario_elemento_codigo'] = array();
                $form['inventario_elemento_foits'] = array();
            }
            foreach ($res['data'] as $row) {
                if(!isset($form['inventario_elemento'][$row['fogr_id']])){
                    $form['inventario_elemento'][$row['fogr_id']] = array();
                }

                 array_push($form['inventario_elemento'][$row['fogr_id']],$row);

                if($row['indf_codigo']==1){
                    $form['inventario_elemento_codigo'][$row['fogr_id']] = $row['foit_id'];
                }
                array_push($form['inventario_elemento_foits'],$row['foit_id']);
            }

            $inventariable = true;
        }
        else{
            unset($forms[$key]);
        }
    }

    //echo "inventariable: ".$inventariable."\n";

    if($inventariable){
        //print_r($forms);

        //obtener modulo
        $res = $db->ExecuteQuery("SELECT info_modulo,info_id_relacionado FROM informe where info_id=".$info_id);
        if($res['status']==0 || $res['rows']==0){
            return $res;
        }

        $info_modulo         = $res['data'][0]['info_modulo'];
        $info_id_relacionado = $res['data'][0]['info_id_relacionado'];


        //Se obtiene emplazamiento dependiendo si es mantenimiento o orden servicio o inspeccion
        $query = "";
        if($info_modulo=='OS'){
            $query = "SELECT cont_id,empl_id FROM orden_servicio WHERE orse_id=$info_id_relacionado";
        }
        else if($info_modulo=='MNT'){
            $query = "SELECT cont_id,empl_id FROM mantenimiento WHERE mant_id=$info_id_relacionado";
        }
        else if($info_modulo=='INSP'){
            $query = "SELECT cont_id,empl_id FROM inspeccion WHERE insp_id=$info_id_relacionado";
        }
        else{
            return array("status"=>true);
        }

        $res = $db->ExecuteQuery($query);
        if($res['status']==0 || $res['rows']==0){
            return $res;
        }
        $cont_id = $res['data'][0]['cont_id'];
        $empl_id = $res['data'][0]['empl_id'];


        //obtenemos el inventario
        $res=$db->ExecuteQuery(" SELECT inve_id FROM inventario WHERE cont_id=$cont_id");
        if($res['status']==0 || $res['rows']==0){
            return $res;
        }

        $inve_id = $res['data'][0]['inve_id'];

        //echo $info_modulo.",".$info_id_relacionado.",".$cont_id.",".$empl_id.",".$inve_id."\n";

        //obtenemos valores
        foreach ($forms as &$form) {
            //Obtener valores
            $fore_id = $form['fore_id'];
            $foits   = implode(",", $form['inventario_elemento_foits']);
            $valores = array();

            $res=$db->ExecuteQuery("SELECT foit_id,fova_valor
                                    FROM formulario_valor
                                    WHERE fore_id=$fore_id AND foit_id IN ($foits)");
            if($res['status']==0){
                return $res;
            }

            foreach ($res['data'] as $row) {
                $valores[$row['foit_id']] = $row['fova_valor'];
            }

            //print_r($valores);

            foreach($form['inventario_elemento'] as $pagina => $elemento){
                $codigo = "";
                if(isset($form['inventario_elemento_codigo'][$pagina]) &&
                   isset($valores[$form['inventario_elemento_codigo'][$pagina]])){
                    $codigo = $valores[$form['inventario_elemento_codigo'][$pagina]];
                }

                //echo $codigo."\n";
                if($codigo!=""){
                    //Poniendo NOACTIVO registro anterior
                    $res=$db->ExecuteQuery("UPDATE inventario_elemento
                                            SET inel_estado='NOACTIVO'
                                            WHERE inel_codigo='$codigo'");
                    if($res['status']==0){
                        return $res;
                    }
                }

                //agrego elemento nuevo
                 $res = $db->ExecuteQuery("INSERT INTO inventario_elemento SET
                            inve_id = $inve_id,
                            empl_id = $empl_id,
                            inel_codigo = '$codigo',
                            inel_fecha = NOW(),
                            inel_estado = 'ACTIVO',
                            inel_fuente_modulo = '$info_modulo',
                            inel_fuente_id_relacionado = $info_id_relacionado");
                if($res['status']==0){
                    return $res;
                }

                $inel_id = $res['data'][0]['id'];


                //Agregar características
                foreach ($elemento as $c) {
                    if($c['indf_codigo']=='0'){
                        $inec_genero  = $c['indf_genero'];
                        $inec_especie = $c['indf_especie'];
                        $inec_campo   = $c['indf_campo'];
                        $inec_valor   = $valores[$c['foit_id']];

                        $res = $db->ExecuteQuery("INSERT INTO inventario_elemento_caracteristica SET
                                inel_id = $inel_id,
                                inec_genero = '$inec_genero',
                                inec_especie = '$inec_especie',
                                inec_campo = '$inec_campo',
                                inec_valor = '$inec_valor'");
                        if ($res['status'] == 0) {
                            return $res;
                        }
                    }
                }
            }
        }

        return $res;
    }
    return array("status"=>true);

}

?>


 <?php
include("db.php");

global $DEF_CONFIG;

//Cargar el primer día del mes en que empieza el período
$FECHA      = isset($argv[1])?$argv[1]:date("Y-m-d");

$DIAS_FECHA_PROGRAMADA = isset($DEF_CONFIG['mnt']['diasFechaProgramada'])?$DEF_CONFIG['mnt']['diasFechaProgramada']:15; //dias de la fecha programada luego de la fecha de inicio del periodo
$FORM_INGRESO 		   = isset($DEF_CONFIG['mnt']['formIngreso'])?$DEF_CONFIG['mnt']['formIngreso']:1; //id de formulario de ingreso
$FORM_SALIDA           = isset($DEF_CONFIG['mnt']['formSalida'])?$DEF_CONFIG['mnt']['formSalida']:4; //id de formulario de salida
$FORM_SITUACION        = isset($DEF_CONFIG['mnt']['formSituacion'])?$DEF_CONFIG['mnt']['formSituacion']:2; //id de formulario de situacion
$ESTADO_CANCELADA      = isset($DEF_CONFIG['mnt']['estadoMPPCancelado'])?$DEF_CONFIG['mnt']['estadoMPPCancelado']:'NO REALIZADO'; //estado en que queda MPP al cancelar

echo 'FECHA: '.$FECHA."\n";
echo 'DIAS_FECHA_PROGRAMADA: '.$DIAS_FECHA_PROGRAMADA."\n";
echo 'FORM_INGRESO: '.$FORM_INGRESO."\n";
echo 'FORM_SALIDA'.$FORM_SALIDA."\n";
echo 'ESTADO_CANCELADA'.$ESTADO_CANCELADA."\n";

//____________________________________________________________________________________
//Obtener período(s)
function ObtenerPeriodos($db,$fecha){
	echo 'ObtenerPeriodos'."\n";
	echo 'fecha : '.$fecha."\n";
	
	
	
	
    $contracts = array();
    $res = $db->ExecuteQuery("SELECT
                                        cont_id,
                                        cont_nombre,
                                        DATE(cont_fecha_inicio) AS cont_fecha_inicio
                              FROM contrato
                              WHERE cont_estado='ACTIVO'
							        AND cont_id = 29
                            ");
    if($res['status']){
        if(0<$res['rows']){
            $contracts = $res['data'];
			print_r ($res['data']);
        } else {
            return array("status"=>false,"error"=>"No se han definido contratos");
        }
    } else {
        return $res;
    }

    #$data = array("add"=>array(),"del"=>array());
	$data = array("add"=>array());
    foreach($contracts as $c){
        $cont_id = $c['cont_id'];

        $periods = array();

        $query ="SELECT
                        periodicidad.peri_id,
                        periodicidad.peri_nombre,
                        periodicidad.peri_meses,
                        DATE_ADD(NOW(),INTERVAL -1 DAY) fecha_inicio,
                        DATE_ADD(DATE_ADD(NOW(),INTERVAL -1 DAY), INTERVAL 1 MONTH ) fecha_termino
						
                FROM
                        periodicidad
                INNER JOIN rel_contrato_periodicidad ON (rel_contrato_periodicidad.cont_id=$cont_id 
                                                            AND periodicidad.peri_id = rel_contrato_periodicidad.peri_id  
                                                            AND rel_contrato_periodicidad.rcpe_participa_mpp='1')
                WHERE peri_meses IS NOT NULL
				AND peri_meses > 0";
		echo("********************\n");
		echo($query."\n");
		echo("********************\n");
		$res = $db->ExecuteQuery($query);
        if($res['status']){
            if(0<$res['rows']){
                $periods = $res['data'];
            } else {
                continue;
            }
        } else {
            return $res;
        }

        foreach($periods AS $p){
			#$fecha_inicio = new DateTime($c['cont_fecha_inicio']);
			#$fecha_inicio = $fecha;
			
			
			#$fecha_termino = strtotime ( '+1 month' , strtotime ( $fecha_inicio ) ) ;
			#$fecha_termino = date("Y-m-d" , $fecha_termino );

			#$result = $fecha_inicio->format('Y-m-d H:i:s');
			#echo $result;
			#$result = $fecha_termino->format('Y-m-d H:i:s');
			#echo $result;
			#echo "\n******************";
			
			/*array_push($data['add'],array("cont_id"=>$cont_id,
										  "peri_id"=>$p['peri_id'],
										  "peri_nombre"=>$p['peri_nombre'],
										  "peri_mes"=>$p['peri_meses'],
										  "fecha_inicio"=>$fecha_inicio->format('Y-m-d'),
										  "fecha_termino"=>$fecha_termino->format('Y-m-d')));
			*/
			array_push($data['add'],array("cont_id"=>$cont_id,
										  "peri_id"=>$p['peri_id'],
										  "peri_nombre"=>$p['peri_nombre'],
										  "peri_mes"=>$p['peri_meses'],
										  "fecha_inicio"=>$p['fecha_inicio'],
										  "fecha_termino"=>$p['fecha_termino']));										  
		}

	}
    return array("status"=>true,"data"=>$data);
}


//____________________________________________________________________________________
//Cargar MPPs
function CargarMPPs($db,$cont_id,$peri_id,$peri_mes,$fecha_inicio,$fecha_termino,$fecha_programada,$form_ingreso,$form_salida,$form_situacion,$fecha){
	echo 'CargarMPPs'."\n";
	echo 'cont_id'.$cont_id."\n";
	echo 'peri_id'.$peri_id."\n";
	echo 'peri_mes'.$peri_mes."\n";
	echo 'fecha_inicio'.$fecha_inicio."\n";
	echo 'fecha_termino'.$fecha_termino."\n";
	echo 'fecha_programada'.$fecha_programada."\n";
	echo 'form_ingreso'.$form_ingreso."\n";
	echo 'form_salida'.$form_salida."\n";
	echo 'form_situacion'.$form_situacion."\n";
	
	

	$mes_de_carga = substr($fecha, 5, 2);
	echo 'mes de carga : '.$mes_de_carga."\n";

	#exit;
    //Recupera lista de empresas
    $empresas = array();
    $res = $db->ExecuteQuery("SELECT
                                    zona_id,
                                    empr_id
                                FROM rel_contrato_empresa
                                WHERE cont_id=$cont_id 
								AND coem_tipo='CONTRATISTA' 
								AND coem_participa_mpp=1");
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                if($row['zona_id']==NULL || $row['zona_id']==""){
                    $empresas["%"] = $row['empr_id'];
                } else {
                    $empresas[$row['zona_id']] = $row['empr_id'];
                }
            }
        } else {
            return array("status"=>false,"error"=>"No se han definido empresa contratistas participantes de MPP en contrato $cont_id");
        }
    } else {
        return $res;
    }


    //Datos de periodo
    $rcpe_id = 0;
    $rcpe_dias_apertura=0;
    $rcpe_dias_post_cierre=0;
    $res = $db->ExecuteQuery("SELECT rcpe_id,rcpe_dias_apertura,rcpe_dias_post_cierre
                                FROM rel_contrato_periodicidad
                                WHERE cont_id='$cont_id' 
								AND peri_id='$peri_id'");
    if($res['status']){
        if(0<$res['rows']){
            $rcpe_id 				= $res['data'][0]['rcpe_id'];
            $rcpe_dias_apertura 	= $res['data'][0]['rcpe_dias_apertura'];
            $rcpe_dias_post_cierre 	= $res['data'][0]['rcpe_dias_post_cierre'];
			
			echo 'rcpe_id : '.$rcpe_id."\n";
			echo 'rcpe_dias_apertura : '.$rcpe_dias_apertura."\n";
			echo 'rcpe_dias_post_cierre : '.$rcpe_dias_post_cierre."\n";
        } else {
            return array("status"=>false,"error"=>"Periodo no habilitado en contrato $cont_id");
        }
    } else {
        return $res;
    }

    //Recupera los formularios asociados respecto al mes de carga 
    $forms = array();
    /*$query = "  SELECT  clpr_id
                        ,espe_id
                        ,form_id
                        ,madf_mes_carga
                FROM mantenimiento_definicion_formulario 
                WHERE cont_id=$cont_id 
                        #AND peri_id='$peri_id' 
                        AND madf_aplica_mes=1
						AND madf_mes_carga = $mes_de_carga
						AND mant_estado='ACTIVO'";
    $res = $db->ExecuteQuery($query);
    $madf_aplica_mes=0;
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                $now = date("n", strtotime($fecha_inicio));
				echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n";
				echo "\n".$now."\n" ;
				echo "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n";
				#exit;
                #if($now == $madf_mes_carga){
                    if(!isset($forms[$row['clpr_id']][$row['espe_id']])){
                        $forms[$row['clpr_id']][$row['espe_id']] = array();
                    }
                    array_push($forms[$row['clpr_id']][$row['espe_id']],$row['form_id']);
                #}
            }
        }
    }
	*/
    
    $query = "  SELECT clpr_id
						,espe_id
						,form_id
						,madf_mes_carga
                FROM mantenimiento_definicion_formulario 
                WHERE cont_id=$cont_id 
                AND peri_id='$peri_id' 
                AND madf_aplica_mes=1
				AND madf_mes_carga = $mes_de_carga
				AND madf_estado='ACTIVO'";
    echo "\n QUERY:";
    echo $query."\n";
    echo "\n";
    $res = $db->ExecuteQuery($query);
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                if(!isset($forms[$row['clpr_id']][$row['espe_id']])){
                    $forms[$row['clpr_id']][$row['espe_id']] = array();
                }
                array_push($forms[$row['clpr_id']][$row['espe_id']],$row['form_id']);
            }
        } 
    } else {
        return $res;
    }
	

    //Recupera los emplazamientos que deben de tener mantenimientos para 
    $emplazamientos = array();
    $clpr_ids = implode("','",array_keys($forms));
    $res = $db->ExecuteQuery("SELECT emplazamiento.empl_id,clas_id,rel_contrato_emplazamiento.clpr_id,empl_nombre,zona.zona_id
                              FROM emplazamiento
                              INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id=emplazamiento.empl_id)
                              INNER JOIN clasificacion_programacion ON (clasificacion_programacion.clpr_id=rel_contrato_emplazamiento.clpr_id 
																		AND clasificacion_programacion.clpr_activo='ACTIVO')
                              LEFT JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)
                              LEFT JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id and zona.cont_id =rel_contrato_emplazamiento.cont_id)
                              WHERE emplazamiento.empl_estado='ACTIVO' 
							  AND rel_contrato_emplazamiento.cont_id=$cont_id 
							  AND rel_contrato_emplazamiento.rece_mpp=1  
							  AND rel_contrato_emplazamiento.clpr_id IN ('$clpr_ids')  
							  AND zona.zona_tipo='CONTRATO'");
    if($res['status']){
        if(0<$res['rows']){
            $emplazamientos	= $res['data'];
        } else {
            return array("status"=>false,"error"=>"Sin emplazamientos definidos");
        }
    } else {
        return $res;
    }

    //crear mantenimiento período..
    $res = $db->ExecuteQuery("INSERT INTO mantenimiento_periodos SET
                             rcpe_id='$rcpe_id',
                             mape_fecha_pre_apertura = DATE_ADD('$fecha_inicio', INTERVAL - $rcpe_dias_apertura DAY),
                             mape_fecha_inicio = '$fecha_inicio',
                             mape_fecha_cierre = '$fecha_termino',
                             mape_fecha_post_cierre = DATE_ADD('$fecha_termino', INTERVAL $rcpe_dias_post_cierre DAY),
                             mape_estado = 'PROCESANDO'");
    if(!$res['status']){
        return $res;
    }
    $mape_id = $res['data'][0]['id']; #Corresponde al mantenimiento periodo del cual se va a realizar la carga

    //Generando MPPs
    $mpps_creados=0;
	echo 'Generando MPPs'."\n";
    foreach ($emplazamientos as $emplazamiento) {
		
        $empl_nombre 	= $emplazamiento['empl_nombre'];
        $empl_id 		= $emplazamiento['empl_id'];
        $clas_id 		= $emplazamiento['clas_id'];
        $clpr_id 		= $emplazamiento['clpr_id'];
        $zona_id 		= $emplazamiento['zona_id'];
        $empr_id 		= NULL;

        if(isset($empresas[$zona_id])){
            $empr_id = $empresas[$zona_id];
        } else{
            $empr_id = $empresas["%"];
        }

        if($empr_id==NULL){
            continue;
        }

		echo 'empl_nombre : '.$empl_nombre."\n";
		echo 'empl_id : '.$empl_id."\n";
        echo 'clas_id : '.$clas_id."\n";
        echo 'clpr_id : '.$clpr_id."\n";
        echo 'zona_id : '.$zona_id."\n";
        echo 'empr_id : '.$empr_id."\n";
        foreach($forms[$clpr_id] as $espe_id => $f){
			
			echo 'espe_id : '.$espe_id."\n";
			echo 'rcpe_id : '.$rcpe_id."\n";

            //Obtener ultima fecha de realización o fecha programada
            $query = "SELECT  IF(mant_fecha_ejecucion,DATEDIFF(DATE(mant_fecha_ejecucion),mape_fecha_inicio),DATEDIFF(DATE(mant_fecha_programada),mape_fecha_inicio)) AS days
                                            FROM mantenimiento
                                            INNER JOIN mantenimiento_periodos ON (mantenimiento.mape_id=mantenimiento_periodos.mape_id)
                                            WHERE  mantenimiento.empl_id='$empl_id' 
                                            AND    mantenimiento.espe_id='$espe_id' 
                                            AND    mantenimiento_periodos.rcpe_id='$rcpe_id'
                                            ORDER BY mantenimiento.mape_id DESC LIMIT 1";

			echo("********************\n");
			echo($query."\n");
			echo("********************\n");
		
			$res = $db->ExecuteQuery($query);
            if(!$res['status']){
                return $res;
            }
            if(0<$res['rows']){
                $days = $res['data'][0]['days'];

                $fi = new DateTime($fecha_inicio);
                $ft = new DateTime($fecha_termino);

                $fi->add(new DateInterval("P".$days."D"));
                if($fi < $ft){
                    $fecha_programada = $fi->format('Y-m-d');
                }
                else{
                    $fecha_programada = $ft->format('Y-m-d');
                }
            }

            //MODIFICACION MTTO


            //crear mantenimiento..
            $res = $db->ExecuteQuery("INSERT INTO mantenimiento SET
                                     empr_id='$empr_id',
                                     cont_id='$cont_id',
                                     mape_id='$mape_id',
                                     empl_id='$empl_id',
                                     espe_id='$espe_id',
                                     mant_fecha_programada='$fecha_programada',
                                     clas_id='$clas_id',
                                     clpr_id='$clpr_id'");
            if(!$res['status']){
                return $res;
            }
            $mant_id = $res['data'][0]['id'];

            
			$values = "";
			$values = "($mant_id,$form_ingreso),($mant_id,$form_situacion),";
			
            foreach ($f as $form_id) {
                $values .= "($mant_id,$form_id),";
            }
			//if ($espe_id == $espe_aseo) {
			$values .= "($mant_id,$form_salida)";$coma = substr($values, -1);
			
			$coma = substr($values, -1);
			if ("," ==$coma) {
				$values = substr($values, 0, -1 );
			}
			

            //crear mantenimiento formularios..
            $res = $db->ExecuteQuery("INSERT INTO rel_mantenimiento_formulario (mant_id,form_id) VALUES $values");
            if(!$res['status']){
                return $res;
            }
            $mpps_creados++;
    } 
}

    return array("status"=>true,"mpps"=>$mpps_creados);
}

//____________________________________________________________________________________
//Cancelar MPPs
function CancelarMPPs($db,$cont_id,$peri_id,$fecha_inicio,$estado){
	echo 'CancelarMPPs'."\n";
	echo 'cont_id : '.$cont_id."\n";               /* Contrato correspondiente de los mantenimientos a cancelar*/
	echo 'peri_id : '.$peri_id."\n";               /* ID del periodo a cancelar*/
	echo 'fecha_inicio : '.$fecha_inicio."\n";     /* Fecha de inicio usado para */
	echo 'estado : '.$estado."\n";                 /* Estado en el cual el MNT se considera cancelado*/
	
    //Datos de periodo
    $rcpe_id = 0;
    $res = $db->ExecuteQuery("SELECT rcpe_id
                                FROM rel_contrato_periodicidad 
                                WHERE cont_id='$cont_id' 
                                AND peri_id='$peri_id'
    ");
    if($res['status']){
        if(0<$res['rows']){
            $rcpe_id = $res['data'][0]['rcpe_id'];
			echo 'rcpe_id : '.$rcpe_id."\n";
        } else {
            return array("status"=>false,"error"=>"Periodo no habilitado en contrato $cont_id");
        }
    } else {
        return $res;
    }

    //Buscar mantenimiento_periodo
    $mape_ids=array();
    $res = $db->ExecuteQuery("SELECT mape_id
                              FROM mantenimiento_periodos
                              WHERE rcpe_id='$rcpe_id'
							  AND mape_estado <> 'CERRADO'");
    if($res['status']){
        if(0<$res['rows']){
            foreach($res['data'] as $row){
                array_push($mape_ids,$row['mape_id']);
            }
        } else {
            return array("status"=>false,"error"=>"No se ha encontrado mantenimiento_periodo");
        }
    } else {
        return $res;
    }

    foreach ($mape_ids as $mape_id) {
		echo 'mape_id : '.$mape_id."\n";
        $res = $db->ExecuteQuery("UPDATE mantenimiento_periodos SET mape_estado='CERRADO' WHERE mape_id='$mape_id'");
        if(!$res['status']){
            return $res;
        }

        $res = $db->ExecuteQuery("UPDATE mantenimiento mnt
                                  SET mnt.mant_estado='$estado'
                                  WHERE mnt.mape_id='$mape_id' 
                                  AND mnt.mant_estado NOT IN ('APROBADA','RECHAZADA','ANULADA','NO REALIZADO')
								  AND mnt.mant_id NOT IN   (SELECT info.info_id_relacionado 
                                                            FROM informe info 
                                                            WHERE info.info_modulo='MNT' 
																AND info.info_id_relacionado = mnt.mant_id
																AND info.info_estado IN ('SINVALIDAR', 'PREAPROBADO')
														 )
		");
        if(!$res['status']){
            return $res;
        }
    }
    return array("status"=>true);
}

//____________________________________________________________________________________
//MAIN
echo 'MAIN';
$db = new MySQL_Database();
$res = ObtenerPeriodos($db,$FECHA);
if($res['status']){
    $db->startTransaction();

    $mpp_total = 0;
    $periodos_a_cargar   = $res['data']['add'];
    //$periodos_a_cancelar = $res['data']['del'];

    foreach ($periodos_a_cargar as $periodo) {
        /*
        echo "Cancelando período ".$periodo['peri_nombre']."\n";
        
        $res = CancelarMPPs($db, 
                          $periodo['cont_id'],
                          $periodo['peri_id'],
                          $periodo['fecha_inicio'],
                          $ESTADO_CANCELADA);
        if(!$res['status']){
            //$db->Rollback();
            //$db->Disconnect();
            echo $res['error']."\n";
            //exit(1);
        }
        */
        echo "Cargando período ".$periodo['peri_nombre']." (".$periodo['peri_mes'].") a iniciar en ".$periodo['fecha_inicio']." al ".$periodo['fecha_termino']."\n";

        $fecha_inicio = new DateTime($periodo['fecha_inicio']);
        $fecha_inicio->add(new DateInterval("P".$DIAS_FECHA_PROGRAMADA."D"));
        $fecha_programada = $fecha_inicio->format('Y-m-d');
		
		#echo "fecha_inicio : ".$fecha_inicio."\n";
        #echo "fecha_programada : ".$fecha_programada."\n";
		$res = CargarMPPs($db,
                          $periodo['cont_id'],
                          $periodo['peri_id'],
                          $periodo['peri_mes'],
                          $periodo['fecha_inicio'],
                          $periodo['fecha_termino'],
                          $fecha_programada,
                          $FORM_INGRESO,
                          $FORM_SALIDA,
						  $FORM_SITUACION,
						  $FECHA);
        if(!$res['status']){
            //$db->Rollback();
            //$db->Disconnect();
            echo $res['error']."\n";
            //echo "Advertencia: No se han cargado ni cancelado MPPs\n\n";
            //exit(1);
        } else{
            $mpp = isset($res['mpps'])?$res['mpps']:0;
            echo $mpp." MPPs cargados\n\n";
            $mpp_total += intval($mpp);
        }
    }

    /*
    foreach ($periodos_a_cancelar as $periodo) {
        echo "Cancelando período ".$periodo['peri_nombre']." iniciado en ".$periodo['fecha_inicio']."\n";

        $res = CancelarMPPs($db,
                          $periodo['cont_id'],
                          $periodo['peri_id'],
                          $periodo['fecha_inicio'],
                          $ESTADO_CANCELADA);
        if(!$res['status']){
            //$db->Rollback();
            //$db->Disconnect();
            echo $res['error']."\n";
            //exit(1);
        }
    }
    */

    $db->Commit();//$db->Rollback();

    echo $mpp_total." MPPs cargados en total\n\n";
}
else{
    echo $res['error'];
}

$db->Disconnect();
exit(0);
?>

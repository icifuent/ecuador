 <?php
include("db.php");

echo "Actualizacion periodica Estado";

$db = new MySQL_Database();

global $DEF_CONFIG;
$id_form_subida_servicio = $DEF_CONFIG['sla']['idFormularioSubidaServicio'];

//________________________________________________
echo "\n Actualizacion tabla mnt_consolidado";

	$db->startTransaction();

	$res_cont = $db->ExecuteQuery("
										SELECT count(1) as contador from mantenimiento
												WHERE
				    					mant_consolidado = '0'
										
		");
		
	echo "\n Count realizado";
		
	if($res_cont['status']==0){
		echo "\n".$res_cont['error'];
		$db->Rollback();
		exit(1);
	}
	
	$db->Commit();
	
	$contador = ceil($res_cont['data'][0]['contador']/100);
	
	echo "\n Contador = ".$contador;
	
	while($contador!=0){
		echo "\n Entra al while";
		$contador=$contador-1;
		echo "\n Contador = ".$contador;
		
		//Marcamos las que usaremos para la actualizacion
		
		$db->startTransaction();
		
		$res = $db->ExecuteQuery("
		UPDATE mantenimiento
		SET mant_consolidado = '2'

		WHERE
							mantenimiento.mant_consolidado = '0'
							AND mant_estado IN ('RECHAZADA','APROBADA','NOACTIVO','ANULADA')   
							Limit 100
		;
		");
		if($res['status']==0){
			$db->Rollback();
			echo "\n".$res['error'];
			exit(1);
		}
		
		$db->Commit();

		//Realizamos el INSERT mnt_consolidado
		
		$db->startTransaction();
		
		$res = $db->ExecuteQuery("
		INSERT INTO mnt_consolidado (
	  mant_id
    , cont_id
    , cont_nombre
    , peri_id
    , peri_nombre
    , espe_id
    , espe_nombre
    , empr_id
    , empr_nombre
    , emvi_id
    , emvi_fecha_ingreso
    , emvi_fecha_salida
    , usua_asignado_id
    , usua_asignado_nombre
    , usua_creador_id
    , usua_creador_nombre
    , usua_validador
    , mape_id
    , tmpr_id
    , empl_id
    , empl_nombre
    , empl_direccion
    , empl_macrositio
    , empl_subtel
    , empl_requiere_acceso
    , empl_permisos_acceso
    , duto_id
    , duto_nombre
    , regi_nombre
    , clas_id
    , clas_nombre
    , clpr_id
    , clpr_nombre
    , mant_fecha_programada
    , mant_fecha_ejecucion
    , tare_fecha_despacho
    , mant_estado
    , mant_estado_proceso
    , mant_responsable
    , mant_fecha_validacion
    , mant_descripcion
    , mant_sla_incluida
    , mant_observacion
    , zona_movistar
    , zona_contrato
    , cluster
    , responsable_emp
	, comu_id
	, comu_nombre
	, empl_nemonico
	, empl_espacio
	, empl_observacion
	, empl_tecnologia
	, mant_consolidado_actualizar
							)
   SELECT distinct
			mantenimiento.mant_id,
            mantenimiento.cont_id,
            contrato.cont_nombre,
            periodicidad.peri_id,
			peri_nombre,
            mantenimiento.espe_id,
			espe_nombre,
            mantenimiento.empr_id,
			empr_nombre,
            emplazamiento_visita.emvi_id,
			emplazamiento_visita.emvi_fecha_ingreso,
			emplazamiento_visita.emvi_fecha_salida,
			usuario_asignado.usua_id,
			usuario_asignado.usua_nombre,
			mantenimiento.usua_creador,
			usuario_creador.usua_nombre,
            mantenimiento.usua_validador,
            mantenimiento_periodos.mape_id,
            tmpr_id,
			emplazamiento.empl_id,
			empl_nombre,
			empl_direccion,
			empl_macrositio,
			empl_subtel,
			IF(empl_observacion_ingreso=NULL,'NO','SI') AS 'Requiere Acceso EMP.',
            TRIM(REPLACE(REPLACE(REPLACE(empl_observacion_ingreso, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'Permisos de acceso EMP.',
            emplazamiento.duto_id,
            duto_nombre AS 'Propietario Torre EMP.',
            regi_nombre,
            mantenimiento.clas_id,
            clas_nombre,
            clasificacion_programacion.clpr_id,
			clpr_nombre,
			mantenimiento.mant_fecha_programada,
            mantenimiento.mant_fecha_ejecucion,
        	(
        		SELECT 
        			MAX(tare_fecha_despacho) as tare_fecha_despacho
				FROM tarea 
			 	WHERE tare_modulo='MNT' 
			 	AND tare_id_relacionado=mantenimiento.mant_id
		 	) AS 'Fecha proceso MNT',
            mant_estado,
			IF(mant_estado IN ('NOACTIVO', 'ANULADA'),'ANULADO', IF(mant_estado IN ('RECHAZADA','APROBADA','NO REALIZADO'),'FINALIZADO','ACTIVO')) AS 'Estado MNT',
			mantenimiento.mant_responsable,
            mantenimiento.mant_fecha_validacion,
			mantenimiento.mant_descripcion,
            mantenimiento.mant_sla_incluida,
            TRIM(REPLACE(REPLACE(REPLACE(mantenimiento.mant_observacion, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'MNT Observacion',
			(
				SELECT 
					zona_nombre
				FROM rel_zona_emplazamiento 
				INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR')
				WHERE rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
				LIMIT 1
			) AS 'Zona MOVISTAR',
			(
				SELECT 
					zona_nombre
				FROM rel_zona_emplazamiento 
				INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO')
				WHERE rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
				LIMIT 1
			) AS 'ZONA DE CONTRATO CIM',
			(
				SELECT 
					zona_nombre
				FROM rel_zona_emplazamiento 
				INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER')
				WHERE	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
				LIMIT 1
			) AS 'Cluster',
			(
				SELECT 
				usua_nombre
				FROM rel_zona_emplazamiento
				INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
				INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
				INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_cargo='GESTOR')
				WHERE rel_zona_emplazamiento.empl_id=emplazamiento.empl_id
                LIMIT 1
			) AS 'Responsable EMP.'
				, comuna.comu_id
				, comuna.comu_nombre
				, empl_nemonico
				, empl_espacio
				, TRIM(REPLACE(REPLACE(REPLACE(empl_observacion, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'EMPL Observacion',
			(
                select group_concat(c.tecn_nombre)
                from tecnologia c 
                , rel_emplazamiento_tecnologia b
                where  b.empl_id = emplazamiento.empl_id
                AND c.tecn_id = b.tecn_id 
                GROUP BY b.empl_id                
			) AS 'Tecnologia',
			0 AS 'mant_consolidado_actualizar'
			FROM 	contrato
						
			INNER JOIN mantenimiento ON (mantenimiento.cont_id = contrato.cont_id)
			INNER JOIN empresa ON (empresa.empr_id = mantenimiento.empr_id) 
			INNER JOIN especialidad ON (especialidad.espe_id = mantenimiento.espe_id)   
			INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
			INNER JOIN rel_contrato_periodicidad ON (rel_contrato_periodicidad.rcpe_id = mantenimiento_periodos.rcpe_id) 
			INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)	
			LEFT JOIN rel_mantenimiento_asignacion_usuario ON 
						(rel_mantenimiento_asignacion_usuario.maas_id = (SELECT maas_id 
								FROM mantenimiento_asignacion 
								WHERE mantenimiento_asignacion.mant_id = mantenimiento.mant_id 
								ORDER BY maas_id DESC LIMIT 1) AND rmau_tipo='JEFECUADRILLA')  
			LEFT JOIN usuario AS usuario_asignado ON (usuario_asignado.usua_id=rel_mantenimiento_asignacion_usuario.usua_id)
            LEFT JOIN usuario AS usuario_creador ON (usuario_creador.usua_id=mantenimiento.usua_creador)
			INNER JOIN emplazamiento ON (emplazamiento.empl_id = mantenimiento.empl_id)         
			INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = mantenimiento.clas_id)
			INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
			INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
			INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
			INNER JOIN region ON (region.regi_id = provincia.regi_id)
			INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id = mantenimiento.empl_id 
						AND rel_contrato_emplazamiento.cont_id = mantenimiento.cont_id )
			INNER JOIN clasificacion_programacion ON (clasificacion_programacion.clpr_id = rel_contrato_emplazamiento.clpr_id AND clpr_activo = 'ACTIVO')
			INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=emplazamiento.empl_id)
			INNER JOIN zona AS zona_alcance ON (rel_zona_emplazamiento.zona_id = zona_alcance.zona_id 
						AND zona_alcance.zona_tipo='ALCANCE')
			
			LEFT JOIN emplazamiento_visita ON 
						(emplazamiento_visita.emvi_id = (SELECT emvi_id  
											FROM emplazamiento_visita 
											WHERE emvi_modulo='MNT' 
											AND emvi_id_relacionado=mantenimiento.mant_id 
											AND emplazamiento_visita.empl_id=mantenimiento.empl_id 
											ORDER BY emvi_id DESC LIMIT 1))
		WHERE 
					mantenimiento.mant_consolidado = '2'
					AND mant_estado IN ('RECHAZADA','APROBADA','NOACTIVO','ANULADA')							
		;
		");
		if($res['status']==0){
			$db->Rollback();
			echo "\n".$res['error'];
			exit(2);

		}

		//Marcamos las procesadas
		$res = $db->ExecuteQuery("
		UPDATE mantenimiento
		SET mant_consolidado = '1' 
		WHERE mant_consolidado = '2'

		;
		");
		if($res['status']==0){
			$db->Rollback();
			echo "\n".$res['error'];
			exit(4);
		}
		$db->Commit();
	}
echo "\n Procedimiento finalizado exitosamente";
exit(0);
?>

<?php
	include("db.php");
	global $DEF_CONFIG;
	require $BASEDIR."../libs/password.php";
	$out = array();

	$db = new MySQL_Database();
	$res = $db->ExecuteQuery("SELECT usua_id, usua_password
									FROM usuario
									");
	if(!$res['status']){
		echo "Error al recuperar los usuarios";
 	    exit(1);
	}

	$out = $res['data'];

	foreach ($out AS $detalle) {
		$db->startTransaction();
		echo $detalle['usua_id'];
		echo $detalle['usua_password'];
		echo "\n";
		$usua_id=$detalle['usua_id'];
		$password_encriptada = password_hash($detalle['usua_password'], PASSWORD_DEFAULT);
		$res = $db->ExecuteQuery("UPDATE usuario
									SET usua_crypt_pass='$password_encriptada'
									WHERE usua_id=$usua_id");
		if(!$res['status']){
			$db->rollback();
			echo "Error al actualizar el usuario";
 	    	exit(1);
		}

		$db->commit();

	}

	$test = $password_encriptada;

	echo $test;

	exit(0);

?>
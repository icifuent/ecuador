<?php

include_once("config.php");
date_default_timezone_set('America/Santiago');

class MySQL_Database{
	var $lastQuery = "";
	function MySQL_Database(){
		$this->Connect();
	}	

	function Connect(){
		global $DB_HOST;
		global $DB_USER;
		global $DB_PASS;
		global $DB_NAME;
		
		$pdb = mysql_pconnect($DB_HOST,$DB_USER,$DB_PASS);
		if(!$pdb){
			return mysql_error();
		}
			
		if(!mysql_select_db($DB_NAME)){
			return mysql_error();									
		}		
		setlocale(LC_ALL,"es_CL.UTF-8"); 
        mysql_query('SET CHARACTER SET utf8');
        return true;
	}

	function Disconnect(){
		mysql_close();
	}			
	
	function ExecuteQuery($szQuery){
        $tResult 			 = array();
		$tResult['status']   = 0;
		$tResult['rows'] 	 = 0;
		$tResult['data']	 = array();
		$tResult['error']	 = "";
		$this->lastQuery     = $szQuery;
		$szQuery             = trim($szQuery);			

		$tDbResult = mysql_query($szQuery);
		if($tDbResult){											
			$tResult['status'] = 1;		
			if(strpos($szQuery,"INTO OUTFILE")===FALSE){			
				if(strpos($szQuery,"SELECT")===0 || strpos($szQuery,"SHOW")===0){
					$tResult['rows'] = mysql_num_rows($tDbResult);
					if($tRow = mysql_fetch_assoc($tDbResult)){	
						do{
							array_push($tResult['data'],$tRow);
						}
						while($tRow = mysql_fetch_assoc($tDbResult));						
					}					
				}
				else{
					$tResult['data'][0]['status'] = $tResult['status'];
					$tResult['data'][0]['id']     = mysql_insert_id();
					$tResult['rows']              = mysql_affected_rows();			
				}
			}							
		}	
		else{
			$tResult['error'] = mysql_error();
		}				
		return $tResult;
	}
	 
	 function ExecuteFastQuery($szQuery){
		$tDbResult = mysql_query($szQuery);			
		return $tDbResult;
	}
	
	 
	 function startTransaction(){
	 		$szQuery = "START TRANSACTION";	
			return $this->ExecuteQuery($szQuery);	
	 }
	 		 
	 function Commit(){
	 		$szQuery = "COMMIT";	
			return $this->ExecuteQuery($szQuery);	
	 }
	 function Rollback(){
	 		$szQuery = "ROLLBACK";	
			return $this->ExecuteQuery($szQuery);	
	 }		
}
?>
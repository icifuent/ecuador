<?php
include("config.php");
include("db.php");
include("send_email.php");
include("send_push.php");

global $BASEDIR;

//Funciones utiles
include("utils.php");

//Manejadores de eventos
include("os.php");
include("mnt.php");
include("insp.php");

//Desactivo el limite de memoria
ini_set('memory_limit', '-1');

//___________________________________________________________________________________________
//___________________________________________________________________________________________
//Ciclo de despacho
$db = new MySQL_Database();
$ALIVE 		= TRUE;
$INTERVAL 	= 30000000;//5s
$BENCHMARK  = TRUE;

Loggear("Iniciando servicio");
while($ALIVE) {
	try{
	    $res = $db->ExecuteQuery("SELECT * FROM evento WHERE even_estado='DESPACHADO'");
		if($res['status']){
			if(0<$res['rows']){
				foreach($res['data'] as $row){
					$even_id     = $row['even_id'];
					$even_modulo = $row['even_modulo'];
					$even_evento = $row['even_evento'];
					$even_datos  = ParsearDatos($row['even_datos']);
					$even_creador = $row['usua_creador'];

					$usuarios_notificacion = array();

					Loggear("Evento: ".$even_modulo.":".$even_evento." (id: ".$row['even_id_relacionado'].")");
					if($BENCHMARK){
						$ti = microtime(true);
					}

					switch($even_modulo){
						case 'OS':{
							$orse_id      = $row['even_id_relacionado'];

							//Obtener responsables
							$result 	  = ObtenerResponsablesOS($db,$orse_id);
							if($result['status']==0){
					       		Loggear("Error al obtener responsables para OS: ".$result['error'],LOG_ERR);
					       		continue;
					       	}
					       	$responsables = $result['data'];

							switch($even_evento){
								case 'CREADA':{
									$usuarios_notificacion = $responsables['gestores'];

									$result = OSCreada($db,$responsables,$orse_id);
									break;
								}
								case 'ANULADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = OSAnulada($db,$responsables,$orse_id);
									break;
								}
								case 'ASIGNADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = OSAsignada($db,$responsables,$orse_id,$even_datos['oras_id']);
									break;
								}
								case 'ASIGNACION_CANCELADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = OSAsignacionCancelada($db,$responsables,$orse_id,$even_datos['oras_id']);
									break;
								}

								case 'PRESUPUESTO_AGREGADO':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSPresupuestoAgregado($db,$responsables,$orse_id,$even_datos['pres_id']);
									break;
								}
								case 'PRESUPUESTO_VALIDADO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSPresupuestoValidado($db,$responsables,$orse_id,$even_datos['pres_id'],$even_datos['pres_estado']);
									break;
								}
								case 'PRESUPUESTO_OBSERVADO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSPresupuestoObservado($db,$responsables,$orse_id,$even_datos['pres_id'],$even_datos['pres_estado']);
									break;
								}
								case 'VISITA_EJECUTANDO':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																	$responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);
									$result = OSVisitaEjecutando($db,$responsables,$orse_id);
									break;
								}
								case 'INFORME_AGREGADO':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);
									$result = OSInformeAgregado($db,$responsables,$orse_id,$even_datos['info_id']);

									//PRESUPUESTO
									$result = OSPresupuestoAgregar($db,$responsables,$orse_id);

									break;
								}
								case 'INFORME_VALIDADO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSInformeValidado($db,$responsables,$orse_id,$even_datos['info_id'],$even_datos['info_estado']);
									break;
								}
								case 'OS_FINALIZADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSFinalizada($db,$responsables,$orse_id);
									break;
								}

								case 'OS_VALIDADA':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSValidada($db,$responsables,$orse_id,$even_datos['orse_estado'],$even_datos['info_id']);
									break;
								}
								case 'SOLICITUD_CAMBIO':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSSolicitudCambio($db,$responsables,$orse_id,$even_datos['razon']);
									break;
								}
								case 'SOLICITUD_CAMBIO_VALIDADA':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSSolicitudCambioValidada($db,$responsables,$orse_id,$even_datos['orse_tipo_anterior'],$even_datos['orse_tipo_actual'],$even_datos['orse_solicitud_cambio']);
									break;
								}
								case 'SOLICITUD_INFORME':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSSolicitudInforme($db,$responsables,$orse_id,$even_datos['razon']);
									break;
								}
								case 'SOLICITUD_INFORME_VALIDADA':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = OSSolicitudInformeValidada($db,$responsables,$orse_id,$even_datos['orse_solicitud_informe_web']);
									break;
								}
							}

							OSAJustarResponsable($db,$orse_id);
							break;
						}
						case 'MNT':{
							$mant_id    = $row['even_id_relacionado'];

							//Obtener responsables
							$result 	  = ObtenerResponsablesMNT($db,$mant_id);
							if($result['status']==0){
					       		Loggear("Error al obtener responsables para MNT: ".$result['error'],LOG_ERR);
					       		continue;
					       	}
					       	$responsables = $result['data'];

							switch($even_evento){
								case 'CREADA':{
									$usuarios_notificacion = $responsables['gestores'];

									$result = MNTCreada($db,$responsables,$mant_id);
									break;
								}
                                case 'ANULADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = MNTAnulada($db,$responsables,$mant_id);
									break;
								}
								case 'ASIGNADA':{
									$usuarios_notificacion = Juntar($responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = MNTAsignada($db,$responsables,$mant_id,$even_datos['maas_id']);
									break;
								}
								case 'ASIGNACION_CANCELADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = MNTAsignacionCancelada($db,$responsables,$mant_id,$even_datos['maas_id']);
									break;
								}

								case 'VISITA_EJECUTANDO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = MNTVisitaEjecutando($db,$responsables,$mant_id);
									break;
								}
								case 'INFORME_AGREGADO':{
									$usuarios_notificacion = Juntar($responsables['despachadores'],
													                $responsables['gestores'],
													            	$responsables['gestor_proveedor']);

									$result = MNTInformeAgregado($db,$responsables,$mant_id,$even_datos['info_id']);
									break;
								}
								case 'INFORME_VALIDADO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = MNTInformeValidado($db,$responsables,$mant_id,$even_datos['info_id'],$even_datos['info_estado']);
									break;
								}
								case 'MNT_VALIDADA':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = MNTValidada($db,$responsables,$mant_id,$even_datos['mant_estado'],$even_datos['info_id']);
									break;
								}

								case 'SOLICITUD_INFORME':{
									$usuarios_notificacion = Juntar($responsables['despachadores'],
																	$responsables['gestores']);

									$result = MNTSolicitudInforme($db,$responsables,$mant_id,$even_datos['razon'],$even_datos['maso_id']);
									break;
								}
								case 'SOLICITUD_INFORME_VALIDADA':{
									$usuarios_notificacion = Juntar(ObtenerCreadorSolicitud($db,$even_datos['maso_id']),
																	$responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = MNTSolicitudInformeValidada($db,$responsables,$mant_id,$even_datos['mant_solicitud_informe_web']);
									break;
								}

								case 'SOLICITUD_CAMBIO_FECHA_PROGRAMADA':{
									$usuarios_notificacion = Juntar($responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									$result = MNTSolicitudCambioFechaProgramada($db,$responsables,$mant_id,$even_datos['nueva_fecha_programada'],$even_datos['razon'],$even_datos['maso_id']);
									break;
								}

								case 'SOLICITUD_CAMBIO_FECHA_PROGRAMADA_VALIDADA':{

									$usuarios_notificacion = Juntar(ObtenerCreadorSolicitud($db,$even_datos['maso_id']),
																	$responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores'],
																	$responsables['gestor_proveedor']);

									//revisar notificación de usuarios responsables...

									$result = MNTSolicitudCambioFechaProgramadaValidada($db,$responsables,$mant_id,$even_datos['mant_solicitud_cambio_fecha_programada']);
									break;
								}
							}

							MNTAJustarResponsable($db,$mant_id);

							break;
						}

						case 'INSP':{
							$insp_id    = $row['even_id_relacionado'];

							//Obtener responsables
							$result 	  = ObtenerResponsablesINSP($db,$insp_id);
							if($result['status']==0){
					       		Loggear("Error al obtener responsables para INSP: ".$result['error'],LOG_ERR);
					       		continue;
					       	}
					       	$responsables = $result['data'];

							switch($even_evento){
								case 'CREADA':{
									$usuarios_notificacion = $responsables['gestores'];

									$result = INSPCreada($db,$responsables,$insp_id);
									break;
								}
                                case 'ANULADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores']);

									$result = INSPAnulada($db,$responsables,$insp_id);
									break;
								}
								case 'ASIGNADA':{
									$usuarios_notificacion = Juntar($responsables['despachadores'],
													                $responsables['gestores']);

									$result = INSPAsignada($db,$responsables,$insp_id,$even_datos['inas_id']);
									break;
								}
								case 'ASIGNACION_CANCELADA':{
									$usuarios_notificacion = Juntar($responsables['creador'],
																    $responsables['despachadores'],
													                $responsables['gestores']);

									$result = INSPAsignacionCancelada($db,$responsables,$insp_id,$even_datos['inas_id']);
									break;
								}

								case 'VISITA_EJECUTANDO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
													                $responsables['gestores']);

									$result = INSPVisitaEjecutando($db,$responsables,$insp_id);
									break;
								}
								case 'INFORME_AGREGADO':{
									$usuarios_notificacion = Juntar($responsables['despachadores'],
													                $responsables['gestores']);

									$result = INSPInformeAgregado($db,$responsables,$insp_id,$even_datos['info_id']);
									break;
								}
								case 'INFORME_VALIDADO':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores']);

									$result = INSPInformeValidado($db,$responsables,$insp_id,$even_datos['info_id'],$even_datos['info_estado']);
									break;
								}
								case 'INSP_VALIDADA':{
									$usuarios_notificacion = Juntar($responsables['despachador'],
																	$responsables['despachadores'],
																	$responsables['gestores']);

									$result = INSPValidada($db,$responsables,$insp_id,$even_datos['insp_estado']);
									break;
								}
							}
							break;
						}
					}

					//Saco a creador del evento de la lista de notificación
					if(($key = array_search($even_creador, $usuarios_notificacion)) !== false) {
					    unset($usuarios_notificacion[$key]);
					}

	                //Envio de notificaciones (utils.php)
	                if(0<count($usuarios_notificacion)){
	                	$resNotif = EnviarNotificacion($db,$usuarios_notificacion,$even_modulo,$even_evento,$row['even_id_relacionado'],$row['even_datos']);
						if($resNotif['status']==0){
					   		Loggear("Error al enviar notificacion: ".$resNotif['error'],LOG_ERR);
					   		Loggear($db->lastQuery);
		                }

		                $resEmail = EnviarEmail($db,$usuarios_notificacion,$even_modulo,$even_evento,$row['even_id_relacionado'],$row['even_datos']);
		                if($resEmail['status']==0){
					   		Loggear("Error al enviar notificacion: ".$resEmail['error'],LOG_ERR);
					   		Loggear($db->lastQuery);
		                }
	                }


					if($result['status']){
						//Actualizo el estado del evento
						$res2 = $db->ExecuteQuery("UPDATE evento SET even_estado='ATENDIDO' WHERE even_id=$even_id");
				       	if($res2['status']==0){
				       		Loggear("Error al actualizar evento: ".$res2['error'],LOG_ERR);
				       		continue;
				       	}
					}else {
						$error = $result['error'];

						//Logeo error
						Loggear($even_modulo.":".$even_evento."  ".$error,LOG_ERR);

						if (isset($error)) {
							$error = addslashes($error);
							if(200 > sizeof($error)){
								$error = substr($error, 1, 199);
							}
						}

						$res2 = $db->ExecuteQuery("UPDATE evento SET even_estado='ERROR',even_comentario='$error' WHERE even_id=$even_id");
				    if($res2['status']==0){
				      Loggear("Error al registrar error en manejo de evento:  ".$res2['error'],LOG_ERR);
				      continue;
				    }
					}


					if($BENCHMARK){
						$elapsed = round(microtime(true) - $ti,3)*1000;
						Loggear("Evento procesado en ".$elapsed." ms");
					}

				}
			}
		}
		else{
			Loggear("Error al obtener eventos: ".$res['error'],LOG_ERR);
			//if($res['error']=="MySQL server has gone away"){
				$db->Disconnect();
				$db->Connect();
			//}
		}
	}
	catch (Exception $e) {
		Loggear("Excepción:  ".$e->getMessage(),LOG_ERR);
	}

	usleep($INTERVAL);
}

Loggear("Finalizando servicio");









?>

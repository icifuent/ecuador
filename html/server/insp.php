<?php
//______________________________________________________________________
// Funciones utiles de INSP
function ObtenerResponsablesINSP($db,$insp_id){
   $res = $db->ExecuteQuery("SELECT
                               inspeccion.usua_creador AS creador,
                               inspeccion_asignacion.usua_creador AS despachador
                             FROM 
                               inspeccion
                             LEFT JOIN inspeccion_asignacion ON (inspeccion.insp_id=inspeccion_asignacion.insp_id)
                             WHERE
                                 inspeccion.insp_id=$insp_id
                             ORDER BY inas_fecha_asignacion DESC
                             LIMIT 1");
   if($res['status']==0){
      return $res;
   }

   $responsables = array("creador"=>$res['data'][0]['creador'],
                         "despachador"=>$res['data'][0]['despachador'],
                         "gestores"=>array(),
                         "despachadores"=>array());

   $res = $db->ExecuteQuery("SELECT DISTINCT
                               usuario.usua_id,
                               perfil.perf_nombre
                             FROM 
                               inspeccion
                             INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=inspeccion.empl_id)
                             INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                             INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                             INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_estado='ACTIVO')
                             INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                             INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND (perfil.perf_nombre='GESTOR' OR (perfil.perf_nombre='DESPACHADOR' AND usuario.empr_id=inspeccion.empr_id)) )
                             WHERE
                                 insp_id=$insp_id");
   if($res['status']==0){
      return $res;
   }

   foreach($res['data'] as $row) {
      if($row['perf_nombre']=="GESTOR"){
         if($row['usua_id'] != $responsables["creador"]){
            $responsables["gestores"][] = $row['usua_id'];
         }
      }
      else if($row['perf_nombre']=="DESPACHADOR"){
         if($row['usua_id'] != $responsables["despachador"]){
            $responsables["despachadores"][] = $row['usua_id'];
         }
      } 
   }

   return array("status"=>1,"data"=>$responsables);
}

function ObtenerInformacionINSP($db,$insp_id){
   $res = $db->ExecuteQuery("SELECT 
                                   insp.insp_id,
                                   insp.insp_fecha_solicitud,  
                                   insp.insp_tipo,
                                   emp.empl_nombre, 
                                   emp.empl_nemonico,
                                   emp.empl_direccion,
                                   esp.espe_nombre, 
                                   e.empr_nombre, 
                                   c.comu_nombre, 
                                   r.regi_nombre

                               FROM 
                                   inspeccion insp, 
                                   empresa e, 
                                   rel_contrato_empresa rce, 
                                   especialidad esp, 
                                   emplazamiento emp,
                                   comuna c, 
                                   region r, 
                                   provincia p
                               WHERE
                                   insp.insp_id= $insp_id
                                   AND rce.cont_id=insp.cont_id
                                   AND rce.empr_id=e.empr_id   
                                   AND insp.empr_id=e.empr_id

                                   AND insp.empl_id=emp.empl_id
                                   AND insp.espe_id=esp.espe_id

                                   AND emp.comu_id=c.comu_id
                                   AND p.prov_id = c.prov_id
                                   AND r.regi_id = p.regi_id");
   if($res['status'] == 0) {
      Loggear("Error al obtener información de INSP: ".$res['error'],LOG_ERR);
      return "";
   }
   return "            
    INSP Nº: <b>$insp_id</b><br>
    Empresa: <b>".$res['data'][0]['empr_nombre']."</b><br>
    Tipo: <b>".$res['data'][0]['insp_tipo']."</b><br>
    Fecha programada: <b>".$res['data'][0]['insp_fecha_solicitud']."</b><br>
    Especialidad: <b>".$res['data'][0]['espe_nombre']."</b><br>
    
    Emplazamiento: <b>".$res['data'][0]['empl_nombre']."</b><br>
    Nem&oacute;nico: <b>".$res['data'][0]['empl_nemonico']."</b><br>
    Direcci&oacute;n: <b>".$res['data'][0]['empl_direccion']."</b><br>
    Comuna: <b>".$res['data'][0]['comu_nombre']."</b><br>
    Región: <b>".$res['data'][0]['regi_nombre']."</b>";
}

//______________________________________________________________________
// Manejo de eventos
function INSPCreada($db,$responsables,$insp_id){

   if($responsables['despachadores']==0){       
      //EnviarNotificacion($db,$usua_creador,"SYSTEM","SIN_DESPACHADOR",0,){
      return array("status"=>false,"error"=>"No se ha asignado un usuario despachador para INSP $insp_id");
   }

   $res = EnviarTarea($db,$responsables['despachadores'],'INSP','ASIGNAR',$insp_id);
   if($res['status']==0){
      return $res;
   }

	//Actualizar orden de servicio
	return $db->ExecuteQuery("UPDATE inspeccion SET insp_estado='ASIGNANDO',insp_responsable='CONTRATISTA' WHERE insp_id=$insp_id");
}

function INSPAnulada($db,$responsables,$insp_id){
  $usua_asignado = 0;
  $res = $db->ExecuteQuery("SELECT DISTINCT
                             usua_id
                            FROM 
                            inspeccion_asignacion
                            INNER JOIN rel_inspeccion_asignacion_usuario ON (inspeccion_asignacion.inas_id=rel_inspeccion_asignacion_usuario.inas_id)
                            WHERE
                            insp_id=$insp_id AND rinu_tipo='JEFECUADRILLA' AND inas_estado='ACTIVO'");
  if($res['status']==0){
      return $res;
   }

  if(0<$res['rows']){
    $usua_asignado = $res['data'][0]['usua_id'];
    return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se ha anulado Inspección Nº ".$insp_id);
  }
  return array("status"=>1);
}


function INSPAsignada($db,$responsables,$insp_id,$inas_id){
	//Obtener usuario asignado
	$usua_asignado = 0;
	$res = $db->ExecuteQuery("SELECT 
								usua_id 
							   FROM rel_inspeccion_asignacion_usuario 
							   WHERE inas_id=$inas_id AND rinu_tipo='JEFECUADRILLA'");
	if($res['status']==0){
		return $res;
	}
	if($res['rows']==0){
		return array("status"=>false,"error"=>"No se ha asignado un usuario jefe de cuadrilla para INSP $insp_id");
	}
	$usua_asignado = $res['data'][0]['usua_id'];

	$res = EnviarTarea($db,$usua_asignado,'INSP','VISITAR_SITIO',$insp_id);
	if($res['status']==0){
		return $res;
	}

	//actualizar estado
	$res = $db->ExecuteQuery("UPDATE inspeccion SET insp_estado='ASIGNADA',insp_responsable='CONTRATISTA' WHERE insp_id=$insp_id");
	if($res['status']==0){
		return $res;
	}

	return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se le ha asignado INSP Nº ".$insp_id);
}


function INSPAsignacionCancelada($db,$responsables,$insp_id,$inas_id){
  //Obtener usuario asignado
  $usua_asignado = 0;
  $res = $db->ExecuteQuery("SELECT 
                usua_id 
                 FROM rel_inspeccion_asignacion_usuario 
                 WHERE inas_id=$inas_id AND rinu_tipo='JEFECUADRILLA'");
  if($res['status']==0){
    return $res;
  }
  if($res['rows']==0){
    return array("status"=>false,"error"=>"No se ha asignado un usuario jefe de cuadrilla para INSP $insp_id");
  }
  $usua_asignado = $res['data'][0]['usua_id'];

  //cancelar tarea
  $res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='CANCELADA' 
                            WHERE tare_modulo='INSP' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado='$insp_id' AND usua_id='$usua_asignado'");
  if($res['status']==0){
    return $res;
  }

  return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se ha cancelado su asignación a INSP Nº ".$insp_id);
}


function INSPVisitaEjecutando($db,$responsables,$insp_id){
  $res = $db->ExecuteQuery("INSERT INTO emplazamiento_visita SET 
                              empl_id = (SELECT empl_id FROM inspeccion WHERE inspeccion.insp_id=$insp_id),
                              emvi_modulo = 'INSP',
                              emvi_id_relacionado = $insp_id,
                              emvi_fecha_ingreso = NOW(),
                              emvi_estado = 'ACTIVO'");
  if($res['status']==0){
    return $res;
  }
	
  return array("status"=>1);
}
function  INSPInformeAgregado($db,$responsables,$insp_id,$info_id){
	include_once("export.php");

  $res = $db->ExecuteQuery("UPDATE emplazamiento_visita SET 
                            emvi_fecha_salida = NOW(),
                            emvi_estado = 'NOACTIVO'
                            WHERE emvi_modulo = 'INSP' AND emvi_id_relacionado = $insp_id");
  if($res['status']==0){
    return $res;
  }

  if(0<count($responsables['gestores'])){
  	//Generar informe en pdf 
  	$res = PDFINSPInforme($db,$insp_id,$info_id);
  	if(!$res['status']){
  		return $res;
  	}

  	$res = EnviarTarea($db,$responsables['gestores'],'INSP','VALIDAR_INFORME',$insp_id,'{\"info_id\":'.$info_id.'}');
  	if($res['status']==0){
  		return $res;
  	}
  }
  else{
    return array("status"=>false,"error"=>"No se ha asignado un usuario gestor para INSP $insp_id");
  }

	//Actualizar orden de servicio
	return $db->ExecuteQuery("UPDATE inspeccion SET insp_estado='VALIDANDO',insp_responsable='MOVISTAR' WHERE insp_id=$insp_id");
}

function INSPInformeValidado($db,$responsables,$insp_id,$info_id,$info_estado){
  //Calcular conformidad
  if($info_estado=="PREAPROBADO"){
    include_once("config.php");
    global $DEF_CONFIG;

    $formIngreso = $DEF_CONFIG['insp']['formIngreso'];
    $formSalida  = $DEF_CONFIG['insp']['formSalida'];

    $res = $db->ExecuteQuery("SELECT
                               (SELECT 
                                count(*) 
                                FROM 
                                  formulario_valor 
                                WHERE 
                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='NOK') AS noks,
                               (SELECT 
                                count(*) 
                                FROM 
                                  formulario_valor 
                                WHERE 
                                  formulario_valor.fore_id=formulario_respuesta.fore_id AND fova_valor='OK') AS oks
                              FROM 
                                rel_informe_formulario_respuesta
                              INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id=rel_informe_formulario_respuesta.fore_id AND formulario_respuesta.form_id NOT IN ($formIngreso,$formSalida))
                              WHERE 
                              info_id=$info_id");
    if($res['status']==0){
      return $res;
    }

    $oks   = 0;
    $noks  = 0;
    foreach ($res['data'] as $row) {
        $noks  += $row['noks'];
        $oks   += $row['oks'];
    }
    $total = $oks + $noks;

    if($total>0){
        $conformidad = intval((1 - $noks/$total)*100);
        $res = $db->ExecuteQuery("UPDATE inspeccion SET insp_conformidad='$conformidad' WHERE insp_id=$insp_id");
        if($res['status']==0){
          return $res;
        }
    }
  }

  $res = EnviarTarea($db,$responsables['gestores'],'INSP','VALIDAR_INSP',$insp_id);
  return $res;
}



function INSPValidada($db,$responsables,$insp_id,$insp_estado){

  if($insp_estado=="ACEPTADA_REPAROS" || $insp_estado=="RECHAZADA"){
    include_once("config.php");
    global $DEF_CONFIG;

    $FORM_INGRESO   = $DEF_CONFIG['insp']['formIngreso'];
    $FORM_SALIDA    = $DEF_CONFIG['insp']['formSalida'];
    $SUBE_REPAROS   = $DEF_CONFIG['insp']['subeReparos'];
    $SUBE_RECHAZADA = $DEF_CONFIG['insp']['subeRechazada'];

    $res = $db->ExecuteQuery("SELECT
                              cont_id,
                              empl_id,
                              usua_validador,
                              IF(inspeccion.mant_id IS NOT NULL,(SELECT empr_id FROM mantenimiento WHERE mantenimiento.mant_id=inspeccion.mant_id),usuario.empr_id) AS empr_id
                            FROM
                              inspeccion
                            LEFT JOIN usuario ON (usuario.usua_id = inspeccion.usua_validador)
                            WHERE
                              insp_id=$insp_id");
    if($res['status']==0){
      return $res;
    }
    if($res['rows']==0){
      return array("status"=>false,"error"=>"No se pudieron obtener datos de inspección $insp_id para creacion de OS");
    }

    $cont_id          = $res['data'][0]['cont_id'];
    $empr_id          = $res['data'][0]['empr_id'];
    $empl_id          = $res['data'][0]['empl_id'];
    $sube_id          = ($insp_estado=="ACEPTADA_REPAROS")?($SUBE_REPAROS):($SUBE_RECHAZADA);
    $orse_tipo        = ($insp_estado=="ACEPTADA_REPAROS")?("OSGN"):("OSGU");
    $usua_creador     = $res['data'][0]['usua_validador'];
    $orse_descripcion = "";

    //Obtener NOKs
    $res = $db->ExecuteQuery("SELECT DISTINCT
                                fogr_nombre,
                                foit_nombre
                              FROM
                                informe
                              INNER JOIN rel_informe_formulario_respuesta ON (rel_informe_formulario_respuesta.info_id=informe.info_id)
                              INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id=rel_informe_formulario_respuesta.fore_id AND formulario_respuesta.form_id  NOT IN ($FORM_INGRESO,$FORM_SALIDA))
                              INNER JOIN formulario_valor ON (formulario_valor.fore_id=formulario_respuesta.fore_id AND formulario_valor.fova_valor='NOK')
                              INNER JOIN formulario_item ON (formulario_item.foit_id=formulario_valor.foit_id)
                              INNER JOIN formulario_grupo ON (formulario_grupo.fogr_id=formulario_item.fogr_id)
                              WHERE 
                                info_modulo='INSP' AND info_id_relacionado=$insp_id");
    if( $res['status']==0 ){
        return $res;
    }

    if(0<$res['rows']){
      $orse_descripcion = "Items NOK: ";
      $sep = "";
      foreach($res['data'] AS $row){
          $orse_descripcion .= $sep.$row['fogr_nombre']." > ".$row['foit_nombre'];
          $sep = ", ";
      }
    }

    $res = $db->ExecuteQuery("INSERT INTO orden_servicio SET 
                                cont_id='$cont_id',
                                empr_id='$empr_id',
                                empl_id='$empl_id',
                                sube_id='$sube_id',
                                orse_tipo='$orse_tipo',
                                orse_descripcion='$orse_descripcion',
                                orse_indisponibilidad='NO',
                                orse_fecha_solicitud=NOW(),
                                orse_fecha_creacion=NOW(),
                                usua_creador='$usua_creador',
                                orse_tag=''");
    if( $res['status']==0 ){
        return $res;
    }
    $orse_id = $res['data'][0]['id'];

    $res = $db->ExecuteQuery("UPDATE inspeccion SET orse_id = '$orse_id' WHERE insp_id=$insp_id");
    if($res['status']==0){
      return $res;
    }
    
  }


  //Agregar a inventario informe(s) aprobado(s)
  if($insp_estado=='ACEPTADA'){
    //Obtener informe
    $res = $db->ExecuteQuery("SELECT info_id FROM informe 
                              WHERE
                                info_modulo='INSP' AND info_id_relacionado='$insp_id' AND info_estado='APROBADO'");

    if($res['status']==0){
      return $res;
    }

    foreach ($res['data'] as $row) {
      $res = AgregarInventario($db,$row['info_id']);
      if($res['status']==0){
        Loggear("Error al agregar a inventario informe ".$row['info_id'].": ".$res['error'],LOG_ERR);
      }
    }
  }


  return array("status"=>1);
}
?>

<?php
 
include("db.php");
$db = new MySQL_Database();

$res = $db->ExecuteQuery("SELECT * FROM (      
                                SELECT 'Nº OS',
										'Tipo OS',
										'Especialidad OS',
										'Alarma OS',
										'EECC OS',
										'Usuario creador OS',
										'Fecha creación OS',
										'Fecha solicitud OS',
										'Usuario asignado OS',
										'Estado proceso OS',
										'Fecha ejecución OS',
										'Estado OS',
										'Nombre EMP.',
										'Dirección EMP.',
										'Clasificación EMP.',
										'Dueño de Torre EMP.',
										'Requiere Acceso EMP.',
										'Permisos de acceso EMP.',
										'Región EMP.',
										'Zona Movistar EMP.',
										'Zona Contrato EMP.',
										 'Zona Cluster EMP.',
										'Responsable EMP.',
										'Id LPU',
										'Grupo LPU',	
									    'Item LPU',
									    'SAP OPEX LPU',
										'SAP CAPEX LPU',
									    'Valor LPU',
									    'Cantidad LPU',
										 'Subtotal LPU'							        
										
							UNION ALL

							SELECT
										orden_servicio.orse_id AS 'Nº OS',
										orse_tipo AS 'Tipo OS',
										espe_nombre AS 'Especialidad OS',
										sube_nombre AS 'Alarma OS',
										empr_nombre AS 'EECC OS',
										usuario_creador.usua_nombre AS 'Usuario creador OS',
										orse_fecha_creacion AS 'Fecha creación OS',
										orse_fecha_solicitud AS 'Fecha solicitud OS',
										usuario_asignado.usua_nombre AS 'Usuario asignado OS',
										orse_estado AS 'Estado proceso OS',
										 (SELECT MAX(tare_fecha_despacho) FROM tarea WHERE tare_modulo='OS' AND tare_id_relacionado=orden_servicio.orse_id) AS 'Fecha ejecución OS',
										IF(orse_estado='NOACTIVO','ANULADO', IF(orse_estado='RECHAZADA' OR orse_estado='APROBADA','FINALIZADO','ACTIVO'))AS 'Estado OS',
										
										empl_nombre AS 'Nombre EMP.',
										empl_direccion AS 'Dirección EMP.',
										clas_nombre AS 'Clasificación EMP.',
										duto_nombre AS 'Dueño de Torre EMP.',
										IF(empl_observacion_ingreso=NULL,'NO','SI') AS 'Requiere Acceso EMP.',
										replace(empl_observacion_ingreso, '\n', '')  AS 'Permisos de acceso EMP.',
										regi_nombre AS 'Región EMP.',
										(SELECT 
											zona_nombre
										 FROM rel_zona_emplazamiento 
										 INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' AND zona.cont_id=1)
										 WHERE 
										 	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
										 LIMIT 1) AS 'Zona Movistar EMP.',
										(SELECT 
											zona_nombre
										 FROM rel_zona_emplazamiento 
										 INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' AND zona.cont_id=1)
										 WHERE 
										 	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
										 LIMIT 1) AS 'Zona Contrato EMP.',
										(SELECT 
											zona_nombre
										 FROM rel_zona_emplazamiento 
										 INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER' AND zona.cont_id=1)
										 WHERE 
										 	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
										 LIMIT 1) AS 'Zona Cluster EMP.',
										 (SELECT 
											usua_nombre
										  FROM 
											rel_zona_emplazamiento
										  INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
										  INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
										  INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_cargo='GESTOR')
										  WHERE
											rel_zona_emplazamiento.empl_id=emplazamiento.empl_id) AS 'Responsable EMP.',
											
										lpu_item_precio.lpip_id AS 'Id LPU',
										lpgr_nombre AS 'Grupo LPU',	
									    replace(lpit_nombre, '\n', ' ') AS 'Item LPU',
									    lpip_sap_opex AS 'SAP OPEX LPU',
										lpip_sap_capex AS 'SAP CAPEX LPU',
									    lpip_precio AS 'Valor LPU',
									    prit_cantidad AS 'Cantidad LPU',
										lpip_precio*prit_cantidad AS 'Subtotal LPU'							        
																	        
									FROM 
									presupuesto
									INNER JOIN presupuesto_item ON (presupuesto_item.pres_id = presupuesto.pres_id)
									INNER JOIN lpu_item_precio ON (presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
									INNER JOIN lpu_item ON (lpu_item_precio.lpit_id=lpu_item.lpit_id)
									INNER JOIN lpu_grupo ON (lpu_item.lpgr_id=lpu_grupo.lpgr_id)

									INNER JOIN orden_servicio ON (orden_servicio.orse_id = presupuesto.orse_id)
									INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
									INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)		
									INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)							
									INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
									LEFT JOIN rel_orden_servicio_asignacion_usuario ON (rel_orden_servicio_asignacion_usuario.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) AND roau_tipo='JEFECUADRILLA')	
									LEFT JOIN usuario AS usuario_asignado ON (usuario_asignado.usua_id=rel_orden_servicio_asignacion_usuario.usua_id)
															
									INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)					
									INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = emplazamiento.clas_id)
									INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
									INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
									INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
									INNER JOIN region ON (region.regi_id = provincia.regi_id)
																				
									WHERE orden_servicio.orse_fecha_solicitud >= '2017-01-01 00:00:00' 
                                    AND orden_servicio.orse_fecha_solicitud <= '2017-06-30 23:59:59' 
                                    
                                    
				) AS RESULTADO INTO OUTFILE '/var/lib/mysql-files/OS_sap_Felipe.csv'
						CHARACTER SET latin1
						FIELDS TERMINATED BY ';'
						OPTIONALLY ENCLOSED BY '\"'
						LINES TERMINATED BY '\n'";
						
						echo $query;
						$res = $db->ExecuteQuery($query);
						echo "2";
?>

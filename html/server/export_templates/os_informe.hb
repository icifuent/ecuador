<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title></title>
<link href="server/export_templates/styles.css" rel="stylesheet" media="screen">
</head>
<body>
	<div id="header">
	  <table>
	        <tr>
	            <td align="left" width="140px"><img src="img/logo-navbar.png" width="auto" height="40px"/></td>
	            <td align="left" class="siom-title">Informe {{{data.os.orse_tipo}}} OS N° {{{data.os.orse_id}}}</td>
	            <td align="right"  >
	                <div class="siom-title-number">OS No.: <b>{{{data.os.orse_id}}}</b></div>
	                <div class="siom-title-date">Fecha: <b>{{{data.informe.info_fecha_creacion}}}</b></div>
	            </td>
	        </tr>
	    </table>
	</div>

	<div id="footer">
	  <table>
	      <tr>
	          <td align="left" width="80%">
	          	<img src="img/logo-telefonica-negro.png" width="auto" height="15px"/>
	          	D. Red/ G. Gestion de Redes y Servicios/ SG. Operación de Red
	          </td>
	          <td align="right" valign="top"  width="20%">P&aacute;gina: <span class="page-number"></span></td>
	      </tr>
	  </table>
	</div>


	<fieldset class="siom-fieldset">
		<legend>Emplazamiento</legend>
		<table width="100%">
	      <tr>
	          <td width="40%" colspan="2">
	          	<div class="siom-label">NOMBRE</div>
		   		<div class="siom-value">{{empty data.os.empl_nombre}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">NEMONICO</div>
		   		<div class="siom-value">{{empty data.os.empl_nemonico}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">CLASIFICACIÓN</div>
		   		<div class="siom-value">{{empty data.os.clas_nombre}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">RESPONSABLE</div>
		   		<div class="siom-value">{{empty data.os.empl_responsable}}</div>
	          </td>
	      </tr>
	      <tr>
	          <td width="40%" colspan="2">
	          	<div class="siom-label">DIRECCIÓN</div>
		   		<div class="siom-value">{{empty data.os.empl_direccion}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">COMUNA</div>
		   		<div class="siom-value">{{empty data.os.comu_nombre}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">REGIÓN</div>
		   		<div class="siom-value">{{empty data.os.regi_nombre}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ZONA</div>
		   		<div class="siom-value">{{empty data.os.empl_zona}}</div>
	          </td>
	      </tr>
	      <tr>
	          <td width="20%">
	          	<div class="siom-label">TECNOLOGIA</div>
		   		<div class="siom-value">{{empty data.os.empl_tecnologia}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">MACROSITIO</div>
		   		<div class="siom-value">{{boolean data.os.empl_macrositio}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">SUBTEL</div>
		   		<div class="siom-value">{{boolean data.os.empl_subtel}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ESPACIO</div>
		   		<div class="siom-value">{{empty data.os.empl_espacio}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">TIPO ACCESO</div>
		   		<div class="siom-value">{{empty data.os.empl_tipo_acceso}}</div>
	          </td>
	      </tr>
	  	  <tr>
	          <td width="50%" colspan="2">
	          	<div class="siom-label">OBSERVACIÓN</div>
		   		<div class="siom-value">{{empty data.os.empl_observacion}}</div>
	          </td>
	          <td width="50%" colspan="2">
	          	<div class="siom-label">ACCESO</div>
		   		<div class="siom-value">{{empty data.os.empl_observacion_ingreso}}</div>
	          </td>
	      </tr>
	  	</table>
	</fieldset>

	<fieldset class="siom-fieldset"  style="margin-top:20px;">
	    <legend>Orden de servicio</legend>
	    <table width="100%">
	      <tr>
	          <td width="20%">
	          	<div class="siom-label">Nº OS</div>
		   		<div class="siom-value">{{empty data.os.orse_id}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">TIPO</div>
		   		<div class="siom-value">{{empty data.os.orse_tipo}}</div>
	          </td>
	          <td width="40%" colspan="2">
	          	<div class="siom-label">ESPECIALIDAD</div>
		   		<div class="siom-value">{{empty data.os.espe_nombre}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ALARMA</div>
		   		<div class="siom-value">{{empty data.os.sube_nombre}}</div>
	          </td>
	      </tr>
	      <tr>
	          <td width="20%">
	          	<div class="siom-label">EMPRESA</div>
		   		<div class="siom-value">{{empty data.os.empr_nombre}}</div>
	          </td>
	          <td width="60%" colspan="3">
	          	<div class="siom-label">DESCRIPCION</div>
		   		<div class="siom-value">{{empty data.os.orse_descripcion}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">FECHA PROGRAMADA</div>
		   		<div class="siom-value">{{empty data.os.orse_fecha_solicitud}}</div>
	          </td>
	      </tr>

	    </table>
	</fieldset>



	<fieldset class="siom-fieldset"  style="margin-top:20px;" id="siom-ver-informe">
	    <legend>Informe</legend>
	    {{#if data.informe}}
	    <table width="100%">
	    	<tr>
	          <td width="20%">
	          	<div class="siom-label">GENERADO POR</div>
		   		<div class="siom-value">{{empty data.informe.usua_creador}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">FECHA CREACIÓN</div>
		   		<div class="siom-value">{{empty data.informe.info_fecha_creacion}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">VALIDADO POR</div>
		   		<div class="siom-value">{{empty data.informe.usua_validador}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">FECHA VALIDACIÓN</div>
		   		<div class="siom-value">{{empty data.informe.info_fecha_validacion}}</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ESTADO</div>
		   		<div class="siom-value">{{empty data.informe.info_estado}}</div>
	          </td>
	      </tr>
	    </table>

	    
		{{#each data.formularios}}
			<div class="siom-subtitle">{{{uppercase rtfr_accion}}}</div>

			<div class="siom-form">
				<div class="siom-label">FECHA</div>
			   	<div class="siom-value">{{{rtfr_fecha}}}</div>
			   	<div class="siom-separator"></div>

				{{#each data}}
				<div class="siom-subtitle2">{{{uppercase @key}}}</div>
					{{#each .}}
						{{#notAggregated foit_opciones}}
						<div class="siom-label">{{uppercase foit_nombre}}</div>

						{{#compare foit_tipo "CAMERA"}}	
							{{#each fova_valor}}
				   			<div class="siom-image">
				   				<img src="uploads/{{image}}" width="300px" height="auto"><br>
								<label>{{comment}}</label>
				   			</div>
				   			{{/each}}
				   		{{else}}
				   			{{#compare foit_tipo "AGGREGATOR"}}	
				   				<div class="siom-value"> </div>
				   				<div class="siom-separator"></div>
				   				
						  		{{#each fova_valor}}
						  			<div class="siom-separator-aggregator"></div>
						  			{{#each .}}
							  			{{#isImage .}}
								  			<div class="siom-label">{{aggregatorLabel ../../../foit_opciones @index}}</div>

											{{#each .}}
										  	<div class="siom-image">
								   				<img src="uploads/{{image}}" width="300px" height="auto"><br>
												<label>{{comment}}</label>
								   			</div>
										  	{{/each}}

							  			{{else}}
								  			<div class="siom-label">{{aggregatorLabel ../../../foit_opciones @index}}</div>
											<div class="siom-value">{{empty .}}</div>
										{{/isImage}}
										<div class="siom-separator"></div>
						  			{{/each}}
						  			<div class="siom-separator-aggregator"></div>
						  		{{/each}}
						  		

							{{else}}
								<div class="siom-value">{{empty fova_valor}}</div>
							{{/compare}}
						{{/compare}}
						<div class="siom-separator"></div>

						{{/notAggregated}}
					{{/each}}
				{{/each}}

				<div class="siom-label">UBICACIÓN</div>
		   		<div class="siom-location">
		   			{{#if fore_ubicacion.lat}}
		   			<img src='http://maps.googleapis.com/maps/api/staticmap?center={{fore_ubicacion.lat}},{{fore_ubicacion.lng}}&markers=color:red%7C{{fore_ubicacion.lat}},{{fore_ubicacion.lng}}&zoom=15&size=300x200&key=AIzaSyDCP_5NyMrEdKOlsuVW69YrPr2ZmQMz7FY'>	
		   			{{else}}
		   			Sin ubicación
		   			{{/if}}
		   		</div>
		   		<div class="siom-separator"></div>
			</div>
		{{else}}
    		<div class="alert alert-warning" role="alert">
		  	<strong>Aviso</strong> no hay datos asociados a este informe
			</div>
		{{/each}}
	    {{else}}
	    	<div class="alert alert-warning" role="alert">
		  	<strong>Aviso</strong> ruta indicada no tiene un informe definido.
			</div>
	    {{/if}}
	</fieldset>
</body>
</html>



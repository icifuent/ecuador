<?php return function ($in, $debugopt = 1) {
    $cx = array(
        'flags' => array(
            'jstrue' => true,
            'jsobj' => true,
            'spvar' => true,
            'prop' => false,
            'method' => false,
            'mustlok' => false,
            'echo' => false,
            'debug' => $debugopt,
        ),
        'constants' => array(),
        'helpers' => array(            
        	'empty' => function($arg1) {
	    		if(is_array($arg1)){
	    			$arg1 = $arg1[0];
	    		}
	    		if($arg1==""){
	    			return "N/D";
	    		}
	            return $arg1;
	       	},
            'boolean' => function($arg1) {
	    		if(is_array($arg1)){
	    			$arg1 = $arg1[0];
	    		}
	    		if($arg1==""){
	    			return "N/D";
	    		}
	            return ($arg1=="0")?("NO"):("SI");
	       	},
            'uppercase' => function($arg1) {
				if(is_array($arg1)){
					$arg1 = $arg1[0];
				}
				return mb_strtoupper($arg1);
			},
            'aggregatorLabel' => function($arg){
				$opciones = $arg[0];
				$indice   = $arg[1];

				$label = "";
		        if(isset($opciones) && isset($opciones['labels']) && $indice < count($opciones['labels'])){
		        	$label = $opciones['labels'][$indice];
		        }
		        return $label;
			},
		),
        'blockhelpers' => array(),
        'hbhelpers' => array(            
        	'compare' => function($arg1,$arg2, $options){
				if ($arg1==$arg2) {
				    return $options['fn']();
				} else {
				    return $options['inverse']();
				}
			},
            'notAggregated' => function($arg,$options){
           		return $options['fn']();
		  	},
            'isImage' => function($arg,$options){
				if ($arg && is_array($arg)) {
				    return $options['fn']();
				} else {
				    return $options['inverse']();
				}
			},
		),
        'partials' => array(),
        'scopes' => array(),
        'sp_vars' => array('root' => $in),
    );
    
    return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title></title>
<link href="server/export_templates/styles.css" rel="stylesheet" media="screen">
</head>
<body>
	<div id="header">
	  <table>
	        <tr>
	            <td align="left" width="140px"><img src="img/logo-navbar.png" width="auto" height="40px"/></td>
	            <td align="left" class="siom-title">
'.LCRun3::hbch($cx, 'compare', array(array(((isset($in['data']['mantenimiento']['peri_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['peri_nombre'] : null),'A SOLICITUD'),array()), $in, false, function($cx, $in) {return '	            		Informe MPS Nº '.LCRun3::raw($cx, ((isset($in['data']['mantenimiento']['mant_id']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['mant_id'] : null)).' '.LCRun3::raw($cx, ((isset($in['data']['mantenimiento']['espe_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['espe_nombre'] : null)).'
';}, function($cx, $in) {return '	            		Informe MPP Nº '.LCRun3::raw($cx, ((isset($in['data']['mantenimiento']['mant_id']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['mant_id'] : null)).' '.LCRun3::raw($cx, ((isset($in['data']['mantenimiento']['espe_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['espe_nombre'] : null)).' '.LCRun3::raw($cx, ((isset($in['data']['mantenimiento']['peri_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['peri_nombre'] : null)).'
';}).'				</td>
	            <td align="right"  >
	                <div class="siom-title-number">MNT No.: <b>'.LCRun3::raw($cx, ((isset($in['data']['mantenimiento']['mant_id']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['mant_id'] : null)).'</b></div>
	                <div class="siom-title-date">Fecha: <b>'.LCRun3::raw($cx, ((isset($in['data']['informe']['info_fecha_creacion']) && is_array($in['data']['informe'])) ? $in['data']['informe']['info_fecha_creacion'] : null)).'</b></div>
	            </td>
	        </tr>
	    </table>
	</div>

	<div id="footer">
	  <table>
	      <tr>
	          <td align="left" width="80%">
	          	<img src="img/logo-telefonica-negro.png" width="auto" height="15px"/>
	          	D. Red/ G. Gestion de Redes y Servicios/ SG. Operación de Red
	          </td>
	          <td align="right" valign="top"  width="20%">P&aacute;gina: <span class="page-number"></span></td>
	      </tr>
	  </table>
	</div>


	<fieldset class="siom-fieldset">
		<legend>Emplazamiento</legend>
		<table width="100%">
	      <tr>
	          <td width="40%" colspan="2">
	          	<div class="siom-label">NOMBRE</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">NEMONICO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_nemonico']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_nemonico'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">CLASIFICACIÓN</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['clas_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['clas_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">RESPONSABLE</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_responsable']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_responsable'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>
	      <tr>
	          <td width="40%" colspan="2">
	          	<div class="siom-label">DIRECCIÓN</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_direccion']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_direccion'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">COMUNA</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['comu_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['comu_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">REGIÓN</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['regi_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['regi_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ZONA</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_zona']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_zona'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>
	      <tr>
	          <td width="20%">
	          	<div class="siom-label">TECNOLOGIA</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_tecnologia']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_tecnologia'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">MACROSITIO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'boolean', array(array(((isset($in['data']['mantenimiento']['empl_macrositio']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_macrositio'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">SUBTEL</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'boolean', array(array(((isset($in['data']['mantenimiento']['empl_subtel']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_subtel'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ESPACIO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_espacio']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_espacio'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">TIPO ACCESO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_tipo_acceso']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_tipo_acceso'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>
	  	  <tr>
	          <td width="50%" colspan="2">
	          	<div class="siom-label">OBSERVACIÓN</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_observacion']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_observacion'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="50%" colspan="2">
	          	<div class="siom-label">ACCESO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empl_observacion_ingreso']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empl_observacion_ingreso'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>
	  	</table>
	</fieldset>

	<fieldset class="siom-fieldset"  style="margin-top:20px;">
	    <legend>Mantenimiento</legend>
	    <table width="100%">
	      <tr>
	          <td width="20%">
	          	<div class="siom-label">Nº MNT</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['mant_id']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['mant_id'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">PERIODO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['peri_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['peri_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="40%" colspan="2">
	          	<div class="siom-label">ESPECIALIDAD</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['espe_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['espe_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ALARMA</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['sube_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['sube_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>
	      <tr>
	          <td width="20%">
	          	<div class="siom-label">EMPRESA</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['mantenimiento']['empr_nombre']) && is_array($in['data']['mantenimiento'])) ? $in['data']['mantenimiento']['empr_nombre'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="60%" colspan="3">
	          	<div class="siom-label">DESCRIPCION</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['os']['orse_descripcion']) && is_array($in['data']['os'])) ? $in['data']['os']['orse_descripcion'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">FECHA PROGRAMADA</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['os']['orse_fecha_solicitud']) && is_array($in['data']['os'])) ? $in['data']['os']['orse_fecha_solicitud'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>

	    </table>
	</fieldset>



	<fieldset class="siom-fieldset"  style="margin-top:20px;" id="siom-ver-informe">
	    <legend>Informe</legend>
'.LCRun3::ifv($cx, ((isset($in['data']['informe']) && is_array($in['data'])) ? $in['data']['informe'] : null), $in, function($cx, $in) {return '	    <table width="100%">
	    	<tr>
	          <td width="20%">
	          	<div class="siom-label">GENERADO POR</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['informe']['usua_creador']) && is_array($in['data']['informe'])) ? $in['data']['informe']['usua_creador'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">FECHA CREACIÓN</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['informe']['info_fecha_creacion']) && is_array($in['data']['informe'])) ? $in['data']['informe']['info_fecha_creacion'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">VALIDADO POR</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['informe']['usua_validador']) && is_array($in['data']['informe'])) ? $in['data']['informe']['usua_validador'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">FECHA VALIDACIÓN</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['informe']['info_fecha_validacion']) && is_array($in['data']['informe'])) ? $in['data']['informe']['info_fecha_validacion'] : null)),array()), 'encq').'</div>
	          </td>
	          <td width="20%">
	          	<div class="siom-label">ESTADO</div>
		   		<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['data']['informe']['info_estado']) && is_array($in['data']['informe'])) ? $in['data']['informe']['info_estado'] : null)),array()), 'encq').'</div>
	          </td>
	      </tr>
	    </table>

	    
'.LCRun3::sec($cx, ((isset($in['data']['formularios']) && is_array($in['data'])) ? $in['data']['formularios'] : null), $in, true, function($cx, $in) {return '			<div class="siom-subtitle">'.LCRun3::ch($cx, 'uppercase', array(array(((isset($in['rtfr_accion']) && is_array($in)) ? $in['rtfr_accion'] : null)),array()), 'raw').'</div>

			<div class="siom-form">
				<div class="siom-label">FECHA</div>
			   	<div class="siom-value">'.LCRun3::raw($cx, ((isset($in['rfrr_fecha']) && is_array($in)) ? $in['rfrr_fecha'] : null)).'</div>
			   	<div class="siom-separator"></div>

'.LCRun3::sec($cx, ((isset($in['data']) && is_array($in)) ? $in['data'] : null), $in, true, function($cx, $in) {return '				<div class="siom-subtitle2">'.LCRun3::ch($cx, 'uppercase', array(array(((isset($cx['sp_vars']['key']) && is_array($cx['sp_vars'])) ? $cx['sp_vars']['key'] : null)),array()), 'raw').'</div>
'.LCRun3::sec($cx, $in, $in, true, function($cx, $in) {return ''.LCRun3::hbch($cx, 'notAggregated', array(array(((isset($in['foit_opciones']) && is_array($in)) ? $in['foit_opciones'] : null)),array()), $in, false, function($cx, $in) {return '						<div class="siom-label">'.LCRun3::ch($cx, 'uppercase', array(array(((isset($in['foit_nombre']) && is_array($in)) ? $in['foit_nombre'] : null)),array()), 'encq').'</div>

'.LCRun3::hbch($cx, 'compare', array(array(((isset($in['foit_tipo']) && is_array($in)) ? $in['foit_tipo'] : null),'CAMERA'),array()), $in, false, function($cx, $in) {return ''.LCRun3::sec($cx, ((isset($in['fova_valor']) && is_array($in)) ? $in['fova_valor'] : null), $in, true, function($cx, $in) {return '				   			<div class="siom-image">
				   				<img src="uploads/'.LCRun3::encq($cx, ((isset($in['image']) && is_array($in)) ? $in['image'] : null)).'" width="300px" height="auto"><br>
								<label>'.LCRun3::encq($cx, ((isset($in['comment']) && is_array($in)) ? $in['comment'] : null)).'</label>
				   			</div>
';}).'';}, function($cx, $in) {return ''.LCRun3::hbch($cx, 'compare', array(array(((isset($in['foit_tipo']) && is_array($in)) ? $in['foit_tipo'] : null),'AGGREGATOR'),array()), $in, false, function($cx, $in) {return '				   				<div class="siom-value"> </div>
				   				<div class="siom-separator"></div>
				   				
'.LCRun3::sec($cx, ((isset($in['fova_valor']) && is_array($in)) ? $in['fova_valor'] : null), $in, true, function($cx, $in) {return '						  			<div class="siom-separator-aggregator"></div>
'.LCRun3::sec($cx, $in, $in, true, function($cx, $in) {return ''.LCRun3::hbch($cx, 'isImage', array(array($in),array()), $in, false, function($cx, $in) {return '								  			<div class="siom-label">'.LCRun3::ch($cx, 'aggregatorLabel', array(array(((isset($cx['scopes'][count($cx['scopes'])-3]['foit_opciones']) && is_array($cx['scopes'][count($cx['scopes'])-3])) ? $cx['scopes'][count($cx['scopes'])-3]['foit_opciones'] : null),((isset($cx['sp_vars']['index']) && is_array($cx['sp_vars'])) ? $cx['sp_vars']['index'] : null)),array()), 'encq').'</div>

'.LCRun3::sec($cx, $in, $in, true, function($cx, $in) {return '										  	<div class="siom-image">
								   				<img src="uploads/'.LCRun3::encq($cx, ((isset($in['image']) && is_array($in)) ? $in['image'] : null)).'" width="300px" height="auto"><br>
												<label>'.LCRun3::encq($cx, ((isset($in['comment']) && is_array($in)) ? $in['comment'] : null)).'</label>
								   			</div>
';}).'
';}, function($cx, $in) {return '								  			<div class="siom-label">'.LCRun3::ch($cx, 'aggregatorLabel', array(array(((isset($cx['scopes'][count($cx['scopes'])-3]['foit_opciones']) && is_array($cx['scopes'][count($cx['scopes'])-3])) ? $cx['scopes'][count($cx['scopes'])-3]['foit_opciones'] : null),((isset($cx['sp_vars']['index']) && is_array($cx['sp_vars'])) ? $cx['sp_vars']['index'] : null)),array()), 'encq').'</div>
											<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array($in),array()), 'encq').'</div>
';}).'										<div class="siom-separator"></div>
';}).'						  			<div class="siom-separator-aggregator"></div>
';}).'						  		

';}, function($cx, $in) {return '								<div class="siom-value">'.LCRun3::ch($cx, 'empty', array(array(((isset($in['fova_valor']) && is_array($in)) ? $in['fova_valor'] : null)),array()), 'encq').'</div>
';}).'';}).'						<div class="siom-separator"></div>

';}).'';}).'';}).'
				<div class="siom-label">UBICACIÓN</div>
		   		<div class="siom-location">
'.LCRun3::ifv($cx, ((isset($in['fore_ubicacion']['lat']) && is_array($in['fore_ubicacion'])) ? $in['fore_ubicacion']['lat'] : null), $in, function($cx, $in) {return '		   			<img src=\'http://maps.googleapis.com/maps/api/staticmap?center='.LCRun3::encq($cx, ((isset($in['fore_ubicacion']['lat']) && is_array($in['fore_ubicacion'])) ? $in['fore_ubicacion']['lat'] : null)).','.LCRun3::encq($cx, ((isset($in['fore_ubicacion']['lng']) && is_array($in['fore_ubicacion'])) ? $in['fore_ubicacion']['lng'] : null)).'&markers=color:red%7C'.LCRun3::encq($cx, ((isset($in['fore_ubicacion']['lat']) && is_array($in['fore_ubicacion'])) ? $in['fore_ubicacion']['lat'] : null)).','.LCRun3::encq($cx, ((isset($in['fore_ubicacion']['lng']) && is_array($in['fore_ubicacion'])) ? $in['fore_ubicacion']['lng'] : null)).'&zoom=15&size=300x200&key=AIzaSyDCP_5NyMrEdKOlsuVW69YrPr2ZmQMz7FY\'>	
';}, function($cx, $in) {return '		   			Sin ubicación
';}).'		   		</div>
		   		<div class="siom-separator"></div>
			</div>
';}, function($cx, $in) {return '    		<div class="alert alert-warning" role="alert">
		  	<strong>Aviso</strong> no hay datos asociados a este informe
			</div>
';}).'';}, function($cx, $in) {return '	    	<div class="alert alert-warning" role="alert">
		  	<strong>Aviso</strong> ruta indicada no tiene un informe definido.
			</div>
';}).'	</fieldset>
</body>
</html>


';
}
?>
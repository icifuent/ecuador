<?php
//______________________________________________________________________
// Funciones utiles de MNT
function ObtenerResponsablesMNT($db,$mant_id){
   $res = $db->ExecuteQuery("SELECT
                               mantenimiento.usua_creador AS creador,
                               mantenimiento_asignacion.usua_creador AS despachador
                             FROM 
                               mantenimiento
                             LEFT JOIN mantenimiento_asignacion ON (mantenimiento.mant_id=mantenimiento_asignacion.mant_id)
                             WHERE
                                 mantenimiento.mant_id=$mant_id
                             ORDER BY maas_fecha_asignacion DESC
                             LIMIT 1");
   if($res['status']==0){
      return $res;
   }

   $responsables = array("creador"=>$res['data'][0]['creador'],
                         "despachador"=>$res['data'][0]['despachador'],
                         "gestores"=>array(),
                         "despachadores"=>array());

   $res = $db->ExecuteQuery("SELECT DISTINCT
                               usuario.usua_id,
                               perfil.perf_nombre
                             FROM 
                               mantenimiento
                             INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=mantenimiento.empl_id)
                             INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                             INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                             INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_estado='ACTIVO')
                             INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                             INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND (perfil.perf_nombre='GESTOR PROVEEDOR' OR perfil.perf_nombre='GESTOR' OR (perfil.perf_nombre='DESPACHADOR' AND usuario.empr_id=mantenimiento.empr_id)) )
                             WHERE
                                 mant_id=$mant_id");
   if($res['status']==0){
      return $res;
   }

   foreach($res['data'] as $row) {
      if($row['perf_nombre']=="GESTOR"){
         if($row['usua_id'] != $responsables["creador"]){
            $responsables["gestores"][] = $row['usua_id'];
         }
      }
      else if($row['perf_nombre']=="DESPACHADOR"){
         if($row['usua_id'] != $responsables["despachador"]){
            $responsables["despachadores"][] = $row['usua_id'];
         }
      }
      else if($row['perf_nombre']=="GESTOR PROVEEDOR"){
        $responsables["gestor_proveedor"][] = $row['usua_id'];
      } 
   }

   return array("status"=>1,"data"=>$responsables);
}

function ObtenerInformacionMNT($db,$mant_id){
   $res = $db->ExecuteQuery("SELECT 
                                   mnt.mant_id,
                                   mnt.mant_fecha_programada,  
                                   pe.peri_nombre,
                                   emp.empl_nombre, 
                                   emp.empl_nemonico,
                                   emp.empl_direccion,
                                   esp.espe_nombre, 
                                   e.empr_nombre, 
                                   c.comu_nombre, 
                                   r.regi_nombre

                               FROM 
                                   mantenimiento mnt, 
                                   empresa e, 
                                   rel_contrato_empresa rce, 
                                   especialidad esp, 
                                   emplazamiento emp,
                                   comuna c, 
                                   region r, 
                                   provincia p, 
                                   periodicidad pe, 
                                   rel_contrato_periodicidad rcp,
                                   mantenimiento_periodos mp
                               WHERE
                                   mnt.mant_id= $mant_id
                                   AND rce.cont_id=mnt.cont_id
                                   AND rce.empr_id=e.empr_id   
                                   AND mnt.empr_id=e.empr_id

                                   AND mnt.empl_id=emp.empl_id
                                   AND mnt.espe_id=esp.espe_id

                                   AND emp.comu_id=c.comu_id
                                   AND p.prov_id = c.prov_id
                                   AND r.regi_id = p.regi_id

                                   AND mnt.mape_id = mp.mape_id
                                   AND mp.rcpe_id = rcp.rcpe_id
                                   AND rcp.peri_id = pe.peri_id
                                   AND rcp.cont_id = mnt.cont_id");
   if($res['status'] == 0) {
      Loggear("Error al obtener información de MNT: ".$res['error'],LOG_ERR);
      return "";
   }
   return "            
    MNT Nº: <b>$mant_id</b><br>
    Empresa: <b>".$res['data'][0]['empr_nombre']."</b><br>
    Periodicidad: <b>".$res['data'][0]['peri_nombre']."</b><br>
    Fecha programada: <b>".$res['data'][0]['mant_fecha_programada']."</b><br>
    Especialidad: <b>".$res['data'][0]['espe_nombre']."</b><br>
    
    Emplazamiento: <b>".$res['data'][0]['empl_nombre']."</b><br>
    Nem&oacute;nico: <b>".$res['data'][0]['empl_nemonico']."</b><br>
    Direcci&oacute;n: <b>".$res['data'][0]['empl_direccion']."</b><br>
    Comuna: <b>".$res['data'][0]['comu_nombre']."</b><br>
    Región: <b>".$res['data'][0]['regi_nombre']."</b>";
}

function ObtenerCreadorSolicitud($db,$maso_id){
   if($maso_id){
     $res = $db->ExecuteQuery("SELECT
                                 usua_creador AS creador
                               FROM 
                                 mantenimiento_solicitud
                               WHERE
                                   maso_id=$maso_id");
     if($res['status']==0){
        return $res;
     }

     if(0<$res['rows']){
        return $res['data'][0]['creador'];
     }
   }
   return null;
 }


 function MNTAJustarResponsable($db,$mant_id){
  $res = $db->ExecuteQuery("SELECT
                                  tare_tipo
                              FROM
                                  tarea
                              WHERE 
                              tare_modulo='MNT' AND tare_estado IN ('CREADA','DESPACHADA') 
                              AND tare_id_relacionado = $mant_id
                              ");
  if($res['status'] == 0) {
      Loggear("Error al tareas pendientes de MNT: ".$res['error'],LOG_ERR);
  }

  $mant_responsable = "CONTRATISTA";
  $tareas_movistar  = array("VALIDAR_INFORME", "VALIDAR_MNT", "VALIDAR_SOLICITUD_CAMBIO_FECHA_PROGRAMADA","VALIDAR_SOLICITUD_INFORME");
  foreach ($res['data'] AS $row) {
      if(in_array($row['tare_tipo'],$tareas_movistar)){
          $mant_responsable = "MOVISTAR";
          break;
      }
  }

  $res = $db->ExecuteQuery("UPDATE mantenimiento SET mant_responsable='$mant_responsable' WHERE mant_id=$mant_id");
  if($res['status'] == 0) {
      Loggear("Error al actualizar responsable de MNT: ".$res['error'],LOG_ERR);
  }
}

//______________________________________________________________________
// Manejo de eventos
function MNTCreada($db,$responsables,$mant_id){

   if($responsables['despachadores']==0){       
      //EnviarNotificacion($db,$usua_creador,"SYSTEM","SIN_DESPACHADOR",0,){
      return array("status"=>false,"error"=>"No se ha asignado un usuario despachador para MNT $mant_id");
   }

   $res = EnviarTarea($db,array_merge($responsables['despachadores'], $responsables['gestor_proveedor']),'MNT','ASIGNAR',$mant_id);
   if($res['status']==0){
      return $res;
   }

	//Actualizar orden de servicio
	return $db->ExecuteQuery("UPDATE mantenimiento SET mant_estado='ASIGNANDO' WHERE mant_id=$mant_id");
}

function MNTAnulada($db,$responsables,$mant_id){
  $usua_asignado = 0;
  $res = $db->ExecuteQuery("SELECT DISTINCT
                             usua_id
                            FROM 
                            mantenimiento_asignacion
                            INNER JOIN rel_mantenimiento_asignacion_usuario ON (mantenimiento_asignacion.maas_id=rel_mantenimiento_asignacion_usuario.maas_id)
                            WHERE
                            mant_id=$mant_id AND rmau_tipo='JEFECUADRILLA' AND maas_estado='ACTIVO'");
  if($res['status']==0){
      return $res;
   }

  if(0<$res['rows']){
    $usua_asignado = $res['data'][0]['usua_id'];
    return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se ha anulado MNT Nº ".$mant_id);
  }
  return array("status"=>1);
}


function MNTAsignada($db,$responsables,$mant_id,$maas_id){
	//Obtener usuario asignado
	$usua_asignado = 0;
	$res = $db->ExecuteQuery("SELECT 
								usua_id 
							   FROM rel_mantenimiento_asignacion_usuario 
							   WHERE maas_id=$maas_id AND rmau_tipo='JEFECUADRILLA'");
	if($res['status']==0){
		return $res;
	}
	if($res['rows']==0){
		return array("status"=>false,"error"=>"No se ha asignado un usuario jefe de cuadrilla para MNT $mant_id");
	}
	$usua_asignado = $res['data'][0]['usua_id'];

	$res = EnviarTarea($db,$usua_asignado,'MNT','VISITAR_SITIO',$mant_id);
	if($res['status']==0){
		return $res;
	}

	//actualizar estado
	$res = $db->ExecuteQuery("UPDATE mantenimiento SET mant_estado='ASIGNADA' WHERE mant_id=$mant_id");
	if($res['status']==0){
		return $res;
	}

	return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se le ha asignado MNT Nº ".$mant_id);
}


function MNTAsignacionCancelada($db,$responsables,$mant_id,$maas_id){
  //Obtener usuario asignado
  $usua_asignado = 0;
  $res = $db->ExecuteQuery("SELECT 
                usua_id 
                 FROM rel_mantenimiento_asignacion_usuario 
                 WHERE maas_id=$maas_id AND rmau_tipo='JEFECUADRILLA'");
  if($res['status']==0){
    return $res;
  }
  if($res['rows']==0){
    return array("status"=>false,"error"=>"No se ha asignado un usuario jefe de cuadrilla para MNT $mant_id");
  }
  $usua_asignado = $res['data'][0]['usua_id'];

  //cancelar tarea
  $res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='CANCELADA' 
                            WHERE tare_modulo='MNT' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado='$mant_id' AND usua_id='$usua_asignado'");
  if($res['status']==0){
    return $res;
  }

  return EnviarPush($db,$usua_asignado,"UPDATE_ASSIGNMENT","Se ha cancelado su asignación a MNT Nº ".$mant_id);
}


function MNTVisitaEjecutando($db,$responsables,$mant_id){
  $res = $db->ExecuteQuery("INSERT INTO emplazamiento_visita SET 
                              empl_id = (SELECT empl_id FROM mantenimiento WHERE mantenimiento.mant_id=$mant_id),
                              emvi_modulo = 'MNT',
                              emvi_id_relacionado = $mant_id,
                              emvi_fecha_ingreso = NOW(),
                              emvi_estado = 'ACTIVO'");
  if($res['status']==0){
    return $res;
  }
	
  return array("status"=>1);
}
function  MNTInformeAgregado($db,$responsables,$mant_id,$info_id){
	include_once("export.php");

  $res = $db->ExecuteQuery("UPDATE emplazamiento_visita SET 
                            emvi_fecha_salida = NOW(),
                            emvi_estado = 'NOACTIVO'
                            WHERE emvi_modulo = 'MNT' AND emvi_id_relacionado = $mant_id");
  if($res['status']==0){
    return $res;
  }

  if(0<count($responsables['gestor_proveedor'])){
  	//Generar informe en pdf 
  	$res = PDFMNTInforme($db,$mant_id,$info_id);
  	if(!$res['status']){
  		return $res;
  	}

  	$res = EnviarTarea($db,$responsables['gestor_proveedor'],'MNT','VALIDAR_INFORME',$mant_id,'{\"info_id\":'.$info_id.'}');
  	if($res['status']==0){
  		return $res;
  	}
  }
  else{
    return array("status"=>false,"error"=>"No se ha asignado un usuario gestor para MNT $mant_id");
  }

	//Actualizar mantenimiento
	return $db->ExecuteQuery("UPDATE mantenimiento SET mant_estado='VALIDANDO',mant_responsable='MOVISTAR',mant_fecha_ejecucion=(SELECT info_fecha_creacion FROM informe WHERE info_id = $info_id) WHERE mant_id=$mant_id");
}
function MNTInformeValidado($db,$responsables,$mant_id,$info_id,$info_estado){
  $res = EnviarTarea($db,$responsables['gestores'],'MNT','VALIDAR_MNT',$mant_id);
  return $res;
}


function MNTSolicitudInforme($db,$responsables,$mant_id,$razon,$maso_id){
  if(0<count($responsables['gestores'])){
    return EnviarTarea($db,$responsables['gestores'],'MNT','VALIDAR_SOLICITUD_INFORME',$mant_id,'{\"razon\":\"'.$razon.'\",\"maso_id\":\"'.$maso_id.'\"}');
	}
  else{
    return array("status"=>false,"error"=>"No se ha asignado un usuario gestor para MNT $mant_id");
  }
}
function MNTSolicitudInformeValidada($db,$responsables,$mant_id,$mant_solicitud_informe_web){
   return array("status"=>1);
}



function MNTSolicitudCambioFechaProgramada($db,$responsables,$mant_id,$nueva_fecha_programada,$razon,$maso_id){
  if(0<count($responsables['gestores'])){
    return EnviarTarea($db,$responsables['gestores'],'MNT','VALIDAR_SOLICITUD_CAMBIO_FECHA_PROGRAMADA',$mant_id,'{\"nueva_fecha_programada\":\"'.$nueva_fecha_programada.'\",\"razon\":\"'.$razon.'\",\"maso_id\":\"'.$maso_id.'\"}');
  }
  else{
    return array("status"=>false,"error"=>"No se ha asignado un usuario gestor para MNT $mant_id");
  }
}
function MNTSolicitudCambioFechaProgramadaValidada($db,$responsables,$mant_id,$mant_solicitud_cambio_fecha_programada){
   return array("status"=>1);
}


function MNTValidada($db,$responsables,$mant_id,$mant_estado, $info_id){

  //Agregar Inspeccion
  $res = $db->ExecuteQuery("SELECT
                              cont_id,
                              empl_id,
                              espe_id,
                              usua_validador,
                              usuario.empr_id
                            FROM
                              mantenimiento
                            LEFT JOIN usuario ON (usuario.usua_id = mantenimiento.usua_validador)
                            WHERE
                              mant_id=$mant_id");
  if($res['status']==0){
    return $res;
  }
  if($res['rows']==0){
    return array("status"=>false,"error"=>"No se pudieron obtener datos de MNT $mant_id para creacion de inspeccion");
  }

  $cont_id          = $res['data'][0]['cont_id'];
  $empr_id          = $res['data'][0]['empr_id'];
  $empl_id          = $res['data'][0]['empl_id'];
  $espe_id          = $res['data'][0]['espe_id'];
  $insp_descripcion = "Inspección relacionada con MNT Nº $mant_id, finalizada con estado $mant_estado";
  $usua_creador     = $res['data'][0]['usua_validador'];

  //Crear inspeccion
  $query = "INSERT INTO inspeccion SET 
                cont_id='$cont_id',
                empr_id='$empr_id',
                empl_id='$empl_id',
                espe_id='$espe_id',
                insp_tipo='MPP',
                insp_descripcion='$insp_descripcion',
                insp_fecha_solicitud=NOW(),
                insp_fecha_creacion=NOW(),
                usua_creador='$usua_creador',
                insp_estado='CREADA',
                mant_id=$mant_id";
  $res = $db->ExecuteQuery($query);
  if( $res['status']==0 ){
      return $res;
  }

  //Agregar a inventario informe(s) aprobado(s)
  if($mant_estado=='APROBADA'){
    //Obtener informe
    $res = $db->ExecuteQuery("SELECT info_id FROM informe 
                              WHERE
                                info_modulo='MNT' AND info_id_relacionado='$mant_id' AND info_estado='APROBADO'");

    if($res['status']==0){
      return $res;
    }

    foreach ($res['data'] as $row) {
      $res = AgregarInventario($db,$row['info_id']);
      if($res['status']==0){
        Loggear("Error al agregar a inventario informe ".$row['info_id'].": ".$res['error'],LOG_ERR);
      }
    }
  }
  else{
    $res = EnviarTarea($db,$responsables['gestor_proveedor'],'MNT','VALIDAR_INFORME',$mant_id,'{\"info_id\":'.$info_id.'}');
    if($res['status']==0){
      return $res;
    }
  }


   return array("status"=>1);
}
?>

<?php
global $BASEDIR;

include($BASEDIR."api/upload.php");

function OSDetalle($db,$orse_id){
    $query = "SELECT 
                os.*, 
                emp.*, 
                empcl.clas_nombre,
                e.empr_nombre, 
                sesp.sube_nombre, 
                esp.espe_nombre, 
                c.comu_nombre, 
                GROUP_CONCAT(t.tecn_nombre) as tecn_nombre, 
                z.zona_id, z.zona_nombre, 
                r.regi_id, r.regi_nombre 
              FROM orden_servicio os, empresa e, subespecialidad sesp, especialidad esp, emplazamiento emp, emplazamiento_clasificacion empcl, comuna c, region r, provincia p, tecnologia t, rel_emplazamiento_tecnologia ret, zona z, rel_zona_emplazamiento rze 
              WHERE 
                os.orse_id=$orse_id 
                AND os.empr_id=e.empr_id 
                AND os.empl_id=emp.empl_id 
                AND os.sube_id=sesp.sube_id 
                AND sesp.espe_id=esp.espe_id 
                
                AND emp.clas_id = empcl.clas_id 
                AND emp.comu_id=c.comu_id 
                AND p.prov_id = c.prov_id 
                AND r.regi_id = p.regi_id 
                AND ret.empl_id=emp.empl_id 
                AND ret.tecn_id=t.tecn_id 
                AND rze.empl_id = emp.empl_id 
                AND rze.zona_id = z.zona_id 
                AND z.cont_id = os.cont_id 
                AND z.zona_tipo = 'CONTRATO' 
              GROUP BY 
                t.tecn_nombre ";
    return $db->ExecuteQuery($query);
}

function MNTDetalle($db,$mant_id){
	$query = "  SELECT 
                    mnt.*, 
                    emp.*, 
                    empcl.clas_nombre,
                    mp.*,
                    pe.peri_id,
                    pe.peri_nombre,
                    e.empr_nombre, 
                    esp.espe_nombre, 
                    co.comu_nombre, 
                    GROUP_CONCAT(t.tecn_nombre) as tecn_nombre, 
                    z.zona_id, z.zona_nombre, 
                    r.regi_id, r.regi_nombre
                FROM 
                    mantenimiento mnt, 
                    empresa e, 
                    rel_contrato_empresa rce, 
                    especialidad esp, 
                    emplazamiento emp,
                    emplazamiento_clasificacion empcl,
                    comuna co, 
                    region r, 
                    provincia p, 
                    tecnologia t, 
                    rel_emplazamiento_tecnologia ret, 
                    zona z, 
                    rel_zona_emplazamiento rze,
                    periodicidad pe, 
                    rel_contrato_periodicidad rcp,
                    mantenimiento_periodos mp
                WHERE
                    mnt.mant_id= $mant_id                   
                    AND rce.cont_id=mnt.cont_id
                    AND rce.empr_id=e.empr_id	
                    AND mnt.empr_id=e.empr_id

                    AND mnt.empl_id=emp.empl_id
                    AND mnt.espe_id=esp.espe_id

                    AND emp.clas_id = empcl.clas_id
                    AND emp.comu_id=co.comu_id
                    AND p.prov_id = co.prov_id
                    AND r.regi_id = p.regi_id
                    AND ret.empl_id=emp.empl_id
                    AND ret.tecn_id=t.tecn_id
                    AND rze.empl_id = emp.empl_id
                    AND rze.zona_id = z.zona_id
                    AND z.cont_id = mnt.cont_id
                    AND z.zona_tipo = 'CONTRATO'
                    AND mnt.mape_id = mp.mape_id
                    AND mp.rcpe_id = rcp.rcpe_id
                    AND rcp.peri_id = pe.peri_id
                    AND rcp.cont_id = mnt.cont_id   
                GROUP BY 
                    t.tecn_nombre
                ;";
    return $db->ExecuteQuery($query);
}

function INSPDetalle($db,$insp_id){
	$query = " SELECT 
                    inspeccion.*, 
                    emplazamiento.*, 
                    especialidad.espe_nombre
                FROM 
                    inspeccion
                INNER JOIN emplazamiento ON (inspeccion.empl_id=emplazamiento.empl_id)
                INNER JOIN especialidad ON (inspeccion.espe_id=especialidad.espe_id)
                WHERE
                    inspeccion.insp_id= $insp_id
                ;";
    return $db->ExecuteQuery($query);
}


function ObtenerInforme($db,$info_id){
	$out = array();
	$res = $db->ExecuteQuery("SELECT
                                uc.usua_nombre AS usua_creador,
                                info_id,
                                info_fecha_creacion,
                                info_data,
                                uv.usua_nombre AS usua_validador,
                                info_fecha_validacion,
                                info_estado
                            FROM
                                informe
                            INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
                            LEFT JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
                            WHERE
                                info_id=$info_id");
    if( $res['status']==0 ){
        return $res;
    }

    $out['informe'] = array();
    if(0<$res['rows']){
        $out['informe'] = $res['data'][0];

        $out['formularios'] = array();

        //Obtener visitas
        $info_data = json_decode($out['informe']['info_data'],true);
        if($info_data!=null){
            $tare_id = $info_data['tare_id'];

            $res = $db->ExecuteQuery("SELECT
                                            rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                        FROM
                                            rel_tarea_formulario_respuesta
                                        INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                        INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
                                        WHERE
                                            tare_id=$tare_id
										GROUP BY fore_id");
            if( $res['status']==0 ){
                return $res;
            }

            $out['formularios'] = $res['data'];

            for($i=0;$i<count($out['formularios']);$i++){
                $fore_id = $out['formularios'][$i]['fore_id'];

                if($out['formularios'][$i]['fore_ubicacion']!=""){
                    $out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
                }

                $res = $db->ExecuteQuery("SELECT
                								fogr_nombre,
                                                foit_nombre,
                                                foit_tipo,
                                                fova_valor,
                                                foit_opciones
                                            FROM
                                                formulario_respuesta
                                            INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
                                            INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
                                            LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
                                            WHERE
                                                formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
                                            ORDER BY fogr_orden,foit_orden;");
                if( $res['status']==0 ){
                    return $res;
                }
                
                $out['formularios'][$i]['data'] = array();
                foreach($res['data'] as $row){
                	if(!isset($out['formularios'][$i]['data'][$row['fogr_nombre']])){
                		$out['formularios'][$i]['data'][$row['fogr_nombre']] = array();
                	}

                    if($row['foit_tipo']=="CAMERA"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_tipo']=="AGGREGATOR"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_opciones']!=""){
                        $row['foit_opciones'] = json_decode($row['foit_opciones'],true);
                    }

                    array_push($out['formularios'][$i]['data'][$row['fogr_nombre']],$row);
                }
            }

        }
    }
    return array("status"=>true,"data"=>$out);
}

function ExcelOSPresupuesto($db,$orse_id,$pres_id){
	global $BASEDIR;
	require_once $BASEDIR."../libs/PHPExcel/Classes/PHPExcel.php";

	$EXCEL_STYLES = array(
			  'header' => array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '000000'),
			        'size'  => 10,
			        'name'  => 'Verdana'
			    )
			  ),
			  'value' => array(
			    'font'  => array(
			        'bold'  => false,
			        'color' => array('rgb' => '000000'),
			        'size'  => 10,
			        'name'  => 'Verdana'
			    )
			  )
		);

	try{
		$res = $db->ExecuteQuery("SELECT
										orden_servicio.orse_id AS 'Nº OS',
										orse_tipo AS 'Tipo OS',
										espe_nombre AS 'Especialidad OS',
										sube_nombre AS 'Alarma OS',
										empr_nombre AS 'EECC OS',
										usuario_creador.usua_nombre AS 'Usuario creador OS',
										orse_fecha_creacion AS 'Fecha creación OS',
										orse_fecha_solicitud AS 'Fecha solicitud OS',
										usuario_asignado.usua_nombre AS 'Usuario asignado OS',
										orse_estado AS 'Estado proceso OS',
										 (SELECT MAX(tare_fecha_despacho) FROM tarea WHERE tare_modulo='OS' AND tare_id_relacionado=orden_servicio.orse_id) AS 'Fecha ejecución OS',
										IF(orse_estado='NOACTIVO','ANULADO', IF(orse_estado='RECHAZADA' OR orse_estado='APROBADA','FINALIZADO','ACTIVO'))AS 'Estado OS',
										
										empl_nombre AS 'Nombre EMP.',
										empl_direccion AS 'Dirección EMP.',
										clas_nombre AS 'Clasificación EMP.',
										duto_nombre AS 'Dueño de Torre EMP.',
										IF(empl_observacion_ingreso=NULL,'NO','SI') AS 'Requiere Acceso EMP.',
										empl_observacion_ingreso AS 'Permisos de acceso EMP.',
										regi_nombre AS 'Región EMP.',
										(SELECT 
											zona_nombre
										 FROM rel_zona_emplazamiento 
										 INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR')
										 WHERE 
										 	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
										 LIMIT 1) AS 'Zona Movistar EMP.',
										(SELECT 
											zona_nombre
										 FROM rel_zona_emplazamiento 
										 INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO')
										 WHERE 
										 	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
										 LIMIT 1) AS 'Zona Contrato EMP.',
										(SELECT 
											zona_nombre
										 FROM rel_zona_emplazamiento 
										 INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER')
										 WHERE 
										 	rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
										 LIMIT 1) AS 'Zona Cluster EMP.',
										 (SELECT 
											usua_nombre
										  FROM 
											rel_zona_emplazamiento
										  INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
										  INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
										  INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_cargo='GESTOR')
										  WHERE
											rel_zona_emplazamiento.empl_id=emplazamiento.empl_id) AS 'Responsable EMP.',
											
										lpu_item_precio.lpip_id AS 'Id LPU',
										lpgr_nombre AS 'Grupo LPU',	
									    lpit_nombre AS 'Item LPU',
									    lpip_sap_opex AS 'SAP OPEX LPU',
										lpip_sap_capex AS 'SAP CAPEX LPU',
									    lpip_precio AS 'Valor LPU',
									    prit_cantidad AS 'Cantidad LPU',
										lpip_precio*prit_cantidad AS 'Subtotal LPU'							        
																	        
									FROM 
									presupuesto
									INNER JOIN presupuesto_item ON (presupuesto_item.pres_id = presupuesto.pres_id)
									INNER JOIN lpu_item_precio ON (presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
									INNER JOIN lpu_item ON (lpu_item_precio.lpit_id=lpu_item.lpit_id)
									INNER JOIN lpu_grupo ON (lpu_item.lpgr_id=lpu_grupo.lpgr_id)

									INNER JOIN orden_servicio ON (orden_servicio.orse_id = presupuesto.orse_id)
									INNER JOIN subespecialidad ON (subespecialidad.sube_id = orden_servicio.sube_id)
									INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)		
									INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)							
									INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
									LEFT JOIN rel_orden_servicio_asignacion_usuario ON (rel_orden_servicio_asignacion_usuario.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) AND roau_tipo='JEFECUADRILLA')	
									LEFT JOIN usuario AS usuario_asignado ON (usuario_asignado.usua_id=rel_orden_servicio_asignacion_usuario.usua_id)
															
									INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)					
									INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = emplazamiento.clas_id)
									INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
									INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
									INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
									INNER JOIN region ON (region.regi_id = provincia.regi_id)
																				
									WHERE presupuesto.pres_id=$pres_id
									ORDER BY presupuesto_item.prit_id");
		if(!$res['status']){
			throw new Exception($res['error']);
		}
		if(0==$res['rows']){
			throw new Exception("No hay datos para id de presupuesto $pres_id");
		}
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("SIOM")
									 ->setLastModifiedBy("SIOM")
									 ->setTitle("OS Presupuesto")
									 ->setDescription("Presupuesto para OS Nº ".$orse_id);
		$sheet = $objPHPExcel->setActiveSheetIndex(0);
		
		//header
		$col = "A";
		$row =  1;
		foreach(array_keys($res['data'][0]) as $field){
			$sheet->setCellValue($col.$row,$field);
			$sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);

			$sheet->getColumnDimension($col)->setAutoSize( true );
			$col++;	
		}

		//datos
		$row++;
		$total = 0;
		foreach($res['data'] as $data){
			$col = "A";
			foreach($data as $key => $value){
				$sheet->setCellValue($col.$row,$value);
				$sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['value']);
				
				if($key=="subtotal"){
					$total += $value;
				}
				$col++;	
			}
			$row++;
		}
		/*
		$col = chr(ord($col)-1);
		$sheet->setCellValue($col.$row,$total);
		$sheet->getStyle($col.$row)->applyFromArray($EXCEL_STYLES['header']);
		*/

		$filename = date("dmyHis")."_os_".$orse_id."_presupuesto_".$pres_id.".xls";
		$res      = Upload::GenerateFilename($filename);

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($res['abspath']);

		$filename = $res['relpath'];
		$name     = sprintf("OS-%d-PRES-%03d",$orse_id,$pres_id);
		$res = $db->ExecuteQuery("INSERT INTO repositorio SET
									repo_tipo_doc='PRESUPUESTO',
									repo_nombre='$name',
									repo_tabla='presupuesto',
									repo_tabla_id='$pres_id',
									repo_ruta='$filename',
									repo_fecha_creacion=NOW(),
									repo_data='{\"orse_id\":$orse_id}',
									usua_creador=1");
		if(!$res['status']){
			unlink($res['abspath']);
		}
	}
	catch (Exception $e) {
	   $res = array("status"=>false,"error"=>"Error al generar excel: ".$e->getMessage());
	}
	return $res;
}


function PDFOSInforme($db,$orse_id,$info_id){
	global $BASEDIR;
    

	require_once($BASEDIR."../libs/lightncandy/lightncandy.php");
    require_once($BASEDIR."../libs/dompdf/dompdf_config.inc.php");

    ini_set('display_errors', TRUE);

	try{
		//Obtener datos de os
		$res = OSDetalle($db,$orse_id);
		if(!$res['status']){
			throw new Exception($res['error']);
		}
		$out['os'] = $res['data'][0];

		//Obtener datos de informe
		$res = $db->ExecuteQuery("SELECT
                                    uc.usua_nombre AS usua_creador,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
                                LEFT JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
                                WHERE
                                    info_modulo='OS' AND info_id=$info_id AND info_id_relacionado=$orse_id");
	    if( $res['status']==0 ){
	        throw new Exception($res['error']);
	    }

	    $res = ObtenerInforme($db,$info_id);
	    if(!$res['status']){
			throw new Exception($res['error']);
		}
		$out = array_merge($out,$res['data']);
		

		$filename = date("dmyHis")."_os_".$orse_id."_informe_".$info_id.".pdf";
		$path     = Upload::GenerateFilename($filename);
        $pathname = $path['abspath']; 
		$filename = $path['relpath'];

		$renderer = include("export_templates/os_informe.php");
		$html = $renderer(array("data"=>$out));
			
		$dompdf = new DOMPDF();
	    $dompdf->load_html($html);
	    $dompdf->set_base_path(realpath(__DIR__ . '/..'));
	    $dompdf->set_paper("letter","portrait");
        $dompdf->render();
        file_put_contents($pathname,$dompdf->output());
		$name     = sprintf("OS-%d-INFO-%03d",$orse_id,$info_id);
		$res = $db->ExecuteQuery("INSERT INTO repositorio SET
									repo_tipo_doc='INFORME',
									repo_nombre='$name',
									repo_tabla='informe',
									repo_tabla_id='$info_id',
									repo_ruta='$filename',
									repo_fecha_creacion=NOW(),
									repo_data='{\"orse_id\":$orse_id}',
									usua_creador=1");
		if(!$res['status']){
			unlink($pathname);
		}
	}
	catch (Exception $e) {
	   $res = array("status"=>false,"error"=>"Error al generar pdf: ".$e->getMessage());
	}
	return $res;
}


function PDFMNTInforme($db,$mant_id,$info_id){
	global $BASEDIR;

	require_once($BASEDIR."../libs/lightncandy/lightncandy.php");
    require_once($BASEDIR."../libs/dompdf/dompdf_config.inc.php");

    ini_set('display_errors', TRUE);

	try{
		//Obtener datos de os
		$res = MNTDetalle($db,$mant_id);
		if(!$res['status']){
			throw new Exception($res['error']);
		}
		$out['mantenimiento'] = $res['data'][0];

		//Obtener datos de informe
		$res = $db->ExecuteQuery("SELECT
                                    uc.usua_nombre AS usua_creador,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
                                INNER JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
                                WHERE
                                    info_modulo='MNT' AND info_id=$info_id AND info_id_relacionado=$mant_id");
	    if( $res['status']==0 ){
	        throw new Exception($res['error']);
	    }
	    
	    $res = ObtenerInforme($db,$info_id);
	    if(!$res['status']){
			throw new Exception($res['error']);
		}
		$out = array_merge($out,$res['data']);	

		$filename = date("dmyHis")."_mnt_".$mant_id."_informe_".$info_id.".pdf";
		$path     = Upload::GenerateFilename($filename);
		$pathname = $path['abspath']; 
		$filename = $path['relpath'];

		$renderer = include("export_templates/mnt_informe.php");
		$html = $renderer(array("data"=>$out));
			
		$dompdf = new DOMPDF();
	    $dompdf->load_html($html);
	    $dompdf->set_base_path(realpath(__DIR__ . '/..'));
	    $dompdf->set_paper("letter","portrait");
	    $dompdf->render();
    	file_put_contents($pathname,$dompdf->output());

		$name     = sprintf("MNT-%d-INFO-%03d",$mant_id,$info_id);
		$res = $db->ExecuteQuery("INSERT INTO repositorio SET
									repo_tipo_doc='INFORME',
									repo_nombre='$name',
									repo_tabla='informe',
									repo_tabla_id='$info_id',
									repo_ruta='$filename',
									repo_fecha_creacion=NOW(),
									repo_data='{\"mant_id\":$mant_id}',
									usua_creador=1");
		if(!$res['status']){
			unlink($pathname);
		}
	}
	catch (Exception $e) {
	   $res = array("status"=>false,"error"=>"Error al generar pdf: ".$e->getMessage());
	}
	return $res;
}


function PDFINSPInforme($db,$insp_id,$info_id){
	global $BASEDIR;

	require_once($BASEDIR."../libs/lightncandy/lightncandy.php");
    require_once($BASEDIR."../libs/dompdf/dompdf_config.inc.php");

    ini_set('display_errors', TRUE);

	try{
		//Obtener datos de os
		$res = INSPDetalle($db,$insp_id);
		if(!$res['status']){
			throw new Exception($res['error']);
		}
		$out['inspeccion'] = $res['data'][0];

		//Obtener datos de informe
		$res = $db->ExecuteQuery("SELECT
                                    uc.usua_nombre AS usua_creador,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id=informe.usua_creador)
                                INNER JOIN usuario uv ON (uv.usua_id=informe.usua_validador)
                                WHERE
                                    info_modulo='INSP' AND info_id=$info_id AND info_id_relacionado=$insp_id");
	    if( $res['status']==0 ){
	        throw new Exception($res['error']);
	    }
	    
	    $res = ObtenerInforme($db,$info_id);
	    if(!$res['status']){
			throw new Exception($res['error']);
		}
		$out = array_merge($out,$res['data']);	

		$filename = date("dmyHis")."_insp_".$insp_id."_informe_".$info_id.".pdf";
		$path     = Upload::GenerateFilename($filename);
		$pathname = $path['abspath']; 
		$filename = $path['relpath'];

		$renderer = include("export_templates/insp_informe.php");
		$html = $renderer(array("data"=>$out));
			
		$dompdf = new DOMPDF();
	    $dompdf->load_html($html);
	    $dompdf->set_base_path(realpath(__DIR__ . '/..'));
	    $dompdf->set_paper("letter","portrait");
	    $dompdf->render();
    	file_put_contents($pathname,$dompdf->output());

		$name     = sprintf("INSP-%d-INFO-%03d",$insp_id,$info_id);
		$res = $db->ExecuteQuery("INSERT INTO repositorio SET
									repo_tipo_doc='INFORME',
									repo_nombre='$name',
									repo_tabla='informe',
									repo_tabla_id='$info_id',
									repo_ruta='$filename',
									repo_fecha_creacion=NOW(),
									repo_data='{\"insp_id\":$insp_id}',
									usua_creador=1");
		if(!$res['status']){
			unlink($pathname);
		}
	}
	catch (Exception $e) {
	   $res = array("status"=>false,"error"=>"Error al generar pdf: ".$e->getMessage());
	}
	return $res;
}


//__________________________________________________
//test
//include("db.php");
//$db = new MySQL_Database();
//print_r(ExcelOSPresupuesto($db,26,14));
//print_r(PDFOSInforme($db,24,1));
//print_r(PDFMNTInforme($db,9,5));

?>

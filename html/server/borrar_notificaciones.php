<?php

	#IMPORTAR PHP PARA DB
	include("db.php");
	
	#CONEXION BBDD
	$db = new MySQL_Database();
	#ORDEN DE SERVICIO
	$res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' 
								WHERE noti_estado='DESPACHADA'
								AND noti_modulo='OS'
                    			AND noti_id_relacionado IN (SELECT orse_id 
										FROM orden_servicio 
										WHERE orse_estado IN ('ANULADA', 'APROBADA','RECHAZADA', 'NO REALIZADA'))
    							");
	##$dbo->Commit(); 


	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='ANULADA' 
              					WHERE tare_modulo='OS' 
              					AND tare_id_relacionado IN (SELECT orse_id FROM orden_servicio 
                                WHERE orse_estado IN ('NOACTIVO', 'NO REALIZADO'))
    						 ");
	#$dbo->Commit();

	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='CANCELADA' 
                  				WHERE tare_modulo='OS' 
                  					AND tare_estado IN ('CREADA','DESPACHADA','EJECUTANDO') 
                  					AND tare_id_relacionado IN (SELECT orse_id 
															    FROM orden_servicio orse_estado IN ('RECHAZADA', 'APROBADO') 
																AND orse_fecha_solicitud<= NOW())
							");
	#$dbo->Commit(); 

	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'REALIZADA' 
                             WHERE tare_modulo = 'OS' AND tare_tipo='VALIDAR_OS' AND tare_id_relacionado IN (SELECT orse_id FROM orden_servicio WHERE orse_estado IN ('APROBADA','RECHAZADA') and orse_fecha_solicitud<= NOW())
    						");

	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'REALIZADA' 
                             WHERE tare_modulo = 'OS' 
							 AND tare_tipo IN ('VALIDAR_INFORME', 'VALIDAR_SOLICITUD_INFORME') 
							 AND tare_id_relacionado IN (SELECT orse_id 
														 FROM orden_servicio WHERE orse_estado IN ('APROBADA','RECHAZADA') 
							 							 AND orse_fecha_solicitud<= NOW())
    						");
	#$dbo->Commit(); 
	#MANTENIMIENTO
	$res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' 
								WHERE noti_estado='DESPACHADA'
								AND noti_modulo='MNT'
                    			AND noti_id_relacionado IN (SELECT mant_id 
															FROM mantenimiento 
															WHERE mant_estado IN ('ANULADA', 'APROBADA','RECHAZADA', 'NO REALIZADA') 
															AND mant_fecha_programada <= NOW())
    						");
	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='ANULADA'
              					WHERE tare_modulo='MNT' 
              					AND tare_id_relacionado IN (SELECT mant_id 
															FROM mantenimiento 
															WHERE mant_estado IN ('ANULADA', 'NO REALIZADO'))
							");
	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'CANCELADA' 
                             WHERE tare_modulo = 'MNT' 
							 AND tare_tipo='VALIDAR_MNT' 
							 AND tare_id_relacionado IN (SELECT mant_id 
														 FROM mantenimiento 
														 WHERE mant_estado IN ('APROBADA','RECHAZADA') 
														 AND mant_fecha_programada<= NOW())
    						");

	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'REALIZADA' 
                             WHERE tare_modulo = 'MNT' 
							 AND tare_tipo IN ('VALIDAR_INFORME', 'VALIDAR_SOLICITUD_INFORME') 
							 AND tare_id_relacionado IN (SELECT mant_id 
														FROM mantenimiento 
														WHERE mant_estado IN ('APROBADA','RECHAZADA') 
														AND mant_fecha_programada<= NOW())
    						");
	#$dbo->Commit(); 
	#INSPECCIONES

	$res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' 
              					WHERE 
                    				noti_modulo='INSP'
                    				AND noti_estado='DESPACHADA'
                    				AND noti_id_relacionado IN (SELECT insp_id 
																FROM inspeccion 
																WHERE insp_estado='ANULADA' 
																AND insp_fecha_creacion <= NOW())
    						");

	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'REALIZADA' 
                             WHERE tare_modulo = 'INSP' 
							 AND tare_tipo IN ('VALIDAR_INFORME', 'VALIDAR_SOLICITUD_INFORME') 
							 AND tare_id_relacionado IN (SELECT insp_id 
														 FROM inspeccion 
														 WHERE insp_estado IN ('APROBADA','RECHAZADA') 
														 AND insp_fecha_solicitud<= NOW())
    						");
	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'REALIZADA' 
                              WHERE tare_modulo = 'INSP' AND tare_tipo = 'ASIGNAR' 
                              AND insp_id IN (SELECT insp_id 
												FROM inspeccion_asignacion
                             					where
                                					inas_fecha_asignacion <= NOW()
                                					AND inas_estado='ACTIVO')

    						");
	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado = 'REALIZADA' 
                              WHERE tare_modulo = 'INSP' 
							  AND tare_tipo = 'VALIDAR_INSP' 
                              AND insp_id IN (SELECT id_relacionado 
												FROM informe
                             					where
                             						info_estado='APROBADO'
                             						and info_fecha_validacion <= NOW()
                                				)

    						");
	#$dbo->Commit(); 

	$res = $db->ExecuteQuery("	UPDATE tarea SET tare_estado='ANULADA' 
			                  	WHERE tare_modulo='MNT' 
			                    ND tare_id_relacionado IN (
			                                SELECT mant_id 
											FROM mantenimiento 
			                                WHERE empl_id IN (SELECT empl_id 
															  FROM emplazamiento_visita 
															  WHERE emvi_estado = 'NOACTIVO') 
			                                				  AND mant_estado = 'CREADA')
			                    AND tare_tipo not IN ('VALIDAR_SOLICITUD_CAMBIO_FECHA_PROGRAMADA', 'VALIDAR_SOLICITUD_INFORME')
							");
	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE tarea SET tare_estado='ANULADA' 
			                  WHERE 
			                        tare_modulo='OS' 
			                        AND tare_id_relacionado IN (
			                                SELECT orse_id FROM orden_servicio 
			                                WHERE empl_id IN (SELECT empl_id 
															  FROM emplazamiento_visita 
															  WHERE emvi_estado = 'NOACTIVO') 
			                                AND orse_estado = 'CREADA')
			                    
							");

	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' 
			                 WHERE noti_modulo='MNT'
		                     AND noti_estado='DESPACHADA'
		                     AND noti_id_relacionado IN (
		                                SELECT mant_id 
									    FROM mantenimiento 
		                                WHERE empl_id IN (SELECT empl_id 
														FROM emplazamiento_visita 
														WHERE emvi_estado = 'NOACTIVO') 
		                                				AND mant_estado = 'CREADA')
							");

	#$dbo->Commit(); 
	$res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' 
	                 		WHERE noti_modulo='OS'
	                        AND noti_estado='DESPACHADA'
	                        AND noti_id_relacionado IN (
	                                	SELECT orse_id 
										FROM orden_servicio 
	                                	WHERE empl_id IN (SELECT empl_id 
													FROM emplazamiento_visita 
												    WHERE emvi_estado = 'NOACTIVO')
	                                				AND orse_estado = 'CREADA'
							");	
#$dbo->Commit(); 
?>

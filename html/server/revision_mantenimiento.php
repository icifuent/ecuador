<?php
include("db.php");

$FECHA = isset($argv[1])?$argv[1]:date("Y-m-d");


$db = new MySQL_Database();

//________________________________________________
echo "Obteniendo contratos\n";
$contracts = array();
$res = $db->ExecuteQuery("SELECT 
							cont_id,
							cont_nombre,
							DATE(cont_fecha_inicio) AS cont_fecha_inicio 
						  FROM contrato 
						  WHERE cont_estado='ACTIVO'");
if($res['status']){
	if(0<$res['rows']){
		$contracts = $res['data'];
	}
	else{
		echo("No se han definido contratos\n");
		exit(1);
	}
}
else{
	echo $res['error']."\n";
	exit(2);
}


foreach($contracts as $c){
	echo "Revisando MNTs contrato '".$c['cont_nombre']."'\n";
	$cont_id = $c['cont_id'];

	//________________________________________________
	echo "Obteniendo periodicidades para contrato\n";
	$periods = array();
	$res = $db->ExecuteQuery("SELECT 
								periodicidad.peri_id,
								periodicidad.peri_nombre,
								periodicidad.peri_meses,
								rcpe_dias_apertura,
								rcpe_dias_post_cierre
							FROM
							periodicidad
							INNER JOIN rel_contrato_periodicidad ON (rel_contrato_periodicidad.cont_id=$cont_id AND
															         periodicidad.peri_id = rel_contrato_periodicidad.peri_id)
							WHERE peri_meses IS NOT NULL");
	if($res['status']){
		if(0<$res['rows']){
			$periods = $res['data'];
		}
		else{
			echo("No se han definido periodicidades para el contrato\n");
			continue;
		}
	}
	else{
		echo $res['error']."\n";
		exit(2);
	}

	//calcular cantidad de meses desde inicio de contrato a fecha ingresada
	$d1 = new DateTime($c['cont_fecha_inicio']);
	$d2 = new DateTime($FECHA);
	$months = $d1->diff($d2)->m + ($d1->diff($d2)->y*12);
	$monthPeriod = $months%12;
	
	echo "Inicio del contrato: ".$c['cont_fecha_inicio']."\n";
	echo "Meses desde inicio contrato: $months\n";
	echo "Meses en este periodo anual: $monthPeriod\n";

	foreach($periods AS $p){
		if($months%$p['peri_meses']==0){
			$monthPeriod = ($months%12)/$p['peri_meses'];

			echo $p['peri_nombre']." $monthPeriod\n";

			if(0<=$months - $p['peri_meses']){
				$fecha_inicio = new DateTime($c['cont_fecha_inicio']);
				$fecha_inicio->add(new DateInterval("P".($months - $p['peri_meses'])."M")); 
				echo "Cerrando período iniciado en ".$fecha_inicio->format('Y-m-d')."\n";



				$fecha_inicio = new DateTime($c['cont_fecha_inicio']);
				$fecha_inicio->add(new DateInterval("P".$months."M")); 
				echo "Abriendo período iniciando en ".$fecha_inicio->format('Y-m-d')."\n";



			}

		}
	}






	echo "\n\n";
}




?>
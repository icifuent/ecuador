<?php

include("db.php");

echo "Inicio proceso de Exportar Informes KPI\n";

$db = new MySQL_Database();
$db->startTransaction();

$ruta_archivo 	= $argv[1];
$fecha_inicio 	= $argv[2];
$fecha_fin 		= $argv[3];
$mes 			= $argv[4];
$anyo 			= $argv[5];
 
$retError  = 0;
$retError1 = 0;
$retError2 = 0;
$retError3 = 0;
$retError4 = 0;

$delimiter = ';';
$enclosure = '\"';


//________________________________________________

	//Registros KPI Correctivos
	
	echo "******Informes KPI Correctivo******\n";

	$headerCorrectivo = "SELECT * FROM (
		SELECT
		  'OS_id'
		  ,'Contrato'
		  ,'EECC'
		  ,'Tipo'
		  ,'Estado'
		  ,'Indisponibilidad'
		  ,'Fechacreacion'
		  ,'FechaProgramada'
		  ,'FechaSubidaServicio'
		  ,'Region'
		  ,'Especialidad'
		  ,'Alarma'
		  ,'Emplazamiento_id'
		  ,'FechaCreacionPresupuesto'
		  ,'FechaValidacionPresupuesto'
		  ,'FechaCreacionInforme'
		  ,'FechaValidacionInforme' 
		  ,'FechaValidacionOS'
		  ,'FechaIngresoSitio'
		  ,'FechaSalidaSitio'
		  ,'FechaCierre'
		  ,'Severidad'	
		  ,'CausaAfectacion'	
		  ,'GrupoInfraestructura'	
		  ,'OrdenAnterior'	
		  ,'ProblemaAccesoSitio'
		  ,'ContadorVisitas'
		  ,'CausaApertura'
		  ,'CausaCierre'
		  ,'FechaInstalacionEquipo'
		  ,'NecesidadRepuesto'
		  ,'ParadasReloj'
	UNION ALL "; 
	$sqlCorrectivo = "SELECT
							orden_servicio.orse_id AS 'OS_id'
							,contrato.cont_nombre AS 'Contrato'
							, ( Select em.empr_nombre 
								from empresa em 
								, rel_contrato_empresa r 
								where r.empr_id = em.empr_id 
								and r.cont_id = contrato.cont_id
								and em.empr_estado='ACTIVO'
				                and r.coem_tipo ='CONTRATISTA'
							)'EECC'
							,orden_servicio.orse_tipo AS 'Tipo'
							,IF(orden_servicio.orse_estado IN ('NOACTIVO','ANULADA'),'ANULADO', IF(orden_servicio.orse_estado IN ('RECHAZADA','APROBADA'),'FINALIZADO','ACTIVO'))AS 'Estado'
							,orden_servicio.orse_indisponibilidad AS 'Indisponibilidad'
							,orden_servicio.orse_fecha_creacion AS 'Fechacreacion'
							,orden_servicio.orse_fecha_solicitud AS 'FechaProgramada'
							,subida_servicio AS 'FechaSubidaServicio'
							,region.regi_nombre AS 'Region'
							,especialidad.espe_nombre AS 'Especialidad'
							,sube_nombre AS 'Alarma'
							,emplazamiento.empl_id AS 'Emplazamiento_id'
							,(SELECT MAX(pres_fecha_creacion) 
								FROM presupuesto 
							 WHERE presupuesto.orse_id = orden_servicio.orse_id ) AS 'FechaCreacionPresupuesto'
							,(SELECT MAX(pres_fecha_validacion) 
								FROM presupuesto 
							 WHERE presupuesto.orse_id = orden_servicio.orse_id ) AS 'FechaValidacionPresupuesto'
							,(SELECT MAX(info_fecha_creacion) 
								FROM informe 
							 WHERE informe.info_id_relacionado = orden_servicio.orse_id
							 AND informe.info_modulo ='OS') AS 'FechaCreacionInforme'
							,(SELECT MAX(info_fecha_validacion) 
								FROM informe 
							 WHERE informe.info_id_relacionado = orden_servicio.orse_id
							 AND informe.info_modulo ='OS') AS 'FechaValidacionInforme' 
							,orden_servicio.orse_fecha_validacion AS  'FechaValidacionOS'
							,emplazamiento_visita.emvi_fecha_ingreso AS 'FechaIngresoSitio'
							,emplazamiento_visita.emvi_fecha_salida AS 'FechaSalidaSitio'
							,orden_servicio.orse_fecha_validacion AS  'FechaCierre'
							,(select case when orden_servicio.orse_tipo = 'OSEU' or orden_servicio.orse_tipo = 'OSGU' then 'CRITICO'			
								else 'MAYOR' end) as 'Severidad'			
							, null as 'CausaAfectacion'	
							,'ACCESO MÓVIL' as 'GrupoInfraestructura'
							,(select max(ant.orse_id)					
								from orden_servicio ant 			
							where ant.empl_id = orden_servicio.empl_id 			
				            and ant.sube_id = orden_servicio.sube_id 								
				            and ant.orse_id < orden_servicio.orse_id
				            and year(ant.orse_fecha_creacion) = year('$fecha_inicio 00:00:00' - INTERVAL 1 MONTH)
							and month(ant.orse_fecha_creacion) = month('$fecha_inicio 00:00:00' - INTERVAL 1 MONTH)   
							) as 'OrdenAnterior'	
							,(select  case when fova_valor = 'SI' then 'NO'
							else  'SI' end									
							from tarea tar
							INNER JOIN rel_tarea_formulario_respuesta rel_for ON rel_for.tare_id = tar.tare_id							
							INNER JOIN formulario_grupo ON (formulario_grupo.form_id=rel_for.form_id)									
							INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)									
							LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=rel_for.fore_id)									
							where tar.tare_modulo = 'OS'									
							and tar.tare_id_relacionado = orden_servicio.orse_id									
							and formulario_valor.foit_id = 1 									
							and formulario_grupo.form_id = 1
							order by tar.tare_id desc
							limit 1														
							) as 'ProblemaAccesoSitio'
							,(SELECT count(tare_id_relacionado) 			
							FROM tarea						
							INNER JOIN usuario ON (usuario.usua_id=tarea.usua_id)						
							WHERE tare_modulo='OS' 						
							AND tare_tipo='VISITAR_SITIO' 						
							and tare_id_relacionado = orden_servicio.orse_id						
							group by tare_id_relacionado) as 'ContadorVisitas' 						
							, null as 'CausaApertura'
							, null as 'CausaCierre'
							, null as 'FechaInstalacionEquipo'
							, null as 'NecesidadRepuesto'
							, 0 as 'ParadasReloj'
						FROM 
							orden_servicio
							INNER JOIN contrato on (contrato.cont_id= orden_servicio.cont_id)
							INNER JOIN subespecialidad ON ( orden_servicio.orse_tipo <> 'OSI') AND (subespecialidad.sube_id = orden_servicio.sube_id)
							INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
							INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
							INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
							INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
							INNER JOIN region ON (region.regi_id = provincia.regi_id)
							LEFT JOIN sla_orden_servicio ON (sla_orden_servicio.orse_id = orden_servicio.orse_id)
							LEFT JOIN emplazamiento_visita ON (emplazamiento_visita.emvi_id = (SELECT emvi_id  FROM emplazamiento_visita 
																					WHERE emvi_modulo='OS' AND emvi_id_relacionado=orden_servicio.orse_id 
																					AND emplazamiento_visita.empl_id=orden_servicio.empl_id 
																					ORDER BY emvi_id DESC LIMIT 1))	
					WHERE 	orden_servicio.cont_id = 1			
						AND orden_servicio.orse_tipo in ('OSEU', 'OSGU')
						AND orden_servicio.orse_fecha_solicitud >= '$fecha_inicio 00:00:00'
						AND orden_servicio.orse_fecha_solicitud <= '$fecha_fin 23:59:59') AS RESULTADO;";

   /* $exportCorrectivo=  " INTO OUTFILE '$ruta_archivo/CHI_FLM_CORRECTIVO_INCIDENCIAS_$anyo$mes.csv'
        character set latin1
		FIELDS TERMINATED BY ';'
		OPTIONALLY ENCLOSED BY '\"'
		LINES TERMINATED BY '\n'; ";
		
	$query=$headerCorrectivo . $sqlCorrectivo . $exportCorrectivo;*/
	
	$query=$headerCorrectivo . $sqlCorrectivo;

	$res = $db->ExecuteQuery($query);

	if($res['status']==0)
	{
		$retError1=1;
		echo "Error al kpi correctivo==> ";
		echo $res['error']."\n";
	};

	$fp = fopen($ruta_archivo . '/CHI_FLM_CORRECTIVO_INCIDENCIAS_' . $anyo . $mes . '.csv', 'w');
	//add BOM to fix UTF-8 in Excel
	fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
	
	foreach ($res['data'] as $fields) 
	{
		fputcsv($fp, $fields , $delimiter);
	}

	fclose($fp);
	
	//KPI Bajo Demanda
	echo "******Informes KPI baja demanda******\n";
	$headerBaja= "SELECT * FROM (
				SELECT
				  'OS_id'
				  ,'Contrato'
				  , 'EECC'
				  ,'Tipo'
				  ,'Estado'
				  ,'Indisponibilidad'
				  ,'Fechacreacion'
				  ,'FechaProgramada'
				  ,'FechaSubidaServicio'
				  ,'Region'
				  ,'Especialidad'
				  ,'Alarma'
				  ,'Emplazamiento_id'
				  ,'FechaCreacionPresupuesto'
				  ,'FechaValidacionPresupuesto'
				  ,'FechaCreacionInforme'
				  ,'FechaValidacionInforme' 
				  ,'FechaValidacionOS'
				  ,'FechaIngresoSitio'
				  ,'FechaSalidaSitio'
				  ,'FechaCierre'
				  ,'ParadasReloj'
		UNION ALL ";
	$sqlBaja = " SELECT
				  orden_servicio.orse_id AS 'OS_id'
				  ,contrato.cont_nombre AS 'Contrato'
				  , ( Select em.empr_nombre 
						from empresa em 
						, rel_contrato_empresa r 
						where r.empr_id = em.empr_id 
						and r.cont_id = 1
						and em.empr_estado='ACTIVO'
		                and r.coem_tipo ='CONTRATISTA'
					  ) AS 'EECC'
				  ,orden_servicio.orse_tipo AS 'Tipo'
				  ,IF(orden_servicio.orse_estado IN ('NOACTIVO','ANULADA'),'ANULADO', IF(orden_servicio.orse_estado IN ('RECHAZADA','APROBADA'),'FINALIZADO','ACTIVO'))AS 'Estado'
				  ,orden_servicio.orse_indisponibilidad AS 'Indisponibilidad'
				  ,orden_servicio.orse_fecha_creacion AS 'Fechacreacion'
				  ,orden_servicio.orse_fecha_solicitud AS 'FechaProgramada'
				  ,subida_servicio AS 'FechaSubidaServicio'
				  ,region.regi_nombre AS 'Region'
				  ,especialidad.espe_nombre AS 'Especialidad'
				  ,sube_nombre AS 'Alarma'
				  ,emplazamiento.empl_id AS 'Emplazamiento_id'
				  ,(SELECT MAX(pres_fecha_creacion) 
						FROM presupuesto 
					 WHERE presupuesto.orse_id = orden_servicio.orse_id ) AS 'FechaCreacionPresupuesto'
				  ,(SELECT MAX(pres_fecha_validacion) 
						FROM presupuesto 
					 WHERE presupuesto.orse_id = orden_servicio.orse_id ) AS 'FechaValidacionPresupuesto'
				  ,(SELECT MAX(info_fecha_creacion) 
						FROM informe 
					 WHERE informe.info_id_relacionado = orden_servicio.orse_id
					 AND informe.info_modulo ='OS') AS 'FechaCreacionInforme'
				  ,(SELECT MAX(info_fecha_validacion) 
						FROM informe 
					 WHERE informe.info_id_relacionado = orden_servicio.orse_id
					 AND informe.info_modulo ='OS') AS 'FechaValidacionInforme' 
				  ,orden_servicio.orse_fecha_validacion AS  'FechaValidacionOS'
				  ,emplazamiento_visita.emvi_fecha_ingreso AS 'FechaIngresoSitio'
				  ,emplazamiento_visita.emvi_fecha_salida AS 'FechaSalidaSitio'
				  ,orden_servicio.orse_fecha_validacion AS  'FechaCierre'
				  ,0 as 'ParadasReloj'
				FROM 
					orden_servicio
					INNER JOIN contrato on (contrato.cont_id= orden_servicio.cont_id)
					INNER JOIN subespecialidad ON ( orden_servicio.orse_tipo <> 'OSI') AND (subespecialidad.sube_id = orden_servicio.sube_id)
					INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
					INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
					INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
					INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
					INNER JOIN region ON (region.regi_id = provincia.regi_id)
					LEFT JOIN sla_orden_servicio ON (sla_orden_servicio.orse_id = orden_servicio.cont_id)
					LEFT JOIN emplazamiento_visita ON (emplazamiento_visita.emvi_id = (SELECT emvi_id  FROM emplazamiento_visita 
											WHERE emvi_modulo='OS' AND emvi_id_relacionado=orden_servicio.orse_id 
											AND emplazamiento_visita.empl_id=orden_servicio.empl_id ORDER BY emvi_id DESC LIMIT 1))
					WHERE orden_servicio.cont_id = 1
						AND orden_servicio.orse_tipo in ('OSEN' , 'OSGN')
						AND orden_servicio.orse_fecha_solicitud >= '$fecha_inicio 00:00:00'
						AND orden_servicio.orse_fecha_solicitud <= '$fecha_fin 23:59:59') AS RESULTADO;";
  
	/*$exportBaja =" INTO OUTFILE '$ruta_archivo/CHI_FLM_SERVICIOS_BAJO_DEMANDA_$anyo$mes.csv'
        character set latin1
		FIELDS TERMINATED BY ';'
		OPTIONALLY ENCLOSED BY '\"'
		LINES TERMINATED BY '\n'; ";
		
	$query=$headerBaja . $sqlBaja . $exportBaja;*/

	$query = $headerBaja . $sqlBaja;
		
	$res = $db->ExecuteQuery($query);

	if($res['status']==0)
	{
		$retError2=1;
		echo "Error al kpi baja==> ";
		echo $res['error']."\n";
	};	

	$fp = fopen($ruta_archivo . '/CHI_FLM_SERVICIOS_BAJO_DEMANDA_' . $anyo . $mes . '.csv', 'w');
	//add BOM to fix UTF-8 in Excel
	fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		
	foreach ($res['data'] as $fields) 
	{
		fputcsv($fp, $fields , $delimiter);
	}

	fclose($fp);
	
	//KPI Preventivo
	
	echo "******Informes KPI preventivo******\n";

	$headerPreventivo="SELECT * FROM (
						SELECT   'mant_id'
								,'Contrato'
			                    ,'EECC MNT'
								,'peri_nombre' 
								,'Estado proceso MNT'
								,'Fecha proceso MNT'
								,'Estado MNT'
								,'mant_responsable'
								,'Especialidad MNT'
								,'SubEspecialidad MNT'
								,'mant_fecha_programada'
								,'Fecha ingreso sitio'
								,'Fecha salida sitio'
								,'mant_fecha_ejecucion' 
								,'mant_fecha_validacion'
								,'regi_nombre'
								,'empl_id'
								,'empl_nombre'
								,'clas_nombre'
								,'usua_creador'
								,'usua_creador_nombre'
								,'usua_validador'
								,'usua_validador_nombre'
								,'Zona MOVISTAR'
								,'ZONA CONTRATO CIM'
								,'tipo_mantenimiento'
								,'grupo_infraestructura'
								,'probl_accesositio'
				UNION ALL ";
	$sqlPreventivo = " SELECT
						mantenimiento.mant_id 
	                    ,contrato.cont_nombre
	                    ,empresa.empr_nombre AS 'EECC MNT'
						,peri_nombre 
						,mant_estado 'Estado proceso MNT'
						,(SELECT MAX(tare_fecha_despacho) 
								FROM tarea 
						 WHERE tare_modulo='MNT' 
						 AND tare_id_relacionado=mantenimiento.mant_id) AS 'Fecha proceso MNT'
						,IF(mant_estado IN ('NOACTIVO', 'ANULADA'),'ANULADO', IF(mant_estado IN ('RECHAZADA','APROBADA','NO REALIZADO'),'FINALIZADO','ACTIVO')) AS 'Estado MNT'
						,mant_responsable
						,especialidad.espe_nombre AS 'Especialidad MNT'
						,'' AS 'Subespecialidad MNT'
						,mant_fecha_programada
						,emplazamiento_visita.emvi_fecha_ingreso AS 'Fecha ingreso sitio'
						,emplazamiento_visita.emvi_fecha_salida AS 'Fecha salida sitio'
						,mant_fecha_ejecucion 
						,mant_fecha_validacion
						,regi_nombre
						,emplazamiento.empl_id
						,emplazamiento.empl_nombre
						,clas_nombre
						#,usuario_creador.usua_id AS 'usua_creador'
						#,usuario_creador.usua_nombre AS 'usua_creador_nombre'
						,null AS 'usua_creador'
						,null AS 'usua_creador_nombre'
						#,usuario_validador.usua_id AS 'usua_validador'
						#,usuario_validador.usua_nombre AS 'usua_validador_nombre'
						,null AS 'usua_validador'
						,null AS 'usua_validador_nombre'
						,(SELECT 
								zona_nombre
						FROM rel_zona_emplazamiento 
						INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR' AND zona.cont_id = 1)
						WHERE rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
						LIMIT 1) AS 'Zona MOVISTAR'
						,(SELECT 
								zona_nombre
						FROM rel_zona_emplazamiento 
						INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO' AND zona.cont_id = 1)
						WHERE rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
						LIMIT 1) AS 'ZONA DE CONTRATO CIM'	
						, 'MANTENIMIENTO PREVENTIVO INTEGRAL POR SITIO' as 'tipo_mantenimiento'
						, 'ACCESO MÓVIL' as 'grupo_infraestructura'
						,(select  case when fova_valor = 'SI' then 'NO'
									   else  'SI' end
						from tarea tar
						INNER JOIN rel_tarea_formulario_respuesta rel_for ON rel_for.tare_id = tar.tare_id							
						INNER JOIN formulario_grupo ON (formulario_grupo.form_id=rel_for.form_id)									
						INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)									
						LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=rel_for.fore_id)									
						where tar.tare_modulo = 'MNT'									
						and tar.tare_id_relacionado = mantenimiento.mant_id
						and formulario_valor.foit_id = 1 									
						and formulario_grupo.form_id = 1
						order by tar.tare_id desc
						limit 1														
						) as 'probl_accesositio'			
			FROM 	contrato
			INNER JOIN mantenimiento ON (mantenimiento.cont_id = contrato.cont_id)
			INNER JOIN emplazamiento ON (emplazamiento.empl_id = mantenimiento.empl_id)   
			INNER JOIN empresa ON (empresa.empr_id = mantenimiento.empr_id) 
			INNER JOIN especialidad ON (especialidad.espe_id = mantenimiento.espe_id)   
			INNER JOIN mantenimiento_periodos ON (mantenimiento_periodos.mape_id = mantenimiento.mape_id)
			INNER JOIN rel_contrato_periodicidad ON (rel_contrato_periodicidad.rcpe_id = mantenimiento_periodos.rcpe_id) 
			INNER JOIN periodicidad ON (periodicidad.peri_id = rel_contrato_periodicidad.peri_id)	
			INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
			INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
			INNER JOIN region ON (region.regi_id = provincia.regi_id)
			INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = mantenimiento.clas_id)
			INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id = mantenimiento.empl_id 
						AND rel_contrato_emplazamiento.cont_id = mantenimiento.cont_id )
			LEFT JOIN usuario AS usuario_creador ON (usuario_creador.usua_id=mantenimiento.usua_creador)					
			LEFT JOIN usuario AS usuario_validador ON (usuario_validador.usua_id=mantenimiento.usua_validador)	
			LEFT JOIN emplazamiento_visita ON 
						(emplazamiento_visita.emvi_id = (SELECT emvi_id  
											FROM emplazamiento_visita 
											WHERE emvi_modulo='MNT' 
											AND emvi_id_relacionado=mantenimiento.mant_id 
											AND emplazamiento_visita.empl_id=mantenimiento.empl_id 
											ORDER BY emvi_id DESC LIMIT 1))
								
			WHERE mantenimiento.cont_id = 1 
			AND mant_fecha_programada >= '$fecha_inicio 00:00:00'
			AND mant_fecha_programada <= '$fecha_fin 23:59:59') AS RESULTADO;";
                        
	/*$exportPreventivo =" INTO OUTFILE '$ruta_archivo/CHI_FLM_MNTO_PREVENT_$anyo$mes.csv'
        character set latin1
		FIELDS TERMINATED BY ';'
		OPTIONALLY ENCLOSED BY '\"'
		LINES TERMINATED BY '\n'; ";
		
	$query=$headerPreventivo . $sqlPreventivo . $exportPreventivo;*/

	$query=$headerPreventivo . $sqlPreventivo;

	$res = $db->ExecuteQuery($query);

	if($res['status']==0)
	{
		$retError3=1;
		echo "Error al kpi Preventivo==> ";
		echo $res['error']."\n";
	};		
	
	$fp = fopen($ruta_archivo . '/CHI_FLM_MNTO_PREVENT_' . $anyo . $mes . '.csv', 'w');
	//add BOM to fix UTF-8 in Excel
	fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
	
	foreach ($res['data'] as $fields) 
	{
		fputcsv($fp, $fields , $delimiter);
	}
	fclose($fp);

	//KPI Sitios
	echo "******Informes KPI sitio******\n";
	
	$headerSitio= "SELECT * FROM (
				SELECT   'empl_id'
						, 'Contrato'
						, 'EECC'
						, 'e.empl_nombre'
						, 'comu_nombre'
						, 'ZonaContrato'
						, 'ZonaMovistar'
						, 'empl_direccion'
						, 'empl_tipo'
						, 'empl_macrositio'
						, 'empl_subtel'
						, 'clasificacion_mpp'
						, 'empl_latitud'
						, 'empl_longitud'
						, 'empl_estado'
						, 'empl_subestado'
	                    , 'grupo_infraestructura'	
	                    , 'sub_grupoinfraestructura'
				UNION ALL ";

	$sqlSitio = " SELECT DISTINCT
					  e.empl_id
					, c.cont_nombre 
					, ( Select em.empr_nombre 
						from empresa em 
						, rel_contrato_empresa r 
						where r.empr_id = em.empr_id 
						and r.cont_id = c.cont_id
						and em.empr_estado='ACTIVO'
		                and r.coem_tipo ='CONTRATISTA'
					  )'EECC'
					, e.empl_nombre
					, comu_nombre
					, zc.zona_nombre ZonaContrato
					, zm.zona_nombre ZonaMovistar
					, e.empl_direccion
					, e.empl_tipo
					, e.empl_macrositio
					, e.empl_subtel
					, cp.clpr_nombre Clasificacion_mpp
					, e.empl_latitud
					, e.empl_longitud
					, e.empl_estado
					, e.empl_subestado
					, 'ACCESO MÓVIL' as 'grupo_infraestructura'	
		            , (select case when tecn_nombre = 'GSM'  then '2G'
								   when tecn_nombre = 'UMTS' then '3G'
								   when tecn_nombre = 'LTE'  then '4G'
								   else 'OTROS' end 
					   from rel_emplazamiento_tecnologia rel_e 
					   , tecnologia t 
					   where rel_e.empl_id = e.empl_id
					   and t.tecn_id = rel_e.tecn_id
					   limit 1
					 ) as 'sub_grupoinfraestructura'
					
					from emplazamiento e
					INNER JOIN comuna ON comuna.comu_id = e.comu_id
					INNER JOIN rel_zona_emplazamiento rel_zc ON rel_zc.empl_id = e.empl_id 
					INNER JOIN zona zc ON zc.zona_tipo='CONTRATO' 
										and zc.zona_id = rel_zc.zona_id 
										and zc.cont_id=1
					INNER JOIN rel_zona_emplazamiento rel_zm ON rel_zm.empl_id = e.empl_id 
					INNER JOIN zona zm ON zm.zona_tipo='MOVISTAR' 
										and zm.zona_id = rel_zm.zona_id 
										and zm.cont_id=1
					INNER JOIN rel_contrato_emplazamiento rel_ce ON rel_ce.empl_id = e.empl_id
					INNER JOIN contrato c ON c.cont_id = rel_ce.cont_id
					INNER JOIN clasificacion_programacion cp ON cp.clpr_id = rel_ce.clpr_id 
															 and cp.clpr_activo = 'ACTIVO' 
															 and cp.cont_id=rel_ce.cont_id 
					
					where e.empl_fecha_creacion <= '$fecha_fin 23:59:59'
					and e.empl_estado = 'ACTIVO'
		            and c.cont_id=1) AS RESULTADO;";                 
    
	/*$exportSitio =" INTO OUTFILE '$ruta_archivo/CHI_FLM_SITIOS_ACTIVOS_$anyo$mes.csv'
	character set latin1
	FIELDS TERMINATED BY ';'
	OPTIONALLY ENCLOSED BY '\"'
	LINES TERMINATED BY '\n'; ";

	$query=$headerSitio . $sqlSitio . $exportSitio;*/

	$query=$headerSitio . $sqlSitio;

	$res = $db->ExecuteQuery($query);

	if($res['status']==0)
	{
		$retError4=1;
		echo "Error al kpi Sitios==> ";
		echo $res['error']."\n";
	};	

	$fp = fopen($ruta_archivo . '/CHI_FLM_SITIOS_ACTIVOS_' . $anyo . $mes . '.csv', 'w');
	//add BOM to fix UTF-8 in Excel
	fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
	
	foreach ($res['data'] as $fields) 
	{
		fputcsv($fp, $fields , $delimiter);
	}

	fclose($fp);
	
	if($retError1==1 || $retError2==1 || $retError3==1 || $retError4==1){
		$retError=1;
	}
echo "Fin de Proceso de Generacion de Informes KPI\n";
exit($retError);

?>
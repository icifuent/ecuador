<?php

	#IMPORTAR PHP PARA DB
	include("db.php");

	#VARIABLES
	$archivo_mnt         	= $argv[1];
	$fechaInicio         	= $argv[2];
	$fechaFin         		= $argv[3];

	#CONEXION BBDD
	$db = new MySQL_Database();
	
	echo "/n";
	echo "MNT REPORTE:";
	echo "/n";
	
	#MNT
	$query=("SELECT * from (
										SELECT 		'Num. MNT',
													'Contrato',
													'Periodicidad',
													'Especialidad',
													'Empresa',
													'fecha_ingreso',
													'fecha_salida',
													'Usuario',
													'Emplazamiento',
													'Direccion',
													'Macrositio',
													'SUBTEL',
													'Requiere Acceso EMP.',
													'Permisos de acceso EMP.',
													'Propietario Torre EMP.',
													'Clasificacion',
													'CLPR_NOMBRE',
													'Fecha Programada MNT',
													'Fecha Ejecucion MNT',
													'Fecha Proceso MNT',
													'Estado',
													'Responsable',
										            'Fecha Validacion',
													'Descripción',
										            'SLA',
										            'Observacion',
										            'Zona MOVISTAR',
													'ZONA DE CONTRATO CIM',
													'Cluster',
													'Responsable EMP.'


										UNION ALL 

												SELECT distinct
													mant_id,
										            cont_nombre,
										            peri_nombre,
										            espe_nombre,
										            empr_nombre,
										            emvi_fecha_ingreso,
													emvi_fecha_salida,
													usua_creador_nombre,
													empl_nombre,
													empl_direccion,
													empl_macrositio,
													empl_subtel,
													empl_requiere_acceso,
										            empl_permisos_acceso,
										            duto_nombre,
										            clas_nombre,
										            clpr_nombre,
													mant_fecha_programada,
										            mant_fecha_ejecucion,
										        	tare_fecha_despacho,
										            mant_estado_proceso,
													mant_responsable,
										            mant_fecha_validacion,
													mant_descripcion,
										            mant_sla_incluida,
										            mant_observacion,
													zona_movistar,
													zona_contrato,
													cluster,
													responsable_emp

										            FROM 
										            mnt_consolidado            
												WHERE mant_fecha_programada >= '" .$fechaInicio ."' AND mant_fecha_programada <= '" .$fechaFin 
												."' ) AS RESULTADO INTO OUTFILE '" .$archivo_mnt ."'
												CHARACTER SET latin1
												FIELDS TERMINATED BY ';'			
												LINES TERMINATED BY '\n'");
	echo $query;
	$res = $db->ExecuteQuery($query);
?>
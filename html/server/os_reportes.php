<?php

	#IMPORTAR PHP PARA DB
	include("db.php");

	#VARIABLES
	$archivo_os         	= $argv[1];
	$archivo_os_sap         = $argv[2];
	$fechaInicio         	= $argv[3];
	$fechaFin         		= $argv[4];
	$fechaInicioSap         = $argv[5];
	$fechaFinSap         	= $argv[6];

	#CONEXION BBDD
	$db = new MySQL_Database();
	
	echo "/n";
	echo "OS REPORTE:";
	echo "/n";
	
	#OS
	$query=("SELECT * from (
										SELECT 		'Num. OS',
													'Tipo OS',
													'Especialidad OS',
													'Alarma OS',
													'EECC OS',
													'Usuario creador OS',
													'Fecha creacion OS',
													'Fecha programada OS',
													'Fecha ingreso sitio',
													'Fecha salida sitio',
													'Fecha finalizacion OS',
													'Jefe cuadrilla asignado OS',
													'Descripcion OS',
													'Tecnicos asignado OS',
													'Valor total Ultimo Ppto.',
													'Estado proceso OS',        
													'Estado OS',
													'Nombre EMP.',
													'Direccion EMP.',
													'Clasificacion EMP.',
													'Clasificacion Programacion EMP.',
													'Dueño de Torre EMP.',
													'Requiere Acceso EMP.',
													'Permisos de acceso EMP.',
													'Region EMP.',
													'Zona MOVISTAR',
													'ZONA DE CONTRATO CIM',
													'Cluster',
													'Responsable EMP.',
													'Codigo Incidencia'	

										UNION ALL 
												SELECT
										              orse_id AS 'Num. OS',
										              orse_tipo AS 'Tipo OS',
										              espe_nombre AS 'Especialidad OS',
										              sube_nombre AS 'Alarma OS',
										              empr_nombre AS 'EECC OS',
										              usua_creador AS 'Usuario creador OS',
										              orse_fecha_creacion AS 'Fecha creacion OS',
										              orse_fecha_solicitud AS 'Fecha programada OS',
										              emvi_fecha_ingreso AS 'Fecha ingreso sitio',
										              emvi_fecha_salida AS 'Fecha salida sitio',
										              orse_fecha_validacion AS 'Fecha finalizacion OS',
										              usua_jefe_cuadrilla AS 'Jefe cuadrilla asignado OS',
													  REPLACE(REPLACE(orse_descripcion, '\r', ''), '\n', '') AS 'Descripcion OS',
										              orse_tecnico_asignado AS 'Tecnicos asignado OS',
										              valor_ult_presupuesto AS 'Valor total Ultimo Ppto.',
										              orse_estado_proceso AS 'Estado proceso OS',        
										              orse_estado AS 'Estado OS',
										              empl_nombre AS 'Nombre EMP.',
										              empl_direccion AS 'Direccion EMP.',
										              clas_nombre AS 'Clasificacion EMP.',
													  clpr_nombre AS 'Clasificacion Programacion EMP.',
										              duto_nombre AS 'Dueño de Torre EMP.',
										              requiere_acceso_emp AS 'Requiere Acceso EMP.',
										              permisos_acceso_emp AS 'Permisos de acceso EMP.',
										              regi_nombre AS 'Region EMP.',
										              zona_movistar AS 'Zona MOVISTAR',
										              zona_contrato AS 'ZONA DE CONTRATO CIM',
										              cluster AS 'Cluster',
										              responsable_emp AS 'Responsable EMP.',
										              orse_tag as 'Codigo Incidencia'										                                    
										            FROM 
										            os_consolidado            
												WHERE orse_fecha_solicitud >= '" .$fechaInicio ."' AND orse_fecha_solicitud <= '" .$fechaFin 
												."' ) AS RESULTADO INTO OUTFILE '" .$archivo_os ."'
												CHARACTER SET latin1
												FIELDS TERMINATED BY ';'			
												LINES TERMINATED BY '\n'");
	echo $query;
	
	$res = $db->ExecuteQuery($query);
	
	echo "/n";
	echo "OS_SAP REPORTE";
	echo "/n";
	#OS_SAP
	$query=("SELECT * from (


							select 
										'Num. OS',
										'Tipo OS',
										'Especialidad OS',
										'Alarma OS',
										'EECC OS',
										'Usuario creador OS',
										'Fecha creacion OS',
										'Fecha solicitud OS',
										'Jefe Cuadrilla',
										'Estado proceso OS',
										'Fecha ejecucion OS',
										'Estado OS',
										'Nombre EMP.',
										'Direccion EMP.',
										'Clasificacion EMP.',
										'Dueño de Torre EMP.',
										'Requiere Acceso EMP.',
										'Permisos de acceso EMP.',
										'Region EMP.',
										'Zona Movistar',
										'ZONA DE CONTRATO CIM',
										'Cluster',
										'Responsable EMP.',
										'Id LPU',
										'Grupo LPU', 
										'Item LPU',
										'SAP OPEX LPU',
										'SAP CAPEX LPU',
										'Valor LPU',
										'Cantidad LPU',
										'Subtotal LPU',
										'Codigo Incidencia'
							UNION ALL
							
							SELECT
					                    os_consolidado.orse_id AS 'Num. OS',
					                    orse_tipo AS 'Tipo OS',
					                    espe_nombre AS 'Especialidad OS',
					                    sube_nombre AS 'Alarma OS',
					                    empr_nombre AS 'EECC OS',
					                    usua_creador AS 'Usuario creador OS',
					                    orse_fecha_creacion AS 'Fecha creacion OS',
					                    orse_fecha_solicitud AS 'Fecha solicitud OS',
					                    usua_jefe_cuadrilla AS 'Jefe Cuadrilla',
					                    orse_estado AS 'Estado proceso OS',
					                    tare_fecha_despacho AS 'Fecha ejecucion OS',
					                    orse_estado AS 'Estado OS',
					                    empl_nombre AS 'Nombre EMP.',
					                    empl_direccion AS 'Direccion EMP.',
					                    clas_nombre AS 'Clasificacion EMP.',
					                    duto_nombre AS 'Dueño de Torre EMP.',
					                    requiere_acceso_emp AS 'Requiere Acceso EMP.',
					                    permisos_acceso_emp AS 'Permisos de acceso EMP.',
					                    regi_nombre AS 'Region EMP.',
					                    zona_movistar AS 'Zona Movistar',
					                    zona_contrato AS 'ZONA DE CONTRATO CIM',
					                    cluster AS 'Cluster',
					                    responsable_emp AS 'Responsable EMP.',
					                    lpip_id AS 'Id LPU',
					                    lpgr_nombre AS 'Grupo LPU', 
					                    REPLACE(REPLACE(lpit_nombre, '\r', ''), '\n', '') AS 'Item LPU',
					                    lpip_sap_opex AS 'SAP OPEX LPU',
					                    lpip_sap_capex AS 'SAP CAPEX LPU',
					                    lpip_precio AS 'Valor LPU',
					                    prit_cantidad AS 'Cantidad LPU',
					                    lpip_precio*prit_cantidad AS 'Subtotal LPU',
					                    os_consolidado.orse_tag as 'Codigo Incidencia'                     
					                  FROM os_consolidado
					                  INNER JOIN os_consolidado_sap ON (os_consolidado.orse_id = os_consolidado_sap.orse_id)   
										WHERE orse_fecha_solicitud >='" .$fechaInicioSap 
										."' AND orse_fecha_solicitud <= '" .$fechaFinSap ."') AS RESULTADO INTO OUTFILE '" .$archivo_os_sap
				."' CHARACTER SET latin1
				FIELDS TERMINATED BY ';'			
				LINES TERMINATED BY '\n' ");
	echo "/n";
	echo $query;
	echo "/n";
	$res = $db->ExecuteQuery($query);
	

	/*$res = $db->ExecuteQuery("UPDATE notificacion SET noti_estado='ENTREGADA' 
								WHERE noti_estado='DESPACHADA'
								AND noti_modulo='OS'
                    			AND noti_id_relacionado in (select orse_id from orden_servicio where orse_estado in ('ANULADA', 'APROBADA','RECHAZADA') and orse_fecha_solicitud<= NOW())
                    			");
*/
?>
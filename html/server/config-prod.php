<?php

//________________________________________________________________
//Configuración DB
//$DB_HOST = '192.168.2.86';
//$DB_USER = 'root';
//$DB_PASS = 'Panchita23.';
//$DB_NAME = 'temporalcentro';

$DB_HOST = 'pro-rds-siom-ec.cb0rs0uevfmi.us-east-1.rds.amazonaws.com';
$DB_USER =  'user-app-ec';
$DB_PASS = 'Everis#2018';
$DB_NAME = 'prosiomec';

//________________________________________________________________
//Directorio servicio
$BASEDIR = "/var/www/siom.movistar.ec/html/";

//________________________________________________________________
//Configuración  Google
$MAPS_API_KEY = "AIzaSyCouhkFXGlVzYKDzHME-sJ2pq6iTw0UtrU";
$PUSH_API_KEY = "AIzaSyCyCS3-ZVsDEza5VtfwHBsi_nEixsit4es";
//$PUSH_API_KEY = "AIzaSyBYr-6yzbJqncLUNj6iTVBdaCJi6Jogqno";
//$PUSH_API_KEY = "AIzaSyA4tYtmN3JJ39zja413y8hVdvVl8WfUzL0";
$PUSH_URL     = "https://android.googleapis.com/gcm/send";

//________________________________________________________________
//Configuración aplicación
$DEF_CONFIG = array(
	"app_timezone" => "-04:00",
    "geofence_enabled" => true,
    "max_file_upload" => 20971520, //20 M
	"os"=>array(
    		"colorStatus" => array(
    			"OSGU"=>array("warning_offset"=>3), //en horas
    			"OSGN"=>array("warning_offset"=>24),
    			"OSEU"=>array("warning_offset"=>3),
    			"OSEN"=>array("warning_offset"=>24),
    			"OSI" =>array("warning_offset"=>24)
    		),
    		"minDescription"=>0,//10,
            "formularios"=>array(
                "1"=>"Ingreso a sitio",
                "2"=>"Situación encontrada",
                "3"=>"Subida de servicio",
                "4"=>"Salida de sitio",
                ),
			"formulario_ingreso_id" => 1,
			"formulario_salida_id" => 4	
    	),
    "mnt"=>array(
            "minDescription"=>0,
            "formIngreso"=>1,
            "formSalida"=>4,
            "diasFechaProgramada"=>15,
            "estadoMPPCancelado"=>"NO REALIZADO",
			"estadoMPPAprobadoAutomaticamente"=>"APROB_AUTOMATICA"
        ),
    "insp"=>array(
            "subeReparos"=>42,
            "subeRechazada"=>43,
            "formIngreso"=>1,
            "formSalida"=>4,
        ),
    "sla"=>array(
            "idFormularioSubidaServicio"=>3
        )
);

//________________________________________________________________
//Configuracion de log
$LOGS_CONFIG = array(
    'rootLogger' => array(
        //'level' => "ERROR",
        'appenders' => array('default'),
    ),
    'appenders' => array(
        'default' => array(
            'class' => 'LoggerAppenderDailyFile',
            'layout' => array(
                'class' => 'LoggerLayoutPattern',
                'params' => array(
                    'conversionPattern' => '%date{d.m.Y H:i:s,u} [%-5level] %msg%n'
                )
            ),
            'params' => array(
                'file' => '/var/www/siom.movistar.ec/html/logs/siom_%s.log',
                'datePattern' => 'Ymd'
            )
        )
    )
);

?>

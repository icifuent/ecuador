 <?php
include("db.php");

echo "Actualizacion periodica SLA\n";

$db = new MySQL_Database();
$db->startTransaction();

global $DEF_CONFIG;
$id_form_subida_servicio = $DEF_CONFIG['sla']['idFormularioSubidaServicio'];

//________________________________________________
echo "Actualizacion tabla sla_orden_servicio\n";

//Marcamos las que usaremos para la actualizacion
$res = $db->ExecuteQuery("
UPDATE orden_servicio
SET orse_sla_incluida = 2 
WHERE
    orse_sla_incluida = 0
    AND orse_fecha_validacion IS NOT NULL 
    AND orse_estado IN ('RECHAZADA','APROBADA')   
    AND orse_tipo IN ('OSGN', 'OSEN', 'OSGU', 'OSEU')  
;
");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(1);
}

//Realizamos el INSERT
$res = $db->ExecuteQuery("
INSERT INTO sla_orden_servicio 
SELECT 
    os.orse_id
    ,os.cont_id
    ,os.orse_tipo
    ,os.orse_estado
    ,os.orse_indisponibilidad
    ,os.orse_descripcion
    ,os.orse_fecha_creacion
    ,os.orse_fecha_solicitud
    ,subida_serv.rtfr_fecha AS subida_servicio
    ,pres.pres_fecha_creacion
    ,pres.pres_fecha_validacion
    ,info.info_fecha_creacion
    ,info.info_fecha_validacion
    ,os.orse_fecha_validacion
    ,info.info_id
    ,regi.regi_id
    ,regi.regi_nombre
    ,empl.empl_id
    ,empl.empl_nombre
    ,espe.espe_id
    ,espe.espe_nombre
	
    ,GREATEST(TIMESTAMPDIFF(SECOND,os.orse_fecha_creacion,os.orse_fecha_validacion),0) AS sla_tiempo_orse
    ,GREATEST(TIMESTAMPDIFF(SECOND,os.orse_fecha_solicitud,subida_serv.rtfr_fecha),0) AS sla_tiempo_respuesta
    ,GREATEST(TIMESTAMPDIFF(SECOND,subida_serv.rtfr_fecha,pres.pres_fecha_creacion),-100000000) AS sla_entrega_presupuesto
    ,GREATEST(TIMESTAMPDIFF(SECOND,pres.pres_fecha_creacion,pres.pres_fecha_validacion),0) AS sla_validacion_presupuesto
    ,GREATEST(TIMESTAMPDIFF(SECOND,subida_serv.rtfr_fecha,info.info_fecha_creacion),0) AS sla_entrega_informe
    ,GREATEST(TIMESTAMPDIFF(SECOND,info.info_fecha_creacion,info.info_fecha_validacion),0) AS sla_validacion_informe
    ,GREATEST(TIMESTAMPDIFF(SECOND,COALESCE(GREATEST(pres.pres_fecha_validacion,info.info_fecha_validacion),pres.pres_fecha_validacion,info.info_fecha_validacion),os.orse_fecha_validacion),0) AS sla_solucion_final
	
    ,0
    ,0
    ,NULL
    
    ,0
    ,NULL
    ,0
    ,NULL
    
    ,TIMESTAMPDIFF(SECOND,os.orse_fecha_creacion,subida_serv.rtfr_fecha) AS sla_disponibilidad
    ,0
    ,0
    ,NULL
    
    ,os.usua_creador
    ,usua.usua_nombre AS usua_creador_nombre
    ,jefe_cuadrilla.usua_id AS ultimo_jefe_cuadrilla
    ,jefe_cuadrilla.usua_nombre AS ultimo_jefe_cuadrilla_nombre
	
FROM 
    orden_servicio os
    LEFT JOIN (
            SELECT * FROM (
                    SELECT 
                            p.orse_id,
                            p.pres_nombre,
                            p.pres_estado,
                            p.pres_fecha_creacion,
                            p.pres_fecha_validacion,
                            r.repo_ruta AS pres_ruta
                    FROM
                            presupuesto p
                            INNER JOIN repositorio r ON ( r.repo_tabla = 'presupuesto' AND r.repo_tipo_doc='PRESUPUESTO' AND r.repo_tabla_id=p.pres_id )

                    ORDER BY orse_id ASC, pres_fecha_validacion DESC
            ) pres_inner
            GROUP BY orse_id
    ) pres ON os.orse_id=pres.orse_id
    LEFT JOIN (
            SELECT * FROM (
                    SELECT 
                            tare_id_relacionado
                            ,rtfr_fecha
                    FROM
                            tarea tare
                            INNER JOIN rel_tarea_formulario_respuesta rtfr ON (tare.tare_id = rtfr.tare_id)
                    WHERE
                            tare_modulo = 'OS'
                            AND tare_tipo = 'VISITAR_SITIO'
                            AND tare_estado = 'REALIZADA'
                            AND rtfr_fecha IS NOT NULL
                            AND form_id = $id_form_subida_servicio
                    ORDER BY
                            tare_id_relacionado ASC
                            ,rtfr.rtfr_fecha DESC
            ) tareas
            GROUP BY tare_id_relacionado
    ) subida_serv ON os.orse_id=subida_serv.tare_id_relacionado

    LEFT JOIN (
            SELECT * FROM (
                    SELECT 
                            i.info_id_relacionado
                            ,i.info_id
                            ,i.info_estado
                            ,i.info_fecha_creacion
                            ,i.info_fecha_validacion
                            ,i.info_data
                            ,i.info_observacion
                            ,r.repo_ruta as info_ruta
                    FROM 
                            informe i
                            INNER JOIN repositorio r ON ( r.repo_tabla = 'informe' AND r.repo_tipo_doc='INFORME' AND r.repo_tabla_id=i.info_id )
                    WHERE 
                            i.info_modulo = 'OS'
                    ORDER BY 
                            i.info_id_relacionado ASC
                            ,i.info_fecha_validacion DESC
                    ) info
            GROUP BY info_id_relacionado
    ) info ON os.orse_id=info.info_id_relacionado
    
    LEFT JOIN (
        SELECT * FROM (
                SELECT 
                        os.orse_id
                        ,osa.oras_fecha_asignacion
                        ,roau.usua_id
                        ,u.usua_nombre
                FROM 
                        orden_servicio os 
                        INNER JOIN orden_servicio_asignacion osa ON os.orse_id = osa.orse_id
                        INNER JOIN rel_orden_servicio_asignacion_usuario roau ON osa.oras_id = roau.oras_id
                        INNER JOIN usuario u ON roau.usua_id = u.usua_id
                WHERE 
                        roau.roau_tipo = 'JEFECUADRILLA'
                ORDER BY 
                        os.orse_id ASC
                        ,oras_fecha_asignacion DESC
                ) jefe_cuadrilla_inner
        GROUP BY 
                orse_id
    ) jefe_cuadrilla ON os.orse_id=jefe_cuadrilla.orse_id


    #para filtros
    INNER JOIN emplazamiento empl ON os.empl_id = empl.empl_id
    INNER JOIN comuna comu ON empl.comu_id = comu.comu_id
    INNER JOIN provincia prov ON comu.prov_id = prov.prov_id
    INNER JOIN region regi ON prov.regi_id = regi.regi_id
    INNER JOIN subespecialidad sube ON os.sube_id = sube.sube_id
    INNER JOIN especialidad espe ON sube.espe_id = espe.espe_id
    INNER JOIN usuario usua ON os.usua_creador = usua.usua_id

WHERE
    os.orse_sla_incluida = 2
GROUP BY orse_id
;
");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(2);
}


//Marcamos las procesadas
$res = $db->ExecuteQuery("
UPDATE orden_servicio
SET orse_sla_incluida = 1 
WHERE orse_sla_incluida = 2
;
");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(3);
}



//________________________________________________
echo "Actualizacion tabla sla_mantenimiento\n";

//Marcamos las que usaremos para la actualizacion
$res = $db->ExecuteQuery("
UPDATE mantenimiento
SET mant_sla_incluida = 2 
WHERE
    mant_sla_incluida = 0
    AND (mant_fecha_validacion IS NOT NULL OR mant_estado = 'NO REALIZADO') 
    AND mant_estado IN ('RECHAZADA','APROBADA','FINALIZADA','NO REALIZADO')    
    AND clas_id IN (SELECT clas_id FROM emplazamiento_clasificacion WHERE clas_nombre REGEXP BINARY '.*[AB][ABCD]+' )
;
");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(1);
}

//Realizamos el INSERT
$res = $db->ExecuteQuery("
    INSERT INTO sla_mantenimiento (

    SELECT 
            mant.mant_id
            ,mant.cont_id
            ,peri.peri_id
            ,peri.peri_nombre
            ,mant.mant_estado
            ,mant.mant_responsable
            ,mant.mant_descripcion	

            ,mant.mant_fecha_programada
            ,mant.mant_fecha_ejecucion
            ,IF( mant_estado = 'NO REALIZADO', DATE_SUB( DATE_ADD(mape.mape_fecha_inicio,INTERVAL peri.peri_meses MONTH), INTERVAL 1 DAY ), mant.mant_fecha_validacion ) AS mant_fecha_validacion

            ,info.info_id
            ,regi.regi_id
            ,regi.regi_nombre
            ,empl.empl_id
            ,empl.empl_nombre
            ,espe.espe_id
            ,espe.espe_nombre
            ,clas.clas_id
            ,clas.clas_nombre
            ,empr.empr_id
            ,empr.empr_nombre

            ,mant.usua_creador
            ,IFNULL(usua_creador.usua_nombre,'') AS usua_creador_nombre
            ,mant.usua_validador
            ,usua_validador.usua_nombre AS usua_validador_nombre	

            ,0
            ,NULL
            ,0
            ,0
            ,NULL

    FROM 
            mantenimiento mant

            #para filtros
            INNER JOIN emplazamiento empl ON mant.empl_id = empl.empl_id
            INNER JOIN emplazamiento_clasificacion clas ON mant.clas_id = clas.clas_id
            INNER JOIN empresa empr ON mant.empr_id = empr.empr_id
            INNER JOIN comuna comu ON empl.comu_id = comu.comu_id
            INNER JOIN provincia prov ON comu.prov_id = prov.prov_id
            INNER JOIN region regi ON prov.regi_id = regi.regi_id
            INNER JOIN especialidad espe ON mant.espe_id = espe.espe_id
            LEFT JOIN usuario usua_creador ON mant.usua_creador = usua_creador.usua_id
            LEFT JOIN usuario usua_validador ON mant.usua_validador = usua_validador.usua_id
            INNER JOIN mantenimiento_periodos mape ON mant.mape_id = mape.mape_id
            INNER JOIN rel_contrato_periodicidad rcpe ON mape.rcpe_id = rcpe.rcpe_id
            INNER JOIN periodicidad peri ON rcpe.peri_id = peri.peri_id
            LEFT JOIN (
                SELECT * FROM (
                    SELECT 
                        i.info_id_relacionado
                        ,i.info_id
                        ,i.info_fecha_creacion
                        ,i.info_fecha_validacion
                    FROM 
                        informe i
                        INNER JOIN repositorio r ON ( r.repo_tabla = 'informe' AND r.repo_tipo_doc='INFORME' AND r.repo_tabla_id=i.info_id )
                    WHERE 
                        i.info_modulo = 'MNT'
                    ORDER BY 
                        i.info_id_relacionado ASC
                        ,i.info_fecha_validacion DESC
                    ) info
                GROUP BY info_id_relacionado
            ) info ON mant.mant_id=info.info_id_relacionado
    WHERE 
            mant.mant_sla_incluida = 2
    )
    ;
    ");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(2);
}


//Marcamos las procesadas
$res = $db->ExecuteQuery("
UPDATE mantenimiento
SET mant_sla_incluida = 1 
WHERE mant_sla_incluida = 2
;
");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(3);
}



//________________________________________________
echo "Actualizacion tabla sla_inspeccion\n";

//Marcamos las que usaremos para la actualizacion
$res = $db->ExecuteQuery("
UPDATE inspeccion
SET insp_sla_incluida = 2 
WHERE
    insp_sla_incluida = 0
    AND insp_fecha_validacion IS NOT NULL 
    AND insp_tipo = 'MPP'
    AND insp_estado = 'ACEPTADA_REPAROS'
;
");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(1);
}

//Realizamos el INSERT
$res = $db->ExecuteQuery("
    INSERT INTO sla_inspeccion (

    SELECT 
        insp.insp_id
        ,insp.cont_id
        ,insp.mant_id
        #,insp.insp_tipo
        ,insp.insp_estado
        ,insp.insp_responsable
        ,insp.insp_descripcion	

        ,insp.insp_fecha_creacion
        ,insp.insp_fecha_solicitud
        ,insp.insp_fecha_validacion
        ,info.info_id

        ,regi.regi_id
        ,regi.regi_nombre
        ,empl.empl_id
        ,empl.empl_nombre
        ,espe.espe_id
        ,espe.espe_nombre
        ,empr.empr_id
        ,empr.empr_nombre

        ,insp.usua_creador
        ,usua_creador.usua_nombre AS usua_creador_nombre
        ,insp.usua_validador
        ,usua_validador.usua_nombre AS usua_validador_nombre	

        ,0
        ,NULL
        
    FROM 
        inspeccion insp
        #para filtros
        INNER JOIN emplazamiento empl ON insp.empl_id = empl.empl_id
        INNER JOIN empresa empr ON insp.empr_id = empr.empr_id
        INNER JOIN comuna comu ON empl.comu_id = comu.comu_id
        INNER JOIN provincia prov ON comu.prov_id = prov.prov_id
        INNER JOIN region regi ON prov.regi_id = regi.regi_id
        INNER JOIN especialidad espe ON insp.espe_id = espe.espe_id
        #INNER JOIN mantenimiento mant ON insp.mant_id = mant.mant_id
        LEFT JOIN usuario usua_creador ON insp.usua_creador = usua_creador.usua_id
        LEFT JOIN usuario usua_validador ON insp.usua_validador = usua_validador.usua_id
        LEFT JOIN (
            SELECT * FROM (
                SELECT 
                    i.info_id_relacionado
                    ,i.info_id
                    ,i.info_estado
                    ,i.info_fecha_creacion
                    ,i.info_fecha_validacion
                    ,i.info_data
                    ,i.info_observacion
                    ,r.repo_ruta as info_ruta
                FROM 
                    informe i
                    INNER JOIN repositorio r ON ( r.repo_tabla = 'informe' AND r.repo_tipo_doc='INFORME' AND r.repo_tabla_id=i.info_id )
                WHERE 
                    i.info_modulo = 'INSP'
                ORDER BY 
                    i.info_id_relacionado ASC
                    ,i.info_fecha_validacion DESC
                ) info
            GROUP BY info_id_relacionado
        ) info ON insp.insp_id=info.info_id_relacionado
    WHERE 
        insp.insp_sla_incluida = 2
    )
    ;
    ");
if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(2);
}


//Marcamos las procesadas
$res = $db->ExecuteQuery("
UPDATE inspeccion
SET insp_sla_incluida = 1 
WHERE insp_sla_incluida = 2
;
");

if($res['status']==0){
	$db->Rollback();
	echo $res['error']."\n";
	exit(3);
}



$db->Commit();

echo "Procedimiento finalizado exitosamente\n";
exit(0);
?>
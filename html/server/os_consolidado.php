 <?php
include("db.php");

echo "Actualizacion periodica OS CONSOLIDADO\n";

$db = new MySQL_Database();
$db->startTransaction();


//________________________________________________
echo "Actualizacion tabla os_consolidado\n";

	//Borramos los registros con estado distinto ACTIVO (orse_estado)
	$res = $db->ExecuteQuery("delete from os_consolidado where orse_estado = 'ACTIVO'");
	if($res['status']==0){
		$db->Rollback();
		echo "Error al borrar datos desde os_consolidado";
		echo $res['error']."\n";
		exit(1);
	};
	
	//Truncamos la tabla temporal (orse_estado)
	$res = $db->ExecuteQuery("Truncate Table os_consolidado_temporal");
	if($res['status']==0){
		$db->Rollback();
		echo "Error al borrar datos desde os_consolidado_temporal";
		echo $res['error']."\n";
		exit(2);
	};
	
	
	$fecha_validacion = getdate();
	print_r($fecha_validacion);
	
	if(strlen($fecha_validacion[mon])==1){
		$fecha_validacion[mon] = "0$fecha_validacion[mon]";
	}
	if(strlen($fecha_validacion[mday])==1){
		$fecha_validacion[mday] = "0$fecha_validacion[mday]";
	}
	$fecha_validacion_ini = "$fecha_validacion[year]-$fecha_validacion[mon]-$fecha_validacion[mday] 00:00:00";
	$fecha_validacion_fin = "$fecha_validacion[year]-$fecha_validacion[mon]-$fecha_validacion[mday] 23:59:59";

	//$fecha_validacion_ini ="2015-01-01 00:00:00";
	//$fecha_validacion_fin ="2017-01-31 23:59:59";
	print_r ($fecha_validacion_ini);
	print_r ($fecha_validacion_fin);
	
	


	//Realizamos el INSERT a la tabla temporal
	$query="
	INSERT INTO os_consolidado_temporal 
		SELECT DISTINCT
			orden_servicio.orse_id ,
			orden_servicio.cont_id,
			orse_tipo ,
			espe_nombre,
			sube_nombre,
			empr_nombre,
			usuario_creador.usua_nombre,
			orse_fecha_creacion,
			orse_fecha_solicitud,
			(	SELECT MAX(tare_fecha_despacho) 
				FROM tarea WHERE tare_modulo='OS' 
					AND tare_id_relacionado=orden_servicio.orse_id
			) as tare_fecha_despacho,
			emplazamiento_visita.emvi_fecha_ingreso,
			emplazamiento_visita.emvi_fecha_salida,
			orse_fecha_validacion,
			jefe.usua_nombre as usua_jefe_cuadrilla,
			orse_descripcion,
			(	SELECT 
				GROUP_CONCAT(DISTINCT tecnicos.usua_nombre)
				FROM rel_orden_servicio_asignacion_usuario AS tecnicos_asignado 
				INNER JOIN usuario AS tecnicos ON (tecnicos.usua_id=tecnicos_asignado.usua_id)
				WHERE
				  tecnicos_asignado.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) 
				  AND tecnicos_asignado.roau_tipo='ACOMPANANTE'
			) as orse_tecnico_asignado,
			(
				SELECT
				ROUND(SUM(prit_cantidad * lpu_item_precio.lpip_precio),2) AS pres_valor
				FROM
				presupuesto
				INNER JOIN presupuesto_item ON(presupuesto.pres_id=presupuesto_item.pres_id)
				INNER JOIN lpu_item_precio ON(presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
				WHERE orse_id=orden_servicio.orse_id
				GROUP BY presupuesto.pres_id
				ORDER BY CASE WHEN pres_estado = 'APROBADO' THEN 1 ELSE CASE WHEN pres_estado = 'PREAPROBADO' THEN 2 ELSE 3 END END, pres_fecha_creacion DESC
				LIMIT 1
			) as valor_ult_presupuesto,
			orse_estado as orse_estado_proceso,        
			IF(orse_estado IN ('NOACTIVO','ANULADA'),'ANULADO', IF(orse_estado IN ('RECHAZADA','APROBADA'),'FINALIZADO','ACTIVO')) orse_estado,		  
			empl_nombre,
			empl_direccion,
			clas_nombre,
			clpr_nombre,
			duto_nombre,
			IF(empl_observacion_ingreso=NULL,'NO','SI') requiere_acceso_emp,
			TRIM(REPLACE(REPLACE( REPLACE(empl_observacion_ingreso, '\n', ' '), '\r', ' '), '\t', ' ')) AS 'permisos_acceso_emp' ,
			regi_nombre,
			(
				SELECT 
					zona_nombre
				FROM rel_zona_emplazamiento 
				INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='MOVISTAR')
				WHERE rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
				LIMIT 1
			) as zona_movistar,
			(
				SELECT 
					zona_nombre
				FROM rel_zona_emplazamiento 
				INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CONTRATO')
				WHERE 
					rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
				LIMIT 1
			) as zona_contrato,
			(	SELECT 
					zona_nombre
				FROM rel_zona_emplazamiento 
				INNER JOIN zona ON (zona.zona_id = rel_zona_emplazamiento.zona_id AND zona_tipo='CLUSTER')
				WHERE 
					rel_zona_emplazamiento.empl_id = emplazamiento.empl_id
				LIMIT 1
			) as cluster,
			(
				SELECT DISTINCT
				GROUP_CONCAT(DISTINCT usuario.usua_nombre)
				FROM rel_zona_emplazamiento
				INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
				INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
				INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id  AND usuario.usua_estado='ACTIVO')
				INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
				INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre='GESTOR')
				WHERE
					rel_zona_emplazamiento.empl_id=orden_servicio.empl_id
			) as responsable_emp,
			orden_servicio.orse_tag as orse_tag			
			FROM orden_servicio
				INNER JOIN subespecialidad ON  ( orden_servicio.orse_tipo <> 'OSI') AND (subespecialidad.sube_id = orden_servicio.sube_id)
				INNER JOIN especialidad ON (especialidad.espe_id = subespecialidad.espe_id)   
				INNER JOIN empresa ON (empresa.empr_id = orden_servicio.empr_id)              
				INNER JOIN usuario AS usuario_creador ON (usuario_creador.usua_id = orden_servicio.usua_creador)
				LEFT JOIN rel_orden_servicio_asignacion_usuario AS jefe_asignado ON (jefe_asignado.oras_id = (SELECT oras_id FROM orden_servicio_asignacion WHERE orden_servicio_asignacion.orse_id = orden_servicio.orse_id ORDER BY oras_id DESC LIMIT 1) AND jefe_asignado.roau_tipo='JEFECUADRILLA') 
				LEFT JOIN usuario AS jefe ON (jefe.usua_id=jefe_asignado.usua_id)
				INNER JOIN emplazamiento ON (emplazamiento.empl_id = orden_servicio.empl_id)          
				INNER JOIN emplazamiento_clasificacion ON (emplazamiento_clasificacion.clas_id = emplazamiento.clas_id)
				INNER JOIN emplazamiento_dueno_torre ON (emplazamiento_dueno_torre.duto_id = emplazamiento.duto_id)
				INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id = orden_servicio.empl_id AND rel_contrato_emplazamiento.cont_id = orden_servicio.cont_id )
				INNER JOIN clasificacion_programacion ON (clasificacion_programacion.clpr_id = rel_contrato_emplazamiento.clpr_id AND clpr_activo = 'ACTIVO')
				INNER JOIN comuna ON (comuna.comu_id = emplazamiento.comu_id)
				INNER JOIN provincia ON (provincia.prov_id = comuna.prov_id)
				INNER JOIN region ON (region.regi_id = provincia.regi_id)
				LEFT JOIN emplazamiento_visita ON 	(emplazamiento_visita.emvi_id = (SELECT emvi_id  
																						FROM emplazamiento_visita 
																						WHERE emvi_modulo='OS' 
																							AND emvi_id_relacionado=orden_servicio.orse_id 
																							AND emplazamiento_visita.empl_id=orden_servicio.empl_id 
																						ORDER BY emvi_id DESC LIMIT 1
																					)
													)
				LEFT JOIN presupuesto ON (presupuesto.orse_id = orden_servicio.orse_id)
				LEFT JOIN presupuesto_item ON (presupuesto_item.pres_id = presupuesto.pres_id)
				LEFT JOIN lpu_item_precio ON (presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
				LEFT JOIN lpu_item ON (lpu_item_precio.lpit_id=lpu_item.lpit_id)
				LEFT JOIN lpu_grupo ON (lpu_item.lpgr_id=lpu_grupo.lpgr_id)
		
			WHERE (orse_fecha_creacion >= '$fecha_validacion_ini'	AND orse_fecha_creacion <='$fecha_validacion_fin')
					OR (orse_fecha_solicitud >= '$fecha_validacion_ini'	AND orse_fecha_solicitud <='$fecha_validacion_fin')
					OR (orse_fecha_validacion >= '$fecha_validacion_ini'	AND orse_fecha_validacion <='$fecha_validacion_fin')
					OR (orse_estado IN  ( 'ASIGNADA', 'ASIGNANDO') )
	";
	
	echo $query;
	
	$res = $db->ExecuteQuery($query);
	if($res['status']==0){
		$db->Rollback();
		echo "Problemas al insertar las os_consolidado_temporal";
		echo $res['error']."\n";
		exit(3);
	};
	
	$query="INSERT INTO os_consolidado_temporal_sap
				SELECT DISTINCT
						orden_servicio.orse_id,
						presupuesto.pres_id,
						lpu_item_precio.lpip_id,
						lpgr_nombre , 
						lpit_nombre ,
						lpip_sap_opex ,
						lpip_sap_capex ,
						lpip_precio ,
						prit_cantidad ,
						lpip_precio*prit_cantidad AS 'subtotal_lpu',
						orden_servicio.orse_tag as 'orse_tag' 
					FROM orden_servicio
					LEFT JOIN presupuesto ON (presupuesto.orse_id = orden_servicio.orse_id)
					INNER JOIN presupuesto_item ON (presupuesto_item.pres_id = presupuesto.pres_id)
					INNER JOIN lpu_item_precio ON (presupuesto_item.lpip_id=lpu_item_precio.lpip_id)
					INNER JOIN lpu_item ON (lpu_item_precio.lpit_id=lpu_item.lpit_id)
					INNER JOIN lpu_grupo ON (lpu_item.lpgr_id=lpu_grupo.lpgr_id)
					
					WHERE (orse_fecha_creacion >= '$fecha_validacion_ini'	AND orse_fecha_creacion <='$fecha_validacion_fin')
					OR (orse_fecha_solicitud >= '$fecha_validacion_ini'	AND orse_fecha_solicitud <='$fecha_validacion_fin')
					OR (orse_fecha_validacion >= '$fecha_validacion_ini'	AND orse_fecha_validacion <='$fecha_validacion_fin')
					OR (orse_estado IN  ( 'ASIGNADA', 'ASIGNANDO') )";
	
	echo $query;
	
	
	$res = $db->ExecuteQuery($query);
	if($res['status']==0){
		$db->Rollback();
		echo "Problemas al insertar las os_consolidado_temporal_sap";
		echo $res['error']."\n";
		exit(4);
	};

	
	//Realizamos el INSERT a la tabla consolidada
	$res = $db->ExecuteQuery("
	INSERT INTO os_consolidado 
	SELECT  t.*
	FROM os_consolidado_temporal t 
	WHERE  t.orse_id not in (
				SELECT orse_id 
				FROM os_consolidado )
				
                
	");
	//WHERE  (orse_fecha_creacion >= '$fecha_validacion_ini' AND orse_fecha_creacion <= '$fecha_validacion_fin')
	//			OR (orse_fecha_solicitud >= '$fecha_validacion_ini' AND orse_fecha_solicitud <= '$fecha_validacion_fin')
	//			OR (orse_fecha_validacion >= '$fecha_validacion_ini' AND orse_fecha_validacion <= '$fecha_validacion_fin'))
	if($res['status']==0){
		$db->Rollback();
		echo "Problemas al insertar las os_consolidado";
		echo $res['error']."\n";
		exit(5);
	};
                
	$res = $db->ExecuteQuery("
	INSERT INTO os_consolidado_sap
	SELECT  t.*
	FROM os_consolidado_temporal_sap t 
	WHERE  t.orse_id not in (
				SELECT orse_id 
				FROM os_consolidado_sap )
	");
	if($res['status']==0){
		$db->Rollback();
		echo "Problemas al insertar las os_consolidado_sap";
		echo $res['error']."\n";
		exit(5);
	};

$db->Commit();

echo "Procedimiento finalizado exitosamente\n";
exit(0);
?>

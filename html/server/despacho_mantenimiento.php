 <?php
include("db.php");

if(count($argv)<2){
	die("Debe indicar periodo (BIMESTRAL|TRIMESTRAL|SEMESTRAL|ANUAL)\n");
}
$PERIODO          = $argv[1];
$CONTRATO         = isset($argv[2])?$argv[2]:1;
$EMPRESA          = isset($argv[3])?$argv[3]:2;
$FECHA_INICIO     = isset($argv[4])?$argv[4]:date("Y-m-d");
$FECHA_TERMINO    = isset($argv[5])?$argv[5]:date("Y-m-d");
$FECHA_PROGRAMADA = isset($argv[6])?$argv[6]:date("Y-m-d");
$MES              = isset($argv[7])?$argv[7]:1;

$FORM_INGRESO = 1;
$FORM_SALIDA  = 4;


echo "PERIODO      : $PERIODO $MES\n";
echo "CONTRATO     : $CONTRATO\n";
echo "EMPRESA      : $EMPRESA\n";
echo "FECHA_INICIO : $FECHA_INICIO\n";
echo "FECHA_TERMINO: $FECHA_TERMINO\n\n";

$db = new MySQL_Database();

//________________________________________________
echo "Identificando período $PERIODO\n";
$peri_id = 0;
$res = $db->ExecuteQuery("SELECT peri_id FROM periodicidad WHERE peri_nombre='$PERIODO'");
if($res['status']){
	if(0<$res['rows']){
		$peri_id = $res['data'][0]['peri_id'];
	}
	else{
		echo("Periodo especificado no válido\n");
		exit(1);
	}
}
else{
	echo $res['error']."\n";
	exit(2);
}

//________________________________________________
echo "Verificando período en contrato $CONTRATO\n";
$rcpe_id = 0;
$rcpe_dias_apertura=0;
$rcpe_dias_post_cierre=0;
$res = $db->ExecuteQuery("SELECT rcpe_id,rcpe_dias_apertura,rcpe_dias_post_cierre 
							FROM rel_contrato_periodicidad WHERE cont_id='$CONTRATO' AND peri_id='$peri_id'");
if($res['status']){
	if(0<$res['rows']){
		$rcpe_id 				= $res['data'][0]['rcpe_id'];
		$rcpe_dias_apertura 	= $res['data'][0]['rcpe_dias_apertura'];
		$rcpe_dias_post_cierre 	= $res['data'][0]['rcpe_dias_post_cierre'];
	}
	else{
		echo("Periodo no habilitado en contrato '$CONTRATO'\n");
		exit(3);
	}
}
else{
	echo $res['error']."\n";
	exit(4);
}

//________________________________________________
echo "Obteniendo formularios para el período\n";
$forms = array();
$res = $db->ExecuteQuery("SELECT clas_id,espe_id,form_id 
							FROM mantenimiento_definicion_formulario WHERE cont_id='$CONTRATO' AND peri_id='$peri_id' AND peri_mes='$MES'");
if($res['status']){
	if(0<$res['rows']){
		foreach($res['data'] as $row){
			if(!isset($forms[$row['clas_id']][$row['espe_id']])){
				$forms[$row['clas_id']][$row['espe_id']] = array();
			}
			array_push($forms[$row['clas_id']][$row['espe_id']],$row['form_id']);
		}
	}
	else{
		echo("Periodo sin formulario definidos\n");
		exit(5);
	}
}
else{
	echo $res['error']."\n";
	exit(6);
}

//________________________________________________
echo "Obteniendo emplazamiento con mantenimientos para el período\n";
$emplazamientos = array();
$clas_ids = implode("','",array_keys($forms));
$res = $db->ExecuteQuery("SELECT emplazamiento.empl_id,clas_id,empl_nombre 
						  FROM emplazamiento 	
						  INNER JOIN rel_contrato_emplazamiento ON (rel_contrato_emplazamiento.empl_id=emplazamiento.empl_id)
						  WHERE rel_contrato_emplazamiento.cont_id=$CONTRATO AND 
						  	    rel_contrato_emplazamiento.rece_mpp=1 AND 
								emplazamiento.clas_id IN ('$clas_ids')");
if($res['status']){
	if(0<$res['rows']){
		$emplazamientos	= $res['data'];
	}
	else{
		echo("Sin emplazamiento\n");
		exit(5);
	}
}
else{
	echo $res['error']."\n";
	exit(6);
}

echo "   ".count($emplazamientos) . " con mantenimientos para el período\n";

//________________________________________________
echo "Generando mantenimientos...\n";
$empr_id = $EMPRESA;
$cont_id = $CONTRATO;

$db->startTransaction();

foreach ($emplazamientos as $emplazamiento) {
	$empl_nombre 	= $emplazamiento['empl_nombre'];
	$empl_id 		= $emplazamiento['empl_id'];
	$clas_id 		= $emplazamiento['clas_id'];

	//echo "Creando mantenimiento para $empl_nombre\n";

	foreach($forms[$clas_id] as $espe_id => $f){

		//crear mantenimiento período...
		$res = $db->ExecuteQuery("INSERT INTO mantenimiento_periodos SET 
								 rcpe_id='$rcpe_id',
								 mape_fecha_pre_apertura = DATE_ADD('$FECHA_INICIO', INTERVAL -$rcpe_dias_apertura DAY),
								 mape_fecha_inicio = '$FECHA_INICIO',
								 mape_fecha_cierre = '$FECHA_TERMINO',
								 mape_fecha_post_cierre = DATE_ADD('$FECHA_TERMINO', INTERVAL $rcpe_dias_post_cierre DAY),
								 mape_estado = 'PROCESANDO'");
		if(!$res['status']){
			$db->Rollback();
			echo $res['error']."\n";
			exit(7);
		}
		$mape_id = $res['data'][0]['id'];

		//crear mantenimiento...
		$res = $db->ExecuteQuery("INSERT INTO mantenimiento SET 
								 empr_id='$empr_id',
								 cont_id='$cont_id',
								 mape_id='$mape_id',
								 empl_id='$empl_id',
								 espe_id='$espe_id',
								 mant_fecha_programada='$FECHA_PROGRAMADA'");
		if(!$res['status']){
			$db->Rollback();
			echo $res['error']."\n";
			exit(8);
		}
		$mant_id = $res['data'][0]['id'];

		$values = "($mant_id,$FORM_INGRESO),";
		foreach ($f as $form_id) {
			$values .= "($mant_id,$form_id),";
		}
		$values .= "($mant_id,$FORM_SALIDA)";
		
		//crear mantemineito formularios...
		$res = $db->ExecuteQuery("INSERT INTO rel_mantenimiento_formulario (mant_id,form_id) VALUES $values");
		if(!$res['status']){
			$db->Rollback();
			echo $res['error']."\n";
			exit(8);
		}

		echo "$empl_id => $values\n";
	}
}

$db->Commit();
echo "Procedimiento finalizado exitosamente\n";
exit(0);
?>
<?php

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.
//CACHE
clearstatcache();
clearstatcache("siom.js");

//Chequea login y devuelve  secciones disponibles
Flight::route('POST /login', function(){
    global $DEF_CONFIG;
    global $PERFILES;
    
    $db = new MySQL_Database();

    $user = mysql_real_escape_string($_POST['user']);
    $pass = mysql_real_escape_string($_POST['pass']);
    $res = $db->ExecuteQuery("SELECT 
                                    usua_id,
                                    usua_nombre,
                                    empresa.empr_id,
                                    empr_nombre
                                 FROM usuario
                                 INNER JOIN empresa ON (empresa.empr_id = usuario.empr_id)
                                 WHERE usua_login='$user' AND usua_password='$pass' AND usua_acceso_web='1' AND usua_estado='ACTIVO'");
    if($res['status']){
      if(0<$res['rows']){
        $usuario = $res['data'][0];
        $usua_id = $usuario['usua_id'];


        $res = $db->ExecuteQuery("SELECT 
                                    contrato.cont_id,
                                    cont_nombre
                                 FROM contrato
                                 INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.cont_id=contrato.cont_id AND usua_id=$usua_id 
                                    AND recu_estado='ACTIVO')");
        if(!$res['status']){
            Flight::Log($res['error']);
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
        }  
        $contratos = $res['data'];


        $res = $db->ExecuteQuery("SELECT
                                    perf_nombre
                                  FROM
                                    rel_usuario_perfil
                                  INNER JOIN perfil ON (rel_usuario_perfil.perf_id=perfil.perf_id)
                                  WHERE usua_id=$usua_id");
        if(!$res['status']){
            Flight::Log($res['error']);
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
        }  
        $usuario['usua_cargo'] = array();
        $perfil = array();

        foreach($res['data'] as $row){
          if(isset($PERFILES[$row['perf_nombre']])){
            $perfil = array_merge($perfil,$PERFILES[$row['perf_nombre']]);
          }
          array_push($usuario['usua_cargo'],$row['perf_nombre']);
        }
        $perfil = array_values(array_unique($perfil));
        sort($perfil);

        ////session_start();
        $_SESSION['user_id']    = $usuario['usua_id'];
        $_SESSION['user_name']  = $usuario['usua_nombre'];
        $_SESSION['usua_cargo'] = $usuario['usua_cargo'];
        $_SESSION['empr_id']    = $usuario['empr_id'];
        $_SESSION['empr_nombre']  = $usuario['empr_nombre'];
        $_SESSION['perfil']     = $perfil;
        $_SESSION['logged']     = true;
        $_SESSION['config']     = $DEF_CONFIG;
        $_SESSION['contracts']  = $contratos;
        $_SESSION['sections']   = array();
        $_SESSION['cont_id']    = $contratos[0]['cont_id']; //TODO
		$_SESSION['LAST_ACTIVITY'] = time();
 
        array_push($_SESSION['sections'],array("link"=>"#/dashboard","section"=>"Dashboard","sections"=>array(
                 array('link' =>"#/dashboard/os" ,"section"=>"Orden de servicio" ),                 
                 array("link"=>"#/dashboard/mnt" ,"section"=>"Mantenimiento")
                )));

        array_push($_SESSION['sections'],array("link"=>"#/tracking","section"=>"Tracking"));

        array_push($_SESSION['sections'],array("link"=>"#/emplazamientos","section"=>"Emplazamientos"));
        
        array_push($_SESSION['sections'],array("link"=>"#/mnt","section"=>"Mantenimiento"));
        
        array_push($_SESSION['sections'],array("link"=>"#/os","section"=>"Orden de servicio"));
        
        array_push($_SESSION['sections'],array("link"=>"#/insp","section"=>"Inspecciones"));
		
	    array_push($_SESSION['sections'],array("link"=>"#/defaMovil","section"=>"Reporte falla"));
		
		array_push($_SESSION['sections'],array("link"=>"#/qr","section"=>"Códigos QR"));

        array_push($_SESSION['sections'],array("link"=>"#/inve","section"=>"Inventario"));

        array_push($_SESSION['sections'],array("link"=>"#/docs","section"=>"Documentación"));
        
        array_push($_SESSION['sections'],array("link"=>"#/sla","section"=>"SLA","sections"=>array(
                  array('link' =>"#/sla/tiempos" ,"section"=>"Tiempos de Respuesta")              
                 ,array('link' =>"#/sla/fallas","section"=>"Tasa de Fallas")
                 ,array('link' =>"#/sla/reiteradas","section"=>"Tasa de Fallas Reiteradas")
                 ,array('link' =>"#/sla/disponibilidad" ,"section"=>"Disponibilidad")
                 ,array('link' =>"#/sla/ejecucion" ,"section"=>"Cumplimiento Mantenimientos")
                 ,array('link' =>"#/sla/cronograma" ,"section"=>"Cumplimiento Planificacion")
                 ,array('link' =>"#/sla/calidad" ,"section"=>"Calidad Mantenimientos")
                 ,array('link' =>"#/sla/calidadycronograma" ,"section"=>"Calidad y Cronograma")
		         ,array('link' =>"#/sla/evaluacion" ,"section"=>"Evaluacion")
                )));
        
        array_push($_SESSION['sections'],array("link"=>"#/core","section"=>"Core","sections"=>array(
                 array('link' =>"#/core/contrato" ,"section"=>"Contrato" ),
				 array("link"=>"#/core/emplazamiento_core","section"=>"Emplazamientos"),                 
                 array('link' =>"#/core/empresa" ,"section"=>"Empresas" ),
                 array('link' =>"#/core/curso" ,"section"=>"Cursos" ),            
                 array("link"=>"#/core/usuario","section"=>"Usuarios"),
                 array("link"=>"#/core/formulario","section"=>"Formulario"),
                 array("link"=>"#/core/periodicidad","section"=>"Periodicidad")
                )));
        

        $res = array("status"=>true,
                     "user_id"=>$_SESSION['user_id'],
                     "user_name"=>$_SESSION['user_name'],
                     "user_level"=>$_SESSION['usua_cargo'],
                     "profile"=>$_SESSION['perfil'],
                     "client"=>$_SESSION['empr_nombre'],
                     "config"=>$_SESSION['config'],
                     "sections"=>$_SESSION['sections'],
                     "contracts"=>$_SESSION['contracts'],
                     "contrato"=>$_SESSION['cont_id']);   
      }
      else{
        $res = array("status"=>0,"error"=>"Usuario inválido");
      }
    }

    Flight::json($res);
});

//Login______________________________
Flight::route('GET /islogged', function(){
    //session_start();
    if(isset($_SESSION['user_id'])){
        Flight::json(array("status"=>true,
                           "logged"=>true,
                           "user_id"=>$_SESSION['user_id'],
                           "user_name"=>$_SESSION['user_name'],
                           "user_level"=>$_SESSION['usua_cargo'],
                           "profile"=>$_SESSION['perfil'],
                           "client"=>$_SESSION['empr_nombre'],
                           "config"=>$_SESSION['config'],
                           "sections"=>$_SESSION['sections'],
                           "contracts"=>$_SESSION['contracts'],
                           "contrato"=>$_SESSION['cont_id']));
    }
    else{
        Flight::json(array("status"=>true,"logged"=>0));
    }
    
});

Flight::route('POST /logout', function(){
    //session_start();
    $_SESSION = array();
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }
    session_destroy();

    Flight::json(array("status"=>1));
});

?>

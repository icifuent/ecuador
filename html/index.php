<?php
include_once("server/config.php");
include_once("server/analyticstracking.php");
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<title>SIOM | Sistema Integrador de Operación y Mantenimiento</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="SIOM, Sistema Integrador de Operación y Mantenimiento">
        <meta name="author" content="movistar">
        <meta name="google" content="notranslate">
        
        <meta http-equiv="cache-control" content="max-age=0" />
        <meta http-equiv="cache-control" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        

		<link href="css/normalize.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-datepicker.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-select.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-wizard.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-timepicker.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-fileinput.min.css" rel="stylesheet" media="screen">
        <link href="css/bootstrap-table.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
        <link href="css/pnotify.min.css" rel="stylesheet" media="screen">
        <link href="css/dragula.min.css" rel="stylesheet" media="screen">

        <link href="css/login.css" rel="stylesheet" media="screen">
        <link href="css/main.css" rel="stylesheet" media="screen">
        <link href="css/content.css" rel="stylesheet" media="screen">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
	</head>
	<body>
		<div id="main"></div>
		  
        <script src="https://www.google.com/jsapi" type="text/javascript"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $MAPS_API_KEY;?>&sensor=false" type="text/javascript" ></script>
    	<script src="js/jquery-3.1.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap-datepicker.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/bootstrap-datepicker.es.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/bootstrap-timepicker.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.bootpag.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.timeago.js" type="text/javascript"></script>
        <script src="js/jquery.timeago.es.js" type="text/javascript"></script>

        <script src="js/handlebars.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/swag.min.js" type="text/javascript" charset="utf-8"></script>
        
    	<script src="js/sammy-0.7.6.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/sammy.handlebars.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/sammy.googleanalytics.js" type="text/javascript" charset="utf-8"></script>
        <!--
        <script src="js/sammy.storage.js" type="text/javascript" charset="utf-8"></script>
        
        <script src="js/socket.io-1.3.4.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/siom-websocket.js" type="text/javascript" charset="utf-8"></script>-->
        <script src="js/pnotify.min.js" type="text/javascript" charset="utf-8"></script>

        <script src="js/StyledMarker.js" type="text/javascript" charset="utf-8"></script>

        <script src="<?php echo auto_version('js/siom-sync.js');?>" type="text/javascript" charset="utf-8"></script> 
        <script src="<?php echo auto_version('js/siom-helpers.js');?>" type="text/javascript" charset="utf-8"></script>
    	<script src="<?php echo auto_version('js/siom.js');?>" type="text/javascript" charset="utf-8"></script>	

        <script>
            //Cargar google charts
            google.load("visualization", '1.1', {packages:['corechart','bar'],'language': 'es'});


            //Cargar analytics
            /*
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-60707060-1','auto');// 'none');
            ga('send', 'pageview');
            */
        </script>
  	</body>
</html>

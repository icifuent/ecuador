<?php
include_once("../rest/db.php");

class ExportSIOM
{
	public static $STYLES = array(
			'titulo' => array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '000000'),
			        'size'  => 15,
			        'name'  => 'Verdana'
				)
			),
			'descripcion' => array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '000000'),
			        'size'  => 12,
			        'name'  => 'Verdana'
			    )
			 ),
			 'subtitulo' => array(
			    'font'  => array(
			        'bold'  => false,
			        'color' => array('rgb' => '000000'),
			        'size'  => 10,
			        'name'  => 'Verdana'
				)
			),
			 'error' => array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '990000'),
			        'size'  => 12,
			        'name'  => 'Verdana'
			    )
			  ),
			  'header' => array(
			    'font'  => array(
			        'bold'  => true,
			        'color' => array('rgb' => '000000'),
			        'size'  => 10,
			        'name'  => 'Verdana'
			    )
			  ),
			  'valor' => array(
			    'font'  => array(
			        'bold'  => false,
			        'color' => array('rgb' => '000000'),
			        'size'  => 10,
			        'name'  => 'Verdana'
			    )
			  )
		);

	public static function GenerateExcelPresupuesto($objPHPExcel,$params){
		$db = new MySQL_Database();

		$titulo       =  $params['title'];
		$descripcion  =  $params['description'];
		$subtitulo    =  $params['subtitle'];

		$sheet = $objPHPExcel->setActiveSheetIndex(0);

		$sheet->getCell('A1')->setValue($titulo);
		$sheet->getStyle('A1')->applyFromArray(self::$STYLES['titulo']);
		$sheet->getCell('A2')->setValue($descripcion);
		$sheet->getStyle('A2')->applyFromArray(self::$STYLES['descripcion']);

		$sheet->getCell('A3')->setValue($subtitulo);
		$sheet->getStyle('A3')->applyFromArray(self::$STYLES['subtitulo']);


		$cantidad = array();
		$items    = array();
		for($i=0;$i<count($params['data']);$i++){
			$item = $params['data'][$i]['lpip_id'];

			array_push($items,$item);
			$cantidad[$item] = $params['data'][$i]['prit_cantidad'];
		}

		if(count($items)==0){
			$sheet->getCell('A4')->setValue("No se han especificado items en presupuesto");
		}
		else{
			$items_str = implode("','",$items);
			$query = "SELECT
				       lpu_item.lpit_id AS 'id',
					   zona_nombre AS 'zona',
					   lpgr_nombre AS 'grupo',
					   lpit_nombre AS 'nombre',
					   lpit_comentario AS 'comentario',
					   lpit_unidad AS 'unidad',
					   lpit_sap_capex AS 'sap_capex',
					   lpit_sap_opex AS 'sap_opex',
					   lpit_code1 AS 'code1',
					   lpit_code2 AS 'code2',
					   lpit_code3 AS 'code3',
					   lpit_code4 AS 'code4',
					   lpit_code5 AS 'code5',
					   lpgc_nombre AS 'categoria',
					   lpip_precio AS 'precio',
					FROM
					lpu_item_precio
					INNER JOIN lpu_item ON (lpu_item_precio.lpit_id=lpu_item.lpit_id)
					INNER JOIN lpu_grupo ON (lpu_item.lpgr_id=lpu_grupo.lpgr_id)
					INNER JOIN lpu_grupo_clase ON (lpu_item_precio.lpgc_id=lpu_grupo_clase.lpgc_id)
					INNER JOIN zona ON (lpu_item_precio.zona_id=zona.zona_id)
					WHERE
					lpip_id IN ('$items_str')";
    		$dbo = new MySQL_Database();
   			$res = $dbo->ExecuteQuery($query);
   			if($res['status']){
   				if(0<$res['rows']){
	   				$fields = array_keys($res['data'][0]);
	   				array_push($fields,"cantidad");
	   				array_push($fields,"subtotal");

	   				$col = "A";
	   				$row =  4;

	   				foreach($fields as $field){
	   					$sheet->setCellValue($col.$row,$field);
	   					$sheet->getStyle($col.$row)->applyFromArray(self::$STYLES['header']);
	   					$col++;	
	   				}

	   				$row++;
	   			
	   				foreach($res['data'] as $data){
	   					$col = "A";
	   					foreach($data as $key => $value){
		   					$sheet->setCellValue($col.$row,$value);
		   					$sheet->getStyle($col.$row)->applyFromArray(self::$STYLES['valor']);
		   					$col++;	
		   				}

		   				$sheet->setCellValue($col.$row,$cantidad[$data['lpit_id']]);
		   				$sheet->getStyle($col.$row)->applyFromArray(self::$STYLES['valor']);
		   				$col++;	

		   				$sheet->setCellValue($col.$row,"=".($col-2).$row."+".($col-1).$row);
	   					$sheet->getStyle($col.$row)->applyFromArray(self::$STYLES['valor']);
	
		   				$row++;
	   				}
	   			}
   				else{
   					$sheet->getCell('A4')->setValue("No hay datos");
   				}
   			}
   			else{
   				$sheet->getCell('A4')->setValue($res['error']);
   				$sheet->getStyle('A4')->applyFromArray(self::$STYLES['error']);
   			}
		}
	}
}
?>